package id.catchadeal.kerjayuk.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateTimeDeserializer extends JsonDeserializer {

    Logger logger = LoggerFactory.getLogger(JsonDateTimeDeserializer.class);

    SimpleDateFormat format = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);
    SimpleDateFormat androidRetrofitFormat = new SimpleDateFormat(DateAppConfig.RETROFIT_DATE_FORMAT);

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {


        String dateString = jsonParser.getText();
        if(StringUtils.isEmpty(dateString)){
            return null ;
        }

        Date date = null ;
        try {
            date =  format.parse(dateString);
        } catch (ParseException e) {
//            e.printStackTrace();
        }
        if(date==null){
            try {
                date =  androidRetrofitFormat.parse(dateString);
            } catch (ParseException e) {
                logger.error("TETEP ERROR", e);
                e.printStackTrace();
            }
        }

        return date ;

    }
}
