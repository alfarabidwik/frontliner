package id.catchadeal.kerjayuk.model.misc;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.beans.Transient;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class TaskCategoryDto extends EBaseDto {

    public static final Long REGULER_ID = 1l;
    public static final Long PRODUCT_KEUANGAN_ID = 2l;
    public static final Long MATEL_AND_VISIT_ID = 3l;
    public static final Long SURVEY_ID = 4l;


    int sortir ;

    @NotNull(message = "${name.cannot.be.null}")
    String name ;

//    @NotNull(message = "${name.cannot.be.null}")
    String defaultIcon ;

    @NotNull(message = "${hexaColor.cannot.be.null}")
    String hexaColor ;

    @Pattern(regexp = Constant.ALPHABET_LOWER_PATTERN, message = "topic hanya boleh mengandung alphabet dengan huruf kecil tanpa spasi")
    String topic ;


    long count ;

    Long totalTask = 0l ;
    Long totalAvailableTask = 0l ;


    String defaultIconUrl ;
    String coloredIconUrl ;


}
