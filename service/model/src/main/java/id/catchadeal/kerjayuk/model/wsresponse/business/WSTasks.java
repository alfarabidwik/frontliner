package id.catchadeal.kerjayuk.model.wsresponse.business;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSTasks extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<TaskDto> data ;
}
