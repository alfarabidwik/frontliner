package id.catchadeal.kerjayuk.model.misc;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkingDayDto extends EBaseDto implements Serializable {
    String name ;
}
