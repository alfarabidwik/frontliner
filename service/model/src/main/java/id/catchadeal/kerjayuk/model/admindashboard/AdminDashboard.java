package id.catchadeal.kerjayuk.model.admindashboard;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString(of = "id", callSuper = true)
public class AdminDashboard  extends EBaseDto {

    Long totalUser = 0l ;
    Long thisMonthUser = 0l ;
    Long previousMonthUser = 0l ;

    public Double getThisMonthUserPercent(){
        return Double.valueOf(thisMonthUser)*100/totalUser;
    }
    public Double previousMonthUserPercent(){
        return Double.valueOf(previousMonthUser)*100/totalUser;
    }


    Long totalTask = 0l ;
    Long thisMonthTask = 0l ;
    Long previousMonthTask  = 0l ;

    public Double getThisMonthTaskPercent(){
        return Double.valueOf(thisMonthTask)*100/totalTask;
    }
    public Double previousMonthTaskPercent(){
        return Double.valueOf(previousMonthTask)*100/totalTask;
    }

    Long totalTaskDone = 0l ;
    Long thisMonthTaskDone = 0l ;
    Long previousMonthTaskDone = 0l ;

    public Double getThisMonthTaskDonePercent(){
        return Double.valueOf(thisMonthTaskDone)*100/totalTaskDone;
    }
    public Double previousMonthTaskDonePercent(){
        return Double.valueOf(previousMonthTaskDone)*100/totalTaskDone;
    }

    BigDecimal totalPayment = new BigDecimal(0);
    BigDecimal thisMonthPayment = new BigDecimal(0);
    BigDecimal previousMonthPayment = new BigDecimal(0);

    public Double getThisMonthPaymentPercent(){
        if(thisMonthPayment.compareTo(new BigDecimal(0))<=0){
            return thisMonthPayment.doubleValue();
        }
        if(thisMonthPayment.compareTo(new BigDecimal(0))>0 && totalPayment.compareTo(new BigDecimal(0))>=0){
            return 100.0;
        }
        return thisMonthPayment.multiply(new BigDecimal(100)).divide(totalPayment).doubleValue();
    }
    public Double previousMonthPaymentPercent(){
        if(previousMonthPayment.compareTo(new BigDecimal(0))<=0){
            return previousMonthPayment.doubleValue();
        }
        if(previousMonthPayment.compareTo(new BigDecimal(0))>0 && totalPayment.compareTo(new BigDecimal(0))>=0){
            return 100.0;
        }
        return previousMonthPayment.multiply(new BigDecimal(100)).divide(totalPayment).doubleValue();
    }

}
