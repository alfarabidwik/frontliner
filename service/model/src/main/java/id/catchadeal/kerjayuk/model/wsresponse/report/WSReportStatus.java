package id.catchadeal.kerjayuk.model.wsresponse.report;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.report.ReportStatusDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSReportStatus extends WSResponse {

    //@ApiModelProperty(position = 3)
     ReportStatusDto data ;
}
