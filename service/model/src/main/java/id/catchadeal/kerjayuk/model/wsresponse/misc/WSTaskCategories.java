package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSTaskCategories extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<TaskCategoryDto> data ;
}
