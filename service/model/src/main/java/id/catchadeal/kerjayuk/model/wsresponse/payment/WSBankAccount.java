package id.catchadeal.kerjayuk.model.wsresponse.payment;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankAccountDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSBankAccount extends WSResponse {

    //@ApiModelProperty(position = 3)
     BankAccountDto data ;
}
