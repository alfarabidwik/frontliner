package id.catchadeal.kerjayuk.model.wsresponse.business;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSTaskApplications extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<TaskApplicationDto> data ;
}
