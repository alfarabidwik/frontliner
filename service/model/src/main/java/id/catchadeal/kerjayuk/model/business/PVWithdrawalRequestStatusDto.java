package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PVWithdrawalRequestStatusDto extends EBaseDto {

    WithdrawalRequestDto withdrawalRequest;

    WithdrawalStatusDto withdrawalStatus;
    AdminDto adminExecutor;

    String note ;

    String accountNumber ;

    BankDto bank ;

    String receiptCode ;


    String image ;

    String imageUrl ;

    String statusName ;
    String statusDescription ;



}
