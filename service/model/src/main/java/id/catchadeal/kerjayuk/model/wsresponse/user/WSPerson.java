package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.PersonDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSPerson extends WSResponse {

    //@ApiModelProperty(position = 3)
     PersonDto data ;
}
