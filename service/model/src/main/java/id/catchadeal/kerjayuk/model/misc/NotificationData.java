package id.catchadeal.kerjayuk.model.misc;

import lombok.Data;

import java.io.Serializable;

@Data
public class NotificationData implements Serializable {

    String notificationId ;
    String referenceId ;
    String referenceClass ;
    String addressLink ;
}
