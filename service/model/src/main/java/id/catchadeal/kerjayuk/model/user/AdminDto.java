package id.catchadeal.kerjayuk.model.user;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.role.RoleDto;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
public class AdminDto extends EBaseDto{


     String firstname ;
     String lastname ;

     String email ;
     String password ;
     String image ;

     String adminStatus ;
     RoleDto role ;

    public String getFullname(){
        return firstname+(lastname!=null?" "+lastname:"");
    }


}
