package id.catchadeal.kerjayuk.model.wsresponse.report;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.report.ReportStatusDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSReportStatuses extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<ReportStatusDto> data ;
}
