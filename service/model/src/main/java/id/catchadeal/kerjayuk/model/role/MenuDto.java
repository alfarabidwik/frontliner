package id.catchadeal.kerjayuk.model.role;

import id.catchadeal.kerjayuk.model.EBaseDto;

public class MenuDto extends EBaseDto {

     String name ;
     String clazz ;
     String group ;
     String link ;
}
