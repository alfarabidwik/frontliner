package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class FormLaneApplicationDto extends EBaseDto {
    String title  ;

    List<FormApplicationDto> formApplications = new ArrayList<>();
}
