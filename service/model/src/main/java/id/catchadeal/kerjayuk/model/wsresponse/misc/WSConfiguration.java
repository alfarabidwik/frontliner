package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.ConfigurationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSConfiguration extends WSResponse {

    //@ApiModelProperty(position = 3)
     ConfigurationDto data ;
}
