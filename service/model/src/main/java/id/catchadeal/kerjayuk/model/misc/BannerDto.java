package id.catchadeal.kerjayuk.model.misc;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class BannerDto extends EBaseDto {

     String image ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
     Date startDate ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
     Date endDate ;
     int sortir ;
     String pagelink ;
     String title ;
     String description ;

     String imageLink ;


}
