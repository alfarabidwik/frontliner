package id.catchadeal.kerjayuk.model.wsresponse.psychotest;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.psychotest.PersonalityCharacterDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSPersonalityCharacter extends WSResponse {

    //@ApiModelProperty(position = 3)
     PersonalityCharacterDto data ;
}
