package id.catchadeal.kerjayuk.model.wsresponse.psychotest;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.psychotest.PersonalityDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSPersonality extends WSResponse {

    //@ApiModelProperty(position = 3)
     PersonalityDto data ;
}
