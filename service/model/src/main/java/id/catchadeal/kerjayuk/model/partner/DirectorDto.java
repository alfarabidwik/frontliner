package id.catchadeal.kerjayuk.model.partner;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
public class DirectorDto extends EBaseDto {

    String name ;
    String phoneNumber ;

//    @JsonIgnoreProperties("director")
//    Company partner ;

}
