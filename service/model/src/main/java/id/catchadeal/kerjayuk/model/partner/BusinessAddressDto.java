package id.catchadeal.kerjayuk.model.partner;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString(of = "id", callSuper = true)
public class BusinessAddressDto extends EBaseDto {


    String title ;

    @NotNull(message = "${address.cannot.be.null}")
    String address ;

    @NotNull(message = "${village.cannot.be.null}")
    VillageDto village ;

    @NotNull(message = "${latitude.cannot.be.null}")
    Double latitude ;

    @NotNull(message = "${longitude.cannot.be.null}")
    Double longitude ;


}
