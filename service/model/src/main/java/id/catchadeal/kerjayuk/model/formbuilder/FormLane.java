package id.catchadeal.kerjayuk.model.formbuilder;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class FormLane extends EBaseDto {
    String title  ;
    List<Form> cards = new ArrayList<>();

}
