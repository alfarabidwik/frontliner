package id.catchadeal.kerjayuk.model.user;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
public class RegistrationDto {
    String firstname ;
    String lastname ;
    String mobilePhone ;
    String email ;
    String referralCode ;
    String agentCode ;
    String fullname ;

}
