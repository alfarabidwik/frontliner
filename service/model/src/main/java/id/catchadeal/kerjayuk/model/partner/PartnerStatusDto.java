package id.catchadeal.kerjayuk.model.partner;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class PartnerStatusDto extends EBaseDto {

    String name ;
    String description ;

}
