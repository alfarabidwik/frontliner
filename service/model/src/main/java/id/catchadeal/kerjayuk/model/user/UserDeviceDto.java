package id.catchadeal.kerjayuk.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class UserDeviceDto extends EBaseDto {


    String fcmToken ;
    String platform ;
    String deviceId ;
//    @JsonIgnoreProperties("userDevices")
//    UserDto user;



}

