package id.catchadeal.kerjayuk.model.psychotest;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class PersonalityCharacterDto extends EBaseDto {

    public static final String WEAKNESS = "weakness";
    public static final String STRENGTH = "strength";

    String title ;

    String type ;

    PersonalityDto personality ;

}
