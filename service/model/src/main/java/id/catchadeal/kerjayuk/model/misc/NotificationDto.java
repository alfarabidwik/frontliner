package id.catchadeal.kerjayuk.model.misc;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
public class NotificationDto extends EBaseDto implements Serializable {

    public static final String WITHDRAWAL = "WITHDRAWAL";

    public static final String NEWS = "NEWS";
    public static final String ARTICLE = "ARTICLE";
    public static final String BROADCAST = "BROADCAST";
    public static final String SPECIFIC = "SPECIFIC";
    public static final String NEW_TASK = "NEW_TASK";

    public static final String[] NOTIFICATION_TOPICS = new String[]{NEWS, ARTICLE, BROADCAST, SPECIFIC, NEW_TASK};

    public static final String NOTIFICATION_TOPICS_STRING = NEWS+","+ARTICLE+","+BROADCAST+","+SPECIFIC+", "+NEW_TASK;

    public static final String[] NOTIFICATION_TYPES = new String[]{WITHDRAWAL};
    public static final String NOTIFICATION_TYPES_STRING = WITHDRAWAL;


    String title ;
    String message ;
    String topic ;
    String type ;
    Set<NotificationUserDto> notificationUsers = new HashSet<>();

    NotificationData data ;
    AdminDto admin;

    String image ;
    String imageLink ;

}
