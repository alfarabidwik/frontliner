package id.catchadeal.kerjayuk.model.transaction;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.*;

@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseStatusDto extends EBaseDto {
    String name ;
    String description ;

}
