package id.catchadeal.kerjayuk.model.wsresponse.payment;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankAccountDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSBankAccounts extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<BankAccountDto> data ;
}
