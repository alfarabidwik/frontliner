package id.catchadeal.kerjayuk.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateDeserializer extends JsonDeserializer {
    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        SimpleDateFormat format = new SimpleDateFormat(DateAppConfig.API_DATE);
        String date = jsonParser.getText();
        try {
            if(StringUtils.isEmpty(date)){
                return null ;
            }
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null ;
        }

    }
}
