package id.catchadeal.kerjayuk.model.psychotest;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class PersonalityDto extends EBaseDto {


    @NotNull(message = "${title.cannot.be.empty}")
    String title ;

}
