package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.WorkingDayDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSWorkingDays extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<WorkingDayDto> data ;
}
