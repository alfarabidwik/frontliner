package id.catchadeal.kerjayuk.model.wsresponse.company;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.partner.PersonalDto;
import lombok.Data;

import java.util.Set;

@Data
public class WSPersonals extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<PersonalDto> data ;
}
