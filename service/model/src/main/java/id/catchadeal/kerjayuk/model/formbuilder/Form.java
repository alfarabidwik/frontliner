package id.catchadeal.kerjayuk.model.formbuilder;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class Form extends EBaseDto {

    public static final String TEXT  = "TEXT" ;
    public static final String  EMAIL  = "EMAIL"  ;
    public static final String  NUMBER  = "NUMBER" ;
    public static final String  PHONE  = "PHONE" ;
    public static final String  CURRENCY  = "CURRENCY" ;
    public static final String  DATE  = "DATE" ;
    // static DROPDOWN : string = "DROPDOWN" ;
    public static final String  DROPDOWN_API_DATA  = "DROPDOWN_API_DATA" ;
    public static final String IMAGE  = "IMAGE" ;


    @Getter@Setter String tag ;
    @Getter@Setter String title;
    @Getter@Setter String placeHolder ;
    @Getter@Setter String type;
    @Getter@Setter boolean mandatory ;
    @Getter@Setter String allowedCharacters;
    @Getter@Setter int maxLength ;
    @Getter@Setter int line ;
    @Getter@Setter String fetchApi ;
    @Getter@Setter List<QueryParam> queryParams = new ArrayList<>();
    @Getter@Setter String imageDirectory ;

    @Setter boolean dependAnotherTag ;

    public boolean isDependAnotherTag() {
        this.dependAnotherTag = queryParams!=null && queryParams.size()>0;
        return this.dependAnotherTag ;
    }



}
