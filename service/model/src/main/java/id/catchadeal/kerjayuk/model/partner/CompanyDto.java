package id.catchadeal.kerjayuk.model.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.util.JsonDateDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
public class CompanyDto extends EBaseDto {

    @NotNull(message = "${partner.name.cannot.be.empty}")
    String companyName ;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date buildDate ;
    String officePhoneNumber ;
    String officeFax ;

    String companyDescription ;

    String logo ;

    @NotNull(message = "${latitude.cannot.be.empty}")
    Double latitude ;
    @NotNull(message = "${longitude.cannot.be.empty}")
    Double longitude ;

    @NotNull(message = "${business.field.cannot.be.empty}")
    String businessField ;

    @NotNull(message = "${address.cannot.be.empty}")
    String address ;

    @NotNull(message = "${village.cannot.be.empty}")
    VillageDto village ;

    @NotNull(message = "${corporateType.cannot.be.empty}")
    CorporateTypeDto corporateType ;

//    @JsonIgnoreProperties("partner")
//    NumberOfEmployeeDto numberOfEmployee ;
//
//    @JsonIgnoreProperties("partner")
//    DirectorDto director ;

    String logoUrl ;





}
