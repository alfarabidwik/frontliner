package id.catchadeal.kerjayuk.model.wsresponse.report;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.report.ReportCategoryDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSReportCategory extends WSResponse {

    //@ApiModelProperty(position = 3)
     ReportCategoryDto data ;
}
