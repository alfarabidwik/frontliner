package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.WorkingDayDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSWorkingDay extends WSResponse {

    //@ApiModelProperty(position = 3)
     WorkingDayDto data ;
}
