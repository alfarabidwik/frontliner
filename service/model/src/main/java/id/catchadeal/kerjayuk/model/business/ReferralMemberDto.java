package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@Data
public class ReferralMemberDto extends EBaseDto {

    UserDto user ;

}
