package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.*;

@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class TaskApplicationStatusDto extends EBaseDto {

    public static final Long TAKEN_ID = 1l;
    public static final Long SUBMITTED_ID = 2l;
    public static final Long ACCEPTED_ID = 3l;
    public static final Long REJECTED_ID = 4l;


    public static final String TAKEN_IDS = "1";
    public static final String SUBMITTED_IDS = "2";
    public static final String ACCEPTED_IDS = "3";
    public static final String REJECTED_IDS = "4";


    public TaskApplicationStatusDto(Long id) {
        setId(id);
    }

    String name ;
    String description ;
    int sortir ;
}
