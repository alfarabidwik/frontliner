package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDeviceDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSUserDevice extends WSResponse {

    //@ApiModelProperty(position = 3)
     UserDeviceDto data ;
}
