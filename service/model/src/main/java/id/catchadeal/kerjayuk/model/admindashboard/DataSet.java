package id.catchadeal.kerjayuk.model.admindashboard;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DataSet<OBJECT> {

    public static final String BLUE = "#6a82fb";
    public static final String RED = "#FF0739";
    public static final String GREEN = "#58ff10";
    public static final String INDIGO = "#536dfe";
    public static final String PURPLE = "#9c27b0";
    public static final String PINK = "#ff4081";
    public static final String ORANGE = "#ff9800";
    public static final String YELLOW = "#ffeb3b";
    public static final String TEAL = "#009688";
    public static final String CYAN = "#00bcd4";
    public static final String WHITE = "#fff";


    private String label ;
    private String backgroundColor ;
    private String borderColor ;

    private String stack ;
    private List<OBJECT> data = new ArrayList<>();

}
