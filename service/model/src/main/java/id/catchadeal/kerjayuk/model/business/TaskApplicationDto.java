package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class TaskApplicationDto extends EBaseDto {


    UserDto user ;

    TaskDto task ;

    Set<PVTaskApplicationStatusDto> pvTaskApplicationStatuses = new HashSet<>();

    TaskApplicationStatusDto taskApplicationStatus ;

    String orderReference ;
    String partnerOrderReference ;

    String viewOrderReference ;

    String groupFormName ;

    List<FormLaneApplicationDto> formLaneApplications = new ArrayList<>();

    Set<ReferralMemberDto> referralMembers = new HashSet<>();

    BigDecimal itemFee = new BigDecimal(0);
    BigDecimal totalFee = new BigDecimal(0);

}
