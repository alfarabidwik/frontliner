package id.catchadeal.kerjayuk.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PVTaskApplicationStatusDto extends EBaseDto {

    @JsonIgnoreProperties("pvTaskApplicationStatuses")
    TaskApplicationDto taskApplication ;

    @JsonIgnoreProperties("pvTaskApplicationStatuses")
    TaskApplicationStatusDto taskApplicationStatus ;

    String note ;


}
