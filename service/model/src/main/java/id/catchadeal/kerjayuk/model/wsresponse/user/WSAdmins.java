package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSAdmins extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<AdminDto> data ;
}
