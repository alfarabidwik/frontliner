package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSNotification extends WSResponse {

    //@ApiModelProperty(position = 3)
     NotificationDto data ;
}
