package id.catchadeal.kerjayuk.model.wsresponse.misc;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSTaskCategory extends WSResponse {

    //@ApiModelProperty(position = 3)
     TaskCategoryDto data ;
}
