package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawalStatusDto extends EBaseDto {

    public static final String WAITING_ID = "1";
    public static final String ACCEPTED_ID = "2";
    public static final String ISSUED_ID = "3";
    public static final String REJECTED_ID = "4";
    public static final String CANCELED_ID = "5";


    public static final WithdrawalStatusDto WAITING = new WithdrawalStatusDto(Long.valueOf(WAITING_ID));
    public static final WithdrawalStatusDto ACCEPTED = new WithdrawalStatusDto(Long.valueOf(ACCEPTED_ID));
    public static final WithdrawalStatusDto ISSUED = new WithdrawalStatusDto(Long.valueOf(ISSUED_ID));
    public static final WithdrawalStatusDto REJECTED = new WithdrawalStatusDto(Long.valueOf(REJECTED_ID));
    public static final WithdrawalStatusDto CANCELED = new WithdrawalStatusDto(Long.valueOf(CANCELED_ID));

    public static final String DESCRIPTION = "1 = Waiting \n2 = Accepted \n3 = Issued \n4 = Rejected\n4 = Canceled\"";


    String name ;
    String description ;

    public WithdrawalStatusDto(Long id ){
        this.id = id ;
    }

    public static WithdrawalStatusDto build(Long id){
        return new WithdrawalStatusDto(id);
    }

}
