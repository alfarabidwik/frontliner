package id.catchadeal.kerjayuk.model.formbuilder;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;
@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class GroupFormDto extends EBaseDto {

    String name ;
    Set<FormLane> formLanes = new HashSet<>();

}
