package id.catchadeal.kerjayuk.model.wsresponse.business;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSTaskApplication extends WSResponse {

    //@ApiModelProperty(position = 3)
     TaskApplicationDto data ;
}
