package id.catchadeal.kerjayuk.model.email;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class SendEmailDto extends EBaseDto {


    @NotNull(message = "{subject.cannot.be.empty}")
     String subject ;
     String content ;
     Boolean allCustomer ;
     Boolean allSubscriber ;
     String sendStatus ;
     Boolean sendDirectly ;
     Date sendAt ;
     Boolean regularSender ;

     String sender ;

    List<SendEmailPreference> customerEmails  = new ArrayList<>();
    List<SendEmailPreference> subscriberEmails  = new ArrayList<>();

     List<AttachmentDto> attachments= new ArrayList<>();


     Boolean contentType  ;

}
