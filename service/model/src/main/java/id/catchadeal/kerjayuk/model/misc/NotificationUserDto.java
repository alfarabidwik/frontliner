package id.catchadeal.kerjayuk.model.misc;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class NotificationUserDto extends EBaseDto implements Serializable {

    UserDto user ;
    NotificationDto notification ;
    Boolean read ;

}
