package id.catchadeal.kerjayuk.model.report;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor@NoArgsConstructor
public class ReportCategoryDto extends EBaseDto {

    String name ;
    String description ;

}
