package id.catchadeal.kerjayuk.model.misc;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ToString(of = "id", callSuper = true)
//@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class ConfigurationDto extends EBaseDto {


    String workingAgreementFile ;

    String workingAgreementFileUrl ;

    String name ;
    String companyName = "" ;
    String companyDescription = "";

    String companyAddress ;

    VillageDto companyVillage ;

    String companyPhoneNumber ;
    String companyFaximile ;
    String contactPersonPhoneNumber = "" ;
    String contactWhatsapp ;

    Integer pageRowSize = 10 ;

    String hashtag ;
    String instagramLink ;
    String facebookLink ;
    String twitterLink ;

    BigDecimal minWithdrawal = new BigDecimal(0) ;

    String facebookAppId ;
    String googleClientId ;

    String baseApi ;

    int bannerWidth ;
    int bannerHeight ;
    int mobileBannerWidth ;
    int mobileBannerHeight ;

    String regularEmailSender ;
    String mailDomain ;

    int taskBannerWidth ;
    int taskBannerHeight ;





}
