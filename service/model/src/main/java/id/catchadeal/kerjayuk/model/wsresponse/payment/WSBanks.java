package id.catchadeal.kerjayuk.model.wsresponse.payment;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSBanks extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<BankDto> data ;
}
