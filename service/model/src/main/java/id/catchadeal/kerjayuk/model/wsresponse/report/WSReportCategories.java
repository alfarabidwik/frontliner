package id.catchadeal.kerjayuk.model.wsresponse.report;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.report.ReportCategoryDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSReportCategories extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<ReportCategoryDto> data ;
}
