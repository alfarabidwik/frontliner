package id.catchadeal.kerjayuk.model.business;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.formbuilder.GroupFormDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.partner.PartnerDto;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ToString(of = "id", callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto extends EBaseDto {


    public static final String REFERRAL_AGENT_TASK = "REFERRAL_AGENT_TASK";
    public static final String REGULAR_TASK = "REGULAR_TASK";

    /**
     * Task Period Type
     * */
    public static final String NEVER_END = "NEVER_END";
    public static final String PERIODIC = "PERIODIC";

    /**
     * Application Limit Type
     * */
    public static final String NO_LIMIT = "NO_LIMIT";
    public static final String LIMITED = "LIMITED";


    /**
     * Application Worker Verification Type
     * */
    public static final String MUST_VERIFIED = "MUST_VERIFIED";
    public static final String NO_NEED_VERIFIED = "NO_NEED_VERIFIED";


    PartnerDto partner ;

    @NotNull(message = "${title.cannot.be.null}")
    String title ;
    @NotNull(message = "${subtitle.cannot.be.null}")
    String subtitle ;

    boolean published ;

    @NotNull(message = "${periodType.cannot.be.null}")
    String periodType ;

    @NotNull(message = "${validatingTimeInHour.cannot.be.null}")
    Integer validatingTimeInHour ;

    @NotNull(message = "${description.cannot.be.null}")
    String description ;

    @NotNull(message = "${additionalInfo.cannot.be.null}")
     String additionalInfo ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date startPeriod ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date endPeriod ;

    @NotNull(message = "${applicationLimitType.cannot.be.null}")
    String applicationLimitType ;

    Integer applicationLimitCount = 0 ;

    Integer applicationCount = 0 ;

    @NotNull(message = "${workerVerificationType.cannot.be.null}")
    String workerVerificationType ;

    String bannerImage ;

    String bannerImageUrl ;

    @NotNull(message = "${fee.cannot.be.null}")
    BigDecimal fee ;

    String taskAddressLink ;

    String callbackId ;

    @NotNull(message = "${taskCategory.cannot.be.null}")
    TaskCategoryDto taskCategory ;

    GroupFormDto groupForm ;

    Integer suggestion  ;

    String type ;

    boolean applyable  ;
    String message = null ;



}
