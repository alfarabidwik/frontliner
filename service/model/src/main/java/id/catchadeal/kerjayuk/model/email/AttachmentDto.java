package id.catchadeal.kerjayuk.model.email;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;

@Data
public class AttachmentDto extends EBaseDto {

     String filename ;
     String fileLink ;

}
