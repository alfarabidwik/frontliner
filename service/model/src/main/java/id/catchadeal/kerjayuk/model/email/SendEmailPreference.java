package id.catchadeal.kerjayuk.model.email;

import lombok.Data;

import java.io.Serializable;

@Data
public class SendEmailPreference implements Serializable {
     String email ;
     String responseStatus ;
}
