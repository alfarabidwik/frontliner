package id.catchadeal.kerjayuk.util;

import org.springframework.context.MessageSource;

public class Constant {


    public static final String FORM_MEDIA_DIRECTORY = "/media/kerjayuk/form";

    public static String prefixApi = "";
    public static MessageSource message = null;


    public static final String AUTHORIZATION = "Authorization";

    public static final String ALL = "ALL";
    public static final String ACTIVE = "ACTIVE";
    public static final String EXPIRED = "EXPIRED";


    public static final String AVAILABLE_DISCOUNT_DATE  = ALL+","+ACTIVE+","+EXPIRED;

    public static final Long SYSTEM = -1l;
    public static final String MASTER_PASSWORD = "Ecommerce%Master" ;

    public static final String DD_MM_YYYY_HH_MM = "dd-MM-yyy hh:mm";


    public static final int SUCCESS_CODE = 200 ;
    public static final int FAILED_CODE = 300 ;
    public static final int VERIFICATION_FAILED_CODE = 302 ;
    public static final int ALREADY_VERIFIED_CODE = 303 ;
    public static final int JWT_SESSION_EXPIRED_CODE = 301 ;
    public static final int IMAGE_NOT_FOUND = 301 ;
    public static final int FILE_NOT_FOUND = 301 ;
    public static final int SMS_VERIFICATION_REACH_LIMIT = 301 ;
    public static final String SUCCESS = "SUCCESS" ;
    public static final String DELETED_SUCCESSFULLY = "DELETED_SUCCESSFULLY" ;
    public static final String FAILED = "FAILED" ;
    public static final String ALREADY_EXISTS = "ALREADY_EXISTS" ;

//    public static final String EMAIL_PATTERN = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
    public static final String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String ID_PATTERN = "[0-9]+";
    public static final String ALPHABET_PATTERN = "^[a-zA-Z]*$";
    public static final String ALPHABET_SPACE_PATTERN = "^(?![ ]+$)[a-zA-Z ]*$";
    public static final String NUMERIC_PATTERN = "^[0-9]*$";
    public static final String ALPHABET_LOWER_PATTERN = "^[a-z]*$";


//    public static final String REST_IMAGE_PHOTO_PROFILE = "image/photoProfile";
//    public static final String REST_IMAGE_IDCARD = "image/idcard";
//    public static final String REST_IMAGE_TRANSFER_BILL = "image/transferBill";



    public static final String MASTER_PARTNER_JSON_FILE = "json/partner.json";
    public static final String MASTER_TASK_CATEGORY_JSON_FILE = "json/task-category.json";
    public static final String MASTER_WORKING_DAY_JSON_FILE = "json/working-day.json";
    public static final String MASTER_LONG_TIME_EXPERIENCE_JSON_FILE = "json/long-time-experience.json";
    public static final String MASTER_EMPLOYEE_STATUS_JSON_FILE = "json/employee-status.json";
    public static final String MASTER_PARTNER_STATUS_JSON_FILE = "json/partner-status.json";
    public static final String MASTER_WITHDRAWAL_STATUS_JSON_FILE = "json/withdrawal-status.json";
    public static final String MASTER_CORPORATE_TYPE_JSON_FILE = "json/corporate-type.json";
    public static final String MASTER_PERSONALITY_JSON_FILE = "json/personality.json";
    public static final String MASTER_PSYCHOTEST_JSON_FILE = "json/psychotest.json";
    public static final String MASTER_PRODUCT_JSON_FILE = "json/product.json";
    public static final String MASTER_SELF_PROMOTION_JSON_FILE = "json/self-promotion.json";
    public static final String MASTER_PURCHASE_STATUS_JSON_FILE = "json/purchase-status.json";
    public static final String MASTER_TASK_APPLICATION_STATUS_JSON_FILE = "json/task-application-status.json";
    public static final String MASTER_TASK_JSON_FILE = "json/task.json";
    public static final String MASTER_REFERRAL_TASK_JSON_FILE = "json/referral_task.json";
    public static final String MASTER_SPINNING_ITEM_JSON_FILE = "json/spinning-item.json";
    public static final String MASTER_SUPER_ADMIN_JSON_FILE = "json/super-admin.json";
    public static final String MASTER_USER_JSON_FILE = "json/users.json";
    public static final String MASTER_COMMENT_JSON_FILE = "json/comment.json";
    public static final String MASTER_REPORT_STATUS_JSON_FILE = "json/report-status.json";
    public static final String MASTER_MENU_JSON_FILE = "json/menu.json";
    public static final String MASTER_ROLE_JSON_FILE = "json/role.json";
    public static final String MASTER_ROLE_MENU_JSON_FILE = "json/role-menu.json";




    public static final String REST_BANK_IMAGE = "/image/bank";
    public static final String REST_FAQ_IMAGE = "/image/faq";
    public static final String REST_USER_PHOTO = "/image/user/photo";
    public static final String REST_EMPLOYEE_ID_CARD = "/image/employee/idcard";
    public static final String REST_TASK_CATEGORY_ICON = "/image/task-category";
    public static final String REST_CONFIGURATION_WORKING_AGREEMENT_DOCUMENT = "/image/configuration/workingAgreement";
    public static final String REST_TEMPORARY_IMAGE = "/image/temporary";
    public static final String REST_ADMIN_PHOTO_PROFILE = "/image/admin/photoProfile";
    public static final String REST_USER_ID_CARD = "/image/user/idCard";
    public static final String REST_USER_SELFIE_ID_CARD = "/image/user/selfieIdCard";
    public static final String REST_TASK_BANNER_IMAGE = "/image/task/banner";
    public static final String REST_PARTNER_IMAGE = "/image/partner/image";
    public static final String REST_PARTNER_SIUP_OR_NPWP = "/image/partner/siupOrNpwp";
    public static final String REST_PARTNER_ID_CARD = "/image/partner/idcard";
    public static final String REST_BANNER_IMAGE = "/image/banner";
    public static final String REST_WITHDRAWAL_RECEIPT_IMAGE = "/image/withdrawal/receipt";


    public static final String REST_EMAIL_ATTACHMENT_FILE = "/file/email/attachment";



    public static final Integer PRODUCTION = 1;
    public static final Integer DEVELOPMENT = 0;


    public static final Integer BUILD_MODE = DEVELOPMENT;

//    public static final String PASSWORD_ENCRYPTION_KEY = "InsyaAllahBermanfaatUntukOrangLain";

    public static final String CLIENT_HEADER_KEY = "InsyaAllahBermanfaatUntukOrangLain";


    public static final String ROLE = "role";
    public static final String UNDEFINED = "undefined";
    public static final String USER = "user";
    public static final String ADMIN = "admin";
    public static final String SWAGGER = "swagger";


    public static final String RESET_PASSWORD_TML = "reset_password";
    public static final String RESET_PASSWORD_SUCCESS_TML = "reset_password_success";

    public static final float VERY_LOW_QUALITY = 0.2f;
    public static final float MEDIUM_QUALITY = 0.35f;
    public static final float HIGH_QUALITY = 0.7f;

    public static final String CACHE_CONFIGURATION = "configuration";

    public static final String CACHE_CUSTOMER_ID = "customerId";
    public static final String CACHE_USER_PRODUCTS = "userProducts";
    public static final String CACHE_USER_DASHBOARD = "userDashboard";
    public static final String CACHE_BANNERS = "banners";
    public static final String CACHE_HEADINGS = "headings";
    public static final String CACHE_SELEBGRAMS = "selebgrams";
    public static final String CACHE_ONLINE_SHOPS = "onlineShops";


    public static final String CURRENCY_SYMBOL = "Rp ";

    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final String ANY = "any";


    public static final String PHOTO = "photo";
    public static final String ID_CARD = "id_card";
    public static final String SELFIE_ID_CARD = "selfie_id_card";
    public static final String FAMILY_CARD = "family_card";
    public static final String SKCK = "skck";
    public static final String DIPLOMA = "diploma";
    public static final String CERTIFICATE_1 = "certificate_1";
    public static final String CERTIFICATE_2 = "certificate_2";
    public static final String CERTIFICATE_3 = "certificate_3";
    public static final String SIM = "sim";
    public static final String COMPANY_LOGO = "company_logo";
    public static final String SIUP_OR_NPWP = "siup_or_npwp";

    public static final String DIPLOMA_OR_CERTIFICATE = "diploma_or_certificate";


    public static final String PARTNER_DOCUMENT_VERIFICATION_TYPE = ID_CARD+", "+SIUP_OR_NPWP;
    public static final String EMPLOYEE_DOCUMENT_VERIFICATION_TYPE = ID_CARD+", "+FAMILY_CARD+", "+SKCK+", "+DIPLOMA_OR_CERTIFICATE;
    public static final String IMAGE_UPLOAD_TYPE = PHOTO+", "+ID_CARD+", "+SELFIE_ID_CARD;


    public static final String COMMON_TOPIC = "common";
    public static final String PAYMENT_STATUS_TOPIC = "payment_status";
    public static final String APPLICATION_STATUS_TOPIC = "application_status";
    public static final String JOB_VACANCY_TOPIC = "job_vacancy";
    public static final String ACCOUNT_STATUS_TOPIC = "account_status";
    public static final String NEWS_TOPIC = "news";
    public static final String ARTICLE_TOPIC = "article";
    public static final String CURRENT_PACKAGE_TOPIC = "current_package";

    public static final String FIREBASE_TOPIC_SWAGGER_NOTE = "<b>( Available Topic )</b>\n" +
            "-- common ==> desc = Only for test \n" +
            "-- news ==> desc = Both employee & partner must subscribe it to get notif everytime a news  published \n" +
            "-- article ==> desc = Both employee & partner must subscribe it to get notif everytime an article published \n" ;


    public static final String AVENGER_KEY = "TindakanPenyusupanAdalahPelanggaran,DanKamiAkanMelaporkanTPelanggaranSecepatnyaKepadaPihakYangBerwajib!!)(*&^%$#@!";


    public static final String FIRST_REGISTERED_ADDRESS = "First registered address ";

//    public static final String FIREBASE_TOPIC_SWAGGER_NOTE = "<br>( Available Topic )</b>\n" +
//            "1 = common, desc = Only for test \n" +
//            "2 = payment_status, desc = Both employee & partner must subscribe it to get notif everytime their payment status changed \n" +
//            "3 = application_status, desc = employee could get notif everytime his job application status changed \n" +
//            "4 = job_vacancy, desc = partner could get notif everytime his job vacancy status changed \n" +
//            "5 = account_status, desc = Both employee & partner must subscribe it to get notif everytime their account status changed \n" +
//            "2 = news, desc = Both employee & partner must subscribe it to get notif everytime a news  published \n" +
//            "3 = article, desc = Both employee & partner must subscribe it to get notif everytime an article published \n" ;
//            "7 = current_package, desc = Both employee & partner must subscribe it to get notif everytime their package / product status changed \n" ;

}
