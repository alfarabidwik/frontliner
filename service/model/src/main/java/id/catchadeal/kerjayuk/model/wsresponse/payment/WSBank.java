package id.catchadeal.kerjayuk.model.wsresponse.payment;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSBank extends WSResponse {

    //@ApiModelProperty(position = 3)
     BankDto data ;
}
