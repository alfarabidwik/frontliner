package id.catchadeal.kerjayuk.model.region;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CityDto extends Option {
    String platNumber ;

    @JsonIgnoreProperties("cities")
    ProvinceDto province ;

    String courierId ;

}
