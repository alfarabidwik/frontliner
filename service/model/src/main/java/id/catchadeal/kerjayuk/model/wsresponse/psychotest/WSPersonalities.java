package id.catchadeal.kerjayuk.model.wsresponse.psychotest;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.psychotest.PersonalityDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSPersonalities extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<PersonalityDto> data ;
}
