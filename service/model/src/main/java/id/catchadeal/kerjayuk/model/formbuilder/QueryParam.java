package id.catchadeal.kerjayuk.model.formbuilder;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class QueryParam implements Serializable {
    String key ;
    String reference ;
}
