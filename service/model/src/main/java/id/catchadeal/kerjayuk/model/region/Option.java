package id.catchadeal.kerjayuk.model.region;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;

@Data
public class Option extends EBaseDto {
    String name ;
}
