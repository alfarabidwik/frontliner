package id.catchadeal.kerjayuk.model.region;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VillageDto extends Option {
    String postalCode ;
    @JsonIgnoreProperties("village")
    DistrictDto district ;

//    String courierId ;

}
