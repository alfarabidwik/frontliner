package id.catchadeal.kerjayuk.model.business;

import lombok.Data;

import java.io.Serializable;

@Data
public class MutationInfo  implements Serializable {

     String entityClassName ;
     Long id ;
     String description ;

    public static MutationInfo build(String entityClassName, Long id, String description){
        MutationInfo mutationInfo = new MutationInfo();
        mutationInfo.setEntityClassName(entityClassName);
        mutationInfo.setId(id);
        mutationInfo.setDescription(description);
        return mutationInfo ;
    }
}
