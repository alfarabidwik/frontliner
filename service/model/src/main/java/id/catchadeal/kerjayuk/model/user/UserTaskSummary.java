package id.catchadeal.kerjayuk.model.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class UserTaskSummary {

    Long onGoingTask = 0l;
    Long totalTask = 0l;
}
