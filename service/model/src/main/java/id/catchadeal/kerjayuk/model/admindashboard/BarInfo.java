package id.catchadeal.kerjayuk.model.admindashboard;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class BarInfo {
    private Long id ;
    private String label ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    private Date startDate ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    private Date endDate ;

}
