package id.catchadeal.kerjayuk.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

public class WSResponse<D, E> {

	//@ApiModelProperty(position = 0)
	@Getter @Setter int code ;
	//@ApiModelProperty(position = 1)
	@Getter@Setter String message ;
	//@ApiModelProperty(position = 2)
	@Getter@Setter E messageError ;
	//@ApiModelProperty(position = 3)
	@Getter@Setter D data ;
	//@ApiModelProperty(position = 4)
	@Getter long totalElement ;
	//@ApiModelProperty(position = 5)
	@Getter int totalPage ;
	//@ApiModelProperty(position = 6)
	@Getter int pageElement ;

	public static <D> WSResponse instance(int code, String message, D data){
		WSResponse instance = new WSResponse<>();
		instance.setCode(code);
		instance.setMessage(message);
		instance.setData(data);
		return instance ;
	}

	public static <D> WSResponse instance(int code, String message){
		WSResponse instance = new WSResponse<>();
		instance.setCode(code);
		instance.setMessage(message);
		return instance ;
	}

	public static <E> WSResponse instanceError(int code, String message, E messageError){
		WSResponse instance = new WSResponse<>();
		instance.setCode(code);
		instance.setMessage(message);
		instance.setMessageError(messageError);
		return instance ;
	}

	public static <D> WSResponse instance(int code, D data){
		WSResponse instance = new WSResponse<>();
		instance.setCode(code);
		instance.setData(data);
		return instance ;
	}

	public WSResponse setTotalElement(long totalElement) {
		this.totalElement = totalElement;
		return this ;
	}

	public WSResponse setTotalPage(int totalPage) {
		this.totalPage = totalPage;
		return this;
	}

	public WSResponse setPageElement(int pageElement) {
		this.pageElement = pageElement;
		return this ;
	}

	public final boolean isSuccess(){
		return code==200;
	}

	public void test(){
		
	}
}
