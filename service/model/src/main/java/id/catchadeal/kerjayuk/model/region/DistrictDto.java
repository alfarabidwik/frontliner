package id.catchadeal.kerjayuk.model.region;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DistrictDto extends Option {

    @JsonIgnoreProperties("districts")
    CityDto city ;


    String courierId ;

}
