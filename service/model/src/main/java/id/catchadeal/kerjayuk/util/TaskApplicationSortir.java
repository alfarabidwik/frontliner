package id.catchadeal.kerjayuk.util;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TaskApplicationSortir {


    String label ;
    String sortBy ;
    Boolean ascending ;

    public TaskApplicationSortir(){

    }

    public TaskApplicationSortir(String label, String sortBy, Boolean ascending){
        this.label = label ;
        this.sortBy = sortBy ;
        this.ascending =ascending ;
    }


    public static List<TaskApplicationSortir> sortirs(){
        List<TaskApplicationSortir> taskApplicationSortirs = new ArrayList<>();

        TaskApplicationSortir taskApplicationSortir  = new TaskApplicationSortir();
        taskApplicationSortir.setLabel("Terbaru");
        taskApplicationSortir.setAscending(false);
        taskApplicationSortir.setSortBy("ja.updated");
        taskApplicationSortirs.add(taskApplicationSortir);

        taskApplicationSortir  = new TaskApplicationSortir();
        taskApplicationSortir.setLabel("Terlama");
        taskApplicationSortir.setAscending(true);
        taskApplicationSortir.setSortBy("ja.updated");
        taskApplicationSortirs.add(taskApplicationSortir);

        taskApplicationSortir  = new TaskApplicationSortir();
        taskApplicationSortir.setLabel("Insentif Tertinggi");
        taskApplicationSortir.setAscending(false);
        taskApplicationSortir.setSortBy("jjv.fee");
        taskApplicationSortirs.add(taskApplicationSortir);

        taskApplicationSortir  = new TaskApplicationSortir();
        taskApplicationSortir.setLabel("Insentif Terendah");
        taskApplicationSortir.setAscending(true);
        taskApplicationSortir.setSortBy("jjv.fee");
        taskApplicationSortirs.add(taskApplicationSortir);

        return taskApplicationSortirs ;

    }

}
