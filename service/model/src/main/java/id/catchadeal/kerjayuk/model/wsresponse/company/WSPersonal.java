package id.catchadeal.kerjayuk.model.wsresponse.company;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.partner.PersonalDto;
import lombok.Data;

@Data
public class WSPersonal extends WSResponse {

    //@ApiModelProperty(position = 3)
     PersonalDto data ;
}
