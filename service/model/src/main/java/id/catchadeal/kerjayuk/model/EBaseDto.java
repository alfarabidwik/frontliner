package id.catchadeal.kerjayuk.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class EBaseDto implements Serializable {

    protected Long id ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date created ;
    Long createdBy ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date updated ;
    Long updatedBy ;
    Boolean active ;

    public EBaseDto(){
        if(id==null || id.equals(0) || id == 0){
            id = null ;
        }
    }

    public Long getId() {
        if(id==null || id.equals(0) || id==0){
            id = null ;

        }
        return id;
    }

    public void setId(Long id) {
        if(id==null || id.equals(0) || id==0){
            id = null ;

        }
        this.id = id;
    }


}
