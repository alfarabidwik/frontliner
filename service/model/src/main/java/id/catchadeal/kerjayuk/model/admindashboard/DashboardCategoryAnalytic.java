package id.catchadeal.kerjayuk.model.admindashboard;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class DashboardCategoryAnalytic {

    public static final int HEADING = 1;
    public static final int CATEGORY = 2;


    List<BarInfo> barInfos = new ArrayList<>();
    List<DataSet<BigDecimal>> datasets = new ArrayList<>();

    BigDecimal allUserIncomeAmount = new BigDecimal(0);
    BigDecimal allUserWithdrawAmount = new BigDecimal(0);

}
