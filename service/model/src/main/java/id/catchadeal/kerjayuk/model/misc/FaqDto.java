package id.catchadeal.kerjayuk.model.misc;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class FaqDto extends EBaseDto {

    String title ;
    String subtitle ;
    String link;
    String image ;

    String imageLink ;


}
