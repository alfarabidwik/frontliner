package id.catchadeal.kerjayuk.model.business;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString(of = "statusOrder")
@NoArgsConstructor
@AllArgsConstructor
public class TaskPartnerStatus implements Serializable {

    int statusOrder ;
    String statusCode ;
    String statusLabel ;
    String statusInfo ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date created ;
}
