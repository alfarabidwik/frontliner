package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawalRequestDto extends EBaseDto {

    UserDto user ;

    BigDecimal amount = new BigDecimal(0);

    Set<PVWithdrawalRequestStatusDto> pvWithdrawalRequestStatuses = new HashSet<>();
    WithdrawalStatusDto withdrawalStatus;

    String imageUrl ;

}
