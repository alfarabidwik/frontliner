package id.catchadeal.kerjayuk.model.payment;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.region.Option;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = "id")
public class BankDto extends Option {

    String label ;
    String code;
    String image ;

    String imageLink ;


}
