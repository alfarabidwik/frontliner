package id.catchadeal.kerjayuk.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class UserDto extends PersonDto {

    public static final String[] VERIFICATION_STATUSES = new String[]{"VERIFIED","NOT_VERIFIED","NEED_VERIFIED","VERIFICATION_REJECTED"};
    public static final String VERIFIED = VERIFICATION_STATUSES[0];
    public static final String NOT_VERIFIED = VERIFICATION_STATUSES[1];
    public static final String NEED_VERIFIED = VERIFICATION_STATUSES[2];
    public static final String VERIFICATION_REJECTED = VERIFICATION_STATUSES[3];


    String agentCode ;

    String referralCode ;
    String organization ;
    String password ;
    @NotNull(message = "${agreeTermAndCondition.cannot.be.empty}")
    @NotNull(message = "${roleOfUser.cannot.be.empty}")
    String role ;
    boolean agreeTermAndCondition ;
    boolean firstLogin ;

    BigDecimal totalIncome = new BigDecimal(0);

    BigDecimal currentBalance = new BigDecimal(0);

    BigDecimal totalWithdrawal = new BigDecimal(0);

    Long onGoingTask = 0l ;
    Long totalTask = 0l ;
    Long totalCompletedTask  = 0l ;
    Long totalExpiredTask  = 0l;
    Long totalRejectedTask  = 0l;

    String userStatus ;

    String accountNumber ;
    String accountName ;
    BankDto bank ;

    String verificationStatus ;

    RegistrationDto registration;

    @JsonIgnoreProperties("user")
    Set<UserDeviceDto> userDevices = new HashSet<>();

    String authorization ;

    String registrationToken ;

    boolean isActive ;
    boolean isVerified ;


}
