package id.catchadeal.kerjayuk.model.business;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class FormApplicationDto extends EBaseDto {

    FormLaneApplicationDto formLaneApplication ;
    Long valueId ;
    Form form ;
    String value ;

    String imageUrl ;

}
