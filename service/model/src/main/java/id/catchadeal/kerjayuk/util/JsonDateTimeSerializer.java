package id.catchadeal.kerjayuk.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateTimeSerializer extends JsonSerializer<Date> {

    Logger logger = LoggerFactory.getLogger(JsonDateTimeSerializer.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);
    SimpleDateFormat androidRetrofitFormat = new SimpleDateFormat(DateAppConfig.RETROFIT_DATE_FORMAT);


    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        boolean valid = true ;

        try {
            if(date==null){
                return;
            }
            String formattedDate = dateFormat.format(date);
            jsonGenerator.writeString(formattedDate);
        }catch (Exception e){
            e.printStackTrace();
            valid = false ;
        }
        if(!valid){
            try {
                if(date==null){
                    return;
                }
                String formattedDate = androidRetrofitFormat.format(date);
                jsonGenerator.writeString(formattedDate);
            }catch (Exception e){
                e.printStackTrace();
                logger.error("TETEP ERROR", e);
                valid = false ;
            }
        }

    }
}
