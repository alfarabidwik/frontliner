package id.catchadeal.kerjayuk.model.payment;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = "id")
public class BankAccountDto extends EBaseDto {


    BankDto bank ;

    String customerName ;
    String accountCode ;
//    String mobileAppsCellNumber ;
//    String smsBankingSenderId ;
//    String smsBankingTemplate ;
//    String smsPrefixIndicator ;
//    String currencyFormat ;





}
