package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSUser extends WSResponse {

    //@ApiModelProperty(position = 3)
     UserDto data ;
}
