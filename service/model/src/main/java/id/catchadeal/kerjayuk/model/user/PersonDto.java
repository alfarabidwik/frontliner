package id.catchadeal.kerjayuk.model.user;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.util.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@NoArgsConstructor
public class PersonDto extends EBaseDto {

    String email ;
    String homePhoneNumber ;
    @NotNull(message = "${mobilePhone.cannot.be.empty}")
    @Pattern(regexp = Constant.NUMERIC_PATTERN, message = "please.input.a.valid.number")
    String mobilePhone ;
    String firstname ;
    String lastname ;
    String idCardCode ;
    String idCard ;
    String photo ;
    String selfieIdCard ;


    //@ApiModelProperty(example = DateAppConfig.API_DATE_EXAMPLE)
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    Date birthdate ;

    String gender ;
    String maritalStatus ;

    Double latitude ;
    Double longitude ;

    String address ;

    VillageDto village ;


    @Pattern(regexp = Constant.ALPHABET_SPACE_PATTERN, message = "please.input.a.valid.fullname")
    String fullname ;

    String idCardUrl ;
    String photoUrl ;
    String selfieIdCardUrl;


}
