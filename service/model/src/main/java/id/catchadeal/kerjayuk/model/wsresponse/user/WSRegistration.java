package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.RegistrationDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSRegistration extends WSResponse {

    //@ApiModelProperty(position = 3)
     RegistrationDto data ;
}
