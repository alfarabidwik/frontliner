package id.catchadeal.kerjayuk.model.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class PartnerDto extends EBaseDto {

    /**
     * Constant.PERSONAL / Constant.COMPANY
    * */
    @NotNull(message = "${partnerType.cannot.be.empty}")
    String partnerType ;
    String fullName ;
    String website ;
    String email ;
    String image ;
    String imageUrl ;

    long point ;

    Boolean selfPromotion ;

    @JsonIgnoreProperties("partner")
    CompanyDto company ;

    @JsonIgnoreProperties("partner")
    PersonalDto personal ;

    @JsonIgnoreProperties("partner")
    PartnerStatusDto partnerStatus ;


    String idCard ;
    String siupOrNwpw ;

    String siupOrNwpwUrl ;

    String idCardUrl ;

    long taskCount ;


}
