package id.catchadeal.kerjayuk.model.wsresponse.psychotest;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.psychotest.PersonalityCharacterDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

@Data
public class WSPersonalityCharacters extends WSResponse {

    //@ApiModelProperty(position = 3)
     Set<PersonalityCharacterDto> data ;
}
