package id.catchadeal.kerjayuk.model.role;

import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import lombok.Data;

import java.util.List;

@Data
public class RoleDto extends EBaseDto {

     String name ;

     List<RoleMenuDto> roleMenus ;

     List<AdminDto> admins ;

}
