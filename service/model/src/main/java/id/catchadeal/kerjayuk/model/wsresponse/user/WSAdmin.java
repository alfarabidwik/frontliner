package id.catchadeal.kerjayuk.model.wsresponse.user;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WSAdmin extends WSResponse {

    //@ApiModelProperty(position = 3)
     AdminDto data ;
}
