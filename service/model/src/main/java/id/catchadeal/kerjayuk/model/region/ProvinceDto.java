package id.catchadeal.kerjayuk.model.region;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProvinceDto extends Option {
    String countryCode ;

    String courierId ;

}
