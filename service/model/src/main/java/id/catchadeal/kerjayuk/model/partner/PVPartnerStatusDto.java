package id.catchadeal.kerjayuk.model.partner;

import id.catchadeal.kerjayuk.model.EBaseDto;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = "id", callSuper = true)
public class PVPartnerStatusDto extends EBaseDto {


    PartnerDto partner ;

    PartnerStatusDto partnerStatus ;

    String cause ;

    String note ;

}
