package id.catchadeal.kerjayuk.service.jurnal;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.jurnal.AdminActivityRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.util.*;


@Service
public class AdminActivityService extends BasicRepoService<AdminActivity> {

    @Autowired
    AdminActivityRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<AdminActivity> findAll() {
        return repository.findAll(new Sort(Sort.Direction.DESC, "updated"));
    }

    public DataPage<AdminActivity> findAll(String type, Date startDate, Date endDate, List<String> adminNames, Integer page, String sortir, Boolean ascending) {
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        StringBuilder sqlBuilder = new StringBuilder();
        String fetchedObject = "SELECT aa ";
        String fetchedObjectCount = "SELECT count(aa.id) ";
        String sql = " FROM AdminActivity aa WHERE aa.id IS NOT NULL ";
        if(type!=null){
            sqlBuilder.append(" AND aa.type = :type ");
            paramaterMap.put("type", type);
        }

        if(startDate!=null && endDate!=null){
            Calendar endDateCalendar = Calendar.getInstance();
            endDateCalendar.setTime(endDate);
            endDateCalendar.set(Calendar.DAY_OF_MONTH, endDateCalendar.get(Calendar.DAY_OF_MONTH)+1);

            sqlBuilder.append(" AND aa.created >= :startDate ");
            paramaterMap.put("startDate", startDate);

            sqlBuilder.append(" AND aa.created <= :endDate ");
            paramaterMap.put("endDate", endDateCalendar.getTime());
        }

        if(adminNames.size()>0){
            sqlBuilder.append(" AND aa.admin.firstname IN (:adminNames) ");
            paramaterMap.put("adminNames", adminNames);
        }

        sqlBuilder.append(" GROUP BY aa.id, aa.created ");

        String finalSql = fetchedObject+sql+sqlBuilder.toString()+" ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");
        String finalSqlCount = fetchedObjectCount+sql+sqlBuilder.toString()+" ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");


        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(finalSqlCount);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            e.printStackTrace();
            countResult = 0 ;
        }


        Query query = entityManager.createQuery(finalSql);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<AdminActivity> adminActivities = query.getResultList();

        DataPage dataPage = DataPage.builder(adminActivities, pageRow, countResult);
        return  dataPage ;

    }

    public List<AdminActivity> findAll(String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        return repository.findAll(sort);
    }





}
