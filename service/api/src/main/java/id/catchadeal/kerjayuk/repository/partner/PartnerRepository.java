package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;


public interface PartnerRepository extends JpaRepository<Partner, Long> {

    Partner findByWebsite(String website);

}
