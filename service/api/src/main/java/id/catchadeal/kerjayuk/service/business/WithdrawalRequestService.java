package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.PVWithdrawalRequestStatus;
import id.catchadeal.kerjayuk.entity.business.WithdrawalRequest;
import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.business.WithdrawalRequestRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.DateUtility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WithdrawalRequestService extends BasicRepoService<WithdrawalRequest> {

    @Autowired private WithdrawalRequestRepository repository ;
    @Autowired PVWithdrawalRequestStatusService pvWithdrawalRequestStatusService ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }


    public DataPage<WithdrawalRequest> withdrawalRequestDataPage
            (Long userId, Long withdrawalStatusId, String search, Date startDate, Date endDate, Integer page, Boolean ascending, String sortir){

        Map<String, Object> parameterMap = new HashMap<>();
        String selectObject = " SELECT wr ";
        String queryFrom = " FROM WithdrawalRequest wr LEFT JOIN FETCH wr.user u LEFT JOIN FETCH wr.withdrawalStatus ws ";
        String whereClause = " WHERE 1+1=2 ";
        String groupBy = " GROUP BY wr.id, wr.created, u.id, ws.id ";
        String orderBy = " ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");



        if(userId!=null){
            whereClause = whereClause+" AND u.id = :userId ";
            parameterMap.put("userId", userId);

        }
        if(withdrawalStatusId!=null){
            whereClause = whereClause+" AND ws.id = :withdrawalStatusId ";
            parameterMap.put("withdrawalStatusId", withdrawalStatusId);

        }
        if(StringUtils.isNotEmpty(search)){
            whereClause = whereClause+" AND (" +
                    "LOWER(CONCAT(COALESCE(u.firstname, ''), ' ', COALESCE(u.lastname, ''))) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.email, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.mobilePhone, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.agentCode, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    ") ";
            parameterMap.put("search", search);
        }
        if(startDate!=null && endDate!=null){
            whereClause = whereClause+" AND ( wr.created <= :startDate AND wr.endDate >= :endDate)";
            parameterMap.put("startDate", DateUtility.startOfDay(new Date()));
            parameterMap.put("endDate", DateUtility.endOfDay(new Date()));
        }

        String finalQuery = selectObject+queryFrom+whereClause+groupBy+orderBy;

        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(finalQuery);
            for(String key :parameterMap.keySet()) {
                queryTotal.setParameter(key, parameterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            e.printStackTrace();
            countResult = 0 ;
        }

        Query query = entityManager.createQuery(finalQuery);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);
        for(String key :parameterMap.keySet()) {
            query.setParameter(key, parameterMap.get(key));
        }

        List<WithdrawalRequest> withdrawalRequests = query.getResultList();
        return DataPage.builder(withdrawalRequests, pageRow, countResult);
    }

    public WithdrawalRequest request(User user, BigDecimal amount){
        if(amount.compareTo(configuration().getMinWithdrawal())<0){
            throw new AppException(Constant.FAILED_CODE, message("amount.is.less.than.minimum.withdrawal"));
        }
        if(user.getCurrentBalance().compareTo(amount)<0){
            throw new AppException(Constant.FAILED_CODE, message("balance.is.less.than.request.amount"));
        }
        WithdrawalRequest withdrawalRequest = new WithdrawalRequest();
        withdrawalRequest.setAmount(amount);
        withdrawalRequest.setWithdrawalStatus(WithdrawalStatus.WAITING);
        withdrawalRequest.setUser(user);
        withdrawalRequest = save(withdrawalRequest);

        PVWithdrawalRequestStatus pvWithdrawalRequestStatus = PVWithdrawalRequestStatus.build(withdrawalRequest, WithdrawalStatus.WAITING, null, null);
        pvWithdrawalRequestStatus = pvWithdrawalRequestStatusService.save(pvWithdrawalRequestStatus);
        return withdrawalRequest;
    }

    public Boolean checkIfExist(User user){
        String sql = "SELECT COUNT(wr.id) FROM j_withdrawal_request wr WHERE wr.user_id = :userId AND wr.withdrawal_status_id = :withdrawalStatusId ";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userId", user.getId());
        query.setParameter("withdrawalStatusId", WithdrawalStatus.WAITING.getId());
        BigInteger count = (BigInteger) query.getSingleResult();
        return count.intValue()>0;

    }



}
