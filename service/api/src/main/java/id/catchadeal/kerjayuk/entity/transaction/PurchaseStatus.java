package id.catchadeal.kerjayuk.entity.transaction;

import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_purchase_status")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseStatus extends NonIdEBase {

    public static final String WAITING_FOR_PAYMENT_ID = "1";
    public static final String WAITING_FOR_ACCEPT_PAYMENT_ID = "2";
    public static final String ACCEPTED_PAYMENT_ID = "3";
    public static final String REJECTED_PAYMENT_ID = "4";
    public static final String RETURN_PAYMENT_ID = "5";
    public static final String REJECTED_ID = "6";
    public static final String EXPIRED_ID = "7";
    public static final String CANCELED_ID = "8";


    public static final PurchaseStatus WAITING_FOR_PAYMENT = new PurchaseStatus(Long.valueOf(WAITING_FOR_PAYMENT_ID));
    public static final PurchaseStatus WAITING_FOR_ACCEPT_PAYMENT = new PurchaseStatus(Long.valueOf(WAITING_FOR_ACCEPT_PAYMENT_ID));
    public static final PurchaseStatus ACCEPTED_PAYMENT = new PurchaseStatus(Long.valueOf(ACCEPTED_PAYMENT_ID));
    public static final PurchaseStatus REJECTED_PAYMENT = new PurchaseStatus(Long.valueOf(REJECTED_PAYMENT_ID));
    public static final PurchaseStatus RETURN_PAYMENT = new PurchaseStatus(Long.valueOf(RETURN_PAYMENT_ID));
    public static final PurchaseStatus REJECTED = new PurchaseStatus(Long.valueOf(REJECTED_ID));
    public static final PurchaseStatus EXPIRED = new PurchaseStatus(Long.valueOf(EXPIRED_ID));
    public static final PurchaseStatus CANCELED = new PurchaseStatus(Long.valueOf(CANCELED_ID));

    public static final String DESCRIPTION = "<b>( STATUS CLASSIFICATION )</b> " +
            "\n1 = Waiting For Payment " +
            "\n2 = Waiting For Accept Payment " +
            "\n3 = Accepted Payment " +
            "\n4 = Rejected Payment " +
            "\n5 = Return Payment " +
            "\n6 = Rejected "+
            "\n7 = Expired "+
            "\n8 = Canceled ";


    String name ;
    String description ;


    public PurchaseStatus(Long id) {
        this.id = id;
    }
}
