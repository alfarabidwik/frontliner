package id.catchadeal.kerjayuk.util;

import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.security.JwtTokenProvider;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatusUtils {

    @Autowired JwtTokenProvider jwtTokenProvider ;
    @Autowired UserService userService ;
    @Autowired AdminService adminService ;

    public User validateEmployeeStatus(String authorization){
        User user = jwtTokenProvider.getUser(authorization);
//        if(!user.getEmployee().getEmployeeStatus().getId().equals(EmployeeStatus.GOOD)){
//            throw new AppException(Constant.FAILED_CODE, user.getEmployee().getPvEmployeeStatus().getCause());
//        }
        return user ;
    }

}
