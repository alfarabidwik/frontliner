package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.BusinessAddress;
import id.catchadeal.kerjayuk.repository.partner.BusinessAddressRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessAddressService extends BasicRepoService<BusinessAddress> {

    @Autowired private BusinessAddressRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public List<BusinessAddress> findByPartnerIdAndTitle(Long partnerId, String title){
        return repository.findByPartnerIdAndTitle(partnerId, title);
    }


}
