package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.business.PVWithdrawalRequestStatusService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${api}")
public class PVWithdrawalRequestStatusController extends BasicController {

    @Autowired
    PVWithdrawalRequestStatusService pvWithdrawalRequestStatusService ;

    @GetMapping(path = "/pvWithdrawalRequestStatuses")
    @AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSRegistrations.class)
    public WSResponse findAll(
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "created") String sortir) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, pvWithdrawalRequestStatusService.findAll(Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir)));
    }


}
