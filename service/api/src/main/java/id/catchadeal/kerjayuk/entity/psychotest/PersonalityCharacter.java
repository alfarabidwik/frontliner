package id.catchadeal.kerjayuk.entity.psychotest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="m_personality_character", indexes = {
        @Index(columnList = "type", name = "PersonalityCharacter_type_idx"),
        @Index(columnList = "created", name = "PersonalityCharacter_created_idx"),
        @Index(columnList = "updated", name = "PersonalityCharacter_updated_idx"),
        @Index(columnList = "active", name = "PersonalityCharacter_active_idx"),
})
//@Table(name="m_personality_character")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class PersonalityCharacter extends NonIdEBase {

    public static final String WEAKNESS = "weakness";
    public static final String STRENGTH = "strength";

    @Column(nullable = false)
    String title ;

    @Column(nullable = false)
    String type ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("personalityCharacters")
    @JoinColumn(name="personality_id", nullable = false)
    Personality personality ;


    @Override
    public void initTransient() {
        super.initTransient();

    }

}
