package id.catchadeal.kerjayuk.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.payment.Bank;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Table(name=User.TABLE, indexes = {
        @Index(columnList = "agentCode", name = "User_agentCode_idx", unique = true),
        @Index(columnList = "mobilePhone", name = "User_mobilePhone_idx", unique = false),
        @Index(columnList = "role", name = "User_role_idx", unique = false),
        @Index(columnList = "latitude,longitude", name = "User_latlong_idx"),
        @Index(columnList = "created", name = "User_created_idx"),
        @Index(columnList = "updated", name = "User_updated_idx"),
        @Index(columnList = "active", name = "User_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class User extends Person implements UserDetails {

    public static final String TABLE = "j_user";
    public static final String TAG = User.class.getSimpleName();

    public static final String[] USER_STATUSES = new String[]{"ACTIVE","BLOCKED"};
    public static final String ACTIVE = USER_STATUSES[0];
    public static final String BLOCKED = USER_STATUSES[1];

    @Column(nullable = false, unique = true, columnDefinition = "character varying(10) DEFAULT ''")
    String agentCode ;

    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT ''")
    String referralCode ;
    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT 'UMUM'")
    String organization ;
    String password ;
    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT 'USER'")
    String role ;
    @Column(nullable = false, columnDefinition = "boolean DEFAULT false")
    Boolean agreeTermAndCondition ;
    @Column(nullable = false, columnDefinition = "boolean DEFAULT false")
    Boolean firstLogin ;

    @Column(nullable = false, columnDefinition = "numeric(19,2) DEFAULT 0")
    BigDecimal totalIncome = new BigDecimal(0);

    @Column(nullable = false, columnDefinition = "numeric(19,2) DEFAULT 0")
    BigDecimal currentBalance = new BigDecimal(0);

    @Column(nullable = false, columnDefinition = "numeric(19,2) DEFAULT 0")
    BigDecimal totalWithdrawal = new BigDecimal(0);

    @Formula(value = "(SELECT COUNT(t.id) from j_task_application t WHERE t.user_id = id AND t.task_application_status_id = "+ TaskApplicationStatusDto.TAKEN_IDS +" )")
    Long onGoingTaskFormula ;
    @Column(nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    Long onGoingTask = 0l ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task_application t WHERE t.user_id = id)")
    Long totalTaskFormula ;
    @Column(nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    Long totalTask = 0l ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task_application t WHERE t.user_id = id AND t.task_application_status_id = "+ TaskApplicationStatusDto.ACCEPTED_IDS +" )")
    Long totalCompletedTaskFormula ;
    @Column(nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    Long totalCompletedTask  = 0l ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task_application t WHERE t.user_id = id AND t.task_application_status_id = "+ TaskApplicationStatusDto.REJECTED_IDS +" )")
    Long totalRejectedTaskFormula ;
    @Column(nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    Long totalRejectedTask  = 0l;

    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT 'ACTIVE'")
    String userStatus ;

    @Column(columnDefinition = "character varying(100) DEFAULT ''")
    String accountNumber ;

    @Column(columnDefinition = "character varying(100) DEFAULT ''")
    String accountName ;

    @OneToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("user")
    @JoinColumn(name="bank_id")
    Bank bank ;

    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT 'NOT_VERIFIED'")
    String verificationStatus ;

    @OneToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("user")
    @JoinColumn(name="registration_id", updatable = false)
    Registration registration;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SUBSELECT) @JsonIgnoreProperties("user")
    Set<UserDevice> userDevices = new HashSet<>();

    @Transient
    String authorization ;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return mobilePhone;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public void initAgentCode(MiscellaneousService miscellaneousService){
        if(StringUtils.isEmpty(agentCode)){
            String agentCode = miscellaneousService.randomAgentCode();
            BigInteger count = (BigInteger) BasicRepoService.staticEntityManager.createNativeQuery("SELECT COUNT(id) FROM "+TABLE+" WHERE agent_code = :agentCode").setParameter("agentCode", agentCode).getSingleResult();
            if(count.intValue()>0){
                initAgentCode(miscellaneousService);
            }else{
                setAgentCode(agentCode);
            }
        }
    }


//    public void checkFlagNeedVerified(MultipartFile photoMultipartFile, MultipartFile idCardMultipartFile, MultipartFile selfieIdCardMultipartFile){
//        if(photoMultipartFile!=null && idCardMultipartFile!=null && selfieIdCardMultipartFile!=null){
//            this.verificationStatus = NEED_VERIFIED;
//        }
//    }

    public void checkFlagNeedVerified(){
        if(StringUtils.isNotEmpty(photo) && StringUtils.isNotEmpty(idCard) && StringUtils.isNotEmpty(selfieIdCard)){
            this.verificationStatus = UserDto.NEED_VERIFIED;
        }
    }


    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isEmpty(organization)){
            organization = "UMUM";
        }
        if(StringUtils.isNotEmpty(organization)){
            organization = organization.toUpperCase();
        }
        if(StringUtils.isEmpty(role)){
            this.role = Constant.USER;
        }
        if(StringUtils.isEmpty(userStatus)){
            this.userStatus = ACTIVE;
        }
        if(StringUtils.isEmpty(verificationStatus)){
            this.verificationStatus = UserDto.NOT_VERIFIED;
        }
        if(agreeTermAndCondition==null){
            agreeTermAndCondition = Boolean.FALSE;
        }
        if(StringUtils.isEmpty(this.role)){
            this.role = Constant.USER;
        }
        if(totalTaskFormula!=null){
            totalTask = totalTaskFormula ;
        }
        if(totalCompletedTaskFormula!=null){
            totalCompletedTask = totalCompletedTaskFormula ;
        }
        if(totalRejectedTaskFormula!=null){
            totalRejectedTask = totalRejectedTaskFormula ;
        }
        if(onGoingTaskFormula!=null){
            onGoingTask = onGoingTaskFormula;
        }
        if(StringUtils.isNotEmpty(password) && password.startsWith("$2y$10$")){
            password = password.replace("$2y$10$", "$2a$10$");
        }
    }

    @Transient
    public boolean isUserActive(){
        return userStatus!=null && userStatus.equals(ACTIVE);
    }

    @Transient
    public boolean isVerified(){
        return verificationStatus!=null && verificationStatus.equals(UserDto.VERIFIED);
    }

}
