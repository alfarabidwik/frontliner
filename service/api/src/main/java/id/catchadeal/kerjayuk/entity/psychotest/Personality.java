package id.catchadeal.kerjayuk.entity.psychotest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name="m_personality", indexes = {
        @Index(columnList = "title", name = "Personality_title_idx"),
        @Index(columnList = "created", name = "Personality_created_idx"),
        @Index(columnList = "updated", name = "Personality_updated_idx"),
        @Index(columnList = "active", name = "Personality_active_idx"),
})
//@Table(name="m_personality")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Personality extends NonIdEBase {


    @Column(nullable = false,columnDefinition = "TEXT")
    String title ;

    @OneToMany(mappedBy = "personality", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("personality")
    Set<PersonalityCharacter> personalityCharacters = new HashSet<>();

}
