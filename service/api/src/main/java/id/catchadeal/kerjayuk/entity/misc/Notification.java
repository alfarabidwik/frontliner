package id.catchadeal.kerjayuk.entity.misc;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.org.apache.bcel.internal.generic.NEW;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.misc.NotificationData;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name="j_notification")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class Notification extends EBase {


    @OneToMany(mappedBy = "notification", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT) @JsonIgnoreProperties("notification")
    Set<NotificationUser> notificationUsers = new HashSet<>();

    @Column(nullable = false, columnDefinition = "TEXT")
    String title ;
    @Column(nullable = false, columnDefinition = "TEXT")
    String message ;
    @Column(columnDefinition = "TEXT DEFAULT '"+ NotificationDto.SPECIFIC+"'", nullable = false)
    String topic ;

    @Column(nullable = false, columnDefinition = "TEXT DEFAULT ''")
    String type ;

    @Type(type = "jsonb")
    @Column(name = "data", columnDefinition = "jsonb")
    NotificationData data ;


    String image ;
    @Transient
    String imageLink ;

    @ManyToOne(fetch= FetchType.LAZY)
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("notifications")
    @JoinColumn(name="admin_id")
    Admin admin ;

    @Override
    public void initTransient() {
        super.initTransient();

        if(image!=null){
            imageLink = Constant.prefixApi+image;
        }
        if(type==null){
            type = "";
        }
    }
}
