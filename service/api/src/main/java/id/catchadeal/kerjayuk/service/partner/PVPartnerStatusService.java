package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.PVPartnerStatus;
import id.catchadeal.kerjayuk.repository.partner.PVPartnerStatusRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PVPartnerStatusService extends BasicRepoService<PVPartnerStatus> {

    @Autowired private PVPartnerStatusRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public PVPartnerStatus findTopByPartnerIdAndPartnerStatusId(Long partnerId, Long partnerStatusId, Sort sort){
        return repository.findTopByPartnerIdAndPartnerStatusId(partnerId, partnerStatusId, sort);
    }


}
