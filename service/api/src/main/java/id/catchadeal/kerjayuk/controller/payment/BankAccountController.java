package id.catchadeal.kerjayuk.controller.payment;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.payment.BankAccount;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankAccountDto;
import id.catchadeal.kerjayuk.model.wsresponse.payment.WSBankAccount;
import id.catchadeal.kerjayuk.model.wsresponse.payment.WSBankAccounts;
import id.catchadeal.kerjayuk.service.payment.BankAccountService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class BankAccountController extends BasicController {

    @Autowired BankAccountService bankAccountService;

    @GetMapping(path = "/bankAccounts")
    @AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSBankAccounts.class)
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(defaultValue = "created") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){

        List<BankAccount> bankAccounts = bankAccountService.findAll(active, sortir, ascending);
        if(active!=null){
            List<BankAccount> bankAccountResults = new ArrayList<>();
            for (BankAccount bankAccount : bankAccounts) {
                if(bankAccount.getBank().getActive()==active){
                    bankAccountResults.add(bankAccount);
                }
            }
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bankAccountResults);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bankAccounts);
    }

    @PostMapping(path = "/bankAccount/save")
    @AdminRest
    @ApiOperation(value = "", response = WSBankAccount.class)
    public WSResponse save(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
            @RequestBody @Valid BankAccountDto bankAccountDto){
        BankAccount bankAccount = modelMapper.map(bankAccountDto, BankAccount.class);
        bankAccount = bankAccountService.save(bankAccount);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bankAccount);
    }

    @GetMapping(path = "/bankAccount/delete/{id}")
    @AdminRest
    public WSResponse delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable Long id){
        BankAccount bankAccount = bankAccountService.findById(id);
        bankAccountService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

}
