package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.WorkingDay;
import id.catchadeal.kerjayuk.repository.misc.WorkingDayRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkingDayService extends BasicRepoService<WorkingDay> {

    @Autowired private WorkingDayRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public void firstGenerate(){

    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }
}
