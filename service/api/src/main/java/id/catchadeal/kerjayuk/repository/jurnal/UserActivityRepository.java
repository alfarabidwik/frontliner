package id.catchadeal.kerjayuk.repository.jurnal;

import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserActivityRepository extends JpaRepository<UserActivity, Long> { }
