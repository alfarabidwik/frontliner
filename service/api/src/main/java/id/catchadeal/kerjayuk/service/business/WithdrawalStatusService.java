package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import id.catchadeal.kerjayuk.repository.business.WithdrawalStatusRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class WithdrawalStatusService extends BasicRepoService<WithdrawalStatus> {

    @Autowired private WithdrawalStatusRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }

}
