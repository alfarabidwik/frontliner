package id.catchadeal.kerjayuk.entity.jurnal;

import lombok.Data;

import java.io.Serializable;

@Data
public class DescriptionVariable implements Serializable {
    private Object value ;
    private String tableRef ;
    private String columnRef ;
    private int index ;
    private String variableName ;


    public static DescriptionVariable build(Object value, String tableRef, String columnRef, int index){
        DescriptionVariable descriptionVariable = new DescriptionVariable();
        descriptionVariable.setValue(value);
        descriptionVariable.setTableRef(tableRef);
        descriptionVariable.setColumnRef(columnRef);
        descriptionVariable.setIndex(index);
        return descriptionVariable ;
    }

}
