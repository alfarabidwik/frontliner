package id.catchadeal.kerjayuk.entity.payment;

import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.util.Constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Data
@Table(name="m_bank")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class Bank extends EBase {

    String label ;
    String name ;
    String code;
    String image ;

    @Transient
    String imageLink ;

    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isNotEmpty(image)){
            this.imageLink = Constant.prefixApi+ Constant.REST_BANK_IMAGE+"/"+this.getId()+"/"+this.image ;
        }
    }

}
