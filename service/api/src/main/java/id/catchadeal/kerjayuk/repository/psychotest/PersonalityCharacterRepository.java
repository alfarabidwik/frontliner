package id.catchadeal.kerjayuk.repository.psychotest;

import id.catchadeal.kerjayuk.entity.psychotest.PersonalityCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonalityCharacterRepository extends JpaRepository<PersonalityCharacter, Long> {

    List<PersonalityCharacter> findByPersonalityId(Long personalityId);
}
