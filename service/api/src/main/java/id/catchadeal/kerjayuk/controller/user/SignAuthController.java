package id.catchadeal.kerjayuk.controller.user;

import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.business.ReferralMember;
import id.catchadeal.kerjayuk.entity.business.TaskApplication;
import id.catchadeal.kerjayuk.entity.jurnal.DescriptionVariable;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.user.PersonDto;
import id.catchadeal.kerjayuk.model.user.UserDeviceDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSUser;
import id.catchadeal.kerjayuk.service.business.ReferralMemberService;
import id.catchadeal.kerjayuk.service.business.TaskApplicationService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.Utils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class SignAuthController extends BasicController {


    Logger logger = LoggerFactory.getLogger(SignAuthController.class);

    @Autowired TaskApplicationService taskApplicationService ;
    @Autowired ReferralMemberService referralMemberService ;

    @PostMapping(path = "/sign/authEmail")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse authEmail(
            @RequestParam String email,
            @RequestParam String password){
        Boolean exists = userService.existsByEmailAndActiveIsTrue(email);
        if(exists){
            User user = userService.findTopByEmailWithStatusActive(email);
            logger.debug("DB Password : {}, input password : {}", user.getPassword(), password);
            if(StringUtils.isEmpty(user.getPassword()) || !passwordEncoder.matches(password, user.getPassword())){
                throw new AppException(Constant.FAILED_CODE, message("invalid.email.or.password"));
            }
            if(!user.isUserActive()){
                throw new AppException(Constant.FAILED_CODE, message("your.account.is.has.been.blocked.please.contact.admin.support"));
            }

            user.setAuthorization(jwtTokenProvider.createToken(user.getMobilePhone(), user.getId(), Arrays.asList(user.getRole())));
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
        }else{
            throw new AppException(Constant.FAILED_CODE, message("invalid.email"));
        }
    }

    @PostMapping(path = "/sign/inEmail")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse signinEmail(
            @RequestParam String email,
            @RequestParam String password,
            @RequestParam String deviceId,
            @RequestParam(defaultValue = "") String fcmToken,
            @ApiParam(allowableValues = UserDevice.ANDROID+","+UserDevice.IOS, required = true) @RequestParam String platform){
        Boolean exists = userService.existsByEmailAndActiveIsTrue(email);
        if(exists){
            User user = userService.findTopByEmailWithStatusActive(email);
            if(StringUtils.isEmpty(user.getPassword()) || !passwordEncoder.matches(password, user.getPassword())){
                throw new AppException(Constant.FAILED_CODE, message("invalid.email.or.password"));
            }
            if(!user.isUserActive()){
                throw new AppException(Constant.FAILED_CODE, message("your.account.is.has.been.blocked.please.contact.admin.support"));
            }

            user.setAuthorization(jwtTokenProvider.createToken(user.getMobilePhone(), user.getId(), Arrays.asList(user.getRole())));
            UserDevice userDevice = userDeviceService.replaceByNewDevice(user.getId(), platform, fcmToken, deviceId);
            user.getUserDevices().add(userDevice);
            userActivityService.save(UserActivity.build(user, UserActivity.LOGIN, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), request.getParameterMap()));

            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
        }else{
            throw new AppException(Constant.FAILED_CODE, message("invalid.email"));
        }
    }

    @PostMapping(path = "/sign/applyMobilePhoneAndLogin")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse applyMobilePhoneAndLogin(
            @RequestParam String authorization,
            @RequestParam String mobilePhone,
            @RequestParam String deviceId,
            @RequestParam(defaultValue = "") String fcmToken,
            @ApiParam(allowableValues = UserDevice.ANDROID+","+UserDevice.IOS, required = true) @RequestParam String platform){
        User user = jwtTokenProvider.getUser(authorization);
        String previousMobilePhone = user.getMobilePhone();
        user.setMobilePhone(mobilePhone);
        user = userService.save(user);
        user.setAuthorization(jwtTokenProvider.createToken(user.getMobilePhone(), user.getId(), Arrays.asList(user.getRole())));
        UserDevice userDevice = userDeviceService.replaceByNewDevice(user.getId(), platform, fcmToken, deviceId);
        user.getUserDevices().add(userDevice);
        userActivityService.save(UserActivity.build(user, UserActivity.APPLY_MOBILE_PHONE_AND_LOGIN,
                Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), request.getParameterMap(), previousMobilePhone));

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }


    @PostMapping(path = "/sign/inOrUp")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse inOrUp(
            @RequestParam String mobilePhone,
            @RequestParam String deviceId,
            @RequestParam(defaultValue = "") String fcmToken,
            @ApiParam(allowableValues = UserDevice.ANDROID+","+UserDevice.IOS, required = true) @RequestParam String platform){

        Boolean exists = userService.existsByMobilePhoneAndActiveIsTrue(mobilePhone);
//        Map<String, Object> map = new HashMap();
        if(!exists){
            UserDto userDto = new UserDto();

            userDto.setMobilePhone(mobilePhone);
            userDeviceService.deleteDevice(deviceId);
            UserDeviceDto userDeviceDto = new UserDeviceDto(fcmToken, platform, deviceId);
            userDto.getUserDevices().add(userDeviceDto);

            String registrationToken = miscellaneousService.random30Code();
            userDto = appCacheManager.registrationToken(registrationToken, userDto);
            userDto.setRegistrationToken(registrationToken);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userDto);
        }else{
            User user = userService.findTopByMobilePhoneWithStatusActive(mobilePhone);
            if(!user.isUserActive()){
                throw new AppException(Constant.FAILED_CODE, message("your.account.is.has.been.blocked.please.contact.admin.support"));
            }

            Long userId = user.getId();

            user.setAuthorization(jwtTokenProvider.createToken(user.getMobilePhone(), userId, Arrays.asList(user.getRole())));
            UserDevice userDevice = userDeviceService.replaceByNewDevice(user.getId(), platform, fcmToken, deviceId);
            user.getUserDevices().add(userDevice);
            userActivityService.save(UserActivity.build(user, UserActivity.LOGIN, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), request.getParameterMap()));

            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
        }
    }

    @PostMapping(path = "/signout")
    @FrontendRest
    public WSResponse signout(@RequestHeader(Constant.AUTHORIZATION) String authorization, @RequestParam String deviceId){
        Long userId = jwtTokenProvider.getUserId(authorization);
        List<UserDevice> userDevices = userDeviceService.findByDeviceIdAndUserId(deviceId, userId, Sort.by(Sort.Direction.ASC, "created"));
        if(userDevices!=null){
            userDevices.forEach(userDevice -> {
                userDeviceService.delete(userDevice.getId());
            });
        }
        User user = jwtTokenProvider.getUser(authorization);
        userActivityService.save(UserActivity.build(user, UserActivity.LOGOUT, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), request.getParameterMap()));
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    @GetMapping(path = "/signup/tempRegistrationData")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse getRegistrationForm(@RequestParam String registrationToken){
        return WSResponse.instance(Constant.SUCCESS_CODE, appCacheManager.cacheValue("registrationToken",registrationToken, UserDto.class));
    }

    @PostMapping(path = "/signup/profile")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse profile(
            @RequestHeader(name = "registrationToken") String registrationToken, @RequestBody@Valid PersonDto profileData){
        UserDto userDto = appCacheManager.cacheValue("registrationToken", registrationToken, UserDto.class);
        try {
            userDto =  Utils.mergeInheritance(userDto, profileData, UserDto.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        userDto = appCacheManager.registrationToken(registrationToken, userDto);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userDto);
    }


    @PostMapping(path = "/signup/uploadImage")
    @FrontendRest
    public WSResponse uploadImage(
            @RequestHeader(name = "registrationToken") String registrationToken,
            @ApiParam(allowableValues = Constant.IMAGE_UPLOAD_TYPE, required = true) @RequestParam String type,
            @RequestParam MultipartFile multipartFile) throws Exception{
        UserDto userDto = appCacheManager.cacheValue("registrationToken", registrationToken, UserDto.class);
        String filename = String.valueOf(System.nanoTime());
        String image = fileStorage.storeFile(temporaryDir, null, filename, multipartFile);
        if(type.equalsIgnoreCase(Constant.PHOTO)){
            userDto.setPhoto(image);
            userDto.setPhotoUrl(Constant.prefixApi+ Constant.REST_TEMPORARY_IMAGE+"/"+image);
            userDto = appCacheManager.registrationToken(registrationToken, userDto);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userDto.getPhotoUrl());
        }

        if(type.equalsIgnoreCase(Constant.SELFIE_ID_CARD)){
            userDto.setSelfieIdCard(image);
            userDto.setSelfieIdCardUrl(Constant.prefixApi+ Constant.REST_TEMPORARY_IMAGE+"/"+image);
            userDto = appCacheManager.registrationToken(registrationToken, userDto);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, userDto.getSelfieIdCardUrl());
        }

        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }




    @PostMapping(path = "/sign/commitRegistration")
    @FrontendRest
    @Transactional
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse commitRegistration(@RequestParam(name = "registrationToken") String registrationToken,
                                         @RequestParam(defaultValue = "false") boolean agreeTermAndCondition,
                                         @RequestParam String email,
                                         @RequestParam String fullname,
                                         @RequestParam(required = false) String referralCode) throws Exception{
        if(!agreeTermAndCondition){
            throw new AppException(Constant.FAILED_CODE, message("you.must.agree.with.term.and.condition"));
        }
        UserDto userDto = appCacheManager.cacheValue("registrationToken", registrationToken, UserDto.class);
        if(userService.existsByMobilePhone(userDto.getMobilePhone())){
            throw new AppException(Constant.FAILED_CODE, message("mobile.phone.is.already.registered"));
        }
        if(userService.existsByEmail(email)){
            throw new AppException(Constant.FAILED_CODE, message("email.is.already.registered"));
        }

        userDto.setAgreeTermAndCondition(true);
        userDto.setEmail(email);
        userDto.setFullname(fullname);
        userDto.setReferralCode(referralCode);
        User user = buildUser(userDto, Registration.SELF_REGISTRATION);
        user = userService.findById(user.getId());
        user.setAuthorization(jwtTokenProvider.createToken(user.getMobilePhone(), user.getId(), Arrays.asList(user.getRole())));
        appCacheManager.evictCacheByNameAndKey("registrationToken", registrationToken);
        if(StringUtils.isNotEmpty(referralCode)){
            User referralUser = userService.findByAgentCodeAndUserStatusIsActive(referralCode);
            TaskApplication taskApplication = taskApplicationService.findByUserAndTaskTypeAndStatus(referralUser.getId(), TaskDto.REFERRAL_AGENT_TASK, TaskApplicationStatusDto.TAKEN_ID);
            if(taskApplication!=null){
                ReferralMember referralMember = new ReferralMember();
                referralMember.setUser(user);
                referralMember.setTaskApplication(taskApplication);
                referralMember = referralMemberService.save(referralMember);
                taskApplication.getReferralMembers().add(referralMember);
            }
        }
        commitUserActivity(user, UserActivity.REGISTER, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)));
        notificationService.sendSpesific(user, message("registration.success.title"), message("registration.success.description", user.getFirstname()), null, null, null);
        return WSResponse.instance(Constant.SUCCESS_CODE, user);
    }




}
