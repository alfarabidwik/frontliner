package id.catchadeal.kerjayuk.service.google;

import com.google.firebase.messaging.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
public class FCMService {


    private static final Logger logger = LoggerFactory.getLogger(FCMService.class);

    @Autowired protected ModelMapper mapper;

    @Autowired Gson gson ;


    public void sendMessage(Map<String, String> data, NotificationDto request)throws Exception {
        Message message = getPreconfiguredMessageWithData(data, request);
        logger.debug("Trying to send notification = {} ", gson.toJson(message));
        String response = sendAndGetResponse(message);
        logger.info("Sent notification with data. Topic: " + request.getTopic() + ", " + response);
    }

    public void sendMessageWithoutData(NotificationDto request)
            throws InterruptedException, ExecutionException {
        Message message = getPreconfiguredMessageWithoutData(request);
        logger.debug("Trying to send notification = {} ", gson.toJson(message));
        String response = sendAndGetResponse(message);
        logger.info("Sent notification without data. Topic: " + request.getTopic() + ", " + response);
    }

    public void sendMessageToToken(NotificationDto request, String token)
            throws InterruptedException, ExecutionException {
        Message message = getPreconfiguredMessageToToken(request, token);
        logger.debug("Trying to send notification = {} ", gson.toJson(message));
        String response = sendAndGetResponse(message);
        logger.info("Sent notification to token. Device token: " + token + ", " + response);
    }

    private String sendAndGetResponse(Message message) throws InterruptedException, ExecutionException {
        return FirebaseMessaging.getInstance().sendAsync(message).get();
    }

    private AndroidConfig getAndroidConfig(String topic) {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(2).toMillis()).setCollapseKey(topic)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder()
                        .setSound(NotificationParameter.SOUND.getValue())
                        .setColor(NotificationParameter.COLOR.getValue())
                        .setChannelId("1")
                        .setTag(topic).build())
                .build();
    }

    private ApnsConfig getApnsConfig(String topic) {
        return ApnsConfig.builder()
                .setAps(Aps.builder()
                        .setCategory(topic)
                        .setThreadId(topic)
                        .build()).build();
    }

    private Message getPreconfiguredMessageToToken(NotificationDto request, String token) {
        Message.Builder builder =  getPreconfiguredMessageBuilder(request).setToken(token);
        if(request.getData()!=null){
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(request.getData()), type);//mapper.map(request.getData(), type);
            builder.putAllData(map);

        }
        return builder.build();
    }

    private Message getPreconfiguredMessageWithoutData(NotificationDto request) {
        return getPreconfiguredMessageBuilder(request).setTopic(request.getTopic())
                .build();
    }

    private Message getPreconfiguredMessageWithData(Map<String, String> data, NotificationDto request) {
        return getPreconfiguredMessageBuilder(request).putAllData(data).setTopic(request.getTopic())
                .build();
    }

    private Message.Builder getPreconfiguredMessageBuilder(NotificationDto request) {
        AndroidConfig androidConfig = getAndroidConfig(request.getTopic());
        ApnsConfig apnsConfig = getApnsConfig(request.getTopic());
        Notification notification = new Notification(request.getTitle(), request.getMessage());
        return Message.builder().setApnsConfig(apnsConfig).setAndroidConfig(androidConfig).setNotification(notification);
    }


}