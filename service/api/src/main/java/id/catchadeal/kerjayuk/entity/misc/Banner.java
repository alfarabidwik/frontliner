package id.catchadeal.kerjayuk.entity.misc;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Data
@Table(name="j_banner")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class Banner extends EBase {


    private String image ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    private Date startDate ;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    private Date endDate ;
    private int sortir ;
    private String pagelink ;

    @Column(nullable = false)
    private String title ;

    @Column(columnDefinition = "TEXT")
    private String description ;

    @Transient
    private String imageLink ;


    @PreUpdate
    @PrePersist
    @Override
    public void prePersist() {
        super.prePersist();
        if(startDate!=null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 1);
            startDate = calendar.getTime();
        }
        if(endDate!=null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            endDate = calendar.getTime();
        }
    }

    @Override
    public void initTransient() {
        if(StringUtils.isNotEmpty(this.image)){
            this.imageLink =  Constant.prefixApi+Constant.REST_BANNER_IMAGE+"/"+this.getId()+"/"+this.image ;
        }
    }



}
