package id.catchadeal.kerjayuk.controller;

import com.google.gson.Gson;
import id.catchadeal.kerjayuk.component.AppCacheManager;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.jurnal.DescriptionVariable;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.exception.PartnerGatewayException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.security.JwtTokenProvider;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.partner.PartnerService;
import id.catchadeal.kerjayuk.service.partner.PersonalService;
import id.catchadeal.kerjayuk.service.misc.NotificationService;
import id.catchadeal.kerjayuk.service.jurnal.AdminActivityService;
import id.catchadeal.kerjayuk.service.jurnal.UserActivityService;
import id.catchadeal.kerjayuk.service.mail.EmailService;
import id.catchadeal.kerjayuk.service.misc.ConfigurationService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.user.*;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.RestUtils;
import id.catchadeal.kerjayuk.util.ValidationBuilder;
import io.jsonwebtoken.ExpiredJwtException;
import io.swagger.annotations.ApiParam;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.QueryException;
import org.hibernate.TransientPropertyValueException;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.persistence.MappedSuperclass;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import java.io.File;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.*;

@MappedSuperclass
public abstract class BasicController<E> {

    private static final Logger logger = LoggerFactory.getLogger(BasicController.class.getName());


    @Value("${temporary.dir}") protected String temporaryDir ;
    @Value("${user.photo.dir}") protected String userPhotoDir ;
    @Value("${user.id-card.dir}") protected String userIdCardDir ;
    @Value("${user.selfie.idcard.dir}") protected String userSelfieIdCardDir ;
    @Value("${withdrawal.receipt.dir}") protected String withdrawalReceiptImageDir ;
    @Value("${static.image.dir}") protected String staticImageDir ;
    @Value("${task.category.dir}") protected String taskCategoryDir ;
    @Value("${bank.image.dir}") protected String bankImageDir ;
    @Value("${faq.image.dir}") protected String faqImageDir ;
    @Value("${partner.image.dir}") protected String partnerImageDir ;
    @Value("${banner.photo.dir}") String bannerPhotoDir ;
    @Value("${task.banner.photo.dir}") protected String taskBannerPhotoDir ;
    @Value("${admin.photo.dir}") protected String adminPhotoDir ;


    @Value("${partner.id.card.dir}") protected String partnerIdCardDir ;
    @Value("${partner.siup.or.npwp.dir}") String partnerSiupOrNpwpDir ;

    @Value("${dr.strange.avenger.key}")
    protected String drStrangeAvengerKey ;
    @Value("${admin.domain}")
    protected String adminDomain ;




    @Autowired protected PasswordEncoder passwordEncoder ;
    @Autowired MessageSource messageSource ;
    @Autowired protected Gson gson ;

    @Autowired protected ModelMapper modelMapper ;
    @Autowired LocalValidatorFactoryBean validator ;
    @Autowired protected HttpServletRequest request ;
    @Autowired protected AppCacheManager appCacheManager ;


    @Autowired protected UserDeviceService userDeviceService ;
    @Autowired protected NotificationService notificationService ;
    @ApiParam protected ConfigurationService configurationService ;

    @Autowired protected UserService userService ;
    @Autowired protected PersonalService personalService ;
    @Autowired protected JwtTokenProvider jwtTokenProvider ;
    @Autowired protected FileStorage fileStorage;
    @Autowired protected MiscellaneousService miscellaneousService ;
    @Autowired protected RestUtils restUtils ;

    @Autowired protected AdminActivityService adminActivityService ;
    @Autowired protected UserActivityService userActivityService ;
    @Autowired protected AdminService adminService ;
    @Autowired protected RegistrationService registrationService ;
    @Autowired protected EmailService emailService ;
    @Autowired protected PartnerService partnerService ;



    @Value("${page.row}")
    protected int pageRow ;



    @PostConstruct
    public void postConstruct(){
    }


    public Type type(){
        return null ;
    }

    public Type typeList(){
        return null ;
    }

    public WSResponse findAll(Boolean ascending, String sortir){
        return WSResponse.instance(0, "");
    }

    public WSResponse findById(Long id){
        return WSResponse.instance(0, "");
    }

    public WSResponse save(E ob){
        return WSResponse.instance(0, "");
    }

    public WSResponse save(E ob, MultipartFile multipartFile) throws Exception{
        return WSResponse.instance(0, "");
    }
    public WSResponse save(String jsonObjectInString, MultipartFile multipartFile) throws Exception{
        return WSResponse.instance(0, "");
    }

    public WSResponse delete(Long id){
        return WSResponse.instance(0, "");
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public <T> void validate(T object){
        Set<ConstraintViolation<T>> constrains = validator.validate(object);
        List<String> strings = new ArrayList<>();
        for (ConstraintViolation<T> constrain : constrains) {
            String objectError = constrain.getMessage();
            strings.add(objectError);
        }
        if(strings.size()>0){
            throw new RuntimeException(ArrayUtils.toString(strings));
        }
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, IllegalArgumentException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        if(e.getMessage().contains(QueryException.class.getName())){
            return WSResponse.instance(Constant.FAILED_CODE, QueryException.class.getName());
        }
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getLocalizedMessage(), null);
    }

    @ExceptionHandler(value = ExpiredJwtException.class)
    public WSResponse expiredJwtExceptionHandler(HttpServletRequest req, ExpiredJwtException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instanceError(Constant.JWT_SESSION_EXPIRED_CODE, "This session has been expired, please resign in", null);
    }


    @ExceptionHandler(value = Exception.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getMessage(), e);
    }
    @ExceptionHandler(value = RuntimeException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, RuntimeException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getMessage(), e);
    }


    @ExceptionHandler(value = AppException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, AppException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return WSResponse.instanceError(e.getCode(), e.getMessage(), e.getMessageError());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, MethodArgumentNotValidException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return ValidationBuilder.validate(e.getBindingResult());
    }


    @ExceptionHandler(value = TransientPropertyValueException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, TransientPropertyValueException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        String detailMessage = "If you want to save an object which is has been stored in a database, please provide an id of this object, or you can save this object before calling this api";
        return WSResponse.instanceError(Constant.FAILED_CODE, e.getMessage(), detailMessage);
    }

    @ExceptionHandler(value = InvalidDataAccessApiUsageException.class)
    public WSResponse defaultErrorHandler(HttpServletRequest req, InvalidDataAccessApiUsageException e) throws Exception {
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        String detailMessage = "If you want to save an object which is has been stored in a database, please provide an id of this object, or you can save this object before calling this api";
        return WSResponse.instanceError(Constant.FAILED_CODE, detailMessage, e);
    }

    @ExceptionHandler(value = PartnerGatewayException.class)
    public ResponseEntity illegalStateException(HttpServletRequest req, PartnerGatewayException e){
        e.printStackTrace();
        logger.error(req.getRequestURI(), e);
        return ResponseEntity.status(e.getCode()).body(WSResponse.instance(e.getCode(), e.getMessage()));
    }



    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public WSResponse handleHttpMediaTypeNotAcceptableException() {
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }


    /**
     * Admin Activity
     * */

    public void commitAdminActivity(String authorization, String type, Object object){
        Admin admin = jwtTokenProvider.getAdmin(authorization);
        adminActivityService.save(AdminActivity.build(admin, type, Arrays.asList(DescriptionVariable.build(admin.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object));
    }

    public void commitAdminActivity(String authorization, String type, Object object, Object previousObject){
        Admin admin = jwtTokenProvider.getAdmin(authorization);
        adminActivityService.save(AdminActivity.build(admin, type, Arrays.asList(DescriptionVariable.build(admin.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object, previousObject));
    }

    public void commitAdminActivity(Admin admin, String type, Object object){
        adminActivityService.save(AdminActivity.build(admin, type, Arrays.asList(DescriptionVariable.build(admin.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object));
    }

    public void commitAdminActivity(Admin admin, String type, Object object, Object previousObject){
        adminActivityService.save(AdminActivity.build(admin, type, Arrays.asList(DescriptionVariable.build(admin.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object, previousObject));
    }

    /**
     * User Activity
     * */

    public void commitUserActivity(String authorization, String type, Object object){
        User user = jwtTokenProvider.getUser(authorization);
        userActivityService.save(UserActivity.build(user, type, Arrays.asList(DescriptionVariable.build(user.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object));
    }

    public void commitUserActivity(String authorization, String type, Object object, Object previousObject){
        User user = jwtTokenProvider.getUser(authorization);
        userActivityService.save(UserActivity.build(user, type, Arrays.asList(DescriptionVariable.build(user.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object, previousObject));
    }

    public void commitUserActivity(User user, String type, Object object){
        userActivityService.save(UserActivity.build(user, type, Arrays.asList(DescriptionVariable.build(user.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object));
    }

    public void commitUserActivity(User user, String type, Object object, Object previousObject){
        userActivityService.save(UserActivity.build(user, type, Arrays.asList(DescriptionVariable.build(user.getFullname(), Admin.TABLE, "firstname, lastname", 0)), object, previousObject));
    }

    public String generateAgentCode(){
        String agentCode = miscellaneousService.randomAgentCode();
        BigInteger count = (BigInteger) BasicRepoService.staticEntityManager.createNativeQuery("SELECT COUNT(id) FROM "+User.TABLE+" WHERE agent_code = :agentCode").setParameter("agentCode", agentCode).getSingleResult();
        if(count.intValue()>0){
            return generateAgentCode();
        }else{
            return agentCode ;
        }
    }



    protected User buildUser(UserDto userDto, String executor) throws Exception{
        userDto.setAgreeTermAndCondition(true);
        String userGson = gson.toJson(userDto);

        User user = gson.fromJson(userGson, User.class);
        user.initAgentCode(miscellaneousService);
        user = userService.save(user);

        Set<UserDevice> userDevices = user.getUserDevices();
        for (UserDevice userDevice : userDevices) {
            userDevice.setUser(user);
            userDeviceService.save(userDevice);
        }

        Registration registration = registrationService.register(user.getFirstname(), user.getLastname(), user.getMobilePhone(), user.getEmail(), user.getReferralCode(), user.getAgentCode(), executor);
        user.setRegistration(registration);
        user = userService.save(user);

        if(user.getPhoto()!=null){
            fileStorage.moveFile(temporaryDir+ File.separator+user.getPhoto(), userPhotoDir+File.separator+user.getId()+File.separator+user.getPhoto());
        }

        if(user.getSelfieIdCard()!=null){
            fileStorage.moveFile(temporaryDir+ File.separator+user.getSelfieIdCard(), userSelfieIdCardDir+File.separator+user.getId()+File.separator+user.getSelfieIdCard());
        }
        return user;

    }

    public File base64ToFile(String base64) throws AppException{
        try{
            if(StringUtils.isEmpty(base64)){
                return null ;
            }
            byte[] decodedBytes = Base64.getDecoder().decode(URLDecoder.decode(base64, "UTF-8"));//Base64.getDecoder().decode();
            File file = new File(String.valueOf(System.nanoTime())+".jpg");
            FileUtils.writeByteArrayToFile(file, decodedBytes);
            return file ;
        }catch (Exception e){
            e.printStackTrace();
            throw new AppException(Constant.FAILED_CODE, e.getMessage());
        }
    }



}
