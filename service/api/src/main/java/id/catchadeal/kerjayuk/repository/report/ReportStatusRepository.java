package id.catchadeal.kerjayuk.repository.report;

import id.catchadeal.kerjayuk.entity.report.ReportStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportStatusRepository extends JpaRepository<ReportStatus, Long> {



}
