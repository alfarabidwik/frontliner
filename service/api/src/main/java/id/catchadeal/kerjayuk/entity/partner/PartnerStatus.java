package id.catchadeal.kerjayuk.entity.partner;

import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_partner_status")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class PartnerStatus extends NonIdEBase {

    public static final String PUBLISHED_ID = "1";
    public static final String BANNED_ID = "2";
    public static final String SUBJECT_OF_VIOLATION_ID = "3";
    public static final String UNPUBLISH_ID = "4";

    public static final String PUBLISHED_LABEL = "PUBLISHED";
    public static final String BANNED_LABEL = "BANNED";
    public static final String SUBJECT_OF_VIOLATION_LABEL = "SUBJECT_OF_VIOLATION";
    public static final String UNPUBLISH_LABEL = "UNPUBLISH";


    public static final Long PUBLISHED = Long.valueOf(PUBLISHED_ID);
    public static final Long BANNED = Long.valueOf(BANNED_ID);
    public static final Long SUBJECT_OF_VIOLATION = Long.valueOf(SUBJECT_OF_VIOLATION_ID);
    public static final Long UNPUBLISH = Long.valueOf(UNPUBLISH_ID);

    public static final String DESCRIPTION =
            "1 - Published\n" +
                    "2 - Banned\n" +
                    "3 - Subject Of Violation \n" +
                    "4 - Unpublish ";


    String name ;
    @Column(columnDefinition = "TEXT")
    String description ;


}
