package id.catchadeal.kerjayuk.repository.misc;

import id.catchadeal.kerjayuk.entity.misc.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {


}
