package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import id.catchadeal.kerjayuk.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Table(name="m_withdrawal_request_status", indexes = {
        @Index(columnList = "created", name = "WitdrawalStatus_created_idx"),
        @Index(columnList = "updated", name = "WitdrawalStatus_updated_idx"),
        @Index(columnList = "active", name = "WitdrawalStatus_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawalStatus extends NonIdEBase {

    public static final String WAITING_ID = "1";
    public static final String ACCEPTED_ID = "2";
    public static final String ISSUED_ID = "3";
    public static final String REJECTED_ID = "4";
    public static final String CANCELED_ID = "5";


    public static final WithdrawalStatus WAITING = new WithdrawalStatus(Long.valueOf(WAITING_ID));
    public static final WithdrawalStatus ACCEPTED = new WithdrawalStatus(Long.valueOf(ACCEPTED_ID));
    public static final WithdrawalStatus ISSUED = new WithdrawalStatus(Long.valueOf(ISSUED_ID));
    public static final WithdrawalStatus REJECTED = new WithdrawalStatus(Long.valueOf(REJECTED_ID));
    public static final WithdrawalStatus CANCELED = new WithdrawalStatus(Long.valueOf(CANCELED_ID));

    public static final String DESCRIPTION = "1 = Waiting \n2 = Accepted \n3 = Issued \n4 = Rejected\n4 = Canceled\"";


    String name ;
    String description ;

    public WithdrawalStatus(Long id ){
        this.id = id ;
    }

    public static WithdrawalStatus build(Long id){
        return new WithdrawalStatus(id);
    }

}
