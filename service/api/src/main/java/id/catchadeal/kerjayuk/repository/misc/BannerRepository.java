package id.catchadeal.kerjayuk.repository.misc;

import id.catchadeal.kerjayuk.entity.misc.Banner;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Long> {


    List<Banner> findByActive(Boolean active, Sort sort);


}
