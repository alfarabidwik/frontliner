package id.catchadeal.kerjayuk.repository.psychotest;

import id.catchadeal.kerjayuk.entity.psychotest.Personality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalityRepository extends JpaRepository<Personality, Long> {

}
