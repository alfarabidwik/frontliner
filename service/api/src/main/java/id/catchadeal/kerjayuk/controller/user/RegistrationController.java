package id.catchadeal.kerjayuk.controller.user;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.user.RegistrationService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "${api}")
public class RegistrationController extends BasicController {

    @Autowired RegistrationService registrationService ;
    @Autowired MiscellaneousService miscellaneousService ;

    @GetMapping(path = "/registrations")
    @AdminRest
    @ApiOperation(value = "", response = WSRegistrations.class)
    public WSResponse findAll(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "") String keyword,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "created") String sortir) {
        Page<Registration> registrationPage = registrationService.findByKeywordLike(keyword, PageRequest.of(page, pageRow, Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir)));
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, registrationPage.getContent())
                .setTotalPage(registrationPage.getTotalPages())
                .setTotalElement(registrationPage.getTotalElements())
                .setPageElement(pageRow);
    }


}
