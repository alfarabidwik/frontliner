package id.catchadeal.kerjayuk.repository.user;

import id.catchadeal.kerjayuk.entity.user.Admin;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Long> {

    Optional<Admin> findByEmail(String email);

    Boolean existsByEmail(String email);

    @Query("SELECT a FROM Admin a WHERE a.firstname LIKE %?1% OR a.lastname LIKE %?1% OR a.role.name LIKE %?1%")
    Page<Admin> findByNameOrRoleNameContaining(String name, Pageable pageable);

    @Query("SELECT a.firstname FROM Admin a")
    List<String> firstnames(Sort sort);

}
