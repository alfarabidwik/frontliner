package id.catchadeal.kerjayuk.repository.report;

import id.catchadeal.kerjayuk.entity.report.ReportCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportCategoryRepository extends JpaRepository<ReportCategory, Long> {



}
