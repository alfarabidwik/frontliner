package id.catchadeal.kerjayuk.repository.region;

import id.catchadeal.kerjayuk.entity.region.Province;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProvinceRepository extends JpaRepository<Province, Long> {

    List<Province> findByActive(Boolean active, Sort sort);
    Page<Province> findByActive(Boolean active, Pageable pageable);

}
