package id.catchadeal.kerjayuk.entity.region;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "m_village")
@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Village extends EBase implements Serializable {
    String name ;
    String postalCode ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="district_id")
    District district ;

//    String courierId ;

    @Override
    public void initTransient() {
        super.initTransient();

    }


}
