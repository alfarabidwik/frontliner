package id.catchadeal.kerjayuk.service.region;

import id.catchadeal.kerjayuk.entity.region.City;
import id.catchadeal.kerjayuk.repository.region.CityRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CityService extends BasicRepoService<City> {

    @Autowired CityRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Cacheable(cacheNames = "Cities1")
    @Override
    public List<City> findAll() {
        return repository.findAll();
    }

    @Cacheable(cacheNames = "Cities2", key = "{#active, #sort}")
    public List<City> findAll(Boolean active, Sort sort) {
        if(active!=null){
            return repository.findByActive(active, sort);
        }else{
            return repository.findAll(sort);
        }
    }


    @Cacheable(cacheNames = "Cities3", key = "{#active, #page, #sortir, #ascending}")
    public Page<City> findAll3(Boolean active, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActive(active, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findAll(PageRequest.of(page, pageRow, sort));
        }
    }


    @Cacheable(cacheNames = "City", key = "#id")
//    @Override
    public City findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public City save(City village) {
        return super.save(village);
    }

    @Cacheable(cacheNames = "Cities4", key = "{#active, #provinceId, #sort}")
    public List<City> findByProvinceId(Boolean active, Long provinceId, Sort sort){
        if(active!=null){
            return repository.findByActiveAndProvinceId(active, provinceId, sort);
        }else{
            return repository.findByProvinceId(provinceId, sort);
        }
    }

    @Cacheable(cacheNames = "Cities5", key = "{#active, #provinceId, #page, #sortir, #ascending}")
    public Page<City> findAll4(Boolean active, Long provinceId, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActiveAndProvinceId(active, provinceId, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findByProvinceId(provinceId, PageRequest.of(page, pageRow, sort));
        }
    }



}
