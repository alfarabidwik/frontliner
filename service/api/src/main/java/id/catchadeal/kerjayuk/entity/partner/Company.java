package id.catchadeal.kerjayuk.entity.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.JsonDateDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name="j_company", indexes = {
        @Index(columnList = "address", name = "company_address_idx"),
        @Index(columnList = "latitude,longitude", name = "company_latlong_idx"),
        @Index(columnList = "created", name = "Company_created_idx"),
        @Index(columnList = "updated", name = "Company_updated_idx"),
        @Index(columnList = "active", name = "Company_active_idx"),
})
//@Table(name = "j_company")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Company extends EBase {


    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date buildDate ;
    String officePhoneNumber ;
    String officeFax ;

    @Column(columnDefinition = "TEXT")
    String companyDescription ;

    Double latitude ;
    Double longitude ;

    String businessField ;

    @Column(columnDefinition = "TEXT")
    String address ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("companies")
    @JoinColumn(name="village_id")
    Village village ;

    @OneToOne(fetch= FetchType.LAZY, mappedBy = "company")@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("company")
    Partner partner;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("companies")
    @JoinColumn(name="corporate_type_id")
    CorporateType corporateType;



    @Override
    public void initTransient() {
        super.initTransient();
        mergeActiveStatus(partner);

    }


}
