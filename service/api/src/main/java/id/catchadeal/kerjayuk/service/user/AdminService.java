package id.catchadeal.kerjayuk.service.user;

import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.admindashboard.AdminDashboard;
import id.catchadeal.kerjayuk.model.admindashboard.BarInfo;
import id.catchadeal.kerjayuk.model.admindashboard.DashboardCategoryAnalytic;
import id.catchadeal.kerjayuk.model.admindashboard.DataSet;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import id.catchadeal.kerjayuk.repository.user.AdminRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.DateUtility;
import id.catchadeal.kerjayuk.util.Utils;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

@Service
public class AdminService extends BasicRepoService<Admin> {

    @Autowired private AdminRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public Admin findByEmail(String email){
        return repository.findByEmail(email).orElseThrow(() -> new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "email")));
    }

    public Admin signin(String email){
        return repository.findByEmail(email).orElseThrow(() -> new AppException(Constant.FAILED_CODE, "Invalid email or password"));
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }


    @Override
    public List<Admin> findAll() {
        return repository.findAll();
    }

    public Page<Admin> findByNameOrRoleNameContaining(String search, int page, Boolean ascending, String sortir) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        return repository.findByNameOrRoleNameContaining(search, PageRequest.of(page, pageRow, sort));
    }

    @Override
    public Admin save(Admin admin) {
        return super.save(admin);
    }

    public Admin save(AdminDto adminDto) {
        Admin admin = mapper.map(adminDto, Admin.class);
        return super.save(admin);
    }

    public Boolean existsById(Long id){
        return repository.existsById(id);
    }

    public Boolean existsByEmail(String email){
        return repository.existsByEmail(email);
    }

    public List<String> firstnames(String sortir, Boolean ascending){
        return repository.firstnames(new Sort(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir));
    }

    public AdminDashboard adminDashboard(){
        Date currentDate = DateUtility.currentDate();
        Date firstDayOfCurrentMonth = DateUtility.firstDayOfcurrentMonth();

        Date lastDayOfPreviousMonth = DateUtility.lastDayOfPreviousMonth();
        Date firstDayOfPreviousMonth = DateUtility.firstDayOfPreviousMonth();


        String customerQuery = "SELECT " +
                "(SELECT COUNT(u.id) FROM j_user u) as totalUser, " +
                "(SELECT COUNT(u.id) FROM j_user u WHERE u.created >= :firstDayOfCurrentMonth AND u.created <= :currentDate ) as thisMonthUser, " +
                "(SELECT COUNT(u.id) FROM j_user u WHERE u.created >= :firstDayOfPreviousMonth AND u.created <= :lastDayOfPreviousMonth ) as previousMonthUser, " +

                "(SELECT COUNT(jt.id) FROM j_task jt) as totalTask, " +
                "(SELECT COUNT(jt.id) FROM j_task jt WHERE jt.created >= :firstDayOfCurrentMonth AND jt.created <= :currentDate ) as thisMonthTask, " +
                "(SELECT COUNT(jt.id) FROM j_task jt WHERE jt.created >= :firstDayOfPreviousMonth AND jt.created <= :lastDayOfPreviousMonth ) as previousMonthTask, "+

                "(SELECT COUNT(jta.id) FROM j_task_application jta WHERE jta.task_application_status_id = :taskApplicationStatusId) as totalTaskDone , " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta WHERE jta.task_application_status_id = :taskApplicationStatusId AND jta.created >= :firstDayOfCurrentMonth AND jta.created <= :currentDate ) as thisMonthTaskDone, " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta WHERE jta.task_application_status_id = :taskApplicationStatusId AND jta.created >= :firstDayOfPreviousMonth AND jta.created <= :lastDayOfPreviousMonth ) as previousMonthTaskDone, "+

                "(SELECT SUM(jwr.amount) FROM j_withdrawal_request jwr WHERE jwr.withdrawal_status_id = :withdrawalStatusId) as totalPayment , " +
                "(SELECT SUM(jwr.amount) FROM j_withdrawal_request jwr WHERE jwr.withdrawal_status_id = :withdrawalStatusId AND jwr.updated >= :firstDayOfCurrentMonth AND jwr.updated <= :currentDate ) as thisMonthPayment, " +
                "(SELECT SUM(jwr.amount) FROM j_withdrawal_request jwr WHERE jwr.withdrawal_status_id = :withdrawalStatusId AND jwr.updated >= :firstDayOfPreviousMonth AND jwr.updated <= :lastDayOfPreviousMonth ) as previousMonthPayment ";

        Query query = entityManager.createNativeQuery(customerQuery).unwrap( org.hibernate.query.NativeQuery.class ).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setParameter("firstDayOfCurrentMonth", firstDayOfCurrentMonth);
        query.setParameter("currentDate", currentDate);
        query.setParameter("firstDayOfPreviousMonth", firstDayOfPreviousMonth);
        query.setParameter("lastDayOfPreviousMonth", lastDayOfPreviousMonth);
        query.setParameter("taskApplicationStatusId", TaskApplicationStatus.ACCEPTED.getId());
        query.setParameter("withdrawalStatusId", WithdrawalStatus.ISSUED.getId());

        Map map = (Map) query.getSingleResult();
        AdminDashboard adminDashboard = new AdminDashboard();
        adminDashboard.setTotalUser(Utils.cast(BigInteger.class, map.get("totalUser".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setThisMonthUser(Utils.cast(BigInteger.class, map.get("thisMonthUser".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setPreviousMonthUser(Utils.cast(BigInteger.class, map.get("previousMonthUser".toLowerCase()), BigInteger.ZERO).longValue());

        adminDashboard.setTotalTask(Utils.cast(BigInteger.class, map.get("totalTask".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setThisMonthTask(Utils.cast(BigInteger.class, map.get("thisMonthTask".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setPreviousMonthTask(Utils.cast(BigInteger.class, map.get("previousMonthTask".toLowerCase()), BigInteger.ZERO).longValue());

        adminDashboard.setTotalTaskDone(Utils.cast(BigInteger.class, map.get("totalTaskDone".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setThisMonthTaskDone(Utils.cast(BigInteger.class, map.get("thisMonthTaskDone".toLowerCase()), BigInteger.ZERO).longValue());
        adminDashboard.setPreviousMonthTaskDone(Utils.cast(BigInteger.class, map.get("previousMonthTaskDone".toLowerCase()), BigInteger.ZERO).longValue());

        adminDashboard.setTotalPayment(Utils.cast(BigDecimal.class, map.get("totalPayment".toLowerCase()), BigDecimal.ZERO));
        adminDashboard.setThisMonthPayment(Utils.cast(BigDecimal.class, map.get("thisMonthPayment".toLowerCase()), BigDecimal.ZERO));
        adminDashboard.setPreviousMonthPayment(Utils.cast(BigDecimal.class, map.get("previousMonthPayment".toLowerCase()), BigDecimal.ZERO));

        return adminDashboard ;
    }


    public DashboardCategoryAnalytic categoryAnalytic(Date startDate, Date endDate){
        DashboardCategoryAnalytic dashboardCategoryAnalytic = new DashboardCategoryAnalytic();
        String sql = "";
        String table = "";
        String productMatcher = "";

//        sql = "SELECT c.id, c.name, " +
//                "(SELECT coalesce(sum(t.total_item_price), 0) FROM j_transaction t " +
//                "LEFT JOIN j_transaction_detail jtd ON jtd.transaction_id = t.id " +
//                "LEFT JOIN j_inventory ji ON jtd.inventory_id = ji.id " +
//                "LEFT JOIN m_product mp ON ji.product_id = mp.id " +
//                "WHERE t.transaction_status_id = "+TransactionStatus._DONE+" AND "+productMatcher+" = c.id AND t.created >= :startDate AND t.created <= :endDate) as transaction_done, " +
//                "(SELECT coalesce(sum(t.total_item_price), 0) FROM j_transaction t " +
//                "LEFT JOIN j_transaction_detail jtd ON jtd.transaction_id = t.id " +
//                "LEFT JOIN j_inventory ji ON jtd.inventory_id = ji.id " +
//                "LEFT JOIN m_product mp ON ji.product_id = mp.id " +
//                "WHERE t.transaction_status_id = "+TransactionStatus._REJECTED+" AND "+productMatcher+" = c.id AND t.created >= :startDate AND t.created <= :endDate) as transaction_rejected " +
//                "FROM "+table+" c ";

        sql = "SELECT " +
                "tc.id as id , tc.name as name, " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta LEFT JOIN j_task jt ON jta.task_id = jt.id " +
                "WHERE jta.task_application_status_id = "+TaskApplicationStatus.SUBMITTED.getId().toString()+" AND jta.created >= :startDate AND jta.created <= :endDate AND jt.task_category_id = tc.id ) as taskSubmitted, " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta LEFT JOIN j_task jt ON jta.task_id = jt.id  " +
                "WHERE jta.task_application_status_id = "+TaskApplicationStatus.ACCEPTED.getId().toString()+" AND jta.created >= :startDate AND jta.created <= :endDate AND jt.task_category_id = tc.id ) as taskAccepted, " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta LEFT JOIN j_task jt ON jta.task_id = jt.id  " +
                "WHERE jta.task_application_status_id = "+TaskApplicationStatus.REJECTED.getId().toString()+" AND jta.created >= :startDate AND jta.created <= :endDate AND jt.task_category_id = tc.id ) as taskRejected " +
                "FROM m_task_category tc ";

        List<Map> maps = new ArrayList<>();
        Query query = entityManager.createNativeQuery(sql).unwrap( org.hibernate.query.NativeQuery.class ).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);
        endDateCalendar.set(Calendar.DAY_OF_MONTH, endDateCalendar.get(Calendar.DAY_OF_MONTH)+1);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDateCalendar.getTime());

        try {
            maps = query.getResultList();
            List<BarInfo> barInfos = new ArrayList<>();
            List<Long> taskSubmittedValues = new ArrayList<>();
            List<Long> taskAccepted = new ArrayList<>();
            List<Long> taskRejected = new ArrayList<>();
            maps.forEach(map -> {
                BarInfo barInfo = new BarInfo();
                barInfo.setId(Utils.cast(Long.class, map.get("id")));
                barInfo.setLabel(Utils.cast(String.class, map.get("name")));
                barInfos.add(barInfo);
                taskSubmittedValues.add(Utils.cast(Long.class, map.get("taskSubmitted".toLowerCase())));
                taskAccepted.add(Utils.cast(Long.class, map.get("taskAccepted".toLowerCase())));
                taskRejected.add(Utils.cast(Long.class, map.get("taskRejected".toLowerCase())));
            });
            dashboardCategoryAnalytic.setBarInfos(barInfos);

            DataSet taskSubmittedDataSet = new DataSet();
            taskSubmittedDataSet.setLabel(message("Submitted"));
            taskSubmittedDataSet.setStack(message("Submitted"));
            taskSubmittedDataSet.setBackgroundColor(DataSet.YELLOW);
            taskSubmittedDataSet.setData(taskSubmittedValues);
            dashboardCategoryAnalytic.getDatasets().add(taskSubmittedDataSet);

            DataSet taskAcceptedDataSet = new DataSet();
            taskAcceptedDataSet.setLabel(message("Accepted"));
            taskAcceptedDataSet.setStack(message("Accepted"));
            taskAcceptedDataSet.setBackgroundColor(DataSet.BLUE);
            taskAcceptedDataSet.setData(taskAccepted);
            dashboardCategoryAnalytic.getDatasets().add(taskAcceptedDataSet);

            DataSet taskRejectedDataSet = new DataSet();
            taskRejectedDataSet.setLabel(message("Rejected"));
            taskRejectedDataSet.setStack(message("Rejected"));
            taskRejectedDataSet.setBackgroundColor(DataSet.RED);
            taskRejectedDataSet.setData(taskRejected);
            dashboardCategoryAnalytic.getDatasets().add(taskRejectedDataSet);


        }catch (Exception e){
            e.printStackTrace();
        }

        String sql2 = "SELECT SUM(u.total_income) as allUserIncomeAmount, SUM(u.total_withdrawal) as allUserWithdrawAmount FROM j_user u ";

//        String sql2 = "SELECT coalesce(sum(t.total_pay), 0) as total_amount, " +
//                "coalesce(sum(t.total_item_price), 0) as item_amount, " +
//                "coalesce(sum(t.bubble_wrap_fee), 0) as bubble_wrap_amount, " +
//                "coalesce(sum(t.courier_cost), 0) as courier_amount, " +
//                "coalesce(sum(t.unique_price), 0) as unique_amount From j_transaction t WHERE t.transaction_status_id = "+TransactionStatus._DONE+" AND t.created >= :startDate AND t.created <= :endDate";
//
        Query query2 = entityManager.createNativeQuery(sql2).unwrap( org.hibernate.query.NativeQuery.class )
                .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        Map map = new HashMap();
        try{
            map = (Map) query2.getSingleResult();
            dashboardCategoryAnalytic.setAllUserIncomeAmount(Utils.cast(BigDecimal.class, map.get("allUserIncomeAmount".toLowerCase())));
            dashboardCategoryAnalytic.setAllUserWithdrawAmount(Utils.cast(BigDecimal.class, map.get("allUserWithdrawAmount".toLowerCase())));
        }catch (Exception e){
            e.printStackTrace();
        }
        return dashboardCategoryAnalytic ;

    }



}
