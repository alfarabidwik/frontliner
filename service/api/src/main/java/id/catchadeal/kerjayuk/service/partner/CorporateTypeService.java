package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.CorporateType;
import id.catchadeal.kerjayuk.repository.partner.CorporateTypeRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CorporateTypeService extends BasicRepoService<CorporateType> {

    @Autowired private CorporateTypeRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }


    public Boolean existsByName(String name){
        return repository.existsByName(name);
    }

}
