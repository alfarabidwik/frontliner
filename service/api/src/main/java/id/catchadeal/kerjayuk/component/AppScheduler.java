package id.catchadeal.kerjayuk.component;

import id.catchadeal.kerjayuk.entity.email.SendEmail;
import id.catchadeal.kerjayuk.service.business.TaskService;
import id.catchadeal.kerjayuk.service.misc.NotificationService;
import id.catchadeal.kerjayuk.service.mail.SendEmailService;
import id.catchadeal.kerjayuk.service.misc.ConfigurationService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@PropertySource("classpath:messages/messages.properties")
public class AppScheduler extends BasicComponent{

//      * "0 0 * * * *" = the top of every hour of every day.
//      * "*/10 * * * * *" = every ten seconds.
//      * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
//      * "0 0 8,10 * * *" = 8 and 10 o'clock of every day.
//      * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
//      * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
//      * "0 0 0 25 12 ?" = every Christmas Day at midnight

    /*
    * <second> <minute> <hour> <day-of-month> <month> <day-of-week> <year> <command>
    * */

    Logger logger = LoggerFactory.getLogger(AppScheduler.class);

    @Value("${daily.reward}") protected String dailyReward ;
    @Value("${monthly.reward}") protected String monthlyReward ;

    @Autowired UserService userService ;
    @Autowired ConfigurationService configurationService ;
    @Autowired NotificationService notificationService ;
    @Autowired
    TaskService taskService;
    @Autowired SendEmailService sendEmailService ;


    @Scheduled(fixedRate = 1000*60*10)
    public void _10minutesEmailScheduler() {
        Boolean justScheduledEmailExist = sendEmailService.justScheduled(90);

        if (justScheduledEmailExist) {
            List<SendEmail> sendEmails = sendEmailService.scheduledEmails(120);
            for (int i = 0; i < sendEmails.size(); i++) {
                SendEmail sendEmail = sendEmails.get(i);
                try {
                    sendEmail = sendEmailService.sendScheduled(sendEmail);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }




}
