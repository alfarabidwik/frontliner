package id.catchadeal.kerjayuk.security;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.service.auth.UserAuthService;
import id.catchadeal.kerjayuk.util.Constant;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.*;

@Service
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${security.jwt.token.secret-key}")
    private String secretKey ;

    Gson gson = new Gson();


//    @Value("${security.jwt.token.expire-length:3600000}")

    private long validityInMilliseconds = 1000*60*60*24*365;//1 Tahun
//    private long validityInMilliseconds = 1000*5;//5 Detik

    @Autowired
    private UserAuthService userAuthService;

    public String createToken(String jwtSubject, Long userId, List<String> roles) {

        Claims claims = Jwts.claims().setSubject(jwtSubject);
        claims.put("roles", roles);
        claims.put("userId", String.valueOf(userId));

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)//
            .signWith(SignatureAlgorithm.HS256, secretKey)//
            .compact();
    }

    public Authentication getAdminAuthentication(String token) {
        UserDetails userDetails = this.userAuthService.loadAdminByEmail(getLoginId(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public Authentication getUserAuthentication(String token) {
        UserDetails userDetails = this.userAuthService.loadUserBySubjectKey(getLoginId(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public id.catchadeal.kerjayuk.entity.user.User getUser(String token){
        Authentication authentication = getUserAuthentication(token);
        User user = (User) authentication.getPrincipal();
        return user ;
    }

    public Admin getAdmin(String token){
        Authentication authentication = getAdminAuthentication(token);
        Admin admin = (Admin) authentication.getPrincipal();
        return admin ;
    }

    public String getLoginId(String authorization) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authorization).getBody().getSubject();
    }

    public List<String> getRoles(String token) {
        if(token==null){
            return null;
        }
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            List<String> roles = (List<String>) map.get("roles");
            return roles;
        }catch (Exception e){
            e.printStackTrace();
            return Arrays.asList(Constant.USER, Constant.ADMIN);
        }
    }

    public Long getUserId(String token) {
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);//.getBody();//.get("roles", ArrayList.class);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            if(map.get("userId")!=null){
                String userIdString = (String) map.get("userId");
                return Long.valueOf(userIdString);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HttpHeaders.AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return bearerToken;
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())) {
                return false;
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false ;
        }
    }

    public boolean isAdmin(String token){
        try{
            Jwt<JwsHeader, Claims> jwt =  Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            Claims claims = jwt.getBody();
            logger.debug(new Gson().toJson(claims));
            Type type = new TypeToken<Map>(){}.getType();
            Map map = gson.fromJson(gson.toJson(claims), type);
            List<String> roles = (List<String>) map.get("roles");
            return (roles.get(0).equals(Constant.ADMIN));
        }catch (Exception e){
            e.printStackTrace();
        }
        return false ;

    }

}