package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.payment.Bank;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="pv_withdrawal_request_status", indexes = {
        @Index(columnList = "created", name = "PVWithdrawalRequestStatus_created_idx"),
        @Index(columnList = "updated", name = "PVWithdrawalRequestStatus_updated_idx"),
        @Index(columnList = "active", name = "PVWithdrawalRequestStatus_active_idx"),
})
//@Table(name = "pv_task_application_status")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class PVWithdrawalRequestStatus extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties({"pvWithdrawalRequestStatuses", "withdrawalStatus"})
    @JoinColumn(name="withdrawal_request_id", updatable = false, nullable = false)
    WithdrawalRequest withdrawalRequest;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties({"pvWithdrawalRequestStatuses", "withdrawalStatus"})
    @JoinColumn(name="withdrawal_status_id", nullable = false)
    WithdrawalStatus withdrawalStatus;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("pvWithdrawalRequestStatuses")
    @JoinColumn(name="admin_id")
    Admin adminExecutor;

    String note ;

    String accountNumber ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("pvWithdrawalRequestStatuses")
    @JoinColumn(name="bank_id")
    Bank bank ;

    String receiptCode ;

    String image ;

    @Transient
    String imageUrl ;

    @Transient
    String statusName ;
    @Transient
    String statusDescription ;

    public static PVWithdrawalRequestStatus build(WithdrawalRequest withdrawalRequest, WithdrawalStatus withdrawalStatus, Admin adminExecutor, String note){
        PVWithdrawalRequestStatus pvWithdrawalRequestStatus = new PVWithdrawalRequestStatus();
        pvWithdrawalRequestStatus.setAdminExecutor(adminExecutor);
        pvWithdrawalRequestStatus.setWithdrawalStatus(withdrawalStatus);
        pvWithdrawalRequestStatus.setWithdrawalRequest(withdrawalRequest);
        pvWithdrawalRequestStatus.setNote(note);
        return pvWithdrawalRequestStatus ;
    }

    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isNotEmpty(image)){
            this.imageUrl = Constant.prefixApi+Constant.REST_WITHDRAWAL_RECEIPT_IMAGE+"/"+getId()+"/"+image;
        }
        if(withdrawalRequest!=null){
            statusName = withdrawalStatus.getName();
            statusDescription = withdrawalStatus.getDescription();
        }


    }
}
