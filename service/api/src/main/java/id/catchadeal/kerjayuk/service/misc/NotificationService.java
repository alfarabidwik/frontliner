package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.Notification;
import id.catchadeal.kerjayuk.entity.misc.NotificationUser;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.misc.NotificationData;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.repository.misc.NotificationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.google.FCMService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class NotificationService extends BasicRepoService<Notification> {

    @Autowired private NotificationRepository repository ;

    @Autowired NotificationUserService notificationUserService ;
    @Autowired UserService userService ;

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);
    private FCMService fcmService;

    public NotificationService(FCMService fcmService) {
        this.fcmService = fcmService;
    }

    @Override
    public JpaRepository repository() {
        return repository;
    }


    public DataPage<Notification> notifications(Boolean createByAdmin, Integer page, String sortir, Boolean ascending){

        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sqlCount = "SELECT COUNT(n.id) ";
        String sqlObject = "SELECT n ";

        String sqlFrom = " FROM Notification n WHERE 1+1 = 2 ";
        if(createByAdmin!=null){
            if(createByAdmin){
                sqlFrom = sqlFrom+" AND n.admin != null ";
            }else{
                sqlFrom = sqlFrom+" AND n.admin == null ";
            }
        }


        sqlFrom = sqlFrom+" GROUP BY n.id, n.created ";
        sqlFrom = sqlFrom+" ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");

        String finalSqlCount = sqlCount+sqlFrom;
        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(finalSqlCount);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            countResult = 0 ;
        }

        String finalSqlObject = sqlObject+sqlFrom;
        logger.debug("SQL NOTIFICATION {}  ", finalSqlObject);
        Query queryObject  = entityManager.createQuery(finalSqlObject);
        queryObject.setFirstResult(offset);
        queryObject.setMaxResults(pageRow);
        for(String key :paramaterMap.keySet()) {
            queryObject.setParameter(key, paramaterMap.get(key));
        }

        List<Notification> objects = queryObject.getResultList();

        DataPage dataPage = DataPage.builder(objects, pageRow, countResult);
        return dataPage;
    }



    public Notification sendTopicNotification(Notification notification){
        NotificationDto notificationDto = mapper.map(notification, NotificationDto.class);
        try{
            HashMap hashMap = new HashMap();
            hashMap.put("messageId", String.valueOf(System.nanoTime()));
            hashMap.put("text", notificationDto.getMessage());
            fcmService.sendMessage(hashMap, notificationDto);
            notification = save(notification);
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage(), e);
        }
        return notification ;
    }


    public Notification sendToToken(Notification notification, String token) throws Exception{
        try{
            NotificationDto notificationDto = mapper.map(notification, NotificationDto.class);
            fcmService.sendMessageToToken(notificationDto, token);
            System.out.println("OOOOOOOOO I");
            notification = save(notification);
            return notification ;
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            throw  e ;
        }
    }

    public Notification sendSpesific(Admin admin, List<User> users, String title, String message, Long referenceId, Class reference, String imageLink){
        return sendTo(admin, users, NotificationDto.SPECIFIC, null, title, message, referenceId, reference!=null?reference.getName():null, null, imageLink);
    }

    public Notification sendSpesific(User user, String title, String message, Long referenceId, Class reference, String image){
        return sendTo(null, Arrays.asList(user), NotificationDto.SPECIFIC, null, title, message, referenceId, reference!=null?reference.getName():null, null, image);
    }

    public Notification sendSpesific(User user, String type, String title, String message, Long referenceId, Class reference, String image){
        return sendTo(null, Arrays.asList(user), NotificationDto.SPECIFIC, type, title, message, referenceId, reference!=null?reference.getName():null, null,image);
    }

    public Notification sendBroadcast(Admin admin, String topic, String type, String title, String message, Long referenceId, Class reference, String addressLink, String image){
        return sendTo(admin, null, topic, type, title, message, referenceId, reference!=null?reference.getName():null, addressLink, image);
    }

    @Transactional
    public Notification sendTo(Admin admin, List<User> users, String topic, String type, String title, String message, Long referenceId, String reference, String addressLink, String image){
        Notification notification = new Notification();
        notification.setTopic(topic);
        notification.setType(type);
        notification.setTitle(title);
        notification.setMessage(message);
        notification.setImage(image);
        notification.setAdmin(admin);
        notification = save(notification);
        NotificationData notificationData = buildNotificationData(notification.getId(), referenceId, reference, addressLink);
        notification.setData(notificationData);

        if(topic.equals(NotificationDto.SPECIFIC)){
            for (User user : users) {
                NotificationUser notificationUser = new NotificationUser();
                notificationUser.setNotification(notification);
                notificationUser.setUser(user);
                notificationUserService.save(notificationUser);

                Set<UserDevice> userDevices = user.getUserDevices();
                if(userDevices!=null){
                    int n = 1;
                    for (UserDevice userDevice : userDevices) {
                        try{
                            sendToToken(notificationUser.getNotification(), userDevice.getFcmToken());
                            logger.debug("Notification index {}, to {}", n, userDevice.getFcmToken());
                            n++;
                        }catch (Exception e){
                            e.printStackTrace();
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
            }
        }else{
            users = userService.findAll();
            for (User user : users) {
                NotificationUser notificationUser = new NotificationUser();
                notificationUser.setNotification(notification);
                notificationUser.setUser(user);
                notificationUserService.save(notificationUser);
            }
            sendTopicNotification(notification);
        }
        return notification;
    }


    NotificationData buildNotificationData(Long notificationId, Long referenceId, String reference, String addressLink){
        NotificationData notificationData = new NotificationData();

        if(referenceId!=null){
            notificationData.setReferenceId(String.valueOf(referenceId));
        }
        notificationData.setReferenceClass(reference);
        notificationData.setNotificationId(String.valueOf(notificationId));
        notificationData.setAddressLink(addressLink);
        return  notificationData ;
    }


    public boolean existById(Long id){
        return repository.existsById(id);
    }

}