package id.catchadeal.kerjayuk.entity.region;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Table(name = "m_province")
@Entity
@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Province extends EBase implements Serializable {
    String name ;
    String countryCode ;

    @OneToMany(mappedBy="province", fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SELECT) @JsonIgnoreProperties("province")
    List<City> cities ;

    String courierId ;

    @Override
    public void initTransient() {
        super.initTransient();

    }

}
