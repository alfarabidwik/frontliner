package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.FormApplication;
import id.catchadeal.kerjayuk.entity.business.FormLaneApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormLaneApplicationRepository extends JpaRepository<FormLaneApplication, Long> {

}
