package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.Configuration;
import id.catchadeal.kerjayuk.repository.misc.ConfigurationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class ConfigurationService extends BasicRepoService<Configuration> {

    @Autowired private ConfigurationRepository repository ;

    @Value("${banner.width}") int bannerWidth ;
    @Value("${banner.height}") int bannerHeight ;
    @Value("${task.banner.width}") int taskBannerWidth ;
    @Value("${task.banner.height}") int taskBannerHeight ;

    @Value("${mail.domain}") String mailDomain ;
    @Value("${mail.sender}") String mailSender ;


    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public Configuration findById(Long id) {
        Configuration configuration =  super.findById(id);
        configuration.setBannerWidth(bannerWidth);
        configuration.setBannerHeight(bannerHeight);
        configuration.setTaskBannerWidth(taskBannerWidth);
        configuration.setTaskBannerHeight(taskBannerHeight);
        configuration.setMailDomain(mailDomain);
        configuration.setRegularEmailSender(mailSender);
        return configuration ;

    }

    public Configuration configuration() {
        Configuration configuration =  super.configuration();
        configuration.setBannerWidth(bannerWidth);
        configuration.setBannerHeight(bannerHeight);
        configuration.setTaskBannerWidth(taskBannerWidth);
        configuration.setTaskBannerHeight(taskBannerHeight);
        configuration.setMailDomain(mailDomain);
        configuration.setRegularEmailSender(mailSender);
        return configuration ;
    }

    @Override
    public Configuration save(Configuration object) {
        object.setUpdated(new Date());
        object =  repository.save(object);
        appCacheManager.evictCacheByName(Constant.CACHE_CONFIGURATION);
        object = configuration();
        return object;
    }

    @Cacheable(cacheNames = "entityNames")
    public List<String> entities(){
        List<String> entityNames = new ArrayList<>();
        try{
            Reflections reflections = new Reflections("id.catchadeal.frontlinerapi");
            Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Entity.class);
            annotated.forEach(aClass -> {
                entityNames.add(aClass.getName());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        return entityNames;
    }

    public Configuration activate(Long configurationId){
        List<Configuration> configurations = findAll();
        for (int i = 0; i < configurations.size(); i++) {
            Configuration configuration = configurations.get(i);
            configuration.setActive(false);
            configuration = save(configuration);
        }
        Configuration configuration = findById(configurationId);
        configuration.setActive(true);
        configuration = save(configuration);
        appCacheManager.evictCacheByName("Configuration");
        configuration = configuration();
        return configuration;

    }

}
