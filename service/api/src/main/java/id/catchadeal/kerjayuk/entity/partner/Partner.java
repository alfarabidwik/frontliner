package id.catchadeal.kerjayuk.entity.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name="j_partner", indexes = {
        @Index(columnList = "partnerType", name = "Partner_partnerType_idx"),
        @Index(columnList = "created", name = "Partner_created_idx"),
        @Index(columnList = "updated", name = "Partner_updated_idx"),
        @Index(columnList = "active", name = "Partner_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Partner extends EBase {

    public static final String PERSONAL = "PERSONAL";
    public static final String COMPANY = "COMPANY";
    @Column(nullable = false)
    String partnerType ;

    @Column(nullable = false)
    String fullName ;

    String website ;
    String email ;

    String image ;
    @Transient String imageUrl ;
    @Transient String imageUri ;


    @OneToOne(fetch= FetchType.LAZY, cascade = CascadeType.ALL)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("partner")
    @JoinColumn(name="company_id")
    Company company ;

    @OneToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("partner")
    @JoinColumn(name="personal_id")
    Personal personal ;

    @OneToMany(mappedBy = "partner", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("partner")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    Set<PVPartnerStatus> pvPartnerStatuses = new HashSet<>();

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("partner")
    @JoinColumn(name="partner_status_id")
    PartnerStatus partnerStatus;

    @OneToMany(mappedBy = "partner", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("partner")
    Set<BusinessAddress> businessAddresses = new HashSet<>();

    @Transient
    Double distance ;

    String idCard ;
    String siupOrNwpw ;

    @Column(columnDefinition = "boolean default false", nullable = false)
    boolean idCardVerification ;
    @Column(columnDefinition = "boolean default false", nullable = false)
    boolean siupOrNwpwVerification ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task t WHERE t.partner_id = id)")
    Long taskCountFormula ;

    Long taskCount ;


    @Transient
    String siupOrNwpwUrl ;

    @Transient
    String idCardUrl ;


    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isNotEmpty(idCard)){
            idCardUrl = Constant.prefixApi+Constant.REST_PARTNER_ID_CARD+"/"+getId()+"/"+idCard;
        }
        if(StringUtils.isNotEmpty(siupOrNwpw)){
            siupOrNwpwUrl = Constant.prefixApi+Constant.REST_PARTNER_SIUP_OR_NPWP+"/"+getId()+"/"+siupOrNwpw;
        }

        if(StringUtils.isEmpty(idCard)){
            idCard = null ;
        }

        if(StringUtils.isEmpty(siupOrNwpw)){
            siupOrNwpw = null ;
        }

        if(idCard==null){
            idCardVerification = false ;
        }
        if(siupOrNwpw==null){
            siupOrNwpwVerification = false ;
        }

        imageUri = Constant.REST_PARTNER_IMAGE+"/"+getId()+"/"+image;
        imageUrl = Constant.prefixApi+imageUri;
        taskCount = taskCountFormula;
    }




}
