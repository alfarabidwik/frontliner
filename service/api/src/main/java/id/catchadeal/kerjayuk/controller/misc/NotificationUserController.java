package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.NotificationUser;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.service.misc.NotificationUserService;
import id.catchadeal.kerjayuk.service.user.UserDeviceService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class NotificationUserController extends BasicController {

    Logger logger = LoggerFactory.getLogger(NotificationUserController.class);

    @Autowired NotificationUserService notificationUserService;
    @Autowired UserDeviceService userDeviceService ;

    @GetMapping(path = "/notificationUsers")
    @FrontendRest
    public WSResponse read(@RequestHeader(Constant.AUTHORIZATION) String authorization,
                           @RequestParam(required = false) String topics,
                           @RequestParam(required = false) String types,
                           @RequestParam(required = false) String topicExcludes,
                           @RequestParam(required = false) String typeExcludes,
                           @RequestParam(defaultValue = "0") Integer page,
                           @RequestParam(defaultValue = "false") Boolean ascending,
                           @RequestParam(defaultValue = "nu.created") String sortir) {
        User user = jwtTokenProvider.getUser(authorization);
        DataPage<NotificationUser> notificationUserDataPage = notificationUserService.notificationUsers(user, topics, types, topicExcludes, typeExcludes, page, sortir, ascending);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notificationUserDataPage.getDatas()).setPageElement(notificationUserDataPage.getPageElement())
                .setTotalElement(notificationUserDataPage.getTotalElement()).setTotalPage(notificationUserDataPage.getTotalPage());
    }

    @GetMapping(path = "/notificationUser/countUnreadNotification")
    @FrontendRest
    public WSResponse countUnreadNotification(@RequestHeader(Constant.AUTHORIZATION) String authorization,
                           @RequestParam(required = false) String topics,
                           @RequestParam(required = false) String types,
                           @RequestParam(required = false) String topicExcludes,
                           @RequestParam(required = false) String typeExcludes) {
        User user = jwtTokenProvider.getUser(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notificationUserService.countUnreadNotification(user, topics, types, topicExcludes, typeExcludes));
    }



    @GetMapping(path = "/notificationUser/read")
    @FrontendRest
    public WSResponse read(@RequestParam Long notificationUserId) {
        NotificationUser notificationUser = notificationUserService.findById(notificationUserId);
        notificationUser.setRead(true);
        notificationUser = notificationUserService.save(notificationUser);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notificationUser);
    }


    @GetMapping(path = "/notificationUser/delete")
    @Override
    @AdminRest
    public WSResponse delete(@RequestParam Long id) {
        notificationUserService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }


}