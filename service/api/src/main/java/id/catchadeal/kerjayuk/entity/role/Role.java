package id.catchadeal.kerjayuk.entity.role;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import id.catchadeal.kerjayuk.entity.user.Admin;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.*;

@Data
@Table(name="m_role")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class Role extends NonIdEBase {

    private String name ;

    @OneToMany(mappedBy="role", fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT) @JsonIgnoreProperties("role")
    private Set<RoleMenu> roleMenus = new HashSet<>();

    @OneToMany(mappedBy="role", fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SELECT) @JsonIgnoreProperties("role")
    private List<Admin> admins = new ArrayList<>();

    @Transient
    public List<Menu> getTreeMenus(){
        try{
            HashMap<String, Menu> keyParentMenu = new HashMap<>();
            if(this.roleMenus!=null){
                List<RoleMenu> roleMenus = Arrays.asList(getRoleMenus().toArray(new RoleMenu[getRoleMenus().size()]));
                Collections.<RoleMenu>sort(roleMenus, (menu1, menu2) -> menu1.getMenu().getSortir()-menu2.getMenu().getSortir());

                roleMenus.forEach(roleMenu -> {
                    String nameMenu = roleMenu.getMenu().getName();
                    String groupMenu = roleMenu.getMenu().getGroupMenu();
                    if((nameMenu!=null && groupMenu!=null) && nameMenu.equalsIgnoreCase(groupMenu)){
                        boolean active = (roleMenu.getActive()!=null && roleMenu.getActive() && roleMenu.getMenu().getActive()!=null && roleMenu.getMenu().getActive());
                        if(active){
                            keyParentMenu.put(roleMenu.getMenu().getName(), roleMenu.getMenu());
                        }
                    }
                });
                roleMenus.forEach(roleMenu -> {
                    String nameMenu = roleMenu.getMenu().getName();
                    String groupMenu = roleMenu.getMenu().getGroupMenu();
                    boolean hasParent = false ;
                    if((nameMenu!=null && groupMenu!=null) && !groupMenu.equalsIgnoreCase(nameMenu) && keyParentMenu.containsKey(groupMenu)){
                        boolean active = (roleMenu.getActive()!=null && roleMenu.getActive() && roleMenu.getMenu().getActive()!=null && roleMenu.getMenu().getActive());
                        if(active){
                            hasParent = true ;
                            Menu parentMenu = keyParentMenu.get(groupMenu);
                            if(!parentMenu.hasChildMenu(roleMenu.getMenu().getId())){
                                parentMenu.getChildMenus().add(roleMenu.getMenu());
                            }
                        }
                    }
                    if(!hasParent){
                        boolean active = (roleMenu.getActive()!=null && roleMenu.getActive() && roleMenu.getMenu().getActive()!=null && roleMenu.getMenu().getActive());
                        if(active){
                            keyParentMenu.put(roleMenu.getMenu().getName(), roleMenu.getMenu());
                        }
                    }
                });

            }
            List<Menu> resultMenus = new ArrayList<>();
            keyParentMenu.forEach((s, menu) -> {
                resultMenus.add(menu);
            });
            Collections.<Menu>sort(resultMenus, (menu1, menu2) -> menu1.getSortir()-menu2.getSortir());
            return resultMenus;
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


}
