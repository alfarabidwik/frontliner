package id.catchadeal.kerjayuk.entity.business;

import id.catchadeal.kerjayuk.entity.NonIdEBase;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Data
@Table(name="m_task_application_status", indexes = {
        @Index(columnList = "created", name = "TaskApplicationStatus_created_idx"),
        @Index(columnList = "updated", name = "TaskApplicationStatus_updated_idx"),
        @Index(columnList = "active", name = "TaskApplicationStatus_active_idx"),
})
//@Table(name = "m_task_application_status")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class TaskApplicationStatus extends NonIdEBase {


    public static final TaskApplicationStatus TAKEN = new TaskApplicationStatus(TaskApplicationStatusDto.TAKEN_ID);
    public static final TaskApplicationStatus SUBMITTED = new TaskApplicationStatus(TaskApplicationStatusDto.SUBMITTED_ID);
    public static final TaskApplicationStatus ACCEPTED = new TaskApplicationStatus(TaskApplicationStatusDto.ACCEPTED_ID);
    public static final TaskApplicationStatus REJECTED = new TaskApplicationStatus(TaskApplicationStatusDto.REJECTED_ID);

    public static final String DESCRIPTION = "1 = Taken \n2 = Submitted \n3 = Accepted \n4 = Rejected";


    String name ;
    String description ;
    int sortir ;


    public TaskApplicationStatus(Long id) {
        this.id = id;
    }
}
