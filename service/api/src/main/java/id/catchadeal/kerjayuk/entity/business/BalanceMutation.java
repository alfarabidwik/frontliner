package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.business.MutationInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Table(name="j_balance_mutation", indexes = {
        @Index(columnList = "created", name = "BalanceMutation_created_idx"),
        @Index(columnList = "updated", name = "BalanceMutation_updated_idx"),
        @Index(columnList = "active", name = "BalanceMutation_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BalanceMutation extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("balanceMutations")
    @JoinColumn(name="user_id", updatable = false, nullable = false)
    User user ;

    BigDecimal debitAmount = new BigDecimal(0);
    BigDecimal creditAmount = new BigDecimal(0);

    @Type(type = "jsonb")
    @Column(name = "mutation_info", columnDefinition = "jsonb", nullable = false)
    MutationInfo mutationInfo ;



    public static BalanceMutation build(User user, BigDecimal debitAmount, BigDecimal creditAmount, MutationInfo mutationInfo){
        BalanceMutation balanceMutation = new BalanceMutation();
        balanceMutation.setUser(user);
        balanceMutation.setDebitAmount(debitAmount);
        balanceMutation.setCreditAmount(creditAmount);
        balanceMutation.setMutationInfo(mutationInfo);
        return balanceMutation ;
    }


}
