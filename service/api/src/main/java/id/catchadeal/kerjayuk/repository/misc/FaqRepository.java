package id.catchadeal.kerjayuk.repository.misc;

import id.catchadeal.kerjayuk.entity.misc.Faq;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FaqRepository extends JpaRepository<Faq, Long> {

    List<Faq> findByActive(Boolean active, Sort sort);

    @Query("SELECT f FROM Faq f WHERE (f.title LIKE (CONCAT('%',LOWER(:search),'%')) OR f.subtitle LIKE (CONCAT('%',LOWER(:search),'%'))) ")
    List<Faq> findBySearch(@Param("search") String search, Sort sort);

    @Query("SELECT f FROM Faq f WHERE f.active = :active AND (f.title LIKE (CONCAT('%',LOWER(:search),'%')) OR f.subtitle LIKE (CONCAT('%',LOWER(:search),'%'))) ")
    List<Faq> findByActive(@Param("active") Boolean active, @Param("search") String search, Sort sort);


}
