package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.business.*;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.MutationInfo;
import id.catchadeal.kerjayuk.model.business.ReferralMemberDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.wsresponse.business.WSTaskApplication;
import id.catchadeal.kerjayuk.model.wsresponse.business.WSTaskApplications;
import id.catchadeal.kerjayuk.service.business.*;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.NumberUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "${api}")
public class TaskApplicationController extends BasicController {

    Logger logger = LoggerFactory.getLogger(TaskApplicationController.class);

    @Autowired TaskApplicationService taskApplicationService;
    @Autowired TaskApplicationStatusService taskApplicationStatusService;
    @Autowired TaskService taskService;
    @Autowired FormApplicationService formApplicationService ;
    @Autowired FormLaneApplicationService formLaneApplicationService ;
    @Autowired BalanceMutationService balanceMutationService ;


    @GetMapping(path = "taskApplications")
    @FrontendRest@AdminRest
    @ApiOperation(value = "Please according status to filter list response", notes = TaskApplicationStatus.DESCRIPTION, response = WSTaskApplications.class)
    public WSResponse taskApplications(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                      @RequestParam(required = false) Long userId,
                                      @RequestParam(required = false) Long taskId,
                                      @RequestParam(defaultValue = "") String search,
                                      @RequestParam(required = false) Boolean published,
                                       @RequestParam(required = false) Long taskCategoryId,
                                       @RequestParam(required = false) BigDecimal feeStart,
                                       @RequestParam(required = false) BigDecimal feeEnd,
                                       @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                       @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
                                       @ApiParam(
                                               allowableValues = TaskApplicationStatusDto.TAKEN_IDS+","
                                              +TaskApplicationStatusDto.SUBMITTED_IDS+","
                                              +TaskApplicationStatusDto.ACCEPTED_IDS +","
                                              +TaskApplicationStatusDto.REJECTED_IDS) @RequestParam(required = false) Long taskApplicationStatusId,
                                      @RequestParam(defaultValue = "0") Integer page,
                                      @RequestParam(defaultValue = "false") Boolean ascending,
                                      @ApiParam(allowableValues = "ja.updated, jt.fee, ja.itemFee", required = true) @RequestParam(defaultValue = "ja.updated") String sortir){
        DataPage dataPage = taskApplicationService.taskApplications(userId, taskId, search, published, taskCategoryId, feeStart, feeEnd, startDate, endDate, taskApplicationStatusId, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());

    }

    @GetMapping("/taskApplication")
    public WSResponse findById(@RequestHeader(name = Constant.AUTHORIZATION) String authorization, @RequestParam Long id) {
        TaskApplication taskApplication = taskApplicationService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplication);
    }

    @GetMapping("/taskApplication/getMyTaskApplication")
    public WSResponse getMyTaskApplication(@RequestHeader(name = Constant.AUTHORIZATION) String authorization, @RequestParam Long taskId) {
        User user = jwtTokenProvider.getUser(authorization);
        TaskApplication taskApplication = taskApplicationService.findByUserIdAndTaskIdAndTaskApplicationStatusId(user.getId(), taskId, TaskApplicationStatus.TAKEN.getId());
        if(taskApplication==null){
            Task task = taskService.findById(taskId);
            taskApplication = taskApplicationService.apply(user, task);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplication);
    }


    @Transactional
    @PostMapping(path = "taskApplication/apply")
    @FrontendRest
    @ApiOperation(value = "", response = WSTaskApplication.class)
    public WSResponse apply(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                     @RequestParam Long taskId){

        User user = restUtils.user(authorization);
        Task task = taskService.findById(taskId);
        if(!task.isApplyable()){
            throw new AppException(Constant.FAILED_CODE, task.getMessage());
        }
        TaskApplication taskApplication = taskApplicationService.apply(user, task);

        commitUserActivity(user, UserActivity.APPLY_TASK, taskApplication);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplication);
    }

    @Transactional
    @PostMapping(path = "taskApplication/commit")
    @FrontendRest
    @ApiOperation(value = "", response = WSTaskApplication.class)
    public WSResponse commit(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                             @RequestBody TaskApplicationDto taskApplicationDto) throws Exception{

        taskApplicationService.findById(taskApplicationDto.getId());
        User user = restUtils.user(authorization);

        TaskApplication taskApplication = modelMapper.map(taskApplicationDto, TaskApplication.class);
        Task task  = taskService.findById(taskApplication.getTask().getId());
        if(!task.isApplyable()){
            throw new AppException(Constant.FAILED_CODE, task.getMessage());
        }

        taskApplication.setUser(user);
        taskApplication.setTotalFee(taskApplication.getTask().getFee());
        final String orderReference = taskApplication.getOrderReference();
        List<FormLaneApplication> formLaneApplications = taskApplication.getFormLaneApplications();
        taskApplication = taskApplicationService.save(taskApplication);
        for (FormLaneApplication formLaneApplication : formLaneApplications) {
            formLaneApplication.setTaskApplication(taskApplication);
            formLaneApplication = formLaneApplicationService.save(formLaneApplication);
            List<FormApplication> formApplications = formLaneApplication.getFormApplications();
            for (FormApplication formApplication : formApplications) {
                Form form = formApplication.getForm();
                if(form.getType().equalsIgnoreCase(Form.IMAGE)){
                    String imageDirectory = form.getImageDirectory();
                    String image = fileStorage.storeFile(Constant.FORM_MEDIA_DIRECTORY+imageDirectory, null, orderReference, base64ToFile(formApplication.getValue()));
                    formApplication.setValue(image);
                }
                formApplication.setFormLaneApplication(formLaneApplication);
                formApplication.setForm(form);
                formApplication = formApplicationService.save(formApplication);
            }
        }
        /**
         * Override Set Accumulative Fee For Referral User
        * */
        BigDecimal totalFee = new BigDecimal(0);
        Set<ReferralMember> referralMembers = taskApplication.getReferralMembers();
        for (ReferralMember referralMember : referralMembers) {
            totalFee = totalFee.add(taskApplication.getItemFee());
        }
        if(totalFee.compareTo(new BigDecimal(0))>0){
            taskApplication.setTotalFee(totalFee);
        }
        taskApplication = taskApplicationService.save(taskApplication);
        taskApplication = taskApplicationService.updateStatus(taskApplication, TaskApplicationStatus.SUBMITTED.getId(), null);
        commitUserActivity(user, UserActivity.COMMIT_TASK, taskApplication);
        notificationService.sendSpesific(taskApplication.getUser(),message("task.application.send.title", taskApplication.getTask().getTitle()), message("task.application.send.description"), taskApplication.getId(),
                TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());

        return WSResponse.instance(Constant.SUCCESS_CODE, message("task.form.has.been.submitted"), taskApplication);
    }


    @Transactional
    @PostMapping(path = "taskApplication/changeStatus")
    @AdminRest
    @ApiOperation(value = "Please According To Status Classification", notes = TaskApplicationStatus.DESCRIPTION, response = WSTaskApplication.class)
    public WSResponse changeStatus(
            @RequestHeader(name = Constant.AUTHORIZATION) String authorization,
            @RequestParam Long taskApplicationId,
            @ApiParam(allowableValues =
                    TaskApplicationStatusDto.TAKEN_IDS+","+
                            TaskApplicationStatusDto.ACCEPTED_IDS
                            +","+ TaskApplicationStatusDto.REJECTED_IDS, required = true)
            @RequestParam Long taskApplicationStatusId,
            @RequestParam(required = false) String note){


        if(!jwtTokenProvider.isAdmin(authorization)){
            throw new AppException(Constant.FAILED_CODE, "disallowed.for.non.admin");
        }

        Admin admin = jwtTokenProvider.getAdmin(authorization);

        TaskApplication taskApplication = taskApplicationService.findById(taskApplicationId);
        TaskApplicationStatus previousStatus = taskApplication.getTaskApplicationStatus();
        if(taskApplication.getTaskApplicationStatus()!=null && taskApplication.getTaskApplicationStatus().getId().equals(taskApplicationStatusId)){
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplication);
        }

        taskApplication = taskApplicationService.updateStatus(taskApplication, taskApplicationStatusId, note);
        if(taskApplicationStatusId.equals(TaskApplicationStatus.ACCEPTED.getId())){
            MutationInfo mutationInfo = new MutationInfo();
            mutationInfo.setDescription(taskApplication.getTaskApplicationStatus().getDescription());
            mutationInfo.setId(taskApplicationId);
            mutationInfo.setEntityClassName(TaskApplication.class.getName());
            balanceMutationService.creditBalance(taskApplication.getUser(), taskApplication.getTotalFee(), mutationInfo);
            notificationService.sendSpesific(taskApplication.getUser(), message("task.application.success.title", taskApplication.getTask().getTitle()), message("task.application.success.description",
                    taskApplication.getUser().getFullname(), taskApplication.getTask().getTitle()+", "+ taskApplication.getTask().getTaskCategory().getName()), taskApplication.getId(), TaskApplicationDto.class,
                    taskApplication.getTask().getPartner().getImageUri());
            notificationService.sendSpesific(taskApplication.getUser(), message("fee.success.title"),
                    message("fee.success.description", NumberUtil.moneyFormat(taskApplication.getTotalFee(), true)), taskApplication.getId(), TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());

        }
        if(taskApplicationStatusId.equals(TaskApplicationStatus.REJECTED.getId())){
            notificationService.sendSpesific(taskApplication.getUser(),  message("task.application.rejected.title", taskApplication.getTask().getTitle()), message("task.application.rejected.description"),
                    taskApplication.getId(), TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());
        }

        commitAdminActivity(admin, AdminActivity.CHANGE_TASK_APPLICATION_STATUS, taskApplication, previousStatus);

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplication);
    }

    @GetMapping("/taskApplication/exists")
    public WSResponse taskExists(@RequestHeader(Constant.AUTHORIZATION) String authorization, @RequestParam Long taskId){
        User user = jwtTokenProvider.getUser(authorization);
        Boolean exist = taskApplicationService.existsByUserIdAndTaskId(user.getId(), taskId, TaskApplicationStatusDto.TAKEN_ID);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, exist);

    }







}