package id.catchadeal.kerjayuk.service.region;

import id.catchadeal.kerjayuk.entity.region.District;
import id.catchadeal.kerjayuk.repository.region.DistrictRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DistrictService extends BasicRepoService<District> {

    @Autowired DistrictRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Cacheable(cacheNames = "Districts1")
    @Override
    public List<District> findAll() {
        return repository.findAll();
    }

    @Cacheable(cacheNames = "Districts2", key = "{#active, #sort}")
    public List<District> findAll(Boolean active, Sort sort) {
        if(active!=null){
            return repository.findByActive(active, sort);
        }else{
            return repository.findAll(sort);
        }
    }

    @Cacheable(cacheNames = "Districts3", key = "{#active, #page, #sortir, #ascending}")
    public Page<District> findAll3(Boolean active, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActive(active, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findAll(PageRequest.of(page, pageRow, sort));
        }
    }

    @Cacheable(cacheNames = "District", key = "#id")
//    @Override
    public District findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public District save(District village) {
        return  super.save(village);
    }

    @Cacheable(cacheNames = "Districts4", key = "{#active, #cityId, #sort}")
    public List<District> findByCityId(Boolean active, Long cityId, Sort sort){
        if(active!=null){
           return repository.findByActiveAndCityId(active, cityId, sort);
        }else{
            return repository.findByCityId(cityId, sort);
        }
    }

    @Cacheable(cacheNames = "Districts5", key = "{#active, #cityId, #page, #sortir, #ascending}")
    public Page<District> findAll4(Boolean active, Long cityId, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActiveAndCityId(active, cityId, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findByCityId(cityId, PageRequest.of(page, pageRow, sort));
        }
    }


}
