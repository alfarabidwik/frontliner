package id.catchadeal.kerjayuk.service.payment;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.payment.Bank;
import id.catchadeal.kerjayuk.repository.payment.BankRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class BankService extends BasicRepoService<Bank> {

    @Autowired
    BankRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<Bank> findAll() {
        return repository.findAll(new Sort(Sort.Direction.DESC, "updated"));
    }


    public List<Bank> findAll(Boolean active, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        List<Bank> banks = new ArrayList<>();
        if(active!=null){
            banks = repository.findByActive(active, sort);
        }else{
            banks = repository.findAll(sort);
        }
        return banks;
    }

//    @Override
    public Bank findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    //@Transactional
    @Override
    public Bank save(Bank customer) {
        return super.save(customer);
    }





}
