package id.catchadeal.kerjayuk.service.psychotest;

import id.catchadeal.kerjayuk.entity.psychotest.PersonalityCharacter;
import id.catchadeal.kerjayuk.repository.psychotest.PersonalityCharacterRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalityCharacterService extends BasicRepoService<PersonalityCharacter> {

    @Autowired private PersonalityCharacterRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }


    public List<PersonalityCharacter> findByPersonalityId(Long personalityId){
        return repository.findByPersonalityId(personalityId);
    }


}
