package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.BusinessAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface BusinessAddressRepository extends JpaRepository<BusinessAddress, Long> {
    List<BusinessAddress> findByPartnerIdAndTitle(Long partnerId, String title);
}
