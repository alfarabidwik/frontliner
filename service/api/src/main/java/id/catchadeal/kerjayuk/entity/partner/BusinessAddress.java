package id.catchadeal.kerjayuk.entity.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.region.Village;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="j_business_address", indexes = {
        @Index(columnList = "title", name = "BusinessAddress_title_idx"),
        @Index(columnList = "latitude,longitude", name = "BusinessAddress_latlong_idx"),
        @Index(columnList = "address", name = "BusinessAddress_address_idx"),
        @Index(columnList = "created", name = "BusinessAddress_created_idx"),
        @Index(columnList = "updated", name = "BusinessAddress_updated_idx"),
        @Index(columnList = "active", name = "BusinessAddress_active_idx"),
})
//@Table(name="j_business_address")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class BusinessAddress extends EBase {


    String title ;

    @Column(columnDefinition = "TEXT")
    String address ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("businessAddresses")
    @JoinColumn(name="village_id")
    Village village ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("businessAddresses")
    @JoinColumn(name="partner_id")
    Partner partner;

    @Column(columnDefinition = "double precision DEFAULT 0")
    Double latitude ;
    @Column(columnDefinition = "double precision DEFAULT 0")
    Double longitude ;

    @Override
    public void initTransient() {
        super.initTransient();
        mergeActiveStatus(partner);
    }
}
