package id.catchadeal.kerjayuk.repository.misc;

import id.catchadeal.kerjayuk.entity.misc.TaskCategory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskCategoryRepository extends JpaRepository<TaskCategory, Long> {


    List<TaskCategory> findByActive(Boolean active, Sort sort);


}
