package id.catchadeal.kerjayuk.entity;


import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
@MappedSuperclass
//@DynamicUpdate
//@DynamicInsert
public abstract class NonIdEBase extends BasicField implements Serializable {


    @Id
    protected Long id ;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id ;
    }

//    @Override
//    public int hashCode() {
//        if (id == null) {
//            return (int) System.nanoTime();
//        }
//        return id.intValue();
//    }

//    @Override
//    public boolean equals(Object obj) {
//        // it checks if the argument is of the
//        // type Geek by comparing the classes
//        // of the passed argument and this object.
//        // if(!(obj instanceof Geek)) return false; ---> avoid.
//        try {
//            if(obj == null)
//                return false;
//
//            if(this == obj)
//                return true;
//
//            // type casting of the argument.
//            EBase eBase = (EBase) obj;
//
//            // comparing the state of argument with
//            // the state of 'this' Object.
//            return (eBase.id!=null) && (this.id!=null) && (eBase.id == this.id) ;
//        }catch (Exception e){
//            return false ;
//        }
//    }


}
