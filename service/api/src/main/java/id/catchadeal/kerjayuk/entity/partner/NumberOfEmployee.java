package id.catchadeal.kerjayuk.entity.partner;

import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Data
@Table(name="m_number_of_employee", indexes = {
        @Index(columnList = "created", name = "NumberOfEmployee_created_idx"),
        @Index(columnList = "updated", name = "NumberOfEmployee_updated_idx"),
        @Index(columnList = "active", name = "NumberOfEmployee_active_idx"),
})
//@Table(name="m_number_of_employee")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class NumberOfEmployee extends EBase {

    int rangeStart ;
    int rangeEnd ;

    @Override
    public void initTransient() {
        super.initTransient();

    }


}
