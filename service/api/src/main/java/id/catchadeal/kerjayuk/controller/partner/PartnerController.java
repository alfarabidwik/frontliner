package id.catchadeal.kerjayuk.controller.partner;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.partner.*;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.partner.BusinessAddressDto;
import id.catchadeal.kerjayuk.model.partner.PartnerDto;
import id.catchadeal.kerjayuk.service.partner.BusinessAddressService;
import id.catchadeal.kerjayuk.service.partner.CompanyService;
import id.catchadeal.kerjayuk.service.partner.PersonalService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class PartnerController extends BasicController {

    Logger logger = LoggerFactory.getLogger(PartnerController.class);

    @Autowired BusinessAddressService businessAddressService ;
    @Autowired CompanyService companyService ;
    @Autowired PersonalService personalService ;


    @GetMapping(path = "partners")
    @FrontendRest
    @AdminRest
    @ApiOperation(value = "Please according to partner status classification", notes = PartnerStatus.DESCRIPTION)
    public WSResponse partners(
            @RequestHeader(name = Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "") String keyword,
            @RequestParam(required = false) Double latitude,
            @RequestParam(required = false) Double longitude,
            @ApiParam(allowableValues = PartnerStatus.PUBLISHED_ID+","+PartnerStatus.BANNED_ID+","+PartnerStatus.SUBJECT_OF_VIOLATION_ID) @RequestParam(required = false) Long partnerStatusId,
            @ApiParam(allowableValues = Partner.COMPANY+","+Partner.PERSONAL) @RequestParam(required = false) String type,
            @RequestParam(required = false) Long provinceId,
            @RequestParam(required = false) Long cityId,
            @RequestParam(required = false) Long districtId,
            @RequestParam(required = false) Long villageId,
            @RequestParam(required = false) Boolean active,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Boolean ascending,
            @ApiParam(allowableValues = "jp.full_name, jp.website, jp.created, jp.updated") @RequestParam(required = false) String sortir){

        DataPage dataPage = partnerService.partners( keyword, latitude, longitude, partnerStatusId, type, provinceId, cityId, districtId, villageId, active,  page, pageRow, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());
    }



    @GetMapping(path = "partner")
    @Override
    @AdminRest
    @ApiOperation(value = "")
    public WSResponse findById(@RequestParam Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, partnerService.findById(id));
    }

    @GetMapping(path = "partner/delete/{id}")
    @Override
    @AdminRest
    @ApiOperation(value = "")
    public WSResponse delete(@PathVariable Long id) {
        Partner partner = partnerService.findById(id);
        partnerService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }

    @Transactional
    @PostMapping(path = "partner/businessAddress/save")
    @FrontendRest
    @ApiOperation(value = "")
    public WSResponse saveBusinessAddress(
            @RequestHeader(name = Constant.AUTHORIZATION) String authorization,
            @RequestParam Long partnerId,
            @RequestBody @Valid BusinessAddressDto businessAddressData){
        Partner partner= partnerService.findById(partnerId);
        BusinessAddress businessAddress = modelMapper.map(businessAddressData, BusinessAddress.class);
        businessAddress.setPartner(partner);
        businessAddress = businessAddressService.save(businessAddress);

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, businessAddress);
    }

    @GetMapping(path = "partner/businessAddress/delete")
    @FrontendRest
    public WSResponse deleteExperience(@RequestHeader(name = Constant.AUTHORIZATION) String authorization, @RequestParam Long businessAddressId){
        businessAddressService.delete(businessAddressId);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }

    @Transactional
    @PostMapping(path = "partner/changeStatus")
    @AdminRest
    @ApiOperation(value = "Please according to employee status classification",
            notes = PartnerStatus.DESCRIPTION+"\n<b>Note and cause is additional in case if administrator need more information about status changes</b> ")
    public WSResponse changeStatus(
            @RequestHeader(name = Constant.AUTHORIZATION) String authorization,
            @RequestParam Long partnerId,
            @RequestParam(required = false) String cause,
            @RequestParam(required = false) String note,
            @ApiParam(allowableValues = PartnerStatus.PUBLISHED_ID+","+PartnerStatus.BANNED_ID+","+PartnerStatus.SUBJECT_OF_VIOLATION_ID) @RequestParam(required = false) Long partnerStatusId){
        Partner partner = partnerService.updateStatus(partnerService.findById(partnerId), partnerStatusId, cause, note);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, partner);
    }

    @PostMapping(path = "partner/saveUpload")
    @FrontendRest
    @ApiOperation(value = "")
    public WSResponse updateProfile(
            @RequestHeader(name = Constant.AUTHORIZATION) String authorization,
            @RequestParam String partnerDtoGson,
            @RequestParam(required = false) MultipartFile multipartFile) throws Exception{

        PartnerDto partnerDto= gson.fromJson(partnerDtoGson, PartnerDto.class);

        Partner partner = modelMapper.map(partnerDto, Partner.class);

        Company company = partner.getCompany();
        if(company!=null){
            company  = companyService.save(company);
        }
        Personal personal  = partner.getPersonal();
        if(personal!=null){
            personal = personalService.save(personal);
        }
        partner = partnerService.save(partner);

        boolean needUpdate = false;
        if(multipartFile!=null){
            String filename = String.valueOf(new Date().getTime());
            String image = fileStorage.storeFile(partnerImageDir, partner.getId().toString(), filename, multipartFile);
            partner.setImage(image);
            needUpdate = true ;
        }
        if(needUpdate){
            partner = partnerService.save(partner);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS,  partner);
    }


}
