package id.catchadeal.kerjayuk.controller.email;

import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.email.SendEmail;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.email.SendEmailDto;
import id.catchadeal.kerjayuk.service.mail.SendEmailService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "${api}")
public class SendEmailController extends BasicController {

    @Autowired
    SendEmailService sendEmailService;


    @GetMapping(path = "/sendEmails")
    @UniversalRest
    public WSResponse findAll(
            @RequestParam(defaultValue = "created") String sortir,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "") String search){
        Utils.validateParam(sortir);
        Page<SendEmail> sendEmailPage = sendEmailService.findAll(sortir, ascending, page, search);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, sendEmailPage.getContent()).setTotalElement(sendEmailPage.getTotalElements()).setTotalPage(sendEmailPage.getTotalPages()).setPageElement(pageRow);
    }

    @PostMapping(path = "/sendEmail/send")
    @UniversalRest
    public WSResponse save(@RequestBody @Valid SendEmailDto sendEmailDto) throws Exception{
        SendEmail sendEmail = sendEmailService.send(sendEmailDto);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, sendEmail);
    }
    @PostMapping(path = "/sendEmail/simpleSend")
    @UniversalRest
    public WSResponse simpleSend(@RequestParam String email) throws Exception{
        emailService.simpleSend(email, "Ini pesan test");
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


}
