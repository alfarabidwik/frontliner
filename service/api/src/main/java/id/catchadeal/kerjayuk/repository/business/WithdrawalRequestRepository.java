package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.WithdrawalRequest;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WithdrawalRequestRepository extends JpaRepository<WithdrawalRequest, Long> {

}
