package id.catchadeal.kerjayuk.service.auth;

import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserAuthService implements UserDetailsService {


    @Autowired UserService userService ;
    @Autowired AdminService adminService ;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null ;
    }

    public UserDetails loadUserBySubjectKey(String subjectKey){
        User user = userService.findTopByEmailOrMobilePhoneAndActive(subjectKey);
        return user ;
    }

    public UserDetails loadAdminByEmail(String email){
        Admin admin = adminService.findByEmail(email);
        return admin ;
    }



}
