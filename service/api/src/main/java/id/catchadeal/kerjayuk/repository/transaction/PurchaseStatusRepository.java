package id.catchadeal.kerjayuk.repository.transaction;

import id.catchadeal.kerjayuk.entity.transaction.PurchaseStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseStatusRepository extends JpaRepository<PurchaseStatus, Long> {

}
