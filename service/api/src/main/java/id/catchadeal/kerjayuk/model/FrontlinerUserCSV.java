package id.catchadeal.kerjayuk.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FrontlinerUserCSV {

    @CsvBindByName String fullname = "";
    @CsvBindByName(required = true) String email ;
    @CsvBindByName(required = true) String password ;
    @CsvBindByName String mobilePhone ;
    @CsvBindByName String organization ;


}
