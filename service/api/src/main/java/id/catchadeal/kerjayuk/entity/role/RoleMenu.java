package id.catchadeal.kerjayuk.entity.role;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="pv_role_menu")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class RoleMenu extends EBase {

    @ManyToOne(fetch=FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="role_id")
    private Role role ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="menu_id")
    private Menu menu ;


}
