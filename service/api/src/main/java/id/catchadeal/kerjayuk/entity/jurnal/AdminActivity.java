package id.catchadeal.kerjayuk.entity.jurnal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.Admin;

import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.*;
import java.util.*;

@Data
@Table(name="j_admin_activity")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class AdminActivity extends EBase {

    /**
     * DESCRIPTION
     * */

    public static final String LOGIN = "admin.login" ;
    public static final String LOGOUT = "admin.logout" ;
    public static final String CHANGE_PASSWORD = "admin.change.password" ;

    public static final String CREATE_NEW_ADMIN = "admin.create.new.admin" ;
    public static final String UPDATE_ADMIN = "admin.update.admin" ;
    public static final String DELETE_ADMIN = "admin.delete.admin" ;
    public static final String UPDATE_PROFILE = "admin.update.profile" ;
    public static final String UPLOAD_PHOTO = "admin.upload.photo" ;
    public static final String RESEND_PASSWORD = "admin.resend.password.admin" ;


    /**
     * TODO COMPLETE MESSAGE PROPERTIES BELOW
     * */

    public static final String CREATE_CONFIGURATION = "admin.create.new.configuration" ;
    public static final String UPDATE_CONFIGURATION = "admin.update.configuration" ;
    public static final String DELETE_CONFIGURATION = "admin.delete.configuration" ;

    /**
     * FINISH PROPERTIES ABOVE
     * */


    public static final String CREATE_NEW_COLOR = "admin.create.new.color" ;
    public static final String UPDATE_COLOR = "admin.update.color" ;
    public static final String DELETE_COLOR = "admin.delete.color" ;

    public static final String CREATE_NEW_BANK_ACCOUNT = "admin.create.new.bank.account" ;
    public static final String UPDATE_BANK_ACCOUNT = "admin.update.bank.account" ;
    public static final String DELETE_BANK_ACCOUNT = "admin.delete.bank.account" ;

    public static final String CREATE_NEW_BANK = "admin.create.new.bank" ;
    public static final String UPDATE_BANK = "admin.update.bank" ;
    public static final String DELETE_BANK = "admin.delete.bank" ;
    public static final String UPLOAD_BANK_PHOTO = "admin.upload.bank.photo" ;




    public static final String CREATE_NEW_MIDTRANS_MEDIATOR = "admin.create.new.midtrans.mediator" ;
    public static final String UPDATE_MIDTRANS_MEDIATOR = "admin.update.midtrans.mediator" ;
    public static final String DELETE_MIDTRANS_MEDIATOR = "admin.delete.midtrans.mediator" ;
    public static final String UPLOAD_MIDTRANS_MEDIATOR_PHOTO = "admin.upload.midtrans.mediator.photo" ;


    public static final String CREATE_NEW_GROUP_FORM = "admin.create.new.group.form" ;
    public static final String UPDATE_GROUP_FORM = "admin.update.group.form" ;
    public static final String DELETE_GROUP_FORM = "admin.delete.group.form" ;


    public static final String CREATE_NEW_BANNER = "admin.create.new.banner" ;
    public static final String UPDATE_BANNER = "admin.update.banner" ;
    public static final String DELETE_BANNER = "admin.delete.banner" ;
    public static final String UPLOAD_BANNER_PHOTO = "admin.upload.banner.photo" ;


    /**
     * LABEL
    * */
    public static final String LOGIN_LABEL = "admin.login.label" ;
    public static final String LOGOUT_LABEL = "admin.logout.label" ;
    public static final String CHANGE_PASSWORD_LABEL = "admin.change.password.label" ;

    public static final String CREATE_NEW_ADMIN_LABEL = "admin.create.new.admin.label" ;
    public static final String UPDATE_ADMIN_LABEL = "admin.update.admin.label" ;
    public static final String DELETE_ADMIN_LABEL = "admin.delete.admin.label" ;
    public static final String UPDATE_PROFILE_LABEL = "admin.update.profile.label" ;
    public static final String UPLOAD_PHOTO_LABEL = "admin.upload.photo.label" ;


    /**
     * TODO COMPLETE MESSAGE PROPERTIES BELOW
     * */
    public static final String RESEND_PASSWORD_LABEL = "admin.resend.password.admin.label" ;
    public static final String CREATE_NEW_COURIER_LABEL = "admin.create.new.courier.label" ;
    public static final String UPDATE_COURIER_LABEL = "admin.update.courier.label" ;
    public static final String DELETE_COURIER_LABEL = "admin.delete.courier.label" ;
    public static final String UPLOAD_COURIER_PHOTO_LABEL = "admin.upload.courier.photo.label" ;

    public static final String CREATE_CONFIGURATION_LABEL = "admin.create.new.configuration.label" ;
    public static final String UPDATE_CONFIGURATION_LABEL = "admin.update.configuration.label" ;
    public static final String DELETE_CONFIGURATION_LABEL = "admin.delete.configuration.label" ;


    public static final String CREATE_NEW_COLOR_LABEL = "admin.create.new.color.label" ;
    public static final String UPDATE_COLOR_LABEL = "admin.update.color.label" ;
    public static final String DELETE_COLOR_LABEL = "admin.delete.color.label" ;

    public static final String CREATE_NEW_SIZE_LABEL = "admin.create.new.size.label" ;
    public static final String UPDATE_SIZE_LABEL = "admin.update.size.label" ;
    public static final String DELETE_SIZE_LABEL = "admin.delete.size.label" ;

    public static final String CREATE_NEW_BANK_ACCOUNT_LABEL = "admin.create.new.bank.account.label" ;
    public static final String UPDATE_BANK_ACCOUNT_LABEL = "admin.update.bank.account.label" ;
    public static final String DELETE_BANK_ACCOUNT_LABEL = "admin.delete.bank.account.label" ;

    public static final String CREATE_NEW_BANK_LABEL = "admin.create.new.bank.label" ;
    public static final String UPDATE_BANK_LABEL = "admin.update.bank.label" ;
    public static final String DELETE_BANK_LABEL = "admin.delete.bank.label" ;
    public static final String UPLOAD_BANK_PHOTO_LABEL = "admin.upload.bank.photo.label" ;

    public static final String CREATE_NEW_MIDTRANS_MEDIATOR_LABEL = "admin.create.new.midtrans.mediator.label" ;
    public static final String UPDATE_MIDTRANS_MEDIATOR_LABEL = "admin.update.midtrans.mediator.label" ;
    public static final String DELETE_MIDTRANS_MEDIATOR_LABEL = "admin.delete.midtrans.mediator.label" ;
    public static final String UPLOAD_MIDTRANS_MEDIATOR_PHOTO_LABEL = "admin.upload.midtrans.mediator.photo.label" ;


    public static final String CREATE_NEW_BANNER_LABEL = "admin.create.new.banner.label" ;
    public static final String UPDATE_BANNER_LABEL = "admin.update.banner.label" ;
    public static final String DELETE_BANNER_LABEL = "admin.delete.banner.label" ;
    public static final String UPLOAD_BANNER_PHOTO_LABEL = "admin.upload.banner.photo.label" ;

    public static final String CREATE_NEW_GROUP_FORM_LABEL = "admin.create.new.group.form.label" ;
    public static final String UPDATE_GROUP_FORM_LABEL = "admin.update.group.form.label" ;
    public static final String DELETE_GROUP_FORM_LABEL = "admin.delete.group.form.label" ;

    public static final String CHANGE_TASK_APPLICATION_STATUS = "admin.change.task.application.status" ;
    public static final String CHANGE_TASK_APPLICATION_STATUS_LABEL = "admin.change.task.application.status.label" ;

    public static final String PUBLISH_TASK = "admin.publish.task" ;
    public static final String PUBLISH_TASK_LABEL = "admin.publish.task.label" ;


    public static final String CREATE_TASK = "admin.create.task" ;
    public static final String CREATE_TASK_LABEL = "admin.create.task.label" ;

    public static final String UPDATE_TASK = "admin.update.task" ;
    public static final String UPDATE_TASK_LABEL = "admin.update.task.label" ;


    public static final String UP_TASK = "admin.up.task" ;
    public static final String UP_TASK_LABEL = "admin.up.task.label" ;


    public static List<AbstractMap.SimpleEntry> allTypeEntries(){
        List<AbstractMap.SimpleEntry> entries = new ArrayList<>();
        entries.add(new HashMap.SimpleEntry<>(LOGIN, LOGIN_LABEL));
        entries.add(new HashMap.SimpleEntry<>(LOGOUT, LOGOUT_LABEL));
        entries.add(new HashMap.SimpleEntry<>(CHANGE_PASSWORD, CHANGE_PASSWORD_LABEL));
        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_ADMIN, CREATE_NEW_ADMIN_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_ADMIN, UPDATE_ADMIN_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_PROFILE, UPDATE_PROFILE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_PHOTO, UPLOAD_PHOTO_LABEL));
        entries.add(new HashMap.SimpleEntry<>(RESEND_PASSWORD, RESEND_PASSWORD_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_CONFIGURATION, CREATE_CONFIGURATION_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_CONFIGURATION, UPDATE_CONFIGURATION_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_CONFIGURATION, DELETE_CONFIGURATION_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_COLOR, CREATE_NEW_COLOR_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_COLOR, UPDATE_COLOR_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_COLOR, DELETE_COLOR_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_BANK_ACCOUNT,CREATE_NEW_BANK_ACCOUNT_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_BANK_ACCOUNT, UPDATE_BANK_ACCOUNT_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_BANK_ACCOUNT, DELETE_BANK_ACCOUNT_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_BANK,CREATE_NEW_BANK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_BANK, UPDATE_BANK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_BANK, DELETE_BANK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_BANK_PHOTO, UPLOAD_BANK_PHOTO_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_MIDTRANS_MEDIATOR, CREATE_NEW_MIDTRANS_MEDIATOR_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_MIDTRANS_MEDIATOR, UPDATE_MIDTRANS_MEDIATOR_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_MIDTRANS_MEDIATOR, DELETE_MIDTRANS_MEDIATOR_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_MIDTRANS_MEDIATOR_PHOTO, UPLOAD_MIDTRANS_MEDIATOR_PHOTO_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_BANNER, CREATE_NEW_BANNER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_BANNER, UPDATE_BANNER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_BANNER, DELETE_BANNER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_BANNER_PHOTO, UPLOAD_BANNER_PHOTO_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_NEW_GROUP_FORM, CREATE_NEW_GROUP_FORM_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_GROUP_FORM, UPDATE_GROUP_FORM_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_GROUP_FORM, DELETE_GROUP_FORM_LABEL));


        entries.add(new HashMap.SimpleEntry<>(CHANGE_TASK_APPLICATION_STATUS, CHANGE_TASK_APPLICATION_STATUS_LABEL));

        entries.add(new HashMap.SimpleEntry<>(PUBLISH_TASK, PUBLISH_TASK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UP_TASK, UP_TASK_LABEL));

        entries.add(new HashMap.SimpleEntry<>(CREATE_TASK, CREATE_TASK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_TASK, UPDATE_TASK_LABEL));

        return entries ;
    }



    @Column(name = "type", nullable = false)
    private String type ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("adminActivities")
    @JoinColumn(name="admin_id", nullable = false)
    private Admin admin;

    @Type(type = "jsonb")
    @Column(name = "description_variables", columnDefinition = "jsonb")
    List<DescriptionVariable> descriptionVariables = new ArrayList<>();

    @Type( type = "jsonb" )
    @Column(columnDefinition = "jsonb")
    private JsonNode jsonObject;

    @Type( type = "jsonb" )
    @Column(columnDefinition = "jsonb")
    private JsonNode previousObject;


    @Transient
    public String getTypeView(){
        Locale locale = LocaleContextHolder.getLocale();
        return Constant.message.getMessage(type+".label", new Object[]{}, locale);
    }

    @Transient
    public String getMessage(){
        Locale locale = LocaleContextHolder.getLocale();
        if(descriptionVariables!=null){
            Object[] args = new Object[descriptionVariables.size()];
            Collections.sort(descriptionVariables, (descriptionVariable1, descriptionVariable2) -> {
                return descriptionVariable1.getIndex()-descriptionVariable2.getIndex();
            });
            for (int i = 0; i < descriptionVariables.size() ; i++) {
                args[i] = descriptionVariables.get(i).getValue();
            }
            return Constant.message.getMessage(type, args, locale);
        }
        return "(Unknown)";
    }

    public static AdminActivity build(Admin admin, String type, List<DescriptionVariable> descriptionVariables, Object object){
        AdminActivity customerActivity = new AdminActivity();
        customerActivity.setType(type);
        customerActivity.setAdmin(admin);
        customerActivity.setDescriptionVariables(descriptionVariables);
        JsonNode jsonNode = null ;
        if(object instanceof String){
            jsonNode = JacksonUtil.toJsonNode((String) object);
        }else{
            jsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(object));
        }
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
//                if(jsonNode1.getNodeType()==JsonNodeType.OBJECT){
//                    strings.add(s);
//                }
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }
            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });
            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            customerActivity.setJsonObject(objectNode);
        }
        return  customerActivity ;
    }

    public static AdminActivity build(Admin admin, String type, List<DescriptionVariable> descriptionVariables, Object object, Object previousObject){
        AdminActivity customerActivity = new AdminActivity();
        customerActivity.setType(type);
        customerActivity.setAdmin(admin);
        customerActivity.setDescriptionVariables(descriptionVariables);
        JsonNode currentJsonNode = null ;
        if(object instanceof String){
            currentJsonNode = JacksonUtil.toJsonNode((String) object);
        }else{
            currentJsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(object));
        }
        if (currentJsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) currentJsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
//                if(jsonNode1.getNodeType()==JsonNodeType.OBJECT){
//                    strings.add(s);
//                }
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }

            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });
            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            customerActivity.setJsonObject(objectNode);
        }

        JsonNode previousJsonNode = null ;
        if(object instanceof String){
            previousJsonNode = JacksonUtil.toJsonNode((String) previousObject);
        }else{
            previousJsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(previousObject));
        }
        if (previousJsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) previousJsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
//                if(jsonNode1.getNodeType()==JsonNodeType.OBJECT){
//                    strings.add(s);
//                }
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }

            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });
            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            customerActivity.setPreviousObject(objectNode);
        }



        return  customerActivity ;
    }



}
