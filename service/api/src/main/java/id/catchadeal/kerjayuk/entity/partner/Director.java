package id.catchadeal.kerjayuk.entity.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Table(name="j_director")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Director extends EBase {

    String name ;
    String phoneNumber ;

//    @OneToOne(fetch= FetchType.LAZY, mappedBy = "director")@JsonManagedReference
//    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("director")
//    Company company ;

    @Override
    public void initTransient() {
//        mergeActiveStatus(company);
        super.initTransient();

    }

}
