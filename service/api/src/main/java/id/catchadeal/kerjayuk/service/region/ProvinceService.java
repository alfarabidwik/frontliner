package id.catchadeal.kerjayuk.service.region;

import id.catchadeal.kerjayuk.entity.region.Province;
import id.catchadeal.kerjayuk.repository.region.ProvinceRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProvinceService extends BasicRepoService<Province> {

    @Autowired
    ProvinceRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Cacheable(cacheNames = "Provinces1")
    @Override
    public List<Province> findAll() {
        return repository.findAll();
    }

    @Cacheable(cacheNames = "Provinces2", key = "{#active, #sort}")
    public List<Province> findAll(Boolean active, Sort sort) {
        if(active!=null){
            return repository.findByActive(active, sort);
        }else{
            return repository.findAll(sort);
        }
    }

    @Cacheable(cacheNames = "Provinces3", key = "{#active, #page, #sortir, #ascending}")
    public Page<Province> findAll3(Boolean active, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActive(active, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findAll(PageRequest.of(page, pageRow, sort));
        }
    }


    @Cacheable(cacheNames = "Province", key = "#id")
//    @Override
    public Province findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public Province save(Province village) {
        return super.save(village);
    }


}
