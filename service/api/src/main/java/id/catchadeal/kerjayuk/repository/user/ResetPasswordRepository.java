package id.catchadeal.kerjayuk.repository.user;

import id.catchadeal.kerjayuk.entity.user.ResetPassword;
import id.catchadeal.kerjayuk.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ResetPasswordRepository extends JpaRepository<ResetPassword, Long> {


    Optional<ResetPassword> findByToken(String token);
    Boolean existsByToken(String token);

}
