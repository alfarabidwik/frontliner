package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.CorporateType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CorporateTypeRepository extends JpaRepository<CorporateType, Long> {

    @Query("select case when count(ct)> 0 then true else false end from CorporateType ct where lower(ct.name) like lower(:name)")
    Boolean existsByName(@Param("name") String name);


}
