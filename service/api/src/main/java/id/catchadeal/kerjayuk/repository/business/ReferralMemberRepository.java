package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.ReferralMember;
import id.catchadeal.kerjayuk.entity.business.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ReferralMemberRepository extends JpaRepository<ReferralMember, Long> {

}
