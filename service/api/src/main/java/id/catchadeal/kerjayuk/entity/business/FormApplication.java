package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name="pv_form_application", indexes = {
        @Index(columnList = "created", name = "FormApplication_created_idx"),
        @Index(columnList = "updated", name = "FormApplication_updated_idx"),
        @Index(columnList = "active", name = "FormApplication_active_idx"),
})
@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class FormApplication extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("formApplications")
    @JoinColumn(name="form_lane_application_id", nullable = false)
    FormLaneApplication formLaneApplication ;

    @Type(type = "jsonb")
    @Column(name = "form", columnDefinition = "jsonb")
    Form form ;

    Long valueId ;
    String value ;

    @Transient
    String imageUrl ;

    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isEmpty(imageUrl) && form.getType().equalsIgnoreCase(Form.IMAGE)){
            this.imageUrl = Constant.prefixApi+Constant.FORM_MEDIA_DIRECTORY+form.getImageDirectory()+"/"+value;
        }
    }
}
