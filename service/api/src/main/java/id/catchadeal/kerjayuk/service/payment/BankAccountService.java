package id.catchadeal.kerjayuk.service.payment;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.payment.BankAccount;
import id.catchadeal.kerjayuk.repository.payment.BankAccountRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class BankAccountService extends BasicRepoService<BankAccount> {

    @Autowired
    BankAccountRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<BankAccount> findAll() {
        return repository.findAll(new Sort(Sort.Direction.DESC, "updated"));
    }

    public List<BankAccount> findAll(Boolean active, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending? Sort.Direction.ASC:Sort.Direction.DESC, sortir);
        List<BankAccount> bankAccounts = new ArrayList<>();
        if(active!=null){
            bankAccounts = repository.findByActive(active, sort);
        }else{
            bankAccounts = repository.findAll(sort);
        }
        return bankAccounts;
    }


//    @Override
    public BankAccount findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public BankAccount save(BankAccount customer) {
        return super.save(customer);
    }





}
