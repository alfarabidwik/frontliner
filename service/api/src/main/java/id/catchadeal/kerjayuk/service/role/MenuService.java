package id.catchadeal.kerjayuk.service.role;

import id.catchadeal.kerjayuk.entity.role.Menu;
import id.catchadeal.kerjayuk.repository.role.MenuRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MenuService extends BasicRepoService<Menu> {

    @Autowired MenuRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public List<Menu> findAll(Boolean active) {
        if(active!=null){
            return repository.findByActive(active);
        }else{
            return repository.findAll();
        }
    }

}
