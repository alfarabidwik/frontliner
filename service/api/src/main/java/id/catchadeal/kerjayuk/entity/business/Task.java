package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.formbuilder.GroupForm;
import id.catchadeal.kerjayuk.entity.misc.TaskCategory;
import id.catchadeal.kerjayuk.entity.misc.WorkingDay;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import id.catchadeal.kerjayuk.entity.partner.Partner;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import id.catchadeal.kerjayuk.util.ServerUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@Data
@Table(name="j_task", indexes = {
        @Index(columnList = "title", name = "Task_title_idx", unique = false),
        @Index(columnList = "created", name = "task_created_idx"),
        @Index(columnList = "updated", name = "Task_updated_idx"),
        @Index(columnList = "active", name = "Task_active_idx"),
})
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Task extends EBase {


    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("tasks")
    @JoinColumn(name="partner_id", nullable = false)
    Partner partner ;

    @Column(nullable = false, columnDefinition = "TEXT")
    String title ;
    @Column(nullable = false, columnDefinition = "TEXT default ''")
    String subtitle = "";

    @Column(nullable = false, columnDefinition = "boolean default false")
    boolean published ;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    Integer validatingTimeInHour = 0;

    @Column(nullable = false, columnDefinition = "TEXT default ''")
    private String description ;

    @Column(nullable = false, columnDefinition = "TEXT default ''")
    private String additionalInfo ;

    @Column(nullable = false, columnDefinition = "TEXT default '"+ TaskDto.NEVER_END+"'")
    String periodType ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date startPeriod ;

    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date endPeriod ;

    @Column(nullable = false, columnDefinition = "TEXT default '"+TaskDto.NO_LIMIT+"'")
    String applicationLimitType ;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    Integer applicationLimitCount = 0 ;


    @Formula(value = "(SELECT COUNT(t.id) from j_task_application t WHERE t.task_id = id AND t.task_application_status_id = "+ TaskApplicationStatusDto.ACCEPTED_IDS +" )")
    Long totalCompletedTaskFormula ;

    @Column(nullable = false, columnDefinition = "INTEGER DEFAULT 0")
    Integer applicationCount = 0 ;

    @Column(nullable = false, columnDefinition = "TEXT default '"+TaskDto.NO_NEED_VERIFIED+"'")
    String workerVerificationType ;

    @Column(columnDefinition = "TEXT default ''")
    String bannerImage ;
    @Transient
    String bannerImageUrl ;

    @Column(columnDefinition = "numeric(19,2) DEFAULT 0")
    BigDecimal fee ;

    String taskAddressLink ;
    String callbackId ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("tasks")
    @JoinColumn(name="task_category_id", nullable = false)
    TaskCategory taskCategory;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("tasks")
    @JoinColumn(name="group_form_id")
    GroupForm groupForm ;

    Integer suggestion  ;


    @Column(nullable = false, columnDefinition = "TEXT default '"+TaskDto.REGULAR_TASK+"'")
    String type ;

    @Transient
    Map taskApplicationSummary = new HashMap();

    @Transient
    String[] statuses = new String[]{"waiting","accepted","rejected"};

    @Transient
    boolean applyable  ;
    @Transient
    String message = null ;

    @Override
    public void initTransient() {
        super.initTransient();

        taskApplicationSummary = new HashMap();
        for (int i = 0; i < statuses.length; i++) {
            taskApplicationSummary.put(statuses[i], 0);
        }

        if(StringUtils.isNotEmpty(bannerImage)){
            bannerImageUrl = Constant.prefixApi+ Constant.REST_TASK_BANNER_IMAGE+"/"+getId()+"/"+bannerImage;
        }

        if(totalCompletedTaskFormula!=null){
            this.applicationCount = totalCompletedTaskFormula.intValue();
        }
        if(StringUtils.isEmpty(type)){
            this.type = TaskDto.REGULAR_TASK;
        }
        checkIfApplyable();

//        if(taskApplications !=null && taskApplications.size()>0){
//            taskApplications.forEach(taskApplication -> {
//                if(taskApplication.getTaskApplicationStatus()!=null && taskApplication.getTaskApplicationStatus().getName()!=null){
//                    String statusName = taskApplication.getTaskApplicationStatus().getName().toLowerCase();
//                    if(taskApplicationSummary.containsKey(statusName)){
//                        int taskApplicationSize = (int) taskApplicationSummary.get(statusName);
//                        taskApplicationSize = taskApplicationSize+1;
//                        taskApplicationSummary.put(statusName, taskApplicationSize);
//                    }else{
//                        taskApplicationSummary.put(statusName, 1);
//                    }
//                }
//            });
//        }
    }

    private void checkIfApplyable(){
        applyable = true ;
        if(!published){
            applyable = false ;
            message = ServerUtils.message.getMessage("task.is.already.unpublish", new Object[]{}, Locale.getDefault());
            return;
        }
        if(StringUtils.isNotEmpty(periodType)){
            long currentTime = new Date().getTime();
            if(periodType.equalsIgnoreCase(TaskDto.PERIODIC) && startPeriod.getTime()>currentTime){
                applyable = false ;
                message = ServerUtils.message.getMessage("task.is.not.yet.been.published", new Object[]{}, Locale.getDefault());
                return;
            }
            if(periodType.equalsIgnoreCase(TaskDto.PERIODIC) && endPeriod.getTime()<currentTime){
                applyable = false ;
                message = ServerUtils.message.getMessage("task.is.has.been.expired", new Object[]{}, Locale.getDefault());
                return;
            }
        }
        if(StringUtils.isNotEmpty(applicationLimitType)){
            if(applicationLimitType.equalsIgnoreCase(TaskDto.LIMITED) && applicationLimitCount<=applicationCount){
                applyable = false ;
                message = ServerUtils.message.getMessage("task.application.is.reaching.max.limit", new Object[]{}, Locale.getDefault());
                return;
            }
        }

    }







}
