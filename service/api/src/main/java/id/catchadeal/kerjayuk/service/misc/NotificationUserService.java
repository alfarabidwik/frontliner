package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.NotificationUser;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.misc.NotificationUserRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NotificationUserService extends BasicRepoService<NotificationUser> {

    Logger logger = LoggerFactory.getLogger(NotificationUserService.class);

    @Autowired
    private NotificationUserRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public Page<NotificationUser> findByUserId(Long userId, Pageable pageable){
        return repository.findByUserId(userId, pageable);
    }

    public DataPage<NotificationUser> notificationUsers(User user, String topics, String types, String topicExcludes, String typeExcludes, Integer page, String sortir, Boolean ascending){

        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sqlCount = "SELECT COUNT(nu.id) ";
        String sqlObject = "SELECT nu ";

        String sqlFrom = " FROM NotificationUser nu WHERE 1+1 = 2 ";

        if(user!=null){
            sqlFrom = sqlFrom+" AND nu.user.id = :userId ";
            paramaterMap.put("userId", user.getId());
        }
        if(StringUtils.isNotEmpty(topics)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = topics.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(topics.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(topics).append("'");
            }
            sqlFrom = sqlFrom+" AND nu.notification.topic IN ( "+stringBuilder.toString()+" ) ";
        }
        if(StringUtils.isNotEmpty(types)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = types.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(types.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(types).append("'");
            }
            sqlFrom = sqlFrom+" AND nu.notification.type IN ("+stringBuilder.toString()+") ";
        }

        if(StringUtils.isNotEmpty(topicExcludes)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = topicExcludes.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(topicExcludes.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(topicExcludes).append("'");
            }
            sqlFrom = sqlFrom+" AND nu.notification.topic NOT IN ( "+stringBuilder.toString()+" ) ";
        }
        if(StringUtils.isNotEmpty(typeExcludes)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = typeExcludes.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(typeExcludes.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(typeExcludes).append("'");
            }
            sqlFrom = sqlFrom+" AND nu.notification.type NOT IN ("+stringBuilder.toString()+") ";
        }

        sqlFrom = sqlFrom+" GROUP BY nu.id, nu.created ";
        sqlFrom = sqlFrom+" ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");

        String finalSqlCount = sqlCount+sqlFrom;
        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(finalSqlCount);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            countResult = 0 ;
        }

        String finalSqlObject = sqlObject+sqlFrom;
        logger.debug("SQL NOTIFICATION {}  ", finalSqlObject);
        Query queryObject  = entityManager.createQuery(finalSqlObject);
        queryObject.setFirstResult(offset);
        queryObject.setMaxResults(pageRow);
        for(String key :paramaterMap.keySet()) {
            queryObject.setParameter(key, paramaterMap.get(key));
        }

        List<NotificationUser> objects = queryObject.getResultList();

        DataPage dataPage = DataPage.builder(objects, pageRow, countResult);
        return dataPage;
    }


    public Long countUnreadNotification(User user, String topics, String types, String topicExcludes, String typeExcludes){

        Map<String, Object> paramaterMap = new HashMap<String, Object>();
        String sql = "SELECT COUNT(nu.id) FROM NotificationUser nu WHERE nu.read = false ";
        paramaterMap.put("userId", user.getId());
        if(user!=null){
            sql = sql+" AND nu.user.id = :userId ";
            paramaterMap.put("userId", user.getId());
        }
        if(StringUtils.isNotEmpty(topics)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = topics.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(topics.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(topics).append("'");
            }
            sql = sql+" AND nu.notification.topic IN ( "+stringBuilder.toString()+" ) ";
        }
        if(StringUtils.isNotEmpty(types)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = types.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(types.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(types).append("'");
            }
            sql = sql+" AND nu.notification.type IN ("+stringBuilder.toString()+") ";
        }

        if(StringUtils.isNotEmpty(topicExcludes)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = topicExcludes.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(topicExcludes.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(topicExcludes).append("'");
            }
            sql = sql+" AND nu.notification.topic NOT IN ( "+stringBuilder.toString()+" ) ";
        }
        if(StringUtils.isNotEmpty(typeExcludes)){
            StringBuilder stringBuilder = new StringBuilder();
            int length = typeExcludes.split(",").length;
            if(length>1){
                for (int i = 0; i < length; i++) {
                    stringBuilder.append("'").append(typeExcludes.split(",")[i]).append("'");
                    if(i<length-1){
                        stringBuilder.append(",");
                    }
                }
            }else{
                stringBuilder.append("'").append(typeExcludes).append("'");
            }
            sql = sql+" AND nu.notification.type NOT IN ("+stringBuilder.toString()+") ";
        }

        Query queryObject  = entityManager.createQuery(sql);
        for(String key :paramaterMap.keySet()) {
            queryObject.setParameter(key, paramaterMap.get(key));
        }

        Long count = (Long) queryObject.getSingleResult();
        return count ;

    }

}