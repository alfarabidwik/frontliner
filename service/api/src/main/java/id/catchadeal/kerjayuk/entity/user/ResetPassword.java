package id.catchadeal.kerjayuk.entity.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name=ResetPassword.TABLE)
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class ResetPassword extends EBase {

    public static final String TABLE = "j_reset_password";

    String token ;
    boolean used ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("user")
    @JoinColumn(name="user_id", nullable = false, updatable = false)
    User user ;
}
