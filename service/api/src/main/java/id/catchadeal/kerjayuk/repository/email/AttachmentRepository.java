package id.catchadeal.kerjayuk.repository.email;

import id.catchadeal.kerjayuk.entity.email.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {

}
