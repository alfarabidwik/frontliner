package id.catchadeal.kerjayuk.repository.region;

import id.catchadeal.kerjayuk.entity.region.Village;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VillageRepository extends JpaRepository<Village, Long> {


    List<Village> findByDistrictId(Long districtId, Sort sort);
    Page<Village> findByDistrictId(Long districtId, Pageable pageable);

    List<Village> findByActiveAndDistrictId(Boolean active, Long districtId, Sort sort);
    Page<Village> findByActiveAndDistrictId(Boolean active, Long districtId, Pageable pageable);

    List<Village> findByActive(Boolean active, Sort sort);
    Page<Village> findByActive(Boolean active, Pageable pageable);


}
