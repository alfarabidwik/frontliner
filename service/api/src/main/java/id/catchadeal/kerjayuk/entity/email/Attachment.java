package id.catchadeal.kerjayuk.entity.email;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.util.Constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name= SendEmail.TABLE)
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class Attachment extends EBase {

    public static final String TABLE = "j_attachment";
    private String filename ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="send_email_id")
    private SendEmail sendEmail ;


    @Transient
    private String fileLink ;

    @Override
    public void initTransient() {
        super.initTransient();
        if(filename!=null&& !filename.isEmpty() && sendEmail!=null){
            fileLink  = Constant.prefixApi+ Constant.REST_EMAIL_ATTACHMENT_FILE+"/"+sendEmail.getId()+"/"+filename;
        }
    }


}
