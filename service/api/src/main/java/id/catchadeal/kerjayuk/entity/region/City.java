package id.catchadeal.kerjayuk.entity.region;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "m_city")
@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class City extends EBase implements Serializable {

    String name ;
    String platNumber ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="province_id")
    Province province ;

    @OneToMany(mappedBy="city", fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SELECT) @JsonIgnoreProperties("city")
    List<District> districts ;

    String courierId ;

    @Override
    public void initTransient() {
        super.initTransient();

    }

}
