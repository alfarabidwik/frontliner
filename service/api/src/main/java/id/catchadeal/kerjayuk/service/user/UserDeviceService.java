package id.catchadeal.kerjayuk.service.user;

import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.user.UserDeviceRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.apache.commons.lang.StringUtils;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import javax.persistence.Query;
import java.beans.Transient;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserDeviceService extends BasicRepoService<UserDevice> {

    @Autowired private UserDeviceRepository repository ;
    @Autowired UserService userService;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public List<UserDevice> findByUserId(Long userId){
        return repository.findByUserId(userId);
    }


    @Transactional
    public UserDevice replaceByNewDevice(Long userId, String newPlatform, String newFcmToken, String newDeviceId){
        User user = userService.findById(userId);
        List<UserDevice> userDevices= findByDeviceId(newDeviceId, Sort.by(Sort.Direction.ASC, "created"));
        userDevices.forEach(userDevice -> {
            delete(userDevice.getId());
        });

        UserDevice userDevice = new UserDevice();
        userDevice.setPlatform(newPlatform);
        userDevice.setFcmToken(newFcmToken);
        userDevice.setDeviceId(newDeviceId);
        userDevice.setUser(user);
        userDevice = save(userDevice);
        return userDevice ;

    }

    @Transient
    public void deleteDevice(String deviceId){
        List<UserDevice> userDevices= findByDeviceId(deviceId, Sort.by(Sort.Direction.ASC, "created"));
        userDevices.forEach(userDevice -> {
            delete(userDevice.getId());
        });

    }

    public DataPage findAll(String search, Long userId, Integer page, Boolean ascending, String sortir){
        Map<String, Object> parameterMap = new HashMap<>();
        String fetchedObject = " SELECT ud " ;
        String fetchedCount = " SELECT ud " ;
        String from = " FROM UserDevice ud LEFT JOIN FETCH ud.user u WHERE 1+1 = 2 " ;
        if(StringUtils.isNotEmpty(search)){
            from = from+" AND ( " +
                    " (lower(concat(coalesce(u.firstname, ''), ' ', coalesce(u.lastname, ''))) LIKE CONCAT('%',LOWER(:search),'%')) " +
                    " OR (lower(coalesce(u.email, '')) LIKE CONCAT('%',LOWER(:search),'%')) " +
                    " OR (lower(coalesce(u.mobilePhone, '')) LIKE CONCAT('%',LOWER(:search),'%')) " +
                    "OR (lower(coalesce(u.agentCode, '')) LIKE CONCAT('%',LOWER(:search),'%')) " +
                    ") ";
            parameterMap.put("search", search);
        }
        if(userId!=null){
            from = from+" AND u.id = :userId ";
            parameterMap.put("userId", userId);
        }
        String group = " GROUP BY ud.id, u.id " ;
        String orderBy = " ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");

        String finalSql = fetchedObject+from+group+orderBy;
        String finalSqlCount = fetchedCount+from+group+orderBy;
        int offset = 0 ;
        long countResult = 0 ;
        if(page!=null){
            offset = page*pageRow;
            try {
                Query queryTotal = entityManager.createQuery(finalSqlCount);
                for(String key :parameterMap.keySet()) {
                    queryTotal.setParameter(key, parameterMap.get(key));
                }
                countResult = (long) queryTotal.getResultList().size();
            }catch (Exception e){
                e.printStackTrace();
                countResult = 0 ;
            }
        }
        Query query = entityManager.createQuery(finalSql);
        if(page!=null){
            query.setFirstResult(offset);
            query.setMaxResults(pageRow);
        }
        for(String key :parameterMap.keySet()) {
            query.setParameter(key, parameterMap.get(key));
        }

        List<UserDevice> userDevices  = query.getResultList();

        return DataPage.builder(userDevices, pageRow, countResult);

    }

    public List<UserDevice> findByDeviceIdAndUserId(String deviceId, Long userId){
        return repository.findByDeviceIdAndUserId(deviceId, userId);
    }

    public List<UserDevice> findByFcmToken(String fcmToken){
        return repository.findByFcmToken(fcmToken);
    }


    public List<UserDevice> findByDeviceId(String deviceId, Sort sort){
        return repository.findByDeviceId(deviceId,sort);
    }

    public List<UserDevice> findByDeviceIdAndUserId(String deviceId, Long userId, Sort sort){
        return repository.findByDeviceIdAndUserId(deviceId, userId, sort);
    }

}
