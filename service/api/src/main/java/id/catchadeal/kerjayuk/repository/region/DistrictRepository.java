package id.catchadeal.kerjayuk.repository.region;

import id.catchadeal.kerjayuk.entity.region.District;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Long> {

    List<District> findByCityId(Long cityId, Sort sort);
    Page<District> findByCityId(Long cityId, Pageable pageable);

    List<District> findByActiveAndCityId(Boolean active, Long cityId, Sort sort);
    Page<District> findByActiveAndCityId(Boolean active, Long cityId, Pageable pageable);

    List<District> findByActive(Boolean active, Sort sort);
    Page<District> findByActive(Boolean active, Pageable pageable);


}
