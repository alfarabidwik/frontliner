package id.catchadeal.kerjayuk.repository.formbuilder;

import id.catchadeal.kerjayuk.entity.formbuilder.GroupForm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupFormRepository extends JpaRepository<GroupForm, Long> {

}
