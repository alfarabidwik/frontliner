package id.catchadeal.kerjayuk.service.user;

import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.repository.user.RegistrationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService extends BasicRepoService<Registration> {

    @Autowired private RegistrationRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }


    public Registration register(String firstname , String lastname , String mobilePhone , String email , String referralCode, String agentCode, String executor){
        Registration registration = new Registration();
        registration.setFirstname(firstname);
        registration.setLastname(lastname);
        registration.setMobilePhone(mobilePhone);
        registration.setEmail(email);
        registration.setReferralCode(referralCode);
        registration.setExecutor(executor);
        registration.setAgentCode(agentCode);
        registration = save(registration);
        return registration ;
    }

    public Page<Registration> findByKeywordLike(String keyword, PageRequest pageRequest){
        return repository.findByKeywordLike(keyword, pageRequest);
    }




}
