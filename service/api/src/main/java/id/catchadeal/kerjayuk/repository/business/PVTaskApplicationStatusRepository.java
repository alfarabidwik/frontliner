package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.PVTaskApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PVTaskApplicationStatusRepository extends JpaRepository<PVTaskApplicationStatus, Long> {

}
