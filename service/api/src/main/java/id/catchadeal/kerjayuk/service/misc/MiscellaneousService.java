package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.util.Constant;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class MiscellaneousService {
    @Value("${verification.source}") String verificationSource ;
    String numberSource = "1234567890";


    public String random30Code(){
        Long currentTime = new Date().getTime();
        String currentTimeString = String.valueOf(currentTime);
        String referenceCode = RandomStringUtils.random(30, verificationSource)+currentTimeString;
        return referenceCode ;
    }

    public String random6Code(boolean timestamp){
        String currentTimeString = "";
        if(timestamp){
            Long currentTime = new Date().getTime();
            currentTimeString = String.valueOf(currentTime);
        }
        String referenceCode = RandomStringUtils.random(6, verificationSource)+currentTimeString;
        return referenceCode ;
    }

    public String random8Code(boolean timestamp){
        String currentTimeString = "";
        if(timestamp){
            Long currentTime = new Date().getTime();
            currentTimeString = String.valueOf(currentTime);
        }
        String referenceCode = RandomStringUtils.random(8, verificationSource)+currentTimeString;
        return referenceCode ;
    }


    public String randomCode(int count){
        if(count<=0){
            throw new AppException(Constant.FAILED_CODE, "Count cannot be <= 0");
        }
        String referenceCode = RandomStringUtils.random(count, verificationSource);
        return referenceCode ;
    }

    public BigDecimal randomAmount(int count){
        if(count<=0){
            throw new AppException(Constant.FAILED_CODE, "Count cannot be <= 0");
        }
        String referenceCode = RandomStringUtils.random(count, numberSource);
        return new BigDecimal(Integer.valueOf(referenceCode)) ;
    }


    public String randomAgentCode(){
        Long currentTime = new Date().getTime();
        String referenceCode = RandomStringUtils.random(5, verificationSource);
        return referenceCode ;

    }




}
