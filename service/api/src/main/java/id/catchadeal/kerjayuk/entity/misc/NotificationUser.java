package id.catchadeal.kerjayuk.entity.misc;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.misc.NotificationData;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="j_notification_user")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class NotificationUser extends EBase {


    @ManyToOne(fetch= FetchType.LAZY)
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("notifications")
    @JoinColumn(name="user_id", nullable = false)
    User user ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("notificationUsers")
    @JoinColumn(name="notification_id", nullable = false)
    Notification notification ;

    @Column(columnDefinition = "BOOLEAN DEFAULT false", nullable = false)
    Boolean read ;

    @Override
    public void initTransient() {
        super.initTransient();
        if(read==null){
            read = false ;
        }
    }



}
