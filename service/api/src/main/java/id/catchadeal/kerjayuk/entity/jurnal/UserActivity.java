package id.catchadeal.kerjayuk.entity.jurnal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;

import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.*;
import java.util.*;

@Data
@Table(name="j_user_activity")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class UserActivity extends EBase {


    /**
     * Description
    * */

    public static final String LOGIN = "user.login" ;
    public static final String LOGIN_FACEBOOK = "user.login.facebook" ;
    public static final String LOGIN_GOOGLE = "user.login.google" ;
    public static final String LOGOUT = "user.logout" ;

    public static final String APPLY_MOBILE_PHONE_AND_LOGIN = "apply.mobile.phone.and.login" ;

    public static final String REGISTER = "user.register" ;
    public static final String REGISTER_FACEBOOK = "user.register.facebook" ;
    public static final String REGISTER_GOOGLE = "user.register.google" ;

    public static final String FORGET_PASSWORD = "user.forget.password" ;
    public static final String RESET_PASSWORD = "user.reset.password" ;
    public static final String CHANGE_PASSWORD = "user.change.password" ;

    public static final String VERIFICATION = "user.verification" ;

    public static final String ADD_NEW_CART = "user.add.new.cart" ;
    public static final String DELETE_CART = "user.delete.cart" ;

    public static final String ADD_NEW_FAVORITE = "user.add.new.favorite" ;
    public static final String DELETE_FAVORITE = "user.delete.favorite" ;

    public static final String ADD_NEW_ADDRESS = "user.add.new.address" ;
    public static final String DELETE_ADDRESS = "user.delete.address" ;
    public static final String UPDATE_ADDRESS = "user.update.address" ;


    public static final String UPDATE_PROFILE = "user.update.profile" ;
    public static final String UPLOAD_PHOTO_PROFILE = "user.upload.photo.profile" ;
    public static final String UPLOAD_ID_CARD = "user.upload.id.card" ;
    public static final String UPLOAD_SELFIE_ID_CARD = "user.upload.selfie.id.card" ;



    public static final String EXCHANGE_POIN_VOUCHER = "user.exchange.poin.voucher" ;
    public static final String ORDER = "user.order" ;
    public static final String FINISHED_MIDTRANS_SNAP = "user.finishing.midtrans.snap" ;
    public static final String DENIED_MIDTRANS_SNAP = "user.denied.midtrans.snap" ;
    public static final String PENDING_MIDTRANS_SNAP = "user.pending.midtrans.snap" ;


    public static final String APPLY_TASK = "user.apply.task";
    public static final String APPLY_TASK_LABEL = "user.apply.task.label";

    public static final String COMMIT_TASK = "user.commit.task";
    public static final String COMMIT_TASK_LABEL = "user.commit.task.label";




    /**
     * Message
    * */

    public static final String LOGIN_LABEL = "user.login.label" ;
    public static final String LOGIN_FACEBOOK_LABEL = "user.login.facebook.label" ;
    public static final String LOGIN_GOOGLE_LABEL = "user.login.google.label" ;
    public static final String LOGOUT_LABEL = "user.logout.label" ;

    public static final String APPLY_MOBILE_PHONE_AND_LOGIN_LABEL = "apply.mobile.phone.and.login.label" ;

    public static final String REGISTER_LABEL = "user.register.label" ;
    public static final String REGISTER_FACEBOOK_LABEL = "user.register.facebook.label" ;
    public static final String REGISTER_GOOGLE_LABEL = "user.register.google.label" ;

    public static final String FORGET_PASSWORD_LABEL = "user.forget.password.label" ;
    public static final String RESET_PASSWORD_LABEL = "user.reset.password.label" ;
    public static final String CHANGE_PASSWORD_LABEL = "user.change.password.label" ;

    public static final String VERIFICATION_LABEL = "user.verification.label" ;

    public static final String ADD_NEW_CART_LABEL = "user.add.new.cart.label" ;
    public static final String DELETE_CART_LABEL = "user.delete.cart.label" ;


    public static final String ADD_NEW_FAVORITE_LABEL = "user.add.new.favorite.label" ;
    public static final String DELETE_FAVORITE_LABEL = "user.delete.favorite.label" ;

    public static final String ADD_NEW_ADDRESS_LABEL = "user.add.new.address.label" ;
    public static final String DELETE_ADDRESS_LABEL = "user.delete.address.label" ;
    public static final String UPDATE_ADDRESS_LABEL = "user.update.address.label" ;


    public static final String UPDATE_PROFILE_LABEL = "user.update.profile.label" ;
    public static final String UPLOAD_PHOTO_PROFILE_LABEL = "user.upload.photo.profile.label" ;
    public static final String UPLOAD_ID_CARD_LABEL = "user.upload.id.card.label" ;
    public static final String UPLOAD_SELFIE_ID_CARD_LABEL = "user.upload.selfie.id.card.label" ;


    public static final String EXCHANGE_POIN_VOUCHER_LABEL = "user.exchange.poin.voucher.label" ;
    public static final String ORDER_LABEL = "user.order.label" ;
    public static final String FINISHED_MIDTRANS_SNAP_LABEL = "user.finishing.midtrans.snap.label" ;
    public static final String DENIED_MIDTRANS_SNAP_LABEL = "user.denied.midtrans.snap.label" ;
    public static final String PENDING_MIDTRANS_SNAP_LABEL = "user.pending.midtrans.snap.label" ;




    public static List<AbstractMap.SimpleEntry> allTypeEntries(){
        List<AbstractMap.SimpleEntry> entries = new ArrayList<>();
        entries.add(new HashMap.SimpleEntry<>(LOGIN, LOGIN_LABEL));
        entries.add(new HashMap.SimpleEntry<>(LOGIN_FACEBOOK, LOGIN_FACEBOOK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(LOGIN_GOOGLE, LOGIN_GOOGLE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(LOGOUT, LOGOUT_LABEL));

        entries.add(new HashMap.SimpleEntry<>(APPLY_MOBILE_PHONE_AND_LOGIN, APPLY_MOBILE_PHONE_AND_LOGIN_LABEL));

        entries.add(new HashMap.SimpleEntry<>(REGISTER, REGISTER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(REGISTER_FACEBOOK, REGISTER_FACEBOOK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(REGISTER_GOOGLE, REGISTER_GOOGLE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(VERIFICATION, VERIFICATION_LABEL));
        entries.add(new HashMap.SimpleEntry<>(FORGET_PASSWORD, FORGET_PASSWORD_LABEL));
        entries.add(new HashMap.SimpleEntry<>(RESET_PASSWORD, RESET_PASSWORD_LABEL));
        entries.add(new HashMap.SimpleEntry<>(CHANGE_PASSWORD, CHANGE_PASSWORD_LABEL));

        entries.add(new HashMap.SimpleEntry<>(ADD_NEW_CART, ADD_NEW_CART_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_CART, DELETE_CART_LABEL));

        entries.add(new HashMap.SimpleEntry<>(ADD_NEW_FAVORITE, ADD_NEW_FAVORITE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_FAVORITE, DELETE_FAVORITE_LABEL));

        entries.add(new HashMap.SimpleEntry<>(ADD_NEW_ADDRESS, ADD_NEW_ADDRESS_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DELETE_ADDRESS, DELETE_ADDRESS_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPDATE_ADDRESS, UPDATE_ADDRESS_LABEL));

        entries.add(new HashMap.SimpleEntry<>(UPDATE_PROFILE, UPDATE_PROFILE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_PHOTO_PROFILE, UPLOAD_PHOTO_PROFILE_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_ID_CARD, UPLOAD_ID_CARD_LABEL));
        entries.add(new HashMap.SimpleEntry<>(UPLOAD_SELFIE_ID_CARD, UPLOAD_SELFIE_ID_CARD_LABEL));

        entries.add(new HashMap.SimpleEntry<>(EXCHANGE_POIN_VOUCHER, EXCHANGE_POIN_VOUCHER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(ORDER, ORDER_LABEL));
        entries.add(new HashMap.SimpleEntry<>(FINISHED_MIDTRANS_SNAP, FINISHED_MIDTRANS_SNAP_LABEL));
        entries.add(new HashMap.SimpleEntry<>(DENIED_MIDTRANS_SNAP, DENIED_MIDTRANS_SNAP_LABEL));
        entries.add(new HashMap.SimpleEntry<>(PENDING_MIDTRANS_SNAP, PENDING_MIDTRANS_SNAP_LABEL));


        entries.add(new HashMap.SimpleEntry<>(APPLY_TASK, APPLY_TASK_LABEL));
        entries.add(new HashMap.SimpleEntry<>(COMMIT_TASK, COMMIT_TASK_LABEL));


        return entries ;
    }


    @Column(name = "type", nullable = false)
    private String type ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)  @JsonIgnoreProperties("userActivities")
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @Type(type = "jsonb")
    @Column(name = "description_variables", columnDefinition = "jsonb")
    List<DescriptionVariable> descriptionVariables = new ArrayList<>();

    @Type( type = "jsonb" )
    @Column(columnDefinition = "jsonb")
    private JsonNode jsonObject;

    @Type( type = "jsonb" )
    @Column(columnDefinition = "jsonb")
    private JsonNode previousObject;


    @Transient
    public String getTypeView(){
        Locale locale = LocaleContextHolder.getLocale();
        return Constant.message.getMessage(type+".label", new Object[]{}, locale);
    }


    @Transient
    public String getMessage(){
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[descriptionVariables.size()];
        Collections.sort(descriptionVariables, (descriptionVariable1, descriptionVariable2) -> {
            return descriptionVariable1.getIndex()-descriptionVariable2.getIndex();
        });
        for (int i = 0; i < descriptionVariables.size() ; i++) {
            args[i] = descriptionVariables.get(i).getValue();
        }
        return Constant.message.getMessage(type, args, locale);

    }

    public static UserActivity build(User user, String type, List<DescriptionVariable> descriptionVariables, Object object){
        UserActivity userActivity = new UserActivity();
        userActivity.setType(type);
        userActivity.setUser(user);
        userActivity.setDescriptionVariables(descriptionVariables);
        JsonNode jsonNode = null ;
        if(object instanceof String){
            jsonNode = JacksonUtil.toJsonNode((String) object);
        }else{
            jsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(object));
        }
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) jsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }
            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });

            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            userActivity.setJsonObject(objectNode);
        }

        return userActivity;
    }

    public static UserActivity build(User user, String type, List<DescriptionVariable> descriptionVariables, Object object, Object previousObject){
        UserActivity userActivity = new UserActivity();
        userActivity.setType(type);
        userActivity.setUser(user);
        userActivity.setDescriptionVariables(descriptionVariables);
        JsonNode currentJsonNode = null ;
        if(object instanceof String){
            currentJsonNode = JacksonUtil.toJsonNode((String) object);
        }else{
            currentJsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(object));
        }
        if (currentJsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) currentJsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
//                if(jsonNode1.getNodeType()==JsonNodeType.OBJECT){
//                    strings.add(s);
//                }
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }
            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });

            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            userActivity.setJsonObject(objectNode);
        }

        JsonNode previousJsonNode = null ;
        if(object instanceof String){
            previousJsonNode = JacksonUtil.toJsonNode((String) previousObject);
        }else{
            previousJsonNode = JacksonUtil.toJsonNode(JacksonUtil.toString(previousObject));
        }
        if (previousJsonNode instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) previousJsonNode;
            List<String> strings = new ArrayList<>();
            objectNode.fieldNames().forEachRemaining(s -> {
                JsonNode jsonNode1 = objectNode.get(s);
//                if(jsonNode1.getNodeType()== JsonNodeType.OBJECT){
//                    strings.add(s);
//                }
                if(jsonNode1.getNodeType()==JsonNodeType.NULL){
                    strings.add(s);
                }
            });
            strings.forEach(s -> {
                objectNode.remove(s);
            });
            objectNode.remove("password");
            objectNode.remove("oldPassword");
            objectNode.remove("newPassword");
            objectNode.remove("confirmPassword");
            objectNode.remove("authorization");
            userActivity.setPreviousObject(objectNode);
        }


        return userActivity;
    }
}
