package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.Notification;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.service.misc.NotificationService;
import id.catchadeal.kerjayuk.service.user.UserDeviceService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.LongStream;

@RestController
@RequestMapping(path = "${api}")
public class NotificationController extends BasicController {

    Logger logger = LoggerFactory.getLogger(NotificationController.class);

    @Autowired NotificationService notificationService;
    @Autowired UserDeviceService userDeviceService ;

    @GetMapping(path = "/notifications")
    @AdminRest@FrontendRest
    public WSResponse findAll(
            @RequestHeader(value = Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "true") Boolean createByAdmin,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "created") String sortir) {
        DataPage<Notification> notificationDataPage = notificationService.notifications(createByAdmin, page, sortir, ascending);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notificationDataPage.getDatas()).setTotalPage(notificationDataPage.getTotalPage())
                .setTotalElement(notificationDataPage.getTotalElement()).setPageElement(notificationDataPage.getPageElement());
    }


    @GetMapping(path = "/notification/{id}")
    @Override
    public WSResponse findById(@PathVariable Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notificationService.findById(id));
    }

    @PostMapping(path = "/notification/send")
    @UniversalRest
    public WSResponse notificationToUser(
            @RequestHeader(value = Constant.AUTHORIZATION) String authorization,
            @RequestParam(required = false) String userIds,
            @ApiParam(allowableValues = NotificationDto.NOTIFICATION_TOPICS_STRING, required = true) @RequestParam String topic,
            @RequestParam String title,
            @RequestParam String message,
            @RequestParam(required = false) String addressLink) {

        Admin admin = jwtTokenProvider.getAdmin(authorization);
        Notification notification = null ;
        if(topic.equalsIgnoreCase(NotificationDto.SPECIFIC)){
            List<String> userIdList = Arrays.asList(userIds.split(","));
            List<Long> userIdLongList = new ArrayList<>();
            userIdList.forEach(s -> userIdLongList.add(Long.valueOf(s)));
            List<User> users = new ArrayList<>();
            userIdLongList.forEach(aLong -> {
                users.add(userService.findById(aLong));
            });
            notification = notificationService.sendSpesific(admin, users,  title, message, null, null, null);
        }else{
            notification = notificationService.sendBroadcast(admin, topic, null, title, message, null, null, addressLink, null);
        }

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, notification);
    }

    @GetMapping(path = "/notification/delete")
    @Override
    @AdminRest
    public WSResponse delete(@RequestParam Long id) {
        notificationService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }


}