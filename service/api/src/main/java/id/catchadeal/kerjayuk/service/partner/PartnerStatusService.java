package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.PartnerStatus;
import id.catchadeal.kerjayuk.repository.partner.PartnerStatusRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PartnerStatusService extends BasicRepoService<PartnerStatus> {

    @Autowired private PartnerStatusRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public void firstGenerate(){

    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }
}
