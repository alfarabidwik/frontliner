package id.catchadeal.kerjayuk.repository.user;

import id.catchadeal.kerjayuk.entity.user.Registration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {

    @Query("SELECT r FROM Registration r WHERE " +
            "lower(r.agentCode) LIKE (CONCAT('%',:keyword, '%')) OR " +
            "lower(r.email) LIKE (CONCAT('%',:keyword, '%')) OR " +
            "lower(r.mobilePhone) LIKE (CONCAT('%',:keyword, '%')) OR " +
            "lower(concat(r.firstname, ' ', coalesce(r.lastname, ''))) LIKE (CONCAT('%',:keyword, '%'))")
    Page<Registration> findByKeywordLike(@Param("keyword") String keyword, Pageable pageable);

}
