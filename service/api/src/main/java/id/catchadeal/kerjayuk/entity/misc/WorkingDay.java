package id.catchadeal.kerjayuk.entity.misc;

import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_working_day")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class WorkingDay extends EBase {

    String name ;

    @Override
    public void initTransient() {
        super.initTransient();
    }

}
