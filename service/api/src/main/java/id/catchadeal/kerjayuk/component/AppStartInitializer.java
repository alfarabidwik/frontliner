package id.catchadeal.kerjayuk.component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.catchadeal.kerjayuk.entity.business.Task;
import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import id.catchadeal.kerjayuk.entity.misc.Configuration;
import id.catchadeal.kerjayuk.entity.misc.TaskCategory;
import id.catchadeal.kerjayuk.entity.misc.WorkingDay;
import id.catchadeal.kerjayuk.entity.partner.*;
import id.catchadeal.kerjayuk.entity.psychotest.Personality;
import id.catchadeal.kerjayuk.entity.psychotest.PersonalityCharacter;
import id.catchadeal.kerjayuk.entity.report.ReportStatus;
import id.catchadeal.kerjayuk.entity.role.Menu;
import id.catchadeal.kerjayuk.entity.role.Role;
import id.catchadeal.kerjayuk.entity.role.RoleMenu;
import id.catchadeal.kerjayuk.entity.transaction.PurchaseStatus;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.service.business.TaskApplicationStatusService;
import id.catchadeal.kerjayuk.service.business.TaskService;
import id.catchadeal.kerjayuk.service.business.WithdrawalStatusService;
import id.catchadeal.kerjayuk.service.misc.ConfigurationService;
import id.catchadeal.kerjayuk.service.misc.TaskCategoryService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.misc.WorkingDayService;
import id.catchadeal.kerjayuk.service.partner.*;
import id.catchadeal.kerjayuk.service.psychotest.PersonalityCharacterService;
import id.catchadeal.kerjayuk.service.psychotest.PersonalityService;
import id.catchadeal.kerjayuk.service.report.ReportStatusService;
import id.catchadeal.kerjayuk.service.role.MenuService;
import id.catchadeal.kerjayuk.service.role.RoleMenuService;
import id.catchadeal.kerjayuk.service.role.RoleService;
import id.catchadeal.kerjayuk.service.transaction.PurchaseStatusService;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.service.user.RegistrationService;
import id.catchadeal.kerjayuk.service.user.UserService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.EncryptionUtils;
import id.catchadeal.kerjayuk.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class AppStartInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger logger = LoggerFactory.getLogger(AppStartInitializer.class);


    @Autowired ConfigurationService configurationService ;
    @Autowired TaskCategoryService taskCategoryService;
    @Autowired WorkingDayService workingDayService ;
    @Autowired PersonalityService personalityService ;
    @Autowired PersonalityCharacterService personalityCharacterService ;
    @Autowired PurchaseStatusService purchaseStatusService ;
    @Autowired TaskApplicationStatusService taskApplicationStatusService;
    @Autowired TaskService taskService ;
    @Autowired AdminService adminService ;
    @Autowired MiscellaneousService miscellaneousService ;
    @Autowired ReportStatusService reportStatusService ;
    @Autowired UserService userService ;
    @Autowired RegistrationService registrationService ;
    @Autowired RoleMenuService roleMenuService ;
    @Autowired MenuService menuService ;
    @Autowired RoleService roleService ;
    @Autowired PartnerStatusService partnerStatusService ;
    @Autowired PartnerService partnerService ;
    @Autowired CompanyService companyService ;
    @Autowired PVPartnerStatusService pvPartnerStatusService ;

    @Autowired CorporateTypeService corporateTypeService ;

    @Autowired WithdrawalStatusService withdrawalStatusService ;

    @Autowired Utils utils ;

    @Autowired Gson gson ;

    @Autowired PasswordEncoder passwordEncoder ;

    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        List<Configuration> configurations = configurationService.findAll(Sort.by(Sort.Direction.DESC, "created"));
        if(configurations==null || configurations.size()<=0){
            Configuration configuration = new Configuration();
            configurationService.save(configuration);
        }

        List<TaskCategory> taskCategories = taskCategoryService.findAll(Sort.by(Sort.Direction.ASC, "name"));
        if(taskCategories==null || taskCategories.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1 ;

            try{
                String categoryJson = utils.readJsonFromResource(Constant.MASTER_TASK_CATEGORY_JSON_FILE);
                Type categoryType = new TypeToken<List<TaskCategory>>() {}.getType();
                List<TaskCategory> taskCategoryList = gson.fromJson(categoryJson, categoryType);
                for (int i = 0; i < taskCategoryList.size(); i++) {
                    TaskCategory taskCategory = taskCategoryList.get(i);
                    if(!taskCategoryService.existById(taskCategory.getId())){
                        taskCategory = taskCategoryService.save(taskCategory);
                        map.put(k++, "- Generate Task Category ### ID : "+String.valueOf(taskCategory.getId()));
                    }
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT "+
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        +" ###################");

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        buildCorporateType();
        buildPartnerStatus();
        buildPartner();
        buildWithdrawalStatus();
        buildPsychotest();
        buildPurchaseStatus();
        buildTaskApplicationStatus();
        buildReferralTask();
        buildMenu();
        buildRole();
        buildRoleMenu();
        buildAdmin();
        buildReportStatus();
//        buildUser();
    }


    void buildCorporateType(){
        List<CorporateType> corporateTypes = corporateTypeService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(corporateTypes==null || corporateTypes.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String corporateTypeJson = utils.readJsonFromResource(Constant.MASTER_CORPORATE_TYPE_JSON_FILE);
                Type corporateTypeType = new TypeToken<List<CorporateType>>() {}.getType();
                List<CorporateType> corporateTypeList = gson.fromJson(corporateTypeJson, corporateTypeType);
                for (int i = 0; i < corporateTypeList.size(); i++) {
                    CorporateType corporateType = corporateTypeList.get(i);
                    if(!corporateTypeService.existById(corporateType.getId())){
                        corporateType = corporateTypeService.save(corporateType);
                        map.put(k++, "- Generate Corporate Type ### ID : "+String.valueOf(corporateType.getId()));
                    }
                    map.put(k++, "- Generate Corporate Type ### ID : " + String.valueOf(corporateType.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void buildPartnerStatus(){
        List<PartnerStatus> partnerStatuses = partnerStatusService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(partnerStatuses==null || partnerStatuses.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String partnerStatusJson = utils.readJsonFromResource(Constant.MASTER_PARTNER_STATUS_JSON_FILE);
                Type partnerStatusType = new TypeToken<List<PartnerStatus>>() {}.getType();
                List<PartnerStatus> partnerStatusList = gson.fromJson(partnerStatusJson, partnerStatusType);
                for (int i = 0; i < partnerStatusList.size(); i++) {
                    PartnerStatus partnerStatus = partnerStatusList.get(i);
                    if(!partnerStatusService.existById(partnerStatus.getId())){
                        partnerStatus = partnerStatusService.save(partnerStatus);
                        map.put(k++, "- Generate Partner Status ### ID : "+String.valueOf(partnerStatus.getId()));
                    }
                    map.put(k++, "- Generate Partner Status ### ID : " + String.valueOf(partnerStatus.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void buildPartner(){
        List<Partner> partners = partnerService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(partners==null || partners.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String partnerJson = utils.readJsonFromResource(Constant.MASTER_PARTNER_JSON_FILE);
                Type partnerType = new TypeToken<List<Partner>>() {}.getType();
                List<Partner> partnerList = gson.fromJson(partnerJson, partnerType);
                for (int i = 0; i < partnerList.size(); i++) {
                    Company company = companyService.save(partnerList.get(i).getCompany());
                    Partner partner = partnerList.get(i);
                    partner.setCompany(company);
                    partner = partnerService.save(partner);
                    PVPartnerStatus pvPartnerStatus = new PVPartnerStatus();
                    pvPartnerStatus.setPartner(partner);
                    pvPartnerStatus.setPartnerStatus(partner.getPartnerStatus());
                    pvPartnerStatusService.save(pvPartnerStatus);
                    map.put(k++, "- Generate Partner Status ### ID : "+partner.getId());
                    map.put(k++, "- Generate Partner Status ### ID : "+partner.getId());
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void buildWithdrawalStatus(){
        List<WithdrawalStatus> withdrawalStatuses = withdrawalStatusService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(withdrawalStatuses==null || withdrawalStatuses.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String withdrawalStatusJson = utils.readJsonFromResource(Constant.MASTER_WITHDRAWAL_STATUS_JSON_FILE);
                Type withdrawalStatusType = new TypeToken<List<WithdrawalStatus>>() {}.getType();
                List<WithdrawalStatus> withdrawalStatusList = gson.fromJson(withdrawalStatusJson, withdrawalStatusType);
                for (int i = 0; i < withdrawalStatusList.size(); i++) {
                    WithdrawalStatus withdrawalStatus = withdrawalStatusList.get(i);
                    if(!withdrawalStatusService.existById(withdrawalStatus.getId())){
                        withdrawalStatus = withdrawalStatusService.save(withdrawalStatus);
                        map.put(k++, "- Generate Withdrawal Status ### ID : "+String.valueOf(withdrawalStatus.getId()));
                    }
                    map.put(k++, "- Generate Withdrawal Status ### ID : " + String.valueOf(withdrawalStatus.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    void buildPsychotest() {
        List<Personality> personalities = personalityService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if (personalities == null || personalities.size() <= 0) {
            personalities = new ArrayList<>();
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String personalityJson = utils.readJsonFromResource(Constant.MASTER_PERSONALITY_JSON_FILE);
                Type personalityType = new TypeToken<List<Personality>>() {
                }.getType();
                List<Personality> personalityList = gson.fromJson(personalityJson, personalityType);
                for (int i = 0; i < personalityList.size(); i++) {
                    Personality personality = personalityList.get(i);
                    Set<PersonalityCharacter> personalityCharacterSet = personality.getPersonalityCharacters();
                    personality.setPersonalityCharacters(null);
                    if (!personalityService.existById(personality.getId())) {
                        personality = personalityService.save(personality);
                        map.put(k++, "- Generate Personality ### ID : " + String.valueOf(personality.getId()));
                    }
                    personality.setPersonalityCharacters(personalityCharacterSet);
                    personalities.add(personality);
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        personalities = personalityService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        personalities.forEach(personality -> {
            List<PersonalityCharacter> personalityCharacters = personalityCharacterService.findByPersonalityId(personality.getId());
            if (personalityCharacters == null || personalityCharacters.size() <= 0) {
                Set<PersonalityCharacter> personalityCharacterSet = personality.getPersonalityCharacters();
                personalityCharacterSet.forEach(personalityCharacter -> {
                    personalityCharacter.setPersonality(personality);
                    personalityCharacterService.save(personalityCharacter);
                });
            }
        });

    }

    void buildPurchaseStatus(){
        List<PurchaseStatus> purchaseStatuss = purchaseStatusService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(purchaseStatuss==null || purchaseStatuss.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String purchaseStatusJson = utils.readJsonFromResource(Constant.MASTER_PURCHASE_STATUS_JSON_FILE);
                Type purchaseStatusType = new TypeToken<List<PurchaseStatus>>() {}.getType();
                List<PurchaseStatus> purchaseStatusList = gson.fromJson(purchaseStatusJson, purchaseStatusType);
                for (int i = 0; i < purchaseStatusList.size(); i++) {
                    PurchaseStatus purchaseStatus = purchaseStatusList.get(i);
                    if(!purchaseStatusService.existById(purchaseStatus.getId())){
                        purchaseStatus = purchaseStatusService.save(purchaseStatus);
                        map.put(k++, "- Generate Purchase Status ### ID : "+String.valueOf(purchaseStatus.getId()));
                    }
                    map.put(k++, "- Generate Purchase Status ### ID : " + String.valueOf(purchaseStatus.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    void buildTaskApplicationStatus(){
        List<TaskApplicationStatus> taskApplicationStatuses = taskApplicationStatusService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(taskApplicationStatuses==null || taskApplicationStatuses.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String taskApplicationStatusJson = utils.readJsonFromResource(Constant.MASTER_TASK_APPLICATION_STATUS_JSON_FILE);
                Type taskApplicationStatusType = new TypeToken<List<TaskApplicationStatus>>() {}.getType();
                List<TaskApplicationStatus> taskApplicationStatusList = gson.fromJson(taskApplicationStatusJson, taskApplicationStatusType);
                for (int i = 0; i < taskApplicationStatusList.size(); i++) {
                    TaskApplicationStatus taskApplicationStatus = taskApplicationStatusList.get(i);
                    if(!taskApplicationStatusService.existById(taskApplicationStatus.getId())){
                        taskApplicationStatus = taskApplicationStatusService.save(taskApplicationStatus);
                        map.put(k++, "- Generate Task Application Status ### ID : "+String.valueOf(taskApplicationStatus.getId()));
                    }
                    map.put(k++, "- Generate Task Application Status ### ID : " + String.valueOf(taskApplicationStatus.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

//    void buildDummyTask(){
//        List<Task> tasks = taskService.findAll(Sort.by(Sort.Direction.ASC, "id"));
//        if(tasks==null || tasks.size()<=0){
//            HashMap<Integer, String> map = new HashMap<>();
//            int k = 1;
//
//            try {
//                String tasksJson = utils.readJsonFromResource(Constant.MASTER_TASK_JSON_FILE);
//                Type taskType = new TypeToken<List<Task>>() {}.getType();
//                List<Task> taskList = gson.fromJson(tasksJson, taskType);
//                for (int i = 0; i < taskList.size(); i++) {
//                    Task task = taskList.get(i);
//                    Partner partner = partnerService.findByWebsite("catchadeal.id");
//                    task.setPartner(partner);
//                    task = taskService.save(task);
//                    map.put(k++, "- Generate Task ### ID : "+String.valueOf(task.getId()));
//                    map.put(k++, "- Generate Task Status ### ID : " + String.valueOf(task.getId()));
//                }
//                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
//                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
//                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
//                        + " ###################");
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//    }

    void buildReferralTask(){
        Boolean exists = taskService.existsByType(TaskDto.REFERRAL_AGENT_TASK);
        if(!exists){

            try{
                HashMap<Integer, String> map = new HashMap<>();
                String tasksJson = utils.readJsonFromResource(Constant.MASTER_REFERRAL_TASK_JSON_FILE);
                Task task = gson.fromJson(tasksJson, Task.class);
                task.setType(TaskDto.REFERRAL_AGENT_TASK);
                Partner partner = partnerService.findByWebsite("catchadeal.id");
                task.setPartner(partner);
                task = taskService.save(task);
                map.put(1, "- Generate Task ### ID : "+String.valueOf(task.getId()));
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public void buildMenu(){
        try{
            List<Menu> menuList = menuService.findAll(Sort.by(Sort.Direction.ASC, "id"));
            if(menuList==null || menuList.size()<=0){
                HashMap<Integer, String> map = new HashMap<>();
                int k = 1;
                String menuJson = utils.readJsonFromResource(Constant.MASTER_MENU_JSON_FILE);
                Type menuType = new TypeToken<List<Menu>>() {}.getType();
                List<Menu> menus = gson.fromJson(menuJson, menuType);
                for (int i = 0; i < menus.size(); i++) {
                    Menu menu = menus.get(i);
                    if(!menuService.existsById(menu.getId())){
                        menu = menuService.save(menu);
                        map.put(k++, "- Generated Menu ### ID : "+String.valueOf(menu.getId()));
                    }
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public void buildRole(){
        try{
            List<Role> roleList = roleService.findAll(Sort.by(Sort.Direction.ASC, "id"));
            if(roleList==null || roleList.size()<=0){
                HashMap<Integer, String> map = new HashMap<>();
                int k = 1;
                String roleJson = utils.readJsonFromResource(Constant.MASTER_ROLE_JSON_FILE);
                Type roleType = new TypeToken<List<Role>>() {}.getType();
                List<Role> roles = gson.fromJson(roleJson, roleType);
                for (int i = 0; i < roles.size(); i++) {
                    Role role = roles.get(i);
                    if(!roleService.existsById(role.getId())){
                        role = roleService.save(role);
                        map.put(k++, "- Generated Role ### ID : "+String.valueOf(role.getId()));
                    }
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void buildRoleMenu(){
        try{
            List<RoleMenu> roleMenuList = roleMenuService.findAll(Sort.by(Sort.Direction.ASC, "id"));
            if(roleMenuList==null || roleMenuList.size()<=0){
                HashMap<Integer, String> map = new HashMap<>();
                int k = 1;
                String roleMenuJson = utils.readJsonFromResource(Constant.MASTER_ROLE_MENU_JSON_FILE);
                Type roleMenuType = new TypeToken<List<RoleMenu>>() {}.getType();
                List<RoleMenu> roleMenus = gson.fromJson(roleMenuJson, roleMenuType);
                for (int i = 0; i < roleMenus.size(); i++) {
                    RoleMenu roleMenu = roleMenus.get(i);
                    if(!roleMenuService.existsById(roleMenu.getId())){
                        roleMenu = roleMenuService.save(roleMenu);
                        map.put(k++, "- Generated Role ### ID : "+String.valueOf(roleMenu.getId()));
                    }
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    void buildAdmin(){
        List<Admin> admines = adminService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(admines==null || admines.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String adminJson = utils.readJsonFromResource(Constant.MASTER_SUPER_ADMIN_JSON_FILE);
                Type adminType = new TypeToken<List<Admin>>() {}.getType();
                List<Admin> adminList = gson.fromJson(adminJson, adminType);
                for (int i = 0; i < adminList.size(); i++) {
                    Admin admin = adminList.get(i);
                    if(!adminService.existById(admin.getId())){
                        String encryptedPassword = EncryptionUtils.encrypt(Constant.AVENGER_KEY, admin.getPassword());
                        admin.setPassword(encryptedPassword);
                        admin = adminService.save(admin);

                        map.put(k++, "- Generate Admin  ### ID : "+String.valueOf(admin.getId()));
                    }
                    map.put(k++, "- Generate Admin Item ### ID : " + String.valueOf(admin.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    void buildUser(){
        List<User> users = userService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(users==null || users.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String userJson = utils.readJsonFromResource(Constant.MASTER_USER_JSON_FILE);
                Type userType = new TypeToken<List<User>>() {}.getType();
                List<User> userList = gson.fromJson(userJson, userType);
                for (int i = 0; i < userList.size(); i++) {
                    User user = userList.get(i);
                    if(!userService.existsByEmailOrMobilePhone(user.getEmail(), user.getMobilePhone())){
                        logger.debug("############## PASSWORD == {} ", user.getPassword());
                        String encryptedPassword = passwordEncoder.encode(user.getPassword());
                        user.setPassword(encryptedPassword);
                        user = userService.save(user);

                        Registration registration = registrationService.register(user.getFirstname(), user.getLastname(), user.getMobilePhone(), user.getEmail(), user.getReferralCode(), user.getAgentCode(), Registration.SYSTEM);
                        user.setRegistration(registration);
                        user = userService.save(user);

                        map.put(k++, "- Generate User  ### ID : "+String.valueOf(user.getId()));
                    }
                    map.put(k++, "- Generate User Item ### ID : " + String.valueOf(user.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    void buildReportStatus(){
        List<ReportStatus> reportStatuss = reportStatusService.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if(reportStatuss==null || reportStatuss.size()<=0){
            HashMap<Integer, String> map = new HashMap<>();
            int k = 1;

            try {
                String reportStatusJson = utils.readJsonFromResource(Constant.MASTER_REPORT_STATUS_JSON_FILE);
                Type reportStatusType = new TypeToken<List<ReportStatus>>() {}.getType();
                List<ReportStatus> reportStatusList = gson.fromJson(reportStatusJson, reportStatusType);
                for (int i = 0; i < reportStatusList.size(); i++) {
                    ReportStatus reportStatus = reportStatusList.get(i);
                    if(!reportStatusService.existById(reportStatus.getId())){
                        reportStatus = reportStatusService.save(reportStatus);

                        map.put(k++, "- Generate Report Status  ### ID : "+String.valueOf(reportStatus.getId()));
                    }
                    map.put(k++, "- Generate Report Status Item ### ID : " + String.valueOf(reportStatus.getId()));
                }
                logger.debug("############## FINISH BUILDING MASTER DATA CONTENT == {} ", map);
                logger.debug("############## FINISH BUILDING MASTER DATA AT " +
                        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(new Date())
                        + " ###################");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }






}
