package id.catchadeal.kerjayuk.repository.email;

import id.catchadeal.kerjayuk.entity.email.SendEmail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SendEmailRepository extends JpaRepository<SendEmail, Long> {

    Page<SendEmail> findBySubjectContainingIgnoreCase(String subject, Pageable pageable);

}
