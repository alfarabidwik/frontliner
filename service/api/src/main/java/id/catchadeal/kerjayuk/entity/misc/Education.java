package id.catchadeal.kerjayuk.entity.misc;

import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_education")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Education extends EBase {

    String name ;
    String description ;

    @Override
    public void initTransient() {
        super.initTransient();
    }

}
