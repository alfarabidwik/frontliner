package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.PartnerStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartnerStatusRepository extends JpaRepository<PartnerStatus, Long> {



}
