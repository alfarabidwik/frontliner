package id.catchadeal.kerjayuk.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.component.MyRequestInterceptor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Locale;

@Configuration
@EnableSwagger2
//@EnableWebMvc
public class BasicConfiguration{


    private static final Logger logger4j = LoggerFactory.getLogger(BasicConfiguration.class);

    @Value("${}")

    @Bean
    public Docket frontendApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("1 - Frontend Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(FrontendRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Kerjayuk Frontend Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for Kerjayuk")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@kerjayuk.id"))
                                .build()
                );
    }

    @Bean
    public Docket adminApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("2 - Admin Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(AdminRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Kerjayuk Admin Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for Kerjayuk")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@kerjayuk.id"))
                                .build()
                );
    }

    @Bean
    public Docket universalApi() {
        return new Docket(DocumentationType.SWAGGER_12)
                .groupName("3 - Universal Documentation")
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(UniversalRest.class))
                .paths(PathSelectors.any())
                .build().apiInfo(
                        new ApiInfoBuilder()
                                .title("Kerjayuk Universal Documentation")
                                .description("API Documentation with json structure / formatting," +
                                        "Rest Method, e.t.c for Kerjayuk")
                                .license("Apache 2.0")
                                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                                .termsOfServiceUrl("")
                                .version("1.0.0")
                                .contact(new Contact("", "", "support@kerjayuk.id"))
                                .build()
                );
    }

//    @Bean
//    UiConfiguration uiConfig() {
//        return UiConfigurationBuilder.builder()
//                .docExpansion(DocExpansion.LIST) // or DocExpansion.NONE or DocExpansion.FULL
//                .build();
//    }

//    @Configuration
//    public class SwaggerDocumentationConfig {
//        @Bean
//        public UiConfiguration tryItOutConfig() {
//            final String[] methodsWithTryItOutButton = {  };
//            return UiConfigurationBuilder.builder().supportedSubmitMethods(methodsWithTryItOutButton).build();
//        }
//    }


    @Bean
    Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(DateAppConfig.API_DATE_FORMAT);
        return gsonBuilder.create();
    }

//    @Bean
//    public MethodValidationPostProcessor methodValidationPostProcessor() {
//        return new MethodValidationPostProcessor();
//    }

//    @Bean
//    public DataSource dataSource() throws SQLException {
//        return new HikariDataSource(this);
//    }

    @Autowired MyRequestInterceptor requestInterceptor ;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }

            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(requestInterceptor).addPathPatterns("/**/**/**/");
            }
        };
    }
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**").allowedOrigins("*");
//    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(
                "classpath:messages/messages",
                "classpath:messages/notification",
                "classpath:messages/kerjayuk-notification",
                "classpath:messages/user-activity-label",
                "classpath:messages/admin-activity-label",
                "classpath:messages/user-activity",
                "classpath:messages/admin-activity"
        );
        // If true, the key of the message will be displayed if the key is not found, instead of throwing an exception
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        // The value 0 means always reload the messages to be developer friendly
        messageSource.setCacheSeconds(0);
        return messageSource;
    }


    @Bean
    public LocaleResolver localeResolver() {
        return new SmartLocaleResolver();
    }


    public class SmartLocaleResolver extends CookieLocaleResolver {

        @Override
        public Locale resolveLocale(HttpServletRequest request) {
            for (String httpHeaderName : Collections.list(request.getHeaderNames())) {
                logger4j.debug("===========>> Header name: " + httpHeaderName);
            }
            String acceptLanguage = request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);
            logger4j.debug("===========>> acceptLanguage: " + acceptLanguage);
            Locale locale = super.resolveLocale(request);
            logger4j.debug("===========>> acceptLanguage locale: " + locale.getDisplayCountry());
            if (null == locale) {
                locale = getDefaultLocale();
                logger4j.debug("===========>> Default locale: " + locale.getDisplayCountry());
            }
            return locale;
        }

    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setValidationMessageSource(messageSource);
        return validatorFactoryBean;
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


//    @Bean
//    public SessionFactory sessionFactory(EntityManagerFactory entityManagerFactory) {
//        return entityManagerFactory.unwrap(SessionFactory.class);
//    }



}
