package id.catchadeal.kerjayuk.service.user;

import id.catchadeal.kerjayuk.entity.user.ResetPassword;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.repository.user.ResetPasswordRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ResetPasswordService extends BasicRepoService<ResetPassword> {

    @Autowired ResetPasswordRepository repository ;

    @Override
    public <JPA extends JpaRepository> JPA repository() {
        return (JPA) repository;
    }

    public ResetPassword findByToken(String token){
        return repository.findByToken(token).orElseThrow(() -> new AppException(Constant.FAILED_CODE, message("invalid.link"), message("link.cant.be.used")));
    }
    public Boolean existsByToken(String token){
        return repository.existsByToken(token);

    }

}
