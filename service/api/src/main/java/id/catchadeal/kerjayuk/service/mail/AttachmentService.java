package id.catchadeal.kerjayuk.service.mail;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.email.Attachment;
import id.catchadeal.kerjayuk.entity.email.SendEmail;
import id.catchadeal.kerjayuk.repository.email.AttachmentRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class AttachmentService extends BasicRepoService<Attachment> {

    @Autowired AttachmentRepository repository ;
    @Value("${email.attachment.file.dir}") String emailAttachmentFileDir ;


    @Override
    public JpaRepository repository() {
        return repository;
    }

    //@Transactional
    public List<Attachment> upload(@NotNull SendEmail sendEmail, MultipartFile... multipartFiles) {
        List<Attachment> attachments  = new ArrayList<>();
        if(multipartFiles!=null && multipartFiles.length>0){
            for (int i = 0; i < multipartFiles.length; i++) {
                try {
                    String filename = String.valueOf(new Date().getTime());
                    Attachment attachment = new Attachment();
                    MultipartFile multipartFile = multipartFiles[i];
                    filename = fileStorage.storeFile(emailAttachmentFileDir, String.valueOf(sendEmail.getId()), filename, multipartFile);
                    attachment.setFilename(filename);
                    attachment.setSendEmail(sendEmail);
                    attachment = save(attachment);
                    attachments.add(attachment);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return attachments ;
    }





}
