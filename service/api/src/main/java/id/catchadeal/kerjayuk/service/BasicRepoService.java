package id.catchadeal.kerjayuk.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.catchadeal.kerjayuk.component.AppCacheManager;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.entity.BasicField;
import id.catchadeal.kerjayuk.entity.misc.Configuration;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.repository.misc.ConfigurationRepository;
import id.catchadeal.kerjayuk.security.JwtTokenProvider;
import id.catchadeal.kerjayuk.service.misc.NotificationService;
import id.catchadeal.kerjayuk.util.Constant;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import java.util.*;

@MappedSuperclass
public abstract class BasicRepoService<T extends BasicField> {

    private static final Logger logger = LoggerFactory.getLogger(BasicRepoService.class.getName());

    @Autowired protected MessageSource messageSource;
    @Autowired protected ModelMapper mapper;
    @Autowired protected JwtTokenProvider jwtTokenProvider;
    @Autowired protected RestTemplate restTemplate;
    @Autowired protected FileStorage fileStorage;
    @Autowired protected EntityManager entityManager ;
    @Autowired protected AppCacheManager appCacheManager ;
    @Autowired ConfigurationRepository configurationRepository ;
    @Autowired protected NotificationService notificationService ;
    @Value("${purchase.reward}") protected String purchaseReward ;

    public static EntityManager staticEntityManager ;

    @PostConstruct
    public void construct(){
        BasicRepoService.staticEntityManager = entityManager ;
    }


    @Cacheable(cacheNames = Constant.CACHE_CONFIGURATION)
    public Configuration configuration(){
        List<Configuration> configurations = configurationRepository.findAll(Sort.by(Sort.Direction.DESC, "created"));
        if(configurations==null || configurations.size()==0){
            throw new AppException(Constant.FAILED_CODE, "Configuration not found please restart your server and set a necessary field on Configuration Table");
        }
        return configurations.get(0);
    }


    @Value("${page.row}")
    protected int pageRow ;

    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();

    public Boolean existsById(Long id){
        return repository().existsById(id);
    }

    public List<T> findAll() {
        return repository().findAll();
    }

    public List<T> findAll(Sort sort) {
        return repository().findAll(sort);
    }

    public Page<T> findAll(Pageable pageable) {
        return repository().findAll(pageable);
    }

    public T findById(Long id) {
        Optional<T> optionalT = repository().findById(id);
        if(!optionalT.isPresent()){
            throw new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "id"));
        }
        return optionalT.get();
    }

    public void delete(Long id) {
        repository().deleteById(id);
    }

    @Transactional
    public T save(T object){
        object.setUpdated(new Date());
        object = (T) repository().save(object);
        object.initTransient();
        return object ;
    }

    public <O extends BasicField> void checkForDelete(Set<O> newList, Set<O> previousList){
        if(newList==null || newList.size()==0){
            if(previousList!=null){
                previousList.forEach(o -> {
                    try{
                        delete(o.getId());
                    }catch (Exception e){
                    }
                });

            }
            return;
        }
        if(previousList!=null){
            previousList.forEach(o -> {
                boolean found = false ;
                for (O o1 : newList) {
                    if(o1.getId()!=null && o.getId()!=null && o1.getId().equals(o.getId()) || o1.getId()==o.getId()){
                        found = true ;
                    }
                }
                if(!found){
                    try{
                        delete(o.getId());
                    }catch (Exception e){
                    }
                }
            });
        }
    }

    protected String message(String message, Object... args) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(message, args==null?new Object[]{}:args, locale);
    }

    public abstract <JPA extends JpaRepository> JPA repository();

}
