package id.catchadeal.kerjayuk.service.psychotest;

import id.catchadeal.kerjayuk.entity.psychotest.Personality;
import id.catchadeal.kerjayuk.repository.psychotest.PersonalityRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PersonalityService extends BasicRepoService<Personality> {

    @Autowired private PersonalityRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }



}
