package id.catchadeal.kerjayuk.service.formbuilder;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.formbuilder.GroupForm;
import id.catchadeal.kerjayuk.repository.formbuilder.GroupFormRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;


@Service
public class GroupFormService extends BasicRepoService<GroupForm> {

    @Autowired GroupFormRepository repository ;
    @Value("${email.attachment.file.dir}") String emailGroupFormFileDir ;


    @Override
    public JpaRepository repository() {
        return repository;
    }





}
