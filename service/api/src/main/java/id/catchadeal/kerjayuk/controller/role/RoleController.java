package id.catchadeal.kerjayuk.controller.role;

import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.role.Menu;
import id.catchadeal.kerjayuk.entity.role.Role;
import id.catchadeal.kerjayuk.entity.role.RoleMenu;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.role.RoleDto;
import id.catchadeal.kerjayuk.service.role.RoleMenuService;
import id.catchadeal.kerjayuk.service.role.RoleService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class RoleController extends BasicController {

    @Autowired RoleService roleService;
    @Autowired RoleMenuService roleMenuService ;



    @GetMapping(path = "/roles")
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, roleService.findAll(active));
    }

    @GetMapping(path = "/role/{id}")
    @Override
    public WSResponse findById(@PathVariable Long id) {
        Role role = roleService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, role);
    }

    @PostMapping(path = "/role/save")
    public WSResponse save(@RequestBody @Valid RoleDto roleDto){
        Role role = modelMapper.map(roleDto, Role.class);
        role = roleService.save(role);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, role);
    }

    @GetMapping(path = "/role/delete/{id}")
    public WSResponse delete(@PathVariable Long id){
        roleService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    @PostMapping("/role/updateMenu/{roleId}")
    public WSResponse updateMenu(@PathVariable Long roleId, @RequestBody List<Menu> menus){
        Role role = roleService.findById(roleId);
        List<RoleMenu> roleMenus = roleMenuService.findByRoleId(roleId);
        for (int i = 0; i < roleMenus.size(); i++) {
            roleMenuService.delete(roleMenus.get(i).getId());
        }
        roleMenus.clear();
        for (int i = 0; i < menus.size(); i++) {
            Menu parentMenu = menus.get(i);
            if(parentMenu.isChecked()){
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.setRole(role);
                roleMenu.setMenu(parentMenu);
                roleMenu = roleMenuService.save(roleMenu);
                for (Menu childMenu : parentMenu.getChildMenus()) {
                    if(childMenu.isChecked()){
                        roleMenu = new RoleMenu();
                        roleMenu.setRole(role);
                        roleMenu.setMenu(childMenu);
                        roleMenu = roleMenuService.save(roleMenu);
                    }
                }
            }
        }
        role = roleService.findById(roleId);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, role);
    }


}
