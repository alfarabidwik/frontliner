package id.catchadeal.kerjayuk.repository.payment;

import id.catchadeal.kerjayuk.entity.payment.Bank;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BankRepository extends JpaRepository<Bank, Long> {

    List<Bank> findByActive(Boolean active, Sort sort);
}
