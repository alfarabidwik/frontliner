package id.catchadeal.kerjayuk.repository.user;

import id.catchadeal.kerjayuk.entity.user.UserDevice;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDeviceRepository extends JpaRepository<UserDevice, Long> {

    List<UserDevice> findByUserId(Long userId);


    List<UserDevice> findByDeviceIdAndUserId(String deviceId, Long userId);
    List<UserDevice> findByFcmToken(String fcmToken);
    List<UserDevice> findByDeviceId(String deviceId, Sort sort);
    List<UserDevice> findByDeviceIdAndUserId(String deviceId, Long userId, Sort sort);


}
