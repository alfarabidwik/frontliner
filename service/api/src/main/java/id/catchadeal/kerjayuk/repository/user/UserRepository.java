package id.catchadeal.kerjayuk.repository.user;

import id.catchadeal.kerjayuk.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {


    @Query("SELECT DISTINCT u FROM User u " +
            "LEFT JOIN FETCH u.userDevices ud WHERE ( u.email = ?1  OR u.mobilePhone = ?1 ) AND u.active = ?2 ")
    User findTopByEmailOrMobilePhoneAndActive(String subjectKey, Boolean active, Sort sort);


    @Query("SELECT DISTINCT u FROM User u " +
            "LEFT JOIN FETCH u.userDevices ud WHERE u.mobilePhone = ?1 AND u.active = ?2 ")
    User findTopByMobilePhoneAndActive(String mobilePhone, Boolean active, Sort sort);

    @Query("SELECT DISTINCT u FROM User u " +
            "LEFT JOIN FETCH u.userDevices ud WHERE u.email = ?1 AND u.active = ?2 ")
    User findTopByEmailAndActive(String email, Boolean active, Sort sort);


    Boolean existsByAgentCodeAndActiveIsTrue(String agentCode);

    Boolean existsByMobilePhoneAndActiveIsTrue(String mobilePhone);

    Boolean existsByEmailAndActiveIsTrue(String email);

    @Query("SELECT id FROM User u WHERE u.mobilePhone = ?1 AND u.active = ?2 ")
    Long findUserIdByPhoneNumberAndActive(String mobilePhone, Boolean active, Sort sort);

    @Query("SELECT u.mobilePhone, u.role FROM User u WHERE u.active = ?1 ")
    List<Object[]> findMobilePhoneByActive(Boolean active, Sort sort);


    Page<User> findByActive(Boolean active, Pageable pageable);


    User findByEmail(String email);
    User findByMobilePhone(String mobilePhone);

    Boolean existsByEmail(String email);
    Boolean existsByMobilePhone(String mobilePhone);


    Boolean existsByEmailOrMobilePhone(String email, String mobilePhone);

    @Query("SELECT c.firstname FROM User c")
    List<String> firstnames(Sort sort);

    @Query("SELECT c.email FROM User c WHERE c.email IS NOT NULL")
    List<String> emails(Sort sort);

    User findByAgentCode(String agentCode);

}
