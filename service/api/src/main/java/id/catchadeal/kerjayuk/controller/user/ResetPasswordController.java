package id.catchadeal.kerjayuk.controller.user;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.user.ResetPassword;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.user.RegistrationService;
import id.catchadeal.kerjayuk.service.user.ResetPasswordService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.DateUtility;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class ResetPasswordController extends BasicController {

    @Autowired ResetPasswordService resetPasswordService ;
    @Autowired MiscellaneousService miscellaneousService ;

    @GetMapping(path = "/resetPassword/validate")
    @AdminRest
    @ApiOperation(value = "", response = WSRegistrations.class)
    public WSResponse findAll(@RequestParam String token) {
        ResetPassword resetPassword = resetPasswordService.findByToken(token);
        if(resetPassword.isUsed()){
            throw new AppException(Constant.FAILED_CODE, message("invalid.link"), message("link.has.been.used"));
        }
        if(DateUtility.dayDifferrence(resetPassword.getCreated(), new Date())>1){
            throw new AppException(Constant.FAILED_CODE, message("invalid.link"), message("link.has.been.expired"));
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


}
