package id.catchadeal.kerjayuk.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.role.Role;
import id.catchadeal.kerjayuk.util.Constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Table(name="j_admin")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Admin extends EBase implements UserDetails {

    public static final String TABLE = "j_admin";

    public static final String[] ADMIN_STATUSES = new String[]{"ACTIVE","INACTIVE","BLOCKED"};
    public static final String ACTIVE = ADMIN_STATUSES[0];
    public static final String INACTIVE = ADMIN_STATUSES[1];
    public static final String BLOCKED = ADMIN_STATUSES[2];


    public static final String SUPER_ADMIN = "super_admin";
    public static final String ADMIN = "admin";

    private String firstname = "";
    private String lastname = "";

    private String email ;
    private String password ;
    private String image ;

    @Column(nullable = false, columnDefinition = "character varying(50) DEFAULT 'ACTIVE'")
    private String adminStatus ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="role_id")
    private Role role ;

    @OneToMany(mappedBy="admin", fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SELECT)
    @JsonIgnoreProperties("admin")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    private List<AdminActivity> adminActivities = new ArrayList<>();

    @Transient
    public String getFullname(){
        return firstname+(lastname!=null?" "+lastname:"");
    }

    @Transient
    private String authorization ;

    @Transient
    private String imageLink ;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(Constant.ADMIN).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void initTransient() {
        super.initTransient();
        if(StringUtils.isNotEmpty(image)){
            imageLink = Constant.prefixApi+ Constant.REST_ADMIN_PHOTO_PROFILE+"/"+this.getId()+"/"+this.image ;
        }
    }

}
