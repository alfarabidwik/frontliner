package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.PVTaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.business.Task;
import id.catchadeal.kerjayuk.entity.business.TaskApplication;
import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.business.TaskApplicationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

@Service
public class TaskApplicationService extends BasicRepoService<TaskApplication> {
    Logger logger = LoggerFactory.getLogger(TaskApplicationService.class);

    @Autowired
    TaskApplicationRepository repository ;
    @Autowired
    TaskApplicationStatusService taskApplicationStatusService;
    @Autowired
    PVTaskApplicationStatusService pvTaskApplicationStatusService;
    @Autowired
    TaskService taskService;

    @Autowired
    MiscellaneousService miscellaneousService ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public DataPage taskApplications(Long userId, Long taskId, String search, Boolean published,
                                     Long taskCategoryId, BigDecimal feeStart, BigDecimal feeEnd, Date startDate, Date endDate,
                                     Long taskApplicationStatusId, Integer page, Boolean ascending, String sortir){
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sqlObject = "SELECT ja ";
//        String sqlCount = "SELECT COUNT(ja.id) ";

        String sql = " FROM TaskApplication ja LEFT JOIN FETCH ja.taskApplicationStatus jas LEFT JOIN FETCH ja.task jt " +
                "LEFT JOIN FETCH ja.pvTaskApplicationStatuses pjas LEFT JOIN ja.referralMembers rm WHERE ja.active = true AND jt.active = true ";

        if(StringUtils.isNotEmpty(search)){
            sql = sql+" AND ( LOWER(COALESCE(jt.title, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(jt.description, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(ja.user.firstname, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(ja.user.agentCode, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(ja.user.email, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(ja.user.mobilePhone, '')) LIKE CONCAT('%',LOWER(:search),'%') )";
            paramaterMap.put("search", search);
        }

        if(taskId!=null){
            sql = sql+" AND jt.id = :taskId ";
            paramaterMap.put("taskId", taskId);
        }

        if(userId!=null){
            sql = sql+" AND ja.user.id = :userId ";
            paramaterMap.put("userId", userId);
        }

        if(taskApplicationStatusId!=null){
            sql = sql+" AND jas.id = :taskApplicationStatusId ";
            paramaterMap.put("taskApplicationStatusId", taskApplicationStatusId);
        }

        if(published!=null){
            sql = sql+" AND jt.published = :published ";
            paramaterMap.put("published", published);
        }
        if(taskCategoryId!=null){
            sql = sql+" AND jt.taskCategory.id = :taskCategoryId ";
            paramaterMap.put("taskCategoryId", taskCategoryId);
        }
        if(feeStart!=null && feeEnd!=null){
            sql = sql+" AND jt.fee >= :feeStart ";
            paramaterMap.put("feeStart", feeStart);
            sql = sql+" AND jt.fee <= :feeEnd ";
            paramaterMap.put("feeEnd", feeEnd);
        }

        if(startDate!=null && endDate!=null){
            sql = sql+" AND jt.created >= :startDate AND jt.created <= :endDate ";
            paramaterMap.put("startDate", startDate);
            paramaterMap.put("endDate", endDate);
        }


        String groupBy = " GROUP BY ja.id, jt.id, jas.id, pjas.id, rm.id ";
        String orderBy = " ORDER BY "+sortir+" "+(ascending?" ASC ":" DESC ");


        String finalSql = sqlObject+sql+groupBy+orderBy;
//        String finalSqlCount = sqlCount+sql+groupBy+orderBy;

        int offset = page*pageRow;
        long countResult = 0 ;

        try {
            Query queryTotal = entityManager.createQuery(finalSql);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            countResult = 0 ;
        }
        Query query = entityManager.createQuery(finalSql);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<TaskApplication> taskApplications = query.getResultList();

        DataPage dataPage = DataPage.builder(taskApplications, pageRow, countResult);
        return dataPage ;
    }


    public TaskApplication apply(User user, Task task){
        TaskApplication taskApplication = findByUserIdAndTaskIdAndTaskApplicationStatusId(user.getId(), task.getId(), TaskApplicationStatusDto.TAKEN_ID);
        if(taskApplication !=null){
            taskApplication = save(taskApplication);
        }else{
            taskApplication = new TaskApplication();
            taskApplication.setUser(user);
            taskApplication.setTask(task);
            taskApplication.setItemFee(task.getFee());
            taskApplication.setOrderReference(generateOrderReference());
            taskApplication = save(taskApplication);
            taskApplication = updateStatus(taskApplication, TaskApplicationStatus.TAKEN.getId(), null);

        }

        return taskApplication;
    }



    public TaskApplication findByUserIdAndTaskId(Long userId, Long taskId){
        return repository.findByUserIdAndTaskId(userId, taskId);
    }
    public List<TaskApplication> findByUserId(Long userId){
        return repository.findByUserId(userId);
    }



    public TaskApplication updateStatus(TaskApplication taskApplication, Long taskApplicationStatusId, String note){
        TaskApplicationStatus taskApplicationStatus = taskApplicationStatusService.findById(taskApplicationStatusId);

        taskApplication.setTaskApplicationStatus(taskApplicationStatus);

        PVTaskApplicationStatus pvTaskApplicationStatus = new PVTaskApplicationStatus();
        pvTaskApplicationStatus.setTaskApplication(taskApplication);
        pvTaskApplicationStatus.setTaskApplicationStatus(taskApplicationStatus);
        pvTaskApplicationStatus.setNote(note);

        pvTaskApplicationStatus = pvTaskApplicationStatusService.save(pvTaskApplicationStatus);
        taskApplication = save(taskApplication);
        return findById(taskApplication.getId()) ;
    }

    public Boolean existsByUserIdAndTaskId(Long userId, Long taskId){
        return repository.existsByUserIdAndTaskId(userId, taskId);
    }

    public Boolean existsByUserIdAndTaskId(Long userId, Long taskId, Long taskApplicationStatusId){
        return repository.existsByUserIdAndTaskIdAndTaskApplicationStatusId(userId, taskId, taskApplicationStatusId);
    }

    public TaskApplication findByOrderReference(String orderReference){
        return repository.findByOrderReference(orderReference).orElseThrow(() -> {
            return new AppException(Constant.FAILED_CODE, "order.reference.is.not.found");
        });
    }

    public TaskApplication findByUserIdAndTaskIdAndTaskApplicationStatusId(Long userId, Long taskId, Long taskApplicationStatusId){
        return repository.findByUserIdAndTaskIdAndTaskApplicationStatusId(userId, taskId, taskApplicationStatusId);
    }

    public TaskApplication findByUserAndTaskTypeAndStatus(Long userId, String type, Long taskApplicationStatusId){
        TaskApplication taskApplication = repository.findByUserAndTaskTypeAndStatus(userId, type, taskApplicationStatusId);
        return taskApplication ;
    }


    public String generateOrderReference(){
       String random = miscellaneousService.randomCode(8);
       String checkExists = "SELECT COUNT(ta.id) FROM TaskApplication ta WHERE ta.orderReference = :orderReference ";
       Query query = entityManager.createQuery(checkExists);
       query.setParameter("orderReference", random);
       Long count = (Long) query.getSingleResult();
       if(count>0){
           return generateOrderReference();
       }
       return random ;
    }





}
