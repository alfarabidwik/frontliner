package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.Banner;
import id.catchadeal.kerjayuk.repository.misc.BannerRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Service
public class BannerService extends BasicRepoService<Banner> {

    @Autowired BannerRepository repository ;


    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<Banner> findAll() {
        return repository.findAll(new Sort(Sort.Direction.ASC, "sortir"));
    }

    public List<Banner> findAll(Boolean active, Boolean ascending, String sortir) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActive(active, sort);
        }else{
            return repository.findAll(sort);
        }
    }


//    @Cacheable(cacheNames = Constant.CACHE_BANNERS)
    public List<Banner> activeBanners() {
        Date date = new Date();

        String sql = "SELECT b FROM Banner b WHERE b.active = true AND b.startDate <= :startDate AND b.endDate >= :endDate ORDER BY b.sortir ASC ";
        Query query = entityManager.createQuery(sql);
        query.setParameter("startDate", date);
        query.setParameter("endDate", date);
        List<Banner> banners = query.getResultList();

        return banners ;

    }

    public Boolean justExpired(Integer benchmarkNotifyInSecond){
        Date date = new Date();
        String sql = " SELECT count(b.id) FROM j_banner b LEFT JOIN " +
                "(SELECT b.id, " +
                "((DATE_PART('day', :endDate - b.end_date) * 24 + " +
                "DATE_PART('hour', :endDate - b.end_date)) * 60 + " +
                "DATE_PART('minute', :endDate - b.end_date)) * 60 + " +
                "DATE_PART('second', :endDate - b.end_date) as diff FROM j_banner b ) " +
                "as b_diff ON b_diff.id = b.id " +
                "where b.start_date < :startDate " +
                "AND b.active = true AND diff > 0 AND diff < :benchmarkNotifyInSecond GROUP BY b_diff.diff, b.created, b.end_date ORDER BY b.created DESC ";

        Query query = entityManager.createNativeQuery(sql);
        query.setFirstResult(0);
        query.setMaxResults(1);
        query.setParameter("startDate", date);
        query.setParameter("endDate", date);
        query.setParameter("benchmarkNotifyInSecond", benchmarkNotifyInSecond);
        try{
            BigInteger result = (BigInteger) query.getSingleResult();
            return result.intValue()>0;
        }catch (Exception e){
            return false ;
        }
    }

}
