package id.catchadeal.kerjayuk.controller.jurnal;

import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.service.jurnal.UserActivityService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.Utils;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class UserActivityController extends BasicController {

    @Autowired
    UserActivityService userActivityService;



    @GetMapping(path = "/userActivities")
    public WSResponse findAll(
            @RequestParam(required = false) String type,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
            @RequestParam(defaultValue = "") String userNames,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "created") String sortir,
            @RequestParam(defaultValue = "false") Boolean ascending){

        Utils.validateParam(type, sortir);


        List<String> stringUserNames = new ArrayList<>();
        if(userNames.contains(",")){
            String[] userIdArray = userNames.split(",");
            for (int i = 0; i < userIdArray.length; i++) {
                String name = userIdArray[i];
                stringUserNames.add(name.trim());
            }
        }else{
            userNames = userNames.trim();
            if(!userNames.isEmpty()){
                stringUserNames.add(userNames);
            }
        }
        DataPage<UserActivity> dataPage = userActivityService.findAll(type, startDate, endDate, stringUserNames, page, sortir, ascending);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage()).setPageElement(dataPage.getPageElement());
    }

    @GetMapping(path = "/userActivity/types")
    public WSResponse types(){
        List<AbstractMap.SimpleEntry> list = UserActivity.allTypeEntries();
        for (int i = 0; i < list.size(); i++) {
            String value = (String) list.get(i).getValue();
            list.get(i).setValue(message(value));
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, list);
    }


}
