package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.TaskApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface TaskApplicationRepository extends JpaRepository<TaskApplication, Long> {

    TaskApplication findByUserIdAndTaskId(Long userId, Long taskId);
    List<TaskApplication> findByUserId(Long userId);

    Boolean existsByUserIdAndTaskId(Long userId, Long taskId);
    Boolean existsByUserIdAndTaskIdAndTaskApplicationStatusId(Long userId, Long taskId, Long taskApplicationStatusId);

    Optional<TaskApplication> findByOrderReference(String orderReference);

    TaskApplication findByUserIdAndTaskIdAndTaskApplicationStatusId(Long userId, Long taskId, Long taskApplicationStatusId);

    @Query("SELECT ta FROM TaskApplication ta LEFT JOIN FETCH ta.pvTaskApplicationStatuses WHERE ta.id = :id ")
    Optional<TaskApplication> findById(@Param("id") Long id);

    @Query("SELECT ta FROM TaskApplication ta WHERE ta.user.id = :userId AND ta.task.type = :type AND ta.taskApplicationStatus.id = :taskApplicationStatusId")
    TaskApplication findByUserAndTaskTypeAndStatus(@Param("userId") Long userId, @Param("type") String type, @Param("taskApplicationStatusId") Long taskApplicationStatusId);



}
