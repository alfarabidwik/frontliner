package id.catchadeal.kerjayuk.exception;

import lombok.Getter;
import lombok.Setter;

public class PartnerGatewayException extends RuntimeException {

    @Getter@Setter
    private int code ;
    @Getter@Setter
    private String messageError ;

    public PartnerGatewayException(int code, String message){
        super(message);
        this.code = code;
    }

    public PartnerGatewayException(int code, String message, String messageError){
        super(message);
        this.messageError = messageError ;
        this.code = code;
    }


}
