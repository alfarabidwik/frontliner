package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.business.TaskApplicationStatusService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.user.RegistrationService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "${api}")
public class TaskApplicationStatusController extends BasicController {

    @Autowired
    TaskApplicationStatusService taskApplicationStatusService ;

    @GetMapping(path = "/taskApplicationStatuses")
    @UniversalRest
    @ApiOperation(value = "", response = WSRegistrations.class)
    public WSResponse findAll(
            @RequestParam(defaultValue = "true") Boolean ascending,
            @RequestParam(defaultValue = "sortir") String sortir) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskApplicationStatusService.findAll(Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir)));
    }


}
