package id.catchadeal.kerjayuk.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FrontlinerUserNoPasswordCSV {

    @CsvBindByName String fullname = "";
    @CsvBindByName(required = true) String email ;
    @CsvBindByName String mobilePhone ;
    @CsvBindByName String organization ;


}
