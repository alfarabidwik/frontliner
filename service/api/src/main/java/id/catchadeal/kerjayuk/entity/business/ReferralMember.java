package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.EBaseDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@Data
@Table(name="j_referral_member", indexes = {
        @Index(columnList = "created", name = "ReferralMember_created_idx"),
        @Index(columnList = "updated", name = "ReferralMember_updated_idx"),
        @Index(columnList = "active", name = "ReferralMember_active_idx"),
})
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class ReferralMember extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("referralMembers")
    @JoinColumn(name="task_application_id", nullable = false, updatable = false)
    TaskApplication taskApplication ;


    @ManyToOne(fetch= FetchType.LAZY)
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("taskApplications")
    @JoinColumn(name="user_id", nullable = false, updatable = false)
    User user ;

}
