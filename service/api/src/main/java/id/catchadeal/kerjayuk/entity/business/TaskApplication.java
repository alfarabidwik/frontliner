package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.business.ReferralMemberDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.business.TaskPartnerStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Table(name="j_task_application", indexes = {
        @Index(columnList = "created", name = "TaskApplication_created_idx"),
        @Index(columnList = "updated", name = "TaskApplication_updated_idx"),
        @Index(columnList = "active", name = "TaskApplication_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class TaskApplication extends EBase {


    @ManyToOne(fetch= FetchType.LAZY)
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("taskApplications")
    @JoinColumn(name="user_id", nullable = false)
    User user ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("taskApplications")
    @JoinColumn(name="task_id", nullable = false)
    Task task;

    @OneToMany(mappedBy = "taskApplication", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("taskApplication")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    Set<PVTaskApplicationStatus> pvTaskApplicationStatuses = new HashSet<>();

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("taskApplications")
    @JoinColumn(name="task_application_status_id")
    TaskApplicationStatus taskApplicationStatus;

    @Column(nullable = false, unique = true, columnDefinition = "TEXT default ''")
    String orderReference ;
    @Column(columnDefinition = "TEXT default ''")
    String partnerOrderReference ;

    @Type(type = "jsonb")
    @Column(name = "task_partner_statuses", columnDefinition = "jsonb")
    Set<TaskPartnerStatus> taskPartnerStatuses = new HashSet<>();

    String groupFormName ;

    @Transient
    String viewOrderReference ;

    @OneToMany(mappedBy = "taskApplication", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("taskApplication")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    List<FormLaneApplication> formLaneApplications = new ArrayList<>();

    @OneToMany(mappedBy = "taskApplication", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("taskApplication")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    Set<ReferralMember> referralMembers = new HashSet<>();

    BigDecimal itemFee = new BigDecimal(0);
    BigDecimal totalFee = new BigDecimal(0);



    @Override
    public void initTransient() {
        super.initTransient();
        mergeActiveStatus(user);

        if(taskApplicationStatus ==null){
            if(pvTaskApplicationStatuses !=null && pvTaskApplicationStatuses.size()>0){
                taskApplicationStatus = pvTaskApplicationStatuses.iterator().next().getTaskApplicationStatus();
            }
        }

        if(StringUtils.isEmpty(viewOrderReference)){
            if(StringUtils.isNotEmpty(orderReference)){
                this.viewOrderReference = orderReference;
            }
            if(StringUtils.isNotEmpty(partnerOrderReference)){
                this.viewOrderReference = partnerOrderReference;
            }
        }
        if(itemFee==null || itemFee.compareTo(new BigDecimal(0))==0){
            this.itemFee = task.getFee();
        }

        if(totalFee==null || totalFee.compareTo(new BigDecimal(0))==0){
            if(task.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
                BigDecimal totalFee = new BigDecimal(0);
                if(referralMembers!=null){
                    for (ReferralMember referralMember : referralMembers) {
                        totalFee = totalFee.add(itemFee);
                    }
                }
                this.totalFee = totalFee ;
            }
            if(task.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
                this.totalFee = itemFee;
            }
        }
    }


}
