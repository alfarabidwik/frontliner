package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.ReferralMember;
import id.catchadeal.kerjayuk.repository.business.ReferralMemberRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ReferralMemberService extends BasicRepoService<ReferralMember> {

    @Autowired private ReferralMemberRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }

}
