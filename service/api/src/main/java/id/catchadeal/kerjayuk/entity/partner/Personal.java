package id.catchadeal.kerjayuk.entity.partner;

import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Table(name="j_personal", indexes = {
        @Index(columnList = "created", name = "Personal_created_idx"),
        @Index(columnList = "updated", name = "Personal_updated_idx"),
        @Index(columnList = "active", name = "Personal_active_idx"),
})
//@Table(name="j_personal")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Personal extends EBase {


    @Override
    public void initTransient() {
        super.initTransient();

    }


}
