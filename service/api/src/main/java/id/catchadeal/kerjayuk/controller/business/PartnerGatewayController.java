package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.business.PVTaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.business.Task;
import id.catchadeal.kerjayuk.entity.business.TaskApplication;
import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.PartnerGatewayException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskPartnerStatus;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.service.business.TaskApplicationService;
import id.catchadeal.kerjayuk.service.business.TaskApplicationStatusService;
import id.catchadeal.kerjayuk.service.business.TaskService;
import id.catchadeal.kerjayuk.util.Constant;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(path = "${api-gateway}")
public class PartnerGatewayController extends BasicController {

    @Autowired TaskApplicationService taskApplicationService;
    @Autowired TaskApplicationStatusService taskApplicationStatusService;
    @Autowired TaskService taskService;

    public static final String[] CATCHADEAL_POSITIVE_RESPONSE = new String[]{"2","3","7","8","5","6","13"};
    public static final String[] CATCHADEAL_NEGATIVE_RESPONSE = new String[]{"1","4","10","11","12","9"};

    @PostMapping(path = "/partner/order/new", consumes = { "multipart/form-data" })
    @UniversalRest
    public WSResponse order(@RequestParam String agentCode, @RequestParam String callbackId, @RequestParam String orderReference, @RequestParam String statusCode, @RequestParam String statusLabel, @RequestParam String statusInfo){
        User user = userService.findByAgentCodeAndUserStatusIsActive(agentCode);
        Task task = taskService.findByCallbackIdAndTaskCategoryIdAndPublishedIsTrue(callbackId, TaskCategoryDto.PRODUCT_KEUANGAN_ID);
        TaskApplication taskApplication = new TaskApplication();
        taskApplication.setUser(user);
        taskApplication.setPartnerOrderReference(orderReference);
        taskApplication.setOrderReference(taskApplicationService.generateOrderReference());
        taskApplication.setTask(task);
        TaskPartnerStatus taskPartnerStatus = new TaskPartnerStatus();
        taskPartnerStatus.setStatusOrder(1);
        taskPartnerStatus.setCreated(new Date());
        taskPartnerStatus.setStatusCode(statusCode);
        taskPartnerStatus.setStatusLabel(statusLabel);
        taskPartnerStatus.setStatusInfo(statusInfo);
        taskApplication.getTaskPartnerStatuses().add(taskPartnerStatus);
        taskApplication = taskApplicationService.save(taskApplication);
        taskApplication = taskApplicationService.updateStatus(taskApplication, TaskApplicationStatus.TAKEN.getId(), statusInfo);
        if(StringUtils.isEmpty(statusInfo)){
            notificationService.sendSpesific(taskApplication.getUser(),  message("task.application.status.changes.title", taskApplication.getTask().getTitle()), statusInfo,
                    taskApplication.getId(), TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

    @PostMapping(path = "/partner/order/track", consumes = { "multipart/form-data" })
    @UniversalRest
    public WSResponse order(@RequestParam String orderReference,
                            @RequestParam Integer statusOrder,
                            @RequestParam String statusCode,
                            @RequestParam String statusLabel,
                            @RequestParam(required = false) String statusInfo,
                            @RequestParam(required = false) Boolean finalStatus){

        TaskApplication taskApplication = taskApplicationService.findByOrderReference(orderReference);
        TaskPartnerStatus taskPartnerStatus = new TaskPartnerStatus();
        taskPartnerStatus.setStatusOrder(statusOrder);
        taskPartnerStatus.setCreated(new Date());
        taskPartnerStatus.setStatusCode(statusCode);
        taskPartnerStatus.setStatusLabel(statusLabel);
        taskPartnerStatus.setStatusInfo(statusInfo);
        taskApplication.getTaskPartnerStatuses().add(taskPartnerStatus);
        taskApplication = taskApplicationService.save(taskApplication);
        if(StringUtils.isEmpty(statusInfo)){
            notificationService.sendSpesific(taskApplication.getUser(),  message("task.application.status.changes.title", taskApplication.getTask().getTitle()), statusInfo,
                    taskApplication.getId(), TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());
        }
        if(finalStatus!=null && finalStatus){
            for (int i = 0; i < CATCHADEAL_NEGATIVE_RESPONSE.length; i++) {
                String negativeCode = CATCHADEAL_NEGATIVE_RESPONSE[i];
                if(statusCode.equalsIgnoreCase(negativeCode)){
                    taskApplicationService.updateStatus(taskApplication, TaskApplicationStatus.REJECTED.getId(), statusInfo);
                    notificationService.sendSpesific(taskApplication.getUser(),  message("task.application.rejected.title", taskApplication.getTask().getTitle()), StringUtils.isNotEmpty(statusInfo)?statusInfo:message("task.application.rejected.description"),
                            taskApplication.getId(), TaskApplicationDto.class, taskApplication.getTask().getPartner().getImageUri());
                    break;
                }
            }
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }






}