package id.catchadeal.kerjayuk.repository.role;

import id.catchadeal.kerjayuk.entity.role.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    /**
     * Without JOIN FETCH the associations could be fetched from cache not from database (HIBERNATE BUG)
     * */
    String FORCE_FETCH_QUERY = "SELECT DISTINCT r FROM Role r LEFT JOIN fetch r.roleMenus rm ";

    @Query(FORCE_FETCH_QUERY+" WHERE r.active = ?1 ")
    List<Role> findByActive(Boolean active);

    @Query(FORCE_FETCH_QUERY+" WHERE r.id = ?1 ")
    Optional<Role> findById(Long aLong);
}
