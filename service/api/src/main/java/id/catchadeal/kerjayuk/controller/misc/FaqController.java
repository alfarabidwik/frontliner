package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.Faq;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.FaqDto;
import id.catchadeal.kerjayuk.service.misc.FaqService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;

@RestController
@RequestMapping(path = "${api}")
public class FaqController extends BasicController {

    @Autowired FaqService faqService;
    @Autowired FileStorage fileStorage;
    @Value("${faq.image.dir}") String faqImageDir ;



    @GetMapping(path = "/faqs")
    @AdminRest@FrontendRest
    @ApiOperation(value = "")
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "created") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, faqService.findAll(active,search, sortir, ascending));
    }

    @PostMapping(path = "/faq/save")
    @AdminRest
    @ApiOperation(value = "")
    public WSResponse save(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody @Valid FaqDto faqDto){
        Faq faq = modelMapper.map(faqDto, Faq.class);
        boolean newdata = faq.getId()==null || faq.getId()==0 ;
        faq = faqService.save(faq);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, faq);
    }
    @PostMapping(path = "/faq/saveUpload")
    @AdminRest
    @ApiOperation(value = "")
    public WSResponse saveUpload(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestPart String faqDtoGson, @RequestParam(required = false) MultipartFile multipartFile) throws Exception{
        if(multipartFile!=null){
            BufferedImage image = ImageIO.read(multipartFile.getInputStream());
            Integer width = image.getWidth();
            Integer height = image.getHeight();
        }
        FaqDto faqDto = gson.fromJson(faqDtoGson, FaqDto.class);
        Faq faq = modelMapper.map(faqDto, Faq.class);

        faq = faqService.save(faq);
        if(multipartFile!=null){
            String filename = String.valueOf(System.nanoTime());
            String image = fileStorage.storeFile(faqImageDir, String.valueOf(faq.getId()), filename, multipartFile);
            faq.setImage(image);
            faq = faqService.save(faq);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, faq);
    }


    @PostMapping(path = "/faq/uploadImage")
    @AdminRest
    public WSResponse uploadImage(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestParam Long faqId, @RequestParam MultipartFile multipartFile) throws Exception{
        Faq faq = faqService.findById(faqId);
        if(multipartFile!=null){
            String filename = String.valueOf(System.nanoTime());
            String image = fileStorage.storeFile(faqImageDir, String.valueOf(faq.getId()), filename, multipartFile);
            faq.setImage(image);
            faq = faqService.save(faq);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, faq.getImageLink());
        }
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }


    @GetMapping(path = "/faq/delete/{id}")
    @AdminRest
    public WSResponse delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable Long id){
        Faq faq = faqService.findById(id);
        faqService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

}
