package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.TaskApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TaskApplicationStatusRepository extends JpaRepository<TaskApplicationStatus, Long> {

}
