package id.catchadeal.kerjayuk.service.mail;

import id.catchadeal.kerjayuk.entity.email.SendEmail;
import id.catchadeal.kerjayuk.model.email.SendEmailDto;
import id.catchadeal.kerjayuk.model.email.SendEmailPreference;
import id.catchadeal.kerjayuk.repository.email.SendEmailRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.user.UserService;
import id.catchadeal.kerjayuk.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


@Service
public class SendEmailService extends BasicRepoService<SendEmail> {

    public static final Logger logger = LoggerFactory.getLogger(SendEmailService.class);

    @Autowired SendEmailRepository repository ;
    @Value("${spring.mail.username}") String emailFrom ;
    @Value("${email.html.file.dir}") String emailHtmlFileDir ;
    @Value("${email.attachment.file.dir}") String attachmentFileDir ;

    @Autowired EmailService emailService ;
    @Autowired AttachmentService attachmentService ;
    @Autowired UserService userService ;

    @Override
    public JpaRepository repository() {
        return repository ;
    }

    public Page<SendEmail> findAll(String sortir, Boolean ascending, Integer page, String search) {
        return repository.findBySubjectContainingIgnoreCase(search, PageRequest.of(page, pageRow, Utils.sort(ascending, sortir)));
    }

    @Transactional
    public SendEmail send(SendEmail sendEmail) throws Exception{
        sendEmail = save(sendEmail);
        if(sendEmail.getRegularSender()){
            sendEmail.setSender(configuration().getRegularEmailSender());
        }
        if(sendEmail.getAllUser()){
            List<SendEmailPreference> emailPreferenceForUsers = new ArrayList<>();
            List<String> emails = userService.emails(Utils.sort(true, "created"));
            if(emails!=null && emails.size()>0){
                emails.forEach(s -> {
                    SendEmailPreference sendEmailPreference = new SendEmailPreference();
                    sendEmailPreference.setEmail(s);
                    emailPreferenceForUsers.add(sendEmailPreference);
                });
            }
            sendEmail.setUserEmails(emailPreferenceForUsers);
        }


        /**
         * Send only if email is send directly
         * */
        if(sendEmail.getSendDirectly()){
            HashSet<String> emailTos = new HashSet<>();
            List<SendEmailPreference> userEmails = new ArrayList<>();
            for (int i = 0; i < sendEmail.getUserEmails().size() ; i++) {
                SendEmailPreference userEmail = sendEmail.getUserEmails().get(i);
                String email = userEmail.getEmail();
                if(!emailTos.contains(email)){
                    try{
                        if(sendEmail.getRegularSender()){
                            emailService.sendEmail(sendEmail.getSubject(), email, sendEmail.getContent());
                        }else{
                            emailService.sendEmail(sendEmail.getSubject(), email, sendEmail.getSender(), sendEmail.getContent());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        userEmail.setResponseStatus(e.getMessage());
                    }
                    emailTos.add(email);
                    userEmails.add(userEmail);
                }
            }
            sendEmail.setSendAt(new Date());
            sendEmail.setSendStatus(SendEmail.SENT);
            sendEmail.setUserEmails(userEmails);
        }else{

            HashSet<String> emailTos = new HashSet<>();
            List<SendEmailPreference> userEmails = new ArrayList<>();
            for (int i = 0; i < sendEmail.getUserEmails().size() ; i++) {
                SendEmailPreference userEmail = sendEmail.getUserEmails().get(i);
                String email = userEmail.getEmail();
                if(!emailTos.contains(email)){
                    emailTos.add(email);
                    userEmails.add(userEmail);
                }
            }
            sendEmail.setUserEmails(userEmails);
            sendEmail.setSendStatus(SendEmail.SCHEDULED);
        }
        sendEmail = save(sendEmail);
        return sendEmail;
    }


    //@Transactional
    public SendEmail sendScheduled(SendEmail sendEmail) throws Exception{
        sendEmail = save(sendEmail);
        if(sendEmail.getAllUser()){
            List<SendEmailPreference> emailPreferenceForUsers = new ArrayList<>();
            List<String> emails = userService.emails(Utils.sort(true, "created"));
            if(emails!=null && emails.size()>0){
                emails.forEach(s -> {
                    SendEmailPreference sendEmailPreference = new SendEmailPreference();
                    sendEmailPreference.setEmail(s);
                    emailPreferenceForUsers.add(sendEmailPreference);
                });
            }
            sendEmail.setUserEmails(emailPreferenceForUsers);
        }

        /**
         * Send only if email is send directly
         * */
        HashSet<String> emailTos = new HashSet<>();
        List<SendEmailPreference> userEmails = new ArrayList<>();
        for (int i = 0; i < sendEmail.getUserEmails().size() ; i++) {
            SendEmailPreference userEmail = sendEmail.getUserEmails().get(i);
            String email = userEmail.getEmail();
            if(!emailTos.contains(email)){
                try{
                    if(sendEmail.getRegularSender()){
                        emailService.sendEmail(sendEmail.getSubject(), email, sendEmail.getContent());
                    }else{
                        emailService.sendEmail(sendEmail.getSubject(), email, sendEmail.getSender(), sendEmail.getContent());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    userEmail.setResponseStatus(e.getMessage());
                }
                emailTos.add(email);
                userEmails.add(userEmail);
            }
        }
        sendEmail.setUserEmails(userEmails);

        sendEmail.setSendStatus(SendEmail.SENT);
        sendEmail = save(sendEmail);

        return sendEmail;
    }


    //@Transactional
    public SendEmail send(SendEmailDto sendEmailDto) throws Exception{
        SendEmail sendEmail = mapper.map(sendEmailDto, SendEmail.class);
        return send(sendEmail);
    }


    public Boolean justScheduled(Integer benchmarkNotifyInSecond){
        Date currentDate = new Date();
        String sql = " SELECT count(se.id) FROM j_send_email se LEFT JOIN " +
                "(SELECT se.id, " +
                "((DATE_PART('day', :currentDate - se.send_at) * 24 + " +
                "DATE_PART('hour', :currentDate - se.send_at)) * 60 + " +
                "DATE_PART('minute', :currentDate - se.send_at)) * 60 + " +
                "DATE_PART('second', :currentDate - se.send_at) as diff FROM j_send_email se ) " +
                "as se_diff ON se_diff.id = se.id " +
                "where se.send_directly = false AND se.send_status = 'scheduled' " +
                "AND diff > 0 AND diff < :benchmarkNotifyInSecond GROUP BY se_diff.diff, se.created, se.send_at ORDER BY se.created DESC ";

        Query query = entityManager.createNativeQuery(sql);
        query.setFirstResult(0);
        query.setMaxResults(1);
        query.setParameter("currentDate", currentDate);
        query.setParameter("benchmarkNotifyInSecond", benchmarkNotifyInSecond);
        try{
            BigInteger result = (BigInteger) query.getSingleResult();
            return result.intValue()>0;
        }catch (Exception e){
            return false ;
        }
    }


    public List<SendEmail> scheduledEmails(Integer benchmarkNotifyInSecond){
        Date currentDate = new Date();
        String sql = " SELECT se.id FROM j_send_email se LEFT JOIN " +
                "(SELECT se.id, " +
                "((DATE_PART('day', :currentDate - se.send_at) * 24 + " +
                "DATE_PART('hour', :currentDate - se.send_at)) * 60 + " +
                "DATE_PART('minute', :currentDate - se.send_at)) * 60 + " +
                "DATE_PART('second', :currentDate - se.send_at) as diff FROM j_send_email se ) " +
                "as se_diff ON se_diff.id = se.id " +
                "where se.send_directly = false AND se.send_status = 'scheduled' " +
                "AND diff > 0 AND diff < :benchmarkNotifyInSecond GROUP BY se.id, se_diff.diff, se.created, se.send_at ORDER BY se.created DESC ";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("currentDate", currentDate);
        query.setParameter("benchmarkNotifyInSecond", benchmarkNotifyInSecond);
        try{
            List<BigInteger> ids = query.getResultList();
            List<SendEmail> sendEmails = new ArrayList<>();
            for (int i = 0; i < ids.size(); i++) {
                SendEmail sendEmail = findById(ids.get(i).longValue());
                sendEmails.add(sendEmail);
            }
            return sendEmails;
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList<>() ;
        }

    }






}
