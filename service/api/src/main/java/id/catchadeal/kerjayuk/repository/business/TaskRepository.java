package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface TaskRepository extends JpaRepository<Task, Long> {

    Boolean existsByTaskCategoryId(Long jobcategoryId);

//    @Query("SELECT jv FROM Task jv LEFT JOIN FETCH jv.taskApplications ja LEFT JOIN FETCH ja.taskApplicationStatus jas WHERE jv.id = ?1")
//    Optional<Task> findById(Long aLong);


    Optional<Task> findByCallbackIdAndTaskCategoryIdAndPublishedIsTrue(String callbackId, Long taskCategoryId);

    Boolean existsByType(String type);

}
