package id.catchadeal.kerjayuk.entity.misc;

import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Table(name="m_task_category")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class TaskCategory extends NonIdEBase {


    @Column(nullable = false, name = "sortir", columnDefinition = "integer default 0")
    int sortir ;
    String name ;
    String defaultIcon ;
    String hexaColor ;
    String topic ;


    @Transient
    String defaultIconUrl ;

    @Transient
    Long count ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task t WHERE t.task_category_id = id)")
    Long totalTask = 0l ;

    @Formula(value = "(SELECT COUNT(t.id) from j_task t WHERE t.task_category_id = id AND (" +
            " (t.active = true) AND (t.published = true) AND ((t.period_type = '"+ TaskDto.NEVER_END +"') OR (t.period_type = '"+ TaskDto.PERIODIC +"' AND t.start_period <= NOW() AND t.end_period >= NOW())) " +
            " AND ((t.application_limit_type = '"+TaskDto.NO_LIMIT+"') OR (t.application_limit_type = '"+TaskDto.LIMITED+"' AND t.application_limit_count > t.application_count)))" +
            ")")
    Long totalAvailableTask = 0l ;



    @Override
    public void initTransient() {
        super.initTransient();
        defaultIconUrl = Constant.prefixApi+ Constant.REST_TASK_CATEGORY_ICON+"/"+defaultIcon;
        if(count==null){
            try{
                Query query = BasicRepoService.staticEntityManager.createQuery("SELECT COUNT(t.id) FROM Task t where t.taskCategory.id = :id ").setParameter("id", getId());
                count = (Long) query.getSingleResult();
            }catch (Exception e){
                e.printStackTrace();
                count = 0l;
            }
        }
    }

}
