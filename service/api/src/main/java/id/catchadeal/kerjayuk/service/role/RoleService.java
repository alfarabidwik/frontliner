package id.catchadeal.kerjayuk.service.role;

import id.catchadeal.kerjayuk.entity.role.Role;
import id.catchadeal.kerjayuk.repository.role.RoleRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoleService extends BasicRepoService<Role> {

    @Autowired RoleRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public List<Role> findAll(Boolean active) {
        if(active!=null){
            return repository.findByActive(active);
        }else{
            return repository.findAll();
        }
    }

    public Boolean existsById(Long id){
        return repository.existsById(id);
    }

    


}
