package id.catchadeal.kerjayuk.service.region;

import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.repository.region.VillageRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VillageService extends BasicRepoService<Village> {

    @Autowired VillageRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Cacheable(cacheNames = "Villages1")
    @Override
    public List<Village> findAll() {
        return repository.findAll();
    }

    @Cacheable(cacheNames = "Villages2", key = "{#active, #sort}")
    public List<Village> findAll(Boolean active, Sort sort) {
        if(active!=null){
            return repository.findByActive(active, sort);
        }else{
            return repository.findAll(sort);
        }
    }

    @Cacheable(cacheNames = "Villages3", key = "{#active, #page, #sortir, #ascending}")
    public Page<Village> findAll3(Boolean active, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActive(active, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findAll(PageRequest.of(page, pageRow, sort));
        }
    }

    @Cacheable(cacheNames = "Village", key = "#id")
//    @Override
    public Village findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public Village save(Village village) {
        return super.save(village);
    }

    @Cacheable(cacheNames = "Villages4", key = "{#active, #districtId, #sort}")
    public List<Village> findByDistrictId(Boolean active, Long districtId, Sort sort){
        if(active!=null){
            return repository.findByActiveAndDistrictId(active, districtId, sort);
        }else{
            return repository.findByDistrictId(districtId, sort);
        }
    }

    @Cacheable(cacheNames = "Villages5", key = "{#active, #districtId, #page, #sortir, #ascending}")
    public Page<Village> findAll4(Boolean active, Long districtId, int page, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        if(active!=null){
            return repository.findByActiveAndDistrictId(active, districtId, PageRequest.of(page, pageRow, sort));
        }else{
            return repository.findByDistrictId(districtId, PageRequest.of(page, pageRow, sort));
        }
    }



}
