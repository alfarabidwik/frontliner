package id.catchadeal.kerjayuk.repository.role;

import id.catchadeal.kerjayuk.entity.role.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleMenuRepository extends JpaRepository<RoleMenu, Long> {

    List<RoleMenu> findByRoleId(Long roleId);

}
