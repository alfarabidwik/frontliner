package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.Company;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CompanyRepository extends JpaRepository<Company, Long> {

}
