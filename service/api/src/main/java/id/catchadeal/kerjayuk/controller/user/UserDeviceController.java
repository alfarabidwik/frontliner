package id.catchadeal.kerjayuk.controller.user;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.entity.user.UserDevice;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.service.user.UserDeviceService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class UserDeviceController extends BasicController {

    @Autowired UserDeviceService userDeviceService ;

    @PostMapping("/userDevice/refresh")
    @FrontendRest
    public WSResponse refreshDevice(@RequestHeader(Constant.AUTHORIZATION) String authorization,
                                    @RequestParam String deviceId,
                                    @RequestParam String platform,
                                    @RequestParam String fcmToken){
        User user = restUtils.user(authorization);
        if(!user.isUserActive()){
            return WSResponse.instance(Constant.FAILED_CODE, "Device id is not found");
        }
        List<UserDevice> userDevices = userDeviceService.findByDeviceIdAndUserId(deviceId, user.getId(), Sort.by(Sort.Direction.DESC, "created"));
        if(userDevices!=null && userDevices.size()>0){
            UserDevice singleUserDevice = null ;
            for (int i = 0; i < userDevices.size(); i++) {
                if(i==0){
                    singleUserDevice = userDevices.get(i);
                }
                if(i>0){
                    userDeviceService.delete(userDevices.get(i).getId());
                }
            }
            if(singleUserDevice!=null){
                singleUserDevice.setUser(user);
                singleUserDevice.setFcmToken(fcmToken);
                singleUserDevice.setPlatform(platform);
                singleUserDevice = userDeviceService.save(singleUserDevice);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
            }
        }
        return WSResponse.instance(Constant.FAILED_CODE, "Device id is not found");
    }

    @GetMapping("/userDevices")
    @FrontendRest@AdminRest
    public WSResponse findAll(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) Integer page,
            @RequestParam(defaultValue = "false") Boolean ascending, @RequestParam(defaultValue = "created") String sortir) {
        DataPage dataPage = userDeviceService.findAll(search, userId, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas())
                .setPageElement(dataPage.getPageElement())
                .setTotalElement(dataPage.getTotalElement())
                .setTotalPage(dataPage.getTotalPage());
    }
    @GetMapping("/userDevice/delete/{userDeviceId}")
    @FrontendRest@AdminRest
    public WSResponse delete(
            @PathVariable(required = false) Long userDeviceId) {
        userDeviceService.delete(userDeviceId);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }


}
