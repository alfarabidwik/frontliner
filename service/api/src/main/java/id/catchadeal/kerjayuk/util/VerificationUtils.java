package id.catchadeal.kerjayuk.util;

import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.security.JwtTokenProvider;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VerificationUtils {

    @Autowired JwtTokenProvider jwtTokenProvider ;
    @Autowired UserService userService ;
    @Autowired AdminService adminService ;

    public User validateVerificationStatus(String authorization){
        User user = jwtTokenProvider.getUser(authorization);
//        if(!user.getEmployee().isIdCardVerification()){
//            throw new AppException(Constant.FAILED_CODE, "You have unverified id card and selfie photo, please update and wait until our administrator approve it");
//        }
        return user ;
    }

}
