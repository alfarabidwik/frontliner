package id.catchadeal.kerjayuk.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="j_user_device", indexes = {
        @Index(columnList = "deviceId,role", name = "UserDevice_role_idx", unique = false),
        @Index(columnList = "created", name = "UserDevice_created_idx"),
        @Index(columnList = "updated", name = "UserDevice_updated_idx"),
        @Index(columnList = "active", name = "UserDevice_active_idx"),
})
//@Table(name="j_user_device")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class UserDevice extends EBase {

    public static final String ANDROID = "ANDROID";
    public static final String IOS = "IOS";

    String fcmToken ;
    String platform ;
    String deviceId ;
    String role ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("userDevices")
    @JoinColumn(name="user_id")
    User user;


    @Override
    public void initTransient() {
        super.initTransient();
        mergeActiveStatus(user);
    }
}

