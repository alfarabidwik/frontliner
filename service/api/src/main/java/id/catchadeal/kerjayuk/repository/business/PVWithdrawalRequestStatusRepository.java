package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.PVWithdrawalRequestStatus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PVWithdrawalRequestStatusRepository extends JpaRepository<PVWithdrawalRequestStatus, Long> {

}
