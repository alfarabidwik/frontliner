package id.catchadeal.kerjayuk.controller.role;

import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.role.Menu;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.role.MenuDto;
import id.catchadeal.kerjayuk.service.role.MenuService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class MenuController extends BasicController {

    @Autowired MenuService menuService;


    @GetMapping(path = "/menus")
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, menuService.findAll(active));
    }

    @GetMapping(path = "/menu/{id}")
    @Override
    public WSResponse findById(Long id) {
        Menu menu = menuService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, menu);
    }

    @PostMapping(path = "/menu/save")
    public WSResponse save(@RequestBody @Valid MenuDto menuDto){
        Menu menu = modelMapper.map(menuDto, Menu.class);
        menu = menuService.save(menu);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, menu);
    }

    @GetMapping(path = "/menu/delete/{id}")
    public WSResponse delete(@PathVariable Long id){
        menuService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

    @GetMapping(path = "/menus/tree")
    public WSResponse tree(
            @RequestParam(required = false) Boolean active){
        List<Menu> menus = menuService.findAll(active);

        HashMap<String, Menu> keyParentMenu = new HashMap<>();
        if(menus!=null){
            Collections.<Menu>sort(menus, (menu1, menu2) -> menu1.getSortir()-menu2.getSortir());

            menus.forEach(menu -> {
                String nameMenu = menu.getName();
                String groupMenu = menu.getGroupMenu();
                if(nameMenu.equalsIgnoreCase(groupMenu)){
                    keyParentMenu.put(menu.getName(), menu);
                }
            });
            menus.forEach(menu -> {
                String nameMenu = menu.getName();
                String groupMenu = menu.getGroupMenu();
                boolean hasParent = false ;
                if(!groupMenu.equalsIgnoreCase(nameMenu) && keyParentMenu.containsKey(groupMenu)){
                    hasParent = true ;
                    Menu parentMenu = keyParentMenu.get(groupMenu);
                    parentMenu.getChildMenus().add(menu);
                }
                if(!hasParent){
                    keyParentMenu.put(menu.getName(), menu);
                }
            });

        }
        List<Menu> resultMenus = new ArrayList<>();
        keyParentMenu.forEach((s, menu) -> {
            resultMenus.add(menu);
        });
        Collections.<Menu>sort(resultMenus, (menu1, menu2) -> menu1.getSortir()-menu2.getSortir());

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, resultMenus);
    }

}
