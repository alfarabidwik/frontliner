package id.catchadeal.kerjayuk.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * http://dolszewski.com/spring/sending-html-mail-with-spring-boot-and-thymeleaf/
 * https://memorynotfound.com/spring-mail-sending-email-thymeleaf-html-template-example/
* */
@Service
public class EmailService {


    @Value("${spring.mail.username}") String emailFrom ;
    @Value("${public.domain}") String publicDomain ;

    @Autowired
    ResourceLoader resourceLoader ;


    @Autowired JavaMailSender javaMailSender ;
    @Autowired MailContentBuilder mailContentBuilder ;

    public void simpleSend(String to, String message){
        if(to==null){
            return;
        }
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(emailFrom);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject("Subject Test");
        simpleMailMessage.setText(message);
        javaMailSender.send(simpleMailMessage);
    }

    public void sendEmail(String subject, String to, String content, File... attachments){
        try{
            if(to==null){
                return;
            }
            Resource resource = resourceLoader.getResource("classpath:/templates/catchadeal.svg");
            final FileSystemResource image = new FileSystemResource(resource.getFile());
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                messageHelper.setFrom(emailFrom);
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);
                messageHelper.setText(content, true);
                if(attachments!=null && attachments.length>0){
                    for (int i = 0; i < attachments.length; i++) {
                        File file = attachments[i];
                        messageHelper.addAttachment(file.getName(), file);
                    }
                }
                messageHelper.addInline("logo", image);
            };
            javaMailSender.send(messagePreparator);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sendEmail(String subject, String to, String from, String content, File... attachments){
        try{
            if(to==null){
                return;
            }
            Resource resource = resourceLoader.getResource("classpath:/templates/catchadeal.svg");
            final FileSystemResource image = new FileSystemResource(resource.getFile());
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                messageHelper.setFrom(from);
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);
                messageHelper.setText(content, true);
                if(attachments!=null && attachments.length>0){
                    for (int i = 0; i < attachments.length; i++) {
                        File file = attachments[i];
                        messageHelper.addAttachment(file.getName(), file);
                    }
                }
                messageHelper.addInline("logo", image);
            };
            javaMailSender.send(messagePreparator);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public void sendEmail(String subject, String to, String thyMeLeafFileName, HashMap<String, Object> map){
        try{
            if(to==null){
                return;
            }
            map.put("public_web", publicDomain);
            Resource resource = resourceLoader.getResource("classpath:/templates/catchadeal.svg");
            final FileSystemResource image = new FileSystemResource(resource.getFile());
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                messageHelper.setFrom(emailFrom);
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);
                String content = mailContentBuilder.build(thyMeLeafFileName, map);
                messageHelper.setText(content, true);
                messageHelper.addInline("logo", image);
            };
            javaMailSender.send(messagePreparator);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void sendDefaultTemplate(String subject, String to, String thyMeLeafFileName, HashMap<String, Object> map){
        try{
            if(to==null){
                return;
            }
            map.put("public_web", publicDomain);
            Resource resource = resourceLoader.getResource("classpath:/templates/catchadeal.svg");
            final FileSystemResource image = new FileSystemResource(resource.getFile());
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                messageHelper.setFrom(emailFrom);
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);
                messageHelper.addInline("logo", image);
                String content = mailContentBuilder.build(thyMeLeafFileName, map);
                messageHelper.setText(content, true);

            };
            javaMailSender.send(messagePreparator);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sendPassword(String email, String password) throws Exception{
        Map<String, Object> map = new HashMap<>();
        map.put("password", password);
        String content = mailContentBuilder.build("request_resend_password", map);
        sendEmail("Resend Password", email, content);
    }


}
