package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.BalanceMutation;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.business.MutationInfo;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.business.BalanceMutationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.service.user.UserService;
import id.catchadeal.kerjayuk.util.DateUtility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;

@Service
public class BalanceMutationService extends BasicRepoService<BalanceMutation> {

    @Autowired private BalanceMutationRepository repository ;
    @Autowired private UserService userService;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }


    public DataPage<BalanceMutation> balanceMutationDataPage(Long userId, String search, Date startDate, Date endDate, Integer page, Boolean ascending, String sortir){

        Map<String, Object> parameterMap = new HashMap<>();
        String selectObject = " SELECT bm ";
        String queryFrom = " FROM BalanceMutation bm LEFT JOIN FETCH bm.user u ";
        String whereClause = " WHERE 1+1=2 ";
        String groupBy = " GROUP BY bm.id, bm.created, u.id ";
        String orderBy = " ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");


        if(userId!=null){
            whereClause = whereClause+" AND u.id = :userId ";
            parameterMap.put("userId", userId);

        }
        if(StringUtils.isNotEmpty(search)){
            whereClause = whereClause+" AND (" +
                    "LOWER(CONCAT(COALESCE(u.firstname, ''), ' ', COALESCE(u.lastname, ''))) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.email, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.mobilePhone, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    "OR " +
                    "LOWER(COALESCE(u.agentCode, '')) LIKE LOWER(CONCAT('%',LOWER(:search),'%')) " +
                    ") ";
            parameterMap.put("search", search);
        }
        if(startDate!=null && endDate!=null){
            whereClause = whereClause+" AND ( bm.created >= :startDate AND bm.created <= :endDate)";
            parameterMap.put("startDate", DateUtility.startOfDay(startDate));
            parameterMap.put("endDate", DateUtility.endOfDay(endDate));
        }

        String finalQuery = selectObject+queryFrom+whereClause+groupBy+orderBy;

        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createQuery(finalQuery);
            for(String key :parameterMap.keySet()) {
                queryTotal.setParameter(key, parameterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            countResult = 0 ;
        }

        Query query = entityManager.createQuery(finalQuery);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);
        for(String key :parameterMap.keySet()) {
            query.setParameter(key, parameterMap.get(key));
        }

        List<BalanceMutation> balanceMutations = query.getResultList();



        return DataPage.builder(balanceMutations, pageRow, countResult);
    }

    public void cancelPayment(User user, BigDecimal creditAmount, MutationInfo mutationInfo){
        BalanceMutation balanceMutation = BalanceMutation.build(user, null, creditAmount, mutationInfo);
        save(balanceMutation);
        BigDecimal currentBalance = user.getCurrentBalance();
        BigDecimal totalIncome = user.getTotalIncome();

        user.setCurrentBalance(currentBalance.add(creditAmount));
        user = userService.save(user);
    }


    public void creditBalance(User user, BigDecimal creditAmount, MutationInfo mutationInfo){
        BalanceMutation balanceMutation = BalanceMutation.build(user, null, creditAmount, mutationInfo);
        save(balanceMutation);
        BigDecimal currentBalance = user.getCurrentBalance();
        BigDecimal totalIncome = user.getTotalIncome();

        user.setCurrentBalance(currentBalance.add(creditAmount));
        user.setTotalIncome(totalIncome.add(creditAmount));
        user = userService.save(user);
    }

    public BalanceMutation debitBalance(User user, BigDecimal debitAmount, MutationInfo mutationInfo){
        BalanceMutation balanceMutation = BalanceMutation.build(user, debitAmount, null, mutationInfo);
        save(balanceMutation);
        BigDecimal currentBalance = user.getCurrentBalance();
        BigDecimal totalIncome = user.getTotalIncome();

        user.setCurrentBalance(currentBalance.subtract(debitAmount));
        user = userService.save(user);
        return balanceMutation ;
    }



}
