package id.catchadeal.kerjayuk.controller.payment;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.payment.Bank;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.model.wsresponse.payment.WSBank;
import id.catchadeal.kerjayuk.model.wsresponse.payment.WSBanks;
import id.catchadeal.kerjayuk.service.payment.BankService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;

@RestController
@RequestMapping(path = "${api}")
public class BankController extends BasicController {

    @Autowired BankService bankService;
    @Autowired FileStorage fileStorage;
    @Value("${bank.image.dir}") String bankImageDir ;



    @GetMapping(path = "/banks")
    @AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSBanks.class)
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(defaultValue = "name") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bankService.findAll(active, sortir, ascending));
    }

    @PostMapping(path = "/bank/save")
    @AdminRest
    @ApiOperation(value = "", response = WSBank.class)
    public WSResponse save(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody @Valid BankDto bankDto){
        Bank bank = modelMapper.map(bankDto, Bank.class);
        boolean newdata = bank.getId()==null || bank.getId()==0 ;
        bank = bankService.save(bank);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bank);
    }
    @PostMapping(path = "/bank/saveUpload")
    @AdminRest
    @ApiOperation(value = "", response = WSBank.class)
    public WSResponse saveUpload(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestPart String bankDtoGson, @RequestParam(required = false) MultipartFile multipartFile) throws Exception{
        if(multipartFile!=null){
            BufferedImage image = ImageIO.read(multipartFile.getInputStream());
            Integer width = image.getWidth();
            Integer height = image.getHeight();
        }
        BankDto bankDto = gson.fromJson(bankDtoGson, BankDto.class);
        Bank bank = modelMapper.map(bankDto, Bank.class);

        bank = bankService.save(bank);
        if(multipartFile!=null){
            String filename = String.valueOf(System.nanoTime());
            String image = fileStorage.storeFile(bankImageDir, String.valueOf(bank.getId()), filename, multipartFile);
            bank.setImage(image);
            bank = bankService.save(bank);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bank);
    }


    @PostMapping(path = "/bank/uploadImage")
    @AdminRest
    public WSResponse uploadImage(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestParam Long bankId, @RequestParam MultipartFile multipartFile) throws Exception{
        Bank bank = bankService.findById(bankId);
        if(multipartFile!=null){
            String filename = String.valueOf(System.nanoTime());
            String image = fileStorage.storeFile(bankImageDir, String.valueOf(bank.getId()), filename, multipartFile);
            bank.setImage(image);
            bank = bankService.save(bank);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bank.getImageLink());
        }
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }


    @GetMapping(path = "/bank/delete/{id}")
    @AdminRest
    public WSResponse delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable Long id){
        Bank bank = bankService.findById(id);
        bankService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

}
