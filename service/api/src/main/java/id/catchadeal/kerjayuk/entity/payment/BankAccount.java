package id.catchadeal.kerjayuk.entity.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="j_bank_account")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class BankAccount extends EBase {



    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("bankAccounts")
    @JoinColumn(name="bank_id", nullable = false)
    Bank bank ;

    String customerName ;
    String accountCode ;
//    String mobileAppsCellNumber ;
//    String smsBankingSenderId ;
//    String smsBankingTemplate ;
//    String smsPrefixIndicator ;
//    String currencyFormat ;





}
