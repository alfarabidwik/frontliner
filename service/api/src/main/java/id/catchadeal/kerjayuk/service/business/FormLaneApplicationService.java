package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.FormApplication;
import id.catchadeal.kerjayuk.entity.business.FormLaneApplication;
import id.catchadeal.kerjayuk.repository.business.FormApplicationRepository;
import id.catchadeal.kerjayuk.repository.business.FormLaneApplicationRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class FormLaneApplicationService extends BasicRepoService<FormLaneApplication> {

    @Autowired private FormLaneApplicationRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }

}
