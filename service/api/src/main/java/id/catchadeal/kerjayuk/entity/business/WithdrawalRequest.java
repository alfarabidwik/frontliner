package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.user.User;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name="j_withdrawal_request", indexes = {
        @Index(columnList = "created", name = "WitdrawalRequest_created_idx"),
        @Index(columnList = "updated", name = "WitdrawalRequest_updated_idx"),
        @Index(columnList = "active", name = "WitdrawalRequest_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawalRequest extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("withdrawalRequests")
    @JoinColumn(name="user_id", updatable = false, nullable = false)
    User user ;

    @Column(nullable = false)
    BigDecimal amount = new BigDecimal(0);

    @OneToMany(mappedBy = "withdrawalRequest", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties({"withdrawalRequest", "withdrawalStatus"})
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    Set<PVWithdrawalRequestStatus> pvWithdrawalRequestStatuses = new HashSet<>();

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties({"withdrawalRequests", "withdrawalStatus"})
    @JoinColumn(name="withdrawal_status_id")
    WithdrawalStatus withdrawalStatus;


    @Transient
    String imageUrl ;

    @Override
    public void initTransient() {
        super.initTransient();
        if(withdrawalStatus==null && pvWithdrawalRequestStatuses.size()>0){
            withdrawalStatus = pvWithdrawalRequestStatuses.iterator().next().getWithdrawalStatus();
        }
//        if(pvWithdrawalRequestStatuses!=null){
//            pvWithdrawalRequestStatuses.forEach(pvWithdrawalRequestStatus -> {
//                if(StringUtils.isNotEmpty(pvWithdrawalRequestStatus.getImageUrl())){
//                    this.imageUrl = pvWithdrawalRequestStatus.getImageUrl();
//                }
//            });
//        }
    }
}
