package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.Task;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.business.TaskRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;

@Service
public class TaskService extends BasicRepoService<Task> {

    public static final Logger logger = LoggerFactory.getLogger(TaskService.class);

    @Autowired private TaskRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }



    public DataPage tasks(Long partnerId, String search, Boolean published, Long taskCategoryId,
                          BigDecimal feeStart, BigDecimal feeEnd, Integer page, Boolean ascending, String sortir){
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sqlObject = "SELECT DISTINCT jt ";

        String sql = " FROM Task jt LEFT JOIN FETCH jt.partner p WHERE jt.active = true ";

        if(partnerId!=null){
            sql = sql+" AND ( p.id = :partnerId ) ";
            paramaterMap.put("partnerId", partnerId);
        }

        if(StringUtils.isNotEmpty(search)){
            sql = sql+" AND ( " +
                    "LOWER(COALESCE(jt.title, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(jt.description, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(jt.partner.fullName, '')) LIKE CONCAT('%',LOWER(:search),'%') " +
                    "OR LOWER(COALESCE(jt.taskCategory.name, '')) LIKE CONCAT('%',LOWER(:search),'%') )";
            paramaterMap.put("search", search);
        }

        if(published!=null){
            sql = sql+" AND jt.published = :published ";
            paramaterMap.put("published", published);
        }
        if(taskCategoryId!=null){
            sql = sql+" AND jt.taskCategory.id = :taskCategoryId ";
            paramaterMap.put("taskCategoryId", taskCategoryId);
        }
        if(feeStart!=null){
            sql = sql+" AND jt.fee >= :feeStart ";
            paramaterMap.put("feeStart", feeStart);
        }
        if(feeEnd!=null){
            sql = sql+" AND jt.fee <= :feeEnd ";
            paramaterMap.put("feeEnd", feeEnd);
        }


        String groupBy = " GROUP BY jt.id, p.id ";
        String orderBy = " ORDER BY "+sortir+" "+(ascending?" ASC ":" DESC ");


        String finalSql = sqlObject+sql+groupBy+orderBy;

        int offset = page*pageRow;
        long countResult = 0 ;

        try {
            Query queryTotal = entityManager.createQuery(finalSql);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            e.printStackTrace();
            countResult = 0 ;
        }
        Query query = entityManager.createQuery(finalSql);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<Task> tasks = query.getResultList();

        DataPage dataPage = DataPage.builder(tasks, pageRow, countResult);
        return dataPage ;
    }


    public DataPage published(Long partnerId, String keyword, Long taskCategoryId,
                          BigDecimal feeStart, BigDecimal feeEnd,
                          Date startDate, Date endDate,
                          String workerVerificationType, Boolean suggestionMode,
                          Integer page, Boolean ascending, String sortir){
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String sqlObject = "SELECT DISTINCT jt ";

        String sql = " FROM Task jt LEFT JOIN FETCH jt.partner p WHERE jt.active = true ";

        if(partnerId!=null){
            sql = sql+" AND ( p.id = :partnerId ) ";
            paramaterMap.put("partnerId", partnerId);
        }

        if(StringUtils.isNotEmpty(keyword)){
            sql = sql+" AND ( LOWER(COALESCE(jt.title, '')) LIKE CONCAT('%',LOWER(:keyword),'%') OR LOWER(COALESCE(jt.description, '')) LIKE CONCAT('%',LOWER(:keyword),'%') )";
            paramaterMap.put("keyword", keyword);
        }

        sql = sql+" AND jt.published = :published ";
        paramaterMap.put("published", true);
        if(taskCategoryId!=null){
            sql = sql+" AND jt.taskCategory.id = :taskCategoryId ";
            paramaterMap.put("taskCategoryId", taskCategoryId);
        }
        if(feeStart!=null && feeEnd!=null){
            sql = sql+" AND jt.fee >= :feeStart ";
            paramaterMap.put("feeStart", feeStart);
            sql = sql+" AND jt.fee <= :feeEnd ";
            paramaterMap.put("feeEnd", feeEnd);
        }

        sql = sql+" AND ( (jt.periodType = :periodic AND jt.startPeriod <= :currentDate AND jt.endPeriod >= :currentDate) OR (jt.periodType = :neverEnd) )";
        paramaterMap.put("periodic", TaskDto.PERIODIC);
        paramaterMap.put("neverEnd", TaskDto.NEVER_END);
        paramaterMap.put("currentDate", new Date());

        if(startDate!=null && endDate!=null){
            sql = sql+" AND ( (jt.periodType = :periodic AND :startDate <= jt.startPeriod AND :endDate >= jt.startPeriod ) " +
                    "OR (jt.periodType = :periodic AND :startDate >= jt.startPeriod AND :startDate >= jt.endPeriod ) " +
                    "OR (jt.periodType = :neverEnd) )";
            paramaterMap.put("periodic", TaskDto.PERIODIC);
            paramaterMap.put("neverEnd", TaskDto.NEVER_END);
            paramaterMap.put("startDate", startDate);
            paramaterMap.put("endDate", endDate);
        }

        sql = sql+" AND ((jt.applicationLimitType = :limited AND jt.applicationLimitCount > jt.applicationCount) OR jt.applicationLimitType >= :noLimit ) ";
        paramaterMap.put("limited", TaskDto.LIMITED);
        paramaterMap.put("noLimit", TaskDto.NO_LIMIT);

        if(StringUtils.isNotEmpty(workerVerificationType)){
            sql = sql+" AND jt.workerVerificationType = :workerVerificationType ";
            paramaterMap.put("workerVerificationType", workerVerificationType);
        }

        if(suggestionMode!=null){
            sql = sql+" AND jt.suggestion != null AND jt.suggestion > 0 ";
        }


        String groupBy = " GROUP BY jt.id, p.id ";
        String orderBy = " ORDER BY "+sortir+" "+(ascending?" ASC ":" DESC ");


        String finalSql = sqlObject+sql+groupBy+orderBy;

        int offset = page*pageRow;
        long countResult = 0 ;

        try {
            Query queryTotal = entityManager.createQuery(finalSql);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            e.printStackTrace();
            countResult = 0 ;
        }
        Query query = entityManager.createQuery(finalSql);
        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<Task> tasks = query.getResultList();

        DataPage dataPage = DataPage.builder(tasks, pageRow, countResult);
        return dataPage ;
    }



    public Boolean existsByTaskCategoryId(Long taskcategoryId){
        return existsByTaskCategoryId(taskcategoryId);
    }

    public Boolean existsByType(String type){
        return repository.existsByType(type);
    }



    public Task findByCallbackIdAndTaskCategoryIdAndPublishedIsTrue(String callbackId, Long taskCategoryId){
        return repository.findByCallbackIdAndTaskCategoryIdAndPublishedIsTrue(callbackId, taskCategoryId).orElseThrow(() -> {
            return new AppException(Constant.FAILED_CODE, "task.is.not.found");
        });
    }

}
