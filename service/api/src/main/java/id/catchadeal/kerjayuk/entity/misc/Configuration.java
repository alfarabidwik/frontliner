package id.catchadeal.kerjayuk.entity.misc;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.region.District;
import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.util.Constant;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Table(name="m_configuration")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Configuration extends EBase {



    String workingAgreementFile ;

    @Transient
    String workingAgreementFileUrl ;

    @Column(nullable = false, columnDefinition = "TEXT default ''")
    String name ;
    @Column(nullable = false, columnDefinition = "TEXT default ''")
    String companyName = "" ;
    @Column(columnDefinition = "TEXT")
    String companyDescription = "";

    @Column(nullable = true)
    String companyAddress ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="company_village_id", nullable = true)
    Village companyVillage ;

    String companyPhoneNumber ;
    String companyFaximile ;
    String contactPersonPhoneNumber = "" ;
    String contactWhatsapp ;

    Integer pageRowSize = 10 ;

    String hashtag ;
    String instagramLink ;
    String facebookLink ;
    String twitterLink ;

    @Column(columnDefinition = "numeric(19,2) default 0")
    BigDecimal minWithdrawal = new BigDecimal(0) ;

    @Column(columnDefinition = "TEXT")
    String facebookAppId ;
    @Column(columnDefinition = "TEXT")
    String googleClientId ;

    @Transient
    String baseApi ;

    @Transient int bannerWidth ;
    @Transient int bannerHeight ;
    @Transient int mobileBannerWidth ;
    @Transient int mobileBannerHeight ;

    @Transient String regularEmailSender ;
    @Transient String mailDomain ;


    @Transient int taskBannerWidth ;
    @Transient int taskBannerHeight ;


    @PreUpdate
    @PrePersist
    @Override
    public void prePersist() {
        super.prePersist();
        initTransient();
    }

    @PostUpdate
    @PostPersist
    public void postPersist() {
        super.postPersist();
        initTransient();
    }

    @PostLoad
    @Override
    public void postLoad() {
        super.postLoad();
        initTransient();
    }


    @Override
    public void initTransient() {
        super.initTransient();
        if(pageRowSize==null){
            pageRowSize = 10 ;
        }
        if(minWithdrawal==null){
            minWithdrawal = new BigDecimal(0);
        }

        if(workingAgreementFile!=null && !workingAgreementFile.isEmpty()){
            workingAgreementFileUrl = Constant.prefixApi+ Constant.REST_CONFIGURATION_WORKING_AGREEMENT_DOCUMENT+"/"+this.getId()+"/"+workingAgreementFile ;
        }
        if(StringUtils.isEmpty(baseApi)){
            baseApi = Constant.prefixApi ;
        }
    }


}
