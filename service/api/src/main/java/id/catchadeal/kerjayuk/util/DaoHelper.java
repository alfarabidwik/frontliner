package id.catchadeal.kerjayuk.util;

import id.catchadeal.kerjayuk.entity.BasicField;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Map;

public class DaoHelper {


    public static Long id(Object object){
        try{
            if(object==null){
                return null ;
            }
            return ((BasicField) object).getId();
        }catch (Exception e){
            e.printStackTrace();
        }
        throw new RuntimeException("Object is not an Ebase Or NonIdEbase");
    }

    public static Object setId(Object object, Long id){
        try{
            if(object==null){
                return null ;
            }
            ((BasicField) object).setId(id);
            return object;
        }catch (Exception e){
            e.printStackTrace();
        }
        throw new RuntimeException("Object is not an Ebase Or NonIdEbase");
    }


    public static String cleanColumnName(String name){
        int len = name.length();
        StringBuilder nameBuilder = new StringBuilder("");
        for (int j = 0; j < len; j++) {
            char ch = name.charAt(j);
            // check char is in uppercase or lower case
            if (ch >= 'A' && ch <= 'Z') {
                nameBuilder.append("_").append(String.valueOf(ch).toLowerCase());
            } else {
                nameBuilder.append(ch);
            }
        }
        return nameBuilder.toString().trim();
    }


    public static long rowCount(EntityManager entityManager, String objectToCount, String resQuery, Map<String, Object> parameterMap){
        long countResult = 0 ;
        try {
            Query queryCount = entityManager.createQuery(" SELECT "+objectToCount+" "+resQuery);
            for(String key :parameterMap.keySet()) {
                queryCount.setParameter(key, parameterMap.get(key));
            }
            countResult = (long) queryCount.getResultList().size();
        }catch (Exception e){
            e.printStackTrace();
            countResult = 0 ;
        }
        return countResult ;

    }

}
