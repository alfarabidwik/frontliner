package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.Personal;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonalRepository extends JpaRepository<Personal, Long> {

}
