package id.catchadeal.kerjayuk.entity.report;

import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_report_category")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class ReportCategory extends EBase {

    String name ;
    @Column(columnDefinition = "TEXT")
    String description ;


    @Override
    public void initTransient() {
        super.initTransient();
    }

}
