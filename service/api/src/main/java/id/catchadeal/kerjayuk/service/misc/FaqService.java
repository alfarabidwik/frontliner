package id.catchadeal.kerjayuk.service.misc;//package com.alfa.ecommerce.service;

import id.catchadeal.kerjayuk.entity.misc.Faq;
import id.catchadeal.kerjayuk.repository.misc.FaqRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class FaqService extends BasicRepoService<Faq> {

    @Autowired
    FaqRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<Faq> findAll() {
        return repository.findAll(new Sort(Sort.Direction.DESC, "updated"));
    }


    public List<Faq> findAll(Boolean active, String search, String sortir, Boolean ascending) {
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        List<Faq> faqs = new ArrayList<>();
        if(active!=null){
            faqs = repository.findByActive(active, search, sort);
        }else{
            faqs = repository.findBySearch(search, sort);
        }
        return faqs;
    }

//    @Override
    public Faq findById(Long id) {
        return super.findById(id);
    }

    public void delete(Long id) {
        super.delete(id);
    }

    //@Transactional
    @Override
    public Faq save(Faq customer) {
        return super.save(customer);
    }





}
