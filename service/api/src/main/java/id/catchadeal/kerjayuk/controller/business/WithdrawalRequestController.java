package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.business.BalanceMutation;
import id.catchadeal.kerjayuk.entity.business.PVWithdrawalRequestStatus;
import id.catchadeal.kerjayuk.entity.business.WithdrawalRequest;
import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import id.catchadeal.kerjayuk.entity.jurnal.DescriptionVariable;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.entity.misc.Notification;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.MutationInfo;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.business.BalanceMutationService;
import id.catchadeal.kerjayuk.service.business.PVWithdrawalRequestStatusService;
import id.catchadeal.kerjayuk.service.business.WithdrawalRequestService;
import id.catchadeal.kerjayuk.service.payment.BankService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.NumberUtil;
import id.catchadeal.kerjayuk.util.Utils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

/**
 * TODO CREATE ACTIVITY LOG FOR ADMIN OR USER FROM THIS CLASS TO A BOTTOM FROM THIS CLASS INDEX
 * */

@RestController
@RequestMapping(path = "${api}")
public class WithdrawalRequestController extends BasicController {

    @Autowired WithdrawalRequestService withdrawalRequestService ;
    @Autowired PVWithdrawalRequestStatusService pvWithdrawalRequestStatusService ;
    @Autowired BalanceMutationService balanceMutationService ;
    @Autowired BankService bankService ;

    @GetMapping(path = "/withdrawalRequests")
    @AdminRest@FrontendRest
    public WSResponse withdrawalRequests(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) Long withdrawalStatusId,
            @RequestParam(defaultValue = "") String search,
            @ApiParam(example = DateAppConfig.PARAM_DATE_FORMAT, required = false) @RequestParam(required = false) @DateTimeFormat(pattern = DateAppConfig.PARAM_DATE_FORMAT) Date startDate,
            @ApiParam(example = DateAppConfig.PARAM_DATE_FORMAT, required = false) @RequestParam(required = false) @DateTimeFormat(pattern = DateAppConfig.PARAM_DATE_FORMAT) Date endDate,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "wr.created") String sortir) {

        DataPage dataPage = withdrawalRequestService.withdrawalRequestDataPage(userId, withdrawalStatusId, search, startDate, endDate, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas())
                .setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());
    }


    @PostMapping(path = "/withdrawalRequest/request")
    @FrontendRest
    public WSResponse request(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam BigDecimal amount) {
        User user = jwtTokenProvider.getUser(authorization) ;
        if(!user.isUserActive()){
            throw new AppException(Constant.FAILED_CODE, message("your.not.allowed.to.request.withdrawal"));
        }
        if(!user.isVerified()){
            throw new AppException(Constant.FAILED_CODE, message("please.make.your.account.verified.first"));
        }

        WithdrawalRequest withdrawalRequest = withdrawalRequestService.request(user, amount);
        notificationService.sendSpesific(withdrawalRequest.getUser(), NotificationDto.WITHDRAWAL, message("balance.confirmation.title"),
                message("balance.confirmation.description", NumberUtil.moneyFormat(withdrawalRequest.getAmount(), true)), withdrawalRequest.getId(), withdrawalRequest.getClass(), null);

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, withdrawalRequest);
    }

    @GetMapping(path = "/withdrawalRequest/checkIfRequestExist")
    @FrontendRest
    public WSResponse checkIfRequestExist(@RequestHeader(Constant.AUTHORIZATION) String authorization) {
        User user = jwtTokenProvider.getUser(authorization) ;
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, withdrawalRequestService.checkIfExist(user));
    }



    @PostMapping(path = "/withdrawalRequest/changeStatus")
    @AdminRest@FrontendRest
    public WSResponse changeStatus(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam Long withdrawalRequestId,
            @RequestParam Long withdrawalStatusId,
            @RequestParam(required = false) String note,
            @RequestParam(required = false) String receiptCode,
            @RequestParam(required = false) MultipartFile multipartFile,
            @RequestParam(required = false) String accountNumber,
            @RequestParam(required = false) Long bankId) throws Exception{
        Admin admin = null ;
        if(jwtTokenProvider.isAdmin(authorization)){
            admin = jwtTokenProvider.getAdmin(authorization);
        }
        WithdrawalRequest withdrawalRequest = withdrawalRequestService.findById(withdrawalRequestId);

        PVWithdrawalRequestStatus pvWithdrawalRequestStatus = PVWithdrawalRequestStatus.build(withdrawalRequest, WithdrawalStatus.build(withdrawalStatusId), admin, note);
        pvWithdrawalRequestStatus = pvWithdrawalRequestStatusService.save(pvWithdrawalRequestStatus);
        if(withdrawalStatusId.equals(WithdrawalStatus.ISSUED.getId())){
            if(multipartFile==null){
                throw new AppException(Constant.FAILED_CODE, "multipart.receipt.cannot.be.null");
            }
            if(StringUtils.isEmpty(accountNumber)){
                throw new AppException(Constant.FAILED_CODE, "account.number.cannot.be.empty");
            }
            if(bankId==null){
                throw new AppException(Constant.FAILED_CODE, "bank.id.cannot.be.empty");
            }
            pvWithdrawalRequestStatus.setReceiptCode(receiptCode);
            pvWithdrawalRequestStatus.setAccountNumber(accountNumber);
            pvWithdrawalRequestStatus.setBank(bankService.findById(bankId));

            String filename = String.valueOf(System.nanoTime());
            String image = fileStorage.storeFile(withdrawalReceiptImageDir, String.valueOf(pvWithdrawalRequestStatus.getId()), filename, multipartFile);
            pvWithdrawalRequestStatus.setImage(image);
            pvWithdrawalRequestStatus = pvWithdrawalRequestStatusService.save(pvWithdrawalRequestStatus);

        }


        withdrawalRequest.setWithdrawalStatus(WithdrawalStatus.build(withdrawalStatusId));
        withdrawalRequest = withdrawalRequestService.save(withdrawalRequest);

        withdrawalRequest.getPvWithdrawalRequestStatuses().add(pvWithdrawalRequestStatus);
        if(withdrawalStatusId.equals(WithdrawalStatus.ISSUED.getId())){
            notificationService.sendSpesific(withdrawalRequest.getUser(), NotificationDto.WITHDRAWAL, message("balance.success.title"),
                    StringUtils.isNotEmpty(note)?note:message("balance.success.description", NumberUtil.moneyFormat(withdrawalRequest.getAmount(), true)), withdrawalRequest.getId(), withdrawalRequest.getClass(), null);
            balanceMutationService.debitBalance(withdrawalRequest.getUser(), withdrawalRequest.getAmount(), MutationInfo.build(WithdrawalRequest.class.getName(),
                    withdrawalRequestId, message("disbursement.for.withdrawal.request")));
            userService.updateWithdrawalRequest(true, withdrawalRequest.getAmount(), withdrawalRequest.getUser());
        }
        if(withdrawalStatusId.equals(WithdrawalStatus.REJECTED.getId())){
            notificationService.sendSpesific(withdrawalRequest.getUser(), NotificationDto.WITHDRAWAL, message("balance.rejected.title"),
                    StringUtils.isNotEmpty(note)?note:message("balance.rejected.description"), withdrawalRequest.getId(), withdrawalRequest.getClass(), null);
            userService.updateWithdrawalRequest(true, withdrawalRequest.getAmount(), withdrawalRequest.getUser());
        }
        if(withdrawalStatusId.equals(WithdrawalStatus.CANCELED.getId())){
            notificationService.sendSpesific(withdrawalRequest.getUser(), NotificationDto.WITHDRAWAL, message("balance.cancelled.title"),
                    StringUtils.isNotEmpty(note)?note:message("balance.cancelled.description", NumberUtil.moneyFormat(withdrawalRequest.getAmount(), true)), withdrawalRequest.getId(), withdrawalRequest.getClass(), null);
            userService.updateWithdrawalRequest(false, withdrawalRequest.getAmount(), withdrawalRequest.getUser());
//            balanceMutationService.cancelPayment(withdrawalRequest.getUser(), withdrawalRequest.getAmount(), MutationInfo.build(WithdrawalRequest.class.getName(),
//                    withdrawalRequestId, message("disbursement.cancellation.for.withdrawal.request")));
        }

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, withdrawalRequest);
    }



}
