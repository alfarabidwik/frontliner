package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.Configuration;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.ConfigurationDto;
import id.catchadeal.kerjayuk.model.wsresponse.misc.WSConfiguration;
import id.catchadeal.kerjayuk.service.misc.ConfigurationService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class ConfigurationController extends BasicController {


    @Autowired ConfigurationService configurationService ;
    @Value("${configuration.dir}") String configurationDir ;


    @GetMapping(path = "/configurations")
    public WSResponse findAll(
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "created") String sortir){
        Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, configurationService.findAll(sort));
    }


    @GetMapping(path = "/configuration")
    @UniversalRest@AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSConfiguration.class)
    public WSResponse configuration(){
        return WSResponse.instance(Constant.SUCCESS_CODE, configurationService.configuration());
    }

    @PostMapping(path = "/configuration/save")
    @AdminRest
    @ApiOperation(value = "", response = WSConfiguration.class)
    public WSResponse save(@RequestBody@Valid ConfigurationDto configurationData) {
        Configuration configuration = modelMapper.map(configurationData, Configuration.class);
        configuration = configurationService.save(configuration);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, configuration);
    }

    @PostMapping(path = "/configuration/uploadWorkingAgreement")
    @AdminRest
    public WSResponse uploadWorkingAgreement(@RequestParam MultipartFile multipartFile) throws Exception {
        Configuration configuration = configurationService.configuration();
        if(multipartFile!=null){
            String filename = String.valueOf(System.nanoTime());
            String file = fileStorage.storeFile(configurationDir, String.valueOf(configuration.getId()), filename, multipartFile);
            configuration.setWorkingAgreementFile(file);
            configuration = configurationService.save(configuration);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, configuration.getWorkingAgreementFileUrl());
        }
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }

    @GetMapping(path = "/entities")
    @UniversalRest@AdminRest@FrontendRest
    public WSResponse entities(@RequestHeader(Constant.AUTHORIZATION) String authorization){

        return WSResponse.instance(Constant.SUCCESS_CODE, configurationService.entities());
    }

    @GetMapping(path = "/configuration/{id}")
    @Override
    public WSResponse findById(Long id) {
        Configuration configuration = configurationService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, configuration);
    }

    @GetMapping("/configuration/activate")
    public WSResponse activate(@RequestParam Long configurationId){
        return WSResponse.instance(Constant.SUCCESS_CODE, configurationService.activate(configurationId));

    }

    @GetMapping(path = "/configuration/currentActive")
    public WSResponse currentActive() {
        return WSResponse.instance(Constant.SUCCESS_CODE, configurationService.configuration());
    }


}
