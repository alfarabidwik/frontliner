package id.catchadeal.kerjayuk.repository.misc;

import id.catchadeal.kerjayuk.entity.misc.WorkingDay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingDayRepository extends JpaRepository<WorkingDay, Long> {



}
