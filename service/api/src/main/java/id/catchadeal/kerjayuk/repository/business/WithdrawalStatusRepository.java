package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.WithdrawalStatus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WithdrawalStatusRepository extends JpaRepository<WithdrawalStatus, Long> {

}
