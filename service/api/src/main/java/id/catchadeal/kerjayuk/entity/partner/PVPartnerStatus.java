package id.catchadeal.kerjayuk.entity.partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="pv_partner_status", indexes = {
        @Index(columnList = "created", name = "PVPartnerStatus_created_idx"),
        @Index(columnList = "updated", name = "PVPartnerStatus_updated_idx"),
        @Index(columnList = "active", name = "PVPartnerStatus_active_idx"),
})
//@Table(name="pv_partner_status")
@Entity
@ToString(of = "id", callSuper = true)
public class PVPartnerStatus extends EBase {


    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties({"pvPartnerStatuses", "pvPartnerStatus"})
    @JoinColumn(name="partner_id", updatable = false, nullable = false)
    Partner partner;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("pvPartnerStatuses")
    @JoinColumn(name="partner_status_id", nullable = false)
    PartnerStatus partnerStatus;

    String cause ;

    @Column(columnDefinition = "TEXT")
    String note ;

    @Override
    public void initTransient() {
        super.initTransient();
        mergeActiveStatus(partner);
    }

}
