package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.Company;
import id.catchadeal.kerjayuk.repository.partner.CompanyRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CompanyService extends BasicRepoService<Company> {

    @Autowired private CompanyRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }
}
