package id.catchadeal.kerjayuk.entity.region;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Table(name = "m_district")
@Entity
@Data
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class District extends EBase implements Serializable {
    String name ;

    @ManyToOne(fetch=FetchType.LAZY)@JsonManagedReference/*Sementara*/
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="city_id")
    City city ;

    @OneToMany(mappedBy="district", fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.SELECT) @JsonIgnoreProperties("district")
    List<Village> villages ;


    String courierId ;

    @Override
    public void initTransient() {
        super.initTransient();

    }


}
