package id.catchadeal.kerjayuk.entity.email;

import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.model.email.SendEmailPreference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Table(name= SendEmail.TABLE)
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class SendEmail extends EBase {
    public static final String TABLE = "j_send_email";

    public static final String SENT = "sent";
    public static final String SCHEDULED = "scheduled";

    public static final Boolean MANUAL_CONTENT = true ;
    public static final Boolean UPLOAD_HTML = false ;


    private String subject ;
    @Column(columnDefinition = "TEXT")
    private String content ;
    private Boolean allUser ;
    private String sendStatus ;
    private Boolean sendDirectly ;
    private Date sendAt ;
    private Boolean regularSender ;

    private String sender ;
    private Boolean contentType  ;

    @Type(type = "jsonb")
    @Column(name = "user_emails", columnDefinition = "jsonb")
    List<SendEmailPreference> userEmails  = new ArrayList<>();

    @Transient
    private List<String> userReceivers = new ArrayList<>();

    @Transient
    private List<String> subscriberReceivers = new ArrayList<>();

    @Override
    public void initTransient() {
        super.initTransient();
        if(allUser==null){
            allUser = true ;
        }
        if(sendDirectly==null){
            sendDirectly = true ;
        }
        if(regularSender==null){
            regularSender = true ;
        }
        if(contentType==null){
            contentType = MANUAL_CONTENT;
        }
        if(userEmails!=null){
            userReceivers = new ArrayList<>();
            for (int i = 0; i < userEmails.size(); i++) {
                userReceivers.add(userEmails.get(i).getEmail());
            }
        }
    }



}
