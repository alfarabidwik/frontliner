package id.catchadeal.kerjayuk.controller;

import com.google.common.net.HttpHeaders;
import com.luciad.imageio.webp.WebPWriteParam;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.MediaTypeUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping(path = "${api}")
@ApiIgnore
public class ImageController extends BasicController{

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class.getName());


    @Autowired FileStorage fileStorage ;
    @Autowired ServletContext servletContext;

    @GetMapping(value = Constant.REST_USER_PHOTO+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> employeePhoto(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = userPhotoDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_EMPLOYEE_ID_CARD+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> idCard(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = userSelfieIdCardDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_TASK_CATEGORY_ICON+"/{filename}")
    public ResponseEntity<ByteArrayResource> taskCategoryIconImage(
            @PathVariable("filename") String filename) throws Exception {

        String path = taskCategoryDir+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_BANK_IMAGE+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> bankImage(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = bankImageDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_FAQ_IMAGE+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> faqImage(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = faqImageDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_TEMPORARY_IMAGE+"/{filename}")
    public ResponseEntity<ByteArrayResource> temporary(
            @PathVariable("filename") String filename) throws Exception {

        String path = temporaryDir+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }


    @GetMapping(value = Constant.REST_USER_SELFIE_ID_CARD+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> userSelfieIdCard(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = userSelfieIdCardDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(value = Constant.REST_PARTNER_ID_CARD+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> partnerIdCard(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = partnerIdCardDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }


    @GetMapping(value = Constant.REST_PARTNER_SIUP_OR_NPWP+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> siuOrNpwp(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = partnerSiupOrNpwpDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }


    @GetMapping(value = Constant.REST_PARTNER_IMAGE+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> partnerImage(
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = partnerImageDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, 0, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(Constant.REST_BANNER_IMAGE+"/{bannerId}/{filename}")
    public ResponseEntity<ByteArrayResource> bannerImage(
            HttpServletRequest request,
            @PathVariable("bannerId") String bannerId,
            @PathVariable("filename") String filename) throws Exception {

        String path = bannerPhotoDir+File.separator+bannerId+File.separator+filename;
        File compressedFile = compress(path, Constant.HIGH_QUALITY, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(Constant.REST_TASK_BANNER_IMAGE+"/{taskId}/{filename}")
    public ResponseEntity<ByteArrayResource> taskBannerImage(
            HttpServletRequest request,
            @PathVariable("taskId") String taskId,
            @PathVariable("filename") String filename) throws Exception {

        String path = taskBannerPhotoDir+File.separator+taskId+File.separator+filename;
        File compressedFile = compress(path, Constant.HIGH_QUALITY, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }

    @GetMapping(Constant.REST_USER_ID_CARD+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> userIdCard(
            HttpServletRequest request,
            @PathVariable("id") String id,
            @PathVariable("filename") String filename) throws Exception {

        String path = userIdCardDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, Constant.HIGH_QUALITY, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }
    @GetMapping(Constant.REST_WITHDRAWAL_RECEIPT_IMAGE+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> receiptImage(
            HttpServletRequest request,
            @PathVariable("id") String id,
            @PathVariable("filename") String filename) throws Exception {

        String path = withdrawalReceiptImageDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, Constant.HIGH_QUALITY, false);
        return grabFile(filename, compressedFile.getAbsolutePath());
    }


    @GetMapping(Constant.FORM_MEDIA_DIRECTORY+"/**")
    public ResponseEntity<ByteArrayResource> formImage(
            HttpServletRequest request) throws Exception {
        logger.debug("URI "+request.getRequestURI());

        String requestUrl = request.getRequestURL().toString();
        logger.debug("URL "+requestUrl);
        requestUrl = requestUrl.replace(Constant.prefixApi, "");

//        String path = userIdCardDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(requestUrl, Constant.HIGH_QUALITY, false);
        return grabFile(compressedFile.getName(), compressedFile.getAbsolutePath());
    }

    @GetMapping(Constant.REST_ADMIN_PHOTO_PROFILE+"/{id}/{filename}")
    public ResponseEntity<ByteArrayResource> adminImage(
            HttpServletRequest request,
            @PathVariable("id") Long id,
            @PathVariable("filename") String filename) throws Exception {

        String path = adminPhotoDir+File.separator+id+File.separator+filename;
        File compressedFile = compress(path, Constant.HIGH_QUALITY, false);
        return grabFile(compressedFile.getName(), compressedFile.getAbsolutePath());
    }





    @Cacheable(cacheNames = "images", key = "{#pathFile, #filename}")
    public ResponseEntity<ByteArrayResource> grabFile(String filename, String pathFile) throws Exception{
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, filename);
        Path path = Paths.get(pathFile);
        byte[] data = Files.readAllBytes(path);
        ByteArrayResource resource = new ByteArrayResource(data);


        return ResponseEntity.ok()
                // Content-Disposition
                .cacheControl(CacheControl.maxAge(360, TimeUnit.DAYS).cachePublic())
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
                // Content-Type
                .contentType(mediaType) //
                // Content-Lengh
                .contentLength(data.length) //
                .body(resource);

    }

//    public File compress(String path, float quality, boolean forceAsJpeg) throws Exception{
//        File input = new File(path);
//        String extension = FilenameUtils.getExtension(input.getName());
//        if(extension==null || extension.isEmpty()){
//            // Force to hardcode as image/jpeg
//            extension = "jpeg";
//        }
//
//        if(forceAsJpeg){
//            extension = "jpeg";
//        }
//
//        BufferedImage image = ImageIO.read(input);
//
//        File output = new File("/tmp/"+input.getName());
//        if(output.exists()){
//            output.delete();
//        }
//        OutputStream out = new FileOutputStream(output);
//
//        ImageWriter writer =  ImageIO.getImageWritersByFormatName(extension).next();
//        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
//        writer.setOutput(ios);
//
//        ImageWriteParam param = writer.getDefaultWriteParam();
//        if (param.canWriteCompressed()){
//            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//            param.setCompressionQuality(quality);
//        }
//        writer.write(null, new IIOImage(image, null, null), param);
//
//        out.close();
//        ios.close();
//        writer.dispose();
//
//        return output ;
//    }


    public File compress(String path, float quality, boolean forceAsJpeg) throws Exception {
        File input = null;
        BufferedImage image = null;
        try{
            logger.debug("PATH = {} ", path);
            input = new File(path);
            image = ImageIO.read(input);

        }catch (Exception e){
            e.printStackTrace();
//            input =  ResourceUtils.getFile("classpath:static/img/no_image.png");
            input = new File(staticImageDir+"/no_image.png");
            image = ImageIO.read(input);
        }
//        image = ImageIO.read(input);
        String tempPath = "/tmp/"+FilenameUtils.getBaseName(input.getName())+".webp";
        File output = new File(tempPath);
        if(output.exists()){
            output.delete();
        }
        ImageWriter writer = ImageIO.getImageWritersByMIMEType("image/webp").next();
        WebPWriteParam param = new WebPWriteParam(writer.getLocale());
        param.setCompressionMode(WebPWriteParam.MODE_DEFAULT);
        writer.setOutput(new FileImageOutputStream(output));

        writer.write(null, new IIOImage(image, null, null), param);
        writer.dispose();

        return output ;
    }





}
