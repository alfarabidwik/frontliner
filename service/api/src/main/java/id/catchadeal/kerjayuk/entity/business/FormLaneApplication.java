package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="pv_form_lane_application", indexes = {
        @Index(columnList = "created", name = "FormLaneApplication_created_idx"),
        @Index(columnList = "updated", name = "FormLaneApplication_updated_idx"),
        @Index(columnList = "active", name = "FormLaneApplication_active_idx"),
})
@Data
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
public class FormLaneApplication extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("taskApplications")
    @JoinColumn(name="task_application_id")
    TaskApplication taskApplication;


    String title  ;
    @OneToMany(mappedBy = "formLaneApplication", fetch = FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)@JsonIgnoreProperties("formLaneApplication")
    @org.hibernate.annotations.OrderBy(clause="created DESC")
    List<FormApplication> formApplications = new ArrayList<>();
}
