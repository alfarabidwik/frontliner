package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "${api}")
public class MiscController extends BasicController {



    @GetMapping(path = "/misc/test")
    @UniversalRest
    public WSResponse test(@RequestParam String param) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, passwordEncoder.encode(param));
    }

}
