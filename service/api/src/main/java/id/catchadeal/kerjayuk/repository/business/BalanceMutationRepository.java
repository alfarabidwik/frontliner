package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.BalanceMutation;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BalanceMutationRepository extends JpaRepository<BalanceMutation, Long> {

}
