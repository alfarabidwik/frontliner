package id.catchadeal.kerjayuk.controller.misc;//package com.alfa.ecommerce.controller;

import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.misc.Banner;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.BannerDto;
import id.catchadeal.kerjayuk.service.misc.BannerService;
import id.catchadeal.kerjayuk.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class BannerController extends BasicController {

    public static final Logger logger = LoggerFactory.getLogger(BannerController.class);

    @Autowired BannerService bannerService;
    @Autowired FileStorage fileStorage;
    @Value("${banner.photo.dir}") String bannerPhotoDir ;



    @GetMapping(path = "/banners")
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(defaultValue = "true") Boolean ascending,
            @RequestParam(defaultValue = "sortir") String sortir){

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bannerService.findAll(active, ascending, sortir));
    }

    @GetMapping(path = "/banner/activeBanners")
    public WSResponse activeBanners(){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bannerService.activeBanners());
    }



    @GetMapping(path = "/banner/{id}")
    @Override
    public WSResponse findById(@PathVariable Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, bannerService.findById(id));
    }

    @PostMapping(path = "/banner/save")
    public WSResponse save(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody @Valid BannerDto bannerDto){
        Banner banner = modelMapper.map(bannerDto, Banner.class);
        boolean newdata = banner.getId()==null || banner.getId()==0 ;
        if(newdata){
            commitAdminActivity(authorization, AdminActivity.CREATE_NEW_BANNER, bannerDto);
        }else{
            commitAdminActivity(authorization, AdminActivity.UPDATE_BANNER, bannerDto, bannerService.findById(banner.getId()));
        }
        banner = bannerService.save(banner);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, banner);
    }

    @PostMapping(path = "/banner/saveUpload")
    public WSResponse saveUpload(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
            @RequestPart String bannerDtoGson, @RequestParam(required = false) MultipartFile imageMultipartFile) throws Exception{

        BannerDto bannerDto = gson.fromJson(bannerDtoGson, BannerDto.class);
        Banner banner = modelMapper.map(bannerDto, Banner.class);
        boolean newdata = banner.getId()==null || banner.getId()==0 ;
        if(newdata){
            commitAdminActivity(authorization, AdminActivity.CREATE_NEW_BANNER, bannerDto);
        }else{
            commitAdminActivity(authorization, AdminActivity.UPDATE_BANNER, bannerDto, bannerService.findById(banner.getId()));
        }
        banner = bannerService.save(banner);
        if(imageMultipartFile!=null){
            Date date = new Date();
            String filename = String.valueOf(date.getTime());
            String image = fileStorage.storeFile(bannerPhotoDir, String.valueOf(banner.getId()), filename, imageMultipartFile);
            banner.setImage(image);
            banner = bannerService.save(banner);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, banner);
    }

    @PostMapping(path = "/banner/uploadImage")
    public WSResponse uploadImage(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,@RequestParam Long bannerId, @RequestParam MultipartFile multipartFile) throws Exception{
        Banner banner = bannerService.findById(bannerId);
        if(multipartFile!=null){
            String filename = String.valueOf(System.currentTimeMillis());
            String image = fileStorage.storeFile(bannerPhotoDir, String.valueOf(banner.getId()), filename, multipartFile);
            banner.setImage(image);
            banner = bannerService.save(banner);
            commitAdminActivity(authorization, AdminActivity.UPLOAD_BANNER_PHOTO, filename);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, banner);
        }
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }

    @GetMapping(path = "/banner/delete/{id}")
    public WSResponse delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable Long id) {
        Banner banner = bannerService.findById(id);
        bannerService.delete(id);
        commitAdminActivity(authorization, AdminActivity.DELETE_BANNER, banner);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }
}
