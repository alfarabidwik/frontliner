package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.business.Task;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.misc.Notification;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.wsresponse.business.WSTasks;
import id.catchadeal.kerjayuk.model.wsresponse.business.WSTask;
import id.catchadeal.kerjayuk.service.business.TaskService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class TaskController extends BasicController {

    Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    TaskService taskService;


    @GetMapping(path = "task")
    @FrontendRest@AdminRest
    @Override
    @ApiOperation(value = "", response = WSTask.class)
    public WSResponse findById(@RequestParam Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, taskService.findById(id));
    }

    @GetMapping(path = "tasks")
    @AdminRest
    @ApiOperation(value = "", response = WSTasks.class)
    public WSResponse tasks(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                   @RequestParam(required = false) Long partnerId,
                                   @RequestParam(defaultValue = "") String search,
                                   @RequestParam(required = false) Boolean published,
                                   @RequestParam(required = false) Long taskCategoryId,
                                   @RequestParam(required = false) BigDecimal feeStart,
                                   @RequestParam(required = false) BigDecimal feeEnd,
                                   @RequestParam(defaultValue = "0") Integer page,
                                   @RequestParam(defaultValue = "true") Boolean ascending,
                                   @ApiParam(allowableValues = "jt.updated, jt.publisher, jt.title, jt.fee, p.fullName", required = true) @RequestParam(defaultValue = "jt.updated") String sortir){
        DataPage dataPage = taskService.tasks(partnerId, search, published, taskCategoryId, feeStart, feeEnd, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());
    }

    @GetMapping(path = "tasks/published")
    @FrontendRest@AdminRest
    @ApiOperation(value = "", response = WSTasks.class)
    public WSResponse published(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                            @RequestParam(required = false) Long partnerId,
                            @RequestParam(defaultValue = "") String keyword,
                            @RequestParam(required = false) Long taskCategoryId,
                            @RequestParam(required = false) BigDecimal feeStart,
                            @RequestParam(required = false) BigDecimal feeEnd,
                            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
                            @ApiParam(allowableValues = TaskDto.MUST_VERIFIED+","+TaskDto.NO_NEED_VERIFIED, required = false) @RequestParam(required = false) String workerVerificationType,
                            @RequestParam(required = false) Boolean suggestionMode,
                            @RequestParam(defaultValue = "0") Integer page,
                            @RequestParam(defaultValue = "true") Boolean ascending,
                            @ApiParam(allowableValues = "jt.updated, jt.publisher, jt.title, jt.fee, p.fullName", required = true) @RequestParam(defaultValue = "jt.updated") String sortir){
        DataPage dataPage = taskService.published(partnerId, keyword, taskCategoryId, feeStart, feeEnd, startDate, endDate, workerVerificationType, suggestionMode, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());
    }


    @Transactional
    @PostMapping(path = "task/saveUpload")
    @AdminRest
    @ApiOperation(value = "", response = WSTask.class)
    public WSResponse postTask(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                @RequestParam @Valid String taskDtoGson,
                               @RequestParam(required = false) MultipartFile multipartFile) throws Exception {
        Admin admin = restUtils.user(authorization);
        TaskDto taskDto = gson.fromJson(taskDtoGson, TaskDto.class);
        Task task = modelMapper.map(taskDto, Task.class);
        if(task.getId()!=null){
            commitAdminActivity(admin, AdminActivity.UPDATE_TASK, task);
        }else{
            commitAdminActivity(admin, AdminActivity.CREATE_TASK, task);
        }
        task = taskService.save(task);

        boolean needUpdate = false;
        if(multipartFile!=null){
            String filename = String.valueOf(new Date().getTime());
            String image = fileStorage.storeFile(taskBannerPhotoDir, task.getId().toString(), filename, multipartFile);
            task.setBannerImage(image);
            needUpdate = true ;
        }
        if(needUpdate){
            task = taskService.save(task);
        }
        if(task.isApplyable()){
            notificationService.sendBroadcast(null, NotificationDto.NEW_TASK, null, message("task.new.title"),message("task.new.description",
                    task.getTitle()), task.getId(), TaskDto.class, null, task.getPartner().getImageUri());
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, task);
    }

    @Transactional
    @PostMapping(path = "task/statusPublication")
    @AdminRest
    @ApiOperation(value = "", response = WSTask.class)
    public WSResponse statusPublication(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                     @RequestParam Long id, @ApiParam(required = true, allowableValues = "true,false") @RequestParam Boolean status){
        Admin admin = restUtils.user(authorization);
        Task task = taskService.findById(id);
        boolean published = task.isPublished();
        if(!status.equals(published)){
            task.setPublished(!status);
            task = taskService.save(task);
            commitAdminActivity(admin, AdminActivity.PUBLISH_TASK, task);
            if(task.isApplyable()){
                notificationService.sendBroadcast(null, NotificationDto.BROADCAST, null, message("task.new.title"),message("task.new.description",
                        task.getTitle()), task.getId(), TaskDto.class, null, task.getPartner().getImageUri());
            }
        } else{
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, "Job Vacancy "+ task.getTitle()+" is already published = "+ task.isPublished());
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, task);
    }

    @Transactional
    @PostMapping(path = "task/up")
    @FrontendRest
    @ApiOperation(value = "", response = WSTask.class)
    public WSResponse up(@RequestHeader(name = Constant.AUTHORIZATION) String authorization,
                                                 @RequestParam Long id){
        Admin admin = jwtTokenProvider.getAdmin(authorization);
        Task task = taskService.findById(id);

        task.setUpdated(new Date());
        task = taskService.save(task);
        commitAdminActivity(admin, AdminActivity.UP_TASK, task);
        if(task.isApplyable()){
            notificationService.sendBroadcast(null, NotificationDto.NEW_TASK, null, message("task.new.title"), message("task.new.description",
                    task.getTitle()), task.getId(), TaskDto.class, null, task.getPartner().getImageUri());
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, task);
    }


}
