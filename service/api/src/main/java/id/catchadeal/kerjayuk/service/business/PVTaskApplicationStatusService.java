package id.catchadeal.kerjayuk.service.business;

import id.catchadeal.kerjayuk.entity.business.PVTaskApplicationStatus;
import id.catchadeal.kerjayuk.repository.business.PVTaskApplicationStatusRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PVTaskApplicationStatusService extends BasicRepoService<PVTaskApplicationStatus> {

    @Autowired private PVTaskApplicationStatusRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }
}
