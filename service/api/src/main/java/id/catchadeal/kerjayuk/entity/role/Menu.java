package id.catchadeal.kerjayuk.entity.role;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name="m_menu")
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class Menu extends NonIdEBase {

    private String name ;
    private String clazz ;
    private String groupMenu ;
    private String link ;
    private int sortir ;

    @Transient
    private boolean checked ;

    @Transient @JsonIgnoreProperties("childMenus")
    List<Menu> childMenus = new ArrayList<>();

    @Transient
    public boolean isParentMenu(){
        return name!=null && groupMenu!=null && name.equalsIgnoreCase(groupMenu);
    }

    public boolean hasChildMenu(Long id){
        boolean exist = false ;
        for (Menu childMenu : childMenus) {
            if(childMenu.getId()==id){
                exist = true ;
            }
        }
        return exist ;
    }



}
