package id.catchadeal.kerjayuk.repository.region;

import id.catchadeal.kerjayuk.entity.region.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

    List<City> findByProvinceId(Long provinceId, Sort sort);
    Page<City> findByProvinceId(Long provinceId, Pageable pageable);

    List<City> findByActiveAndProvinceId(Boolean active, Long provinceId, Sort sort);
    Page<City> findByActiveAndProvinceId(Boolean active, Long provinceId, Pageable pageable);

    List<City> findByActive(Boolean active, Sort sort);
    Page<City> findByActive(Boolean active, Pageable pageable);


}
