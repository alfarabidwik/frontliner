package id.catchadeal.kerjayuk.service.report;

import id.catchadeal.kerjayuk.entity.report.ReportCategory;
import id.catchadeal.kerjayuk.repository.report.ReportCategoryRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ReportCategoryService extends BasicRepoService<ReportCategory> {

    @Autowired private ReportCategoryRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }


}
