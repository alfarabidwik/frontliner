package id.catchadeal.kerjayuk.entity.partner;

import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Data
@Table(name="m_corporate_type", indexes = {
        @Index(columnList = "created", name = "CorporateType_created_idx"),
        @Index(columnList = "updated", name = "CorporateType_updated_idx"),
        @Index(columnList = "active", name = "CorporateType_active_idx"),
})
//@Table(name = "m_corporate_type")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class CorporateType extends NonIdEBase {

    String name ;

    @Override
    public void initTransient() {
        super.initTransient();

    }


}
