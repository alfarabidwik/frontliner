package id.catchadeal.kerjayuk.entity.user;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.util.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true, of = "id")
public class Person extends EBase{
    public static final Logger logger = LoggerFactory.getLogger(Person.class);

    public static final String SINGLE = "SINGLE";
    public static final String MARRIED = "MARRIED";
    public static final String DIVORCED = "DIVORCED";

    public static final String MALE = "MALE";
    public static final String FEMALE = "FEMALE";
    public static final String SHEMALE = "SHEMALE";

    @Column(unique = true)
    String email ;
    @Column(unique = true)
    String homePhoneNumber ;
    @Column(unique = true)
    String mobilePhone ;
    String firstname ;
    String lastname ;
    String idCardCode ;
    String idCard ;
    String photo ;
    String selfieIdCard ;

    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date birthdate ;

    String gender ;
    String maritalStatus ;

    @Column(columnDefinition = "TEXT")
    String address ;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name="village_id")
    Village village ;

    @Column(columnDefinition = "double precision DEFAULT 0")
    Double latitude ;
    @Column(columnDefinition = "double precision DEFAULT 0")
    Double longitude ;

    @Transient
    String fullname ;

    @Transient
    String idCardUrl ;

    @Transient
    String photoUrl ;

    @Transient
    String selfieIdCardUrl ;


    @Override
    public void initTransient() {
        super.initTransient();
        lastname = StringUtils.defaultString(lastname);
        if(StringUtils.isNotEmpty(photo)){
            photoUrl = Constant.prefixApi+ Constant.REST_USER_PHOTO+"/"+getId()+"/"+photo;
        }
        if(StringUtils.isNotEmpty(idCard)){
            idCardUrl = Constant.prefixApi+ Constant.REST_USER_ID_CARD+"/"+getId()+"/"+idCard;
        }
        if(StringUtils.isNotEmpty(selfieIdCard)){
            selfieIdCardUrl = Constant.prefixApi+ Constant.REST_USER_SELFIE_ID_CARD+"/"+getId()+"/"+selfieIdCard;
        }
        if(StringUtils.isNotEmpty(fullname) && (StringUtils.isEmpty(firstname) || StringUtils.isEmpty(lastname))){
            if(fullname.contains(" ")){
                this.firstname = fullname.split(" ")[0];
                this.lastname = fullname.split(" ", 2)[1];
            }else{
                this.firstname = fullname ;
            }
        }else{
            this.fullname = this.firstname +" "+ this.lastname;
        }
   }
}
