package id.catchadeal.kerjayuk.controller.user;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.component.AppCacheManager;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.jurnal.DescriptionVariable;
import id.catchadeal.kerjayuk.entity.jurnal.UserActivity;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.entity.user.Registration;
import id.catchadeal.kerjayuk.entity.user.ResetPassword;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.FrontlinerUserCSV;
import id.catchadeal.kerjayuk.model.FrontlinerUserNoPasswordCSV;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.model.user.UserTaskSummary;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSUser;
import id.catchadeal.kerjayuk.service.misc.TaskCategoryService;
import id.catchadeal.kerjayuk.service.user.ResetPasswordService;
import id.catchadeal.kerjayuk.service.user.UserDeviceService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.EncryptionUtils;
import id.catchadeal.kerjayuk.util.PhoneNumberUtil;
import id.catchadeal.kerjayuk.util.Utils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.MessageFormat;
import java.util.*;

@RestController
@RequestMapping(path = "${api}")
public class UserController extends BasicController {

    Logger logger = LoggerFactory.getLogger(UserController.class);


    @Value("${mobile.apps.domain.reset-password}") String resetPasswordLink ;
    @Value("${mobile.apps.domain.login}") String loginLink ;

    @Autowired ResetPasswordService resetPasswordService ;
    @Autowired UserDeviceService userDeviceService ;
    @Autowired TaskCategoryService taskCategoryService;
    @Autowired AppCacheManager appCacheManager ;

    @Value("${swagger-password}")
    private String swaggerPassword;


    @GetMapping(path = "/user")
    @Override
    @AdminRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse findById(@RequestParam Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, userService.findById(id));
    }

    @GetMapping(path = "/user/info")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse info(@RequestHeader(Constant.AUTHORIZATION) String authorization){
        User user = jwtTokenProvider.getUser(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, user);
    }

    @PostMapping(path = "/user/uploadImage")
    @AdminRest@FrontendRest
    public WSResponse uploadPhoto(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(required = true) Long userId,
            @ApiParam(allowableValues = Constant.IMAGE_UPLOAD_TYPE, required = true) @RequestParam String type,
            @RequestParam MultipartFile multipartFile) throws Exception{

        User user = userService.findById(userId);
        if(multipartFile!=null){
            String image = null ;
            String filename = String.valueOf(System.nanoTime());
            if(type.equalsIgnoreCase(Constant.PHOTO)){
                image = fileStorage.storeFile(userPhotoDir, String.valueOf(user.getId()), filename, multipartFile);
                user.setPhoto(image);
                user = userService.save(user);
                userActivityService.save(UserActivity.build(user, UserActivity.UPLOAD_PHOTO_PROFILE, Arrays.asList(DescriptionVariable.build(user.getPhoto(), User.TABLE, "photo", 0)), request.getParameterMap()));
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
            }

            if(type.equalsIgnoreCase(Constant.ID_CARD)){
                image = fileStorage.storeFile(userIdCardDir, String.valueOf(user.getId()), filename, multipartFile);
                user.setIdCard(image);
                user = userService.save(user);
                userActivityService.save(UserActivity.build(user, UserActivity.UPLOAD_ID_CARD, Arrays.asList(DescriptionVariable.build(user.getSelfieIdCard(), User.TABLE, "id_card", 0)), request.getParameterMap()));
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
            }

            if(type.equalsIgnoreCase(Constant.SELFIE_ID_CARD)){
                image = fileStorage.storeFile(userSelfieIdCardDir, String.valueOf(user.getId()), filename, multipartFile);
                user.setSelfieIdCard(image);
                user = userService.save(user);
                userActivityService.save(UserActivity.build(user, UserActivity.UPLOAD_SELFIE_ID_CARD, Arrays.asList(DescriptionVariable.build(user.getSelfieIdCard(), User.TABLE, "selfie_id_card", 0)), request.getParameterMap()));
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
            }

        }
        return WSResponse.instance(Constant.FAILED_CODE, Constant.FAILED);
    }

    @PostMapping(path = "/user/delete")
    @AdminRest
    @ApiOperation(value = "<b>Please read note below</b>", notes = "" +
            "<b>Delete from database is not possible to implement</b>, because its correlated with much other tables\n" +
            "The possiblest thing can be done is only change active status of user but this status is not correlated with employee status and partner status either,\n" +
            "This is special status which only have two param active or not and its not same with partner status or employee status,\n" +
            "<b>Once this user has been deleted (which mean active became false), then it can't be restored, user must take re-register to create new entity of his user account</b>")
    public WSResponse delete(@RequestHeader(Constant.AUTHORIZATION) String authorization,
                             @RequestParam Long userId){
        restUtils.validateRoleException(authorization, Constant.ADMIN);
        User user = userService.findById(userId);
        user.setActive(false);
        user = userService.save(user);

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }

    @GetMapping(path = "/users")
    public WSResponse findAll(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "u.created") String sortir,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(required = false) Boolean deleted,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date userCreatedStart,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date userCreatedEnd,
            @RequestParam(required = false) String userStatus,
            @RequestParam(required = false) String verificationStatus,
            @RequestParam(required = false) String gender,
            @RequestParam(required = false) Long provinceId,
            @RequestParam(required = false) Long cityId,
            @RequestParam(required = false) Long districtId,
            @RequestParam(required = false) Long villageId
    ) {
        Utils.validateParam(sortir);

        DataPage<User> dataPage = userService.userInCustomFilteringNative(page,
                sortir, ascending, search, deleted,
                userCreatedStart, userCreatedEnd, userStatus, verificationStatus, gender,
                provinceId, cityId, districtId, villageId);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage()).setPageElement(dataPage.getPageElement());
    }

    @GetMapping(path = "/user/authorization")
    public WSResponse findById(@RequestHeader String authorization) {
        User user = jwtTokenProvider.getUser(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @PostMapping(path = "/user/save")
    public WSResponse save(@RequestBody UserDto userDto) throws Exception{

        User user = modelMapper.map(userDto, User.class);
        boolean newUser = userDto.getId()==null;
        if(newUser){
            if(userService.existsByEmail(userDto.getEmail())){
                throw new RuntimeException(message("email.already.registered.by.another.account", userDto.getEmail()));
            }
            if(userService.existsByMobilePhone(userDto.getMobilePhone())){
                throw new RuntimeException(message("mobile.number.already.registered.by.another.account", userDto.getMobilePhone()));
            }
        }
        user = userService.save(user);
        if(newUser){
            userActivityService.save(UserActivity.build(user, UserActivity.REGISTER, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), userDto));
        }else{
            userActivityService.save(UserActivity.build(user, UserActivity.UPDATE_PROFILE, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), userDto));
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @GetMapping(path = "/user/generateAgentCode")
    public WSResponse generateAgentCodeController(){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, super.generateAgentCode());
    }

    @PostMapping(path = "/user/saveUpload", consumes = { "multipart/form-data" })
    @AdminRest@FrontendRest
    public WSResponse saveUpload(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam String userDtoGson
            , @RequestParam(required = false) MultipartFile photoMultipartFile
            , @RequestParam(required = false) MultipartFile idCardMultipartFile
            , @RequestParam(required = false) MultipartFile selfieIdCardMultipartFile) throws Exception{
        logger.debug("BODY #### {} ", userDtoGson);
        UserDto userDto = gson.fromJson(userDtoGson, UserDto.class);
        User user = modelMapper.map(userDto, User.class);
        user.setMobilePhone(PhoneNumberUtil.cleanupIndMobilePhone(user.getMobilePhone(), validPhoneNumber -> {}));
        boolean newUser = userDto.getId()==null;
        if(newUser){
            if(userService.existsByEmail(userDto.getEmail())){
                throw new RuntimeException(message("email.already.registered.by.another.account", userDto.getEmail()));
            }
            if(userService.existsByMobilePhone(userDto.getMobilePhone())){
                throw new RuntimeException(message("mobile.number.already.registered.by.another.account", userDto.getMobilePhone()));
            }
            user.initAgentCode(miscellaneousService);
            Admin admin = jwtTokenProvider.getAdmin(authorization);
            Registration registration = registrationService.register(user.getFirstname(), user.getLastname(), user.getMobilePhone(), user.getEmail(), user.getReferralCode(), user.getAgentCode(), admin.getEmail());
            user.setRegistration(registration);
        }

        user = userService.save(user);

        String filename = String.valueOf(System.currentTimeMillis());
        if(photoMultipartFile!=null){
            String image = fileStorage.storeFile(userPhotoDir, String.valueOf(user.getId()), filename, photoMultipartFile);
            user.setPhoto(image);
            user.checkFlagNeedVerified();
        }
        if(idCardMultipartFile!=null){
            String image = fileStorage.storeFile(userIdCardDir, String.valueOf(user.getId()), filename, idCardMultipartFile);
            user.setIdCard(image);
            user.checkFlagNeedVerified();
        }
        if(selfieIdCardMultipartFile!=null){
            String image = fileStorage.storeFile(userSelfieIdCardDir, String.valueOf(user.getId()), filename, selfieIdCardMultipartFile);
            user.setSelfieIdCard(image);
            user.checkFlagNeedVerified();
        }
        user = userService.save(user);

        if(newUser){
            userActivityService.save(UserActivity.build(user, UserActivity.UPDATE_PROFILE, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), userDto));
            notificationService.sendSpesific(user, message("registration.success.title"), message("registration.success.description", user.getFirstname()), null, null, null);
        }else{
            userActivityService.save(UserActivity.build(user, UserActivity.REGISTER, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), userDto));
        }

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @GetMapping(path = "/user/validateEmail")
    public WSResponse validateEmail(@RequestParam(required = true) String email){
        if(!userService.existsByEmail(email)){
            return WSResponse.instance(Constant.SUCCESS_CODE, "Email can be used");
        }else{
            return WSResponse.instance(Constant.FAILED_CODE, message("email.already.registered.by.another.account", email));
        }
    }
    @GetMapping(path = "/user/validateMobilePhone")
    public WSResponse validateMobilePhone(@RequestParam(required = true) String mobilePhone){
        if(!userService.existsByMobilePhone(mobilePhone)){
            return WSResponse.instance(Constant.SUCCESS_CODE, "Phone number can be used");
        }else{
            return WSResponse.instance(Constant.FAILED_CODE, message("mobile.number.already.registered.by.another.account", mobilePhone));
        }
    }

    @GetMapping(path = "/user/validateMobilePhoneWithEmailMatcher")
    public WSResponse validateMobilePhoneWithEmailMatcher(@RequestParam String mobilePhone, @RequestParam String email){
        if(!userService.existsByMobilePhone(mobilePhone)){
            return WSResponse.instance(Constant.SUCCESS_CODE, "Phone number can be used");
        }else{
            User user = userService.findByMobilePhone(mobilePhone);
            if(!user.getEmail().equalsIgnoreCase(email)){
                return WSResponse.instance(Constant.FAILED_CODE, message("mobile.number.already.registered.by.another.account", mobilePhone));
            }else{
                return WSResponse.instance(Constant.SUCCESS_CODE, "Phone number can be used");
            }
        }
    }


//    @PostMapping(path = "/user/verify")
//    public WSResponse verify(@RequestPart String verificationCode) throws Exception{
//        User user = userService.findByVerificationCode(verificationCode);
//        if(user==null){
//            return WSResponse.instance(Constant.VERIFICATION_FAILED_CODE, Constant.FAILED, "User not found");
//        }
//        if(user.getUserStatus().getId().equals(UserStatus._ACTIVE)){
//            return WSResponse.instance(Constant.ALREADY_VERIFIED_CODE, "Already Verified");
//        }
//        UserStatus userStatus = userStatusService.findById(UserStatus._ACTIVE);
//        user.setUserStatus(userStatus);
//        user = userService.save(user);
//        userActivityService.save(UserActivity.build(user, UserActivity.VERIFICATION, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname,lastname", 0)), request.getParameterMap()));
//        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
//    }

    @PostMapping(path = "/user/update")
    public WSResponse update(@RequestBody @Valid UserDto userDto){
        User user = modelMapper.map(userDto, User.class);
        user = userService.save(user);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }

    @PostMapping(path = "/user/updateVerification")
    public WSResponse updateVerification(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam Long userId, @RequestParam String verificationStatus, @RequestParam(required = false) String note){
        User user = userService.findById(userId);
        user.setVerificationStatus(verificationStatus);
        user = userService.save(user);
        if(verificationStatus.equalsIgnoreCase(UserDto.VERIFIED)){
            notificationService.sendSpesific(user, message("verification.success.title"), message("verification.success.description", user.getFirstname()), null, null, null);
        }
        if(verificationStatus.equalsIgnoreCase(UserDto.VERIFICATION_REJECTED)){
            notificationService.sendSpesific(user, message("verification.rejected.title"), StringUtils.isNotEmpty(note)?note:message("verification.rejected.description", user.getFirstname()), null, null, null);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, user);
    }


    @GetMapping(path = "/user/firstnames")
    public WSResponse firstnames(){
        List<String> firstnames = userService.firstnames("firstname", true);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, firstnames);
    }

    @GetMapping(path = "/user/emails")
    public WSResponse emails(){
        List<String> firstnames = userService.emails(Utils.sort(false, "created"));
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, firstnames);
    }

    @PostMapping("/user/forgetPassword")
    public WSResponse forgetPassword(@RequestParam String email) {
        User user = userService.findByEmail(email);
        if(user==null){
            throw new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "User.Email"));
        }
        HashMap hashMap = new HashMap();

        String authorization = jwtTokenProvider.createToken(user.getMobilePhone(), user.getId(), Arrays.asList(Constant.USER));
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setToken(authorization);
        resetPassword.setUser(user);
        resetPasswordService.save(resetPassword);

        hashMap.put("link", MessageFormat.format(resetPasswordLink, authorization));
        emailService.sendEmail(message("reset.password"), email, Constant.RESET_PASSWORD_TML, hashMap);
        userActivityService.save(UserActivity.build(user, UserActivity.FORGET_PASSWORD,
                Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname, lastname", 0)), request.getParameterMap()));

        return WSResponse.instance(Constant.SUCCESS_CODE, message("forget.password.instruction.has.been.sent.to.your.email"));
    }

    @PostMapping("/user/resetPassword")
    public WSResponse resetPassword(String token, String newPassword) {
        ResetPassword resetPassword = resetPasswordService.findByToken(token);
        User user = resetPassword.getUser();
        if(user==null){
            throw new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "User.Email"));
        }
        resetPassword.setUsed(true);
        resetPasswordService.save(resetPassword);
//        user.setPassword(EncryptionUtils.encrypt(Constant.AVENGER_KEY, newPassword));
        user.setPassword(passwordEncoder.encode(newPassword));
        user = userService.save(user);

        HashMap hashMap = new HashMap();

        hashMap.put("link", loginLink);

        emailService.sendEmail(message("reset.password"), newPassword, Constant.RESET_PASSWORD_SUCCESS_TML, hashMap);
        userActivityService.save(UserActivity.build(user, UserActivity.RESET_PASSWORD, Arrays.asList(DescriptionVariable.build(user.getFullname(), User.TABLE, "firstname, lastname", 0)), new HashMap<>()));

        return WSResponse.instance(Constant.SUCCESS_CODE, message("password.has.been.changed"));
    }


    @GetMapping(path = "/user/checkReferralCode")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse checkReferralCode(@RequestParam String referralCode){
        Boolean exists = userService.existsByAgentCodeAndActiveIsTrue(referralCode);
        if(exists){
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
        }else{
            return WSResponse.instance(Constant.FAILED_CODE, "invalid.referral.code");
        }
    }

    @GetMapping(path = "/user/taskSummary")
    @FrontendRest
    @ApiOperation(value = "", response = WSUser.class)
    public WSResponse taskSummary(@RequestHeader(Constant.AUTHORIZATION) String authorization){
        User user =jwtTokenProvider.getUser(authorization);
        UserTaskSummary userTaskSummary = userService.userTaskSummary(user.getId());
        return WSResponse.instance(Constant.SUCCESS_CODE, userTaskSummary) ;
    }

    @PostMapping(path = "/user/migrateUser", consumes = { "multipart/form-data" })
    @AdminRest
    public WSResponse migrateUser(
            @RequestHeader String swaggerPassword,
            @RequestParam MultipartFile csvFile) throws Exception{

        if(!swaggerPassword.equalsIgnoreCase(this.swaggerPassword)){
            return WSResponse.instance(Constant.FAILED_CODE,  "Password doesn't match");
        }

        try (Reader reader = new BufferedReader(new InputStreamReader(csvFile.getInputStream()))) {

            // create csv bean reader
            CsvToBean<FrontlinerUserCSV> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(FrontlinerUserCSV.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            // convert `CsvToBean` object to list of users
            int i = 1 ;
            List<User> users = new ArrayList<>();
            List<FrontlinerUserCSV> frontlinerUserCSVS = csvToBean.parse();
            for (FrontlinerUserCSV frontlinerUserCSV : frontlinerUserCSVS) {
                User user = new User();
                user.setFullname(frontlinerUserCSV.getFullname());
                user.setPassword(frontlinerUserCSV.getPassword());
                user.setEmail(frontlinerUserCSV.getEmail());
                user.setOrganization(frontlinerUserCSV.getOrganization());
                String mobilePhone = StringUtils.defaultString(frontlinerUserCSV.getMobilePhone(), "");
                mobilePhone = mobilePhone.replaceAll("[^\\d]", "");
                mobilePhone = PhoneNumberUtil.cleanupIndMobilePhone(mobilePhone, validPhoneNumber -> {});
                user.setMobilePhone(mobilePhone);
                user.setFirstLogin(true);
                user.setAgentCode(generateAgentCode());
                if(!Utils.isValidEmail(user.getEmail())){
                    throw new Exception("Invalid email = "+user.getEmail()+" at row "+i);
                }

                boolean exists = userService.existsByEmailOrMobilePhone(user.getEmail(), user.getMobilePhone());
                if(!exists){
                    user = userService.save(user);
                    users.add(user);
                }
                i++;
            }

            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, users);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AppException(Constant.FAILED_CODE, ex.getMessage());
        }
    }

    @PostMapping(path = "/user/generateUser", consumes = { "multipart/form-data" })
    @AdminRest
    public WSResponse generateUser(
            @RequestHeader String swaggerPassword,
            @RequestParam MultipartFile csvFile) throws Exception{

        if(!swaggerPassword.equalsIgnoreCase(this.swaggerPassword)){
            return WSResponse.instance(Constant.FAILED_CODE,  "Password doesn't match");
        }

        try (Reader reader = new BufferedReader(new InputStreamReader(csvFile.getInputStream()))) {

            // create csv bean reader
            CsvToBean<FrontlinerUserNoPasswordCSV> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(FrontlinerUserNoPasswordCSV.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            // convert `CsvToBean` object to list of users
            int i = 1 ;
            List<User> users = new ArrayList<>();
            List<FrontlinerUserNoPasswordCSV> frontlinerUserCSVS = csvToBean.parse();
            for (FrontlinerUserNoPasswordCSV frontlinerUserCSV : frontlinerUserCSVS) {
                User user = new User();
                user.setFullname(frontlinerUserCSV.getFullname());
                user.setEmail(frontlinerUserCSV.getEmail());
                user.setOrganization(frontlinerUserCSV.getOrganization());
                String mobilePhone = StringUtils.defaultString(frontlinerUserCSV.getMobilePhone(), "");
                mobilePhone = mobilePhone.replaceAll("[^\\d]", "");
                mobilePhone = PhoneNumberUtil.cleanupIndMobilePhone(mobilePhone, validPhoneNumber -> {});
                user.setMobilePhone(mobilePhone);
                user.setFirstLogin(true);
                user.setAgentCode(generateAgentCode());
                if(!Utils.isValidEmail(user.getEmail())){
                    throw new Exception("Invalid email = "+user.getEmail()+" at row "+i);
                }

                boolean exists = userService.existsByEmailOrMobilePhone(user.getEmail(), user.getMobilePhone());
                if(!exists){
                    user = userService.save(user);
                    users.add(user);
                }
                i++;
            }

            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, users);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AppException(Constant.FAILED_CODE, ex.getMessage());
        }
    }









}
