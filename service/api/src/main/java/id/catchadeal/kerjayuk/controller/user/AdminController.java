package id.catchadeal.kerjayuk.controller.user;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.entity.user.Admin;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.admindashboard.AdminDashboard;
import id.catchadeal.kerjayuk.model.admindashboard.DashboardCategoryAnalytic;
import id.catchadeal.kerjayuk.model.user.AdminDto;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSAdmin;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSAdmins;
import id.catchadeal.kerjayuk.service.mail.EmailService;
import id.catchadeal.kerjayuk.service.misc.MiscellaneousService;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.EncryptionUtils;
import id.catchadeal.kerjayuk.util.Mapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class AdminController extends BasicController {

    @Autowired AdminService adminService ;
    @Autowired MiscellaneousService miscellaneousService ;
    @Autowired FileStorage fileStorage;

    @Autowired EmailService emailService ;

    @GetMapping(path = "/admins")
    @AdminRest
    @ApiOperation(value = "", response = WSAdmins.class)
    public WSResponse findAll(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(defaultValue = "true") Boolean ascending,
            @RequestParam(defaultValue = "id") String sortir) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, adminService.findAll(Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir)));
    }


    @GetMapping(path = "/admin")
    @AdminRest
    @Override
    public WSResponse findById(@RequestParam Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, adminService.findById(id));
    }

    @GetMapping(path = "/admin/info")
    @AdminRest
    @ApiOperation(value = "", response = WSAdmin.class)
    public WSResponse info(@RequestHeader(Constant.AUTHORIZATION) String authorization){
        Admin admin = jwtTokenProvider.getAdmin(authorization);
        return WSResponse.instance(Constant.SUCCESS_CODE, admin);
    }
    @PostMapping(path = "/admin/resendPassword")
    @AdminRest
    public WSResponse requestForgetPassword(
            @RequestParam(Constant.AUTHORIZATION) String authorization,
            @RequestParam String email) throws Exception{
        restUtils.validateRoleException(authorization, Constant.ADMIN);
        Admin admin = restUtils.user(authorization);// adminService.findByEmail(email);
        Admin adminForPassword = adminService.findByEmail(email);
        if(!admin.getRole().getName().equalsIgnoreCase(Admin.SUPER_ADMIN)){
            throw new AppException(Constant.FAILED_CODE, "This api is not allow for a regular admin, please ask to your supervisor to get your password");
        }
        emailService.sendPassword(adminForPassword.getEmail(), EncryptionUtils.decrypt(Constant.AVENGER_KEY, adminForPassword.getPassword()));
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    @PostMapping(path = "/admin/login")
    @AdminRest
    @ApiOperation(value = "", response = WSAdmin.class)
    public WSResponse signin(@RequestParam String email, @RequestParam String password){
        Admin admin = adminService.signin(email);
        String adminPassword = admin.getPassword();
        String encryptedPassword = EncryptionUtils.encrypt(Constant.AVENGER_KEY, password);
        if(!adminPassword.equalsIgnoreCase(encryptedPassword)){
            throw new AppException(Constant.FAILED_CODE, "Invalid email or password");
        }

        if(StringUtils.isNotEmpty(admin.getAdminStatus()) && StringUtils.equalsIgnoreCase(admin.getAdminStatus(), Admin.ACTIVE)){
            String token = jwtTokenProvider.createToken(email, admin.getId(),Arrays.asList(Constant.ADMIN));
            admin.setAuthorization(token);
            commitAdminActivity(admin, AdminActivity.LOGIN, request.getParameterMap());

            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, admin);
        }else{
            if(admin.getAdminStatus()==null){
                throw new AppException(Constant.FAILED_CODE, "Status Of Admin = NULL, please contact your admin support");
            }else{
                throw new AppException(Constant.FAILED_CODE, "Status Of Admin = "+admin.getAdminStatus()+", please contact your admin support");
            }
        }
    }

    @GetMapping(path = "/admin/logout")
    public WSResponse logout(@RequestHeader(required = false, name =  HttpHeaders.AUTHORIZATION) String authorization){
        if(authorization==null){
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
        }
        commitAdminActivity(authorization, AdminActivity.LOGOUT, request.getParameterMap());
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }



    @PostMapping(path = "/admin/saveUpload")
    public WSResponse saveUpload(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
            @RequestPart String adminDtoGson, @RequestParam(required = false) MultipartFile multipartFile) throws Exception{

        Admin executorAdmin = jwtTokenProvider.getAdmin(authorization);

        AdminDto adminDto = gson.fromJson(adminDtoGson, AdminDto.class);
        Admin admin = modelMapper.map(adminDto, Admin.class);

        boolean newUser = admin.getId()==null || admin.getId()==0 ;
        if(newUser){
            commitAdminActivity(authorization, AdminActivity.CREATE_NEW_ADMIN, adminDto);
        }else{
            if(admin.getId()==executorAdmin.getId()){
                commitAdminActivity(authorization, AdminActivity.UPDATE_PROFILE, adminDto, adminService.findById(admin.getId()));
            }else{
                commitAdminActivity(authorization, AdminActivity.UPDATE_ADMIN, adminDto, adminService.findById(admin.getId()));
            }
        }
        if(newUser){
            if(adminService.existsByEmail(admin.getEmail())){
                return WSResponse.instance(Constant.FAILED_CODE, message("email.already.registered.by.another.account", admin.getEmail()));
            }
        }

        admin = adminService.save(admin);
        if(multipartFile!=null){
            String filename = String.valueOf(System.currentTimeMillis());
            String image = fileStorage.storeFile(adminPhotoDir, String.valueOf(admin.getId()), filename, multipartFile);
            admin.setImage(image);
            admin = adminService.save(admin);
        }
        if(newUser){
            String encryptedPassword = EncryptionUtils.encrypt(drStrangeAvengerKey, miscellaneousService.random8Code(false));
            admin.setPassword(encryptedPassword);
            admin = adminService.save(admin);
            HashMap<String, Object> map = new HashMap<>();
            map.put("adminWeb", adminDomain);
            map.put("email", admin.getEmail());
            map.put("password", EncryptionUtils.decrypt(drStrangeAvengerKey, admin.getPassword()));
            try{
                emailService.sendDefaultTemplate("Account Information", admin.getEmail(), "account_information", map);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, admin);
    }


    @PostMapping(path = "/admin/save")
    @AdminRest
    @ApiOperation(value = "", response = WSAdmin.class)
    public WSResponse save(
            @RequestParam(Constant.AUTHORIZATION) String authorization,
            @RequestBody@Valid AdminDto adminData,
            @ApiParam(allowableValues = Admin.SUPER_ADMIN+","+Admin.ADMIN, required = true) @RequestParam String role) throws Exception {
        restUtils.validateRoleException(authorization, Constant.ADMIN);
        Admin executorAdmin = restUtils.user(authorization);
        Admin admin = modelMapper.map(adminData, Admin.class);
        if(admin.getId()==null){
            if(!executorAdmin.getRole().getName().equalsIgnoreCase(Admin.SUPER_ADMIN)){
                throw new AppException(Constant.FAILED_CODE, "This api is not allow for a regular admin, please switch to a super admin or contact your system administrator");
            }
            String password = admin.getPassword();
            String encryptedPassword = EncryptionUtils.encrypt(Constant.AVENGER_KEY, password);
            admin.setPassword(encryptedPassword);
            emailService.sendPassword(admin.getEmail(), password);
        }
        admin = adminService.save(admin);
        return WSResponse.instance(Constant.SUCCESS_CODE, admin);
    }

    @GetMapping(path = "/admin/delete")
    @Override
    @AdminRest
    public WSResponse delete(@RequestParam Long id) {
        adminService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.DELETED_SUCCESSFULLY);
    }
    @GetMapping(path = "/admin/firstnames")
    public WSResponse firstnames(){
        List<String> firstnames = adminService.firstnames("firstname", true);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, firstnames);
    }

    @GetMapping(path = "/admin/dashboard")
    @AdminRest
    public WSResponse dashboard(){
        AdminDashboard adminDashboard = adminService.adminDashboard();

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, adminDashboard);
    }


    @GetMapping("/admin/dashboard/categoryAnalytic")
    @AdminRest
    public WSResponse categoryAnalytic(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization,
                                       @ApiParam(example = DateAppConfig.API_DATE_FORMAT, required = true) @RequestParam @DateTimeFormat(pattern = DateAppConfig.API_DATE_FORMAT) Date startDate,
                                       @ApiParam(example = DateAppConfig.API_DATE_FORMAT, required = true) @RequestParam @DateTimeFormat(pattern = DateAppConfig.API_DATE_FORMAT) Date endDate){
        DashboardCategoryAnalytic dashboardCategoryAnalytic = adminService.categoryAnalytic(startDate, endDate);

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dashboardCategoryAnalytic);
    }


    @GetMapping(path = "/admin/validateEmail")
    public WSResponse validateEmail(@RequestParam(required = true) String email){
        if(!adminService.existsByEmail(email)){
            return WSResponse.instance(Constant.SUCCESS_CODE, "Email can be used");
        }else{
            return WSResponse.instance(Constant.FAILED_CODE, message("email.already.registered.by.another.account", email));
        }
    }

    @GetMapping(path = "/admin/resendPassword")
    public WSResponse resendPassword(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestParam Long adminId){

        Admin admin = adminService.findById(adminId);

        commitAdminActivity(authorization, AdminActivity.RESEND_PASSWORD, Mapper.init().put("email", admin.getEmail()).put("fullname", admin.getFullname()).get());
        String decryptedPassword = EncryptionUtils.decrypt(drStrangeAvengerKey, admin.getPassword());
        HashMap<String, Object> map = new HashMap<>();
        map.put("adminWeb", adminDomain);
        map.put("email", admin.getEmail());
        map.put("password", decryptedPassword);
        try{
            emailService.sendDefaultTemplate("Account Information", admin.getEmail(), "account_information", map);
        }catch (Exception e){
            e.printStackTrace();
        }

        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }





}
