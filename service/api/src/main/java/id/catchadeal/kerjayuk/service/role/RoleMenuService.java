package id.catchadeal.kerjayuk.service.role;

import id.catchadeal.kerjayuk.entity.role.RoleMenu;
import id.catchadeal.kerjayuk.repository.role.RoleMenuRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoleMenuService extends BasicRepoService<RoleMenu> {

    @Autowired
    RoleMenuRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    @Override
    public List<RoleMenu> findAll() {
        return repository.findAll();
    }

    @Override
    public RoleMenu save(RoleMenu roleRoleMenu) {
        return super.save(roleRoleMenu);
    }

    public Boolean existsById(Long id){
        return repository.existsById(id);
    }

    public List<RoleMenu> findByRoleId(Long roleId){
        return repository.findByRoleId(roleId);
    }


}
