package id.catchadeal.kerjayuk.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.util.JsonDateTimeSerializer;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@DynamicUpdate
@DynamicInsert
@TypeDefs({
        @TypeDef(
                typeClass = StringArrayType.class,
                defaultForType = String[].class
        ),
//        @TypeDef(
//                typeClass = IntArrayType.class,
//                defaultForType = int[].class
//        ),
//        @TypeDef(
//                typeClass = EnumArrayType.class,
//                defaultForType = SensorState[].class,
//                parameters = {
//                        @Parameter(
//                                name = EnumArrayType.SQL_ARRAY_TYPE,
//                                value = "sensor_state"
//                        )
//                }
//        )
})
@SelectBeforeUpdate(false)
public abstract class BasicField {

    public abstract Long getId();
    public abstract void setId(Long id);


    //, nullable = false, columnDefinition = "DEFAULT CURRENT_TIMESTAMP"
    @Column(name="created")
    @Getter@Setter
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date created ;

    @Column(name="created_by")
    @Getter@Setter
    protected Long createdBy ;

    //, nullable = false, columnDefinition = "DEFAULT CURRENT_TIMESTAMP"
    @Column(name="updated")
    @Getter@Setter
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    protected Date updated ;

    @Column(name="updated_by")
    @Getter@Setter
    protected Long updatedBy ;

    @Column(name="active", columnDefinition = "boolean default true")
    @Getter@Setter
    protected Boolean active ;

    @PreUpdate
    @PrePersist
    public void prePersist(){
        initTransient();
    }

    @PostPersist
    @PostUpdate
    public void postPersist() {
        initTransient();
    }

    @PostLoad
    public void postLoad(){
        initTransient();
    }

    public void initTransient(){
        if(createdBy==null || createdBy.equals(0l)){
            createdBy = Constant.SYSTEM;
        }
        if(updatedBy==null || updatedBy.equals(0l)){
            updatedBy = Constant.SYSTEM;
        }
        if(created==null){
            created = new Date();
        }
        if(updated==null){
            updated = new Date();
        }
        if(active==null){
            active = Boolean.TRUE;
        }
    }

    public void mergeActiveStatus(BasicField... basicFields){
        if(basicFields==null){
            return;
        }
        if(basicFields.length==1){
            if(basicFields[0] !=null && basicFields[0].active!=null){
                this.active = basicFields[0].active;
            }
            return;
        }
        if(basicFields.length>1){
            Boolean activeCheck = null ;
            for (BasicField basicField : basicFields) {
                if(basicField!=null && basicField.active!=null){
                    if(!basicField.active){
                        activeCheck = false ;
                    }
                    break;
                }
            }
            if(activeCheck!=null){
                this.active = activeCheck;
            }
            return;
        }
    }



}
