package id.catchadeal.kerjayuk.service.transaction;

import id.catchadeal.kerjayuk.entity.transaction.PurchaseStatus;
import id.catchadeal.kerjayuk.repository.transaction.PurchaseStatusRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class PurchaseStatusService extends BasicRepoService<PurchaseStatus> {

    @Autowired private PurchaseStatusRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }

}
