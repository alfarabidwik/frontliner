package id.catchadeal.kerjayuk.entity.report;

import id.catchadeal.kerjayuk.entity.NonIdEBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="m_report_status")
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class ReportStatus extends NonIdEBase {

    public static final String INITIATED_ID = "1";
    public static final String ACCEPTED_ID = "2";
    public static final String REJECTED_ID = "3";
    public static final String CANCELED_ID = "3";

    public static final ReportStatus INITIATED = new ReportStatus(Long.valueOf(INITIATED_ID));
    public static final ReportStatus ACCEPTED = new ReportStatus(Long.valueOf(ACCEPTED_ID));
    public static final ReportStatus REJECTED = new ReportStatus(Long.valueOf(REJECTED_ID));
    public static final ReportStatus CANCELED = new ReportStatus(Long.valueOf(CANCELED_ID));

    public static final String SWAGGER_STATUS_DESCRIPTION =
            "1 = Initiated" +
            "\n2 = Accepted" +
            "\n3 = Rejected"+
            "\n4 = Canceled";

    String name ;
    @Column(columnDefinition = "TEXT")
    String description ;

    public ReportStatus(){

    }
    public ReportStatus(Long id){
        this.id = id ;
    }

    @Override
    public void initTransient() {
        super.initTransient();
    }

}
