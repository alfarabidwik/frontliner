package id.catchadeal.kerjayuk.repository.partner;

import id.catchadeal.kerjayuk.entity.partner.PVPartnerStatus;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PVPartnerStatusRepository extends JpaRepository<PVPartnerStatus, Long> {

    PVPartnerStatus findTopByPartnerIdAndPartnerStatusId(Long partnerId, Long partnerStatusId, Sort sort);

}
