package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.WorkingDay;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.WorkingDayDto;
import id.catchadeal.kerjayuk.model.wsresponse.misc.WSWorkingDay;
import id.catchadeal.kerjayuk.model.wsresponse.misc.WSWorkingDays;
import id.catchadeal.kerjayuk.service.misc.WorkingDayService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "${api}")
public class WorkingDayController extends BasicController {


    @Autowired WorkingDayService workingDayService ;



    @GetMapping(path = "/workingDays")
    @Override
    @UniversalRest
    @ApiOperation(value = "", response = WSWorkingDays.class)
    public WSResponse findAll(@RequestParam(defaultValue = "true") Boolean ascending, @RequestParam(defaultValue = "id") String sortir) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, workingDayService.findAll(Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir)));
    }


    @PostMapping(path = "/workingDay/register")
    @AdminRest
    @ApiOperation(value = "", response = WSWorkingDay.class)
    public WSResponse registerCategory(@RequestParam String workingDayJson,
                                       @RequestParam(required = false) MultipartFile defaultIconFile,
                                       @RequestParam(required = false) MultipartFile coloredIconFile) throws Exception {

        WorkingDayDto workingDayDto = gson.fromJson(workingDayJson, WorkingDayDto.class);
        WorkingDay workingDay = modelMapper.map(workingDayDto, WorkingDay.class);
        workingDay = workingDayService.save(workingDay);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, workingDay);
    }

    @GetMapping(path = "/workingDay")
    @Override
    @UniversalRest
    @ApiOperation(value = "", response = WSWorkingDay.class)
    public WSResponse findById(@RequestParam Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, workingDayService.findById(id));
    }
}
