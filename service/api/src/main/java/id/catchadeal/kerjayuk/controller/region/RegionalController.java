package id.catchadeal.kerjayuk.controller.region;

import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.region.City;
import id.catchadeal.kerjayuk.entity.region.District;
import id.catchadeal.kerjayuk.entity.region.Province;
import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.service.region.CityService;
import id.catchadeal.kerjayuk.service.region.DistrictService;
import id.catchadeal.kerjayuk.service.region.ProvinceService;
import id.catchadeal.kerjayuk.service.region.VillageService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class RegionalController extends BasicController {

    /**
     * VILLAGE
     * */

    @Autowired
    VillageService villageService;


    @GetMapping(path = "/villages")
    @UniversalRest
    public WSResponse findAll(
            @RequestParam(required = false) Boolean active,
            @RequestParam(required = false) Long districtId,
            @RequestParam(required = false) Integer page,
            @RequestParam(defaultValue = "name") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){

        Sort sort = new Sort(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir);

        List<Village> villages = new ArrayList<>();
        if(districtId==null){
            if(page==null){
                villages = villageService.findAll(active, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, villages);
            }else{
                Page<Village> villagePage = villageService.findAll3(active, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, villagePage.getContent()).setPageElement(pageRow).setTotalPage(villagePage.getTotalPages()).setTotalElement(villagePage.getTotalElements());
            }
        }else{
            if(page==null){
                villages = villageService.findByDistrictId(active, districtId, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, villages);
            }else{
                Page<Village> villagePage = villageService.findAll4(active, districtId, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, villagePage.getContent()).setPageElement(pageRow).setTotalPage(villagePage.getTotalPages()).setTotalElement(villagePage.getTotalElements());
            }
        }
    }

    @GetMapping(path = "/village/{id}")
    @Override
    @UniversalRest
    public WSResponse findById(@PathVariable Long id) {
        Village village = villageService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, village);
    }

    @GetMapping(path = "/village/delete/{id}")
    @UniversalRest
    public WSResponse delete(@PathVariable Long id){
        villageService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

    /**
    * END VILLAGE
    * */


    /**
    * DISTRICT
    * */

    @Autowired
    DistrictService districtService;


    @GetMapping(path = "/districts")
    @UniversalRest
    public WSResponse findDistricts(
            @RequestParam(required = false) Long cityId,
            @RequestParam(required = false) Boolean active,
            @RequestParam(required = false) Integer page,
            @RequestParam(defaultValue = "name") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){


        Sort sort = new Sort(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir);
        List<District> districts = new ArrayList<>();
        if(cityId==null){
            if(page==null){
                districts = districtService.findAll(active, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, districts);
            }else{
                Page<District> districtPage = districtService.findAll3(active, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, districtPage.getContent()).setPageElement(pageRow).setTotalPage(districtPage.getTotalPages()).setTotalElement(districtPage.getTotalElements());
            }
        }else{
            if(page==null){
                districts = districtService.findByCityId(active, cityId, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, districts);
            }else{
                Page<District> districtPage = districtService.findAll4(active, cityId, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, districtPage.getContent()).setPageElement(pageRow).setTotalPage(districtPage.getTotalPages()).setTotalElement(districtPage.getTotalElements());
            }
        }
    }

    @GetMapping(path = "/district/{id}")
    @UniversalRest
    public WSResponse findDistrictId(@PathVariable Long id) {
        District district = districtService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, district);
    }

    @GetMapping(path = "/district/delete/{id}")
    @UniversalRest
    public WSResponse deleteDistrict(@PathVariable Long id){
        districtService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    /**
     * END DISTRICT
    * */


    /**
     * CITY
     *
    * */

    @Autowired
    CityService cityService;


    @GetMapping(path = "/cities")
    @UniversalRest
    public WSResponse findCities(
            @RequestParam(required = false) Long provinceId,
            @RequestParam(required = false) Boolean active,
            @RequestParam(required = false) Integer page,
            @RequestParam(defaultValue = "name") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){
        Sort sort = new Sort(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir);
        List<City> cities = new ArrayList<>();
        if(provinceId==null){
            if(page==null){
                cities = cityService.findAll(active, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, cities);
            }else{
                Page<City> cityPage = cityService.findAll3(active, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, cityPage.getContent()).setPageElement(pageRow).setTotalPage(cityPage.getTotalPages()).setTotalElement(cityPage.getTotalElements());
            }
        }else{
            if(page==null){
                cities = cityService.findByProvinceId(active, provinceId, sort);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, cities);
            }else{
                Page<City> cityPage = cityService.findAll4(active, provinceId, page, sortir, ascending);
                return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, cityPage.getContent()).setPageElement(pageRow).setTotalPage(cityPage.getTotalPages()).setTotalElement(cityPage.getTotalElements());
            }
        }
    }

    @GetMapping(path = "/city/{id}")
    @UniversalRest
    public WSResponse findCityById(@PathVariable Long id) {
        City city = cityService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, city);
    }

    @GetMapping(path = "/city/delete/{id}")
    @UniversalRest
    public WSResponse deleteCity(@PathVariable Long id){
        cityService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

    /**
     * END CITY
    * */


    /**
     * PROVINCE
     * */

    @Autowired
    ProvinceService provinceService;


    @GetMapping(path = "/provinces")
    @UniversalRest
    public WSResponse findProvinces(
            @RequestParam(required = false) Boolean active,
            @RequestParam(required = false) Integer page,
            @RequestParam(defaultValue = "name") String sortir,
            @RequestParam(defaultValue = "true") Boolean ascending){

        if(page==null){
            Sort sort = new Sort(ascending?Sort.Direction.ASC: Sort.Direction.DESC, sortir);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, provinceService.findAll(active, sort));
        }else{
            Page<Province> provincePage = provinceService.findAll3(active, page, sortir, ascending);
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, provincePage.getContent())
                    .setTotalElement(provincePage.getTotalElements()).setTotalPage(provincePage.getTotalPages())
                    .setPageElement(pageRow);
        }
    }

    @GetMapping(path = "/province/{id}")
    @UniversalRest
    public WSResponse findProvinceById(@PathVariable Long id) {
        Province province = provinceService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, province);
    }

    @GetMapping(path = "/province/delete/{id}")
    @UniversalRest
    public WSResponse deleteProvince(@PathVariable Long id){
        provinceService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }


    /**
     * END PROVINCE
     * */

}
