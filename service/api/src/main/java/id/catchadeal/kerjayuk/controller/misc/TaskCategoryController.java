package id.catchadeal.kerjayuk.controller.misc;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.misc.TaskCategory;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.wsresponse.misc.WSTaskCategories;
import id.catchadeal.kerjayuk.model.wsresponse.misc.WSTaskCategory;
import id.catchadeal.kerjayuk.service.business.TaskService;
import id.catchadeal.kerjayuk.service.misc.TaskCategoryService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class TaskCategoryController extends BasicController {


    @Autowired
    TaskCategoryService taskCategoryService;
    @Autowired
    TaskService taskService;
    @Value("${task.category.dir}") String taskCategoryDir ;


    @GetMapping(path = "/taskCategories")
    @UniversalRest@AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSTaskCategories.class)
    public WSResponse findAll(
            @RequestParam(required = false) String type,
            @RequestParam(defaultValue = "true") Boolean active,
            @RequestParam(defaultValue = "true") Boolean ascending,
            @RequestParam(defaultValue = "sortir") String sortir) {
        List<TaskCategory> taskCategories = new ArrayList<>();
        if(active!=null){
            taskCategories = taskCategoryService.findByActive(active, Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir));
            if(StringUtils.isNotEmpty(type)){
                if(type.equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
                    List<TaskCategory> taskCategoryList = new ArrayList<>();
                    taskCategories.forEach(taskCategory -> {
                        if(taskCategory.getId().equals(TaskCategoryDto.REGULER_ID)){
                            taskCategoryList.add(taskCategory);
                        }
                    });
                    return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskCategoryList);
                }
            }
        }else{
            taskCategories = taskCategoryService.findAll(Sort.by(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir));
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskCategories);
    }


    @PostMapping(path = "/taskCategory/save")
    @AdminRest
    @ApiOperation(value = "", response = WSTaskCategory.class)
    public WSResponse save(@RequestParam String taskCategoryJson,
                                       @RequestParam(required = false) MultipartFile multipartFile) throws Exception {

        TaskCategoryDto taskCategoryDto = gson.fromJson(taskCategoryJson, TaskCategoryDto.class);

        TaskCategory taskCategory = modelMapper.map(taskCategoryDto, TaskCategory.class);

        taskCategory = taskCategoryService.save(taskCategory);
        boolean needUpdate = false;
        if(multipartFile!=null){
            String filename = String.valueOf(new Date().getTime());
            String image = fileStorage.storeFile(taskCategoryDir, null, filename, multipartFile);
            taskCategory.setDefaultIcon(image);
            needUpdate = true ;
        }
        if(needUpdate){
            taskCategory = taskCategoryService.save(taskCategory);
        }
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, taskCategory);
    }

    @GetMapping(path = "/taskCategory/delete")
    @Override
    @AdminRest
    public WSResponse delete(@RequestParam Long id) {

        TaskCategory taskCategory = taskCategoryService.findById(id);
        if(taskService.existsByTaskCategoryId(taskCategory.getId())){
            throw new AppException(Constant.FAILED_CODE, "There is already job vacancy which is using this job category");
        }
        taskCategoryService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }
}
