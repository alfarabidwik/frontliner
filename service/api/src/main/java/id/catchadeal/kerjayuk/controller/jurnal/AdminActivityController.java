package id.catchadeal.kerjayuk.controller.jurnal;

import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.service.jurnal.AdminActivityService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.Utils;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class AdminActivityController extends BasicController {

    public static final Logger logger = LoggerFactory.getLogger(AdminActivityController.class);

    @Autowired
    AdminActivityService adminActivityService;



    @GetMapping(path = "/adminActivities")
    public WSResponse findAll(
            @RequestParam(required = false) String type,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @ApiParam(example = "yyyy-MM-dd") @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
            @RequestParam(defaultValue = "") String adminNames,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "created") String sortir,
            @RequestParam(defaultValue = "false") Boolean ascending){

        Utils.validateParam(type, sortir);

        List<String> stringAdminNames = new ArrayList<>();
        if(adminNames.contains(",")){
            String[] adminIdArray = adminNames.split(",");
            for (int i = 0; i < adminIdArray.length; i++) {
                String name = adminIdArray[i];
                stringAdminNames.add(name.trim());
            }
        }else{
            adminNames = adminNames.trim();
            if(!adminNames.isEmpty()){
                stringAdminNames.add(adminNames);
            }
        }
        DataPage<AdminActivity> dataPage = adminActivityService.findAll(type, startDate, endDate, stringAdminNames, page, sortir, ascending);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage()).setPageElement(dataPage.getPageElement());
    }

    @GetMapping(path = "/adminActivity/types")
    public WSResponse types(){
        List<AbstractMap.SimpleEntry> list = AdminActivity.allTypeEntries();
        for (int i = 0; i < list.size(); i++) {
            String value = (String) list.get(i).getValue();
            list.get(i).setValue(message(value));
        }
      return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, list);
    }


}
