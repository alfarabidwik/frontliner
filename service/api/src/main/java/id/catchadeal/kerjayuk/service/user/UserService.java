package id.catchadeal.kerjayuk.service.user;

import id.catchadeal.kerjayuk.entity.business.BalanceMutation;
import id.catchadeal.kerjayuk.entity.region.City;
import id.catchadeal.kerjayuk.entity.region.District;
import id.catchadeal.kerjayuk.entity.region.Province;
import id.catchadeal.kerjayuk.entity.region.Village;
import id.catchadeal.kerjayuk.entity.user.User;
import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.model.business.MutationInfo;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.user.UserTaskSummary;
import id.catchadeal.kerjayuk.repository.user.UserRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Constant;
import id.catchadeal.kerjayuk.util.DaoHelper;
import id.catchadeal.kerjayuk.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;

@Service
public class UserService extends BasicRepoService<User> {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired private UserRepository repository ;



    @Override
    public JpaRepository repository() {
        return repository;
    }

    public User findTopByEmailOrMobilePhoneAndActive(String subjectKey){
        return Optional.ofNullable(repository.findTopByEmailOrMobilePhoneAndActive(subjectKey, true, Sort.by(Sort.Direction.DESC, "created"))).orElseThrow(() ->
                new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "mobilePhone"))
        );
    }

    public User findTopByMobilePhoneWithStatusActive(String mobilePhone){
        return Optional.ofNullable(repository.findTopByMobilePhoneAndActive(mobilePhone, true, Sort.by(Sort.Direction.DESC, "created"))).orElseThrow(() ->
                new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "mobilePhone"))
        );
    }

    public User findTopByEmailWithStatusActive(String email){
        return Optional.ofNullable(repository.findTopByEmailAndActive(email, true, Sort.by(Sort.Direction.DESC, "created"))).orElseThrow(() ->
                new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "mobilePhone"))
        );
    }


    public Long findUserIdByPhoneNumberAndActive(String mobilePhone){
        return Optional.ofNullable(repository.findUserIdByPhoneNumberAndActive(mobilePhone, true, Sort.by(Sort.Direction.DESC, "created"))).orElseThrow(() ->
                new AppException(Constant.FAILED_CODE, message("not.found.on.the.system", "mobilePhone"))
        );
    }

    public Boolean existsByMobilePhoneAndActiveIsTrue(String mobilePhone){
        return repository.existsByMobilePhoneAndActiveIsTrue(mobilePhone);
    }

    public Boolean existsByEmailAndActiveIsTrue(String mobilePhone){
        return repository.existsByEmailAndActiveIsTrue(mobilePhone);
    }


    public Page<User> findByActive(Boolean active, Pageable pageable){
        return repository.findByActive(active, pageable);
    }


    public List<Object[]> findMobilePhoneByActive(Boolean active){
        return repository.findMobilePhoneByActive(active, Sort.by(Sort.Direction.DESC, "created"));
    }

    public Boolean existsByEmailOrMobilePhone(String email, String mobilePhone){
        return repository.existsByEmailOrMobilePhone(email, mobilePhone);
    }

    public Boolean existsByAgentCodeAndActiveIsTrue(String agentCode){
        return repository.existsByAgentCodeAndActiveIsTrue(agentCode);
    }



    /**
     * TODO EDIT & Fix BELOW CODE
    *
    * */

    public DataPage<User> findAll(Integer page,String sortir, Boolean ascending,String search) {
        Map<String, Object> parameterMap = new HashMap<>();
        String selectObject = " SELECT c ";
        String queryFrom = " FROM User c";
        String groupBy = " GROUP BY u.id, u.created ";
        String orderBy = " ORDER BY "+sortir+""+(ascending?" ASC ":" DESC ");

        if(!StringUtils.isBlank(search)){
            queryFrom = queryFrom+" WHERE LOWER(COALESCE(u.firstname, '')) LIKE (CONCAT('%',LOWER(:search),'%')) " +
                    "OR LOWER(COALESCE(u.lastname, '')) LIKE (CONCAT('%',LOWER(:search),'%')) " +
                    "OR LOWER(COALESCE(u.email, '')) LIKE (CONCAT('%',LOWER(:search),'%')) " +
                    "OR LOWER(COALESCE(u.phoneNumber, '')) LIKE (CONCAT('%',LOWER(:search),'%')) " +
                    "";
            parameterMap.put("search", search);
        }

        Long countResult = DaoHelper.rowCount(entityManager, "u.id", queryFrom+groupBy+orderBy, parameterMap);
        int offset = page*pageRow;

        Query queryObject = entityManager.createQuery(selectObject+queryFrom+groupBy+orderBy);
        queryObject.setFirstResult(offset);
        queryObject.setMaxResults(pageRow);
        for(String key :parameterMap.keySet()) {
            queryObject.setParameter(key, parameterMap.get(key));
        }
        List<User> users = queryObject.getResultList();
        if(users!=null){
            return DataPage.builder(users, pageRow, countResult);
        }else{
            return DataPage.builder(new ArrayList(), pageRow, 0);
        }


        //        return repository.findByFirstnameOrLastnameContaining(search, PageRequest.of(page, pageRow, Utils.sort(ascending, sortir)));
    }

    /**
     SELECT u.*, mcs.name as status_name, mg.name as gender_name, mu.name city_name, ciu.cicCount as cartCount , cpf.cpfCount as favoriteCount, trx.transactionCount as transactionCount, trx.startCreated as startCreated, trx.endCreated as endCreated
     FROM j_user c
     LEFT JOIN m_user_status mcs ON mcs.id = u.user_status_id
     LEFT JOIN m_gender mg ON mg.id = u.gender_id
     LEFT JOIN m_village mv ON u.village_id = mv.id
     LEFT JOIN m_district md ON md.id = mv.district_id
     LEFT JOIN m_city mc ON mu.id = md.city_id
     LEFT JOIN (SELECT ciu.user_id as userId, count(ciu.user_id) as cicCount FROM pv_user_inventory_cart cic GROUP BY ciu.user_id ) as cic ON ciu.userId = u.id
     LEFT JOIN (SELECT cpf.user_id as userId, count(cpf.user_id) as cpfCount FROM pv_user_product_favorite cpf GROUP BY cpf.user_id ) as cpf ON cpf.userId = u.id
     LEFT JOIN (SELECT trx.user_id as userId, count(trx.user_id) as transactionCount, min(trx.created) as startCreated, max(trx.created) endCreated FROM j_transaction trx GROUP BY trx.user_id ) as trx ON trx.userId = u.id
     * */

    public DataPage<User> userInCustomFilteringNative(Integer page, String sortir, boolean asc, String search, Boolean deleted,
                                                              Date userCreatedStart, Date userCreatedEnd,
                                                              String userStatus, String verificationStatus, String gender,
                                                              Long provinceId, Long cityId,
                                                              Long districtId, Long villageId
    ) {
        List<User> users = new ArrayList<>();
        Map<String, Object> paramaterMap = new HashMap<String, Object>();

        String fetchedObject =
                "SELECT u.id, u.active, u.created, u.created_by, u.updated, u.updated_by, u.address, u.birthdate, " +
                        "u.email, u.firstname, u.lastname, u.mobile_phone, " +
                        "u.agent_code, u.organization, u.total_income, u.photo " +
                        ", u.user_status, u.gender " +
                        ", mp.id as province_id, coalesce(mp.name, '') as prpvince_name " +
                        ", mc.id as city_id, coalesce(mc.name, '') as city_name " +
                        ", md.id as district_id, coalesce(md.name, '') as district_name " +
                        ", mv.id as village_id, coalesce(mv.name, '') as village_name, coalesce(mv.postal_code, '') as village_postal_code  ";

        String fetchedObjectCount =
                "SELECT u.id, u.active, u.created, u.created_by, u.updated, u.updated_by, u.address, u.birthdate, " +
                        "u.email, u.firstname, u.lastname,u.mobile_phone, " +
                        "u.agent_code, u.organization,  u.total_income, u.photo " +
                        ", u.user_status, u.gender " +
                        ", mp.id as province_id, coalesce(mp.name, '') as prpvince_name " +
                        ", mc.id as city_id, coalesce(mc.name, '') as city_name " +
                        ", md.id as district_id, coalesce(md.name, '') as district_name " +
                        ", mv.id as village_id, coalesce(mv.name, '') as village_name, coalesce(mv.postal_code, '') as village_postal_code ";

        String sql = " FROM j_user u " +
                "LEFT JOIN m_village mv ON u.village_id = mv.id " +
                "LEFT JOIN m_district md ON md.id = mv.district_id " +
                "LEFT JOIN m_city mc ON mc.id = md.city_id " +
                "LEFT JOIN m_province mp ON mp.id = mc.province_id " +
                "WHERE(CONCAT(lower(u.firstname),' ',lower(u.lastname)) LIKE CONCAT('%',lower(:search),'%') " +
                "OR lower(u.email) LIKE CONCAT('%',lower(:search),'%') " +
                "OR lower(u.agent_code) LIKE CONCAT('%',lower(:search),'%') " +
                "OR lower(u.organization) LIKE CONCAT('%',lower(:search),'%') " +
                "OR lower(u.mobile_phone) LIKE CONCAT('%',lower(:search),'%')) ";
        paramaterMap.put("search", search);
        StringBuilder queryBuilder = new StringBuilder();

        if(deleted!=null){
            queryBuilder.append(" AND u.active != :deleted ");
            paramaterMap.put("deleted", deleted);
        }

        if(userCreatedStart!=null && userCreatedEnd!=null){
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(userCreatedEnd);
            endDate.set(Calendar.DAY_OF_MONTH, endDate.get(Calendar.DAY_OF_MONTH)+1);

            queryBuilder.append(" AND u.created >= :userCreatedStart ");
            paramaterMap.put("userCreatedStart", userCreatedStart);

            queryBuilder.append(" AND u.created <= :userCreatedEnd ");
            paramaterMap.put("userCreatedEnd", endDate.getTime());
        }

        if(StringUtils.isNotEmpty(userStatus)){
            queryBuilder.append(" AND u.user_status = :userStatus ");
            paramaterMap.put("userStatus", userStatus);
        }

        if(StringUtils.isNotEmpty(verificationStatus)){
            queryBuilder.append(" AND u.verification_status = :verificationStatus ");
            paramaterMap.put("verificationStatus", verificationStatus);
        }

        if(StringUtils.isNotEmpty(gender)){
            queryBuilder.append(" AND u.gender = :gender ");
            paramaterMap.put("gender", gender);
        }

        if(provinceId!=null && provinceId!=0){
            queryBuilder.append(" AND mp.id = :provinceId ");
            paramaterMap.put("provinceId", provinceId);
        }
        if(cityId!=null && cityId!=0){
            queryBuilder.append(" AND mc.id = :cityId ");
            paramaterMap.put("cityId", cityId);
        }
        if(districtId!=null && districtId!=0){
            queryBuilder.append(" AND md.id = :districtId ");
            paramaterMap.put("districtId", districtId);
        }
        if(villageId!=null && villageId!=0){
            queryBuilder.append(" AND mv.id = :villageId ");
            paramaterMap.put("villageId", villageId);
        }

        queryBuilder.append(" GROUP BY u.id, mp.id, mc.id, md.id, mv.id ");

        String finalSql = fetchedObject+sql+queryBuilder.toString()+" ORDER BY "+sortir+""+(asc?" ASC ":" DESC ");

        String finalSqlCount = fetchedObjectCount+sql+queryBuilder.toString()+" ORDER BY "+sortir+""+(asc?" ASC ":" DESC ");
        int offset = page*pageRow;
        long countResult = 0 ;
        try {
            Query queryTotal = entityManager.createNativeQuery(finalSqlCount);
            for(String key :paramaterMap.keySet()) {
                queryTotal.setParameter(key, paramaterMap.get(key));
            }
            countResult = (long) queryTotal.getResultList().size();
        }catch (Exception e){
            countResult = 0 ;
        }


        Query query = entityManager.createNativeQuery(finalSql).unwrap( org.hibernate.query.NativeQuery.class ).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

        query.setFirstResult(offset);
        query.setMaxResults(pageRow);

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<Map> objects = new ArrayList<>();
        try{
            objects = query.getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }

        for (int i = 0; i < objects.size(); i++) {
//            User user = (User) objects.get(i)[0];
//            users.add(user);
            Map map = objects.get(i);
            User user = new User();
            user.setId(Utils.cast(Number.class, map.get("id")).longValue());
            user = findById(user.getId());
//            user.setActive(Utils.cast(Boolean.class, map.get("active")));
//            user.setCreated(Utils.cast(Date.class,map.get("created")));
//            user.setCreatedBy(Utils.cast(Number.class, map.get("created_by")).longValue());
//            user.setUpdated(Utils.cast(Date.class, map.get("updated")));
//            user.setUpdatedBy(Utils.cast(Number.class, map.get("updated_by")).longValue());
//            user.setAddress(Utils.cast(String.class, map.get("address")));
//            user.setBirthdate(Utils.cast(Date.class, map.get("birthdate")));
//            user.setEmail(Utils.cast(String.class, map.get("email")));
//            user.setFirstname(Utils.cast(String.class, map.get("firstname")));
//            user.setLastname(Utils.cast(String.class, map.get("lastname")));
//            user.setMobilePhone(Utils.cast(String.class, map.get("mobile_phone")));
//            user.setPhoto(Utils.cast(String.class, map.get("photo")));
//            user.setGender(Utils.cast(String.class, map.get("gender")));
//            user.setUserStatus(Utils.cast(String.class, map.get("user_status")));
//            user.setAgentCode(Utils.cast(String.class, map.get("agent_code")));
//            user.setOrganization(Utils.cast(String.class, map.get("organization")));
//            user.setTotalIncome(Utils.cast(BigDecimal.class, map.get("total_income")));
//            user.setVerificationStatus(Utils.cast(String.class, map.get("verification_status")));
//
//            Village village = new Village();
//            village.setName(Utils.cast(String.class, map.get("village_name")));
//            village.setId(Utils.cast(Number.class, map.get("village_id")));
//            village.setPostalCode(Utils.cast(String.class, map.get("village_postal_code")));
//
//            District district = new District();
//            district.setName(Utils.cast(String.class, map.get("district_name")));
//            district.setId(Utils.cast(Number.class, map.get("district_id")));
//
//            City city = new City();
//            city.setName(Utils.cast(String.class, map.get("city_name")));
//            city.setId(Utils.cast(Number.class, map.get("city_id")));
//
//            Province province = new Province();
//            province.setName(Utils.cast(String.class, map.get("province_name")));
//            province.setId(Utils.cast(Number.class, map.get("province_id")));
//
//            city.setProvince(province);
//            district.setCity(city);
//            village.setDistrict(district);
//            user.setVillage(village);

            user.initTransient();
            users.add(user);
        }
        DataPage dataPage = DataPage.builder(users, pageRow, countResult);
        return dataPage;
    }

    public User findByEmail(String email){
        return repository.findByEmail(email);
    }


    public User findByMobilePhone(String mobilePhone){
        return repository.findByMobilePhone(mobilePhone);
    }
    public Boolean existsByEmail(String email){
        return repository.existsByEmail(email);
    }

    public Boolean existsByMobilePhone(String mobilePhone){
        return repository.existsByMobilePhone(mobilePhone);
    }

    public List<String> firstnames(String sortir, Boolean ascending){
        return repository.firstnames(new Sort(ascending? Sort.Direction.ASC: Sort.Direction.DESC, sortir));
    }

    public List<String> emails(Sort sort){
        return repository.emails(sort);
    }

    public User findByAgentCodeAndUserStatusIsActive(String agentCode){
        User user = repository.findByAgentCode(agentCode);
        if(user.getUserStatus().equalsIgnoreCase(User.BLOCKED)){
            throw new AppException(Constant.FAILED_CODE, "user.has.been.blocked");
        }
        return user ;
    }

    public UserTaskSummary userTaskSummary(Long userId){
        UserTaskSummary userTaskSummary = new UserTaskSummary();
        String sql = "SELECT " +
                "(SELECT COUNT(jta.id) FROM j_task_application jta WHERE jta.user_id = :userId AND jta.task_application_status_id = :taskApplicationStatusId ) as onGoingTask, " +
                "(SELECT COUNT(jt.id) FROM j_task jt WHERE jt.published = :published AND ((jt.period_type = '"+ TaskDto.PERIODIC +"' AND jt.start_period >= :currentDate AND jt.end_period <= :currentDate) " +
                "OR (jt.period_type = '"+ TaskDto.NEVER_END+"')) " +
                "AND ((jt.application_limit_type = '"+TaskDto.NO_LIMIT+"') " +
                "OR (jt.application_limit_type = '"+TaskDto.LIMITED+"' AND jt.application_limit_count > jt.application_count))) as totalTask ";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userId", userId);
        query.setParameter("taskApplicationStatusId", TaskApplicationStatusDto.TAKEN_ID);
        query.setParameter("published", true);
        query.setParameter("currentDate", new Date());
        query.unwrap( org.hibernate.query.NativeQuery.class ).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        Map map = (Map) query.getSingleResult();
        userTaskSummary.setOnGoingTask(Utils.cast(Long.class, map.get("onGoingTask".toLowerCase())));
        userTaskSummary.setTotalTask(Utils.cast(Long.class, map.get("totalTask".toLowerCase())));

        logger.debug("MAP #### "+gson.toJson(userTaskSummary));
        return userTaskSummary ;
    }

    public void updateWithdrawalRequest(boolean addOrSubstract, BigDecimal amount, User user){
        if(addOrSubstract){
            user.setTotalWithdrawal(user.getTotalWithdrawal().add(amount));
        }else{
            user.setTotalWithdrawal(user.getTotalWithdrawal().subtract(amount));
        }
        save(user);
    }



}
