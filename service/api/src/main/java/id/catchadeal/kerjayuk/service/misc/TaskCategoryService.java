package id.catchadeal.kerjayuk.service.misc;

import id.catchadeal.kerjayuk.entity.misc.TaskCategory;
import id.catchadeal.kerjayuk.repository.misc.TaskCategoryRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskCategoryService extends BasicRepoService<TaskCategory> {

    @Autowired private TaskCategoryRepository repository ;

    @Override
    public JpaRepository repository() {
        return repository;
    }

    public void firstGenerate(){

    }

    public boolean existById(Long id){
        return repository.existsById(id);
    }

    public List<TaskCategory> findByActive(Boolean active, Sort sort){
      return repository.findByActive(active, sort);
    }

}
