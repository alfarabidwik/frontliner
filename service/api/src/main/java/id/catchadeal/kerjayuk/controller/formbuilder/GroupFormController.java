package id.catchadeal.kerjayuk.controller.formbuilder;//package com.alfa.ecommerce.controller;

import id.catchadeal.kerjayuk.component.FileStorage;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.formbuilder.GroupForm;
import id.catchadeal.kerjayuk.entity.jurnal.AdminActivity;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.formbuilder.GroupFormDto;
import id.catchadeal.kerjayuk.service.formbuilder.GroupFormService;
import id.catchadeal.kerjayuk.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "${api}")
public class GroupFormController extends BasicController {

    public static final Logger logger = LoggerFactory.getLogger(GroupFormController.class);

    @Autowired GroupFormService groupFormService;
    @Autowired FileStorage fileStorage;

    @GetMapping(path = "/groupForms")
    public WSResponse findAll(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "true") Boolean ascending,
            @RequestParam(defaultValue = "sortir") String sortir){

        if(page!=null){
            Page<GroupForm> groupFormPage = groupFormService.findAll(PageRequest.of(page,  pageRow, Sort.by(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir)));
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, groupFormPage.getContent())
                    .setTotalPage(groupFormPage.getTotalPages())
                    .setTotalElement(groupFormPage.getTotalElements())
                    .setPageElement(pageRow);
        }else{
            List<GroupForm> groupForms = groupFormService.findAll(Sort.by(ascending?Sort.Direction.ASC:Sort.Direction.DESC, sortir));
            return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, groupForms);
        }

    }



    @GetMapping(path = "/groupForm/{id}")
    @Override
    public WSResponse findById(@PathVariable Long id) {
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, groupFormService.findById(id));
    }

    @PostMapping(path = "/groupForm/save")
    public WSResponse save(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody @Valid GroupFormDto groupFormDto){
        GroupForm groupForm = modelMapper.map(groupFormDto, GroupForm.class);
        boolean newdata = groupForm.getId()==null || groupForm.getId()==0 ;
        if(newdata){
            commitAdminActivity(authorization, AdminActivity.CREATE_NEW_GROUP_FORM, groupFormDto);
        }else{
            commitAdminActivity(authorization, AdminActivity.UPDATE_GROUP_FORM, groupFormDto, groupFormService.findById(groupForm.getId()));
        }
        groupForm = groupFormService.save(groupForm);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, groupForm);
    }

    @GetMapping(path = "/groupForm/delete/{id}")
    public WSResponse delete(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable Long id) {
        GroupForm groupForm = groupFormService.findById(id);
        groupFormService.delete(id);
        commitAdminActivity(authorization, AdminActivity.DELETE_BANNER, groupForm);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }
}
