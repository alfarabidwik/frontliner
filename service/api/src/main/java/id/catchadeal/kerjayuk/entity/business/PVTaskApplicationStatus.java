package id.catchadeal.kerjayuk.entity.business;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name="pv_task_application_status", indexes = {
        @Index(columnList = "created", name = "PVTaskApplicationStatus_created_idx"),
        @Index(columnList = "updated", name = "PVTaskApplicationStatus_updated_idx"),
        @Index(columnList = "active", name = "PVTaskApplicationStatus_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class PVTaskApplicationStatus extends EBase {

    @ManyToOne(fetch= FetchType.LAZY)@JsonBackReference
    @Fetch(FetchMode.JOIN) @JsonIgnoreProperties("pvTaskApplicationStatuses")
    @JoinColumn(name="task_application_id", updatable = false, nullable = false)
    TaskApplication taskApplication;

    @ManyToOne(fetch= FetchType.LAZY)@JsonManagedReference
    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("pvTaskApplicationStatuses")
    @JoinColumn(name="task_application_status_id", nullable = false)
    TaskApplicationStatus taskApplicationStatus;


    @Column(columnDefinition = "TEXT default ''")
    String note ;


}
