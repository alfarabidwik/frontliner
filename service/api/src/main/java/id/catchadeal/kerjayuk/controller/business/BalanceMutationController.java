package id.catchadeal.kerjayuk.controller.business;

import id.catchadeal.kerjayuk.annotation.AdminRest;
import id.catchadeal.kerjayuk.annotation.FrontendRest;
import id.catchadeal.kerjayuk.annotation.UniversalRest;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.model.wsresponse.user.WSRegistrations;
import id.catchadeal.kerjayuk.service.business.BalanceMutationService;
import id.catchadeal.kerjayuk.util.Constant;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(path = "${api}")
public class BalanceMutationController extends BasicController {

    @Autowired
    BalanceMutationService balanceMutationService ;

    @GetMapping(path = "/balanceMutations")
    @AdminRest@FrontendRest
    @ApiOperation(value = "", response = WSRegistrations.class)
    public WSResponse findAll(
            @RequestHeader(Constant.AUTHORIZATION) String authorization,
            @RequestParam(required = false) Long userId,
            @RequestParam(defaultValue = "") String search,
            @ApiParam(example = DateAppConfig.API_DATE, required = false) @RequestParam(required = false) @DateTimeFormat(pattern = DateAppConfig.API_DATE) Date startDate,
            @ApiParam(example = DateAppConfig.API_DATE, required = false) @RequestParam(required = false) @DateTimeFormat(pattern = DateAppConfig.API_DATE) Date endDate,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "false") Boolean ascending,
            @RequestParam(defaultValue = "bm.created") String sortir) {
        DataPage dataPage = balanceMutationService.balanceMutationDataPage(userId, search, startDate, endDate, page, ascending, sortir);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, dataPage.getDatas())
                .setPageElement(dataPage.getPageElement()).setTotalElement(dataPage.getTotalElement()).setTotalPage(dataPage.getTotalPage());
    }


}
