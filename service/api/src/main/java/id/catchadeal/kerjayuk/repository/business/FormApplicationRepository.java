package id.catchadeal.kerjayuk.repository.business;

import id.catchadeal.kerjayuk.entity.business.FormApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormApplicationRepository extends JpaRepository<FormApplication, Long> {

}
