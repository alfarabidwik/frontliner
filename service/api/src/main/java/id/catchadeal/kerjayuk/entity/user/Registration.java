package id.catchadeal.kerjayuk.entity.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.util.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Data
@Table(name=Registration.TABLE, indexes = {
        @Index(columnList = "mobilePhone", name = "Registration_mobilePhone_idx", unique = false),
        @Index(columnList = "email", name = "Registration_email_idx", unique = false),
        @Index(columnList = "created", name = "Registration_created_idx"),
        @Index(columnList = "updated", name = "Registration_updated_idx"),
        @Index(columnList = "active", name = "Registration_active_idx"),
})
@Entity
@ToString(of = "id", callSuper = true)
@EqualsAndHashCode(callSuper = true, of = "id")
public class Registration extends EBase {

    public static final String TABLE = "j_registration";

    public static final String ADMIN = "ADMIN";
    public static final String SELF_REGISTRATION = "SELF_REGISTRATION";
    public static final String SYSTEM = "SYSTEM";


    String firstname ;
    String lastname ;
    String mobilePhone ;
    String email ;
    String referralCode ;
    String executor ;
    String agentCode ;

    @Transient
    String fullname ;

    @Override
    public void initTransient() {
        super.initTransient();
        lastname = StringUtils.defaultString(lastname);
        if(StringUtils.isNotEmpty(fullname) && (StringUtils.isEmpty(firstname) || StringUtils.isEmpty(lastname))){
            if(fullname.contains(" ")){
                this.firstname = fullname.split(" ")[0];
                this.lastname = fullname.split(" ", 2)[1];
            }else{
                this.firstname = fullname ;
            }
        }else{
            this.fullname = this.firstname +" "+ this.lastname;
        }
    }


//    @OneToOne(mappedBy = "registration", fetch= FetchType.LAZY)@JsonManagedReference
//    @Fetch(FetchMode.JOIN)@JsonIgnoreProperties("registration")
//    User user;


}
