package id.catchadeal.kerjayuk.entity.formbuilder;

import id.catchadeal.kerjayuk.entity.EBase;
import id.catchadeal.kerjayuk.model.formbuilder.FormLane;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name= GroupForm.TABLE)
@Entity
@ToString(of = "id")
@EqualsAndHashCode(callSuper = true, of = "id")
public class GroupForm extends EBase {

    public static final String TABLE = "j_group_form";
    String name ;
    @Type(type = "jsonb")
    @Column(name = "form_lanes", columnDefinition = "jsonb", nullable = false)
    Set<FormLane> formLanes = new HashSet<>();

}
