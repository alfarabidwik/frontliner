package id.catchadeal.kerjayuk.controller.role;

import id.catchadeal.kerjayuk.controller.BasicController;
import id.catchadeal.kerjayuk.entity.role.RoleMenu;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.role.RoleMenuDto;
import id.catchadeal.kerjayuk.service.role.RoleMenuService;
import id.catchadeal.kerjayuk.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "${api}")
public class RoleMenuController extends BasicController {

    @Autowired
    RoleMenuService roleMenuService;


    @GetMapping(path = "/roleMenus")
    public WSResponse findAll(){
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, roleMenuService.findAll());
    }

    @GetMapping(path = "/roleMenu/{id}")
    @Override
    public WSResponse findById(Long id) {
        RoleMenu roleMenu = roleMenuService.findById(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, roleMenu);
    }

    @PostMapping(path = "/roleMenu/save")
    public WSResponse save(@RequestBody @Valid RoleMenuDto roleMenuDto){
        RoleMenu roleMenu = modelMapper.map(roleMenuDto, RoleMenu.class);
        roleMenu = roleMenuService.save(roleMenu);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS, roleMenu);
    }

    @GetMapping(path = "/roleMenu/delete/{id}")
    public WSResponse delete(@PathVariable Long id){
        roleMenuService.delete(id);
        return WSResponse.instance(Constant.SUCCESS_CODE, Constant.SUCCESS);
    }

}
