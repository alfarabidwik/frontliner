package id.catchadeal.kerjayuk.repository.payment;

import id.catchadeal.kerjayuk.entity.payment.BankAccount;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {


    List<BankAccount> findByActive(Boolean active, Sort sort);



}
