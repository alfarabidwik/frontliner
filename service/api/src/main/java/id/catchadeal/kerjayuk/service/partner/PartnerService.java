package id.catchadeal.kerjayuk.service.partner;

import id.catchadeal.kerjayuk.entity.partner.PVPartnerStatus;
import id.catchadeal.kerjayuk.entity.partner.Partner;
import id.catchadeal.kerjayuk.entity.partner.PartnerStatus;
import id.catchadeal.kerjayuk.model.misc.DataPage;
import id.catchadeal.kerjayuk.repository.partner.PartnerRepository;
import id.catchadeal.kerjayuk.service.BasicRepoService;
import id.catchadeal.kerjayuk.util.Utils;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

@Service
public class PartnerService extends BasicRepoService<Partner> {
    private static final Logger logger = LoggerFactory.getLogger(PartnerService.class);

    @Autowired private PartnerRepository repository ;
    @Autowired private PartnerStatusService partnerStatusService;
    @Autowired private PVPartnerStatusService pvPartnerStatusService;



    @Override
    public JpaRepository repository() {
        return repository;
    }
    public boolean existById(long id){
        return repository.existsById(id);
    }

    public Partner findByWebsite(String website){
        return repository.findByWebsite(website);
    }


    public Partner updateStatus(Partner partner, Long statusId, String cause, String note){
        PartnerStatus partnerStatus = partnerStatusService.findById(statusId);
        Set<PVPartnerStatus> pvPartnerStatuses = partner.getPvPartnerStatuses();
        PVPartnerStatus pvPartnerStatus = new PVPartnerStatus();
        pvPartnerStatus.setPartnerStatus(partnerStatus);
        pvPartnerStatus.setPartner(partner);
        pvPartnerStatus.setCause(cause);
        pvPartnerStatus.setNote(note);
        pvPartnerStatus = pvPartnerStatusService.save(pvPartnerStatus);
        pvPartnerStatuses.add(pvPartnerStatus);
        partner.setPartnerStatus(partnerStatus);
        partner = save(partner);
       return partner;
    }

    public DataPage partners(String keyword, Double latitude, Double longitude,
                              Long partnerStatusId,
                              String type,
                              Long provinceId, Long cityId, Long districtId, Long villageId,
                              Boolean active, Integer page, Integer resultLimit, Boolean ascending, String sortir){
        Map<String, Object> paramaterMap = new HashMap<String, Object>();
        String distanceField = null ;

        if((latitude!=null && latitude.doubleValue()!=0) && (longitude!=null && longitude.doubleValue()!=0)){
            distanceField = "( (6371*1000) * acos( cos( radians(:latitude) ) * cos( radians( jba.latitude ) ) * cos( radians( jba.longitude ) - radians(:longitude) ) + sin( radians(:latitude) ) * sin( radians( jba.latitude ) ) ) ) AS jba_distance,  ";
        }else{
            distanceField = " ";
        }

        String sqlObject = "SELECT "+distanceField+" jp.id ";
        String sqlCount = "SELECT COUNT(jp.id) ";

        String sql = " FROM j_partner jp LEFT JOIN j_company jc ON jp.company_id = jc.id " +
                "LEFT JOIN j_business_address jba ON jp.id = jba.partner_id " +
                "LEFT JOIN m_village mv ON jba.village_id = mv.id " +
                "LEFT JOIN m_district md ON mv.district_id = md.id " +
                "LEFT JOIN m_city mc ON md.city_id = mc.id " +
                "LEFT JOIN m_province mp ON mc.province_id = mp.id " +
                "WHERE 1+1 = 2 ";

        if(active!=null){
            sql = sql+" AND jp.active = :active ";
            paramaterMap.put("active", active);
        }


        if(keyword!=null && !keyword.isEmpty()){
            sql = sql+" AND ( LOWER(COALESCE(jp.website, '')) LIKE CONCAT('%',LOWER(:keyword),'%') OR LOWER(COALESCE(jp.fullname,'')) LIKE CONCAT('%',LOWER(:keyword),'%') ) ";
            paramaterMap.put("keyword", keyword);
        }

        if((latitude!=null && latitude.doubleValue()!=0) && (longitude!=null && longitude.doubleValue()!=0)){
            paramaterMap.put("latitude", latitude);
            paramaterMap.put("longitude", longitude);
        }

        if(partnerStatusId!=null){
            sql = sql+" AND jp.partner_status_id = :partnerStatusId ";
            paramaterMap.put("partnerStatusId", partnerStatusId);
        }
        if(type!=null && !type.isEmpty()){
            sql = sql+" AND jp.partner_type = :type ";
            paramaterMap.put("type", type);
        }
        if(provinceId!=null){
            sql = sql+" AND mp.id = :provinceId ";
            paramaterMap.put("provinceId", provinceId);
        }
        if(cityId!=null){
            sql = sql+" AND mc.id = :cityId ";
            paramaterMap.put("cityId", cityId);
        }
        if(districtId!=null){
            sql = sql+" AND md.id = :districtId ";
            paramaterMap.put("districtId", districtId);
        }
        if(villageId!=null){
            sql = sql+" AND mv.id = :villageId ";
            paramaterMap.put("villageId", villageId);
        }

        String groupBy = " GROUP BY jp.id, jp.created, jba.latitude, jba.longitude ";
        String orderBy = " " ;

        if(sortir!=null && !sortir.isEmpty() && ascending!=null){
            orderBy = " ORDER BY "+sortir+" "+(ascending?" ASC ":" DESC ");
        }else{
            if((latitude!=null && latitude.doubleValue()!=0) && (longitude!=null && longitude.doubleValue()!=0)){
                ascending = ascending==null?true:ascending;
                orderBy = " ORDER BY jba_distance  "+(ascending?" ASC ":" DESC ");
            }
        }

        String finalSql = sqlObject+sql+groupBy+orderBy;
        String finalSqlCount = sqlCount+sql+groupBy+orderBy;
        int offset = 0 ;
        long countResult = 0 ;
        if(page!=null){
            offset = page*resultLimit;
            try {
                Query queryTotal = entityManager.createNativeQuery(finalSqlCount);
                for(String key :paramaterMap.keySet()) {
                    queryTotal.setParameter(key, paramaterMap.get(key));
                }
                countResult = (long) queryTotal.getResultList().size();
            }catch (Exception e){
                countResult = 0 ;
            }
        }
        Query query = entityManager.createNativeQuery(finalSql).unwrap( org.hibernate.query.NativeQuery.class )
                .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        if(page!=null){
            query.setFirstResult(offset);
            query.setMaxResults(resultLimit);
        }

        for(String key :paramaterMap.keySet()) {
            query.setParameter(key, paramaterMap.get(key));
        }
        List<Map> maps = query.getResultList();
        List<Partner> partners = new ArrayList<>();
        maps.forEach(map -> {
            Partner partner = findById(Utils.cast(Number.class, map.get("id")).longValue());
            if((latitude!=null && latitude.doubleValue()!=0) && (longitude!=null && longitude.doubleValue()!=0)){
                Double distance = Utils.cast(Number.class, map.get("jba_distance"), BigInteger.valueOf(0)).doubleValue();
                partner.setDistance(distance);
            }else{
                partner.setDistance(0.0);
            }
            partners.add(partner);
        });

        DataPage dataPage = DataPage.builder(partners, resultLimit, countResult);
        return dataPage ;
    }



}
