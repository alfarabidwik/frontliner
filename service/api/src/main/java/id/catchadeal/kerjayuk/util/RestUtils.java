package id.catchadeal.kerjayuk.util;

import id.catchadeal.kerjayuk.exception.AppException;
import id.catchadeal.kerjayuk.security.JwtTokenProvider;
import id.catchadeal.kerjayuk.service.user.AdminService;
import id.catchadeal.kerjayuk.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RestUtils {

    private static final Logger logger= LoggerFactory.getLogger(RestUtils.class);

    @Autowired JwtTokenProvider jwtTokenProvider ;
    @Autowired UserService userService ;
    @Autowired AdminService adminService ;


    public <UD extends UserDetails> UD user(String authorization){
        if(authorization==null){
            throw new AppException(Constant.FAILED_CODE, "Authorization identifier is not found");
        }
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        String role = roles.get(0);
        if(role.equalsIgnoreCase(Constant.ADMIN)){
            if(authorization!=null){
                return (UD) jwtTokenProvider.getAdmin(authorization);
            }
        }else{
            if(authorization!=null){
                return (UD) jwtTokenProvider.getUser(authorization);
            }
        }

        return null ;
    }


    public Boolean isRole(String authorization, String role){
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        if(roles!=null && roles.size()>=0){
            String tokenRole = roles.get(0);
            if(tokenRole.equalsIgnoreCase(Constant.ADMIN) && tokenRole.equalsIgnoreCase(role)){
                return true ;
            }else{
                if(!tokenRole.equalsIgnoreCase(role) && !tokenRole.equalsIgnoreCase(Constant.USER)){
                    return false ;
                }
                return true ;
            }
        }else{
            return false ;
        }
    }

    public void validateRoleException(String authorization, String role){
        List<String> roles = jwtTokenProvider.getRoles(authorization);
        logger.debug("ROLES {} ", roles);
        if(roles!=null && roles.size()>=0){
            String tokenRole = roles.get(0);
            if(tokenRole.equalsIgnoreCase(Constant.ADMIN) && tokenRole.equalsIgnoreCase(role)){

            }else{
                if(!tokenRole.equalsIgnoreCase(role) && !tokenRole.equalsIgnoreCase(Constant.USER)){
                    throw new AppException(Constant.FAILED_CODE, tokenRole+" shouldn't access this api");
                }
            }
        }else{
            throw new AppException(Constant.FAILED_CODE, " role is not found");
        }
    }



//    public User validateEmployeeStatus(String authorization){
//        User user = jwtTokenProvider.getUser(authorization);
//        if(!user.getEmployee().getEmployeeStatus().getId().equals(EmployeeStatus.GOOD)){
//            throw new AppException(Constant.FAILED_CODE, user.getEmployee().getPvEmployeeStatus().getCause());
//        }
//        return user ;
//    }
//    public User validateVerificationStatus(String authorization){
//        User user = jwtTokenProvider.getUser(authorization);
//        if(!user.getEmployee().isIdCardVerification()){
//            throw new AppException(Constant.FAILED_CODE, "You have unverified id card and selfie photo, please update and wait until our administrator approve it");
//        }
//        return user ;
//    }
//
//
//    public User validateEmployementData(String authorization){
//        User user = jwtTokenProvider.getUser(authorization);
//        if(user.getEmployee().getWillingToBePlacedProvinces()==null || user.getEmployee().getWillingToBePlacedProvinces().size()<=0){
//            throw new AppException(Constant.FAILED_CODE, "You have missing employment data, please complete it first");
//        }
//        return user ;
//    }






}
