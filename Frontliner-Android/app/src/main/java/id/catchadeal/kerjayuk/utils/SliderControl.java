package id.catchadeal.kerjayuk.utils;

public interface SliderControl {

    int getPosition();
}
