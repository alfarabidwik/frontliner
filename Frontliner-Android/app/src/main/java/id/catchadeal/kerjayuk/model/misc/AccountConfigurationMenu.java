package id.catchadeal.kerjayuk.model.misc;

import java.util.ArrayList;
import java.util.List;

import id.catchadeal.kerjayuk.R;
import lombok.Data;

@Data
public class AccountConfigurationMenu {
    int label ;
    int iconResource ;

    public static final List<AccountConfigurationMenu> menus(){
        List<AccountConfigurationMenu> accountMenus = new ArrayList<>();
        AccountConfigurationMenu accountMenu = new AccountConfigurationMenu();
        accountMenu.setLabel(R.string.ChangePhotoProfile);
        accountMenu.setIconResource(R.drawable.ic_photograph);
        accountMenus.add(accountMenu);

        accountMenu = new AccountConfigurationMenu();
        accountMenu.setLabel(R.string.CompleteYourForm);
        accountMenu.setIconResource(R.drawable.ic_checklist);
        accountMenus.add(accountMenu);

        accountMenu = new AccountConfigurationMenu();
        accountMenu.setLabel(R.string.AboutApplication);
        accountMenu.setIconResource(R.drawable.ic_information);
        accountMenus.add(accountMenu);

        return accountMenus ;
    }

}
