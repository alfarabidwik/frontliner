package id.catchadeal.kerjayuk.model.misc;

import java.util.ArrayList;
import java.util.List;

import id.catchadeal.kerjayuk.R;
import lombok.Data;

@Data
public class AccountMenu {
    int label ;
    int iconResource ;

    public static final List<AccountMenu> menus(){
        List<AccountMenu> accountMenus = new ArrayList<>();
        AccountMenu accountMenu = new AccountMenu();
        accountMenu.setLabel(R.string.mitra_id_card);
        accountMenu.setIconResource(R.drawable.ic_idcard);
        accountMenus.add(accountMenu);

        accountMenu = new AccountMenu();
        accountMenu.setLabel(R.string.FAQ);
        accountMenu.setIconResource(R.drawable.ic_faq);
        accountMenus.add(accountMenu);

        accountMenu = new AccountMenu();
        accountMenu.setLabel(R.string.ContactAdmin);
        accountMenu.setIconResource(R.drawable.ic_admin);
        accountMenus.add(accountMenu);

        return accountMenus ;
    }

}
