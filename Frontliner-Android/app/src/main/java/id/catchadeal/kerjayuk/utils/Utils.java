package id.catchadeal.kerjayuk.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.view.View;

import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.function.Consumer;

public class Utils {

    public static boolean equals(Integer first, Integer second){
        if(first!=null && second!=null){
            return false ;
        }
        return first.equals(second);
    }

    public static boolean isSuccess(Integer responseCode){
        return equals(responseCode, 200);
    }


    public static String abbreviete(String input, Integer max){
        if(input==null || input.equalsIgnoreCase(" ")){
            return "" ;
        }
        if(input.length()>max){
            input = input.substring(0, max);
            input = truncateLast(input);
            input = input+"...";
        }
        return input;
    }

    private static String truncateLast(String input){
        if(!input.endsWith(" ")){
            input = input.substring(0, input.length()-1);
            return truncateLast(input);
        }
        input = input.trim();
        return input ;

    }

    public static void openWhatsapp(String phone, String initiateMessage, Activity activity){
        PackageManager packageManager = activity.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        try {
            String url = "https://api.whatsapp.com/send?phone="+ phone;
            if(StringUtils.isNotEmpty(initiateMessage)){
                url = url+"&text=" + URLEncoder.encode(initiateMessage, "UTF-8");
            }
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                activity.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Bitmap getIdCard(View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return bitmap;
    }

    public static void persistImage(Activity activity, String directory, Bitmap bitmap, String name, Consumer<File> consumer) {
        File filesDir = new File(directory);//activity.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
            consumer.accept(imageFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readTextAssetFile(Activity activity, String filename){
        try{
            InputStream inputStream = activity.getAssets().open(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            return  new String(buffer);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

}
