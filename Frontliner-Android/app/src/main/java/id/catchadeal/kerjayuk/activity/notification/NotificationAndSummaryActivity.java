package id.catchadeal.kerjayuk.activity.notification;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.user.BottomNavigationActivity;
import id.catchadeal.kerjayuk.api.NotificationUserApi;
import id.catchadeal.kerjayuk.holder.NotificationUserHolder;
import id.catchadeal.kerjayuk.interfaze.NotificationTopicRecount;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.misc.NotificationUserDto;
import id.catchadeal.kerjayuk.utils.EndlessRecyclerViewScrollListener;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import lombok.Getter;

public class NotificationAndSummaryActivity extends BottomNavigationActivity implements NotificationTopicRecount {

    public static final String TAG = NotificationAndSummaryActivity.class.getName();

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.news_and_information_summary_layout) LinearLayout newsAndInformationSummaryLayout ;
    @BindView(R.id.promotion_summary_layout) LinearLayout promotionSummaryLayout ;

    @BindView(R.id.unread_notification_tv) TextView unreadNotificationTextView ;
    @BindView(R.id.withdrawal_notification_badge_tv) TextView wihdrawalCountTextView;
    @BindView(R.id.news_notification_badge_tv) TextView newsCountTextView;
    @BindView(R.id.promotion_notification_badge_tv) TextView promotionCountTextView;


    @Getter List<NotificationUserDto> notifications = new ArrayList<>();
    @Getter BasicActRecyclerAdapter<NotificationUserDto, NotificationAndSummaryActivity, NotificationUserHolder> adapter ;

    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener ;

    public static NotificationTopicRecount notificationTopicRecount ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_notification_and_summary;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int bottomMenu() {
        return R.id.notification;
    }
    @Override
    public void focusBar() {
        ((BottomNavigationItemView)((BottomNavigationMenuView)bottomNavigationView.getChildAt(0)).getChildAt(1)).setBackgroundColor(getColor(R.color.greenTertiary));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.notifications = new ArrayList<>();
        adapter = new BasicActRecyclerAdapter<>(this, NotificationUserHolder.class, notifications);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        adapter.initRecyclerView(recyclerView, layoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.VERTICAL));

        this.page = 0 ;
        swipeRefreshLayout.setOnRefreshListener(() -> {
                this.page = 0 ;
                fetchNotifications();
        });
        this.endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                NotificationAndSummaryActivity.this.page = page;
                fetchNotifications();
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        unreadNotificationTextView.setText(getString(R.string.UnreadCount, String.valueOf(0)));
        wihdrawalCountTextView.setVisibility(View.GONE);
        newsCountTextView.setVisibility(View.GONE);
        promotionCountTextView.setVisibility(View.GONE);
        notificationTopicRecount = this ;

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void renderData() {
        swipeRefreshLayout.setRefreshing(true);
        fetchNotifications();
        sumCount();

    }

    void sumCount(){
        String newsTopics = NotificationDto.NEWS+","+NotificationDto.ARTICLE;
        countUnreadNotification(newsTopics, "", "", "", aLong -> {
            if(aLong>0){
                newsCountTextView.setVisibility(View.VISIBLE);
                newsCountTextView.setText(String.valueOf(aLong));
            }else{
                newsCountTextView.setVisibility(View.GONE);
            }
        });

        String promotionTopics = NotificationDto.BROADCAST;
        countUnreadNotification(promotionTopics, "", "", "", aLong -> {
            if(aLong>0){
                promotionCountTextView.setVisibility(View.VISIBLE);
                promotionCountTextView.setText(String.valueOf(aLong));
            }else{
                promotionCountTextView.setVisibility(View.GONE);
            }
        });

        String withdrawalTopics = NotificationDto.SPECIFIC;
        String withdrawalTypes = NotificationDto.WITHDRAWAL;
        countUnreadNotification(withdrawalTopics, withdrawalTypes, "", "", aLong -> {
            if(aLong>0){
                wihdrawalCountTextView.setVisibility(View.VISIBLE);
                wihdrawalCountTextView.setText(String.valueOf(aLong));
            }else{
                wihdrawalCountTextView.setVisibility(View.GONE);
            }
        });
        fetchCurrentNotificationCount();

    }


    public void fetchCurrentNotificationCount(){
        String currentTopics = NotificationDto.SPECIFIC+","+NotificationDto.NEW_TASK;
        String currentTypes = "";
        String currentExcludeTypes = NotificationDto.WITHDRAWAL;
        countUnreadNotification(currentTopics, currentTypes, "", currentExcludeTypes, aLong -> {
            unreadNotificationTextView.setText(getString(R.string.UnreadCount, String.valueOf(aLong)));
        });
    }

    @OnClick({R.id.withdrawal_summary_layout, R.id.news_and_information_summary_layout, R.id.promotion_summary_layout})
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.withdrawal_summary_layout){
            String topics = NotificationDto.SPECIFIC;
            String types = NotificationDto.WITHDRAWAL;
            startActivity(NotificationActivity.class, getString(R.string.CashWithdrawal), topics, types);
        }
        if(view.getId()==R.id.news_and_information_summary_layout){
            String topics = NotificationDto.NEWS+","+NotificationDto.ARTICLE;
            String types = "";
            startActivity(NotificationActivity.class, getString(R.string.NewsAndInformation), topics, types);
        }
        if(view.getId()==R.id.promotion_summary_layout){
            String topics = NotificationDto.BROADCAST;
            String types = "";
            startActivity(NotificationActivity.class, getString(R.string.Promotion), topics, types);
        }
    }

    void fetchNotifications(){
        String topics = NotificationDto.SPECIFIC+","+NotificationDto.NEW_TASK;
        String currentExcludeTypes = NotificationDto.WITHDRAWAL;

        grabServerData(Kitchen.arange(this, NotificationUserApi.class).notifications(currentAuthorization, topics,"", null, currentExcludeTypes, page, false, "created"),
                (wsResponse, response) -> {
                    swipeRefreshLayout.setRefreshing(false);
                    if(wsResponse.isSuccess()){
                        if(this.page==0){
                            this.notifications  = new ArrayList<>();
                        }
                        this.notifications.addAll(wsResponse.getData());
                        this.adapter.setObjects(this.notifications);
                        if(this.notifications.size()==0){
                            TextView textView = recyclerView.getEmptyView().findViewById(R.id.message_tv);
                            textView.setText(R.string.ThereIsNoAvailableNotification);
                        }
                        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
                }, false);

    }

    @Override
    public void onNeedRecount() {
        this.page = 0 ;
        fetchNotifications();
        sumCount();
    }

//    void countUnreadNotification(String topics, String types, String topicExcludes, String typeExcludes, Consumer<Long> callback){
//        grabServerData(Kitchen.arange(this, NotificationUserApi.class).countUnreadNotification(currentAuthorization, topics, types, topicExcludes, typeExcludes),
//                (wsResponse, response) -> {
//                    if(wsResponse.isSuccess()){
//                        callback.accept(wsResponse.getData());
//                    }
//                }, (aBoolean, throwable, response) -> {
//                    throwable.printStackTrace();
//                }, false);
//    }





}
