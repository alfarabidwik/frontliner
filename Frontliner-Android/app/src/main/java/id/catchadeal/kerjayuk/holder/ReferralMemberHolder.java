package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Date;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.activity.task.TaskReferralMemberActivity;
import id.catchadeal.kerjayuk.activity.user.TaskApplicationDetailActivity;
import id.catchadeal.kerjayuk.model.business.ReferralMemberDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.utils.TimeAgo;

public class ReferralMemberHolder extends ACTViewHolder<BaseActivity, ReferralMemberDto, String> {

    @BindView(R.id.user_iv) ImageView userImageView ;
    @BindView(R.id.user_name_tv) TextView usernameTextView ;
    @BindView(R.id.agent_code_tv) TextView agentCodeTextView ;
    @BindView(R.id.mobile_phone_tv) TextView mobilePhoneTextView ;
    @BindView(R.id.registered_at_tv) TextView registeredAtTextView ;
    @BindView(R.id.card_container) View cardContainer ;


    public ReferralMemberHolder(TaskReferralMemberActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_referral_member, viewGroup);
    }

    public ReferralMemberHolder(TaskApplicationDetailActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_referral_member, viewGroup);
    }


    @Override
    public void showData(ReferralMemberDto referralMemberDto) {
        super.showData(referralMemberDto);

        GlideApp.with(getAct()).load(referralMemberDto.getUser().getPhotoUrl()).error(R.drawable.ic_user_avatar).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(userImageView);
        usernameTextView.setText(referralMemberDto.getUser().getFullname());
        agentCodeTextView.setText(referralMemberDto.getUser().getAgentCode());
        mobilePhoneTextView.setText(referralMemberDto.getUser().getMobilePhone());
        registeredAtTextView.setText(getAct().getString(R.string.RegisteredAt, TimeAgo.toDuration((new Date().getTime()-referralMemberDto.getUser().getCreated().getTime()))));
    }

    @Override
    public void find(String findParam) {

    }
}
