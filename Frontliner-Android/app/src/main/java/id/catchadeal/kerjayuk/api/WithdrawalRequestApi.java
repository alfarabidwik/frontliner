package id.catchadeal.kerjayuk.api;

import java.math.BigDecimal;
import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.business.WithdrawalRequestDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WithdrawalRequestApi {
    @POST(Constant.BASE_API+"/withdrawalRequest/request")
    Observable<WSResponse<WithdrawalRequestDto, String>> request(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("amount") BigDecimal amount);


    @GET(Constant.BASE_API+"/withdrawalRequest/checkIfRequestExist")
    Observable<WSResponse<Boolean, String>> checkIfRequestExist(@Header(Constant.HEADER_AUTHORIZATION) String authorization);

}
