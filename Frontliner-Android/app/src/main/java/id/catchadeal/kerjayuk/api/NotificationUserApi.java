package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.misc.NotificationUserDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface NotificationUserApi {

    @GET("api/notificationUsers")
    Observable<WSResponse<List<NotificationUserDto>, String>> notifications(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("topics") String topics,
            @Query("types") String types,
            @Query("topicExcludes") String topicExcludes,
            @Query("typeExcludes") String typeExcludes,
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);

    @GET("api/notificationUser/read")
    Observable<WSResponse> read(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("notificationUserId") Long notificationUserId);


    @GET("api/notificationUser/countUnreadNotification")
    Observable<WSResponse<Long, String>> countUnreadNotification(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("topics") String topics,
            @Query("types") String types,
            @Query("topicExcludes") String topicExcludes,
            @Query("typeExcludes") String typeExcludes);


}
