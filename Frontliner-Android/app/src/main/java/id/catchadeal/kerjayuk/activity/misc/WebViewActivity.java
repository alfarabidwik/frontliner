package id.catchadeal.kerjayuk.activity.misc;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.*;
import android.widget.Toast;

import butterknife.BindView;
import com.alfarabi.alfalibs.tools.AppProgress;
import com.hendraanggrian.bundler.annotations.BindExtra;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class WebViewActivity extends BaseActivity {
    public static final String TAG = WebViewActivity.class.getName();

    @BindExtra String address ;

    @BindView(R.id.webview)
    WebView webView ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_web_view;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void renderData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode( WebSettings.MIXED_CONTENT_ALWAYS_ALLOW );
        }
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                ViewProgress.dismiss();
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                ViewProgress.dismiss();
            }
        });
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewProgress.show(this);
        webView.loadUrl(address);
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

}
