package id.catchadeal.kerjayuk.view;

import android.app.Activity;
import com.kaopiz.kprogresshud.KProgressHUD;

public class ViewProgress {

    static KProgressHUD kProgressHUD;

    public static void show(Activity activity, String... messages){
        kProgressHUD = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        if(messages!=null){
            if(messages.length>0){
                kProgressHUD.setLabel(messages[0]);
            }
            if(messages.length>1){
                kProgressHUD.setLabel(messages[1]);
            }
        }

        kProgressHUD.setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static void dismiss(){
        if(kProgressHUD!=null){
            kProgressHUD.dismiss();
        }
    }

}
