package id.catchadeal.kerjayuk.holder;

import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.apache.commons.lang.StringUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.model.blog.Article;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.Utils;

public class DashboardArticleHolder extends ACTViewHolder<DashboardActivity, Article, String> {

    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.content_tv) HtmlTextView contentTextView ;
    @BindView(R.id.card_view) CardView cardView ;

    @BindView(R.id.shimmer_title_layout) ShimmerFrameLayout shimmerTitleLayout ;
    @BindView(R.id.shimmer_content_layout) ShimmerFrameLayout shimmerContentLayout ;


    public DashboardArticleHolder(DashboardActivity dashboardActivity, ViewGroup viewGroup) {
        super(dashboardActivity, R.layout.holder_dashboard_article, viewGroup);
    }

    @Override
    public void showData(Article object) {
        super.showData(object);
        if(object.getId()!=null){
            shimmerTitleLayout.stopShimmer();
            shimmerTitleLayout.hideShimmer();
            shimmerContentLayout.stopShimmer();
            shimmerContentLayout.hideShimmer();

            GlideApp.with(getAct()).load(object.getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            titleTextView.setText(object.getTitle().getRendered());
            contentTextView.setHtml(Utils.abbreviete(object.getExcerpt().getRendered(), 50));
            cardView.setOnClickListener(v -> {
                getAct().startActivity(WebViewActivity.class, object.getLink());
            });
        }else{
            shimmerTitleLayout.showShimmer(true);
            shimmerContentLayout.showShimmer(true);
        }
    }

    @Override
    public void find(String findParam) {

    }
}
