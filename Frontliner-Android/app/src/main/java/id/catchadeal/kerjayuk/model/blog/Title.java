package id.catchadeal.kerjayuk.model.blog;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Title {
    String rendered ;
    @SerializedName("protected")
    boolean protexted;
}
