package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.holder.AccountConfiguraitionMenuHolder;
import id.catchadeal.kerjayuk.holder.AccountMenuHolder;
import id.catchadeal.kerjayuk.model.misc.AccountConfigurationMenu;
import id.catchadeal.kerjayuk.model.misc.AccountMenu;

public class AccountConfigurationActivity extends BaseActivity {

    public static final String TAG = AccountConfigurationActivity.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;

    private BasicActRecyclerAdapter<AccountConfigurationMenu, AccountConfigurationActivity, AccountConfiguraitionMenuHolder> adapter ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_account_configuration;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new BasicActRecyclerAdapter<>(this, AccountConfiguraitionMenuHolder.class, AccountConfigurationMenu.menus());
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(this));

    }

    @Override
    public void renderData() {

    }
}
