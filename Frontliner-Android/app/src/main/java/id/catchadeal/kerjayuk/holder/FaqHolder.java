package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.activity.user.FaqActivity;
import id.catchadeal.kerjayuk.model.misc.FaqDto;

public class FaqHolder extends ACTViewHolder<FaqActivity, FaqDto, String> {

    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.subtitle_tv) TextView subtitle ;
    @BindView(R.id.left_imageview) ImageView leftImageView ;
    @BindView(R.id.right_imageview) ImageView rightImageView ;
    @BindView(R.id.container_layout) FrameLayout containerLayout ;

    public FaqHolder(FaqActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_faq, viewGroup);
    }

    @Override
    public void showData(FaqDto object) {
        super.showData(object);
        titleTextView.setText(object.getTitle());
        subtitle.setText(object.getSubtitle());

        if(getAdapterPosition()==0 || getAdapterPosition()%2==0){
            leftImageView.setVisibility(View.VISIBLE);
            rightImageView.setVisibility(View.GONE);
            GlideApp.with(getAct()).load(object.getImageLink()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_broken_image).into(leftImageView);
        }else{
            leftImageView.setVisibility(View.GONE);
            rightImageView.setVisibility(View.VISIBLE);
            GlideApp.with(getAct()).load(object.getImageLink()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_broken_image).into(rightImageView);
        }

        containerLayout.setOnClickListener(v -> {
            getAct().startActivity(WebViewActivity.class, object.getLink());
        });



    }

    @Override
    public void find(String findParam) {

    }
}
