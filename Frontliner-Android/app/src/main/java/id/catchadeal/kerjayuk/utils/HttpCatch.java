package id.catchadeal.kerjayuk.utils;

import com.alfarabi.alfalibs.activity.APIBaseActivity;
import com.alfarabi.alfalibs.tools.WLog;
import com.google.gson.Gson;
import okhttp3.Response;

import java.net.ConnectException;

public class HttpCatch {
    public static final String TAG = HttpCatch.class.getName();
    private static final Gson gson = new Gson();

    public static void handle(APIBaseActivity activity, Throwable throwable, Response response){
        if(throwable!=null){
            throwable.printStackTrace();
            WLog.d(TAG, "RESPONSE = "+gson.toJson(response));
            if(throwable instanceof ConnectException){
                activity.showDialog("Server error, please try again later");
                return;
            }
            activity.showDialog(throwable.getMessage());
            return;
        }


    }
}
