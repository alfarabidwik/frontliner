package id.catchadeal.kerjayuk.activity.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.Kitchen;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.HttpCatch;
import id.catchadeal.kerjayuk.utils.Utils;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class ForgetPasswordActivity extends BaseActivity {

    public static final String TAG = ForgetPasswordActivity.class.getName();

    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.email_et) EditText emailEditText ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        emailEditText.setText("alfarabidwik@gmail.com");

        submitButton.setOnClickListener(v -> {
            String email = emailEditText.getText().toString();
            ViewProgress.show(this);
            grabServerData(Kitchen.arange(this, UserApi.class).forgetPassword(email), (wsResponse, response) -> {
                ViewProgress.dismiss();
                if(wsResponse.isSuccess()){
                    Intent intent = getIntent();
                    intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(wsResponse));
                    setCurrentResult(Constant.RESULT_FINISH, intent);
                }else{
                    showAlert(getString(R.string.Failed), wsResponse.getMessage());
                }
            }, (aBoolean, throwable, response) -> {
                ViewProgress.dismiss();
                showAlert(getString(R.string.error), throwable.getMessage());
            });
        });

    }

    @Override
    public void renderData() {

    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}
