package id.catchadeal.kerjayuk.adapter;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.fragment.BannerFragment;
import id.catchadeal.kerjayuk.fragment.WelcomeFragment;
import id.catchadeal.kerjayuk.model.misc.BannerDto;

public class BannerPagerAdapter extends BaseTabPagerAdapter {

    private BaseActivity baseActivity ;
    private List<BannerDto> bannerDtos = new ArrayList<>();

    public static BannerPagerAdapter instance(BaseActivity baseActivity, List<BannerDto> bannerDtos){
        return new BannerPagerAdapter(baseActivity, bannerDtos);
    }


    public BannerPagerAdapter(BaseActivity baseActivity, List<BannerDto> bannerDtos) {
        super(baseActivity.getSupportFragmentManager());
        this.bannerDtos = bannerDtos ;
        this.baseActivity = baseActivity ;
    }

    @Override
    public String[] titles() {
        return new String[bannerDtos.size()];
    }

    @Override
    public SimpleBaseFragment[] fragmentClasses() {
        List<BannerFragment> bannerFragments = new ArrayList<>();
        bannerDtos.forEach(bannerDto -> {
            bannerFragments.add(BannerFragment.instance(bannerDto));
        });
        return  bannerFragments.toArray(new BannerFragment[bannerFragments.size()]);
    }
}
