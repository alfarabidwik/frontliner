package id.catchadeal.kerjayuk;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatDelegate;

import com.alfarabi.alfalibs.AlfaLibsApplication;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.UISimulation;
import com.alfarabi.alfalibs.tools.WLog;
import com.appizona.yehiahd.fastsave.FastSave;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.request.RequestOptions;
import com.github.rrsystems.utilsplus.android.UtilsPlus;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.GsonBuildConfig;

import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.service.firebase.AppFirebaseMessaggingService;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.JsonDateTimeDeserializer;
import id.catchadeal.kerjayuk.utils.JsonDateTimeSerializer;

import net.soroushjavdan.customfontwidgets.FontUtils;

import java.util.Date;

public class KerjayukApplication extends AlfaLibsApplication {

    public static final String TAG = KerjayukApplication.class.getName();
    
    public static Gson gson ;

    @Override
    public void onCreate() {
        super.onCreate();
        WLog.d(TAG, "On Create");
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            throwable.printStackTrace();
        });
        this.DEBUG = true ;
        UtilsPlus.initialize(getApplicationContext());
        FastSave.init(getApplicationContext());
        UISimulation.mode = UISimulation.DEV_REALDATA;
        FontUtils.createFonts(this, "fonts");
        FontUtils.setDefaultFont(this, "fonts", "Montserrat-ExtraLight");

//        Kitchen.setGson(new GsonBuilder().setDateFormat(DateAppConfig.API_DATE_FORMAT)
//                .registerTypeAdapter(Date.class, new JsonDateDeserializer())
//                .registerTypeAdapter(Date.class, new JsonDateSerializer())
//                .create());
//
        Kitchen.preparation(Constant.BASE_DOMAIN, 30, 60, 60, false);
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat(DateAppConfig.API_DATE_FORMAT);
        builder.registerTypeAdapter(Date.class, new JsonDateTimeSerializer());
        builder.registerTypeAdapter(Date.class, new JsonDateTimeDeserializer());
        KerjayukApplication.gson = builder.create();
        Kitchen.setGson(KerjayukApplication.gson);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        for (String notificationTopic : NotificationDto.NOTIFICATION_TOPICS) {
            FirebaseMessaging.getInstance().subscribeToTopic(notificationTopic);
        }
        AppFirebaseMessaggingService.createNotificationChannel(this, "1");

//        GlideApp.with(this)
//                .setDefaultRequestOptions(new RequestOptions()
//                        .set(HttpGlideUrlLoader.TIMEOUT, 30000).diskCacheStrategy(DiskCacheStrategy.ALL));

    }

    @Override
    protected boolean getBuildConfigDebug() {
        return DEBUG;
    }

    @Override
    protected String getBuildConfigVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }

    @Override
    public String emailReportApps() {
        return Constant.REPORT_RECEIVER_EMAIL;
    }

    @Override
    public String subjectLineReportApps() {
        return Constant.REPORT_SUBJECT;
    }

}
