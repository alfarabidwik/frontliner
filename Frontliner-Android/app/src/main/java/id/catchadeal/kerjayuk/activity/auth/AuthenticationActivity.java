package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;

import com.alfarabi.alfalibs.tools.InputTools;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;
import com.alfarabi.alfalibs.tools.WLog;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.util.PhoneNumberUtil;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;

import org.apache.commons.lang.StringUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class AuthenticationActivity extends BaseActivity {

    public static final String TAG = AuthenticationActivity.class.getName();

    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.email_auth_link_tv) HtmlTextView emailAuthLinkTextView ;
    @BindView(R.id.phone_number_et) MaterialEditText phoneNumberEditText ;

    @BindExtra Integer responseCode ;
    @BindExtra String messageTitle ;
    @BindExtra String messageContent ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_authentication;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void renderData() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Spanned agrementSpanned = null ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            agrementSpanned = Html.fromHtml(getString(R.string.Login_using_email_and_password_here), Html.FROM_HTML_MODE_LEGACY);
        } else {
            agrementSpanned = Html.fromHtml(getString(R.string.Login_using_email_and_password_here));
        }
        if(agrementSpanned!=null){
            emailAuthLinkTextView.setText(agrementSpanned);
            CommonView.setTextViewHTMLClickCallback(emailAuthLinkTextView, getString(R.string.Login_using_email_and_password_here), view -> {
                startActivityForResult(EmailAuthenticationActivity.class, Constant.REQUEST_AUTH);
            });
        }
    }

    @OnClick(R.id.submit_button)
    public void onClick(){
        if(InputTools.isComplete(false, phoneNumberEditText)){
            PhoneNumberUtil.cleanupIndMobilePhone(phoneNumberEditText.getText().toString(), validPhoneNumber -> {
                if(validPhoneNumber.isValid()){
                    String phoneNumber = validPhoneNumber.getPhoneNumber();
                    startActivityForResult(OTPActivity.class, Constant.REQUEST_AUTH,  "", "", phoneNumber);
                }else{
                    phoneNumberEditText.setError(getString(R.string.InvalidMobilePhoneCharacter));
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==Constant.REQUEST_AUTH && resultCode==Constant.RESULT_FINISH && data!=null){
            String messageJson = data.getStringExtra(Constant.MESSAGE_JSON);
            WSResponse wsResponse = gson.fromJson(messageJson, WSResponse.class);
            if(wsResponse.getMessage().equalsIgnoreCase(Constant.ERROR_INVALID_PHONE_NUMBER)){
                infoDialog(getString(R.string.InvalidMobilePhoneCharacter), DIALOG_WARNING, () -> true);
            }else{
                infoDialog(wsResponse.getMessage(), DIALOG_WARNING, () -> true);
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StringUtils.isNotEmpty(messageTitle) && StringUtils.isNotEmpty(messageContent)){
            if(responseCode==200){
                showInfo(messageTitle, messageContent);
            }else{
                showAlert(messageTitle, messageContent);
            }
        }
    }
}
