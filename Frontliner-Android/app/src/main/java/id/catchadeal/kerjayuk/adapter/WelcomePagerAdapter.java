package id.catchadeal.kerjayuk.adapter;

import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.fragment.WelcomeFragment;

public class WelcomePagerAdapter extends BaseTabPagerAdapter {

    private BaseActivity baseActivity ;


    public WelcomePagerAdapter(BaseActivity baseActivity) {
        super(baseActivity.getSupportFragmentManager());
        this.baseActivity = baseActivity ;
    }

    @Override
    public String[] titles() {
        return new String[]{"","","","",""};
    }

    @Override
    public SimpleBaseFragment[] fragmentClasses() {
        return new SimpleBaseFragment[]{
                WelcomeFragment.instance(R.string.WelcomeTitle1, R.string.WelcomeIntro1, R.drawable.img_walkthrough_4),
                WelcomeFragment.instance(R.string.WelcomeTitle2, R.string.WelcomeIntro2, R.drawable.img_walkthrough_2),
                WelcomeFragment.instance(R.string.WelcomeTitle3, R.string.WelcomeIntro3, R.drawable.img_walkthrough_1),
                WelcomeFragment.instance(R.string.WelcomeTitle4, R.string.WelcomeIntro4, R.drawable.img_walkthrough_3),
                WelcomeFragment.instance(R.string.WelcomeTitle5, R.string.WelcomeIntro5, R.drawable.img_walkthrough_5),
        };
    }
}
