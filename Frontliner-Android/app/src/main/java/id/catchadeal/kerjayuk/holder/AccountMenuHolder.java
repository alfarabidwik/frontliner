package id.catchadeal.kerjayuk.holder;

import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.user.AccountActivity;
import id.catchadeal.kerjayuk.activity.user.FaqActivity;
import id.catchadeal.kerjayuk.activity.user.IdCardActivity;
import id.catchadeal.kerjayuk.model.misc.AccountMenu;
import id.catchadeal.kerjayuk.util.PhoneNumberUtil;
import id.catchadeal.kerjayuk.utils.Utils;

public class AccountMenuHolder extends ACTViewHolder<AccountActivity, AccountMenu, String> {

    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.card_view) CardView cardView ;

    public AccountMenuHolder(AccountActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_account_menu, viewGroup);
    }

    @Override
    public void showData(AccountMenu object) {
        super.showData(object);
        titleTextView.setText(object.getLabel());
        imageView.setImageResource(object.getIconResource());
        cardView.setOnClickListener(v -> {
            if(object.getLabel()==R.string.mitra_id_card){
                getAct().startActivity(IdCardActivity.class);
            }
            if(object.getLabel()==R.string.FAQ){
                getAct().startActivity(FaqActivity.class);
            }
            if(object.getLabel()==R.string.ContactAdmin){
                PhoneNumberUtil.cleanupIndMobilePhone(getAct().getConfiguration().getContactPersonPhoneNumber(), validPhoneNumber -> {
                    String message = "--"+getAct().getString(R.string.app_name)+"--";
                    message = message+"\nUser : "+getAct().getUser().getFullname();
                    message = message+"\nPhone : "+getAct().getUser().getMobilePhone();
                    message = message+"\nEmail : "+getAct().getUser().getEmail();
                    message = message+"--"+getAct().getString(R.string.app_name)+"--";
                    Utils.openWhatsapp(validPhoneNumber.getPhoneNumber(), message, getAct());
                });
            }
        });
    }

    @Override
    public void find(String findParam) {

    }
}
