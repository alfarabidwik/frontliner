package id.catchadeal.kerjayuk.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Dot {
    int position ;

    public Dot(int position){
        this.position = position ;
    }


    public static List<Dot> welcomeMenus(){
        List<Dot> dots = new ArrayList<>();
        dots.add(new Dot(1));
        dots.add(new Dot(2));
        dots.add(new Dot(3));
        dots.add(new Dot(4));
        dots.add(new Dot(5));
        return dots;
    }

    public static List<Dot> create(int count){
        List<Dot> dots  = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            dots.add(new Dot(i+1));
        }
        return dots ;
    }
}
