package id.catchadeal.kerjayuk.api;

import java.math.BigDecimal;
import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TaskApplicationApi {
    @GET(Constant.BASE_API+"/taskApplications")
    Observable<WSResponse<List<TaskApplicationDto>, String>> taskApplications(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("userId") Long userId,
            @Query("taskId") Long taskId,
            @Query("keyword") String keyword,
            @Query("published") Boolean published,
            @Query("taskCategoryId") Long taskCategoryId,
            @Query("feeStart") BigDecimal feeStart,
            @Query("feeEnd") BigDecimal feeEnd,
            @Query("startDate") String startDate,
            @Query("endDate") String endDate,
            @Query("taskApplicationStatusId") Long taskApplicationStatusId,
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);


    @GET(Constant.BASE_API+"/taskApplication/exists")
    Observable<WSResponse<Boolean, String>> taskApplicationExists(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("taskId") Long taskid);

    @POST(Constant.BASE_API+"/taskApplication/apply")
    Observable<WSResponse<TaskApplicationDto, String>> apply(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("taskId") Long taskId);

    @GET(Constant.BASE_API+"/taskApplication")
    Observable<WSResponse<TaskApplicationDto, String>> findById(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("id") Long id);

    @GET(Constant.BASE_API+"/taskApplication/getMyTaskApplication")
    Observable<WSResponse<TaskApplicationDto, String>> getMyTaskApplication(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("taskId") Long taskId);

    @POST(Constant.BASE_API+"/taskApplication/commit")
    Observable<WSResponse<TaskApplicationDto, String>> commit(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Body TaskApplicationDto taskApplicationDto);





}
