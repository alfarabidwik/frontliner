package id.catchadeal.kerjayuk.model.blog;

import lombok.Data;

@Data
public class Content {
    String rendered ;
}
