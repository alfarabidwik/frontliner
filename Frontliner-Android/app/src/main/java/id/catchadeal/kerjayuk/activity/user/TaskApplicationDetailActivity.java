package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.ActRecyclerAdapter;
import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.task.TaskReferralMemberActivity;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.holder.ReferralMemberHolder;
import id.catchadeal.kerjayuk.holder.TaskApplicationStatusHolder;
import id.catchadeal.kerjayuk.model.business.PVTaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.ReferralMemberDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.view.CardTaskApplication;
import lombok.Getter;

public class TaskApplicationDetailActivity extends BaseActivity {

    public static final String TAG = TaskApplicationDetailActivity.class.getName();

    @BindView(R.id.task_application_code_tv) TextView taskApplicationCodeTextView ;
    @BindView(R.id.last_status_et) MaterialEditText lastStatusEditText ;
    @BindView(R.id.last_status_tv) TextView lastStatusTextView ;
    @BindView(R.id.partner_et) MaterialEditText partnerEditText ;
    @BindView(R.id.fee_et) MaterialEditText feeEditText ;
    @BindView(R.id.task_taken_et) MaterialEditText taskTakenEditText ;

    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;

    @BindView(R.id.header_info_layout) LinearLayout headerInfoLayout ;
    @BindView(R.id.expand_info_layout) ExpandableRelativeLayout expandInfoLayout ;

    @BindView(R.id.header_data_layout) LinearLayout headerDataLayout ;

    @BindView(R.id.expand_data_layout) ExpandableRelativeLayout expandDataLayout ;
    @BindView(R.id.form_layout) LinearLayout formLinearLayout ;
    @BindView(R.id.form_card_view) CardView formCardView ;
    @BindView(R.id.status_rv) AlfaRecyclerView statusRecyclerView ;

    @BindView(R.id.expand_referral_layout) ExpandableRelativeLayout expandReferralLayout ;

    @BindExtra String taskApplicationDtoGson ;
    @BindView(R.id.note_et) EditText noteEditText ;

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.task_application_label_tv) TextView taskApplicationLabelTextView ;

    BasicActRecyclerAdapter<ReferralMemberDto, TaskApplicationDetailActivity, ReferralMemberHolder> referralMemberAdapter ;


    @Getter TaskApplicationDto taskApplicationDto ;
    TaskDto taskDto ;

    BasicActRecyclerAdapter<TaskApplicationStatusDto, TaskApplicationDetailActivity, TaskApplicationStatusHolder> adapter ;


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_application_detail;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskApplicationDto = gson.fromJson(taskApplicationDtoGson, TaskApplicationDto.class);
        taskDto = taskApplicationDto.getTask();
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.page = 0 ;
        expandInfoLayout.collapse();
        headerInfoLayout.setOnClickListener(v -> {
            expandInfoLayout.toggle();
        });

        expandDataLayout.collapse();
        headerDataLayout.setOnClickListener(v -> {
            expandDataLayout.toggle();
            expandReferralLayout.toggle();
        });

        expandReferralLayout.setVisibility(View.GONE);
        expandDataLayout.setVisibility(View.GONE);
        taskApplicationLabelTextView.setText(getString(R.string.TaskApplicationData));

        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            expandReferralLayout.setVisibility(View.GONE);
            expandDataLayout.setVisibility(View.VISIBLE);
            if(taskApplicationDto.getFormLaneApplications()==null || taskApplicationDto.getFormLaneApplications().size()<=0){
                formCardView.setVisibility(View.GONE);
            }else{
                formCardView.setVisibility(View.VISIBLE);
                taskApplicationDto.getFormLaneApplications().forEach(formLaneApplicationDto -> {
                    formLinearLayout.addView(new CardTaskApplication(this, formLaneApplicationDto), new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                });
            }
        }
        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){

            taskApplicationLabelTextView.setText(getString(R.string.UserList));
            expandReferralLayout.setVisibility(View.VISIBLE);
            expandDataLayout.setVisibility(View.GONE);

        }

        adapter = new BasicActRecyclerAdapter<>(this, TaskApplicationStatusHolder.class, CommonView.taskApplicationStatusDtos);
        adapter.initRecyclerView(statusRecyclerView, new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        List<ReferralMemberDto> referralMembers = new ArrayList<>();
        referralMembers.addAll(taskApplicationDto.getReferralMembers().stream().collect(Collectors.toList()));

        referralMemberAdapter = new BasicActRecyclerAdapter<>(this, ReferralMemberHolder.class, referralMembers);
        referralMemberAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));


    }

    @Override
    public void renderData() {

        taskApplicationCodeTextView.setText(taskApplicationDto.getViewOrderReference());
        lastStatusEditText.setText(getString(R.string.SendAtVariable, DateFormatUtils.format(taskApplicationDto.getCreated(), DateAppConfig.VIEW_FULL_DATE_FORMAT)+" WIB"));
        lastStatusTextView.setText(taskApplicationDto.getTaskApplicationStatus().getDescription());

        partnerEditText.setText(taskApplicationDto.getTask().getPartner().getFullName());
        taskTakenEditText.setText(DateFormatUtils.format(taskApplicationDto.getCreated(), DateAppConfig.VIEW_FULL_DATE_FORMAT)+" WIB ");
        GlideApp.with(this).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(partnerImageView);
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            feeEditText.setText(getString(R.string.FeePerAgent, NumberUtil.moneyFormat(taskApplicationDto.getItemFee(), true)));
        }
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            feeEditText.setText(getString(R.string.FeePerTask, NumberUtil.moneyFormat(taskApplicationDto.getItemFee(), true)));
        }

        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());
        if(taskApplicationDto.getPvTaskApplicationStatuses()!=null && taskApplicationDto.getPvTaskApplicationStatuses().size()>0){
            PVTaskApplicationStatusDto pvTaskApplicationStatusDto = taskApplicationDto.getPvTaskApplicationStatuses().iterator().next();
            if(StringUtils.isNotEmpty(pvTaskApplicationStatusDto.getNote())){
                noteEditText.setText(pvTaskApplicationStatusDto.getNote());
            }
        }

    }

    @Override
    @OnClick(R.id.task_detail_button)
    public void onClick(View view) {
        super.onClick(view);
        boolean submitable = false ;
        if(taskApplicationDto.getTaskApplicationStatus()!=null && taskApplicationDto.getTaskApplicationStatus().getId().equals(TaskApplicationStatusDto.TAKEN_ID)){
            submitable = true ;
        }
        startActivity(TaskDetailActivity.class, gson.toJson(taskDto), submitable);
    }

    static List<TaskApplicationSortir> taskApplicationSortirs = new ArrayList<>();

    static {
        taskApplicationSortirs = new ArrayList<>();
        taskApplicationSortirs.add(new TaskApplicationSortir("Terbaru", "jt.updated", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Terlama", "jt.updated", true));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Tertinggi", "jt.fee", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Terendah", "jt.fee", true));
    }




}
