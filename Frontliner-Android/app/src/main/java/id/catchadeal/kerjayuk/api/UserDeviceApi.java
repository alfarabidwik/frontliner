package id.catchadeal.kerjayuk.api;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserDeviceApi {
    @POST(Constant.BASE_API+"/userDevice/refresh")
    Observable<WSResponse> refresh(
            @Header("authorization") String authorization,
            @Query("deviceId") String deviceId,
            @Query("platform") String platform,
            @Query("fcmToken") String fcmToken
    );

}
