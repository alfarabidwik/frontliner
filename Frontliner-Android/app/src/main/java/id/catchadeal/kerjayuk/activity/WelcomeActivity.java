package id.catchadeal.kerjayuk.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.transition.Slide;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.appizona.yehiahd.fastsave.FastSave;
import com.hendraanggrian.bundler.annotations.BindExtra;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.auth.AuthenticationActivity;
import id.catchadeal.kerjayuk.adapter.WelcomePagerAdapter;
import id.catchadeal.kerjayuk.holder.DotHolder;
import id.catchadeal.kerjayuk.model.Dot;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import id.catchadeal.kerjayuk.utils.SliderControl;
import id.catchadeal.kerjayuk.view.AlfaViewPager;
import lombok.Getter;

public class WelcomeActivity extends BaseActivity implements SliderControl {
    
    
    public static final String TAG = WelcomeActivity.class.getName();

    @BindView(R.id.framelayout) FrameLayout frameLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.viewpager) AlfaViewPager viewPager ;
    @BindView(R.id.next_button) Button nextButton ;
    @BindView(R.id.skip_tv) TextView skiTextView ;

    @BindExtra Integer responseCode ;
    @BindExtra String messageTitle ;
    @BindExtra String messageContent ;
    int position = 0;
    WelcomePagerAdapter welcomePagerAdapter ;

    private BasicActRecyclerAdapter<Dot, WelcomeActivity, DotHolder> indicatorAdapter ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        welcomePagerAdapter = new WelcomePagerAdapter(this);
        viewPager.setAdapter(welcomePagerAdapter);
        indicatorAdapter = new BasicActRecyclerAdapter<>(this, DotHolder.class, Dot.welcomeMenus());
        indicatorAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
        nextButton.setOnClickListener(v -> {
            position = viewPager.getCurrentItem();
            if(position<welcomePagerAdapter.fragmentClasses().length-1){
                viewPager.setCurrentItem(++position, true);
                return;
            }
            if(position==welcomePagerAdapter.fragmentClasses().length-1){
                startActivity(AuthenticationActivity.class, responseCode, messageTitle, messageContent);
                FastSave.getInstance().saveBoolean(Constant.FIRST_INSTALLATION, false);
                finish();
                return;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                WelcomeActivity.this.position = position ;
                indicatorAdapter.notifyDataSetChanged();
            }
        });
        skiTextView.setOnClickListener(v -> {
            startActivity(AuthenticationActivity.class, responseCode, messageTitle, messageContent);
            FastSave.getInstance().saveBoolean(Constant.FIRST_INSTALLATION, false);
            finish();
            return;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StringUtils.isNotEmpty(messageTitle) && StringUtils.isNotEmpty(messageContent)){
            if(responseCode==200){
                showInfo(messageTitle, messageContent);
            }else{
                showAlert(messageTitle, messageContent);
            }
            this.responseCode = 200;
            this.messageTitle = "";
            this.messageContent = "";

        }
    }


    @Override
    public void renderData() {

    }

    @Override
    public int getPosition() {
        return position;
    }
}
