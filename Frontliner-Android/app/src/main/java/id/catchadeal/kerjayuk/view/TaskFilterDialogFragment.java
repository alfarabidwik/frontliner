package id.catchadeal.kerjayuk.view;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicFragRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.appizona.yehiahd.fastsave.FastSave;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.time.DateFormatUtils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.holder.FilterTaskApplicationStatusHolder;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.misc.TaskFilter;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import io.apptik.widget.MultiSlider;
import jrizani.jrspinner.JRSpinner;
import lombok.Getter;
import lombok.Setter;

public class TaskFilterDialogFragment extends BottomSheetDialogFragment implements RecyclerCallback {
    
    public static final String TAG = TaskFilterDialogFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.category_spinner) JRSpinner spinner ;
    @BindView(R.id.start_date_et) EditText startDateEditText ;
    @BindView(R.id.end_date_et) EditText endDateEditText ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.min_fee_tv) TextView minFeeTextView ;
    @BindView(R.id.max_fee_tv) TextView maxFeeTextView ;
    @BindView(R.id.multislider) MultiSlider multiSlider ;
    @BindView(R.id.apply_button) Button applyButton ;
    @BindView(R.id.task_application_status_option_cv) CardView taskApplicationStatusCardView ;

    @Getter BasicFragRecyclerAdapter<TaskApplicationStatusDto, TaskFilterDialogFragment, FilterTaskApplicationStatusHolder> adapter ;

    @Getter@Setter List<TaskApplicationStatusDto> taskApplicationStatusDtos = new ArrayList<>();
    @Getter@Setter List<TaskCategoryDto> taskCategoryDtos = new ArrayList<>();

    @Getter@Setter TaskFilter taskFilter ;

    Consumer<TaskFilter> callback = null ;

    @Getter
    private boolean showing ;

    public static TaskFilterDialogFragment instance(List<TaskApplicationStatusDto> taskApplicationStatuses, List<TaskCategoryDto> taskCategoryDtos, TaskFilter taskFilter) {
        TaskFilterDialogFragment taskFilterDialogFragment  = new TaskFilterDialogFragment();
        taskFilterDialogFragment.setTaskFilter(taskFilter);
        taskFilterDialogFragment.setTaskApplicationStatusDtos(taskApplicationStatuses);
        taskFilterDialogFragment.setTaskCategoryDtos(taskCategoryDtos);
        return taskFilterDialogFragment ;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_filter_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WLog.d(TAG, "ON View Created");
        ButterKnife.bind(this, view);
        getView().setBackgroundColor(getActivity().getColor(R.color.page_background));
        multiSlider.setMin(0, true, true);
        multiSlider.setMax(1000, true, true);
        multiSlider.getThumb(0).setMin(0);
        multiSlider.getThumb(0).setMax(1000);
        multiSlider.getThumb(1).setMin(0);
        multiSlider.getThumb(1).setMax(1000);


        minFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMinFee(), true));
        maxFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMaxFee(), true));
        multiSlider.getThumb(0).setValue(taskFilter.getMinFee().intValue()/10000);
        WLog.d(TAG, "VALUE 1 = "+String.valueOf(multiSlider.getThumb(0).getValue()));
        minFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMinFee(), true));
        multiSlider.getThumb(1).setValue(taskFilter.getMaxFee().intValue()/10000);
        maxFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMaxFee(), true));

        toolbar.inflateMenu(R.menu.task_application_filter_menu);
        toolbar.setTitle(R.string.Filter);
        toolbar.setOnMenuItemClickListener(menuItem -> {
            if(menuItem.getItemId()==R.id.close){
                dismiss();
            }
            if(menuItem.getItemId()==R.id.reset){
                spinner.select(0);
                spinner.setText("");
                if(taskApplicationStatusDtos!=null && taskApplicationStatusDtos.size()>0){
                    this.taskFilter.setTaskApplicationStatus(taskApplicationStatusDtos.get(0));
                }
                this.taskFilter.setTaskCategory(null);
                this.taskFilter.setMinFee(BigDecimal.ZERO);
                this.taskFilter.setMaxFee(new BigDecimal(10000000));
                this.adapter.notifyDataSetChanged();
                multiSlider.getThumb(0).setValue(taskFilter.getMinFee().intValue()/10000);
                minFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMinFee(), true));
                multiSlider.getThumb(1).setValue(taskFilter.getMaxFee().intValue()/10000);
                maxFeeTextView.setText(NumberUtil.moneyFormat(taskFilter.getMaxFee(), true));
                taskFilter.setStartDate(null);
                taskFilter.setEndDate(null);
                startDateEditText.setText("");
                endDateEditText.setText("");
            }
            return true ;
        });

        startDateEditText.setOnClickListener(v -> {
            Calendar previousCalendar = Calendar.getInstance();
            previousCalendar.set(0,0,0,0,0,0);
            if(taskFilter.getStartDate()!=null){
                previousCalendar.setTime(taskFilter.getStartDate());
            }else{
                previousCalendar.setTime(new Date());
            }

            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 1);
                taskFilter.setStartDate(calendar.getTime());
                startDateEditText.setText(DateFormatUtils.format(calendar.getTime(), DateAppConfig.VIEW_DATE_FORMAT));
                WLog.d(TAG, DateFormatUtils.format(taskFilter.getStartDate(), "dd-MM-yyyy HH:mm:ss"));
            }, previousCalendar.get(Calendar.YEAR), previousCalendar.get(Calendar.MONTH), previousCalendar.get(Calendar.DAY_OF_MONTH));
            if(taskFilter.getEndDate()!=null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(taskFilter.getEndDate());
                datePickerDialog.setMaxDate(calendar);
            }
            datePickerDialog.show(getActivity().getFragmentManager(), DatePickerDialog.class.getName()+"Start");
        });
        endDateEditText.setOnClickListener(v -> {
            Calendar previousCalendar = Calendar.getInstance();
            previousCalendar.set(0,0,0,0,0,0);
            if(taskFilter.getEndDate()!=null){
                previousCalendar.setTime(taskFilter.getEndDate());
            }else{
                previousCalendar.setTime(new Date());
            }

            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                calendar.set(Calendar.SECOND, 59);
                calendar.set(Calendar.MILLISECOND, 999);
                taskFilter.setEndDate(calendar.getTime());
                endDateEditText.setText(DateFormatUtils.format(calendar.getTime(), DateAppConfig.VIEW_DATE_FORMAT));
                WLog.d(TAG, DateFormatUtils.format(taskFilter.getEndDate(), "dd-MM-yyyy HH:mm:ss"));
            }, previousCalendar.get(Calendar.YEAR), previousCalendar.get(Calendar.MONTH), previousCalendar.get(Calendar.DAY_OF_MONTH));
            if(taskFilter.getStartDate()!=null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(taskFilter.getStartDate());
                datePickerDialog.setMinDate(calendar);
            }
            datePickerDialog.show(getActivity().getFragmentManager(), DatePickerDialog.class.getName()+"End");
        });

        adapter = new BasicFragRecyclerAdapter<>(this, FilterTaskApplicationStatusHolder.class, taskApplicationStatusDtos);
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));

        if(taskApplicationStatusDtos==null || taskApplicationStatusDtos.size()==0){
            taskApplicationStatusCardView.setVisibility(View.GONE);
        }else{
            taskApplicationStatusCardView.setVisibility(View.VISIBLE);
        }

        spinner.setHint(getString(R.string.SelectCategory));
        List<String> labels = new ArrayList<>();
        taskCategoryDtos.forEach(option -> {
            labels.add(option.getName());
        });
        spinner.setItems(labels.toArray(new String[labels.size()]));
        spinner.setOnItemClickListener(position -> {
            this.taskFilter.setTaskCategory(this.taskCategoryDtos.get(position));
        });
        multiSlider.setOnTrackingChangeListener(new MultiSlider.OnTrackingChangeListener() {
            @Override
            public void onStartTrackingTouch(MultiSlider multiSlider, MultiSlider.Thumb thumb, int value) {

            }

            @Override
            public void onStopTrackingTouch(MultiSlider multiSlider, MultiSlider.Thumb thumb, int value) {
                if(thumb.getTag().equalsIgnoreCase("thumb 0")){
                    taskFilter.setMinFee(new BigDecimal(value*10000));
                    minFeeTextView.setText(NumberUtil.moneyFormat(new BigDecimal(value*10000), true));
                }
                if(thumb.getTag().equalsIgnoreCase("thumb 1")){
                    taskFilter.setMaxFee(new BigDecimal(value*10000));
                    maxFeeTextView.setText(NumberUtil.moneyFormat(new BigDecimal(value*10000), true));
                }
            }
        });
        applyButton.setOnClickListener(v -> {
            if(callback!=null){
                callback.accept(taskFilter);
                dismiss();
            }
        });

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog=(BottomSheetDialog)super.onCreateDialog(savedInstanceState);
        bottomSheetDialog.setOnShowListener(dialog -> {
            this.showing = true;
            FrameLayout bottomSheet =  ((BottomSheetDialog)dialog).findViewById(android.support.design.R.id.design_bottom_sheet);
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
            bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        bottomSheetDialog.setOnDismissListener(dialog -> {
            this.showing = false;
        });
        return bottomSheetDialog;
    }

    public void show(FragmentManager fragmentManager, String tag, Consumer<TaskFilter> callback) {
        this.showing = !this.showing;
        this.callback = callback ;
        super.show(fragmentManager, tag);
    }

    @Override
    public Object getObject() {
        return null;
    }
}
