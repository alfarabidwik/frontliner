package id.catchadeal.kerjayuk.activity.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.alfarabi.alfalibs.tools.WLog;
import com.ragnarok.rxcamera.RxCamera;
import com.ragnarok.rxcamera.RxCameraData;
import com.ragnarok.rxcamera.config.CameraUtil;
import com.ragnarok.rxcamera.config.RxCameraConfig;
import com.ragnarok.rxcamera.request.Func;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.utils.Constant;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class CameraActivity extends BaseActivity {

    public static final String TAG = CameraActivity.class.getName();

    @BindView(R.id.textureview) TextureView textureView ;
    @BindView(R.id.flip_iv) ImageView flipImageView;
    @BindView(R.id.flash_iv) ImageView flashImageView;

    private RxCamera rxCamera;
    private boolean flash ;

    RxCameraConfig config = new RxCameraConfig.Builder()
            .useBackCamera()
            .setAutoFocus(true)
            .setPreferPreviewFrameRate(15, 30)
            .setPreferPreviewSize(new Point(640, 480), false)
            .setHandleSurfaceEvent(true)
            .build();




    @Override
    public int contentXmlLayout() {
        return R.layout.activity_camera;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startCamera();
        textureView.setOnTouchListener((v, event) -> {
            if (!checkCamera()) {
                return false;
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                final float x = event.getX();
                final float y = event.getY();
                final Rect rect = CameraUtil.transferCameraAreaFromOuterSize(new Point((int)x, (int)y), new Point(textureView.getWidth(), textureView.getHeight()), 100);
                List<Camera.Area> areaList = Collections.singletonList(new Camera.Area(rect, 1000));
                rx.Observable.zip(rxCamera.action().areaFocusAction(areaList), rxCamera.action().areaMeterAction(areaList),(rxCamera1, rxCamera2) -> {
                    return rxCamera1;
                }).subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        showLog("area focus and metering failed: " + e.getMessage());
                    }

                    @Override
                    public void onNext(Object o) {
                        showLog(String.format("area focus and metering success, x: %s, y: %s, area: %s", x, y, rect.toShortString()));
                    }
                });
            }
            return false;
        });
    }

    @Override
    public void renderData() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (rxCamera != null) {
            rxCamera.closeCamera();
        }
    }

    public void startCamera(){
        RxCamera.open(this, config).flatMap(rxCamera -> {
            this.rxCamera = rxCamera ;
            return rxCamera.bindTexture(textureView);
        }).flatMap(rxCamera -> rxCamera.startPreview()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<RxCamera>() {
            @Override
            public void onCompleted() {

            }
            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(final RxCamera rxCamera) {
                CameraActivity.this.rxCamera = rxCamera ;
            }
        });
    }


    @OnClick({R.id.take_picture_button, R.id.flash_iv, R.id.flip_iv})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(view.getId()==R.id.take_picture_button){
            requestTakePicture();
        }
        if(view.getId()==R.id.flash_iv){
            switchFlash();
        }
        if(view.getId()==R.id.flip_iv){
            switchCamera();
        }
    }

    private void requestTakePicture() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.request().takePictureRequest(true, new Func() {
            @Override
            public void call() {
                showLog("Captured!");
            }
        }, 480, 640, ImageFormat.JPEG, true).subscribe(rxCameraData -> {
            String path = Environment.getExternalStorageDirectory() + "/"+System.nanoTime()+".jpg";
            File file = new File(path);
            Bitmap bitmap = BitmapFactory.decodeByteArray(rxCameraData.cameraData, 0, rxCameraData.cameraData.length);
            bitmap = flipImage(bitmap);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rxCameraData.rotateMatrix, false);
            try {
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                CropImage.activity(Uri.fromFile(file)).setGuidelines(CropImageView.Guidelines.ON).start(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Intent intent = getIntent();
                intent.putExtra(Constant.FILE_PATH, resultUri.getPath());
                setCurrentResult(Constant.RESULT_FINISH, intent);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    public void requestSuccessiveData() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.request().successiveDataRequest().subscribe(new Action1<RxCameraData>() {
            @Override
            public void call(RxCameraData rxCameraData) {
//                showLog("successiveData, cameraData.length: " + rxCameraData.cameraData.length);
            }
        });
    }

    public void requestOneShot() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.request().oneShotRequest().subscribe(new Action1<RxCameraData>() {
            @Override
            public void call(RxCameraData rxCameraData) {
//                showLog("one shot request, cameraData.length: " + rxCameraData.cameraData.length);
            }
        });
    }

    private void requestPeriodicData() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.request().periodicDataRequest(1000).subscribe(new Action1<RxCameraData>() {
            @Override
            public void call(RxCameraData rxCameraData) {
//                showLog("periodic request, cameraData.length: " + rxCameraData.cameraData.length);
            }
        });
    }

    public Bitmap flipImage(Bitmap img) {
        Matrix matrix = new Matrix();
//        matrix.postScale(-1, 1);
        matrix.postScale(1, -1);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    public void actionZoom() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.action().zoom(10).subscribe(new Subscriber<RxCamera>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                showLog("zoom error: " + e.getMessage());
            }

            @Override
            public void onNext(RxCamera rxCamera) {
                showLog("zoom success: " + rxCamera);
            }
        });
    }

    public void actionSmoothZoom() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.action().smoothZoom(10).subscribe(new Subscriber<RxCamera>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                showLog("zoom error: " + e.getMessage());
            }

            @Override
            public void onNext(RxCamera rxCamera) {
                showLog("zoom success: " + rxCamera);
            }
        });
    }

    private void switchFlash() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.action().flashAction(!flash).subscribe(new Subscriber<RxCamera>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                showLog("open flash error: " + e.getMessage());
            }

            @Override
            public void onNext(RxCamera rxCamera) {
                showLog("open flash");
                CameraActivity.this.flash = !CameraActivity.this.flash;
                if(CameraActivity.this.flash){
                    flashImageView.setImageResource(R.drawable.ic_auto_flash_colored);
                }else{
                    flashImageView.setImageResource(R.drawable.ic_auto_flash);
                }
            }
        });
    }

    public void faceDetection() {
        rxCamera.request().faceDetectionRequest().subscribe(new Action1<RxCameraData>() {
            @Override
            public void call(RxCameraData rxCameraData) {
                showLog("on face detection: " + rxCameraData.faceList);
            }
        });
    }

    private void switchCamera() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.switchCamera().subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                showLog("switch camera result: " + aBoolean);
                if(aBoolean){
                    flipImageView.setImageResource(R.drawable.ic_camera);
                }else{
                    flipImageView.setImageResource(R.drawable.ic_camera_colored);
                }

            }
        });
    }

    public boolean checkCamera() {
        if (rxCamera == null || !rxCamera.isOpenCamera()) {
            return false;
        }
        return true;
    }



    public void showLog(String s) {
        WLog.d(TAG, s);
    }
}
