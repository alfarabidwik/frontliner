package id.catchadeal.kerjayuk.model.blog;

import lombok.Data;

@Data
public class Excerpt {
    String rendered ;
}
