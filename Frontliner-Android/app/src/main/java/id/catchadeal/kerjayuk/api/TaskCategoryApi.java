package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TaskCategoryApi {

    @GET(Constant.BASE_API+"/taskCategories")
    Observable<WSResponse<List<TaskCategoryDto>, String>> taskCategories(
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);

}
