package id.catchadeal.kerjayuk.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.WLog;
import com.appizona.yehiahd.fastsave.FastSave;
import com.github.rrsystems.utilsplus.android.UtilsPlus;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.auth.AuthenticationActivity;
import id.catchadeal.kerjayuk.activity.auth.ResetPasswordActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.api.ResetPasswordApi;
import id.catchadeal.kerjayuk.api.TaskApplicationStatusApi;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.api.UserDeviceApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class SplashActivity extends BaseActivity {

    public static final String TAG = SplashActivity.class.getName();
    private Integer responseCode = 200 ;
    private String messageTitle = "" ;
    private String messageContent = "" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().postDelayed(() -> {
            fetchTaskApplicationStatuses();
            handleIntent();
            fetchProvinces();
            fetchBanks();
            fetchConfiguration();
        }, 1000);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent();
    }


    @Override
    public void renderData() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**
         * Do not remark below code to prevent default behaviour of result finish after reset password done
        * */
//        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constant.REQUEST_RESET_PASSWORD && resultCode==Constant.RESULT_FINISH && data!=null){
            String messageJson = data.getStringExtra(Constant.MESSAGE_JSON);
            WSResponse wsResponse = gson.fromJson(messageJson, WSResponse.class);
            if(wsResponse.isSuccess()){
                this.responseCode = wsResponse.getCode();
                this.messageTitle = getString(R.string.Success);
                this.messageContent = wsResponse.getMessage();
            }else{
                this.responseCode = wsResponse.getCode();
                this.messageTitle = getString(R.string.Failed);
                this.messageContent = wsResponse.getMessage();
            }
        }

        requestPermission();
    }

    public void handleIntent(){
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String token = appLinkData.getLastPathSegment();
            Uri appData = Uri.parse("content://com.recipe_app/recipe/").buildUpon().appendPath(token).build();
            if(StringUtils.isNotEmpty(token)){
                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, ResetPasswordApi.class).validateResetPassword(token), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    if(wsResponse.isSuccess()){
                        if(StringUtils.isNotEmpty(currentAuthorization)){
                            this.responseCode = wsResponse.getCode();
                            this.messageTitle = getString(R.string.invalid_link);
                            this.messageContent = getString(R.string.link_cant_be_used);
                            requestPermission();
                        }else{
                            startActivityForResult(ResetPasswordActivity.class, Constant.REQUEST_RESET_PASSWORD, token);
                        }
                    }else{
                        this.responseCode = wsResponse.getCode();
                        this.messageTitle = wsResponse.getMessage();
                        this.messageContent = wsResponse.getMessageError();
                        requestPermission();
                    }
                }, (aBoolean, throwable, response) -> {
                    ViewProgress.dismiss();
                    this.responseCode = response.code();
                    this.messageTitle = getString(R.string.error);
                    this.messageContent = throwable.getMessage();
                    requestPermission();
                });
                return;
            }
        }
        requestPermission();
    }


    public void requestPermission(){
        new RxPermissions(this).request(Manifest.permission.READ_PHONE_STATE).subscribe(aBoolean -> {
            if(aBoolean){
                initDeviceInfo();
            }else{
                showDialog("Warning", getString(R.string.to_use_this_apps_you_must_accept_permission), (dialog, which) -> {
                    requestPermission();
                }, (dialog, which) -> {
                    finish();
                });
            }
        }, throwable -> {
            throwable.printStackTrace();
            finish();
        });
    }

    public void initDeviceInfo(){
        deviceId = UtilsPlus.getInstance().getDeviceID();
        if(StringUtils.isEmpty(deviceId)){
            requestPermission();
            return;
        }
        WLog.d(TAG, "DEVICE_ID ::: "+deviceId);
        FastSave.getInstance().saveString(Constant.DEVICE_ID, deviceId);
        ViewProgress.show(this, getString(R.string.Initializing));
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    try {
                        Thread.sleep(1000);
                        ViewProgress.dismiss();
                        if (!task.isSuccessful()) {
                            initDeviceInfo();
                            return;
                        }
                        String newFirebaseToken = task.getResult().getToken();
                        WLog.d(TAG, "FIREBASE_TOKEN ::: "+newFirebaseToken);

                        if(StringUtils.isNotEmpty(currentAuthorization) && StringUtils.isNotEmpty(newFirebaseToken) && !currentFirebaseToken.equals(newFirebaseToken)){
                            grabServerData(Kitchen.arange(this, UserDeviceApi.class).refresh(currentAuthorization, deviceId, Constant.ANDROID, newFirebaseToken), (wsResponse, response) -> {

                            }, (aBoolean, throwable, response) -> {

                            });
                        }
                        FastSave.getInstance().saveString(Constant.FIREBASE_TOKEN, newFirebaseToken);
                        protectUserFromReauth();
                    } catch (InterruptedException e) {
                        ViewProgress.dismiss();
                        e.printStackTrace();
                        initDeviceInfo();
                    }
                });

    }


    public void protectUserFromReauth(){
        if(StringUtils.isNotEmpty(currentAuthorization)){
            setCurrentResult(Constant.RESULT_FINISH);
            finish();
            startActivity(DashboardActivity.class);
        }else{
            finish();
            boolean firstInstall = FastSave.getInstance().getBoolean(Constant.FIRST_INSTALLATION, true);
//            boolean firstInstall = true;
            if(firstInstall){
                startActivity(WelcomeActivity.class, responseCode, messageTitle, messageContent);
            }else{
                startActivity(AuthenticationActivity.class, responseCode, messageTitle, messageContent);
            }
        }
    }


    public void fetchTaskApplicationStatuses(){
        grabServerData(Kitchen.arange(this, TaskApplicationStatusApi.class).taskApplicationStatuses(true, "id"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                List<TaskApplicationStatusDto> taskApplicationStatusDtos = new ArrayList<>();
                TaskApplicationStatusDto taskApplicationStatusDto  = new TaskApplicationStatusDto();
                taskApplicationStatusDto.setId(999l);
                taskApplicationStatusDto.setName(getString(R.string.All));
                taskApplicationStatusDto.setDescription(getString(R.string.All));
                taskApplicationStatusDtos.add(taskApplicationStatusDto);
                taskApplicationStatusDtos.addAll(wsResponse.getData());
                FastSave.getInstance().saveString(Constant.TASK_APPLICATION_STATUSES, gson.toJson(taskApplicationStatusDtos));
            }
        }, (aBoolean, throwable, response) -> {

        });
    }


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}
