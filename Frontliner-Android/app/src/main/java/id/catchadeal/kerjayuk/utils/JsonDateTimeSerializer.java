package id.catchadeal.kerjayuk.utils;

import com.google.gson.*;
import id.catchadeal.kerjayuk.config.DateAppConfig;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonDateTimeSerializer implements JsonSerializer<Date> {


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);


    @Override
    public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        try {
            if(date==null){
                return null;
            }
            String formattedDate = dateFormat.format(date);
            return new JsonPrimitive(formattedDate);
//            jsonGenerator.writeString(formattedDate);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null ;
    }
}
