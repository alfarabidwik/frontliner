package id.catchadeal.kerjayuk.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicFragRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;

import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.holder.TaskApplicationSortirHolder;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import lombok.Getter;
import lombok.Setter;

public class MediaDialogFragment extends BottomSheetDialogFragment implements RecyclerCallback {

    public static final String TAG = MediaDialogFragment.class.getName();

    @BindView(R.id.camera_layout) LinearLayout cameraLayout ;
    @BindView(R.id.gallery_layout) LinearLayout galleryLayout ;
    Consumer<View> viewConsumer ;

    public static MediaDialogFragment instance() {
        return new MediaDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_media_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getView().setBackgroundColor(getActivity().getColor(R.color.transparent));
        cameraLayout.setOnClickListener(v -> {
            dismiss();
            viewConsumer.accept(v);
        });
        galleryLayout.setOnClickListener(v -> {
            dismiss();
            viewConsumer.accept(v);
        });
    }

    public int show(FragmentTransaction transaction, String tag, Consumer<View> viewConsumer) {
        this.viewConsumer = viewConsumer ;

        return super.show(transaction, tag);
    }

    @Override
    public Object getObject() {
        return null;
    }
}
