package id.catchadeal.kerjayuk.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.fragments.api.APIBaseFragment;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.model.misc.BannerDto;
import lombok.Getter;
import lombok.Setter;

public class BannerFragment extends BaseFragment {
    
    public static final String TAG = BannerFragment.class.getName();

    @BindView(R.id.imageview) ImageView imageView ;
    @Getter @Setter BannerDto bannerDto ;


    public static BannerFragment instance(BannerDto bannerDto){
        BannerFragment bannerFragment = new BannerFragment();
        bannerFragment.setBannerDto(bannerDto);
        return bannerFragment ;
    }


    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_banner;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GlideApp.with(getActivity()).load(bannerDto.getImageLink()).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        imageView.setOnClickListener(v -> {
            if(StringUtils.isNotEmpty(bannerDto.getPagelink())){
                getAct(DashboardActivity.class).startActivity(WebViewActivity.class, bannerDto.getPagelink());
            }
        });
    }
}
