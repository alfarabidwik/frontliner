package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.InputTools;
import com.alfarabi.alfalibs.tools.WLog;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.util.PhoneNumberUtil;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;

import org.apache.commons.lang.StringUtils;

public class SetPhoneNumberActivity extends BaseActivity {

    public static final String TAG = SetPhoneNumberActivity.class.getName();

    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.phone_number_et) MaterialEditText mobilePhoneEditText ;
    @BindExtra String userDtoJson ;

    private UserDto userDto ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_set_phone_number;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userDto = gson.fromJson(userDtoJson, UserDto.class);
    }

    @Override
    public void renderData() {

    }

    @OnClick(R.id.submit_button)
    public void onClick(){
        if(InputTools.isComplete(false, mobilePhoneEditText)){
            PhoneNumberUtil.cleanupIndMobilePhone(mobilePhoneEditText.getText().toString(), validPhoneNumber -> {
                WLog.d(TAG, "VALID PHONE NUMBER "+gson.toJson(validPhoneNumber));
                if(validPhoneNumber.isValid()){
                    String phoneNumber = validPhoneNumber.getPhoneNumber();
                    ViewProgress.show(this);
                    grabServerData( Kitchen.arange(SetPhoneNumberActivity.this, UserApi.class).validateMobilePhoneWithEmailMatcher(phoneNumber, userDto.getEmail()), (wsResponse, response) -> {
                        ViewProgress.dismiss();
                        if(wsResponse.isSuccess()){
                            startActivityForResult(OTPActivity.class, Constant.REQUEST_AUTH, userDto.getAuthorization(), userDto.getEmail(), phoneNumber);
                        }else{
                            confirmDialog(getString(R.string.Failed), wsResponse.getMessage(), DIALOG_WARNING, aBoolean -> {
                                return aBoolean ;
                            });
                        }
                    }, (aBoolean, throwable, response) -> {
                        ViewProgress.dismiss();
                        throwable.printStackTrace();
                        showAlert(getString(R.string.Failed), throwable.getMessage());
                    });
                }else{
                    mobilePhoneEditText.setError(getString(R.string.InvalidMobilePhoneCharacter));
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constant.REQUEST_AUTH && resultCode==Constant.RESULT_FINISH && data!=null){
            String messageJson = data.getStringExtra(Constant.MESSAGE_JSON);
            WSResponse wsResponse = gson.fromJson(messageJson, WSResponse.class);
            if(wsResponse.getMessage().equalsIgnoreCase(Constant.ERROR_INVALID_PHONE_NUMBER)){
                showAlert(getString(R.string.Failed), getString(R.string.InvalidMobilePhoneCharacter));
            }else{
                showAlert(wsResponse);
            }
        }

    }
}
