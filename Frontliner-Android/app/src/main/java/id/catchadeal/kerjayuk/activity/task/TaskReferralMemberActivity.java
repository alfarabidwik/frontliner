package id.catchadeal.kerjayuk.activity.task;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.ActRecyclerAdapter;
import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.annotations.BindExtra;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.adapter.TaskFormAdapter;
import id.catchadeal.kerjayuk.api.TaskApplicationApi;
import id.catchadeal.kerjayuk.fragment.TaskFormFragment;
import id.catchadeal.kerjayuk.holder.ReferralMemberHolder;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.business.FormLaneApplicationDto;
import id.catchadeal.kerjayuk.model.business.ReferralMemberDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.formbuilder.FormLane;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class TaskReferralMemberActivity extends BaseActivity {


    public static final String TAG = TaskReferralMemberActivity.class.getName();

    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;


    BasicActRecyclerAdapter<ReferralMemberDto, TaskReferralMemberActivity,ReferralMemberHolder> adapter ;

    @BindView(R.id.submit_button) Button submitButton ;

    @BindExtra String taskDtoGson ;
    TaskDto taskDto ;

    @BindExtra String taskApplicationDtoGson ;
    TaskApplicationDto taskApplicationDto  ;



    @Override
    public String getTAG() {
        return TAG;
    }


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_referral_member;
    }

    @Override
    public void renderData() {

        GlideApp.with(this).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(partnerImageView);
        feeTextView.setText(NumberUtil.moneyFormat(taskDto.getFee(), true));
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            taskFeeTypeTextView.setText(R.string.PerTask);
        }
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            taskFeeTypeTextView.setText(R.string.PerAgent);
        }

        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());

        if(taskApplicationDto.getReferralMembers()==null || taskApplicationDto.getReferralMembers().size()<=0){
            submitButton.setText(R.string.RegisteredUserNotAvailable);
        }else{
            submitButton.setText(R.string.Send_The_Work);
        }
    }

    @OnClick(R.id.submit_button)
    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(submitButton.getText().toString().equalsIgnoreCase(getString(R.string.RegisteredUserNotAvailable))){
            return;
        }
        confirmDialog(getString(R.string.Confirmation), getString(R.string.Are_you_sure_to_submit_this_task), CONFIRM_GENERAL, aBoolean -> {
            if(aBoolean){
                taskApplicationDto.setTask(taskDto);

                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, TaskApplicationApi.class).commit(currentAuthorization, taskApplicationDto), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    if(wsResponse.isSuccess()){
                        infoDialog(wsResponse.getMessage(), DIALOG_SUCCESS,() -> {
                            setCurrentResult(Constant.RESULT_FINISH);
                            return true ;
                        });
                    }else{
                        showAlert(wsResponse);
                    }
                }, (aBoolean1, throwable, response) -> {
                    ViewProgress.dismiss();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                });
            }
            return true ;
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        taskDto = gson.fromJson(taskDtoGson, TaskDto.class);
        taskApplicationDto = gson.fromJson(taskApplicationDtoGson, TaskApplicationDto.class);

        List<ReferralMemberDto> referralMembers = new ArrayList<>();
        referralMembers.addAll(taskApplicationDto.getReferralMembers().stream().collect(Collectors.toList()));

        adapter = new BasicActRecyclerAdapter<>(this, ReferralMemberHolder.class, referralMembers);
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        if(referralMembers.size()==0){
            TextView textView = recyclerView.getEmptyView().findViewById(R.id.message_tv);
            textView.setText(R.string.ThereIsNoAvailableRegisteredUser);
        }

    }
}
