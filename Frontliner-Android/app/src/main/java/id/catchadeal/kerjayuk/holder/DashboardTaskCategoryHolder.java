package id.catchadeal.kerjayuk.holder;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;

public class DashboardTaskCategoryHolder extends ACTViewHolder<DashboardActivity, TaskCategoryDto, String> {

    @BindView(R.id.card_container) View cardContainerView ;
    @BindView(R.id.card_view) CardView cardView ;
    @BindView(R.id.icon_iv) ImageView iconImageView ;
    @BindView(R.id.name_tv) TextView nameTextView ;
    @BindView(R.id.count_tv) TextView countTextView;

    public DashboardTaskCategoryHolder(DashboardActivity dashboardActivity, ViewGroup viewGroup) {
        super(dashboardActivity, R.layout.holder_dashboard_category, viewGroup);
    }

    @Override
    public void showData(TaskCategoryDto object) {
        super.showData(object);
        GradientDrawable gd = new GradientDrawable();

        // Set the color array to draw gradient
        gd.setColors(new int[]{
                Color.parseColor(object.getHexaColor()),
                Color.parseColor(object.getHexaColor()),
                ColorUtils.blendARGB(Color.parseColor(object.getHexaColor()), Color.parseColor("#FFFFFF"), 0.15f),
                ColorUtils.blendARGB(Color.parseColor(object.getHexaColor()), Color.parseColor("#FFFFFF"), 0.35f)
        });

        // Set the GradientDrawable gradient type linear gradient
        gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        // Set GradientDrawable shape is a rectangle
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        gd.setCornerRadius(16);
        // Set 3 pixels width solid blue color border
//        gd.setStroke(3, Color.BLUE);

//        // Set GradientDrawable width and in pixels
//        gd.setSize(450, 150);
        cardView.setBackground(gd);
//        cardView.setCardBackgroundColor(Color.parseColor(object.getHexaColor()));
        countTextView.setText(String.valueOf(object.getTotalAvailableTask()));
        nameTextView.setText(object.getName());
        GlideApp.with(getAct()).load(object.getDefaultIconUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(iconImageView);

        cardContainerView.setOnClickListener(v -> {
            getAct().startActivity(TaskListActivity.class, getAct().getGson().toJson(object));
        });

    }

    @Override
    public void find(String findParam) {

    }
}
