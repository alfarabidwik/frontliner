package id.catchadeal.kerjayuk.holder;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.view.TaskFilterDialogFragment;

public class FilterTaskApplicationStatusHolder extends SimpleViewHolder<TaskFilterDialogFragment, TaskApplicationStatusDto, String> {

    @BindView(R.id.label_tv) TextView labelTextView ;
    @BindView(R.id.radio_button) RadioButton radioButton ;
    @BindView(R.id.linear_layout) LinearLayout linearLayout ;

    public FilterTaskApplicationStatusHolder(TaskFilterDialogFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.holder_filter_task_application_status, viewGroup);
    }

    @Override
    public void showData(TaskApplicationStatusDto object) {
        super.showData(object);
        labelTextView.setText(object.getDescription());
        linearLayout.setOnClickListener(v -> {
            select(object);
        });
        radioButton.setOnClickListener(v -> {
            select(object);
        });
        if(getFragment().getTaskFilter()!=null && getFragment().getTaskFilter().getTaskApplicationStatus()!=null && getFragment().getTaskFilter().getTaskApplicationStatus().getId()!=null){
            if(getFragment().getTaskFilter().getTaskApplicationStatus().getId().equals(object.getId())){
                radioButton.setChecked(true);
            }else{
                radioButton.setChecked(false);
            }
        }else{
            radioButton.setChecked(false);
        }
    }

    void select(TaskApplicationStatusDto object){
        getFragment().getTaskFilter().setTaskApplicationStatus(object);
        getFragment().getAdapter().notifyDataSetChanged();
    }


    @Override
    public void find(String findParam) {

    }
}
