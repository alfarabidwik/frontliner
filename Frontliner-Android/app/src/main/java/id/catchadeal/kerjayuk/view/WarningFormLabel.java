package id.catchadeal.kerjayuk.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;

public class WarningFormLabel extends AppCompatTextView {
    public WarningFormLabel(Context context) {
        super(context);
    }

    public WarningFormLabel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTextColor(Color.RED);
        setTextSize(14);
        setVisibility(GONE);
    }

    public void alert(String message, EditText... editTexts){
        setVisibility(VISIBLE);
        setText(message);
        if(editTexts!=null){
            EditText editText = editTexts[0];
            final SimpleTextWatcher simpleTextWatcher = new SimpleTextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0){
                        setVisibility(GONE);
                        editText.removeTextChangedListener(this);
                    }
                }
            };
            editText.addTextChangedListener(simpleTextWatcher);
        }
    }




}
