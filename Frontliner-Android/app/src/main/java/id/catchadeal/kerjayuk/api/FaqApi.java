package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.FaqDto;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface FaqApi {

    @GET("api/faqs")
    Observable<WSResponse<List<FaqDto>, String>> faqs(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("search") String search,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);

}
