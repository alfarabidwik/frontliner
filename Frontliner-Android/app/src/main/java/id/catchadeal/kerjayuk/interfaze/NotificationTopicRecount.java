package id.catchadeal.kerjayuk.interfaze;

public interface NotificationTopicRecount {
    void onNeedRecount();
}