package id.catchadeal.kerjayuk.model.blog;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Data;

@Data
public class Article {
    Long id ;
    @SerializedName("date")
    Date date ;
    @SerializedName("date_gmt")
    Date dateGmt ;
    Title title ;
    Content content ;
    Excerpt excerpt ;
    @SerializedName("fimg_url")
    String imageUrl ;
    String link ;

}
