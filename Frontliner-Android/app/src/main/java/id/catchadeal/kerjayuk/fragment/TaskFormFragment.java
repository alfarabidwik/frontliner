package id.catchadeal.kerjayuk.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.tools.WLog;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.task.TaskFormActivity;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.model.formbuilder.FormLane;
import id.catchadeal.kerjayuk.utils.FormIntegration;
import id.catchadeal.kerjayuk.view.FormInputView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFormFragment extends BaseFragment  implements FormIntegration {

    public static final String TAG = TaskFormFragment.class.getName();

    @Getter@Setter TaskDto taskDto ;
    @Getter@Setter int index ;
    @Getter@Setter FormLane formLane ;
    @BindView(R.id.container_layout) LinearLayout containerLayout ;

    @Getter@Setter HashMap<String, FormInputView> formInputViewHashMap = new HashMap<>();

    private static boolean shown = false ;

    public static TaskFormFragment instance(TaskDto taskDto, int index){
        TaskFormFragment taskFormFragment = new TaskFormFragment();
        taskFormFragment.setIndex(index);
        taskFormFragment.setTaskDto(taskDto);
        FormLane formLane = taskDto.getGroupForm().getFormLanes().stream().toArray(FormLane[]::new)[index];
        taskFormFragment.setFormLane(formLane);
        return taskFormFragment ;
    }


    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_task_form;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        formLane.getCards().forEach(form -> {
            if(!formInputViewHashMap.containsKey(form.getTag())){
                formInputViewHashMap.put(form.getTag(), new FormInputView(getActivity(), this, form));
                containerLayout.addView(formInputViewHashMap.get(form.getTag()));
            }
        });
//        formLane.getCards().forEach(form -> {
//            if(formInputViewHashMap.containsKey(form.getTag())){
//                formInputViewHashMap.put(form.getTag(), new FormInputView(getActivity(), this, form));
//                containerLayout.addView(formInputViewHashMap.get(form.getTag()));
//            }
//        });

//        onShow();
    }

    public void onShow(){
//        if(!shown){
//            HashMap<String, FormInputView> formInputViewHashMap = new HashMap<>();
//            formLane.getCards().forEach(form -> {
//                formInputViewHashMap.put(form.getTag(), new FormInputView(getActivity(), this, form));
//            });
//            getAct(TaskFormActivity.class).getFormHashMap().put(formLane.getTitle(), formInputViewHashMap);
//            formLane.getCards().forEach(form -> {
//                getAct(TaskFormActivity.class).getFormHashMap().get(formLane.getTitle()).put(form.getTag(), new FormInputView(getActivity(), this, form));
//                containerLayout.addView(getAct(TaskFormActivity.class).getFormHashMap().get(formLane.getTitle()).get(form.getTag()));
//            });
//            this.shown = true ;
//        }
    }


    public boolean validateInput(){
        boolean valid = true ;
        Iterator<Map.Entry<String, FormInputView>> iterator = formInputViewHashMap.entrySet().iterator();
        while (iterator.hasNext()){
            FormInputView formInputView = iterator.next().getValue();
            valid = formInputView.validateInput();
            if(!valid) break;
        }
        return valid ;
    }


    @Override
    public FormInputView getByTag(String tag) {
        return formInputViewHashMap.get(tag);
    }

    @Override
    public void triggerAnotherFormIfAny(String tag, FormApplicationDto formApplicationDto) {
        WLog.d(TAG, "TAG = "+tag+" AND JSON = "+gson.toJson(formApplicationDto));
        formInputViewHashMap.forEach((s, formInputView) -> {
            if(formInputView.getForm().isDependAnotherTag()){
                if(formInputView.getForm().getQueryParams()!=null){
                    formInputView.getForm().getQueryParams().forEach(queryParam -> {
                        String reference = queryParam.getReference();
                        WLog.d(TAG, "KEY "+queryParam.getKey());
                        WLog.d(TAG, "REFERENCE "+reference);
                        if(reference.contains(tag)){
                            Long id = formApplicationDto.getValueId();
                            formInputView.getQueryParamMap().put(queryParam.getKey(), String.valueOf(id));
                            formInputView.fetchApi();
                        }
                    });
                }
            }
        });

//        if(getAct(TaskFormActivity.class).getFormHashMap().get(formLane.getTitle()).containsKey(tag)){
//
//        }
    }

}
