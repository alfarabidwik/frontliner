package id.catchadeal.kerjayuk.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.fragments.api.APIBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.R;
import lombok.Setter;

public class WelcomeFragment extends BaseFragment {
    
    public static final String TAG = WelcomeFragment.class.getName();

    @BindView(R.id.label_tv) TextView labelTextView ;
    @BindView(R.id.description_tv) TextView descriptionTextView ;
    @BindView(R.id.imageview) ImageView imageView ;
    @Setter int titleResource ;
    @Setter int descriptionResource ;
    @Setter int imageResource ;



    public static WelcomeFragment instance(int titleResource, int descriptionResource, int imageResource){
        WelcomeFragment welcomeFragment = new WelcomeFragment();
        welcomeFragment.setTitleResource(titleResource);
        welcomeFragment.setDescriptionResource(descriptionResource);
        welcomeFragment.setImageResource(imageResource);
        return welcomeFragment ;
    }


    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_welcome;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        labelTextView.setText(titleResource);
        descriptionTextView.setText(descriptionResource);
        imageView.setImageResource(imageResource);
    }
}
