package id.catchadeal.kerjayuk.activity;

import android.app.ActivityOptions;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.activity.APIBaseActivity;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.alfarabi.alfalibs.http.Kitchen;
import com.appizona.yehiahd.fastsave.FastSave;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hendraanggrian.bundler.Bundler;
import com.tapadoo.alerter.Alerter;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.KerjayukApplication;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.api.BankApi;
import id.catchadeal.kerjayuk.api.ConfigurationApi;
import id.catchadeal.kerjayuk.api.RegionalApi;
import id.catchadeal.kerjayuk.api.SignAuthApi;
import id.catchadeal.kerjayuk.api.TaskCategoryApi;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.api.UserDeviceApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.ConfigurationDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.model.region.CityDto;
import id.catchadeal.kerjayuk.model.region.DistrictDto;
import id.catchadeal.kerjayuk.model.region.ProvinceDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.HttpCatch;
import id.catchadeal.kerjayuk.utils.LanguageTools;
import id.catchadeal.kerjayuk.view.ViewProgress;
import lombok.Getter;
import rx.functions.Func0;
import rx.functions.Func1;

public abstract class BaseActivity extends APIBaseActivity implements RecyclerCallback {

    public static final String DIALOG_SUCCESS = "DIALOG_INFO";
    public static final String DIALOG_WARNING = "DIALOG_WARNING";
    public static final String CONFIRM_GENERAL = "CONFIRM_GENERAL";
    public static final String CONFIRM_WITHDRAWAL = "DIALOG_WITHDRAWAL";


    public static final String TAG = BaseActivity.class.getName();

    protected String currentFirebaseToken = "" ;
    @Getter protected String currentAuthorization = "" ;
    protected String deviceId = "" ;
    @Getter
    protected UserDto user ;
    @Getter
    protected ConfigurationDto configuration ;

    @Getter
    protected Gson gson = null ;
    @Nullable@BindView(R.id.toolbar) protected Toolbar toolbar ;

    protected Integer page = 0 ;


    protected List<ProvinceDto> provinceDtos = new ArrayList<>();
    protected List<CityDto> cityDtos = new ArrayList<>();
    protected List<DistrictDto> districtDtos = new ArrayList<>();
    protected List<VillageDto> villageDtos = new ArrayList<>();

    protected List<BankDto> bankDtos = new ArrayList<>();
    protected List<TaskCategoryDto> taskCategoryDtos = new ArrayList<>();



    public static Context context ;

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            throwable.printStackTrace();
        });
        LanguageTools.initLanguage(this);
        gson = KerjayukApplication.gson ;
//        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//        Slide fade = new Slide();
//        fade.setDuration(500);
//        getWindow().setEnterTransition(fade);
//
//        Explode explode = new Explode();
//        explode.setDuration(500);
//        getWindow().setExitTransition(explode);

        setContentView(contentXmlLayout());
        ButterKnife.bind(this);
        if(toolbar!=null){
            try{
                if(toolbar.getNavigationIcon()!=null){
                    toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        Bundler.bind(this);
        context = this;
        initPreference();
        getWindow().getDecorView().setBackgroundColor(getColor(R.color.page_background));
    }

    public View windowView(){
        return getWindow().getDecorView();
    }

    @Override
    public Object getObject() {
        return null;
    }

    public void showTooltip(View view, String message){
        ViewTooltip.on(view).autoHide(true, 2000).color(Color.RED).textColor(Color.WHITE)
                .corner(30).position(ViewTooltip.Position.BOTTOM)
                .text(message).show();
    }

    @Override
    public void showSnackbar(String message, View.OnClickListener onClickListener) {
        final Snackbar snackbar = Snackbar.make(windowView(), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
            onClickListener.onClick(v);
        });
        snackbar.show();
    }

    public void showAlert(String title, String message) {
        Alerter.create(this)
                .setTitle(title)
                .setText(message).setBackgroundColor(R.color.red_500)
                .show();
    }

    public void showInfo(String title, String message) {
        Alerter.create(this)
                .setTitle(title)
                .setText(message).setBackgroundColor(R.color.green_500) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }

    public void showAlert(WSResponse wsResponse){
        if(wsResponse.isSuccess()){
            showInfo(getString(R.string.Success), wsResponse.getMessage());
        }else{
            showAlert(getString(R.string.Failed), wsResponse.getMessage());
        }
    }



    public void showSnackbar(Throwable throwable, View.OnClickListener onClickListener) {
        final Snackbar snackbar = Snackbar.make(windowView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
            onClickListener.onClick(v);
        });
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initPreference();
        context = this ;
        renderData();
        if(FastSave.getInstance().getObject(Constant.CONFIGURATION, ConfigurationDto.class)==null){
            fetchConfiguration();
        }
    }

    public void initPreference(){
        user = FastSave.getInstance().getObject(Constant.USER, UserDto.class);
        configuration = FastSave.getInstance().getObject(Constant.CONFIGURATION, ConfigurationDto.class);
        currentFirebaseToken = FastSave.getInstance().getString(Constant.FIREBASE_TOKEN, "");
        currentAuthorization = FastSave.getInstance().getString(Constant.AUTHORIZATION, "");
        deviceId = FastSave.getInstance().getString(Constant.DEVICE_ID, "");
        String provincesGson = FastSave.getInstance().getString(Constant.PROVINCES, "[]");
        provinceDtos = gson.fromJson(provincesGson, new TypeToken<List<ProvinceDto>>(){}.getType());
        if(provinceDtos==null){
            provinceDtos= new ArrayList<>();
        }

        String bankGson = FastSave.getInstance().getString(Constant.BANKS, "[]");
        bankDtos = gson.fromJson(bankGson, new TypeToken<List<BankDto>>(){}.getType());
        if(bankDtos==null){
            bankDtos = new ArrayList<>();
        }
        taskCategoryDtos = new ArrayList<>();
        String taskCategoryDtoGson = FastSave.getInstance().getString(Constant.TASK_CATEGORIES, "[]");
        taskCategoryDtos = gson.fromJson(taskCategoryDtoGson, new TypeToken<List<TaskCategoryDto>>(){}.getType());
    }


    public void setCurrentResult(int result, Intent... intents){
        if(intents!=null && intents.length>0){
            setResult(result, intents[0]);
        }else{
            setResult(result);
        }
        if(result==Constant.RESULT_FINISH){
            finish();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            setResult(resultCode, data);
        }else{
            setResult(resultCode);
        }
        if(resultCode==Constant.RESULT_FINISH){
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
            return false ;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startActivity(Class toClass,  Object... objects){

        Intent intent = new Intent(this, toClass);
        if(objects!=null){
            intent.putExtras(Bundler.wrap(toClass,  objects));
        }
        if(getIntent()!=null){
            intent.putExtras(getIntent());
        }
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
        startActivity(intent, options.toBundle());
    }


    public void startActivityForResult(Class toClass, int requestCode, Object... objects){

        Intent intent = new Intent(this, toClass);
        if(objects!=null){
            intent.putExtras(Bundler.wrap(toClass,  objects));
        }
        if(getIntent()!=null){
            intent.putExtras(getIntent());
        }
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
        startActivityForResult(intent, requestCode, options.toBundle());
    }


    public void signInWithEmail(String email, String password){
        ViewProgress.show(this);
        grabServerData(Kitchen.arange(this, SignAuthApi.class).inEmail(
                email, password, deviceId, currentFirebaseToken, Constant.ANDROID), (wsResponse, response) -> {
            ViewProgress.dismiss();
            if(wsResponse.isSuccess()){
                UserDto userDto = wsResponse.getData();
                setCurrentResult(Constant.RESULT_FINISH);
                FastSave.getInstance().saveString(Constant.AUTHORIZATION, userDto.getAuthorization());
                FastSave.getInstance().saveObject(Constant.USER, userDto);
                Kitchen.putHanger(Constant.HEADER_AUTHORIZATION, userDto.getAuthorization());
                startActivity(DashboardActivity.class);
            }else{
                showAlert(null, wsResponse.getMessage());
            }
        }, (aBoolean, throwable, response) -> {
            ViewProgress.dismiss();
            HttpCatch.handle(this, throwable, response);
        });
    }

    public abstract void renderData();


    public void onClick(View view){

    }

    public void takePermission(Consumer<Boolean> callback, String... permission){
        new RxPermissions(this).request(permission).subscribe(aBoolean -> {
            callback.accept(aBoolean);
        });
    }

    public void fetchConfiguration(){
        grabServerData(Kitchen.arange(this, ConfigurationApi.class).configuration(), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                FastSave.getInstance().saveObject(Constant.CONFIGURATION, wsResponse.getData());
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


    public void fetchProvinces(){
        grabServerData(Kitchen.arange(this, RegionalApi.class).provinces(true, "name"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                provinceDtos = new ArrayList<>();
                provinceDtos.addAll(wsResponse.getData());
                FastSave.getInstance().saveString(Constant.PROVINCES, gson.toJson(provinceDtos));
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }
    public void fetchCities(Long provinceId, Consumer<List<CityDto>> consumer){
        grabServerData(Kitchen.arange(this, RegionalApi.class).cities(provinceId, true, "name"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                cityDtos = new ArrayList<>();
                cityDtos.addAll(wsResponse.getData());
                consumer.accept(cityDtos);
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }
    public void fetchDistricts(Long cityId, Consumer<List<DistrictDto>> consumer){
        grabServerData(Kitchen.arange(this, RegionalApi.class).districts(cityId, true, "name"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                districtDtos = new ArrayList<>();
                districtDtos.addAll(wsResponse.getData());
                consumer.accept(districtDtos);
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }
    public void fetchVillages(Long districtId, Consumer<List<VillageDto>> consumer){
        grabServerData(Kitchen.arange(this, RegionalApi.class).villages(districtId, true, "name"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                villageDtos = new ArrayList<>();
                villageDtos.addAll(wsResponse.getData());
                consumer.accept(villageDtos);
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    public void fetchBanks(){
        grabServerData(Kitchen.arange(this, BankApi.class).banks(true, true, "name"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                bankDtos = new ArrayList<>();
                bankDtos.addAll(wsResponse.getData());
                FastSave.getInstance().saveString(Constant.BANKS, gson.toJson(bankDtos));

            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    public void signout(){
        ViewProgress.show(this);
        grabServerData(Kitchen.arange(this, SignAuthApi.class).signout(currentAuthorization, deviceId), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                FastSave.getInstance().deleteValue(Constant.AUTHORIZATION);
                FastSave.getInstance().deleteValue(Constant.USER);
                ViewProgress.dismiss();
                startActivity(SplashActivity.class);
                ActivityCompat.finishAffinity(this);
//                Intent loginIntent = SplashActivity.getIntent(context);
//                TaskStackBuilder.create(context).addNextIntentWithParentStack(loginIntent).startActivities();
            }else{
                ViewProgress.dismiss();
                showAlert(wsResponse);
            }
        }, (aBoolean, throwable, response) -> {
            ViewProgress.dismiss();
            throwable.printStackTrace();
            showAlert(getString(R.string.Failed), throwable.getMessage());
        });
    }


    public String getString(String aString) {
        try{
            String packageName = getPackageName();
            int resId = getResources().getIdentifier(aString, "string", packageName);
            return getString(resId);
        }catch (Exception e){
            e.printStackTrace();
            return "Unavailable Identifier";
        }
    }


    public void infoDialog(String message, String dialogType, Func0... callbacks){
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_info, null);
        dialogBuilder.customView(dialogView, false);
        dialogBuilder.contentColor(getColor(R.color.transparent));
        dialogBuilder.backgroundColor(getColor(R.color.transparent));

        View okeButton = dialogView.findViewById(R.id.ok_button);
        ImageView imageView = dialogView.findViewById(R.id.imageview);
        TextView textView = dialogView.findViewById(R.id.message_tv);
        textView.setText(message);
        if(dialogType.equalsIgnoreCase(DIALOG_SUCCESS)){
            imageView.setImageResource(R.drawable.ic_berhasil);
        }
        if(dialogType.equalsIgnoreCase(DIALOG_WARNING)){
            imageView.setImageResource(R.drawable.ic_gagal);
        }
        MaterialDialog materialDialog = dialogBuilder.show();
        okeButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            if(callbacks!=null && callbacks.length>0){
                callbacks[0].call();
            }
        });
    }

    public void confirmDialog(String title, String message, String dialogType, Func1<Boolean, Boolean> callback){
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm, null);
        dialogBuilder.customView(dialogView, false);
        dialogBuilder.contentColor(getColor(R.color.transparent));
        dialogBuilder.backgroundColor(getColor(R.color.transparent));
        View okeButton = dialogView.findViewById(R.id.ok_button);
        View noButton = dialogView.findViewById(R.id.no_button);
        ImageView imageView = dialogView.findViewById(R.id.imageview);
        TextView textView = dialogView.findViewById(R.id.message_tv);
        TextView titleView = dialogView.findViewById(R.id.title_tv);
        if(StringUtils.equals(dialogType, CONFIRM_GENERAL)){
            imageView.setImageResource(R.drawable.ic_informasi);
        }
        if(StringUtils.equals(dialogType, CONFIRM_WITHDRAWAL)){
            imageView.setImageResource(R.drawable.ic_withdraw);

        }
        titleView.setText(title);
        textView.setText(message);

        MaterialDialog materialDialog = dialogBuilder.show();
        okeButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            callback.call(true);
        });
        noButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            callback.call(false);
        });
    }

    public void confirmErrorDialog(String title, String message, int positiveTitleResource, int negativeTitleResource, Func1<Boolean, Boolean> callback){
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_confirm, null);
        dialogBuilder.customView(dialogView, false);
        dialogBuilder.contentColor(getColor(R.color.transparent));
        dialogBuilder.backgroundColor(getColor(R.color.transparent));
        Button okeButton = dialogView.findViewById(R.id.ok_button);
        Button noButton = dialogView.findViewById(R.id.no_button);
        ImageView imageView = dialogView.findViewById(R.id.imageview);
        TextView textView = dialogView.findViewById(R.id.message_tv);
        TextView titleView = dialogView.findViewById(R.id.title_tv);
        imageView.setImageResource(R.drawable.ic_gagal);
        titleView.setText(title);
        textView.setText(message);

        MaterialDialog materialDialog = dialogBuilder.show();
        okeButton.setText(positiveTitleResource);
        noButton.setText(negativeTitleResource);
        okeButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            callback.call(true);
        });
        noButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            callback.call(false);
        });
    }


    public void logoutConfirmDialog(){
        MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.logout_dialog_confirm, null);
        dialogBuilder.customView(dialogView, false);
        dialogBuilder.contentColor(getColor(R.color.transparent));
        dialogBuilder.backgroundColor(getColor(R.color.transparent));
        View okeButton = dialogView.findViewById(R.id.ok_button);
        View noButton = dialogView.findViewById(R.id.no_button);
        ImageView imageView = dialogView.findViewById(R.id.imageview);
        imageView.setImageResource(R.drawable.ic_exit);
        MaterialDialog materialDialog = dialogBuilder.show();
        okeButton.setOnClickListener(v -> {
            materialDialog.dismiss();
            signout();
        });
        noButton.setOnClickListener(v -> {
            materialDialog.dismiss();
        });
    }


    public void refreshUser(){
        grabServerData(Kitchen.arange(this, UserApi.class).authorization(currentAuthorization), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                FastSave.getInstance().saveObject(Constant.USER, wsResponse.getData());
                this.user = wsResponse.getData();
                renderData();
            }

        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


    public void refreshDevice(){
        grabServerData(Kitchen.arange(this, UserDeviceApi.class).refresh(currentAuthorization, deviceId, Constant.ANDROID, currentFirebaseToken), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){

            }else{
                signout();
            }
        }, (aBoolean, throwable, response) -> {

        });

    }






}
