package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.api.FaqApi;
import id.catchadeal.kerjayuk.holder.FaqHolder;
import id.catchadeal.kerjayuk.model.misc.FaqDto;
import id.catchadeal.kerjayuk.utils.DrawableClickListener;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class FaqActivity extends BaseActivity {

    public static final String TAG = FaqActivity.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.search_et) EditText searchEditText ;
    List<FaqDto> faqs = new ArrayList<>();
    private BasicActRecyclerAdapter<FaqDto, FaqActivity, FaqHolder> adapter ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_faq;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.faqs = new ArrayList<>();
        adapter = new BasicActRecyclerAdapter<>(this, FaqHolder.class, faqs);
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));

        this.page = 0 ;
        ViewProgress.show(this);
        fetchFaqs("");
        searchEditText.setOnTouchListener(new DrawableClickListener.RightDrawableClickListener(searchEditText) {
            @Override
            public boolean onDrawableClick() {
                ViewProgress.show(FaqActivity.this);
                fetchFaqs(searchEditText.getText().toString());
                return false;
            }
        });

        searchEditText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if(StringUtils.isEmpty(s.toString())){
                    ViewProgress.show(FaqActivity.this);
                    fetchFaqs("");
                }
            }
        });
        searchEditText.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                fetchFaqs(searchEditText.getText().toString());
                return true;
            }
            return false;
        });
    }

    @Override
    public void renderData() {

    }

    void fetchFaqs(String search){
        grabServerData(Kitchen.arange(this, FaqApi.class).faqs(currentAuthorization, search , false, "created"),
                (wsResponse, response) -> {
                    if(wsResponse.isSuccess()){
                        this.faqs = new ArrayList<>();
                        this.faqs.addAll(wsResponse.getData());
                        this.adapter.setObjects(this.faqs);
                    }
                    ViewProgress.dismiss();
                }, (aBoolean, throwable, response) -> {
                    ViewProgress.dismiss();
                    throwable.printStackTrace();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                });

    }




}
