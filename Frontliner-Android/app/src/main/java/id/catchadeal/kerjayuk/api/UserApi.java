package id.catchadeal.kerjayuk.api;

import android.support.annotation.Nullable;

import java.util.Map;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.model.user.UserTaskSummary;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface UserApi {



    @POST(Constant.BASE_API+"/user/forgetPassword")
    Observable<WSResponse> forgetPassword(
            @Query("email") String email
    );

    @POST(Constant.BASE_API+"/user/resetPassword")
    Observable<WSResponse<Object, String>> resetPassword(
            @Query("token") String token,
            @Query("newPassword") String newPassword);

    @GET(Constant.BASE_API+"/user/validateMobilePhone")
    Observable<WSResponse> validateMobilePhone(@Query("mobilePhone") String mobilePhone);

    @GET(Constant.BASE_API+"/user/validateMobilePhoneWithEmailMatcher")
    Observable<WSResponse> validateMobilePhoneWithEmailMatcher(@Query("mobilePhone") String mobilePhone, @Query("email") String email);


    @GET(Constant.BASE_API+"/user/checkReferralCode")
    Observable<WSResponse> checkReferralCode(@Query("referralCode") String referralCode);

    @POST(Constant.BASE_API+"/user/uploadImage")
    @Multipart
    Observable<WSResponse<UserDto, String>> uploadImage(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("userId") Long userId,
            @Query("type") String type, @Part @Nullable MultipartBody.Part multipartFile);

    @GET(Constant.BASE_API+"/user/authorization")
    Observable<WSResponse<UserDto, String>> authorization(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization);

    @POST(Constant.BASE_API+"/user/saveUpload")
    @Multipart
    Observable<WSResponse<UserDto, String>> saveUpload(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Part("userDtoGson") RequestBody userDtoGson,
            @Part @Nullable MultipartBody.Part photoMultipartFile,
            @Part @Nullable MultipartBody.Part idCardMultipartFile,
            @Part @Nullable MultipartBody.Part selfieIdCardMultipartFile);

    @GET(Constant.BASE_API+"/user/taskSummary")
    Observable<WSResponse<UserTaskSummary, String>> taskSummary(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization);


}
