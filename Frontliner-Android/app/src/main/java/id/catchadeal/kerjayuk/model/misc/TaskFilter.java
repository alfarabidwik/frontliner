package id.catchadeal.kerjayuk.model.misc;

import java.math.BigDecimal;
import java.util.Date;

import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import lombok.Data;

@Data
public class TaskFilter {
    TaskCategoryDto taskCategory ;
    TaskApplicationStatusDto taskApplicationStatus ;
    BigDecimal minFee ;
    BigDecimal maxFee ;
    Date startDate ;
    Date endDate ;

    public TaskFilter(TaskCategoryDto taskCategory, TaskApplicationStatusDto taskApplicationStatus, BigDecimal minFee, BigDecimal maxFee, Date startDate, Date endDate){
        this.taskCategory = taskCategory ;
        this.taskApplicationStatus = taskApplicationStatus;
        this.minFee = minFee ;
        this.maxFee = maxFee;
        this.startDate = startDate ;
        this.endDate = endDate ;
    }

    public TaskFilter(){

    }

}
