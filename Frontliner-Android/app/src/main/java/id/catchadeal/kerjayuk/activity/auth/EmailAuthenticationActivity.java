package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.InputTools;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.api.SignAuthApi;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.HttpCatch;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class EmailAuthenticationActivity extends BaseActivity {

    public static final String TAG = EmailAuthenticationActivity.class.getName();

    @BindView(R.id.email_et) EditText emailEditText ;
    @BindView(R.id.password_et) EditText passwordEditText ;
    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.forget_password_link_tv) HtmlTextView forgetPasswordTextview ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_email_authentication;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode==Constant.REQUEST_RESET_PASSWORD || requestCode==Constant.REQUEST_AUTH) && resultCode==Constant.RESULT_FINISH && data!=null){
            String messageJson = data.getStringExtra(Constant.MESSAGE_JSON);
            WSResponse wsResponse = gson.fromJson(messageJson, WSResponse.class);
            showAlert(wsResponse);
        }
    }

    @Override
    public void renderData() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Spanned agrementSpanned = null ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            agrementSpanned = Html.fromHtml(getString(R.string.ForgetPasswordLink), Html.FROM_HTML_MODE_LEGACY);
        } else {
            agrementSpanned = Html.fromHtml(getString(R.string.ForgetPasswordLink));
        }
        if(agrementSpanned!=null){
            forgetPasswordTextview.setText(agrementSpanned);
            CommonView.setTextViewHTMLClickCallback(forgetPasswordTextview, getString(R.string.ForgetPasswordLink), view -> {
                startActivityForResult(ForgetPasswordActivity.class, Constant.REQUEST_RESET_PASSWORD);
            });
        }
    }

    @OnClick(R.id.submit_button)
    public void onClick(){
        if(!InputTools.isComplete(emailEditText)){
            emailEditText.setError(getString(R.string.EnterEmail));
            return;
        }
        if(!InputTools.isValidEmail(emailEditText)){
            emailEditText.setError(getString(R.string.error_invalid_email));
            return;
        }
        if(!InputTools.isComplete(passwordEditText)){
            passwordEditText.setError(getString(R.string.EnterPassword));
            return;
        }
        if(InputTools.isComplete(false, emailEditText, passwordEditText)){
            ViewProgress.show(this);
            grabServerData(Kitchen.arange(this, SignAuthApi.class).authEmail(
                    emailEditText.getText().toString(), passwordEditText.getText().toString()), (wsResponse, response) -> {
                ViewProgress.dismiss();
                if(wsResponse.isSuccess()){
                    if(wsResponse.getData().isFirstLogin()){
                        startActivityForResult(SetPhoneNumberActivity.class, Constant.REQUEST_AUTH, gson.toJson(wsResponse.getData()));
                    }else{
                        signInWithEmail(emailEditText.getText().toString(), passwordEditText.getText().toString());
                    }
                }else{
                    showAlert(null, wsResponse.getMessage());
                }
            }, (aBoolean, throwable, response) -> {
                ViewProgress.dismiss();
                HttpCatch.handle(this, throwable, response);
            });

        }
    }

}
