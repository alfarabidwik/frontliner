package id.catchadeal.kerjayuk.api;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ResetPasswordApi {

    @GET(Constant.BASE_API+"/resetPassword/validate")
    Observable<WSResponse<Object, String>> validateResetPassword(
            @Query("token") String token
    );

}
