package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.InputTools;
import com.appizona.yehiahd.fastsave.FastSave;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.api.SignAuthApi;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class RegistrationActivity extends BaseActivity {

    public static final String TAG = RegistrationActivity.class.getName();


    @BindView(R.id.email_et) MaterialEditText emailEditText ;
    @BindView(R.id.fullname_et) MaterialEditText fullnameEditText ;
    @BindView(R.id.referral_code_et) MaterialEditText referralCodeEditText ;
    @BindView(R.id.submit_button) Button submitButton ;

    @BindExtra String userDtoGson ;

    private UserDto userDto ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_registration;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userDto = gson.fromJson(userDtoGson, UserDto.class);

//        submitButton.setOnClickListener(v -> {
//            setCurrentResult(Constant.RESULT_FINISH);
//            FastSave.getInstance().saveString(Constant.AUTHORIZATION, "123456");
//            startActivity(DashboardActivity.class);
//        });
    }


    @Override
    public void renderData() {

    }

    @OnClick(R.id.submit_button)
    public void onClick(){
        if(!InputTools.isComplete(false, fullnameEditText)){
            fullnameEditText.setError(getString(R.string.PleaseEnterFullname));
            return;
        }
        if(!InputTools.isComplete(false, emailEditText)){
            emailEditText.setError(getString(R.string.PleaseEnterEmail));
            return;
        }
        if(!InputTools.isValidEmail(emailEditText)){
            emailEditText.setError(getString(R.string.InvalidEmailCharacter));
            return;
        }

        userDto.setEmail(emailEditText.getText().toString());
        userDto.setFullname(fullnameEditText.getText().toString());
        if(InputTools.isComplete(false,referralCodeEditText)){
            userDto.setReferralCode(referralCodeEditText.getText().toString());
            ViewProgress.show(this);
            grabServerData(Kitchen.arange(this, UserApi.class).checkReferralCode(referralCodeEditText.getText().toString()), (wsResponse, response) -> {
                ViewProgress.dismiss();
                if(wsResponse.isSuccess()){
                    register();
                }else{
                    showAlert(wsResponse);
                }
            }, (aBoolean, throwable, response) -> {
                ViewProgress.dismiss();
                throwable.printStackTrace();
                showAlert(getString(R.string.Failed), throwable.getMessage());
            });
        }else{
            register();
        }
    }

    public void register(){
        ViewProgress.show(this);
        grabServerData(Kitchen.arange(this, SignAuthApi.class).commitRegistration(userDto.getRegistrationToken(), true,
                emailEditText.getText().toString(),
                fullnameEditText.getText().toString(), referralCodeEditText.getText().toString()), (wsResponse, response) -> {
            ViewProgress.dismiss();
            if(wsResponse.isSuccess()){
                UserDto userDto = wsResponse.getData();
                setCurrentResult(Constant.RESULT_FINISH);
                FastSave.getInstance().saveString(Constant.AUTHORIZATION, userDto.getAuthorization());
                FastSave.getInstance().saveObject(Constant.USER, userDto);
                startActivity(DashboardActivity.class);
            }else{
                showAlert(wsResponse);
            }
        }, (aBoolean, throwable, response) -> {
            ViewProgress.dismiss();
            throwable.printStackTrace();
            showAlert(getString(R.string.Failed), throwable.getMessage());
        });
    }
}
