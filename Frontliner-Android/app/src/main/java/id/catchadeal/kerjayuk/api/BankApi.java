package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.model.region.CityDto;
import id.catchadeal.kerjayuk.model.region.DistrictDto;
import id.catchadeal.kerjayuk.model.region.ProvinceDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BankApi {

    @GET("api/banks")
    Observable<WSResponse<List<BankDto>, String>> banks(@Query("active") Boolean active, @Query("ascending") Boolean ascending, @Query("sortir") String sortir);


}
