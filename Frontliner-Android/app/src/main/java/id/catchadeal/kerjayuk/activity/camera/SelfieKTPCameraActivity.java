package id.catchadeal.kerjayuk.activity.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.alfarabi.alfalibs.tools.WLog;
import com.ragnarok.rxcamera.RxCamera;
import com.ragnarok.rxcamera.RxCameraData;
import com.ragnarok.rxcamera.config.CameraUtil;
import com.ragnarok.rxcamera.config.RxCameraConfig;
import com.ragnarok.rxcamera.request.Func;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.utils.Constant;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class SelfieKTPCameraActivity extends BaseCameraActivity {

    public static final String TAG = SelfieKTPCameraActivity.class.getName();

    @Override
    public RxCameraConfig cameraConfig() {
        this.cameraFront = true ;
        return new RxCameraConfig.Builder()
                .useFrontCamera()
                .setAutoFocus(true)
                .setPreferPreviewFrameRate(15, 30)
                .setPreferPreviewSize(new Point(640, 480), false)
                .setHandleSurfaceEvent(true)
                .build();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_selfie_ktp_camera;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void renderData() {

    }


    @Override
    public void requestTakePicture() {
        if (!checkCamera()) {
            return;
        }
        rxCamera.request().takePictureRequest(true, new Func() {
            @Override
            public void call() {
                showLog("Captured!");
            }
        }, 480, 640, ImageFormat.JPEG, true).subscribe(rxCameraData -> {
            String path = Environment.getExternalStorageDirectory() + "/"+System.nanoTime()+".jpg";
            File file = new File(path);
            Bitmap bitmap = BitmapFactory.decodeByteArray(rxCameraData.cameraData, 0, rxCameraData.cameraData.length);
            bitmap = flipImage(bitmap);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rxCameraData.rotateMatrix, false);
            try {
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                Intent intent = getIntent();
                intent.putExtra(Constant.FILE_PATH, file.getPath());
                setCurrentResult(Constant.RESULT_FINISH, intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
//                showLog("Save file on " + path);
        });
    }
}
