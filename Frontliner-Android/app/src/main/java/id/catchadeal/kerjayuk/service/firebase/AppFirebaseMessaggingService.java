package id.catchadeal.kerjayuk.service.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.widget.RemoteViews;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.WLog;
import com.appizona.yehiahd.fastsave.FastSave;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.apache.commons.lang.StringUtils;

import java.util.Date;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.notification.NotificationActivity;
import id.catchadeal.kerjayuk.activity.notification.NotificationAndSummaryActivity;
import id.catchadeal.kerjayuk.activity.user.BottomNavigationActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.api.UserDeviceApi;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.utils.Constant;

public class AppFirebaseMessaggingService extends FirebaseMessagingService {

    public static final String TAG = AppFirebaseMessaggingService.class.getName();


    Gson gson = new Gson();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onNewToken(String newFirebaseToken) {
        super.onNewToken(newFirebaseToken);
        createNotificationChannel(this, "1");
        FastSave.getInstance().saveString(Constant.FIREBASE_TOKEN, newFirebaseToken);
        String currentAuthorization = FastSave.getInstance().getString(Constant.AUTHORIZATION, "");
        String deviceId = FastSave.getInstance().getString(Constant.DEVICE_ID, "");
        if(StringUtils.isNotEmpty(currentAuthorization) && StringUtils.isNotEmpty(deviceId)){
            Kitchen.cook(Kitchen.arange(this, UserDeviceApi.class).refresh(currentAuthorization,deviceId, Constant.ANDROID, newFirebaseToken), (wsResponse, response) -> {

            }, (aBoolean, throwable, response) -> {

            });
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if(remoteMessage.getNotification()!=null){
            showNotification(remoteMessage);
            String tag = remoteMessage.getNotification().getTag();
            if(tag!=null){
                if(tag.equalsIgnoreCase(NotificationDto.SPECIFIC) || tag.equalsIgnoreCase(NotificationDto.NEW_TASK)){
                    if(BottomNavigationActivity.notificationEventBadgeRecount!=null){
                        BottomNavigationActivity.notificationEventBadgeRecount.onGetNotification();
                    }
                    if(NotificationAndSummaryActivity.notificationTopicRecount!=null){
                        NotificationAndSummaryActivity.notificationTopicRecount.onNeedRecount();
                    }
                }
                if(tag.equalsIgnoreCase(NotificationDto.BROADCAST) || tag.equalsIgnoreCase(NotificationDto.NEWS) || tag.equalsIgnoreCase(NotificationDto.ARTICLE)){
                    if(NotificationActivity.notificationTopicRecount!=null){
                        NotificationActivity.notificationTopicRecount.onNeedRecount();
                    }
                }
            }

        }

    }


    private void showNotification(RemoteMessage remoteMessage) {
        RemoteMessage.Notification notification = remoteMessage.getNotification();

        Intent intent = new Intent(this, NotificationAndSummaryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        String channelId = "1";
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.holder_notification);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(notification.getTitle());
        bigTextStyle.bigText(notification.getBody());

//        PendingIntent openIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Action openAction =
//                new NotificationCompat.Action.Builder(android.R.drawable.sym_def_app_icon,
//                        "Open", openIntent).build();


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                .setStyle(bigTextStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
//                .setVibrate(new long[]{NotificationCompat.DEFAULT_VIBRATE})
                .setVibrate(new long[0])
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setColor(ContextCompat.getColor(this, R.color.greyPrimary))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION))
//                .setCustomHeadsUpContentView(notificationLayout)
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(pendingIntent, true);
//        builder.addAction(openAction);


        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel(this, channelId);
        manager.notify(0, builder.build());

    }

    public static void createNotificationChannel(Context context, String channelId) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String name = context.getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(context.getClass().getPackage().getName());
            channel.enableVibration(true);
            channel.enableLights(true);
            channel.setShowBadge(true);
            channel.setVibrationPattern(new long[]{400, 200, 400});
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

//    private Intent getMessageReplyIntent(String label) {
//        return new Intent()
//                .addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
//                .setAction(REPLY_ACTION)
//                .putExtra(KEY_PRESSED_ACTION, label);
//    }

}
