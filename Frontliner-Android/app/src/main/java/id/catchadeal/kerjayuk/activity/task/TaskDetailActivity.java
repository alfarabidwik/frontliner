package id.catchadeal.kerjayuk.activity.task;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.InputTools;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.annotations.BindExtra;

import org.apache.commons.lang.time.DateFormatUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Date;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.api.TaskApi;
import id.catchadeal.kerjayuk.api.TaskApplicationApi;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.view.ConfirmButton;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class TaskDetailActivity extends BaseActivity {


    public static final String TAG = TaskDetailActivity.class.getName();

    @BindView(R.id.banner_iv) ImageView bannerImageView ;
    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;
    @BindView(R.id.subtitle_tv) TextView subtitleTextView ;
    @BindView(R.id.worker_verification_status_tv) TextView workerVerificationTextView ;
    @BindView(R.id.task_expiration_time_label_tv) TextView taskExpirationTimeLabelTextView ;
    @BindView(R.id.task_expiration_time_tv) TextView taskExpirationTimeTextView ;
    @BindView(R.id.validating_processing_time_tv) TextView validatingProcessingTimeTextView ;
    @BindView(R.id.description_html_tv) HtmlTextView descriptionHtmlTextView ;
    @BindView(R.id.additional_info_html_tv) HtmlTextView additionalInfoHtmlTextView ;
    @BindView(R.id.term_and_condition_tv) HtmlTextView termAndConditionTextView ;
    @BindView(R.id.submit_button) ConfirmButton submitButton ;
    @BindView(R.id.footer_cardview) CardView footerCardView ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;
    @BindView(R.id.verification_iv) ImageView verificationImageView ;

    @BindExtra String taskDtoGson ;
    TaskDto taskDto ;

    @BindExtra Boolean applyable ;


    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_detail;
    }

    @Override
    public void renderData() {

        GlideApp.with(this).load(taskDto.getBannerImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.bg_kerjayuk).error(R.drawable.bg_kerjayuk).into(bannerImageView);
        GlideApp.with(this).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_launcher).error(R.drawable.ic_launcher).into(partnerImageView);
        feeTextView.setText(NumberUtil.moneyFormat(taskDto.getFee(), true));
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            taskFeeTypeTextView.setText(R.string.PerTask);
        }
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            taskFeeTypeTextView.setText(R.string.PerAgent);
        }


        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());
        subtitleTextView.setText(taskDto.getSubtitle());
        if(taskDto.getWorkerVerificationType().equalsIgnoreCase(TaskDto.MUST_VERIFIED)){
            if(user.getVerificationStatus().equalsIgnoreCase(UserDto.VERIFIED)){
                verificationImageView.setImageResource(R.drawable.ic_status_mitra);
            }else{
                verificationImageView.setImageResource(R.drawable.ic_unverified);
            }
        }else{
            verificationImageView.setImageResource(R.drawable.ic_status_mitra);
        }
        workerVerificationTextView.setText(getString(taskDto.getWorkerVerificationType()));
        if(taskDto.getPeriodType().equalsIgnoreCase(TaskDto.PERIODIC)){
            taskExpirationTimeLabelTextView.setText(R.string.TaskExpirationTime);
            taskExpirationTimeTextView.setText(DateFormatUtils.format(taskDto.getEndPeriod(), DateAppConfig.VIEW_DATE_FORMAT));
        }
        if(taskDto.getPeriodType().equalsIgnoreCase(TaskDto.NEVER_END)){
            taskExpirationTimeLabelTextView.setText(R.string.TaskApplicable);
            taskExpirationTimeTextView.setText(getString(taskDto.getPeriodType()));
        }
        validatingProcessingTimeTextView.setText(String.valueOf(taskDto.getValidatingTimeInHour())+" "+getString(R.string.Hour));
        descriptionHtmlTextView.setHtml(taskDto.getDescription());
        additionalInfoHtmlTextView.setHtml(taskDto.getAdditionalInfo());

        Spanned agreementSpanned = null ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            agreementSpanned = Html.fromHtml(getString(R.string.Task_Agreement_With_Link), Html.FROM_HTML_MODE_LEGACY);
        } else {
            agreementSpanned = Html.fromHtml(getString(R.string.Task_Agreement_With_Link));
        }
        if(agreementSpanned!=null){
            termAndConditionTextView.setText(agreementSpanned);
            CommonView.setTextViewHTMLClickCallback(termAndConditionTextView, getString(R.string.Task_Agreement_With_Link), view -> {
                startActivity(WebViewActivity.class, getString(R.string.Task_Agreement_Link));
            });
        }
        if(!applyable){
            footerCardView.setVisibility(View.GONE);
        }else{
            boolean canApply = true ;
            canApply = taskDto.getActive();
            if(taskDto.getApplicationLimitType().equalsIgnoreCase(TaskDto.LIMITED) && taskDto.getApplicationCount()>-taskDto.getApplicationLimitCount()){
                canApply = false ;
            }
            if(taskDto.getPeriodType().equalsIgnoreCase(TaskDto.PERIODIC) && (taskDto.getEndPeriod().getTime()<(new Date().getTime()) || taskDto.getStartPeriod().getTime()>(new Date().getTime()))){
                canApply = false ;
            }

            if(canApply){
                footerCardView.setVisibility(View.VISIBLE);
            }
        }
    }

    void checkIfExist(){
        footerCardView.setVisibility(View.VISIBLE);
        if(taskDto.getTaskCategory().getId().equals(TaskCategoryDto.PRODUCT_KEUANGAN_ID)){
            submitButton.setText(R.string.Doit);
        }else{
            submitButton.setEnabled(false);
            ViewProgress.show(this);
            grabServerData(Kitchen.arange(this, TaskApplicationApi.class).taskApplicationExists(currentAuthorization, taskDto.getId()), (wsResponse, response) -> {
                ViewProgress.dismiss();
                if(wsResponse.isSuccess()){
                    submitButton.setEnabled(true);
                    if(wsResponse.getData()){
                        submitButton.setText(R.string.Send_The_Work);
                    }else{
                        submitButton.setText(R.string.Take_A_Work);
                    }
                }
            }, (aBoolean, throwable, response) -> {
                ViewProgress.dismiss();
            });
        }
    }

    void fetchDetail(){
        footerCardView.setVisibility(View.GONE);
        ViewProgress.show(this);
        grabServerData(Kitchen.arange(this, TaskApi.class).task(currentAuthorization, taskDto.getId()), (wsResponse, response) -> {
            ViewProgress.dismiss();
            if(wsResponse.isSuccess()){
                if(wsResponse.getData().isApplyable()){
                    checkIfExist();
                }else{
                    confirmErrorDialog(getString(R.string.TaskIsAlreadyUnavailable), wsResponse.getData().getMessage(), R.string.SeeThisTask, R.string.Back, aBoolean -> {
                        if(!aBoolean){
                            finish();
                        }
                        return aBoolean;
                    });
                }
            }else{
                showAlert(wsResponse);
            }
        }, (aBoolean, throwable, response) -> {
            ViewProgress.dismiss();
            showAlert(getString(R.string.Failed), throwable.getMessage());
        });
    }


    @OnClick(R.id.submit_button)
    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(taskDto.getWorkerVerificationType().equalsIgnoreCase(TaskDto.MUST_VERIFIED) && !user.getVerificationStatus().equalsIgnoreCase(UserDto.VERIFIED)){
            infoDialog(getString(R.string.AvailableForVerifiedAccountOnly), DIALOG_WARNING,  () -> {
                return true ;
            });
            return;
        }
        if(taskDto.getTaskCategory().getId().equals(TaskCategoryDto.PRODUCT_KEUANGAN_ID)){
            startActivity(WebViewActivity.class, taskDto.getTaskAddressLink()+"?agentCode="+user.getAgentCode());
        }else{
            if(submitButton.getText().toString().equalsIgnoreCase(getString(R.string.Send_The_Work))){
                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, TaskApplicationApi.class).getMyTaskApplication(currentAuthorization, taskDto.getId()), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    if(wsResponse.isSuccess()){
                        TaskApplicationDto taskApplicationDto  = wsResponse.getData();
                        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
                            startActivityForResult(TaskReferralMemberActivity.class, Constant.REQUEST_SUBMIT, taskDtoGson, gson.toJson(taskApplicationDto));
                        }
                        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
                            startActivityForResult(TaskFormActivity.class, Constant.REQUEST_SUBMIT, taskDtoGson, gson.toJson(taskApplicationDto));
                        }
                    }else{
                        showAlert(wsResponse);
                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                    ViewProgress.dismiss();
                    showAlert(getString(R.string.Failed),  throwable.getMessage());
                });
            }
            if(submitButton.getText().toString().equalsIgnoreCase(getString(R.string.Take_A_Work))){
                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, TaskApplicationApi.class).apply(currentAuthorization, taskDto.getId()), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    if(wsResponse.isSuccess()){
                        TaskApplicationDto taskApplicationDto  = wsResponse.getData();
                        confirmDialog(getString(R.string.Confirmation), getString(R.string.ThanksForApplyingTask_DoYouWantToContinueToSubmitApplication),CONFIRM_GENERAL, aBoolean -> {
                            if(aBoolean){
                                if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
                                    startActivityForResult(TaskReferralMemberActivity.class, Constant.REQUEST_SUBMIT, taskDtoGson, gson.toJson(taskApplicationDto));
                                }
                                if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
                                    startActivityForResult(TaskFormActivity.class, Constant.REQUEST_SUBMIT, taskDtoGson, gson.toJson(taskApplicationDto));
                                }
                            }
                            return aBoolean;
                        });
                    }else{
                        showAlert(wsResponse);
                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                    ViewProgress.dismiss();
                    showAlert(getString(R.string.Failed),  throwable.getMessage());
                });
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        taskDto = gson.fromJson(taskDtoGson, TaskDto.class);
        refreshUser();
        fetchDetail();
    }

}
