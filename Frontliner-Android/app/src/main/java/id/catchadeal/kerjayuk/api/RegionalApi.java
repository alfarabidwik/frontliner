package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.region.CityDto;
import id.catchadeal.kerjayuk.model.region.DistrictDto;
import id.catchadeal.kerjayuk.model.region.ProvinceDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface RegionalApi {

    @GET("api/provinces")
    Observable<WSResponse<List<ProvinceDto>, String>> provinces(@Query("ascending") Boolean ascending, @Query("sortir") String sortir);

    @GET("api/cities")
    Observable<WSResponse<List<CityDto>, String>> cities(@Query("provinceId") Long provinceId, @Query("ascending") Boolean ascending, @Query("sortir") String sortir);

    @GET("api/districts")
    Observable<WSResponse<List<DistrictDto>, String>> districts(@Query("cityId") Long cityId, @Query("ascending") Boolean ascending, @Query("sortir") String sortir);

    @GET("api/villages")
    Observable<WSResponse<List<VillageDto>, String>> villages(@Query("districtId") Long districtId, @Query("ascending") Boolean ascending, @Query("sortir") String sortir);


}
