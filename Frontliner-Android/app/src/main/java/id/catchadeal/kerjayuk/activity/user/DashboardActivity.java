package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.appizona.yehiahd.fastsave.FastSave;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.request.RequestOptions;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.adapter.BannerPagerAdapter;
import id.catchadeal.kerjayuk.api.ArticleApi;
import id.catchadeal.kerjayuk.api.BannerApi;
import id.catchadeal.kerjayuk.api.TaskApi;
import id.catchadeal.kerjayuk.api.TaskCategoryApi;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.holder.DashboardArticleHolder;
import id.catchadeal.kerjayuk.holder.DashboardTaskCategoryHolder;
import id.catchadeal.kerjayuk.holder.DotHolder;
import id.catchadeal.kerjayuk.holder.HorizontalTaskHolder;
import id.catchadeal.kerjayuk.model.Dot;
import id.catchadeal.kerjayuk.model.blog.Article;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.BannerDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.user.UserTaskSummary;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.utils.SliderControl;
import id.catchadeal.kerjayuk.utils.ZoomOutPageTransformer;
import id.catchadeal.kerjayuk.view.BannerViewPager;

public class DashboardActivity extends BottomNavigationActivity implements SliderControl {

    public static final String TAG = DashboardActivity.class.getName();

    @BindView(R.id.greeting_tv) TextView greetingTextView ;
    @BindView(R.id.profile_iv) ImageView profileImageView ;
    @BindView(R.id.task_recyclerview) AlfaRecyclerView taskRecyclerView ;
    @BindView(R.id.category_recyclerview) AlfaRecyclerView categoryRecyclerView ;
    @BindView(R.id.article_recyclerview) AlfaRecyclerView articleRecyclerView ;
    @BindView(R.id.balance_tv) TextView balanceTextView ;
    @BindView(R.id.current_task_cv) CardView currentTaskCardView ;
    @BindView(R.id.waiting_task_cv) CardView waitingTaskCardView ;

    @BindView(R.id.see_all_article_tv) TextView seeAllTextView ;
    @BindView(R.id.total_income_tv) TextView totalIncomeTextView ;
    @BindView(R.id.total_finished_task_tv) TextView totalFinishedTextView ;

    @BindView(R.id.ongoing_task_tv) TextView onGoingTaskTextView;
    @BindView(R.id.ongoing_task_description_tv) TextView onGoingTaskDescriptionTextView;
    @BindView(R.id.waiting_task_tv) TextView waitingTaskTextView;
    @BindView(R.id.waiting_task_description_tv) TextView waitingTaskDescriptionTextView;
    @BindView(R.id.banner_viewpager) BannerViewPager bannerViewPager ;

    @BindView(R.id.dot_recyclerview) AlfaRecyclerView dotRecyclerView ;

    private BasicActRecyclerAdapter<TaskDto, DashboardActivity, HorizontalTaskHolder> taskAdapter ;
    private BasicActRecyclerAdapter<TaskCategoryDto, DashboardActivity, DashboardTaskCategoryHolder> categoryAdapter ;
    private BasicActRecyclerAdapter<Article, DashboardActivity, DashboardArticleHolder> articleAdapter ;

    private List<TaskDto> taskDtos ;
    private List<Article> articles ;
    private List<BannerDto> bannerDtos = new ArrayList<>();
    private UserTaskSummary userTaskSummary = new UserTaskSummary();

    private BasicActRecyclerAdapter<Dot, DashboardActivity, DotHolder> dotAdapter ;

    private int bannerPosition ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int bottomMenu() {
        return R.id.home;
    }

    @Override
    public void focusBar() {
        ((BottomNavigationItemView)((BottomNavigationMenuView)bottomNavigationView.getChildAt(0)).getChildAt(0)).setBackgroundColor(getColor(R.color.greenTertiary));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskDtos = new ArrayList<>();
        taskDtos.addAll(CommonView.tasksLoader());
        taskCategoryDtos = new ArrayList<>();
        String taskCategoryDtoGson = FastSave.getInstance().getString(Constant.TASK_CATEGORIES, "[]");
        taskCategoryDtos = gson.fromJson(taskCategoryDtoGson, new TypeToken<List<TaskCategoryDto>>(){}.getType());

        taskAdapter = new BasicActRecyclerAdapter<>(this, HorizontalTaskHolder.class, taskDtos);
        taskAdapter.initRecyclerView(taskRecyclerView, new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false){
            @Override
            public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                lp.width = getWidth() - (getWidth()/5);
                return true;
            }
        });
        taskRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));
        new GravitySnapHelper(Gravity.START).attachToRecyclerView(taskRecyclerView);

        categoryAdapter = new BasicActRecyclerAdapter<>(this, DashboardTaskCategoryHolder.class, taskCategoryDtos);
        categoryAdapter.initRecyclerView(categoryRecyclerView, new GridLayoutManager(this, 2));
        categoryRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(10, EqualSpacingItemDecoration.GRID));

        articles = new ArrayList<>();
        articles.addAll(CommonView.articleLoader());
        articleAdapter = new BasicActRecyclerAdapter<>(this, DashboardArticleHolder.class, articles);
        articleAdapter.initRecyclerView(articleRecyclerView, new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false){
            @Override
            public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                lp.width = getWidth() - (getWidth()/4);
                return true;
            }
        });
        articleRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));
        new GravitySnapHelper(Gravity.START).attachToRecyclerView(articleRecyclerView);

        dotAdapter = new BasicActRecyclerAdapter<>(this, DotHolder.class, new ArrayList<>());
        dotAdapter.initRecyclerView(dotRecyclerView, new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        dotRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(8));
        bannerViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                DashboardActivity.this.bannerPosition = position;
                dotAdapter.notifyDataSetChanged();
            }
        });
        waitingTaskCardView.setOnClickListener(v -> {
            startActivity(TaskListActivity.class, gson.toJson(new TaskCategoryDto()));
        });
        currentTaskCardView.setOnClickListener(v -> {
            startActivity(TaskApplicationActivity.class);
        });
    }

    @Override
    public void initPreference() {
        super.initPreference();
        String bannerDtoGson = FastSave.getInstance().getString(Constant.BANNER, "[]");
        bannerDtos = gson.fromJson(bannerDtoGson, new TypeToken<List<BannerDto>>(){}.getType());
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchTaskCategories();
        fetchTasks();
        fetchArticles();
        fetchBanner();
        runBanner();
        fetchUserTaskSummary();
        refreshDevice();
        refreshUser();
    }

    @Override
    public void renderData(){
        if(StringUtils.isNotEmpty(user.getPhotoUrl())){
//            GlideApp.with(this).load(user.getPhotoUrl()).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);
            GlideApp.with(this)
                    .load(user.getPhotoUrl()).circleCrop().error(R.drawable.ic_user_transparent)
                    .apply(new RequestOptions().set(HttpGlideUrlLoader.TIMEOUT, 30000)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)).into(profileImageView);


        }
        greetingTextView.setText(getString(R.string.hello_name, user.getFullname()));
        balanceTextView.setText(NumberUtil.moneyFormat(user.getCurrentBalance(), true));
        totalIncomeTextView.setText(NumberUtil.moneyFormat(user.getTotalIncome(), true));
        totalFinishedTextView.setText(String.valueOf(user.getTotalCompletedTask()));

        bannerViewPager.setAdapter(BannerPagerAdapter.instance(this, bannerDtos));
        bannerViewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        dotAdapter.setObjects(Dot.create(bannerDtos.size()));
        onGoingTaskTextView.setText(getString(R.string.variable_count_task, String.valueOf(userTaskSummary.getOnGoingTask())));
        waitingTaskTextView.setText(getString(R.string.variable_count_task, String.valueOf(userTaskSummary.getTotalTask())));
    }

    Timer timer ;
    public void runBanner(){
        if(timer==null){
            timer = new Timer();
        }else{
            timer.cancel();
            timer = new Timer();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(bannerDtos.size()>0){
                    if(bannerViewPager.getCurrentItem()<bannerDtos.size()-1){
                        runOnUiThread(() -> {
                            bannerViewPager.setCurrentItem(bannerViewPager.getCurrentItem()+1, true);
                        });
                    }else{
                        runOnUiThread(() -> {
                            bannerViewPager.setCurrentItem(0);
                        });
                    }
                }
            }
        }, 3000, 3000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer!=null){
            try{
                timer.cancel();
            }catch (Exception e){

            }
        }
    }

    @OnClick({R.id.see_all_article_tv, R.id.profile_iv_layout})
    public void onClick(View view){
        if(view.getId()==R.id.see_all_article_tv){
            startActivity(WebViewActivity.class, Constant.BLOG_WEB);
        }
        if(view.getId()==R.id.profile_iv_layout){
            startActivity(AccountActivity.class);
        }
    }

    void fetchTasks(){
        grabServerData(Kitchen.arange(this, TaskApi.class)
                .published(currentAuthorization, "", "",  null, null, null, null, null,null,null,0,false, "jt.updated"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                taskDtos = new ArrayList<>();
                taskDtos.addAll(wsResponse.getData());
                taskAdapter.setObjects(taskDtos);
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    void fetchUserTaskSummary(){
        grabServerData(Kitchen.arange(this, UserApi.class)
                .taskSummary(currentAuthorization), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                this.userTaskSummary = wsResponse.getData();
                onGoingTaskTextView.setText(getString(R.string.variable_count_task, String.valueOf(userTaskSummary.getOnGoingTask())));
                waitingTaskTextView.setText(getString(R.string.variable_count_task, String.valueOf(userTaskSummary.getTotalTask())));
                if(this.userTaskSummary.getOnGoingTask()<=0){
                    onGoingTaskDescriptionTextView.setText(R.string.ThereIsNoTaskThatYouNeedToComplete);
                }else{
                    onGoingTaskDescriptionTextView.setText(R.string.ThereIsATaskThatYouNeedToComplete);
                }
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


    void fetchTaskCategories(){
        grabServerData(Kitchen.arange(this, TaskCategoryApi.class)
                .taskCategories( 0,true, "sortir"), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                FastSave.getInstance().saveString(Constant.TASK_CATEGORIES, gson.toJson(wsResponse.getData()));
                taskCategoryDtos = new ArrayList<>();
                taskCategoryDtos.addAll(wsResponse.getData());
                categoryAdapter.setObjects(taskCategoryDtos);
            }else{

            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    void fetchArticles(){
        grabServerData(Kitchen.arange(this, ArticleApi.class, Constant.BLOG_DOMAIN)
                .get( 1,4), (responseArticles, response) -> {
            try{
                this.articles = new ArrayList<>();
                this.articles.addAll(responseArticles);
                articleAdapter.setObjects(this.articles);
            }catch (Exception e){
                e.printStackTrace();
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    void fetchBanner(){
        grabServerData(Kitchen.arange(this, BannerApi.class).banners(true, true, "sortir"), wsResponse -> {
            if(wsResponse.isSuccess()){
                bannerDtos = wsResponse.getData();
                bannerViewPager.setAdapter(BannerPagerAdapter.instance(this, bannerDtos));
                dotAdapter.setObjects(Dot.create(bannerDtos.size()));
                FastSave.getInstance().saveString(Constant.BANNER, gson.toJson(bannerDtos));
            }
        }, throwable -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


    @Override
    public int getPosition() {
        return bannerPosition;
    }
}
