package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.appizona.yehiahd.fastsave.FastSave;
import com.google.gson.reflect.TypeToken;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.apache.commons.lang.time.DateFormatUtils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.notification.NotificationActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.api.TaskApplicationApi;
import id.catchadeal.kerjayuk.api.WithdrawalRequestApi;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.holder.TaskApplicationHolder;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.misc.TaskFilter;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.EndlessRecyclerViewScrollListener;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.view.TaskFilterDialogFragment;
import id.catchadeal.kerjayuk.view.TaskSortirDialogFragment;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class TaskApplicationActivity extends BottomNavigationActivity {

    public static final String TAG = TaskApplicationActivity.class.getName();

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;

    @BindView(R.id.available_balance_tv) TextView availableBalanceTextView ;
    @BindView(R.id.total_income_tv) TextView totalIncomeTextView ;
    @BindView(R.id.total_task_tv) TextView totalTaskTextView ;
    @BindView(R.id.withdrawal_tv) TextView withdrawalRequestTextView ;

    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener ;


    TaskCategoryDto taskCategoryDto ;

    SwipyRefreshLayoutDirection direction ;

    BasicActRecyclerAdapter<TaskApplicationDto,TaskApplicationActivity, TaskApplicationHolder> adapter ;
    List<TaskApplicationDto> taskDtos = new ArrayList<>();

    TaskFilterDialogFragment taskFilterDialogFragment;
    TaskSortirDialogFragment taskSortirDialogFragment ;

    TaskApplicationSortir taskApplicationSortir  = new TaskApplicationSortir("Terbaru", "ja.updated", false);

    static List<TaskApplicationSortir> taskApplicationSortirs = new ArrayList<>();

    TaskFilter taskFilter = null ;
    List<TaskApplicationStatusDto> taskApplicationStatuses = new ArrayList<>();

    boolean requestWithdrawalExist = true ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_application;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int bottomMenu() {
        return R.id.history;
    }

    @Override
    public void focusBar() {
        ((BottomNavigationItemView)((BottomNavigationMenuView)bottomNavigationView.getChildAt(0)).getChildAt(2)).setBackgroundColor(getColor(R.color.greenTertiary));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        adapter = new BasicActRecyclerAdapter<>(this, TaskApplicationHolder.class, taskDtos);
        adapter.initRecyclerView(recyclerView, layoutManager);

        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.VERTICAL));

        this.page = 0 ;
        swipeRefreshLayout.setOnRefreshListener(() -> {
            this.page = 0 ;
            fetchTaskApplications();
        });
        this.endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                TaskApplicationActivity.this.page = page;
                fetchTaskApplications();
            }
        };

        Type type = new TypeToken<List<TaskApplicationStatusDto>>(){}.getType();
        String gsonString = FastSave.getInstance().getString(Constant.TASK_APPLICATION_STATUSES, "[]");
        this.taskApplicationStatuses = getGson().fromJson(gsonString, type);
        this.taskFilter = new TaskFilter(taskCategoryDto, taskApplicationStatuses.get(0), new BigDecimal(0), new BigDecimal(10000000), null, null);
    }

    @Override
    public void renderData() {
        availableBalanceTextView.setText(NumberUtil.moneyFormat(user.getCurrentBalance(), true));
        totalIncomeTextView.setText(NumberUtil.moneyFormat(user.getTotalIncome(), true));
        totalTaskTextView.setText(String.valueOf(user.getTotalCompletedTask()));
        renderWithdrawal();
        checkIfWithdrawalRequestExist();
    }
    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.setRefreshing(true);
        fetchTaskApplications();
        refreshUser();

    }

    void renderWithdrawal(){
        if(requestWithdrawalExist){
            withdrawalRequestTextView.setVisibility(View.GONE);
        }else{
            if(user.getCurrentBalance().compareTo(new BigDecimal(0))<=0){
                withdrawalRequestTextView.setVisibility(View.GONE);
            }else{
                withdrawalRequestTextView.setVisibility(View.VISIBLE);
            }
        }
    }


    @OnClick({R.id.filter_layout,R.id.sortir_layout, R.id.withdrawal_tv})
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.filter_layout){
            if(taskFilterDialogFragment ==null){
                Type type = new TypeToken<List<TaskApplicationStatusDto>>(){}.getType();
                String gsonString = FastSave.getInstance().getString(Constant.TASK_APPLICATION_STATUSES, "[]");
                List<TaskApplicationStatusDto> taskApplicationStatuses = getGson().fromJson(gsonString, type);
                TaskFilter taskFilter = new TaskFilter(taskCategoryDtos.get(0), taskApplicationStatuses.get(0), new BigDecimal(0), new BigDecimal(10000000), null, null);
                taskFilterDialogFragment = TaskFilterDialogFragment.instance(taskApplicationStatuses, taskCategoryDtos, taskFilter);
            }
            taskFilterDialogFragment.show(getSupportFragmentManager(), TaskFilterDialogFragment.TAG, taskFilter -> {
                this.taskFilter = taskFilter ;
                this.page = 0 ;
                swipeRefreshLayout.setRefreshing(true);
                fetchTaskApplications();
            });
        }
        if(view.getId()==R.id.sortir_layout){
            if(taskSortirDialogFragment ==null){
                taskSortirDialogFragment = TaskSortirDialogFragment.instance(taskApplicationSortirs, taskApplicationSortir);
            }
            taskSortirDialogFragment.show(getSupportFragmentManager(), TaskSortirDialogFragment.TAG, taskApplicationSortir -> {
                if(taskApplicationSortir!=null){
                    this.taskApplicationSortir = taskApplicationSortir ;
                }else{
                    this.taskApplicationSortir = new TaskApplicationSortir("Terbaru", "ja.updated", false);
                }
                this.page = 0 ;
                swipeRefreshLayout.setRefreshing(true);
                fetchTaskApplications();
            });
        }
        if(view.getId()==R.id.withdrawal_tv){
            if(user.getCurrentBalance().compareTo(new BigDecimal(0))<=0){
                infoDialog(getString(R.string.YouDontHaveBalance), DIALOG_WARNING, () -> true);
                return;
            }
            if(user.getCurrentBalance().compareTo(configuration.getMinWithdrawal())<=0){
                infoDialog(getString(R.string.MinimumBalanceRequestIsVariable, NumberUtil.moneyFormat(configuration.getMinWithdrawal(), true)), DIALOG_WARNING, () -> true);
                return;
            }
            confirmDialog(getString(R.string.RequestWithdrawal),
                    getString(R.string.DouYouWantToRequestWithdrawalOfAmountVariable, NumberUtil.moneyFormat(user.getCurrentBalance(), true)), CONFIRM_WITHDRAWAL, aBoolean -> {
                        if(aBoolean){
                            ViewProgress.show(this);
                            grabServerData(Kitchen.arange(this, WithdrawalRequestApi.class).request(currentAuthorization, user.getCurrentBalance()), (wsResponse, response) -> {
                                ViewProgress.dismiss();
                                if(wsResponse.isSuccess()){
                                    infoDialog(getString(R.string.WithdrawalRequestSucceed), DIALOG_SUCCESS, () -> {
                                        checkIfWithdrawalRequestExist();
                                        return true ;
                                    });
                                }else{
                                    infoDialog(wsResponse.getMessage(), DIALOG_WARNING, () -> true);
                                }
                            }, (aBoolean1, throwable, response) -> {
                                ViewProgress.dismiss();
                                showAlert(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, throwable.getMessage()));
                            });
                        }else{

                        }
                        return true ;
                    });
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK && taskFilterDialogFragment !=null && taskFilterDialogFragment.isShowing()){
            taskFilterDialogFragment.dismiss();
            return false;
        }
        return super.onKeyDown(keyCode, event);

    }

    void fetchTaskApplications(){
        Long taskCategoryId = taskFilter.getTaskCategory()!=null?taskFilter.getTaskCategory().getId():null;
        String startDate = taskFilter.getStartDate()!=null?DateFormatUtils.format(taskFilter.getStartDate(), DateAppConfig.API_DATE):null;
        String endDate = taskFilter.getStartDate()!=null?DateFormatUtils.format(taskFilter.getEndDate(), DateAppConfig.API_DATE):null;
        Long taskApplicationStatusId = null ;
        if(taskFilter.getTaskApplicationStatus()!=null){
            if(!taskFilter.getTaskApplicationStatus().getId().equals(999l)){
                taskApplicationStatusId = taskFilter.getTaskApplicationStatus().getId();
            }
        }


        grabServerData(Kitchen.arange(this, TaskApplicationApi.class)
                .taskApplications(currentAuthorization, user.getId(), null, null, true, taskCategoryId, taskFilter.getMinFee(), taskFilter.getMaxFee(),
                        startDate, endDate, taskApplicationStatusId,  this.page, taskApplicationSortir.getAscending(), taskApplicationSortir.getSortBy()),
                (wsResponse, response) -> {
                    swipeRefreshLayout.setRefreshing(false);
                    if(wsResponse.isSuccess()){
                        if(this.page<=0){
                            this.taskDtos  = new ArrayList<>();
                            this.taskDtos.addAll(wsResponse.getData());
                            adapter.setObjects(this.taskDtos);
                        }else{
                            this.taskDtos.addAll(wsResponse.getData());
                            adapter.setObjects(this.taskDtos);
                        }
                        if(this.taskDtos.size()==0){
                            TextView textView = recyclerView.getEmptyView().findViewById(R.id.message_tv);
                            textView.setText(R.string.ThereIsNoTaskHistoryLetsGetAJobNow);
                            textView.setOnClickListener(v -> {
                                startActivity(TaskListActivity.class, gson.toJson(new TaskCategoryDto()));
                            });
                        }

                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                });
    }


    void checkIfWithdrawalRequestExist(){
        grabServerData(Kitchen.arange(this, WithdrawalRequestApi.class).checkIfRequestExist(currentAuthorization), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                this.requestWithdrawalExist = wsResponse.getData();
                renderWithdrawal();
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            showAlert(getString(R.string.Failed), throwable.getMessage());
        });
    }



    static {
        taskApplicationSortirs = new ArrayList<>();
        taskApplicationSortirs.add(new TaskApplicationSortir("Terbaru", "ja.updated", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Terlama", "ja.updated", true));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Tertinggi", "jt.fee", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Terendah", "jt.fee", true));
    }

}
