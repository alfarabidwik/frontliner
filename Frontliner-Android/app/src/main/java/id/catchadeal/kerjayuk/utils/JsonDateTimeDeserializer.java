package id.catchadeal.kerjayuk.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class JsonDateTimeDeserializer implements JsonDeserializer {
    SimpleDateFormat format = new SimpleDateFormat(DateAppConfig.API_DATE_FORMAT);
    SimpleDateFormat format2 = new SimpleDateFormat(DateAppConfig.BLOG_DATE_FORMAT);

    @Override
    public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        String date = jsonElement.getAsString();
        Object object = null ;
        try {
            if(StringUtils.isEmpty(date)){
                return null ;
            }
            object =  format.parse(date);
        } catch (ParseException e) {
            return null ;
        }
        if(object!=null){
            return object ;
        }
        try {
            if(StringUtils.isEmpty(date)){
                return null ;
            }
            object =  format.parse(date);
        } catch (ParseException e) {
            return null ;
        }
        return object ;
    }
}
