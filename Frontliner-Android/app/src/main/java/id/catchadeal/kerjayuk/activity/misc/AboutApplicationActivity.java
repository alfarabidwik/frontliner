package id.catchadeal.kerjayuk.activity.misc;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.utils.Utils;

public class AboutApplicationActivity extends BaseActivity {


    @BindView(R.id.description_html_tv) HtmlTextView descriptionTextView ;
    @BindView(R.id.version_tv) TextView versionTextView ;

    @Override
    public void renderData() {

    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_about_application;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionTextView.setHtml(Utils.readTextAssetFile(this, "about_application.html"));
        versionTextView.setText(getString(R.string.VersionVariable, getBuildConfigVersionName()));

    }

    protected String getBuildConfigVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }


}
