package id.catchadeal.kerjayuk.api;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SignAuthApi {

    @POST(Constant.BASE_API+"/sign/inOrUp")
    Observable<WSResponse<UserDto, String>> inOrUp(@Query("mobilePhone") String mobilePhone, @Query("deviceId") String deviceId, @Query("fcmToken") String fcmToken, @Query("platform") String platform);

    @POST(Constant.BASE_API+"/signout")
    Observable<WSResponse> signout(@Header(Constant.HEADER_AUTHORIZATION) String authorization, @Query("deviceId") String deviceId);

    @POST(Constant.BASE_API+"/sign/inEmail")
    Observable<WSResponse<UserDto, String>> inEmail(@Query("email") String email, @Query("password") String password, @Query("deviceId") String deviceId, @Query("fcmToken") String fcmToken, @Query("platform") String platform);

    @POST(Constant.BASE_API+"/sign/authEmail")
    Observable<WSResponse<UserDto, String>> authEmail(@Query("email") String email, @Query("password") String password);

    @POST(Constant.BASE_API+"/sign/commitRegistration")
    Observable<WSResponse<UserDto, String>> commitRegistration(
            @Query("registrationToken") String registrationToken,
            @Query("agreeTermAndCondition") Boolean agreeTermAndCondition, @Query("email") String email, @Query("fullname") String fullname, @Query("referralCode") String referralCode);

    @POST(Constant.BASE_API+"/sign/applyMobilePhoneAndLogin")
    Observable<WSResponse<UserDto, String>> applyMobilePhoneAndLogin(@Query("authorization") String authorization, @Query("mobilePhone") String mobilePhone, @Query("deviceId") String deviceId, @Query("fcmToken") String fcmToken, @Query("platform") String platform);


}
