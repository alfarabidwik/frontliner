package id.catchadeal.kerjayuk.activity.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.apache.commons.lang.time.DateFormatUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.notification.NotificationAndSummaryActivity;
import id.catchadeal.kerjayuk.api.TaskApi;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.holder.HorizontalTaskHolder;
import id.catchadeal.kerjayuk.holder.TaskListHolder;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.model.misc.TaskFilter;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import id.catchadeal.kerjayuk.utils.EndlessRecyclerViewScrollListener;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import id.catchadeal.kerjayuk.view.TaskFilterDialogFragment;
import id.catchadeal.kerjayuk.view.TaskSortirDialogFragment;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class TaskListActivity extends BaseActivity {

    public static final String TAG = TaskListActivity.class.getName();

    @BindView(R.id.task_info_tv) TextView taskInfoTextView ;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.best_task_recyclerview) AlfaRecyclerView bestTaskRecyclerView ;
    private BasicActRecyclerAdapter<TaskDto, TaskListActivity, HorizontalTaskHolder> bestTaskAdapter ;

    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener ;

    @BindExtra String taskCategoryDtoGson ;TaskCategoryDto taskCategoryDto ;

    List<TaskDto> taskDtos = new ArrayList<>();
    BasicActRecyclerAdapter<TaskDto, TaskListActivity, TaskListHolder> adapter ;


    TaskFilterDialogFragment taskFilterDialogFragment;
    TaskSortirDialogFragment taskSortirDialogFragment ;

    TaskApplicationSortir taskApplicationSortir  = new TaskApplicationSortir("Terbaru", "jt.updated", false);

    TaskFilter taskFilter = null ;

    private WSResponse wsResponse = WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.SUCCESS_CODE, id.catchadeal.kerjayuk.util.Constant.SUCCESS);

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskCategoryDto = gson.fromJson(taskCategoryDtoGson, TaskCategoryDto.class);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.taskDtos = new ArrayList<>();
        adapter = new BasicActRecyclerAdapter<>(this, TaskListHolder.class, taskDtos);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        adapter.initRecyclerView(recyclerView, layoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));

        bestTaskAdapter = new BasicActRecyclerAdapter<>(this, HorizontalTaskHolder.class, new ArrayList<>());
        bestTaskAdapter.initRecyclerView(bestTaskRecyclerView,  new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false){
            @Override
            public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                lp.width = getWidth() - (getWidth()/5);
                return true;
            }
        });
        bestTaskRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));
        new GravitySnapHelper(Gravity.START).attachToRecyclerView(bestTaskRecyclerView);

        this.page = 0 ;
        this.endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                TaskListActivity.this.page = page;
                fetchTasks();
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            this.page = 0 ;
            fetchTasks();
        });

        this.taskFilter = new TaskFilter(taskCategoryDto, null, new BigDecimal(0), new BigDecimal(10000000), null, null);

    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewProgress.show(this);
        swipeRefreshLayout.setRefreshing(true);
        fetchTasks();
        fetchTaskSuggestion();
    }


    @Override
    public void renderData() {
        taskInfoTextView.setText(getString(R.string.ThereIsSumOfJobWhichYouCanChoose, String.valueOf(wsResponse.getTotalElement())));
    }

    @OnClick({R.id.filter_layout,R.id.sortir_layout})
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.filter_layout){
            if(taskFilterDialogFragment ==null){
                taskFilterDialogFragment = TaskFilterDialogFragment.instance(new ArrayList<>(), taskCategoryDtos, taskFilter);
            }
            taskFilterDialogFragment.show(getSupportFragmentManager(), TaskFilterDialogFragment.TAG, taskFilter -> {
                this.taskFilter = taskFilter ;
                this.page = 0 ;
                swipeRefreshLayout.setRefreshing(true);
                fetchTasks();
                fetchTaskSuggestion();
            });
        }
        if(view.getId()==R.id.sortir_layout){
            if(taskSortirDialogFragment ==null){
                taskSortirDialogFragment = TaskSortirDialogFragment.instance(taskApplicationSortirs, taskApplicationSortir);
            }
            taskSortirDialogFragment.show(getSupportFragmentManager(), TaskSortirDialogFragment.TAG, taskApplicationSortir -> {
                if(taskApplicationSortir!=null){
                    this.taskApplicationSortir = taskApplicationSortir ;
                }else{
                    this.taskApplicationSortir = new TaskApplicationSortir("Terbaru", "jt.updated", false);
                }
                this.page = 0 ;
                swipeRefreshLayout.setRefreshing(true);
                fetchTasks();
                fetchTaskSuggestion();
            });
        }
    }


    void fetchTasks(){
        if(taskFilter.getTaskCategory()!=null && taskFilter.getTaskCategory().getId()!=null){
            getSupportActionBar().setTitle(getString(R.string.TaskOfCategory, taskFilter.getTaskCategory().getName()));
        }else{
            getSupportActionBar().setTitle(getString(R.string.AllTask));
        }

        Long taskCategoryId = taskFilter.getTaskCategory()!=null?taskFilter.getTaskCategory().getId():null;
        String startDate = taskFilter.getStartDate()!=null?DateFormatUtils.format(taskFilter.getStartDate(), DateAppConfig.API_DATE):null;
        String endDate = taskFilter.getEndDate()!=null?DateFormatUtils.format(taskFilter.getEndDate(), DateAppConfig.API_DATE):null;

        grabServerData(Kitchen.arange(this, TaskApi.class)
                .published(currentAuthorization, "", "",  taskCategoryId, taskFilter.getMinFee(), taskFilter.getMaxFee(), startDate, endDate,
                        null, null, this.page, taskApplicationSortir.getAscending(), taskApplicationSortir.getSortBy()), (wsResponse, response) -> {
            ViewProgress.dismiss();
            swipeRefreshLayout.setRefreshing(false);
            if(wsResponse.isSuccess()){
                this.wsResponse = wsResponse;
                renderData();
                if(this.page==0){
                    taskDtos = new ArrayList<>();
                }
                taskDtos.addAll(wsResponse.getData());
                adapter.setObjects(taskDtos);
                if(taskDtos.size()==0){
                    TextView textView = recyclerView.getEmptyView().findViewById(R.id.message_tv);
                    textView.setText(R.string.ThereIsNoAvailableTask);
                }
                recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
            }
        }, (aBoolean, throwable, response) -> {
            ViewProgress.dismiss();
            swipeRefreshLayout.setRefreshing(false);
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        });
    }


    void fetchTaskSuggestion(){
        Long taskCategoryId = taskFilter.getTaskCategory()!=null?taskFilter.getTaskCategory().getId():null;
        String startDate = taskFilter.getStartDate()!=null?DateFormatUtils.format(taskFilter.getStartDate(), DateAppConfig.API_DATE):null;
        String endDate = taskFilter.getEndDate()!=null?DateFormatUtils.format(taskFilter.getEndDate(), DateAppConfig.API_DATE):null;

        grabServerData(Kitchen.arange(this, TaskApi.class)
                .published(currentAuthorization, "", "",  taskCategoryId, taskFilter.getMinFee(), taskFilter.getMaxFee(), startDate, endDate,
                        null, true, 0, taskApplicationSortir.getAscending(), taskApplicationSortir.getSortBy()), (wsResponse, response) -> {
            if(wsResponse.isSuccess()){
                bestTaskAdapter.setObjects(wsResponse.getData());
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


    static List<TaskApplicationSortir> taskApplicationSortirs = new ArrayList<>();

    static {
        taskApplicationSortirs = new ArrayList<>();
        taskApplicationSortirs.add(new TaskApplicationSortir("Terbaru", "jt.updated", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Terlama", "jt.updated", true));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Tertinggi", "jt.fee", false));
        taskApplicationSortirs.add(new TaskApplicationSortir("Insentif Terendah", "jt.fee", true));
    }




}
