package id.catchadeal.kerjayuk.holder;

import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import id.catchadeal.kerjayuk.view.TaskFilterDialogFragment;
import id.catchadeal.kerjayuk.view.TaskSortirDialogFragment;

public class TaskApplicationSortirHolder extends SimpleViewHolder<TaskSortirDialogFragment, TaskApplicationSortir, String> {

    @BindView(R.id.label_tv) TextView labelTextView ;
    @BindView(R.id.checkbox) CheckBox checkBox ;
    @BindView(R.id.linear_layout) LinearLayout linearLayout ;

    public TaskApplicationSortirHolder(TaskSortirDialogFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.holder_task_application_sortir, viewGroup);
    }

    @Override
    public void showData(TaskApplicationSortir object) {
        super.showData(object);
        labelTextView.setText(object.getLabel());
        linearLayout.setOnClickListener(v -> {
            select(object);
        });
        checkBox.setOnClickListener(v -> {
            select(object);
        });
        if(getFragment().getTaskApplicationSortir()!=null &&
                (getFragment().getTaskApplicationSortir().getSortBy().equalsIgnoreCase(object.getSortBy()) && getFragment().getTaskApplicationSortir().getAscending().equals(object.getAscending()))){
            checkBox.setChecked(true);
        }else{
            checkBox.setChecked(false);
        }
    }

    void select(TaskApplicationSortir object){
        getFragment().setTaskApplicationSortir(object);
        getFragment().getAdapter().notifyDataSetChanged();
    }


    @Override
    public void find(String findParam) {

    }
}
