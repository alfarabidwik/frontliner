package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;

import java.util.Set;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.WelcomeActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.activity.user.TaskApplicationDetailActivity;
import id.catchadeal.kerjayuk.model.Dot;
import id.catchadeal.kerjayuk.model.business.PVTaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.SliderControl;

public class TaskApplicationStatusHolder extends ACTViewHolder<TaskApplicationDetailActivity, TaskApplicationStatusDto, String> {

    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.panel_status_frame) FrameLayout panelStatusLayout ;
    @BindView(R.id.left_line) View leftLineView ;
    @BindView(R.id.container_layout) FrameLayout containerLayout ;

    public TaskApplicationStatusHolder(TaskApplicationDetailActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_task_application_status, viewGroup);
    }

    @Override
    public void showData(TaskApplicationStatusDto taskApplicationStatusDto) {
        super.showData(taskApplicationStatusDto);
        if(taskApplicationStatusDto.getId().equals(TaskApplicationStatusDto.REJECTED_ID)){
            if(getAct().getTaskApplicationDto().getTaskApplicationStatus().equals(TaskApplicationStatusDto.REJECTED_ID)){
                containerLayout.setVisibility(View.VISIBLE);
            }else{
                containerLayout.setVisibility(View.GONE);
            }
        }
        if(taskApplicationStatusDto.getId().equals(TaskApplicationStatusDto.ACCEPTED_ID)){
            if(getAct().getTaskApplicationDto().getTaskApplicationStatus().equals(TaskApplicationStatusDto.REJECTED_ID)){
                containerLayout.setVisibility(View.GONE);
            }
        }

        Set<PVTaskApplicationStatusDto> pvTaskApplicationStatusDtos = getAct().getTaskApplicationDto().getPvTaskApplicationStatuses();
        if(pvTaskApplicationStatusDtos!=null){
            for (PVTaskApplicationStatusDto pvTaskApplicationStatusDto : pvTaskApplicationStatusDtos) {
                if(pvTaskApplicationStatusDto.getTaskApplicationStatus().getId().equals(taskApplicationStatusDto.getId())){
                    panelStatusLayout.setBackgroundResource(R.drawable.badge_task_application_status_green);
                    leftLineView.setBackgroundColor(getAct().getResources().getColor(R.color.green_700));
                    imageView.setImageResource(CommonView.taskApplicationStatusPassedIcon.get(taskApplicationStatusDto.getId()));
                    if(taskApplicationStatusDto.getId().equals(TaskApplicationStatusDto.REJECTED_ID)){
                        panelStatusLayout.setBackgroundResource(R.drawable.badge_task_application_status_red);
                        leftLineView.setBackgroundColor(getAct().getResources().getColor(R.color.red_700));
                    }
                    break;
                }else{
                    imageView.setImageResource(CommonView.taskApplicationStatusNotPassedIcon.get(taskApplicationStatusDto.getId()));
                    panelStatusLayout.setBackgroundResource(R.drawable.badge_task_application_status_grey);
                    leftLineView.setBackgroundColor(getAct().getResources().getColor(R.color.greyLine));
                }
            }
            if(getAdapterPosition()>0){
                leftLineView.setVisibility(View.VISIBLE);
            }else{
                leftLineView.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void find(String findParam) {

    }
}
