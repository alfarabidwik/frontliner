package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.InputTools;
import com.hendraanggrian.bundler.annotations.BindExtra;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class ResetPasswordActivity extends BaseActivity {

    public static final String TAG = ResetPasswordActivity.class.getName();

    @BindView(R.id.password_et) EditText passwordEditText ;
    @BindView(R.id.confirm_password_et) EditText confirmPasswordEditText ;

    @BindExtra String token ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_reset_password;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void renderData() {

    }

    @OnClick(R.id.submit_button)
    public void onClick(){
        String passsword = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();
        if(InputTools.isComplete(false, passwordEditText, confirmPasswordEditText)){
            if(passsword.equals(confirmPassword)){
                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, UserApi.class).resetPassword(token, passsword), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    if(wsResponse.isSuccess()){
                        Intent intent = getIntent();
                        intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(wsResponse));
                        setCurrentResult(Constant.RESULT_FINISH, intent);
                    }else{
                        Intent intent = getIntent();
                        intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(wsResponse));
                        setCurrentResult(Constant.RESULT_FINISH, intent);
                    }
                }, (aBoolean, throwable, response) -> {
                    ViewProgress.dismiss();
                    showAlert(getString(R.string.error), throwable.getMessage());
                });
            }else{
                showTooltip(confirmPasswordEditText, getString(R.string.PasswordAndConfirmPasswordIsNotMatch));
            }

        }

    }

}
