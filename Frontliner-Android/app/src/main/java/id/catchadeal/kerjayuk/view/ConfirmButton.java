package id.catchadeal.kerjayuk.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class ConfirmButton extends Button {

    public ConfirmButton(Context context) {
        super(context);
    }

    public ConfirmButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if(enabled){
            setAlpha(1.0f);
        }else{
            setAlpha(0.2f);
        }
    }
}
