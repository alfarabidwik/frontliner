package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.request.RequestOptions;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.holder.AccountMenuHolder;
import id.catchadeal.kerjayuk.model.misc.AccountMenu;
import id.catchadeal.kerjayuk.model.region.CityDto;
import id.catchadeal.kerjayuk.model.region.DistrictDto;
import id.catchadeal.kerjayuk.model.region.ProvinceDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.BottomNavigationViewHelper;

public class AccountActivity extends BottomNavigationActivity {

    public static final String TAG = AccountActivity.class.getName();

    @BindView(R.id.profile_iv) CircleImageView profileImageView ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;

    @BindView(R.id.name_tv) TextView nameTextView ;
    @BindView(R.id.address_tv) TextView addressTextView ;
    @BindView(R.id.email_tv) TextView emailTextView ;
    @BindView(R.id.mobile_phone_tv) TextView mobilePhoneTextView ;
    @BindView(R.id.verified_status_tv) TextView verificationStatusTextView ;


    private BasicActRecyclerAdapter<AccountMenu, AccountActivity, AccountMenuHolder> adapter ;


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_account;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public int bottomMenu() {
        return R.id.account;
    }

    @Override
    public void focusBar() {
        ((BottomNavigationItemView)((BottomNavigationMenuView)bottomNavigationView.getChildAt(0)).getChildAt(3)).setBackgroundColor(getColor(R.color.greenTertiary));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new BasicActRecyclerAdapter<>(this, AccountMenuHolder.class, AccountMenu.menus());
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(this));
        refreshUser();
    }

    @Override
    public void renderData() {
        if(StringUtils.isNotEmpty(user.getPhotoUrl())){
            GlideApp.with(this)
                    .load(user.getPhotoUrl())
                    .apply(new RequestOptions().set(HttpGlideUrlLoader.TIMEOUT, 30000))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_user_transparent).into(profileImageView);

        }
        nameTextView.setText(user.getFullname());
        if(user.getVillage()!=null){
            VillageDto villageDto = user.getVillage();
            DistrictDto districtDto = villageDto!=null?villageDto.getDistrict():new DistrictDto();
            CityDto cityDto  = districtDto!=null?districtDto.getCity():new CityDto();
            ProvinceDto provinceDto = cityDto!=null?cityDto.getProvince():new ProvinceDto();
            addressTextView.setText(StringUtils.defaultString(cityDto.getName(), " "));
        }
        emailTextView.setText(StringUtils.defaultString(user.getEmail(), "-"));
        mobilePhoneTextView.setText(user.getMobilePhone());
        if(user.getVerificationStatus().equalsIgnoreCase(UserDto.VERIFIED)){
            verificationStatusTextView.setText(R.string.Verified);
            verificationStatusTextView.setTextColor(getColor(R.color.green_700));
            verificationStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(getDrawable(R.drawable.ic_shield_check),null, null, null);
        }else{
            verificationStatusTextView.setText(R.string.NotVerified);
            verificationStatusTextView.setTextColor(getColor(R.color.red_700));
            verificationStatusTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(getDrawable(R.drawable.ic_shield_uncheck),null, null, null);
        }
    }


    @OnClick({R.id.configuration_layout, R.id.logout_layout, R.id.profile_iv})
    @Override
    public void onClick(View view){
        if(view.getId()==R.id.configuration_layout){
            startActivity(AccountConfigurationActivity.class);
        }
        if(view.getId()==R.id.logout_layout){
            logoutConfirmDialog();
        }
        if(view.getId()==R.id.profile_iv){
            startActivity(UserPhotoProfileActivity.class);
        }
    }



}
