package id.catchadeal.kerjayuk.activity.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicActRecyclerAdapter;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.api.NotificationUserApi;
import id.catchadeal.kerjayuk.holder.NotificationUserHolder;
import id.catchadeal.kerjayuk.interfaze.NotificationTopicRecount;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.NotificationUserDto;
import id.catchadeal.kerjayuk.utils.EndlessRecyclerViewScrollListener;
import id.catchadeal.kerjayuk.utils.EqualSpacingItemDecoration;
import lombok.Getter;

public class NotificationActivity extends BaseActivity implements NotificationTopicRecount {

    public static final String TAG = NotificationActivity.class.getName();

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.unread_notification_tv) TextView unreadNotificationTextView ;
    @BindView(R.id.total_notification_tv) TextView totalNotificationTextView;

    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener ;

    @BindExtra String title ;
    @BindExtra String topics ;
    @BindExtra String types ;

    @Getter List<NotificationUserDto> notifications = new ArrayList<>();
    @Getter BasicActRecyclerAdapter<NotificationUserDto, NotificationActivity, NotificationUserHolder> adapter ;

    public static NotificationTopicRecount notificationTopicRecount ;

    WSResponse wsResponse = null ;

    Long unreadCount = 0l ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_notification;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.notifications = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        adapter = new BasicActRecyclerAdapter<>(this, NotificationUserHolder.class, notifications);
        adapter.initRecyclerView(recyclerView, layoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(0, EqualSpacingItemDecoration.HORIZONTAL));
        wsResponse =  WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.SUCCESS_CODE, id.catchadeal.kerjayuk.util.Constant.SUCCESS);
        this.page = 0 ;
        swipeRefreshLayout.setOnRefreshListener(() -> {
            this.page = 0 ;
            fetchNotifications();
        });
        this.endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                NotificationActivity.this.page = page;
                fetchNotifications();
            }
        };
        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        unreadNotificationTextView.setText(getString(R.string.UnreadCount, String.valueOf(unreadCount)));
        totalNotificationTextView.setText(getString(R.string.VariableCountNotification, String.valueOf(wsResponse.getTotalElement())));
        notificationTopicRecount = this ;
    }

    @Override
    public void renderData() {
        countUnreadNotification();
        swipeRefreshLayout.setRefreshing(true);
        fetchNotifications();
    }

    void fetchNotifications(){
        grabServerData(Kitchen.arange(this, NotificationUserApi.class).notifications(currentAuthorization, topics, types, null, null, page, false, "created"),
                (wsResponse, response) -> {
                    if(wsResponse.isSuccess()){
                        this.wsResponse = wsResponse ;
                        if(this.page==0){
                            this.notifications = new ArrayList<>();
                        }
                        this.notifications.addAll(wsResponse.getData());
                        this.adapter.setObjects(this.notifications);
                        if(this.notifications.size()==0){
                            TextView textView = recyclerView.getEmptyView().findViewById(R.id.message_tv);
                            textView.setText(R.string.ThereIsNoAvailableNotification);
                        }
                        totalNotificationTextView.setText(getString(R.string.VariableCountNotification, String.valueOf(wsResponse.getTotalElement())));
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
                }, false);

    }

    public void countUnreadNotification(){
        grabServerData(Kitchen.arange(this, NotificationUserApi.class).countUnreadNotification(currentAuthorization, topics, types, null, null),
                (wsResponse, response) -> {
                    if(wsResponse.isSuccess()){
                        this.unreadCount= wsResponse.getData();
                        unreadNotificationTextView.setText(getString(R.string.UnreadCount, String.valueOf(unreadCount)));
                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                }, false);
    }


    @Override
    public void onNeedRecount() {
        countUnreadNotification();
        fetchNotifications();
    }
}
