package id.catchadeal.kerjayuk.holder;

import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.misc.AboutApplicationActivity;
import id.catchadeal.kerjayuk.activity.user.AccountActivity;
import id.catchadeal.kerjayuk.activity.user.AccountConfigurationActivity;
import id.catchadeal.kerjayuk.activity.user.AccountFormActivity;
import id.catchadeal.kerjayuk.activity.user.UserPhotoProfileActivity;
import id.catchadeal.kerjayuk.model.misc.AccountConfigurationMenu;
import id.catchadeal.kerjayuk.model.misc.AccountMenu;

public class AccountConfiguraitionMenuHolder extends ACTViewHolder<AccountConfigurationActivity, AccountConfigurationMenu, String> {

    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.card_view) CardView cardView ;

    public AccountConfiguraitionMenuHolder(AccountConfigurationActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_account_menu, viewGroup);
    }

    @Override
    public void showData(AccountConfigurationMenu object) {
        super.showData(object);
        titleTextView.setText(object.getLabel());
        imageView.setImageResource(object.getIconResource());
        cardView.setOnClickListener(v -> {
            if(object.getLabel()==R.string.ChangePhotoProfile){
                getAct().startActivity(UserPhotoProfileActivity.class);
            }
            if(object.getLabel()==R.string.CompleteYourForm){
                getAct().startActivity(AccountFormActivity.class);
            }
            if(object.getLabel()==R.string.AboutApplication){
                getAct().startActivity(AboutApplicationActivity.class);
            }
        });
    }

    @Override
    public void find(String findParam) {

    }
}
