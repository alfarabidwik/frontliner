package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface TaskApplicationStatusApi {

    @GET("api/taskApplicationStatuses")
    Observable<WSResponse<List<TaskApplicationStatusDto>, String>> taskApplicationStatuses(@Query("ascending") Boolean ascending, @Query("ascending") String sortir);

}
