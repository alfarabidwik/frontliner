package id.catchadeal.kerjayuk.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.InputTools;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;
import com.alfarabi.alfalibs.tools.WLog;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.natewickstrom.rxactivityresult.RxActivityResult;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.camera.CameraActivity;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.model.region.Option;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.FormIntegration;
import jrizani.jrspinner.JRSpinner;
import lombok.Getter;
import lombok.Setter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FormInputView extends LinearLayout {

    public static final String TAG = FormInputView.class.getName();
    
    @Getter@Setter Form form ;
    static HashMap<String, Integer> inputTypeMap = new HashMap<>();
    MaterialEditText editText ;
    TextView errorTextView ;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateAppConfig.DATETIME_VIEW_FORMAT);
    @Getter FormApplicationDto formApplicationDto = null ;
    BaseActivity baseActivity ;

    FormIntegration formIntegration = null ;
    View formView ;
    JRSpinner spinner ;

    @Getter HashMap<String, String> queryParamMap = new HashMap<>();


    public FormInputView(Context context, FormIntegration formIntegration, Form form) {
        super(context);
        this.form = form;
        this.formApplicationDto = new FormApplicationDto();
        this.formApplicationDto.setForm(form);
        this.baseActivity = (BaseActivity) context;
        this.formIntegration = formIntegration;
        initView();
    }


    public FormInputView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.baseActivity = (BaseActivity) context;
        initView();
    }

    public void initView(){
        if(form.getType().equalsIgnoreCase(Form.TEXT)|| form.getType().equalsIgnoreCase(Form.EMAIL) || form.getType().equalsIgnoreCase(Form.NUMBER)
                || form.getType().equalsIgnoreCase(Form.PHONE) || form.getType().equalsIgnoreCase(Form.CURRENCY) || form.getType().equalsIgnoreCase(Form.DATE)){
            formView = LayoutInflater.from(getContext()).inflate(R.layout.form_builder_text_input, null);
            this.editText = formView.findViewById(R.id.edittext);
            this.editText.setGravity(Gravity.TOP);
            editText.setFloatingLabelText(form.getTitle());
            editText.setHint(form.getPlaceHolder());
            editText.setInputType(inputTypeMap.get(form.getType()));
            if(StringUtils.isNotEmpty(form.getAllowedCharacters())){
                InputFilter characterFilter = (source, start, end, dest, dstart, dend) -> {
                    for (int i = start; i < end; i++) {
                        if (source != null && form.getAllowedCharacters().contains((String.valueOf(source.charAt(i))))) {
                            return source;
                        }
                    }
                    return "";
                };
                editText.setFilters(new InputFilter[]{characterFilter});
            }
            if(form.getLine()>0){
                editText.setLines(form.getLine());
            }
            if(form.getMaxLength()>0){
                editText.setMaxCharacters(form.getMaxLength());
            }
            if(form.getType().equalsIgnoreCase(Form.DATE)){
                editText.setOnClickListener(v -> {
                    editText.setCompoundDrawablesRelative(null, null, getContext().getDrawable(R.drawable.ic_calendar_3), null);
                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                        editText.setText(DateFormatUtils.format(calendar.getTime(), "yyyy/MM/dd"));
                        this.formApplicationDto.setValue(simpleDateFormat.format(calendar.getTime()));
                    });
                    datePickerDialog.setMaxDate(Calendar.getInstance());
                    datePickerDialog.show(((BaseActivity)this.getContext()).getFragmentManager(), DatePickerDialog.class.getName()+"Birthdate");
                });
            }else{
                editText.addTextChangedListener(new SimpleTextWatcher() {
                    @Override
                    public void afterTextChanged(Editable s) {
                        FormInputView.this.formApplicationDto.setValue(s.toString());
                    }
                });
            }
            this.addView(formView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        }
        if(form.getType().equalsIgnoreCase(Form.DROPDOWN_API_DATA)){
            formView = LayoutInflater.from(getContext()).inflate(R.layout.form_builder_spinner_input, null);
            TextView labelTextView = formView.findViewById(R.id.label_tv);
            labelTextView.setText(form.getTitle());
            spinner = this.formView.findViewById(R.id.spinner);
            spinner.setTitle(form.getTitle());
            spinner.setHint(form.getPlaceHolder());

            if(form.isDependAnotherTag()){

            }else{
                fetchApi();
            }

            this.addView(formView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        }
        if(form.getType().equalsIgnoreCase(Form.IMAGE)){
            View formView = LayoutInflater.from(getContext()).inflate(R.layout.form_builder_image_input, null);
            ImageView imageView = formView.findViewById(R.id.image_iv);
            TextView labelTextView = formView.findViewById(R.id.label_tv);
            TextView titleTextView = formView.findViewById(R.id.title_tv);
            TextView placehoderTextView = formView.findViewById(R.id.placeholder_tv);
            this.errorTextView = formView.findViewById(R.id.error_tv);
            labelTextView.setText(form.getTitle());
            titleTextView.setText(form.getTitle());
            placehoderTextView.setText("");
            if(form.getPlaceHolder()!=null){
                placehoderTextView.setText(form.getPlaceHolder());
            }
            formView.setOnClickListener(v -> {
                MediaDialogFragment.instance().show(baseActivity.getSupportFragmentManager().beginTransaction(), MediaDialogFragment.TAG, viewCallback -> {
                    if(viewCallback.getId()==R.id.camera_layout){
                        baseActivity.takePermission(aBoolean -> {
                            if(aBoolean){
                                Intent intent = new Intent(getContext(), CameraActivity.class);
                                int requestCode = RandomUtils.nextInt();
                                RxActivityResult.getInstance(getContext()).from(((BaseActivity)getContext())).startActivityForResult(intent, requestCode).subscribe(result -> {
                                    WLog.d(TAG, new Gson().toJson(result));
                                    if (result.getResultCode()==Constant.RESULT_FINISH && result.getData()!=null) {
                                        errorTextView.setVisibility(GONE);
                                        String photoPath = result.getData().getStringExtra(Constant.FILE_PATH);
                                        Bitmap bm = BitmapFactory.decodeFile(photoPath);
                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                                        byte[] byteArrayImage = baos.toByteArray();
                                        try{
                                            String imageBase64 = URLEncoder.encode(Base64.encodeToString(byteArrayImage, Base64.NO_WRAP), "UTF-8");
                                            GlideApp.with(this).load(photoPath).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
                                            errorTextView.setVisibility(GONE);
                                            this.formApplicationDto.setValue(imageBase64);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(baseActivity, baseActivity.getString(R.string.permission_deniedd), Toast.LENGTH_SHORT).show();
                            }
                        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
                    }
                    if(viewCallback.getId()==R.id.gallery_layout){
                        baseActivity.takePermission(aBoolean -> {
                            if(aBoolean){
                                String action = null;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    action = Intent.ACTION_OPEN_DOCUMENT;
                                } else {
                                    action = Intent.ACTION_PICK;
                                }
                                Intent galleryIntent = new Intent(action, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                galleryIntent.setType("image/*");
                                int requestCode = RandomUtils.nextInt();
                                RxActivityResult.getInstance(getContext()).from(((BaseActivity)getContext())).startActivityForResult(galleryIntent, requestCode).subscribe(result -> {
                                    WLog.d(TAG, new Gson().toJson(result));
                                    int cropRequestCode = RandomUtils.nextInt();
                                    if(result!=null &&result.getData()!=null){
                                        Intent cropIntent = CropImage.activity(result.getData().getData()).setGuidelines(CropImageView.Guidelines.ON).getIntent(baseActivity);
                                        RxActivityResult.getInstance(baseActivity).from(baseActivity)
                                                .startActivityForResult(cropIntent, cropRequestCode)
                                                .subscribe(result1 -> {
                                                    CropImage.ActivityResult resultCrop = CropImage.getActivityResult(result1.getData());
                                                    if (result1.getResultCode() == baseActivity.RESULT_OK) {
                                                        String photoPath = resultCrop.getUri().getPath();
                                                        Bitmap bm = BitmapFactory.decodeFile(photoPath);
                                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                                                        byte[] byteArrayImage = baos.toByteArray();
                                                        try{
                                                            String imageBase64 = URLEncoder.encode(Base64.encodeToString(byteArrayImage, Base64.NO_WRAP), "UTF-8");
                                                            GlideApp.with(this).load(photoPath).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
                                                            errorTextView.setVisibility(GONE);
                                                            this.formApplicationDto.setValue(imageBase64);
                                                        }catch (Exception e){
                                                            e.printStackTrace();
                                                        }
                                                    } else if (result1.getResultCode() == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                                                        Exception error = resultCrop.getError();
                                                        error.printStackTrace();
                                                        Toast.makeText(baseActivity, error.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }

                                                });
                                    }

                                });
                            }else{
                                Toast.makeText(baseActivity, baseActivity.getString(R.string.permission_deniedd), Toast.LENGTH_SHORT).show();
                            }
                        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                });
            });
            this.addView(formView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        }
    }

    public boolean validateInput(){
        boolean validate = true ;
        if(form.isMandatory()){
            if(form.getType().equalsIgnoreCase(Form.TEXT)|| form.getType().equalsIgnoreCase(Form.EMAIL) || form.getType().equalsIgnoreCase(Form.NUMBER)
                    || form.getType().equalsIgnoreCase(Form.PHONE) || form.getType().equalsIgnoreCase(Form.CURRENCY) || form.getType().equalsIgnoreCase(Form.DATE)){
                if(editText!=null){
                    if(!InputTools.isComplete(false, this.editText)){
                        this.editText.setError(form.getPlaceHolder());
                        baseActivity.showAlert("", form.getPlaceHolder());
                        validate = false ;
                    }
                }
            }
            if(form.getType().equalsIgnoreCase(Form.DROPDOWN_API_DATA)){
                if(formApplicationDto.getValueId()==null || StringUtils.isEmpty(formApplicationDto.getValue())){
                    this.spinner.setError(form.getPlaceHolder());
                    baseActivity.showAlert("", form.getPlaceHolder());
                    validate = false ;
                }

            }
            if(form.getType().equalsIgnoreCase(Form.IMAGE)){
                if(this.formApplicationDto.getValue()==null){
                    if(errorTextView!=null){
                        errorTextView.setVisibility(VISIBLE);
                        errorTextView.setText(form.getPlaceHolder());
                        baseActivity.showAlert("", form.getPlaceHolder());
                        validate = false ;
                    }
                }

            }
        }
        return validate;
    }

    public void fetchApi(){
        String baseApi = Constant.BASE_DOMAIN_API+form.getFetchApi();
        Set<Map.Entry<String, String>> set = queryParamMap.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            baseApi = baseApi.replace("="+entry.getKey(), "="+entry.getValue());
        }
        WLog.d(TAG, "Hit API "+baseApi);
        ViewProgress.show(baseActivity);
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(baseApi).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException throwable) {
                throwable.printStackTrace();
                baseActivity.runOnUiThread(() -> {
                    ViewProgress.dismiss();
                    baseActivity.showAlert(baseActivity.getString(R.string.Failed), throwable.getMessage());
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.code()==200){
                    String bodyString = response.body().string();
                    if(response.body()!=null){
                        WLog.d(TAG, "RESPONSE BODY "+bodyString);
                        WSResponse<List<Option>, String> wsResponse = baseActivity.getGson().fromJson(bodyString, new TypeToken<WSResponse<List<Option>, String>>(){}.getType());
                        List<Option> options = wsResponse.getData();
                        List<String> labels = new ArrayList<>();
                        options.forEach(option -> {
                            labels.add(option.getName());
                        });
                        baseActivity.runOnUiThread(() -> {
                            spinner.setItems(labels.toArray(new String[labels.size()]));
                            spinner.setOnItemClickListener(position -> {
                                FormInputView.this.formApplicationDto.setValue(options.get(position).getName());
                                FormInputView.this.formApplicationDto.setValueId(options.get(position).getId());
                                FormInputView.this.formIntegration.triggerAnotherFormIfAny(form.getTag(), FormInputView.this.formApplicationDto);
                            });
                        });
                    }
                }
                baseActivity.runOnUiThread(() -> {
                    ViewProgress.dismiss();
                });
            }
        });

    }


    static {
        inputTypeMap = new HashMap<>();
        inputTypeMap.put(Form.TEXT, InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputTypeMap.put(Form.EMAIL, InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        inputTypeMap.put(Form.NUMBER, InputType.TYPE_CLASS_NUMBER);
        inputTypeMap.put(Form.PHONE, InputType.TYPE_CLASS_PHONE);
        inputTypeMap.put(Form.CURRENCY, InputType.TYPE_CLASS_NUMBER);
    }



}
