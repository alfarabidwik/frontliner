package id.catchadeal.kerjayuk.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.business.FormLaneApplicationDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;

public class CardTaskApplication extends LinearLayout {

    FormLaneApplicationDto formLaneApplicationDto ;
    BaseActivity baseActivity ;

    public CardTaskApplication(Context context, FormLaneApplicationDto formLaneApplicationDto) {
        super(context);
        this.formLaneApplicationDto = formLaneApplicationDto;
        this.baseActivity = (BaseActivity) context;
        initView();
    }

    public CardTaskApplication(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.baseActivity = (BaseActivity) context;
    }

    public void initView(){
        LinearLayout cardLinearLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.card_task_application, null);
        LinearLayout formLaneLayout = cardLinearLayout.findViewById(R.id.form_lane_layout);
        List<FormApplicationDto> formApplicationDtos = formLaneApplicationDto.getFormApplications();
        List<FormApplicationDto> formApplicationDtoList = new ArrayList<>();
        formApplicationDtoList.addAll(formApplicationDtos);
        Collections.sort(formApplicationDtoList, (formApplicationDto1, formApplicationDto2) -> {
            return formApplicationDto1.getForm().getId().intValue()-formApplicationDto2.getForm().getId().intValue();
        });
        for (FormApplicationDto formApplicationDto : formApplicationDtoList) {
            Form form = formApplicationDto.getForm();
//            if(!form.getType().equalsIgnoreCase(Form.IMAGE)){
                formLaneLayout.addView(new FormTaskApplicationView(baseActivity, formApplicationDto),  new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
//            }
        }

        this.addView(cardLinearLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

    }

}
