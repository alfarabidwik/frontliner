package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.apache.commons.lang.StringUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.utils.Utils;

public class HorizontalTaskHolder<BA extends BaseActivity> extends ACTViewHolder<BA, TaskDto, String> {

    @BindView(R.id.framelayout) View frameLayout ;
    @BindView(R.id.publisher_tv) TextView publisherTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.task_category_tv) TextView taskCategoryTextView ;
    @BindView(R.id.description_tv) HtmlTextView descriptionTextView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;
    @BindView(R.id.shimmer_publisher_layout) ShimmerFrameLayout shimmerPublisherLayout ;
    @BindView(R.id.shimmer_title_layout) ShimmerFrameLayout shimmerTitleLayout ;
    @BindView(R.id.shimmer_category_layout) ShimmerFrameLayout shimmerCategoryLayout ;
    @BindView(R.id.shimmer_description_layout) ShimmerFrameLayout shimmerDescriptionLayout ;


    public HorizontalTaskHolder(DashboardActivity dashboardActivity, ViewGroup viewGroup) {
        super((BA) dashboardActivity, R.layout.holder_horizontal_task, viewGroup);
    }

    public HorizontalTaskHolder(TaskListActivity activity, ViewGroup viewGroup) {
        super((BA) activity, R.layout.holder_horizontal_task, viewGroup);
    }


    @Override
    public void showData(TaskDto object) {
        super.showData(object);
        if(object.getId()!=null){
            shimmerPublisherLayout.stopShimmer();
            shimmerPublisherLayout.hideShimmer();
            shimmerTitleLayout.stopShimmer();
            shimmerTitleLayout.hideShimmer();
            shimmerCategoryLayout.stopShimmer();
            shimmerCategoryLayout.hideShimmer();
            shimmerDescriptionLayout.stopShimmer();
            shimmerDescriptionLayout.hideShimmer();
            if(StringUtils.isNotEmpty(object.getPartner().getWebsite())){
                publisherTextView.setText(object.getPartner().getWebsite());
            }else{
                publisherTextView.setText(StringUtils.defaultString(object.getPartner().getFullName(), "-"));
            }
            titleTextView.setText(object.getTitle());
            taskCategoryTextView.setText(object.getTaskCategory().getName());
            descriptionTextView.setHtml(Utils.abbreviete(object.getSubtitle(), 60));
            feeTextView.setText(NumberUtil.moneyFormat(object.getFee(), true));
            if(object.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
                taskFeeTypeTextView.setText(R.string.PerTask);
            }
            if(object.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
                taskFeeTypeTextView.setText(R.string.PerAgent);
            }

            frameLayout.setOnClickListener(v -> {
                getAct().startActivity(TaskDetailActivity.class, getAct().getGson().toJson(object), true);
            });
        }else{
            shimmerPublisherLayout.showShimmer(true);
            shimmerTitleLayout.showShimmer(true);
            shimmerCategoryLayout.showShimmer(true);
            shimmerDescriptionLayout.showShimmer(true);
        }
    }

    @Override
    public void find(String findParam) {

    }
}
