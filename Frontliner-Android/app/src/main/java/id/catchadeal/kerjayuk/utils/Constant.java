package id.catchadeal.kerjayuk.utils;

import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;

public class Constant {

    public static final String INDONESIA_COUNTRY_ID = "ID";
    public static final String INDONESIA_LANGUAGE_ID = "in";
    public static final String LANGUAGE = "language";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String AUTHORIZATION = Constant.class.getName()+"_Authorization";
    public static final String FIREBASE_TOKEN = Constant.class.getName()+"_TOKEN";
    public static final String TASK_APPLICATION_STATUSES = Constant.class.getName()+"_"+TaskApplicationDto.class.getName();
    public static final String TASK_CATEGORIES = Constant.class.getName()+"_"+TaskCategoryDto.class.getName();
    public static final String DEVICE_ID = Constant.class.getName()+"_DEVICE_ID";
    public static final String CONFIGURATION = Constant.class.getName()+"_CONFIGURATION";
    public static final String USER = Constant.class.getName()+"_USER";
    public static final String PROVINCES = Constant.class.getName()+"_PROVINCES";
    public static final String BANKS = Constant.class.getName()+"_BANKS";
    public static final String FIRST_INSTALLATION = Constant.class.getName()+"_FIRST_INSTALLATION";
    public static final String BANNER = Constant.class.getName()+"_BANNER";

//    public static final String HOST = "192.168.100.167";
//    public static final String HOST = "192.168.8.102";
//    public static final String HOST = "192.168.8.101";
//    public static final String HOST = "192.168.8.103";
//    public static final String HOST = "192.168.8.104";
//    public static final String HOST = "kerjayuk-api.numeralasia.com";
//
    public static final String BASE_DOMAIN = "https://api.kerjayuk.id/";
//    public static final String BASE_DOMAIN = "https://kerjayuk-api.numeralasia.com/";
//    public static final String BASE_DOMAIN = "http://"+HOST+":8055/";
    public static final String BASE_API = "/api";
    public static final String BASE_DOMAIN_API = BASE_DOMAIN+"api";
    public static final String BLOG_DOMAIN = "https://catchadeal.id/";
    public static final String BLOG_WEB = "https://catchadeal.id/blog";
    public static final String REPORT_RECEIVER_EMAIL = "alfarabidwik@gmail.com";
    public static final String REPORT_SUBJECT = Constant.class.getPackage().getName();


    public static int RESULT_FINISH = 1111;
    public static int REQUEST_TOPUP = 5555;
    public static int REQUEST_AUTH = 5556;
    public static int REQUEST_RESET_PASSWORD = 5557;
    public static int REQUEST_REGISTRATION = 5557;
    public static int REQUEST_PHOTO = 5558;
    public static int REQUEST_GALLERY = 5559;
    public static int REQUEST_KTP_PHOTO = 5560;
    public static int REQUEST_SELFIE_KTP_PHOTO = 5561;
    public static int REQUEST_SUBMIT = 5562;

    public static String REGISTRATION_TOKEN = "REGISTRATION_TOKEN";

    public static final String MESSAGE_JSON = "MESSAGE_JSON";
    public static final String MESSAGE_OBJECT = "MESSAGE_OBJECT";
    public static final String MESSAGE_TITLE = "MESSAGE_TITLE";
    public static final String MESSAGE_CONTENT = "MESSAGE_CONTENT";

    public static final String FILE_PATH = "FILE_PATH";

    public static final String ANDROID = "ANDROID";

    public static final String ERROR_INVALID_PHONE_NUMBER = "ERROR_INVALID_PHONE_NUMBER";


}
