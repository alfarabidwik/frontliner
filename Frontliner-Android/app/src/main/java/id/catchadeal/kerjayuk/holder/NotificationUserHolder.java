package id.catchadeal.kerjayuk.holder;

import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.Date;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.activity.notification.NotificationActivity;
import id.catchadeal.kerjayuk.activity.notification.NotificationAndSummaryActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.user.TaskApplicationDetailActivity;
import id.catchadeal.kerjayuk.api.NotificationUserApi;
import id.catchadeal.kerjayuk.api.TaskApi;
import id.catchadeal.kerjayuk.api.TaskApplicationApi;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.misc.NotificationData;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.model.misc.NotificationUserDto;
import id.catchadeal.kerjayuk.utils.TimeAgo;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class NotificationUserHolder extends ACTViewHolder<BaseActivity, NotificationUserDto, String> {
    
    public static final String TAG = NotificationUserHolder.class.getName();

    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.content_tv) TextView contentTextView ;
    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.date_tv) TextView dateTextView ;
    @BindView(R.id.container_layout) FrameLayout containerLayout ;
    @BindView(R.id.card_view) CardView cardView ;

    public NotificationUserHolder(NotificationAndSummaryActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_notification, viewGroup);
    }

    public NotificationUserHolder(NotificationActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_notification, viewGroup);
    }

    @Override
    public void showData(NotificationUserDto notificationUserDto) {
        super.showData(notificationUserDto);
        titleTextView.setText(notificationUserDto.getNotification().getTitle());
        contentTextView.setText(notificationUserDto.getNotification().getMessage());
        dateTextView.setText(TimeAgo.toDuration(new Date().getTime()-notificationUserDto.getCreated().getTime()));
        GlideApp.with(getAct()).load(notificationUserDto.getNotification().getImageLink()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(imageView);
        if(notificationUserDto.getRead()!=null && !notificationUserDto.getRead()){
            cardView.setCardBackgroundColor(getAct().getColor(R.color.blueQuatenary));
        }else{
            cardView.setCardBackgroundColor(getAct().getColor(R.color.white));
        }
        String topic = notificationUserDto.getNotification().getTopic();
        containerLayout.setOnClickListener(v -> {
            if(!notificationUserDto.getRead()){
                getAct().grabServerData(Kitchen.arange(getAct(), NotificationUserApi.class).read(getAct().getCurrentAuthorization(), notificationUserDto.getId()), (wsResponse, response) -> {
                    if(wsResponse.isSuccess()){
                        if(getAct() instanceof NotificationActivity){
                            ((NotificationActivity) getAct()).getNotifications().get(getAdapterPosition()).setRead(true);
                            ((NotificationActivity) getAct()).getAdapter().notifyItemChanged(getAdapterPosition());
                            ((NotificationActivity) getAct()).countUnreadNotification();
                        }else if(getAct() instanceof NotificationAndSummaryActivity){
                            ((NotificationAndSummaryActivity) getAct()).fetchCurrentNotificationCount();
                            ((NotificationAndSummaryActivity) getAct()).readNotification();
                            ((NotificationAndSummaryActivity) getAct()).getNotifications().get(getAdapterPosition()).setRead(true);
                            ((NotificationAndSummaryActivity) getAct()).getAdapter().notifyItemChanged(getAdapterPosition());
                        }
                    }
                }, (aBoolean, throwable, response) -> { }, false);
            }
            if(topic==null){
                return;
            }
            if(topic.equalsIgnoreCase(NotificationDto.SPECIFIC)){

                if(notificationUserDto.getNotification().getData()!=null){
                    NotificationData notificationData = notificationUserDto.getNotification().getData();
                    if(notificationData.getReferenceClass()!=null && notificationData.getReferenceClass().equalsIgnoreCase(TaskApplicationDto.class.getName())){
                        ViewProgress.show(getAct());
                        getAct().grabServerData(Kitchen.arange(getAct(), TaskApplicationApi.class).findById(getAct().getCurrentAuthorization(), Long.valueOf(notificationUserDto.getNotification().getData().getReferenceId())), (wsResponse, response) -> {
                            ViewProgress.dismiss();
                            if(wsResponse.isSuccess()){
                                getAct().startActivity(TaskApplicationDetailActivity.class, getAct().getGson().toJson(wsResponse.getData()), true);
                            }
                        }, (aBoolean, throwable, response) -> {
                            ViewProgress.dismiss();
                            getAct().showAlert(getString(R.string.Failed), throwable.getMessage());
                        });
                    }
                }
            }
            if(topic.equalsIgnoreCase(NotificationDto.NEWS) || topic.equalsIgnoreCase(NotificationDto.ARTICLE)){
                if(notificationUserDto.getNotification().getData()!=null && StringUtils.isNotEmpty(notificationUserDto.getNotification().getData().getAddressLink())){
                    getAct().startActivity(WebViewActivity.class, notificationUserDto.getNotification().getData().getAddressLink());
                }
            }
            if(topic.equalsIgnoreCase(NotificationDto.BROADCAST)){

            }
            if(topic.equalsIgnoreCase(NotificationDto.NEW_TASK)){
                if(notificationUserDto.getNotification().getData().getReferenceClass().equalsIgnoreCase(TaskDto.class.getName())){
                    ViewProgress.show(getAct());
                    getAct().grabServerData(Kitchen.arange(getAct(), TaskApi.class).task(getAct().getCurrentAuthorization(), Long.valueOf(notificationUserDto.getNotification().getData().getReferenceId())), (wsResponse, response) -> {
                        ViewProgress.dismiss();
                        if(wsResponse.isSuccess()){
                            getAct().startActivity(TaskDetailActivity.class, getAct().getGson().toJson(wsResponse.getData()), true);
                        }
                    }, (aBoolean, throwable, response) -> {
                        ViewProgress.dismiss();
                        getAct().showAlert(getString(R.string.Failed), throwable.getMessage());
                    });
                }
            }
        });
    }

    @Override
    public void find(String findParam) {

    }
}
