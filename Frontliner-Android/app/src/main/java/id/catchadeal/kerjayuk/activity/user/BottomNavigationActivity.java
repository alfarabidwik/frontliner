package id.catchadeal.kerjayuk.activity.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.Kitchen;

import java.util.function.Consumer;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.notification.NotificationAndSummaryActivity;
import id.catchadeal.kerjayuk.api.NotificationUserApi;
import id.catchadeal.kerjayuk.interfaze.NotificationEventBadgeRecount;
import id.catchadeal.kerjayuk.utils.BottomNavigationViewHelper;

public abstract class BottomNavigationActivity extends BaseActivity implements NotificationEventBadgeRecount {

    @BindView(R.id.bottom_navigation) protected BottomNavigationView bottomNavigationView ;

    public static NotificationEventBadgeRecount notificationEventBadgeRecount ;

    public abstract int bottomMenu();

    Long unreadCount = 0l ;

    View notificationBadge = null ;
    TextView badgeTextView = null ;
    FrameLayout badgeFrameLayout = null ;


    public abstract void focusBar();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        if(bottomMenu()!=R.id.home){
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if(item.getItemId()==R.id.home && item.getItemId()!=bottomMenu()){
                startActivity(DashboardActivity.class);
            }
            if(item.getItemId()==R.id.notification && item.getItemId()!=bottomMenu()){
                startActivity(NotificationAndSummaryActivity.class);
            }
            if(item.getItemId()==R.id.history && item.getItemId()!=bottomMenu()){
                startActivity(TaskApplicationActivity.class);
            }
            if(item.getItemId()==R.id.account && item.getItemId()!=bottomMenu()){
                startActivity(AccountActivity.class);
            }
            return true;
        });
    }

    private void addBadgeView() {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        BottomNavigationItemView itemView = (BottomNavigationItemView) menuView.getChildAt(1);

        notificationBadge = LayoutInflater.from(this).inflate(R.layout.bottom_notification_badge, menuView, false);
        badgeTextView = notificationBadge.findViewById(R.id.badge_tv);
        badgeFrameLayout = notificationBadge.findViewById(R.id.badge_frame);
        badgeFrameLayout.setVisibility(View.GONE);
        itemView.addView(notificationBadge);
        countUnreadNotification("", "","", "", aLong -> {
            this.unreadCount = aLong ;
            if(unreadCount>0){
                badgeFrameLayout.setVisibility(View.VISIBLE);
                badgeTextView.setText(String.valueOf(unreadCount));
            }else{
                badgeFrameLayout.setVisibility(View.GONE);
            }
        });
        notificationEventBadgeRecount = this;
        focusBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationView.setSelectedItemId(bottomMenu());
        addBadgeView();
    }

    public void countUnreadNotification(String topics, String types, String topicExcludes, String typeExcludes, Consumer<Long> callback){
        grabServerData(Kitchen.arange(this, NotificationUserApi.class).countUnreadNotification(currentAuthorization, topics, types, topicExcludes, typeExcludes),
                (wsResponse, response) -> {
                    if(wsResponse.isSuccess()){
                        callback.accept(wsResponse.getData());
                    }
                }, (aBoolean, throwable, response) -> {
                    throwable.printStackTrace();
                }, false);
    }

    public void readNotification(){
        unreadCount = unreadCount-1;
        if(unreadCount>0){
            badgeFrameLayout.setVisibility(View.VISIBLE);
            badgeTextView.setText(String.valueOf(unreadCount));
        }else{
            badgeFrameLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onGetNotification() {
        unreadCount= unreadCount+1;
        if(unreadCount>0){
            runOnUiThread(() -> {
                badgeFrameLayout.setVisibility(View.VISIBLE);
                badgeTextView.setText(String.valueOf(unreadCount));
            });
        }else{
            runOnUiThread(() -> {
                badgeFrameLayout.setVisibility(View.GONE);
            });
        }
    }

}


