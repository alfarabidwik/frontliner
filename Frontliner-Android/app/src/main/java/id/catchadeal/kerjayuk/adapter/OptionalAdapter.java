package id.catchadeal.kerjayuk.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import id.catchadeal.kerjayuk.model.region.Option;


public class OptionalAdapter<R extends Option> extends ArrayAdapter<String> {
    
    public static final String TAG = OptionalAdapter.class.getName();


    Map<String, R> regionHashMap = new HashMap<>();
    private Context context ;

    public OptionalAdapter(@NonNull Context context, List<R> regions) {
        super(context, android.R.layout.simple_spinner_dropdown_item, Arrays.asList(regions.stream().map(region -> region.getName()).toArray(String[]::new)));
        this.context = context;
        regions.forEach(region -> {
            regionHashMap.put(region.getName().toLowerCase(), region);
        });
    }

    public R getSelected(int position){
        return regionHashMap.get(getItem(position).toLowerCase());
    }

//    public int mustCurrentPosition(String name){
//        return regionHashMap.entrySet().iterator().forEachRemaining();
//    }


//    public int mustCurrentPosition(Long id){
//        return IntStream.range(0, getCount()).filter(value -> {
//            return Long.valueOf(getItem(value).split("####")[0]).equals(id);
//        }).findFirst().getAsInt();
//    }

    public static OptionalAdapter reCreate(@NonNull Context context, List regions){
        return new OptionalAdapter(context,regions);
    }
}
