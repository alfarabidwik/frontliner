package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.blog.Article;
import id.catchadeal.kerjayuk.model.misc.TaskCategoryDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticleApi {

    @GET("blog/wp-json/wp/v2/posts?_embed")
    Observable<List<Article>> get(
            @Query("page") Integer page,
            @Query("per_page") Integer perPage);

}
