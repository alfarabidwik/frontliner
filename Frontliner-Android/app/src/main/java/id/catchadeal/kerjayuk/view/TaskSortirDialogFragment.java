package id.catchadeal.kerjayuk.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.BasicFragRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.RecyclerCallback;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.holder.TaskApplicationSortirHolder;
import id.catchadeal.kerjayuk.util.TaskApplicationSortir;
import lombok.Getter;
import lombok.Setter;

public class TaskSortirDialogFragment extends BottomSheetDialogFragment implements RecyclerCallback {
    
    public static final String TAG = TaskSortirDialogFragment.class.getName();

    @BindView(R.id.close_button) ImageView closeView ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;

    @Getter BasicFragRecyclerAdapter<TaskApplicationSortir, TaskSortirDialogFragment, TaskApplicationSortirHolder> adapter ;

    @Getter@Setter List<TaskApplicationSortir> taskApplicationSortirs = new ArrayList<>();
    @Getter TaskApplicationSortir taskApplicationSortir ;

    Consumer<TaskApplicationSortir> callback ;

    @Getter
    private boolean showing ;

    public static TaskSortirDialogFragment instance(List<TaskApplicationSortir> taskApplicationSortirs, TaskApplicationSortir taskApplicationSortir) {
        TaskSortirDialogFragment taskSortirDialogFragment = new TaskSortirDialogFragment();
        taskSortirDialogFragment.setTaskApplicationSortirs(taskApplicationSortirs);
        taskSortirDialogFragment.setTaskApplicationSortir(taskApplicationSortir);
        return taskSortirDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_sortir_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getView().setBackgroundColor(getActivity().getColor(R.color.page_background));
        adapter = new BasicFragRecyclerAdapter<>(this, TaskApplicationSortirHolder.class, taskApplicationSortirs);
        adapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));
        closeView.setOnClickListener(v -> {
            dismiss();
        });

    }

    public void show(FragmentManager fragmentManager, String tag, Consumer<TaskApplicationSortir> callback) {
        this.showing = !this.showing;
        this.callback = callback ;
        super.show(fragmentManager, tag);
    }

    public void setTaskApplicationSortir(TaskApplicationSortir taskApplicationSortir) {
        this.taskApplicationSortir = taskApplicationSortir;
        if(callback!=null){
            callback.accept(taskApplicationSortir);
        }
    }

    @Override
    public Object getObject() {
        return null;
    }
}
