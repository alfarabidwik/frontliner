package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.ConfigurationDto;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ConfigurationApi {

    @GET("api/configuration")
    Observable<WSResponse<ConfigurationDto, String>> configuration();


}
