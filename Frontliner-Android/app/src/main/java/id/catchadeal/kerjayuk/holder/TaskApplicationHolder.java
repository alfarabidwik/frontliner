package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.apache.commons.lang.time.DateFormatUtils;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.task.TaskReferralMemberActivity;
import id.catchadeal.kerjayuk.activity.user.TaskApplicationActivity;
import id.catchadeal.kerjayuk.activity.user.TaskApplicationDetailActivity;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.NumberUtil;

public class TaskApplicationHolder extends ACTViewHolder<BaseActivity, TaskApplicationDto, String> {

    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;
    @BindView(R.id.card_container) View cardContainer ;
    @BindView(R.id.date_tv) TextView dateTextView ;
    @BindView(R.id.status_tv) TextView statusTextView ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;

    public TaskApplicationHolder(TaskApplicationActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_task_application, viewGroup);
    }

    @Override
    public void showData(TaskApplicationDto taskApplicationDto) {
        super.showData(taskApplicationDto);
        TaskDto taskDto = taskApplicationDto.getTask();
        GlideApp.with(getAct()).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(partnerImageView);
        feeTextView.setText(NumberUtil.moneyFormat(taskApplicationDto.getItemFee(), true));
        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());
        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            taskFeeTypeTextView.setText(R.string.PerTask);
        }
        if(taskApplicationDto.getTask().getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            taskFeeTypeTextView.setText(R.string.PerAgent);
        }

        cardContainer.setOnClickListener(v -> {
            getAct().startActivity(TaskApplicationDetailActivity.class, getAct().getGson().toJson(taskApplicationDto));
        });
        dateTextView.setText(DateFormatUtils.format(taskApplicationDto.getCreated(), "dd-MMM-yyyy"));
        statusTextView.setText(taskApplicationDto.getTaskApplicationStatus().getDescription());
    }

    @Override
    public void find(String findParam) {

    }
}
