package id.catchadeal.kerjayuk.activity.user;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.InputTools;
import com.alfarabi.alfalibs.tools.SimpleTextWatcher;
import com.appizona.yehiahd.fastsave.FastSave;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.auth.EmailAuthenticationActivity;
import id.catchadeal.kerjayuk.activity.camera.CameraActivity;
import id.catchadeal.kerjayuk.activity.camera.KTPCameraActivity;
import id.catchadeal.kerjayuk.activity.camera.SelfieKTPCameraActivity;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.adapter.OptionalAdapter;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import id.catchadeal.kerjayuk.model.region.CityDto;
import id.catchadeal.kerjayuk.model.region.DistrictDto;
import id.catchadeal.kerjayuk.model.region.ProvinceDto;
import id.catchadeal.kerjayuk.model.region.VillageDto;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AccountFormActivity extends BaseActivity {

    public static final String TAG = AccountFormActivity.class.getName();


    @BindView(R.id.living_address_et) MaterialEditText livingAddressEditText ;
    @BindView(R.id.province_et) MaterialAutoCompleteTextView provinceAutoCompleteTextView ;
    @BindView(R.id.city_et) MaterialAutoCompleteTextView cityAutoCompleteTextView ;
    @BindView(R.id.district_et) MaterialAutoCompleteTextView districtAutoCompleteTextView ;
    @BindView(R.id.village_et) MaterialAutoCompleteTextView villageAutoCompleteTextView ;


    @BindView(R.id.birthdate_et) MaterialEditText birthdateEditText ;
    @BindView(R.id.bank_name_et) MaterialAutoCompleteTextView bankNameAutoCompleteTextView;
    @BindView(R.id.bank_account_code_et) MaterialEditText bankAccountCodeEditText ;
    @BindView(R.id.bank_account_name_et) MaterialEditText bankAccountNameEditText ;
    @BindView(R.id.ktp_code_et) MaterialEditText ktpCodeEditText ;

    @BindView(R.id.agreement_cb) CheckBox agreementCheckBox ;
    @BindView(R.id.agreement_tv) HtmlTextView agreementTextView ;
    @BindView(R.id.ktp_photo_iv) ImageView ktpPhotoImageView ;
    @BindView(R.id.ktp_selfie_iv) ImageView ktpSelfieImageView ;

    OptionalAdapter<ProvinceDto> provinceAdapter = null ;
    OptionalAdapter<CityDto> cityAdapter = null ;
    OptionalAdapter<DistrictDto> districtAdapter = null ;
    OptionalAdapter<VillageDto> villageAdapter = null ;

    OptionalAdapter<BankDto> bankAdapter = null ;

    String ktpPhotoPath;
    String selfieKtpPhotoPath;
    ProvinceDto provinceDto ;
    CityDto cityDto ;
    DistrictDto districtDto ;
    VillageDto villageDto ;

    BankDto bankDto ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_account_form;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spanned agrementSpanned = null ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            agrementSpanned = Html.fromHtml(getString(R.string.i_agree_with_working_agreement_of_catcha), Html.FROM_HTML_MODE_LEGACY);
        } else {
            agrementSpanned = Html.fromHtml(getString(R.string.i_agree_with_working_agreement_of_catcha));
        }
        if(agrementSpanned!=null){
            agreementTextView.setText(agrementSpanned);
            CommonView.setTextViewHTMLClickCallback(agreementTextView, getString(R.string.i_agree_with_working_agreement_of_catcha), view -> {
                startActivity(WebViewActivity.class, getString(R.string.Task_Agreement_Link));
            });
        }


        provinceAdapter = new OptionalAdapter(this, provinceDtos);
        provinceAutoCompleteTextView.setAdapter(provinceAdapter);
        provinceAutoCompleteTextView.setThreshold(1);

        cityAdapter = new OptionalAdapter(this, cityDtos);
        cityAutoCompleteTextView.setAdapter(cityAdapter);
        cityAutoCompleteTextView.setThreshold(1);

        districtAdapter = new OptionalAdapter(this, districtDtos);
        districtAutoCompleteTextView.setAdapter(districtAdapter);
        districtAutoCompleteTextView.setThreshold(1);

        villageAdapter = new OptionalAdapter(this, villageDtos);
        villageAutoCompleteTextView.setAdapter(villageAdapter);
        villageAutoCompleteTextView.setThreshold(1);

        provinceAutoCompleteTextView.setOnClickListener(v -> provinceAutoCompleteTextView.showDropDown());
        cityAutoCompleteTextView.setOnClickListener(v -> cityAutoCompleteTextView.showDropDown());
        districtAutoCompleteTextView.setOnClickListener(v -> districtAutoCompleteTextView.showDropDown());
        villageAutoCompleteTextView.setOnClickListener(v -> villageAutoCompleteTextView.showDropDown());

        bankAdapter = new OptionalAdapter(this, bankDtos);
        bankNameAutoCompleteTextView.setAdapter(bankAdapter);
        bankNameAutoCompleteTextView.setThreshold(1);

        bankNameAutoCompleteTextView.setOnClickListener(v -> bankNameAutoCompleteTextView.showDropDown());
        render();
    }

    @Override
    public void fetchCities(Long provinceId, Consumer<List<CityDto>> consumer) {
        super.fetchCities(provinceId, cityDtos1 -> {
            consumer.accept(cityDtos1);
        });
    }

    @Override
    public void fetchDistricts(Long cityId, Consumer<List<DistrictDto>> consumer) {
        super.fetchDistricts(cityId, districtDtos1 -> {
            consumer.accept(districtDtos1);
        });
    }

    @Override
    public void fetchVillages(Long districtId, Consumer<List<VillageDto>> consumer) {
        super.fetchVillages(districtId, villageDtos1 -> {
            consumer.accept(villageDtos1);
        });
    }

    void render(){
        if(user.getVillage()!=null){
            villageDto = user.getVillage();
            districtDto = villageDto.getDistrict();
            cityDto = districtDto.getCity();
            provinceDto = cityDto.getProvince();
        }
        bankDto = user.getBank();
        if(user.getAddress()!=null){
            livingAddressEditText.setText(user.getAddress());
        }
        if(provinceDto!=null){
            provinceAutoCompleteTextView.setText(provinceDto.getName());
            fetchCities(provinceDto.getId(), cityDtos1 -> {
                cityAdapter = OptionalAdapter.reCreate(this, cityDtos1);
                cityAutoCompleteTextView.setAdapter(cityAdapter);
            });
        }
        provinceAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            provinceDto = provinceAdapter.getSelected(position);
            fetchCities(provinceDto.getId(), cityDtos1 -> {
                cityAdapter = OptionalAdapter.reCreate(this, cityDtos1);
                cityAutoCompleteTextView.setThreshold(1);
                cityAutoCompleteTextView.setAdapter(cityAdapter);
                cityDto = null;
                cityAutoCompleteTextView.setText("");
                cityAutoCompleteTextView.setSelection(0);
            });
        });
        provinceAutoCompleteTextView.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if(StringUtils.isEmpty(s.toString())){
                    cityDto = null;
                    cityAutoCompleteTextView.setText("");
                    cityAutoCompleteTextView.setSelection(0);
                }
            }
        });
        if(cityDto!=null){
            cityAutoCompleteTextView.setText(cityDto.getName());
            fetchDistricts(cityDto.getId(), districtDtos -> {
                districtAdapter = OptionalAdapter.reCreate(this, districtDtos);
                districtAutoCompleteTextView.setAdapter(districtAdapter);
            });
        }
        cityAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            cityDto = cityAdapter.getSelected(position);
            Long cityId = cityDto.getId();
            fetchDistricts(cityId, districtDtos -> {
                districtAdapter = OptionalAdapter.reCreate(this, districtDtos);
                districtAutoCompleteTextView.setThreshold(1);
                districtAutoCompleteTextView.setAdapter(districtAdapter);
                districtDto = null;
                districtAutoCompleteTextView.setText("");
                districtAutoCompleteTextView.setSelection(0);
            });
        });
        cityAutoCompleteTextView.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                districtDto = null ;
                districtAutoCompleteTextView.setText("");
                districtAutoCompleteTextView.setSelection(0);

            }
        });
        if(districtDto!=null){
            districtAutoCompleteTextView.setText(districtDto.getName());
            fetchVillages(districtDto.getId(), villageDtos -> {
                villageAdapter = OptionalAdapter.reCreate(this, villageDtos);
                villageAutoCompleteTextView.setAdapter(villageAdapter);
            });
        }
        districtAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            districtDto = districtAdapter.getSelected(position);
            Long districtId = districtDto.getId();
            fetchVillages(districtId, villageDtos -> {
                villageAdapter = OptionalAdapter.reCreate(this, villageDtos);
                villageAutoCompleteTextView.setThreshold(1);
                villageAutoCompleteTextView.setAdapter(villageAdapter);
                villageDto = null ;
                villageAutoCompleteTextView.setText("");
                villageAutoCompleteTextView.setSelection(0);
            });
        });
        districtAutoCompleteTextView.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                villageDto = null ;
                villageAutoCompleteTextView.setText("");
                villageAutoCompleteTextView.setSelection(0);
            }
        });
        if(villageDto!=null){
            villageAutoCompleteTextView.setText(villageDto.getName());
        }
        villageAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            villageDto = villageAdapter.getSelected(position);
            user.setVillage(villageDto);
        });

        if(user.getBirthdate()!=null){
            birthdateEditText.setText(DateFormatUtils.format(user.getBirthdate(), "yyyy/MM/dd"));
        }
        if(bankDto!=null){
            bankNameAutoCompleteTextView.setText(bankDto.getName());
//            bankNameAutoCompleteTextView.setSelection(bankAdapter.mustCurrentPosition(bankDto.getId()));
        }
        if(StringUtils.isNotEmpty(user.getAccountNumber())){
            bankAccountCodeEditText.setText(user.getAccountNumber());
        }
        if(StringUtils.isNotEmpty(user.getAccountName())){
            bankAccountNameEditText.setText(user.getAccountName());
        }
        if(StringUtils.isNotEmpty(user.getIdCardCode())){
            ktpCodeEditText.setText(user.getIdCardCode());
        }
        if(StringUtils.isNotEmpty(user.getIdCard()) && StringUtils.isNotEmpty(user.getIdCardUrl())){
            GlideApp.with(this).load(user.getIdCardUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(ktpPhotoImageView);
        }
        if(StringUtils.isNotEmpty(user.getSelfieIdCard()) && StringUtils.isNotEmpty(user.getSelfieIdCardUrl())){
            GlideApp.with(this).load(user.getSelfieIdCardUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(ktpSelfieImageView);
        }
        bankNameAutoCompleteTextView.setOnItemClickListener((parent, view, position, id) -> {
            bankDto = bankAdapter.getSelected(position);
            user.setBank(bankDto);
        });
    }

    @Override
    public void renderData() {

    }

    @OnClick({R.id.birthdate_et, R.id.ktp_photo_iv,R.id.ktp_selfie_iv, R.id.submit_button})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(view.getId()==R.id.birthdate_et){
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                user.setBirthdate(calendar.getTime());
                birthdateEditText.setText(DateFormatUtils.format(calendar.getTime(), "yyyy/MM/dd"));
            });
            datePickerDialog.setMaxDate(Calendar.getInstance());
            datePickerDialog.show(this.getFragmentManager(), DatePickerDialog.class.getName()+"Birthdate");
        }
        if(view.getId()==R.id.ktp_photo_iv){
            new RxPermissions(this).request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(aBoolean -> {
                if(aBoolean){
                    getWindow().getDecorView().postDelayed(() -> {
                        startActivityForResult(KTPCameraActivity.class, Constant.REQUEST_KTP_PHOTO);
                    }, 500);
                }else{
                    showAlert(getString(R.string.Failed), getString(R.string.permission_deniedd));
                }
            }, throwable -> {
                throwable.printStackTrace();
            });
        }
        if(view.getId()==R.id.ktp_selfie_iv){
            new RxPermissions(this).request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(aBoolean -> {
                if(aBoolean){
                    getWindow().getDecorView().postDelayed(() -> {
                        startActivityForResult(SelfieKTPCameraActivity.class, Constant.REQUEST_SELFIE_KTP_PHOTO);
                    }, 500);
                }else{
                    showAlert(getString(R.string.Failed), getString(R.string.permission_deniedd));
                }
            }, throwable -> {
                throwable.printStackTrace();
            });
        }
        if(view.getId()==R.id.submit_button){
            if(!InputTools.isComplete(livingAddressEditText)){
                livingAddressEditText.setError(getString(R.string.PleaseEnter, livingAddressEditText.getHint().toString().toLowerCase()));
                return;
            }
            if(provinceDto==null){
                provinceAutoCompleteTextView.setError(getString(R.string.PleaseEnter, provinceAutoCompleteTextView.getHint().toString().toLowerCase()));
                return;
            }
            if(cityDto==null){
                cityAutoCompleteTextView.setError(getString(R.string.PleaseEnter, cityAutoCompleteTextView.getHint().toString().toLowerCase()));
                return;
            }
            if(districtDto==null){
                districtAutoCompleteTextView.setError(getString(R.string.PleaseEnter, districtAutoCompleteTextView.getHint().toString().toLowerCase()));
                return;
            }
            if(villageDto==null){
                villageAutoCompleteTextView.setError(getString(R.string.PleaseEnter, villageAutoCompleteTextView.getHint().toString().toLowerCase()));
                return;
            }
            if(user.getBirthdate()==null){
                birthdateEditText.setError(getString(R.string.PleaseEnter, birthdateEditText.getHint().toString().toLowerCase()));
                return;
            }
            if(bankDto==null){
                bankNameAutoCompleteTextView.setError(getString(R.string.PleaseEnter, bankNameAutoCompleteTextView.getHint().toString().toLowerCase()));
                return;
            }
            if(!InputTools.isComplete(bankAccountCodeEditText)){
                bankNameAutoCompleteTextView.setError(getString(R.string.PleaseEnter, bankAccountCodeEditText.getHint().toString().toLowerCase()));
                return;
            }
            if(!InputTools.isComplete(bankAccountNameEditText)){
                bankAccountNameEditText.setError(getString(R.string.PleaseEnter, bankAccountNameEditText.getHint().toString().toLowerCase()));
                return;
            }
            if(!InputTools.isComplete(ktpCodeEditText)){
                ktpCodeEditText.setError(getString(R.string.PleaseEnter, ktpCodeEditText.getHint().toString().toLowerCase()));
                return;
            }
            if(StringUtils.isEmpty(ktpPhotoPath) && StringUtils.isEmpty(user.getIdCard())){
                infoDialog(getString(R.string.PleaseEnter, "Foto KTP anda"), DIALOG_WARNING, () -> true);
                return;
            }
            if(StringUtils.isEmpty(selfieKtpPhotoPath) && StringUtils.isEmpty(user.getSelfieIdCard())){
                infoDialog(getString(R.string.PleaseEnter, "Foto Selfie dengan KTP anda"), DIALOG_WARNING, () -> true);
                return;
            }

            if(!agreementCheckBox.isChecked()){
                infoDialog(getString(R.string.PleaseAcceptWorkingAgreement), DIALOG_WARNING, () -> true);
                return;
            }
            confirmDialog(getString(R.string.Confirmation), getString(R.string.DoYouWantToUpdateYourProfileData),CONFIRM_GENERAL, aBoolean -> {
                if(aBoolean){
                    user.setAddress(livingAddressEditText.getText().toString());
                    user.setAccountNumber(bankAccountCodeEditText.getText().toString());
                    user.setAccountName(bankAccountNameEditText.getText().toString());
                    user.setIdCardCode(ktpCodeEditText.getText().toString());

                    MultipartBody.Part idCardMultipartFile  = null ;
                    if(ktpPhotoPath!=null){
                        idCardMultipartFile = CommonUtil.generateMultiPart(this, "idCardMultipartFile", Uri.fromFile(new File(ktpPhotoPath)));
                    }
                    MultipartBody.Part selfieIdCardMultipartFile  = null ;
                    if(selfieKtpPhotoPath!=null){
                        selfieIdCardMultipartFile = CommonUtil.generateMultiPart(this, "selfieIdCardMultipartFile", Uri.fromFile(new File(selfieKtpPhotoPath)));
                    }
                    ViewProgress.show(this);
                    grabServerData(Kitchen.arange(this, UserApi.class).saveUpload(currentAuthorization, RequestBody.create(MediaType.parse("text/plain"), gson.toJson(user)), null, idCardMultipartFile, selfieIdCardMultipartFile), (wsResponse, response) -> {
                        if(wsResponse.isSuccess()){
                            this.user = wsResponse.getData();
                            FastSave.getInstance().saveObject(Constant.USER, this.user);
                            ViewProgress.dismiss();
                            infoDialog(wsResponse.getMessage(), DIALOG_SUCCESS, () -> {
                                return true ;
                            });
                        }else{
                            showAlert(wsResponse);
                            ViewProgress.dismiss();
                        }
                    }, (aBoolean1, throwable, response) -> {
                        ViewProgress.dismiss();
                        showAlert(getString(R.string.Failed), throwable.getMessage());
                    });
                }else{

                }
                return true ;
            });


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==Constant.REQUEST_KTP_PHOTO && resultCode==Constant.RESULT_FINISH && data!=null){
            ktpPhotoPath = data.getStringExtra(Constant.FILE_PATH);
            GlideApp.with(this).load(ktpPhotoPath).diskCacheStrategy(DiskCacheStrategy.ALL).into(ktpPhotoImageView);
        }
        if(requestCode==Constant.REQUEST_SELFIE_KTP_PHOTO && resultCode==Constant.RESULT_FINISH && data!=null){
            selfieKtpPhotoPath = data.getStringExtra(Constant.FILE_PATH);
            GlideApp.with(this).load(selfieKtpPhotoPath).diskCacheStrategy(DiskCacheStrategy.ALL).into(ktpSelfieImageView);

        }
    }

}
