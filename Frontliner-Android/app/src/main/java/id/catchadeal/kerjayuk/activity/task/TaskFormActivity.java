package id.catchadeal.kerjayuk.activity.task;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.annotations.BindExtra;

import org.apache.commons.lang.time.DateFormatUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.misc.WebViewActivity;
import id.catchadeal.kerjayuk.adapter.TaskFormAdapter;
import id.catchadeal.kerjayuk.api.TaskApplicationApi;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.fragment.TaskFormFragment;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.business.FormLaneApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskApplicationDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.model.formbuilder.FormLane;
import id.catchadeal.kerjayuk.utils.CommonView;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.utils.FormIntegration;
import id.catchadeal.kerjayuk.utils.NumberUtil;
import id.catchadeal.kerjayuk.view.FormInputView;
import id.catchadeal.kerjayuk.view.ViewProgress;
import lombok.Getter;
import lombok.Setter;

public class TaskFormActivity extends BaseActivity {


    public static final String TAG = TaskFormActivity.class.getName();

    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;

    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.viewpager) ViewPager viewPager ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;

    @BindExtra String taskDtoGson ;
    TaskDto taskDto ;

    @BindExtra String taskApplicationDtoGson ;
    TaskApplicationDto taskApplicationDto  ;

    TaskFormAdapter taskFormAdapter = null ;


    @Override
    public String getTAG() {
        return TAG;
    }


    @Override
    public int contentXmlLayout() {
        return R.layout.activity_task_form;
    }
    @Override
    public void renderData() {

        GlideApp.with(this).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(partnerImageView);
        feeTextView.setText(NumberUtil.moneyFormat(taskDto.getFee(), true));
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            taskFeeTypeTextView.setText(R.string.PerTask);
        }
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            taskFeeTypeTextView.setText(R.string.PerAgent);
        }

        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());

        int currentItem = viewPager.getCurrentItem();
        if(currentItem<taskDto.getGroupForm().getFormLanes().size()-1){
            submitButton.setText(R.string.NextStep);
        }else{
            submitButton.setText(R.string.Send_The_Work);
        }
    }

    @OnClick(R.id.submit_button)
    @Override
    public void onClick(View view) {
        super.onClick(view);
        int currentItem = viewPager.getCurrentItem();
        if(currentItem<taskDto.getGroupForm().getFormLanes().size()-1){
            if(!taskFormAdapter.getTaskFormFragments()[currentItem].validateInput()){
                return;
            }else{
                viewPager.setCurrentItem(currentItem+1);
            }
//            viewPager.setCurrentItem(currentItem+1);
        }else{
            if(!taskFormAdapter.getTaskFormFragments()[currentItem].validateInput()){
                return;
            }
            confirmDialog(getString(R.string.Confirmation), getString(R.string.Are_you_sure_to_submit_this_task), CONFIRM_GENERAL, aBoolean -> {
                if(aBoolean){
                    List<FormLaneApplicationDto> formLaneApplicationDtos = new ArrayList<>();
                    for (int i = 0; i < taskFormAdapter.getTaskFormFragments().length; i++) {
                        TaskFormFragment taskFormFragment = taskFormAdapter.getTaskFormFragments()[i];
                        FormLane formLane = taskFormFragment.getFormLane();
                        FormLaneApplicationDto formLaneApplicationDto = new FormLaneApplicationDto();
                        formLaneApplicationDto.setTitle(formLane.getTitle());
                        taskFormFragment.getFormInputViewHashMap().forEach((s, formInputView) -> {
                            FormApplicationDto formApplicationDto = formInputView.getFormApplicationDto();
                            formLaneApplicationDto.getFormApplications().add(formApplicationDto);
                        });
                        formLaneApplicationDtos.add(formLaneApplicationDto);
                    }
                    taskApplicationDto.setTask(taskDto);
                    taskApplicationDto.setGroupFormName(taskDto.getGroupForm().getName());
                    taskApplicationDto.setFormLaneApplications(formLaneApplicationDtos);

                    ViewProgress.show(this);
                    grabServerData(Kitchen.arange(this, TaskApplicationApi.class).commit(currentAuthorization, taskApplicationDto), (wsResponse, response) -> {
                        ViewProgress.dismiss();
                        if(wsResponse.isSuccess()){
                            infoDialog(wsResponse.getMessage(), DIALOG_SUCCESS,() -> {
                                setCurrentResult(Constant.RESULT_FINISH);
                                return true ;
                            });
                        }else{
                            showAlert(wsResponse);
                        }
                    }, (aBoolean1, throwable, response) -> {
                        ViewProgress.dismiss();
                        showAlert(getString(R.string.Failed), throwable.getMessage());
                    });
                }
                return true ;
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        taskDto = gson.fromJson(taskDtoGson, TaskDto.class);
        taskApplicationDto = gson.fromJson(taskApplicationDtoGson, TaskApplicationDto.class);
        taskFormAdapter = new TaskFormAdapter(this, taskDto);
        viewPager.setAdapter(taskFormAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                taskFormAdapter.getTaskFormFragments()[position].onShow();
                if(position<taskDto.getGroupForm().getFormLanes().size()){
                    submitButton.setText(R.string.Send_The_Work);
                }else{
                    submitButton.setText(R.string.NextStep);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            int currentItem = viewPager.getCurrentItem();
            if(currentItem>0){
                viewPager.setCurrentItem(currentItem-1);
                return false ;
            }else{
                return super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);

    }
}
