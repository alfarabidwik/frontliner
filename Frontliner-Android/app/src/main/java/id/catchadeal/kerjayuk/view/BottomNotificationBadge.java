package id.catchadeal.kerjayuk.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;

public class BottomNotificationBadge extends LinearLayout {


    public BottomNotificationBadge(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View childView = LayoutInflater.from(getContext()).inflate(R.layout.bottom_notification_badge, null);
        this.addView(childView);
    }


    void refetch(){

    }


}

