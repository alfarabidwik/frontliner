package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.misc.BannerDto;
import id.catchadeal.kerjayuk.model.payment.BankDto;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BannerApi {

    @GET("api/banners")
    Observable<WSResponse<List<BannerDto>, String>> banners(@Query("active") Boolean active, @Query("ascending") Boolean ascending, @Query("sortir") String sortir);


}
