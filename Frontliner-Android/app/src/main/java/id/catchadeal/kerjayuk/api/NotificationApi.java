package id.catchadeal.kerjayuk.api;

import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.blog.Article;
import id.catchadeal.kerjayuk.model.misc.NotificationDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface NotificationApi {

    @GET("api/notifications")
    Observable<WSResponse<List<NotificationDto>, String>> notifications(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);

}
