package id.catchadeal.kerjayuk.holder;

import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.WelcomeActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.model.Dot;
import id.catchadeal.kerjayuk.utils.SliderControl;

public class DotHolder<S extends BaseActivity & SliderControl> extends ACTViewHolder<S, Dot, String> {

    @BindView(R.id.imageview) ImageView imageView ;

    public DotHolder(WelcomeActivity activity, ViewGroup viewGroup) {
        super((S) activity, R.layout.holder_dot, viewGroup);
    }

    public DotHolder(DashboardActivity activity, ViewGroup viewGroup) {
        super((S) activity, R.layout.holder_dot, viewGroup);
    }

    @Override
    public void showData(Dot object) {
        super.showData(object);
        if(getAct().getPosition()==getAdapterPosition()){
            imageView.setImageResource(R.drawable.dot_active);
        }else{
            imageView.setImageResource(R.drawable.dot_inactive);
        }
    }

    @Override
    public void find(String findParam) {

    }
}
