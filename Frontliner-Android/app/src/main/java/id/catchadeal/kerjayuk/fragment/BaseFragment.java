package id.catchadeal.kerjayuk.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alfarabi.alfalibs.fragments.api.APIBaseFragment;
import com.google.gson.Gson;

import butterknife.ButterKnife;
import id.catchadeal.kerjayuk.KerjayukApplication;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import lombok.Getter;

public abstract class BaseFragment extends APIBaseFragment {

    @Getter
    protected Gson gson ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        gson = KerjayukApplication.gson ;
        return inflater.inflate(contentXmlLayout(), null);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    public <ACT extends BaseActivity> ACT getAct(Class<ACT> actClass){
        return (ACT)getActivity();
    }
}
