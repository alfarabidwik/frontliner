package id.catchadeal.kerjayuk.activity.user;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.appizona.yehiahd.fastsave.FastSave;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.stream.HttpGlideUrlLoader;
import com.bumptech.glide.request.RequestOptions;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.commons.lang.StringUtils;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.camera.CameraActivity;
import id.catchadeal.kerjayuk.api.UserApi;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.MediaDialogFragment;
import id.catchadeal.kerjayuk.view.ViewProgress;
import okhttp3.MultipartBody;

public class UserPhotoProfileActivity extends BaseActivity {
    
    public static final String TAG = UserPhotoProfileActivity.class.getName();

    @BindView(R.id.profile_iv) ImageView circleImageView ;
    @BindView(R.id.save_button) Button saveButton ;
    @BindView(R.id.take_picture_button) ImageView takeImageView ;

    private String filePath = null ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_user_photo_profile;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        saveButton.setEnabled(false);
        if(StringUtils.isNotEmpty(user.getPhotoUrl())){
            takeImageView.setVisibility(View.GONE);
            GlideApp.with(this)
                    .load(user.getPhotoUrl())
                    .apply(new RequestOptions().set(HttpGlideUrlLoader.TIMEOUT, 30000))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_user_transparent).into(circleImageView);

        }else{
            takeImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void renderData() {
    }

    @OnClick({R.id.camera_button, R.id.profile_iv, R.id.save_button})
    @Override
    public void onClick(View view) {
        super.onClick(view);
        if(view.getId()==R.id.camera_button || view.getId()==R.id.profile_iv){

            MediaDialogFragment.instance().show(getSupportFragmentManager().beginTransaction(), MediaDialogFragment.TAG, viewCallback -> {
                if(viewCallback.getId()==R.id.camera_layout){
                    takePermission(aBoolean -> {
                        if(aBoolean){
                            startActivityForResult(CameraActivity.class, Constant.REQUEST_PHOTO);
                        }else{
                            Toast.makeText(this, getString(R.string.permission_deniedd), Toast.LENGTH_SHORT).show();
                        }
                    }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
                }
                if(viewCallback.getId()==R.id.gallery_layout){
                    takePermission(aBoolean -> {
                        if(aBoolean){
                            String action = null;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                action = Intent.ACTION_OPEN_DOCUMENT;
                            } else {
                                action = Intent.ACTION_PICK;
                            }
                            Intent galleryIntent = new Intent(action, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            galleryIntent.setType("image/*");
                            startActivityForResult(galleryIntent , Constant.REQUEST_GALLERY );
                        }else{
                            Toast.makeText(this, getString(R.string.permission_deniedd), Toast.LENGTH_SHORT).show();
                        }
                    }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                 }
            });
        }
        if(view.getId()==R.id.save_button){
            if(StringUtils.isNotEmpty(filePath)){
                MultipartBody.Part multipartFile  = CommonUtil.generateMultiPart(this, "multipartFile", Uri.fromFile(new File(filePath)));
//                RequestBody RejectedRequestBody = (MimeType.valueOf("text/plain").createRequestBody(Kitchen.getGson().toJson(rejectTask)));
                ViewProgress.show(this);
                grabServerData(Kitchen.arange(this, UserApi.class).uploadImage(currentAuthorization, user.getId(), id.catchadeal.kerjayuk.util.Constant.PHOTO, multipartFile), (wsResponse, response) -> {
                    ViewProgress.dismiss();
                    showAlert(wsResponse);
                    if(wsResponse.isSuccess()){
                        FastSave.getInstance().saveObject(Constant.USER, wsResponse.getData());
                        initPreference();
                    }
                }, (aBoolean, throwable, response) -> {
                    ViewProgress.dismiss();
                    throwable.printStackTrace();
                    showAlert(getString(R.string.Failed), throwable.getMessage());
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==Constant.REQUEST_PHOTO && resultCode==Constant.RESULT_FINISH && data!=null){
            filePath = data.getStringExtra(Constant.FILE_PATH);
            GlideApp.with(this).load(filePath).diskCacheStrategy(DiskCacheStrategy.ALL).into(circleImageView);

        }
        if(requestCode==Constant.REQUEST_GALLERY && data!=null){
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                filePath = resultUri.getPath();
                GlideApp.with(this).load(filePath).diskCacheStrategy(DiskCacheStrategy.ALL).into(circleImageView);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        if(StringUtils.isNotEmpty(filePath)) {
            saveButton.setEnabled(true);
        }
    }
}
