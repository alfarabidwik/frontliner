package id.catchadeal.kerjayuk.adapter;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.fragment.TaskFormFragment;
import id.catchadeal.kerjayuk.fragment.WelcomeFragment;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import lombok.Getter;

public class TaskFormAdapter extends BaseTabPagerAdapter<TaskFormFragment> {

    private BaseActivity baseActivity ;
    private TaskDto taskDto ;
    @Getter TaskFormFragment[] taskFormFragments = null ;

    public TaskFormAdapter(BaseActivity baseActivity, TaskDto taskDto) {
        super(baseActivity.getSupportFragmentManager());
        this.baseActivity = baseActivity ;
        this.taskDto = taskDto;
        int totalCount = taskDto.getGroupForm().getFormLanes().size();
        this.taskFormFragments = new TaskFormFragment[taskDto.getGroupForm().getFormLanes().size()];
        for (int i = 0; i < totalCount; i++) {
            taskFormFragments[i] = TaskFormFragment.instance(taskDto, i);
        }
    }

    @Override
    public String[] titles() {
        String[] titles = taskDto.getGroupForm().getFormLanes().stream().map(formLane -> formLane.getTitle()).toArray(String[]::new);
        return titles;
    }

    @Override
    public TaskFormFragment[] fragmentClasses() {
        return taskFormFragments ;
    }

}
