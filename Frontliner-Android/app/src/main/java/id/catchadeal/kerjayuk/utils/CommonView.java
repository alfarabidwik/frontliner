package id.catchadeal.kerjayuk.utils;

import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.WLog;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.model.blog.Article;
import id.catchadeal.kerjayuk.model.business.TaskApplicationStatusDto;
import id.catchadeal.kerjayuk.model.business.TaskDto;

public class CommonView {

    private static void makeLinkClickable(Context context, SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                WLog.d("ANEH", "MASUK GAK ");
                new FinestWebView.Builder(context).show(span.getURL());
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }
    private static void makeLinkClickable(Context context, SpannableStringBuilder strBuilder, final URLSpan span, Consumer<View> viewConsumer) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                viewConsumer.accept(view);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    public static void setTextViewHTML(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, html.length(), URLSpan.class);
        for(URLSpan span : urls) {
            makeLinkClickable(text.getContext(), strBuilder, span);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void setTextViewHTMLClickCallback(TextView text, String html, Consumer<View> viewConsumer) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, html.length(), URLSpan.class);
        for(URLSpan span : urls) {
            makeLinkClickable(text.getContext(), strBuilder, span, viewConsumer);
        }
        text.setText(strBuilder);
        text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static List<TaskDto> tasksLoader(){
        List<TaskDto> taskDtos = new ArrayList<>();
        taskDtos.add(new TaskDto());
        taskDtos.add(new TaskDto());
        taskDtos.add(new TaskDto());
        taskDtos.add(new TaskDto());
        return taskDtos ;
    }

    public static List<Article> articleLoader(){
        List<Article> articles = new ArrayList<>();
        articles.add(new Article());
        articles.add(new Article());
        articles.add(new Article());
        articles.add(new Article());
        return articles ;
    }

    public static HashMap<Long, Integer> taskApplicationStatusPassedIcon ;
    public static HashMap<Long, Integer> taskApplicationStatusNotPassedIcon ;


    public static List<TaskApplicationStatusDto> taskApplicationStatusDtos = null ;


    static {
        taskApplicationStatusPassedIcon = new HashMap<>();
        taskApplicationStatusPassedIcon.put(TaskApplicationStatusDto.TAKEN_ID, R.drawable.ic_pekerjaan_divalidasi_green);
        taskApplicationStatusPassedIcon.put(TaskApplicationStatusDto.SUBMITTED_ID, R.drawable.ic_pekerjaan_dikirim_green);
        taskApplicationStatusPassedIcon.put(TaskApplicationStatusDto.ACCEPTED_ID, R.drawable.ic_pekerjaan_berhasil_green);
        taskApplicationStatusPassedIcon.put(TaskApplicationStatusDto.REJECTED_ID, R.drawable.ic_pekerjaan_reject);

        taskApplicationStatusNotPassedIcon = new HashMap<>();
        taskApplicationStatusNotPassedIcon.put(TaskApplicationStatusDto.TAKEN_ID, R.drawable.ic_pekerjaan_divalidasi_gray);
        taskApplicationStatusNotPassedIcon.put(TaskApplicationStatusDto.SUBMITTED_ID, R.drawable.ic_pekerjaan_dikirim_gray);
        taskApplicationStatusNotPassedIcon.put(TaskApplicationStatusDto.ACCEPTED_ID, R.drawable.ic_pekerjaan_berhasil_gray);
        taskApplicationStatusNotPassedIcon.put(TaskApplicationStatusDto.REJECTED_ID, R.drawable.ic_pekerjaan_reject);


        taskApplicationStatusDtos = new ArrayList<>();
        taskApplicationStatusDtos.add(new TaskApplicationStatusDto(TaskApplicationStatusDto.TAKEN_ID));
        taskApplicationStatusDtos.add(new TaskApplicationStatusDto(TaskApplicationStatusDto.SUBMITTED_ID));
        taskApplicationStatusDtos.add(new TaskApplicationStatusDto(TaskApplicationStatusDto.ACCEPTED_ID));
        taskApplicationStatusDtos.add(new TaskApplicationStatusDto(TaskApplicationStatusDto.REJECTED_ID));
    }

}
