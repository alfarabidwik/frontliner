package id.catchadeal.kerjayuk.activity.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.Kitchen;
import com.alfarabi.alfalibs.tools.WLog;
import com.appizona.yehiahd.fastsave.FastSave;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.mukesh.OtpView;

import org.apache.commons.lang.StringUtils;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.OnClick;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.user.DashboardActivity;
import id.catchadeal.kerjayuk.api.SignAuthApi;
import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.user.UserDto;
import id.catchadeal.kerjayuk.utils.Constant;
import id.catchadeal.kerjayuk.view.ViewProgress;

public class OTPActivity extends BaseActivity {

    public static final String TAG = OTPActivity.class.getName();

    @BindView(R.id.otp_view) OtpView otpView ;
    @BindView(R.id.request_button) TextView requestButton ;
    @BindView(R.id.timer_tv) HtmlTextView timerTextView ;
    @BindView(R.id.otp_instruction_tv) TextView otpInstructionTextView ;
    @BindExtra String changePhoneNumberAuthorization ;
    @BindExtra String email ;
    @BindExtra String mobilePhone ;

    private String verificationId ;

    private Timer timer ;
    private int second = 30 ;

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_otp;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        smsWaiter();
        requestSmsCode();
        otpInstructionTextView.setText(getString(R.string.EnterACodeThatWeveSentToYou, mobilePhone));
        otpView.setOtpCompletionListener(otp -> {
            if(StringUtils.isNotEmpty(verificationId)){
                if(StringUtils.isNotEmpty(otpView.getText().toString())){
                    PhoneAuthCredential authCredential = PhoneAuthProvider.getCredential(verificationId, otpView.getText().toString());
                    signInWithPhoneAuthCredential(authCredential);
                }
            }
        });
    }

    void smsWaiter(){
        if(timer==null){
            timer = new Timer();
        }else{
            try{
                timer.cancel();
            }catch (Exception e){ }
            timer = new Timer();
        }
        second = 30;
        timerTextView.setHtml(getString(R.string.SMSTextTimerWaitingNotice, String.valueOf(second)+" Detik"));
        requestButton.setVisibility(View.GONE);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    timerTextView.setHtml(getString(R.string.SMSTextTimerWaitingNotice, String.valueOf(--second)+" Detik"));
                    if(second<=0){
                        requestButton.setVisibility(View.VISIBLE);
                        timer.cancel();
                    }
                });
            }
        }, 1000, 1000);
    }


    @Override
    public void renderData() {

    }

    @OnClick(R.id.request_button)
    public void onClick(){
        requestSmsCode();
        smsWaiter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if(second>0){
                return false ;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void requestSmsCode(){
        otpView.setText("");
        WLog.d(TAG, "Mobile PHone = "+mobilePhone);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(mobilePhone, 60, TimeUnit.SECONDS,
                this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        WLog.d(TAG, "onVerificationCompleted : "+phoneAuthCredential);
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        e.printStackTrace();
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            Intent intent = getIntent();
                            intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, ((FirebaseAuthInvalidCredentialsException) e).getErrorCode())));
                            setCurrentResult(Constant.RESULT_FINISH, intent);
                            return;
                        }
                        showAlert((WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, e.getMessage())));
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        WLog.d(TAG, "onCodeSent:" + verificationId);
                        OTPActivity.this.verificationId = verificationId ;
                        super.onCodeSent(verificationId, forceResendingToken);
                    }
                });
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        WLog.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = task.getResult().getUser();
                        if(StringUtils.isNotEmpty(changePhoneNumberAuthorization)){
                            ViewProgress.show(this);
                            grabServerData(Kitchen.arange(this, SignAuthApi.class).applyMobilePhoneAndLogin(changePhoneNumberAuthorization, mobilePhone,deviceId, currentFirebaseToken, Constant.ANDROID), (wsResponse, response) -> {
                                ViewProgress.dismiss();
                                if(wsResponse.isSuccess()){
                                    UserDto userDto = wsResponse.getData();
                                    setCurrentResult(Constant.RESULT_FINISH);
                                    FastSave.getInstance().saveString(Constant.AUTHORIZATION, userDto.getAuthorization());
                                    FastSave.getInstance().saveObject(Constant.USER, userDto);
                                    Kitchen.putHanger(Constant.HEADER_AUTHORIZATION, userDto.getAuthorization());
                                    Intent i = new Intent(this, DashboardActivity.class);        // Specify any activity here e.g. home or splash or login etc
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("EXIT", true);
                                    startActivity(i);
                                    finish();
                                }else{
                                    Intent intent = getIntent();
                                    intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(wsResponse));
                                    setCurrentResult(Constant.RESULT_FINISH, intent);
                                }
                            }, (aBoolean, throwable, response) -> {
                                ViewProgress.dismiss();
                                Intent intent = getIntent();
                                intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, throwable.getMessage())));
                                setCurrentResult(Constant.RESULT_FINISH, intent);
                            });
                        }else{
                            inOrUp();
                        }
                    } else {
                        WLog.d(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            task.getException().printStackTrace();
                            showAlert(getString(R.string.Failed), task.getException().getMessage());
                            return;
                            // The verification code entered was invalid
                        }
                        if (task.getException() !=null ) {
                            Intent intent = getIntent();
                            intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, task.getException().getMessage())));
                            setCurrentResult(Constant.RESULT_FINISH, intent);
                            return;
                        }

                    }
                }).addOnFailureListener(e -> {

                e.printStackTrace();
                    if (e instanceof FirebaseAuthInvalidCredentialsException) {
                        return;
                    }
                    Intent intent = getIntent();
                    intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, e.getMessage())));
                    setCurrentResult(Constant.RESULT_FINISH, intent);
                });
    }

    public void inOrUp(){
        ViewProgress.show(this);
        grabServerData(Kitchen.arange(this, SignAuthApi.class).inOrUp(mobilePhone, deviceId, currentFirebaseToken, Constant.ANDROID), (wsResponse, response) -> {
            ViewProgress.dismiss();
            if(wsResponse.isSuccess()){
                if(wsResponse.getData()!=null && StringUtils.isNotEmpty(wsResponse.getData().getRegistrationToken())){
                    String userDtoString = gson.toJson(wsResponse.getData());
                    startActivityForResult(RegistrationActivity.class, Constant.REQUEST_REGISTRATION, userDtoString);
                }else{
                    UserDto userDto = wsResponse.getData();
                    setCurrentResult(Constant.RESULT_FINISH);
                    FastSave.getInstance().saveString(Constant.AUTHORIZATION, userDto.getAuthorization());
                    FastSave.getInstance().saveObject(Constant.USER, userDto);
//                    startActivity(DashboardActivity.class);
                    Intent i = new Intent(this, DashboardActivity.class);        // Specify any activity here e.g. home or splash or login etc
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("EXIT", true);
                    startActivity(i);
                    finish();

                }
            }else{
                Intent intent = getIntent();
                intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(wsResponse));
                setCurrentResult(Constant.RESULT_FINISH, intent);
            }
        }, (aBoolean, throwable, response) -> {
            throwable.printStackTrace();
            ViewProgress.dismiss();
            Intent intent = getIntent();
            intent.putExtra(Constant.MESSAGE_JSON, gson.toJson(WSResponse.instance(id.catchadeal.kerjayuk.util.Constant.FAILED_CODE, throwable.getMessage())));
            setCurrentResult(Constant.RESULT_FINISH, intent);
        });

    }

}
