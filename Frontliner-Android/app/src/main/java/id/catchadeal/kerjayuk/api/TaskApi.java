package id.catchadeal.kerjayuk.api;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import id.catchadeal.kerjayuk.model.WSResponse;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.Constant;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TaskApi {

    @GET(Constant.BASE_API+"/task")
    Observable<WSResponse<TaskDto, String>> task(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("id") Long id);

    @GET(Constant.BASE_API+"/tasks")
    Observable<WSResponse<List<TaskDto>, String>> tasks(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("publisher") String publisher,
            @Query("keyword") String keyword,
            @Query("published") Boolean published,
            @Query("taskCategoryId") Long taskCategoryId,
            @Query("feeStart") BigDecimal feeStart,
            @Query("feeEnd") BigDecimal feeEnd,
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);

    @GET(Constant.BASE_API+"/tasks/published")
    Observable<WSResponse<List<TaskDto>, String>> published(
            @Header(Constant.HEADER_AUTHORIZATION) String authorization,
            @Query("publisher") String publisher,
            @Query("keyword") String keyword,
            @Query("taskCategoryId") Long taskCategoryId,
            @Query("feeStart") BigDecimal feeStart,
            @Query("feeEnd") BigDecimal feeEnd,
            @Query("startDate") String startDate,
            @Query("endDate") String endDate,
            @Query("workerVerificationType") String workerVerificationType,
            @Query("suggestionMode") Boolean suggestionMode,
            @Query("page") Integer page,
            @Query("ascending") Boolean ascending,
            @Query("sortir") String sortir);


}
