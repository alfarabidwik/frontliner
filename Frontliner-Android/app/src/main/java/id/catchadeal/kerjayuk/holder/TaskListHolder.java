package id.catchadeal.kerjayuk.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.ACTViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.activity.task.TaskDetailActivity;
import id.catchadeal.kerjayuk.activity.task.TaskListActivity;
import id.catchadeal.kerjayuk.model.business.TaskDto;
import id.catchadeal.kerjayuk.utils.NumberUtil;

public class TaskListHolder extends ACTViewHolder<BaseActivity, TaskDto, String> {

    @BindView(R.id.partner_iv) ImageView partnerImageView ;
    @BindView(R.id.fee_tv) TextView feeTextView ;
    @BindView(R.id.partner_name_tv) TextView partnerNameTextView ;
    @BindView(R.id.title_tv) TextView titleTextView ;
    @BindView(R.id.category_tv) TextView categoryTextView ;
    @BindView(R.id.card_container) View cardContainer ;
    @BindView(R.id.task_fee_type_tv) TextView taskFeeTypeTextView ;



    public TaskListHolder(TaskListActivity activity, ViewGroup viewGroup) {
        super(activity, R.layout.holder_task_list, viewGroup);
    }

    @Override
    public void showData(TaskDto taskDto) {
        super.showData(taskDto);

        GlideApp.with(getAct()).load(taskDto.getPartner().getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_launcher).into(partnerImageView);
        feeTextView.setText(NumberUtil.moneyFormat(taskDto.getFee(), true));
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REGULAR_TASK)){
            taskFeeTypeTextView.setText(R.string.PerTask);
        }
        if(taskDto.getType().equalsIgnoreCase(TaskDto.REFERRAL_AGENT_TASK)){
            taskFeeTypeTextView.setText(R.string.PerAgent);
        }

        partnerNameTextView.setText(taskDto.getPartner().getFullName());
        titleTextView.setText(taskDto.getTitle());
        categoryTextView.setText(taskDto.getTaskCategory().getName());

        cardContainer.setOnClickListener(v -> {
            getAct().startActivity(TaskDetailActivity.class, getAct().getGson().toJson(taskDto), true);
        });
    }

    @Override
    public void find(String findParam) {

    }
}
