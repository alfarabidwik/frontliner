package id.catchadeal.kerjayuk.activity.user;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.codekidlabs.storagechooser.StorageChooser;

import org.apache.commons.lang.StringUtils;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.utils.ScreenshotUtil;
import id.catchadeal.kerjayuk.utils.Utils;

public class IdCardActivity extends BaseActivity {
    
    public static final String TAG = IdCardActivity.class.getName();
    @BindView(R.id.id_card_layout) RelativeLayout idCardLayout ;
    @BindView(R.id.profile_iv) ImageView profileImageView ;
    @BindView(R.id.referral_code_tv) TextView referralCodeTextView ;

    @BindView(R.id.qrcode_iv) ImageView qrCodeImageView ;

    StorageChooser chooser = null ;

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void renderData() {

    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_id_card;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            // Getting QR-Code as Bitmap
            QRGEncoder qrgEncoder = new QRGEncoder(user.getAgentCode(), null, QRGContents.Type.TEXT, 120);
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            qrCodeImageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        referralCodeTextView.setText(user.getAgentCode());
        if(StringUtils.isNotEmpty(user.getPhotoUrl())){
            GlideApp.with(this).load(user.getPhotoUrl()).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);
        }
        chooser = new StorageChooser.Builder()
                .withActivity(this)
                .withFragmentManager(getFragmentManager())
                .disableMultiSelect()
                .allowCustomPath(true)
                .setType(StorageChooser.DIRECTORY_CHOOSER)
                .withMemoryBar(true)
                .build();

        chooser.setOnSelectListener(s -> {
//            Bitmap bitmap = Utils.getIdCard(idCardLayout);
            takePermission(aBoolean -> {
                if(aBoolean){
                    Bitmap bitmap = ScreenshotUtil.getInstance().takeScreenshotForView(idCardLayout);
                    Utils.persistImage(this, s, bitmap, user.getAgentCode(), file -> {
                        infoDialog(getString(R.string.YourIdCardHasBeenSavedIn, file.getPath()), DIALOG_SUCCESS, () -> true);
                    });
                }else{
                    showAlert(getString(R.string.permission_deniedd), getString(R.string.to_use_this_apps_you_must_accept_permission));
                }
            }, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.idcard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.download){
            chooser.show();
        }
        return super.onOptionsItemSelected(item);
    }
}
