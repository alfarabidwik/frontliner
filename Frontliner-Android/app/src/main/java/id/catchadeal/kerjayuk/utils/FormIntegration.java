package id.catchadeal.kerjayuk.utils;

import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.view.FormInputView;

public interface FormIntegration {

    FormInputView getByTag(String tag);
    void triggerAnotherFormIfAny(String tag, FormApplicationDto formApplicationDto);

}
