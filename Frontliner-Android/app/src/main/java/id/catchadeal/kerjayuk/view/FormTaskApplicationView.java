package id.catchadeal.kerjayuk.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import id.catchadeal.kerjayuk.R;
import id.catchadeal.kerjayuk.activity.BaseActivity;
import id.catchadeal.kerjayuk.config.DateAppConfig;
import id.catchadeal.kerjayuk.model.business.FormApplicationDto;
import id.catchadeal.kerjayuk.model.formbuilder.Form;
import id.catchadeal.kerjayuk.utils.FormIntegration;
import jrizani.jrspinner.JRSpinner;
import lombok.Getter;

public class FormTaskApplicationView extends LinearLayout {

    public static final String TAG = FormTaskApplicationView.class.getName();

    MaterialEditText editText ;
    TextView errorTextView ;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateAppConfig.DATETIME_VIEW_FORMAT);
    @Getter FormApplicationDto formApplicationDto = null ;
    BaseActivity baseActivity ;

    View formView ;
    JRSpinner spinner ;

    boolean enable = false ;

    @Getter HashMap<String, String> queryParamMap = new HashMap<>();


    public FormTaskApplicationView(Context context, FormApplicationDto formApplicationDto) {
        super(context);
        this.formApplicationDto = formApplicationDto;
        this.baseActivity = (BaseActivity) context;
        initView();
    }


    public FormTaskApplicationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.baseActivity = (BaseActivity) context;
        initView();
    }

    public void initView(){
        Form form = formApplicationDto.getForm();
        if(form.getType().equalsIgnoreCase(Form.TEXT)|| form.getType().equalsIgnoreCase(Form.EMAIL) || form.getType().equalsIgnoreCase(Form.NUMBER)
                || form.getType().equalsIgnoreCase(Form.PHONE) || form.getType().equalsIgnoreCase(Form.CURRENCY) || form.getType().equalsIgnoreCase(Form.DATE) || form.getType().equalsIgnoreCase(Form.DROPDOWN_API_DATA) ){
            formView = LayoutInflater.from(getContext()).inflate(R.layout.form_builder_text_input, null);
            this.editText = formView.findViewById(R.id.edittext);
            editText.setFloatingLabelText(form.getTitle());
            editText.setHint(form.getPlaceHolder());
            editText.setEnabled(enable);
            editText.setTextSize(14);
            editText.setTextColor(Color.BLACK);
            editText.setText(formApplicationDto.getValue());
            this.addView(formView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        }
        if(form.getType().equalsIgnoreCase(Form.IMAGE)){
            View formView = LayoutInflater.from(getContext()).inflate(R.layout.form_builder_image_input, null);
            ImageView imageView = formView.findViewById(R.id.image_iv);
            formView.findViewById(R.id.plus_iv).setVisibility(GONE);
            TextView labelTextView = formView.findViewById(R.id.label_tv);
            TextView titleTextView = formView.findViewById(R.id.title_tv);
            TextView placehoderTextView = formView.findViewById(R.id.placeholder_tv);
            this.errorTextView = formView.findViewById(R.id.error_tv);
            labelTextView.setText(form.getTitle());
            titleTextView.setText(form.getTitle());
            placehoderTextView.setText("");
            if(form.getPlaceHolder()!=null){
                placehoderTextView.setText("(Hasil) "+form.getPlaceHolder());
            }
            GlideApp.with(this).load(formApplicationDto.getImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.ic_broken_image).into(imageView);
            this.addView(formView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        }
    }



}
