import Page from 'components/Page';
import React from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {
  DD_MM_YYYY,
  DD_MM_YYYY_HH_MM_SS, MIME_JPEG,
  PRODUCT_IMAGE_MAX_FILE_SIZE,
  SELEBGRAM_IMAGE_MAX_FILE_SIZE
} from "../utils/Global";
import {allIsEmpty, deleteParam, imageSelector, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import userProfilePicture from 'assets/img/users/user_pp.png';
import axios from "axios";
import ProductSelebgramModal from '../components/modal/ProductSelebgramModal'
import ImageCropper from "../components/modal/ImageCropper";
import Pagination from '../components/Pagination'

import 'react-datepicker/dist/react-datepicker.css';
import SearchInput from '../components/SearchInput'
import Img from 'react-image'
import {getData} from "../utils/StorageUtil";
import queryString from "query-string";
import WarningLabel from "../components/Widget/WarningLabel";


const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class SelebgramPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      selebgrams : [],
      selebgram:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      selebgramModal:false,
      modal: false,
      image:null,
      sortir:"created",
      ascending:true,
      search:"",
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  componentWillReceiveProps(props, nextContext) {
    if(props!=this.props){
      this.setState({
        page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
      }, () => {
        this.fetchAll()
      })
    }
  }


  confirmSave = () =>{
    var selebgram = this.state.selebgram
    var image = this.state.image
    if(allIsEmpty(image, selebgram.name, selebgram.description, selebgram.link, selebgram.sortir)
        || selebgram.sortir === 0){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  saveUpload = () => {
    var formData = new FormData();
    formData.append("selebgramDtoGson", JSON.stringify(this.state.selebgram))
    if(this.state.imageBlob){
      formData.append("multipartFile", this.state.imageBlob);
    }
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.SELEBGRAM_SAVE_UPLOAD, formData, null, res=>{
        this.setState({
          selebgram:{},
          image:null,
        }, () => {
          this.showDialog("Success", res.message)
          this.fetchAll()
        })
      },  true, true)
    })
  }


  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.SELEBGRAM_DELETE+"/"+this.state.selebgram.id, null, null, res => {
        if(res.code===200){
          this.setState({
            selebgram:{},
            image:null,
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  openSelebgramModal=()=>{
    this.setState({
      selebgramModal : true
    })
  }

  closeSelebgramModal=()=>{
    this.setState({
      selebgramModal : false
    }, () => {
      this.fetchAll()
    })
  }

  fetchAll = () => {
    this.get(Global.API.SELEBGRAMS,{
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
        search:this.state.search,
        page: this.state.page-1
      }
    }, null, res =>{
      if(res.code === 200){
        this.setState({
          selebgrams : res.data,
          totalPage: res.totalPage,
          totalElement: res.totalElement,
          pageElement: res.pageElement,
        })
      }
    }, true, true);
  }

  refreshSelebgram = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    let selebgram = this.state.selebgram

    let configuration = getData(Global.CONFIGURATION)
    let aspect = 4/4
    if(configuration.selebgramImageWidth && configuration.selebgramImageHeight){
      aspect = configuration.selebgramImageWidth/configuration.selebgramImageHeight
    }


    return (
        <Page
            title="Selebgram"
            breadcrumbs={[{ name: 'selebgram', active: true }]}
            className="TablePage">
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this selebgram item to your list ?"
              okCallback={this.saveUpload}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this selebgram item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <ProductSelebgramModal modal={this.state.selebgramModal} close={this.closeSelebgramModal} selebgram={selebgram}/>
          {super.render()}
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Selebgram
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col md={3}>
                      <Card>
                        <CardHeader>
                          Selebgram Image
                        </CardHeader>
                        <CardBody>
                          <Row>
                            <Col>
                              <CardImg top src={
                                this.state.image!=null?this.state.image:userProfilePicture}
                                       onClick={e=>{
                                         if(this.state.image!=null){
                                           this.setState({
                                             imageLink:this.state.image,
                                             openPreview:!this.state.openPreview
                                           })
                                         }
                                       }}
                                       onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <WarningLabel message={"*Use Jpeg Format / Max "+SELEBGRAM_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                            </Col>
                          </Row>
                        </CardBody>
                        <CardFooter>
                          <Row>
                            <Col>
                              <ButtonGroup className="float-left">
                                <Button color="primary" onClick={e=>(
                                    imageSelector(file =>{
                                      this.setState({
                                        image:file,
                                        cropperModal:true
                                      })
                                    }, MIME_JPEG, SELEBGRAM_IMAGE_MAX_FILE_SIZE).click()
                                )}>Upload</Button>
                              </ButtonGroup>
                            </Col>
                          </Row>
                        </CardFooter>
                      </Card>
                    </Col>
                    <Col>
                      <Card>
                        <CardBody>
                          <Row>
                            <Col>
                              <FormGroup>
                                <Label for="name">Name</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={!isEmpty(this.state.selebgram.name)?this.state.selebgram.name:""}
                                    onChange={(e) =>{
                                      let selebgram = this.state.selebgram
                                      if(selebgram!=null){
                                        selebgram.name = e.target.value
                                        this.setState({
                                          selebgram:selebgram
                                        })
                                      }
                                    }}
                                    placeholder="Enter name"
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <FormGroup>
                                <Label for="description">Description</Label>
                                <Input
                                    type="text"
                                    name="description"
                                    value={(this.state.selebgram!=null && this.state.selebgram.description!=undefined)?this.state.selebgram.description:""}
                                    onChange={(e) =>{
                                      let selebgram = this.state.selebgram
                                      if(selebgram!=null){
                                        selebgram.description = e.target.value
                                        this.setState({
                                          selebgram:selebgram
                                        })
                                      }
                                    }}
                                    placeholder="Enter description"
                                />
                              </FormGroup>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={6}>
                              <FormGroup>
                                <Label for="sortir">Sortir</Label>
                                <Input
                                    type="number"
                                    name="sortir"
                                    value={(this.state.selebgram!=null && this.state.selebgram.sortir!=undefined)?this.state.selebgram.sortir:""}
                                    onChange={(e) =>{
                                      let selebgram = this.state.selebgram
                                      if(selebgram!=null){
                                        selebgram.sortir = e.target.value
                                        this.setState({
                                          selebgram:selebgram
                                        })
                                      }
                                    }}
                                    placeholder="Enter sortir"
                                />
                              </FormGroup>
                            </Col>
                            <Col md={6}>
                              <ActiveOption
                                  callback={(active)=>{
                                    let selebgram = this.state.selebgram
                                    if(selebgram!=null){
                                      selebgram.active = active
                                      this.setState({
                                        selebgram:selebgram
                                      })
                                    }

                                  }}
                                  default={this.state.selebgram?this.state.selebgram.active:false}
                              />
                            </Col>
                          </Row>
                          <Row>
                            <Col md={10}>
                              <FormGroup>
                                <Label for="pagelink">Pagelink</Label>
                                <Input
                                    type="link"
                                    name="pagelink"
                                    value={!isEmpty(this.state.selebgram.link)?this.state.selebgram.link:""}
                                    onChange={(e) =>{
                                      let selebgram = this.state.selebgram
                                      if(selebgram!=null){
                                        selebgram.link = e.target.value
                                        this.setState({
                                          selebgram:selebgram
                                        })
                                      }
                                    }}
                                    placeholder="Enter pagelink"
                                />
                              </FormGroup>
                            </Col>
                            <Col md={2}>
                              <FormGroup>
                                <Label for="pagelink">Go Pagelink</Label>
                                <br/>
                                <Badge href={this.state.selebgram.link} color="primary" className="form-text mr-1" target="_blank">
                                  Go site
                                </Badge>
                              </FormGroup>
                            </Col>
                          </Row>
                        </CardBody>
                        <CardFooter>
                          <Row>
                            <Col>
                              <ButtonGroup className="float-left">
                                <Button color="danger" onClick={e => (
                                    this.setState({
                                      selebgram:{},
                                      image:null
                                    })
                                )}> Cancel </Button>
                              </ButtonGroup>
                            </Col>
                            <Col>
                              <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>(
                                    this.confirmSave()
                                )}> {
                                  (this.state.selebgram!=null && this.state.selebgram.id!=null)?"Update":"Add"
                                } </Button>
                              </ButtonGroup>
                            </Col>
                          </Row>
                        </CardFooter>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  Selebgram List
                </CardHeader>
                <CardBody>
                  <Col>
                    <Row>
                      <Col md={2}>
                        Sort By :
                        <UncontrolledButtonDropdown key={1}>
                          <DropdownToggle
                              caret
                              color="white"
                              className="text-capitalize m-1">
                            {
                              this.state.sortir
                            }
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(this.state.ascending, "name"))}>Name</DropdownItem>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(this.state.ascending, "created"))}>Created</DropdownItem>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(this.state.ascending, "updated"))}>Updated</DropdownItem>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(this.state.ascending, "sortir"))}>Sortir</DropdownItem>
                          </DropdownMenu>
                        </UncontrolledButtonDropdown>
                      </Col>
                      <Col md={2}>
                        Sortir :
                        <UncontrolledButtonDropdown key={2}>
                          <DropdownToggle
                              caret
                              color="white"
                              className="text-capitalize m-1">
                            {
                              this.state.ascending?"Ascending":"Descending"
                            }
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(true, this.state.sortir))}>Ascending</DropdownItem>
                            <DropdownItem onClick={e=>(this.refreshSelebgram(false, this.state.sortir))}>Descending</DropdownItem>
                          </DropdownMenu>
                        </UncontrolledButtonDropdown>
                      </Col>
                      <Col md={8}>
                    <span className="float-right">
                          <SearchInput
                              placeholder={"Search selebgram name..."}
                              value={this.state.search}
                              onChange={e=>{
                                this.setState({
                                  search:e.target.value
                                }, () => {
                                  if(this.state.search===''){
                                    deleteParam(this.props, 'page')
                                  }
                                })
                              }}
                              onEnter={e=>{
                                if(this.state.search!==''){
                                  deleteParam(this.props, 'page')
                                }
                              }}
                          />
                    </span>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Table hover>
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Sortir</th>
                            <th>Pagelink</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Product</th>
                            <th>Updated</th>
                            <th>Edit</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                            this.state.selebgrams.map((item, index) =>(
                                <tr key={index}>
                                  <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                  <td>{item.name}</td>
                                  <td>{item.sortir}</td>
                                  <td><a href={item.link} target="_blank">Go site</a></td>
                                  <td>{translate(item.active)}</td>
                                  <td width="10%">
                                    <Img
                                        className="img-thumbnail"
                                        src={item.imageLink}
                                        onClick={e=>{
                                          this.setState({
                                            imageLink:item.imageLink,
                                            openPreview:true
                                          })
                                        }}
                                    />
                                  </td>
                                  <td><Button color="primary" onClick={event=>{
                                    event.preventDefault()
                                    this.setState({
                                      selebgram:item,
                                      image:item.imageLink
                                    }, () => {
                                      this.openSelebgramModal()
                                    })
                                  }}>{item.productSelebgrams.length}</Button></td>
                                  <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                  <td>
                                    <Button color="danger" onClick={e => {
                                      this.setState({
                                        selebgram:item,
                                        image:item.imageLink
                                      }, () => {
                                        this.confirmDelete()
                                      })
                                    }}>
                                      <MdDelete/>
                                    </Button>
                                    &nbsp;
                                    <Button color="secondary" onClick={e=>(
                                        this.setState({
                                          selebgram:item,
                                          image:item.imageLink
                                        })
                                    )
                                    }>
                                      <MdEdit/>
                                    </Button>
                                  </td>
                                </tr>
                            ))
                          }
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                  </Col>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
          <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
            axios({
              url: file,
              method: 'GET',
              responseType: 'blob', // important
            }).then((response) => {
              var fileBlob = response.data ;
              this.setState({
                image:file,
                cropperModal:false,
                imageBlob:fileBlob,
              })
            })
          }} cancelCallback={()=>{
            this.setState({
              cropperModal:false,
              image:null,
              imageBlob:null,
            })
          }}/>

        </Page>
    );
  }
}
