import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form, FormFeedback,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import userProfilePicture from 'assets/img/users/user_pp.png';
import Page from "../components/Page";
import ItemOption from "../components/Widget/ItemOption";
import kitchen from "../utils/AxiosInstance";
import Global, {ADMIN_STATUSES} from "../utils/Global";
import {allIsEmpty, changeParam, imageSelector, isValidEmail, navigatePage, parseDate} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import FormText from "reactstrap/es/FormText";
import ImageCropper from "../components/modal/ImageCropper";
import {IoMdKey} from "react-icons/io";

export default class AdminDetailPage extends BasePage{

    constructor(props) {
        super(props);
        this.state = {
            admin:{},
            roles:[],
            adminStatuses:ADMIN_STATUSES,
            image :null,
            imageBlob:null,
            adminIdQuery: queryString.parse(this.props.query).id,
            role:{},
            adminStatus:{},
            modalAddConfirm:false
        }
    }

    // componentWillReceiveProps(props, nextContext) {
    //     if(props!=this.props){
    //         this.setState({
    //             adminIdQuery: queryString.parse(this.props.query).id,
    //         }, () => {
    //             this.fetchAdminDetail(this.state.adminIdQuery)
    //         })
    //     }
    // }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAllRole()
        this.fetchAdminDetail(this.state.adminIdQuery)
    }

    confirmSave = () =>{
        var admin = this.state.admin
        var image = this.state.image
        if(allIsEmpty(image, admin.firstname, admin.lastname, admin.email, admin.adminStatus, admin.role)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            this.setState({
                modalAddConfirm:true
            })
        }
    }

    saveUpload = () => {
        var file = this.state.imageBlob ;
        var formData = new FormData();
        formData.append("adminDtoGson", JSON.stringify(this.state.admin))
        formData.append("multipartFile", file);
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.ADMIN_SAVE_UPLOAD, formData, null, res=>{
                this.setState({
                    admin:res.data
                }, () => {
                    this.showDialog("Success", res.message)
                    // changeParam(this.props, 'id', this.state.admin.id)
                    this.fetchAdminDetail(this.state.admin.id)
                })
            },  true, true)
        })
    }



    fetchAdminDetail = (id) => {
        if(id!=null){
            this.get(Global.API.ADMIN, {
                params :{
                    id : id
                }
            }, null, res=>{
                if(res.code===200){
                    let adminStatus = null
                    ADMIN_STATUSES.forEach(value => {
                        if(value.name==res.data.adminStatus){
                            adminStatus = value
                        }
                    })
                    this.setState({
                        admin:res.data,
                        role:res.data.role,
                        adminStatus:adminStatus,
                        image:res.data.image!=undefined?res.data.imageLink:null,

                    })

                }else{
                    this.props.history.goBack();
                }

            }, true, true)
        }
    }

    fetchAllRole = () =>{
        kitchen.get(Global.API.ROLES).then(res =>{
            if(res.data.code === 200){
                this.setState({
                    roles : res.data.data
                })
            }
        });
    }


    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
        })
    }

    verifyEmail = (email) =>{
        this.get(Global.API.ADMIN_VALIDATE_EMAIL, {
            params:{
                email:email
            }
        }, null, response=>{
            if(response.code!=200){
                this.setState({
                    emailWarning:response.message
                })
            }
        }, false, false)
    }

    render() {

        let adminStatus = this.state.adminStatus?this.state.adminStatus:{}
        var ids = [];
        var labels = [];
        this.state.roles.map((item, index)=>{
            ids.push(item.id);
            labels.push(item.name);
        })
        return (
            <Page
                title="Admin Form"
                breadcrumbs={[{ name: 'admin form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this admin item to your list ?"
                    okCallback={this.saveUpload}
                    cancelCallback={this.closeDialog}/>
                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Admin</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={3}>
                                        <Card>
                                            <CardImg top src={
                                                this.state.image!=null?this.state.image:userProfilePicture} onClick={e=>{
                                                imageSelector(file=>{
                                                    this.setState({
                                                        image:file,
                                                        cropperModal:true
                                                    })
                                                }).click()
                                            }}/>
                                            <CardBody>
                                                <CardTitle>Admin Image</CardTitle>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => {
                                                                e.preventDefault()
                                                                this.setState({
                                                                    admin:null,
                                                                    image:null
                                                                })
                                                            }}>Cancel</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                imageSelector(file=>{
                                                                    this.setState({
                                                                        image:file,
                                                                        cropperModal:true
                                                                    })
                                                                }).click()
                                                            }}>Upload</Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={9}>
                                        <Card>
                                            <CardBody>
                                                <FormGroup>
                                                    <Label for="firstname">Firstname</Label>
                                                    <Input
                                                        type="text"
                                                        name="firstname"
                                                        value={(this.state.admin!=null && this.state.admin.firstname)? this.state.admin.firstname:""}
                                                        onChange={(e) =>{
                                                            let admin = this.state.admin
                                                            if(admin!=null){
                                                                admin.firstname = e.target.value
                                                                this.setState({
                                                                    admin:admin
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter admin firstname"
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label for="lastname">Lastname</Label>
                                                    <Input
                                                        type="text"
                                                        name="lastname"
                                                        value={(this.state.admin!=null && this.state.admin.lastname)?this.state.admin.lastname:""}
                                                        onChange={(e) =>{
                                                            let admin = this.state.admin
                                                            if(admin!=null){
                                                                admin.lastname = e.target.value
                                                                this.setState({
                                                                    admin:admin
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter admin lastname"
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label for="email">Email</Label>
                                                    <Input
                                                        type="text"
                                                        name="email"
                                                        disabled={(this.state.admin!=null && this.state.admin.id)}
                                                        value={(this.state.admin!=null && this.state.admin.email)?this.state.admin.email:""}
                                                        onChange={(e) =>{
                                                            let email = e.target.value
                                                            let admin = this.state.admin
                                                            if(admin!=null){
                                                                admin.email = email
                                                                this.setState({
                                                                    admin:admin
                                                                })
                                                            }
                                                            if(!isValidEmail(email)){
                                                                this.setState({
                                                                    emailWarning:"Invalid email address"
                                                                })
                                                            }else{
                                                                this.setState({
                                                                    emailWarning:undefined
                                                                }, () => {
                                                                    this.verifyEmail(this.state.admin.email)
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter admin email"
                                                    />
                                                    <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.emailWarning}</span></FormText>
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Role"}
                                                        objects={this.state.roles}
                                                        default={this.state.role!=null?this.state.role.id:null}
                                                        callback={(role)=>{
                                                            let admin = this.state.admin
                                                            if(admin!=null && role!=null){
                                                                admin.role = role
                                                                this.setState({
                                                                    admin:admin,
                                                                    role:role
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Status"}
                                                        objects={this.state.adminStatuses}
                                                        default={adminStatus.id}
                                                        callback={(adminStatus)=>{
                                                            let admin = this.state.admin
                                                            if(admin && adminStatus){
                                                                admin.adminStatus = adminStatus.name
                                                                this.setState({
                                                                    admin:admin,
                                                                    adminStatus:adminStatus
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    admin:null
                                                                }, () => {
                                                                   this.props.history.goBack();
                                                                })
                                                                )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="danger" onClick={event=>{
                                                                this.openConfirmDialog("Confirmation", "Do you want to resend a password to its email", ()=>{
                                                                    this.get(Global.API.ADMIN_RESEND_PASSWORD, {
                                                                        params:{
                                                                            adminId:this.state.admin.id
                                                                        }
                                                                    }, null, response=>{
                                                                        if(response.code===200){
                                                                            this.successToast(response.message)
                                                                        }
                                                                    }, true, true)
                                                                })
                                                            }}><IoMdKey/>&nbsp;&nbsp;Resend Password</Button>
                                                            &nbsp;&nbsp;
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.admin!=null && this.state.admin.id!=null)?"Update":"Create"
                                                            } </Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>

                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <ImageCropper src={this.state.image} show={this.state.cropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        this.setState({
                            image:file,
                            cropperModal:false,
                            imageBlob:fileBlob,
                        }, () => {
                            // this.uploadPhotoProfile(file, login)
                        })
                    })
                }}/>
            </Page>
        );
    }


}