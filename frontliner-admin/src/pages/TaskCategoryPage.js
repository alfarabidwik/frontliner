import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  CardImg,
  CardTitle,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Input,
  Label,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import shoppingBag from 'assets/img/products/shopping-bag.png';
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import Global, {CATEGORY_IMAGE_MAX_FILE_SIZE, DD_MM_YYYY_HH_MM_SS, MIME_PNG} from "../utils/Global";
import {allIsEmpty, cloneObject, imageSelector, isEmpty, parseDate} from "../utils/Utilities";
import {MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import ActiveLabel from "../components/Widget/ActiveLabel";
import WarningLabel from "../components/Widget/WarningLabel";
import axios from "axios";
import ImageCropper from "../components/modal/ImageCropper";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class TaskCategoryPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      taskCategories : [],
      taskCategory:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      image : null,
      modal: false,
      ascending:true,
      sortir:'created',
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var taskCategory = this.state.taskCategory
    var image = this.state.imageBlob
    if(allIsEmpty(image, taskCategory.name)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }


  save = () => {
    let taskCategory = this.state.taskCategory
    let imageBlob = this.state.imageBlob
    var formData = new FormData();

    if(!taskCategory.id && !taskCategory.active){
      taskCategory.active = false ;
    }


    formData.append("taskCategoryJson", JSON.stringify(taskCategory))
    formData.append("multipartFile", imageBlob);
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.TASK_CATEGORY_SAVE, formData, null, res=>{
        if(res.code===200){
          this.setState({
            taskCategory:res.data
          }, () => {
            this.showDialog("Success", res.message)
            // this.fetch(this.state.taskCategory.id)
            // changeParam(this.props, 'id', this.state.product.id)
          })
        }
      },  true, true)
    })
  }


  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.TASK_CATEGORY_DELETE+"/"+this.state.taskCategory.id, null, null, res => {
        if(res.code===200){
          this.setState({
            taskCategory:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.TASK_CATEGORIES, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          taskCategories : response.data
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    let aspect = 4/4

    return (
        <Page
            title="Task Category Detail"
            breadcrumbs={[{ name: 'task category detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to udpate this task category item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this task taskCategory item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Task Category Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={3}>
                      <Card body>
                        <Row>
                          <Col>
                            <Card>
                              <CardImg top src={
                                this.state.image!=null?this.state.image:shoppingBag} onClick={e=>{
                                imageSelector(file=>{
                                  this.setState({
                                    image:file,
                                    cropperModal:true
                                  })
                                }, MIME_PNG, CATEGORY_IMAGE_MAX_FILE_SIZE).click()
                              }}
                              onError={(elm)=>this.defaultImage(elm, shoppingBag)}/>
                              <CardBody>
                                <CardTitle>Category Image</CardTitle>
                                <Row>
                                  <Col>
                                    <WarningLabel message={"*Use Jpeg Format / Max "+CATEGORY_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                                  </Col>
                                </Row>
                                <br/>
                                <Row>
                                  <Col>
                                    <ButtonGroup className="float-left">
                                      <Button color="danger" onClick={e => (
                                          this.setState({
                                            product:null,
                                            image:null
                                          })
                                      )}>Cancel</Button>
                                    </ButtonGroup>
                                  </Col>
                                  <Col>
                                    <ButtonGroup className="float-right">
                                      <Button color="primary" onClick={e=>{
                                        imageSelector(file=>{
                                          this.setState({
                                            image:file,
                                            cropperModal:true
                                          })
                                        }, MIME_PNG, CATEGORY_IMAGE_MAX_FILE_SIZE).click()
                                      }}>Upload</Button>

                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.taskCategory.name)?this.state.taskCategory.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.taskCategory
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        taskCategory:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter task category name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Task Count</Label>
                              <Input
                                  type="text"
                                  name="productItem"
                                  value={(this.state.taskCategory)?this.state.taskCategory.totalTask:""}
                                  readOnly
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.taskCategory.active}
                                callback={(active)=>{
                                  let ctg = this.state.taskCategory
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      taskCategory:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    taskCategory:null
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.taskCategory!=null && this.state.taskCategory.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={9}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Task Count</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.taskCategories.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td>{item.totalTask}</td>
                                      <td><ActiveLabel trueLabel={"Active"} falseLabel={"Inactive"} active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="primary" onClick={e=> {
                                          let taskCategory = cloneObject(item)
                                          this.setState({
                                            taskCategory: taskCategory,
                                            image: taskCategory.defaultIconUrl
                                          })
                                        }}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
            axios({
              url: file,
              method: 'GET',
              responseType: 'blob', // important
            }).then((response) => {
              var fileBlob = response.data ;
              this.setState({
                image:file,
                cropperModal:false,
                imageBlob:fileBlob,
              })
            })
          }} cancelCallback={()=>{
            this.setState({
              cropperModal:false,
              image:this.state.taskCategory.imageLink,
              imageBlob:null,
            })
          }}/>

        </Page>
    );
  }
}
