import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'

import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Img from 'react-image'

const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class ProductPage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
      products : [],
      product:{},
      modalDeleteConfirm:false,
      ascending:false,
      sortir:'p.created',
      search:"",
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
            })
        }
    }



    fetchAll = (ascending, sortir, search, page, progressing) => {
        this.get(Global.API.PRODUCTS, {
            params:{
                ascending:ascending,
                sortir:sortir,
                search:search,
                page: page-1
            }
        }, null, res=>{
            if(res.code === 200){
                this.setState({
                    products : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        },progressing, true);
    }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.PRODUCT_DELETE+"/"+this.state.product.id, null, null, res=>{
          if(res.code===200){
              this.successToast(res.message)
              this.setState({
                  category:null
              }, () => {
                  this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
              })
          }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshProduct = (ascending, sortir, progressing) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, progressing)
      })
  )

    render() {
        var i = 0 ;

    return (
        <Page
            title="Products"
            breadcrumbs={[{ name: 'product', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this product item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Product</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={2}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.name", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.brand.name", true))}>Brand Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.heading.name", true))}>Heading Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.category.name", true))}>Category Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={2}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            <SearchInput
                                placeholder={"Type and enter to search..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            deleteParam(this.props, 'page')
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        deleteParam(this.props, 'page')
                                    }
                                }}
                            />
                        </Col>
                        <Col md={5}>
                            <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>{
                                    this.props.history.push('/productDetail')
                                }}>Add Product</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Brand</th>
                              <th>Heading</th>
                              <th>Category</th>
                              <th>Hashtag</th>
                              <th>Image</th>
                              <th>Status</th>
                              <th>Last Update</th>
                              <th>Edit</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.products.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.name}</td>
                                      <td>{item.brand?item.brand.name:"-"}</td>
                                      <td>{item.heading?item.heading.name:"-"}</td>
                                      <td>{item.category?item.category.name:"-"}</td>
                                      <td>{item.relatedHashtag}</td>
                                      <td width="5%">
                                          <Img
                                              className="img-thumbnail"
                                              src={item.imageLink}
                                              onClick={e=>{
                                                  this.setState({
                                                      imageLink:item.imageLink,
                                                      openPreview:true
                                                  })
                                              }}
                                          />
                                          </td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          <Button size="sm" color="danger" onClick={e => {
                                              this.setState({
                                                  product:item
                                              }, () => {
                                                  this.confirmDelete()
                                              })
                                          }}>
                                          <MdDelete/>
                                          </Button>
                                          &nbsp;
                                          <Button size="sm" color="primary" onClick={ e => (
                                              this.props.history.push('/productDetail?id='+item.id)
                                          )}>
                                          <MdEdit/>
                                          </Button>
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }

};
