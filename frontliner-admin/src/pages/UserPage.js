import Page from 'components/Page';
import React from 'react';
import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS, GENDERS, USER_STATUSES} from "../utils/Global";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'
import {allIsEmpty, deleteParam, isEmpty, parseDate, sortirMap, currencyFormat} from "../utils/Utilities";
import ItemOption from "../components/Widget/ItemOption";
import {IoMdOpen} from "react-icons/io/index";
import Pagination from '../components/Pagination'
import queryString from 'query-string';


import {FiArrowDownCircle, FiArrowUpCircle} from "react-icons/fi/index";
import DateInput from "../components/Widget/DateInput";
import {UserDto} from "../model/model";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class UserPage extends BasePage{

    user = new UserDto();
    users = new Array(UserDto);

    constructor(props) {
    super(props);
    this.state ={
        users : this.users,
        user:this.user,
        modalDeleteConfirm:false,
        ascending:false,
        sortir:'u.created',
        search:"",
        page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
        totalPage:0,
        totalElement:0,
        pageElement:0,
        createdStartDate:null,
        createdEndDate:null,
        gender:{},
        userStatus:{},
        provinces:[],
        cities:[],
        districts:[],
        villages:[],
        province:{},
        city:{},
        district:{},
        village:{},
        userFilterExpand:false,
        userTransactionExpand:false,
    }

    this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))



  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
            })
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
    }

  fetchAll = (ascending, sortir, search, progressing) => {
    this.get(Global.API.USERS, {
      params:{
          page:this.state.page-1,
          ascending:ascending,
          sortir:sortir,
          search:search,
          userCreatedStart:this.state.createdStartDate,
          userCreatedEnd:this.state.createdEndDate,
          genderId:this.state.gender.id,
          userStatusId:this.state.userStatus.id,
          provinceId:this.state.province.id,
          cityId:this.state.city.id,
          districtId:this.state.district.id,
          villageId:this.state.village.id,
      }
    }, null, res =>{
      if(res.code === 200){
        this.setState({
          users : res.data,
          totalPage: res.totalPage,
          totalElement: res.totalElement,
          pageElement: res.pageElement,
        })
      }
    }, progressing, true);
  }



   confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.USER_DELETE+"/"+this.state.user.id, null, null, res => {
        if(res.code===200){
          this.setState({
            category:null
          }, () => {
              this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
          })
        }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

    refreshUser = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )


    render() {
        var i = 0 ;
        let user = this.state.user?this.state.user:{}
        let gender = user.gender?user.gender:{}

    return (

        <Page
            title="Users"
            title="Users"
            breadcrumbs={[{ name: 'user', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this user item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
            <Row key={1}>
                <Col>
                    <Card>
                        <CardHeader onClick={event=>{
                            this.setState({
                                userFilterExpand:!this.state.userFilterExpand
                            })
                        }}>
                            {
                                (this.state.userFilterExpand)?(<FiArrowUpCircle/>):((<FiArrowDownCircle/>))
                            }
                            &nbsp;Profile Filter
                        </CardHeader>
                        <Collapse isOpen={this.state.userFilterExpand}>
                            <CardHeader>
                                <Row>
                                    <Col md={3}>
                                        <Label for="createdStartDate">Reg/Created Start</Label>
                                        <DateInput
                                            id="createdStartDate"
                                            value={this.state.createdStartDate}
                                            maxdate={this.state.createdEndDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    createdStartDate : value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter start date"
                                        />
                                    </Col>
                                    <Col md={3}>
                                        <Label for="createdEndDate">Reg/Created End</Label>
                                        <DateInput
                                            id="createdEndDate"
                                            mindate={this.state.createdStartDate}
                                            value={this.state.createdEndDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    createdEndDate :  value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter end date"
                                        />
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Gender"}
                                                objects={GENDERS}
                                                default={!isEmpty(this.state.gender)?this.state.gender.id:null}
                                                callback={(gender)=>{
                                                    user.gender = gender.name
                                                    this.setState({
                                                        gender:gender,
                                                        user:user
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Status"}
                                                objects={USER_STATUSES}
                                                default={!isEmpty(this.state.userStatus)?this.state.userStatus.id:null}
                                                callback={(userStatus)=>{
                                                    this.setState({
                                                        userStatus:userStatus
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Province"}
                                                objects={this.state.provinces}
                                                default={!allIsEmpty(this.state.province)?this.state.province.id:null}
                                                callback={(province)=>{
                                                    this.setState({
                                                        province:!isEmpty(province.id)?province:{},
                                                        cities:[],
                                                        city:!isEmpty(province.id)?this.state.city:{},
                                                        districts:[],
                                                        district:!isEmpty(province.id)?this.state.district:{},
                                                        villages:[],
                                                        village:!isEmpty(province.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(province.id)){
                                                            this.fetchCities(province.id, cities=>{
                                                                this.setState({
                                                                    cities:cities
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"City"}
                                                objects={this.state.cities}
                                                default={!allIsEmpty(this.state.city)?this.state.city.id:null}
                                                callback={(city)=>{
                                                    this.setState({
                                                        city:!isEmpty(city.id)?city:{},
                                                        districts:[],
                                                        district:!isEmpty(city.id)?this.state.district:{},
                                                        villages:[],
                                                        village:!isEmpty(city.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(city.id)){
                                                            this.fetchDistricts(city.id, districts =>{
                                                                this.setState({
                                                                    districts:districts
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"District"}
                                                objects={this.state.districts}
                                                default={!allIsEmpty(this.state.district)?this.state.district.id:null}
                                                callback={(district)=>{
                                                    this.setState({
                                                        district:!isEmpty(district.id)?district:{},
                                                        villages:[],
                                                        village:!isEmpty(district.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(district.id)){
                                                            this.fetchVillages(district.id, villages=>{
                                                                this.setState({
                                                                    villages:villages
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Village"}
                                                objects={this.state.villages}
                                                default={!allIsEmpty(this.state.village)?this.state.village.id:null}
                                                callback={(village)=>{
                                                    this.setState({
                                                        village:!isEmpty(village)?village:{},
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </CardHeader>
                        </Collapse>
                    </Card>
                </Col>
            </Row>
            <Row key={3}>
            <Col>
              <Card>
                  <CardHeader>Sortir</CardHeader>
                  <CardBody>
                    <Row>
                          <Col md={2}>
                              <UncontrolledButtonDropdown key={1}>
                                  <DropdownToggle
                                      caret
                                      color="white"
                                      className="text-capitalize m-1">
                                      {
                                          sortirMap(this.state.sortir)
                                      }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.created", true))}>Created</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.firstname", true))}>Firstname</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.email", true))}>Email</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.mobile_phone", true))}>Phone Number</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.updated", true))}>Last Update</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(this.state.ascending, "u.total_income", true))}>Total Income</DropdownItem>
                                  </DropdownMenu>
                              </UncontrolledButtonDropdown>
                          </Col>
                          <Col md={2}>
                              <UncontrolledButtonDropdown key={2}>
                                  <DropdownToggle
                                      caret
                                      color="white"
                                      className="text-capitalize m-1">
                                      {
                                          this.state.ascending?"Ascending":"Descending"
                                      }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                      <DropdownItem onClick={e=>(this.refreshUser(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshUser(false, this.state.sortir, true))}>Descending</DropdownItem>
                                  </DropdownMenu>
                              </UncontrolledButtonDropdown>
                          </Col>
                          <Col md={3}>
                              <SearchInput
                                  placeholder={"Search name, email, phone, agent..."}
                                  value={this.state.search}
                                  onChange={e=>{
                                      this.setState({
                                          search:e.target.value
                                      }, () => {
                                          if(this.state.search===''){
                                              deleteParam(this.props, "page")
                                          }
                                      })
                                  }}
                                  onEnter={e=>{
                                      if(this.state.search!==''){
                                          deleteParam(this.props, "page")
                                      }
                                  }}
                              />
                          </Col>
                          <Col md={5}>
                              <ButtonGroup className="float-right">
                                  <Button color="primary" onClick={e=>{
                                      this.props.history.push('/userDetail')
                                  }}>Add User</Button>
                              </ButtonGroup>
                          </Col>
                      </Row>
                    <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Fullname</th>
                              <th>Email</th>
                              <th>Mobile Phone</th>
                              <th>Agent Code</th>
                              <th>Organization</th>
                              <th>Income</th>
                              <th>Verification</th>
                              <th>Status</th>
                              <th>
                                  Task OG/DO/REJ
                              </th>
                              <th>Updated</th>
                              <th>View</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.users.map((item, index)=>{
                                  return (
                                      <tr key={item.id}>
                                          <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                          <td>{item.fullname}</td>
                                          <td>{item.email}</td>
                                          <td>{item.mobilePhone}</td>
                                          <td>{item.agentCode}</td>
                                          <td>{item.organization}</td>
                                          <td>{currencyFormat(item.totalIncome, CURRENCY_SYMBOL)}</td>
                                          <td>{item.verificationStatus}</td>
                                          <td>{item.userStatus}</td>
                                          <td>
                                              {isEmpty(item.onGoingTask)?0:item.onGoingTask}
                                              /
                                              {isEmpty(item.totalCompletedTask)?0:item.totalCompletedTask}
                                              /
                                              {isEmpty(item.totalRejectedTask)?0:item.totalRejectedTask}
                                          </td>
                                          <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                          <td>
                                              <span title="See Detail" style={{cursor:'pointer'}}>
                                          <IoMdOpen color="blue" onClick={ e => (
                                              this.props.history.push('/userDetail?id='+item.id)
                                          )}/>
                                          </span>
                                          </td>
                                      </tr>
                                  )
                              })
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
            </Col>
          </Row>
        </Page>
    );
  }
};
