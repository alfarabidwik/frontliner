import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Input,
    Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import userProfilePicture from 'assets/img/users/user_pp.png';
import Page from "../components/Page";
import ItemOption from "../components/Widget/ItemOption";
import Global, {
    CURRENCY_SYMBOL,
    DD_MM_YYYY,
    DD_MM_YYYY_HH_MM_SS,
    GENDERS,
    USER_STATUSES,
    VERIFICATION_STATUSES
} from "../utils/Global";
import {
    allIsEmpty,
    currencyFormat, deleteParam,
    imageSelector,
    isEmpty,
    isValidEmail,
    parseDate,
    sortirMap
} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import FormText from "reactstrap/es/FormText";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye} from "react-icons/io/index";
import SearchInput from '../components/SearchInput'
import Pagination from '../components/Pagination'
import CardFooter from "reactstrap/es/CardFooter";
import {UserDto} from "../model/model.ts";
import DateInput from "../components/Widget/DateInput";
import ImageCropper from "../components/modal/ImageCropper";
import {MdDelete} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";
import UpdateVerificationStatusModal from '../components/modal/UpdateVerificationStatusModal'


export default class UserDetailPage extends BasePage{

    user = new UserDto()

    constructor(props) {
        super(props);
        this.state = {
            phoneNumberWarning:"",
            user:this.user,
            photoBlob:null,
            idCardBlob:null,
            selfieIdCardBlob:null,
            image:null,
            userIdQuery: queryString.parse(this.props.query).id,
            gender:{},
            userStatus:{},
            modalAddConfirm:false,
            ascending:false,
            sortir:'created',
            provinces:[],
            cities:[],
            districts:[],
            villages:[],
            province:{},
            city:{},
            district:{},
            village:{},
            transactionStatuses:[],
            transactions:[],
            transactionStatus:null,
            refCode:"",
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            updateVerificationStatusModal:false,
            deviceSortir:"ud.created",
            deviceSearch:"",
            deviceAscending:false,


        }
        this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchUserDetail(this.state.userIdQuery)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            })
        }
    }


    confirmSave = () =>{
        var user = this.state.user
        if(allIsEmpty(user.firstname, user.email, user.homePhoneNumber, user.birthdate, user.address, user.village, user.gender, user.userStatus)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            this.setState({
                modalAddConfirm:true
            })
        }
    }

    async saveUpload(){
        let photoBlob = this.state.photoBlob
        let idCardBlob = this.state.idCardBlob
        let selfieIdCardBlob = this.state.selfieIdCardBlob
        if(photoBlob){
            photoBlob = await new Promise(resolve => {
                axios({
                    url: photoBlob,
                    method: 'GET',
                    responseType: 'blob', // important
                }).then((response) => {
                    this.closeProgress()
                    var file = response.data ;
                    resolve(file)
                })
            })
        }

        if(idCardBlob){
            idCardBlob = await new Promise(resolve => {
                axios({
                    url: idCardBlob,
                    method: 'GET',
                    responseType: 'blob', // important
                }).then((response) => {
                    this.closeProgress()
                    var file = response.data ;
                    resolve(file)
                })
            })
        }

        if(selfieIdCardBlob){
            selfieIdCardBlob = await new Promise(resolve => {
                axios({
                    url: selfieIdCardBlob,
                    method: 'GET',
                    responseType: 'blob', // important
                }).then((response) => {
                    this.closeProgress()
                    var file = response.data ;
                    resolve(file)
                })
            })
        }
        let user = this.state.user
        user.photoUrl = null
        user.idCardUrl = null
        user.selfieIdCardUrl= null
        var formData = new FormData();
        formData.append("userDtoGson", JSON.stringify(user))
        if(photoBlob){
            formData.append("photoMultipartFile", photoBlob);
        }
        if(idCardBlob){
            formData.append("idCardMultipartFile", idCardBlob);
        }
        if(selfieIdCardBlob){
            formData.append("selfieIdCardMultipartFile", selfieIdCardBlob);
        }
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.USER_SAVE_UPLOAD, formData, null, res=>{
                this.setState({
                    user:res.data
                }, () => {
                    this.showDialog("Success", res.message)
                    this.fetchUserDetail(this.state.user.id)
                })
            },  true, true)
        })

    }


    generateAgentCode(){
        let user = this.state.user
        this.get(Global.API.USER_GENERATE_AGENT_CODE, null, null, response=>{
            if(response.code===200){
                user.agentCode = response.data
                this.setState({
                    user:user
                })
            }

        }, true, true)
    }

    fetchUserDetail = (id) => {
        if(id!=null){
            this.get(Global.API.USER, {
                params :{
                    id : id
                }
            }, null, res=>{
                if(res.code===200){
                    let user = res.data;
                    let gender = null
                    GENDERS.forEach(value => {
                        if(value.name===user.gender){
                            gender = value ;
                        }
                    })
                    let userStatus = null
                    USER_STATUSES.forEach(value => {
                        if(value.name===user.userStatus){
                            userStatus = value ;
                        }
                    })

                    this.setState({
                        user:user,
                        userStatus:userStatus,
                        gender:gender,
                        village:user.village?user.village:null,
                        district:user.village?user.village.district:null,
                        city:user.village?user.village.district.city:null,
                        province:user.village?user.village.district.city.province:null,
                    }, () => {
                        if(user.id){
                            this.fetchUserDevices()
                        }
                        if(this.state.village){
                            this.fetchCities(this.state.province.id, cities=>{
                                this.setState({
                                    cities:cities
                                })
                            })
                        }
                        if(this.state.city){
                            this.fetchDistricts(this.state.city.id, districts =>{
                                this.setState({
                                    districts:districts
                                })
                            })
                        }
                        if(this.state.district){
                            this.fetchVillages(this.state.district.id, villages=>{
                                this.setState({
                                    villages:villages
                                })
                            })
                        }

                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true);
        }
    }

    fetchUserDevices = () => {
        let user = this.state.user ;
        this.get(Global.API.USER_DEVICES, {
            params:{
                ascending:this.state.deviceAscending,
                sortir:this.state.deviceSortir,
                search:this.state.deviceSearch,
                userId:user.id,
            }
        }, null, res=>{
            if(res.code === 200){
                this.setState({
                    userDevices : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        }, true, true);
    }



    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
        })
    }

    verifyEmail = (email) =>{
        this.get(Global.API.USER_VALIDATE_EMAIL, {
            params:{
                email:email
            }
        }, null, response=>{
            if(response.code!=200){
                this.setState({
                    emailWarning:response.message
                })
            }
        }, true, true)

    }

    refreshTransaction = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
        })
    )


    openCropper(image, cropperCallback, cancelCallback){
        this.setState({
            image:image,
            imageCropperModal:true,
            cropperCallback:cropperCallback,
            cancelCallback:cancelCallback
        })
    }


    render() {
        let imageAspect = 4/4

        let user = this.state.user?this.state.user:{}
        let gender = this.state.gender?this.state.gender:{}

        let verificationStatusColor = user.verificationStatus&&user.verificationStatus===VERIFICATION_STATUSES[0].name?"green":"#d80718"

        return (
            <Page
                title="User Form"
                breadcrumbs={[{ name: 'user form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this user item to your list ?"
                    okCallback={()=>{
                        this.showProgress()
                        this.saveUpload()
                    }}
                    cancelCallback={this.closeDialog}/>
                <UpdateVerificationStatusModal
                    user={user} modal={this.state.updateVerificationStatusModal}
                    closeCallback={()=>{
                        this.setState({
                            updateVerificationStatusModal:false
                        })
                    }}
                    okCallback={()=>{
                        this.setState({
                            updateVerificationStatusModal:false
                        }, () => {
                            this.fetchUserDetail(user.id)
                        })
                    }}/>

                <Row key={1}>
                    <Col>
                        <Card>
                            <CardHeader>User</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={4}>
                                        <Card>
                                            <CardHeader>
                                                User Image
                                            </CardHeader>
                                            <CardBody>
                                                <CardImg top src={
                                                    user.photoUrl!=null?user.photoUrl:userProfilePicture}
                                                         onClick={e=>{
                                                             if(allIsEmpty(this.state.user, this.state.user.id)) {
                                                                 this.setState({
                                                                     openPreview:true
                                                                 })
                                                             }
                                                         }}
                                                         onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                            </CardBody>
                                            <CardBody style={{verticalAlign:'text-center'}}>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => {
                                                                user.photoUrl = null
                                                                this.setState({
                                                                    user:user
                                                                })
                                                            }}>Cancel</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                imageSelector(file =>{
                                                                    this.openCropper(file, (blob)=>{
                                                                        user.photoUrl = blob
                                                                        this.setState({
                                                                            photoBlob:blob,
                                                                            user:user
                                                                        })
                                                                    },  () =>{
                                                                        this.setState({
                                                                            photoBlob:null,
                                                                        })
                                                                    })
                                                                }).click()
                                                            }}>Upload</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <Card>
                                                            <CardHeader>
                                                                Id Card
                                                            </CardHeader>
                                                            <CardBody>
                                                                <CardImg top src={
                                                                    user.idCardUrl?user.idCardUrl:userProfilePicture}
                                                                         onClick={e=>{
                                                                             if(allIsEmpty(this.state.user, this.state.user.id)) {
                                                                                 this.setState({
                                                                                     openPreview:true
                                                                                 })
                                                                             }
                                                                         }}
                                                                         onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                                            </CardBody>
                                                            <CardFooter>
                                                                <Row>
                                                                    <Col>
                                                                        <ButtonGroup className="float-left">
                                                                            <Button color="danger" onClick={e => (
                                                                                this.setState({
                                                                                    photo:null
                                                                                })
                                                                            )}>Cancel</Button>
                                                                        </ButtonGroup>
                                                                    </Col>
                                                                    <Col>
                                                                        <ButtonGroup className="float-right">
                                                                            <Button color="primary" onClick={e=>{
                                                                                imageSelector(file =>{
                                                                                    this.openCropper(file, (blob)=>{
                                                                                        user.idCardUrl = blob
                                                                                        this.setState({
                                                                                            idCardBlob:blob,
                                                                                            user:user
                                                                                        })
                                                                                    },  () =>{
                                                                                        this.setState({
                                                                                            idCardBlob:null,
                                                                                        })
                                                                                    })
                                                                                }).click()
                                                                            }}>U</Button>
                                                                        </ButtonGroup>
                                                                    </Col>
                                                                </Row>
                                                            </CardFooter>
                                                        </Card>
                                                    </Col>
                                                    <Col>
                                                        <Card>
                                                            <CardHeader>
                                                                Selfie Id Card
                                                            </CardHeader>
                                                            <CardBody>
                                                                <CardImg top src={
                                                                    user.selfieIdCardUrl!=null?user.selfieIdCardUrl:userProfilePicture}
                                                                         onClick={e=>{
                                                                             if(allIsEmpty(user, user.id)) {
                                                                                 this.setState({
                                                                                     openPreview:true
                                                                                 })
                                                                             }
                                                                         }}
                                                                         onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                                            </CardBody>
                                                            <CardFooter>
                                                                <Row>
                                                                    <Col>
                                                                        <ButtonGroup className="float-left">
                                                                            <Button color="danger" onClick={e => (
                                                                                this.setState({
                                                                                    photo:null
                                                                                })
                                                                            )}>Cancel</Button>
                                                                        </ButtonGroup>
                                                                    </Col>
                                                                    <Col>
                                                                        <ButtonGroup className="float-right">
                                                                            <Button color="primary" onClick={e=>{
                                                                                imageSelector(file =>{
                                                                                    this.openCropper(file, (blob)=>{
                                                                                        user.selfieIdCardUrl = blob
                                                                                        this.setState({
                                                                                            selfieIdCardBlob:blob,
                                                                                            user:user
                                                                                        })
                                                                                    },  () =>{
                                                                                        this.setState({
                                                                                            selfieIdCardBlob:null,
                                                                                        })
                                                                                    })
                                                                                }).click()
                                                                            }}>U</Button>
                                                                        </ButtonGroup>
                                                                    </Col>
                                                                </Row>
                                                            </CardFooter>
                                                        </Card>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col><Button
                                                        style={{width:'100%', background:verificationStatusColor}}
                                                        onClick={event => {
                                                            this.setState({
                                                                updateVerificationStatusModal:true
                                                            })
                                                        }}>{!user.verificationStatus?VERIFICATION_STATUSES[1].name:user.verificationStatus}</Button></Col>
                                                </Row>
                                                <br/>
                                                <Row style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Registered At
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.registration?parseDate(user.registration.created, DD_MM_YYYY_HH_MM_SS):"-"}
                                                    </Col>
                                                </Row>
                                                <Row style={{margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Ever logged in
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.firstLogin?"FALSE":"TRUE"}
                                                    </Col>
                                                </Row>
                                                <Row style={{backgroundColor:'#d5d5d5',margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Total Income
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {currencyFormat(user.totalIncome, CURRENCY_SYMBOL)}
                                                    </Col>
                                                </Row>
                                                <Row  style={{ margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Current Balance
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {currencyFormat(user.currentBalance, CURRENCY_SYMBOL)}
                                                    </Col>
                                                </Row>
                                                <Row style={{backgroundColor:'#d5d5d5',margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Total Withdrawal
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {currencyFormat(user.totalWithdrawal, CURRENCY_SYMBOL)}
                                                    </Col>
                                                </Row>
                                                <Row  style={{ margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Task
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.totalTask}
                                                    </Col>
                                                </Row>
                                                <Row  style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        On Going Task
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.onGoingTask}
                                                    </Col>
                                                </Row>
                                                <Row  style={{ margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Completed Task
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.totalCompletedTask}
                                                    </Col>
                                                </Row>
                                                <Row  style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Rejected Task
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {user.totalRejectedTask}
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={8}>
                                        <Card>
                                            <CardBody>
                                                <Card body>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="mobilePhone">Mobile Phone</Label>
                                                                <Input type="text"
                                                                       className="form-control"
                                                                       value={user.mobilePhone}
                                                                       pattern={"[0-9]*"}
                                                                       onChange={(e)=>{
                                                                           let mobilePhone = e.target.value
                                                                           let user = this.state.user
                                                                           if(user!=null && mobilePhone!=null){
                                                                               user.mobilePhone = mobilePhone.replace(/\D/,'')
                                                                               this.setState({
                                                                                   user:user
                                                                               })
                                                                           }
                                                                       }}
                                                                       placeholder={"Enter mobile number"}/>
                                                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.mobilePhoneWarning}</span></FormText>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="email">Email</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="email"
                                                                    value={user.email}
                                                                    onChange={(e) =>{
                                                                        let email = e.target.value
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.email = email
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                        if(!isValidEmail(email)){
                                                                            this.setState({
                                                                                emailWarning:"Invalid email address"
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                emailWarning:undefined
                                                                            }, () => {
                                                                                this.verifyEmail(this.state.user.email)
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter email"
                                                                />
                                                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.emailWarning}</span></FormText>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="phoneNumber">Phone Number</Label>
                                                                <Input type="text"
                                                                       className="form-control"
                                                                       value={user.homePhoneNumber}
                                                                       pattern={"[0-9]*"}
                                                                       onChange={(e)=>{
                                                                           let homePhoneNumber = e.target.value
                                                                           let user = this.state.user
                                                                           if(user!=null && homePhoneNumber!=null){
                                                                               user.homePhoneNumber = homePhoneNumber.replace(/\D/,'')
                                                                               this.setState({
                                                                                   user:user
                                                                               })
                                                                           }
                                                                       }}
                                                                       placeholder={"Enter home phone number"}/>
                                                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.phoneNumberWarning}</span></FormText>
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md={4}>
                                                            <FormGroup>
                                                                <Label for="agentCode">Agent Code</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="agentCode"
                                                                    value={user.agentCode}
                                                                    disabled={true}
                                                                    onChange={(e) =>{
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.agentCode = e.target.value
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Click Button to Generate"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col md={2}>
                                                            <FormGroup>
                                                                <Label for="postalCode">&nbsp;</Label>
                                                                <br/>
                                                                <Button onClick={event => {
                                                                    this.generateAgentCode()
                                                                }}>{
                                                                    user.agentCode?"Regenerate":"Generate"
                                                                }</Button>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col md={6}>
                                                            <FormGroup>
                                                                <Label for="organization">Organization</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="organization"
                                                                    value={user.organization}
                                                                    onChange={(e) =>{
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.organization = e.target.value
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter organization"
                                                                />
                                                            </FormGroup>
                                                        </Col>

                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="firstname">Firstname</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="firstname"
                                                                    value={(this.state.user!=null && this.state.user.firstname)?this.state.user.firstname:""}
                                                                    onChange={(e) =>{
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.firstname = e.target.value
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter firstname"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="lastname">Lastname</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="lastname"
                                                                    value={(this.state.user!=null && this.state.user.lastname)?this.state.user.lastname:""}
                                                                    onChange={(e) =>{
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.lastname = e.target.value
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter lastname"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="birthdate">Birthdate</Label>
                                                                <DateInput
                                                                    id="birthdate"
                                                                    maxdate={new Date()}
                                                                    value={user.birthdate}
                                                                    onChange={(e) =>{
                                                                        let value = e.target.value
                                                                        user.birthdate = value
                                                                        this.setState({
                                                                            user:user
                                                                        })
                                                                    }}
                                                                    placeholder="Enter birthdate"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Gender"}
                                                                    objects={GENDERS}
                                                                    default={gender.id?gender.id:null}
                                                                    callback={(gender)=>{
                                                                        user.gender = gender.name
                                                                        this.setState({
                                                                            user:user,
                                                                            gender:gender
                                                                        })
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Status"}
                                                                    objects={USER_STATUSES}
                                                                    default={this.state.userStatus!=null && this.state.userStatus.id!=undefined?this.state.userStatus.id:null}
                                                                    callback={(userStatus)=>{
                                                                        let user = this.state.user
                                                                        if(user!=null && userStatus!=null){
                                                                            user.userStatus = userStatus.name
                                                                            this.setState({
                                                                                user:user,
                                                                                userStatus:userStatus
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                                <br/>
                                                <Card body>
                                                    <Row>
                                                        <Col md={9}>
                                                            <FormGroup>
                                                                <Label for="address">Address</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="address"
                                                                    value={user.address}
                                                                    onChange={(e) =>{
                                                                        let user = this.state.user
                                                                        if(user!=null){
                                                                            user.address = e.target.value
                                                                            this.setState({
                                                                                user:user
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter address"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col md={3}>
                                                            <FormGroup>
                                                                <Label for="postalCode">Postal Code</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="postalCode"
                                                                    disabled={true}
                                                                    value={(this.state.user!=null && this.state.user.village)?this.state.user.village.postalCode:""}
                                                                    placeholder="Postal Code"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Province"}
                                                                    objects={this.state.provinces}
                                                                    default={!allIsEmpty(this.state.province)?this.state.province.id:null}
                                                                    callback={(province)=>{
                                                                        if(province!=null){
                                                                            this.setState({
                                                                                province:province,
                                                                                cities:[],
                                                                                districts:[],
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchCities(province.id, cities=>{
                                                                                    this.setState({
                                                                                        cities:cities
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                province:null,
                                                                                cities:[],
                                                                                districts:[],
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select City"}
                                                                    objects={this.state.cities}
                                                                    default={!allIsEmpty(this.state.city)?this.state.city.id:null}
                                                                    callback={(city)=>{
                                                                        if(city!=null){
                                                                            this.setState({
                                                                                city:city,
                                                                                districts:[],
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchDistricts(city.id, districts =>{
                                                                                    this.setState({
                                                                                        districts:districts
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                city:null,
                                                                                districts:[],
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select District"}
                                                                    objects={this.state.districts}
                                                                    default={!allIsEmpty(this.state.district)?this.state.district.id:null}
                                                                    callback={(district)=>{
                                                                        if(district!=null){
                                                                            this.setState({
                                                                                district:district,
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchVillages(district.id, villages=>{
                                                                                    this.setState({
                                                                                        villages:villages
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                district:null,
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Village"}
                                                                    objects={this.state.villages}
                                                                    default={!allIsEmpty(this.state.village)?this.state.village.id:null}
                                                                    callback={(village)=>{
                                                                        if(village!=null){
                                                                            let user = this.state.user
                                                                            user.village = village
                                                                            this.setState({
                                                                                user:user,
                                                                                village:village,
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    user:{}
                                                                }, () => {
                                                                    this.props.history.goBack();
                                                                })
                                                            )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.user!=null && this.state.user.id!=null)?"Update":"Create"
                                                            } </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {
                    !isEmpty(user.id)?
                        this.renderDevice()
                        :
                        null
                }
                <ImageCropper src={this.state.image} aspect={imageAspect} show={this.state.imageCropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        let cropperCallback = this.state.cropperCallback
                        this.setState({
                            image:file,
                            imageCropperModal:false,
                        })
                        if(cropperCallback){
                            cropperCallback(file)
                        }
                    })
                }} cancelCallback={()=>{
                    this.setState({
                        image:null,
                        imageCropperModal:false,
                    })
                    let cancelCallback = this.state.cancelCallback
                    if(cancelCallback){
                        cancelCallback()
                    }
                }}/>

            </Page>

        );
    }

    renderDevice = () =>{
        var i = 0 ;
        return (
            <>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this userDevice item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <Card className="mb-6">
                    <CardHeader>UserDevice</CardHeader>
                    <CardBody>
                        <Row>
                            <Col md={2}>
                                Sort By :
                                <UncontrolledButtonDropdown key={1}>
                                    <DropdownToggle
                                        caret
                                        color="white"
                                        className="text-capitalize m-1">
                                        {
                                            sortirMap(this.state.sortir)
                                        }
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "ud.created", true))}>Created</DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledButtonDropdown>
                            </Col>
                            <Col md={2}>
                                Sortir :
                                <UncontrolledButtonDropdown key={2}>
                                    <DropdownToggle
                                        caret
                                        color="white"
                                        className="text-capitalize m-1">
                                        {
                                            this.state.ascending?"Ascending":"Descending"
                                        }
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem onClick={e=>(this.refreshUserDevice(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                        <DropdownItem onClick={e=>(this.refreshUserDevice(false, this.state.sortir, true))}>Descending</DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledButtonDropdown>
                            </Col>
                            <Col md={3}>
                                <SearchInput
                                    placeholder={"Search device id..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                deleteParam(this.props, 'page')
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            deleteParam(this.props, 'page')
                                        }
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Table hover>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Platform</th>
                                    <th>Device ID</th>
                                    <th style={{maxWidth:'300px'}}>FCM Token</th>
                                    <th>Created</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.userDevices&&this.state.userDevices.map((item, index)=>(
                                        <tr key={item.id}>
                                            <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                            <td>{item.platform}</td>
                                            <td>{item.deviceId}</td>
                                            <td style={{maxWidth:'300px'}}>{item.fcmToken}</td>
                                            <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                            <td>
                                                <Button size="sm" color="danger" onClick={e => {
                                                    e.preventDefault()
                                                    this.setState({
                                                        userDevice:item
                                                    }, () => {
                                                        this.confirmDelete()
                                                    })
                                                }}>
                                                    <MdDelete/>
                                                </Button>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </Table>
                        </Row>
                    </CardBody>
                </Card>
                <br/>
            </>
        );
    }
    renderTransaction = () =>{
        return (
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader>Transaction</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={3}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir.toString())
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "refCode"))}>Ref Code</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "item"))}>Item</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "quantity"))}>Quantity</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "price"))}>Price</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "created"))}>Created</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "updated"))}>Last Updated</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(true, this.state.sortir))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(false, this.state.sortir))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    <SearchInput
                                        placeholder={"Search by ref code..."}
                                        value={this.state.refCode}
                                        onChange={e=>{
                                            this.setState({
                                                refCode:e.target.value
                                            }, () => {
                                                if(this.state.refCode===''){
                                                    this.refreshTransaction(this.state.ascending, this.state.sortir)
                                                }
                                            })
                                        }}
                                        onEnter={e=>{
                                            if(this.state.refCode!==''){
                                                this.refreshTransaction(this.state.ascending, this.state.sortir)
                                            }
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={key =>{
                                if(key==='All'){
                                    this.setState({
                                        transactionStatusId:null
                                    })
                                }else{
                                    this.setState({
                                        transactionStatusId:key
                                    })
                                }
                            }
                            }>
                                <Tab eventKey="All" title="All">
                                    {this.renderTransactionTable()}
                                </Tab>
                                {
                                    this.state.transactionStatuses.map((item, index)=>(
                                        <Tab eventKey={item.id} title={item.name}>
                                            {this.renderTransactionTable()}
                                        </Tab>
                                    ))
                                }
                            </Tabs>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }

    renderTransactionTable = () =>{
        return (
            <Card>
                <CardBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref Code</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Total Pay</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>last Update</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.transactions.map((item, index)=>(
                                <tr key={item.id}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.refCode}</td>
                                    <td>{item.itemCount}</td>
                                    <td>{item.quantity}</td>
                                    <td>{currencyFormat(item.totalPay, CURRENCY_SYMBOL)}</td>
                                    <td>{item.transactionStatus&&item.transactionStatus.name}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td><IoMdEye color="green" onClick={event=>{
                                        this.props.history.push('/transactionDetail?id='+item.id)
                                    }}/></td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }
}