import BasePage from "./BasePage";
import React from 'react'
import {
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {
    changeParam,
    changeParam2,
    changeParam3,
    currencyFormat,
    deleteParam, hasBeenDelivery,
    parseDate,
    sortirMap
} from "../utils/Utilities";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye, IoMdCar, IoMdBus, IoMdBicycle, IoMdNavigate} from "react-icons/io";
import SearchInput from '../components/SearchInput'
import Global, {_DELIVERY, CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS, PRINT_DATA} from "../utils/Global";
import {IoMdOpen} from "react-icons/io/index";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import Button from "reactstrap/es/Button";
import {MdPrint, MdRefresh, MdSearch} from "react-icons/md/index";
import SockJsClient from 'react-stomp'
import {storeData} from "../utils/StorageUtil";



export default class TransactionPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:false,
            sortir:'created',
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            key:queryString.parse(this.props.query).tab?queryString.parse(this.props.query).tab:"All",
            totalPage:0,
            totalElement:0,
            pageElement:0,
            transactionStatuses:[],
            transactions:[],
            transactionStatus:null,
            transactionStatusId:null,
            search:queryString.parse(this.props.query).search?queryString.parse(this.props.query).search:"",
            inputSearch : "",
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchTransactionStatus()
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            let page = queryString.parse(props.query).page?queryString.parse(props.query).page:1
            let key = queryString.parse(props.query).tab?queryString.parse(props.query).tab:"All"
            let search = queryString.parse(props.query).search?queryString.parse(props.query).search:""
            let transactionStatusId = null
            if(key==="All"){
                transactionStatusId = null
            }else{
                transactionStatusId = key
            }
            this.setState({
                page:page,
                key:key,
                search:search,
                transactionStatusId:transactionStatusId
            }, () => {
                this.fetchTransaction(true)
            })
        }
    }


    fetchTransactionStatus = () =>{
        this.get(Global.API.TRANSACTION_STATUSES, null, null, response=>{
            if(response.code===200){
                this.setState({
                    transactionStatuses : response.data
                }, () => {
                    this.fetchTransaction(true)
                })
            }
        }, true, true)
    }

    fetchTransaction = (progressing) =>{
        this.get(Global.API.TRANSACTIONS, {
            params:{
                transactionStatusId:this.state.transactionStatusId,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    transactions : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,

                })
            }
        }, progressing, true)
    }

    refreshTransaction = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
            this.fetchTransaction(progressing)
        })
    )


    render() {
        return (
            <div>
                {super.render()}
                {this.renderTransaction()}
            </div>
        )
    }

    renderTransaction = () =>{
        return (
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader>Transaction</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={3}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir.toString())
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "search", true))}>Ref Code</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "item", true))}>Item</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "quantity", true))}>Quantity</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "totalPay", true))}>Total Pay</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "updated", true))}>Last Updated</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(false, this.state.sortir, true))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={4}>
                                    <Row>
                                        <Col>
                                            <SearchInput
                                                placeholder={"Customer name, Ref code, Resi Code..."}
                                                value={this.state.inputSearch}
                                                onChange={e=>{
                                                    this.setState({
                                                        inputSearch:e.target.value
                                                    }, () => {
                                                        if(this.state.inputSearch===''){
                                                            changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.inputSearch!==''){
                                                        changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col>
                                            {/*<Button onClick={event=>{*/}
                                                {/*let key = this.state.key*/}
                                                {/*changeParam3(this.props, 'page', 1, 'tab', key, 'search', this.state.inputSearch)*/}
                                            {/*}}><MdSearch/></Button>*/}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={key =>{
                                    changeParam2(this.props, 'page', 1, 'tab', key)
                                }
                                }>
                                <Tab eventKey="All" title="All">
                                    {this.renderTransactionTable()}
                                </Tab>
                                {
                                    this.state.transactionStatuses.map((item, index)=>(
                                        <Tab key={index} eventKey={item.id} title={item.name}>
                                            {this.renderTransactionTable()}
                                        </Tab>
                                    ))
                                }
                            </Tabs>
                        </CardBody>
                        <SockJsClient
                            url={Global.BASE_URL}
                            topics={[Global.API.LISTEN_TRANSACTION]}
                            onMessage={(message) => {
                                this.successToast(message)
                                this.fetchTransaction(true)
                            }}
                            ref={ (client) => { this.clientRef = client }} />
                    </Card>
                </Col>
            </Row>
        )
    }


    renderTransactionTable = () =>{
        return (
            <Card>
                <CardBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref Code</th>
                            <th>Customer</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Total Pay</th>
                            <th>Status</th>
                            <th>Voucher</th>
                            <th>Update</th>
                            <th>Resi & Tracking</th>
                            <th>View</th>
                            <th>Print</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.transactions.map((item, index)=>(
                                <tr key={item.id}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.refCode}</td>
                                    <td>
                                        {item.customer.firstname+" "+item.customer.lastname}
                                        &nbsp;&nbsp;
                                        <IoMdOpen
                                            color="blue" style={{cursor:'pointer'}}
                                            onClick={e=>{
                                                this.props.history.push('/customerDetail?id='+item.customer.id)
                                            }
                                        }/>
                                    </td>
                                    <td>{item.itemCount}</td>
                                    <td>{item.quantity}</td>
                                    <td>{currencyFormat(item.totalPay, CURRENCY_SYMBOL)}</td>
                                    <td>{item.transactionStatus?item.transactionStatus.name:"Unknown"}</td>
                                    <td>{item.voucher?"Yes":"No"}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>
                                        <Button color={item.resiCode?"primary":"gray"} onClick={e=>{
                                            if(item.resiCode){
                                                this.get(Global.API.COURIER_WAYBILL, {params:{
                                                        resiCode:item.resiCode,
                                                        serviceName:item.courierCode}}, null, response=>{
                                                        if(response.code===200 && response.data.result){
                                                            this.showTrackingModal(true, response.data.result, item.courierName, item.cost?item.cost.service:"Unknown", item.resiCode)
                                                        }
                                                }, true, true)
                                            }
                                        }}>
                                            <IoMdNavigate color="white"/>
                                        </Button>
                                        &nbsp;
                                        {
                                            item.resiCode?item.resiCode:""
                                        }
                                    </td>
                                    <td>
                                        <IoMdEye
                                            color="green"
                                            style={{cursor:'pointer'}}
                                            onClick={e=>{
                                                this.props.history.push('/transactionDetail?id='+item.id)
                                            }}
                                         />
                                    </td>
                                    <td>
                                        <Button color="green" onClick={event=>{
                                            event.preventDefault()
                                            storeData(PRINT_DATA, item)
                                            let printPageWindow = window.open('/printInvoice', '_blank')
                                            if(printPageWindow){
                                                printPageWindow.addEventListener('DOMContentLoaded', (event) => {
                                                    setInterval(()=>{
                                                        printPageWindow.postMessage("PRINT", "*")
                                                    }, 300)
                                                });
                                            }
                                        }}><MdPrint/></Button>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }


}