import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDetails} from "react-icons/md/index";
import ActiveLabel from "../components/Widget/ActiveLabel";


const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class PaymentMediaPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      paymentMedias : [],
      paymentMedia:{},
      modalAddConfirm:false,
      modal: false,
      ascending:true,
      sortir:'name',
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }


  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
    })
  }

  fetchAll = () => {
    this.get(Global.API.PAYMENT_MEDIAS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          paymentMedias : response.data,
          totalPage: response.totalPage,
          totalElement: response.totalElement,
          pageElement: response.pageElement,
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    return (
        <Page
            title="Payment Media Detail"
            breadcrumbs={[{ name: 'Payment Media detail', active: true }]}
            className="TablePage">
          {super.render()}
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Payment Media Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col>
                      Sort By :
                      <UncontrolledButtonDropdown key={1}>
                        <DropdownToggle
                            caret
                            color="white"
                            className="text-capitalize m-1">
                          {
                            this.state.sortir
                          }
                        </DropdownToggle>
                        <DropdownMenu>
                          <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                          <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                          <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                        </DropdownMenu>
                      </UncontrolledButtonDropdown>
                    </Col>
                    <Col>
                      Sortir :
                      <UncontrolledButtonDropdown key={2}>
                        <DropdownToggle
                            caret
                            color="white"
                            className="text-capitalize m-1">
                          {
                            this.state.ascending?"Ascending":"Descending"
                          }
                        </DropdownToggle>
                        <DropdownMenu>
                          <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                          <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                        </DropdownMenu>
                      </UncontrolledButtonDropdown>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Table hover>
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th width="20%">Domains</th>
                          <th>Client Key</th>
                          <th>Server Key</th>
                          <th>Environment</th>
                          <th>Payment Expired</th>
                          <th>Status</th>
                          <th>Updated</th>
                          <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                          this.state.paymentMedias.map((item, index) =>(
                              <tr key={index}>
                                <th scope="row">{(++index)}</th>
                                <td>{item.name}</td>
                                <td width="20%">
                                  <Row>
                                    <Col>
                                      Base
                                    </Col>
                                    <Col>
                                      <strong>{item.baseDomain}</strong>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col>
                                      API
                                    </Col>
                                    <Col>
                                      <strong>{item.apiDomain}</strong>
                                    </Col>
                                  </Row>
                                  <Row>
                                    <Col>
                                      Simulator
                                    </Col>
                                    <Col>
                                      <a href={item.simulatorDomain?item.simulatorDomain:""} target="_blank">{item.simulatorDomain}</a>
                                    </Col>
                                  </Row>
                                </td>
                                <td>{item.clientKey?item.clientKey:"-"}</td>
                                <td>{item.serverKey?item.serverKey:"-"}</td>
                                <td>{item.environmentMode?item.environmentMode:"-"}</td>
                                <td>{item.limitTransferHour} (Hour)</td>
                                <td><ActiveLabel active={item.active}/></td>
                                <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                <td>
                                  <Button color="primary" onClick={e=>{
                                      if(item.midtransMediators.length>0){
                                        this.props.history.push("/midtransMediators")
                                      }
                                    if(item.banks.length>0){
                                      this.props.history.push("/banks")
                                    }
                                  }}><MdDetails/>
                                  </Button>
                                </td>
                              </tr>
                          ))
                        }
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
