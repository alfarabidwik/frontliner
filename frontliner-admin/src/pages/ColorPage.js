import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import { SketchPicker } from 'react-color';
import ActiveLabel from "../components/Widget/ActiveLabel";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class ColorPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      colors : [],
      color:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'created',
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var color = this.state.color
    if(isEmpty(color.name)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.COLOR_SAVE,  this.state.color, null, res=>{
        if(res.code===200){
          this.setState({
            color:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.COLOR_DELETE+"/"+this.state.color.id, null, null, res=>{
        if(res.code===200){
          this.setState({
            color:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.COLORS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          colors : response.data
        })
      }
    }, true, true);
  }

  onPickerChange = (color) =>{
    let selectedColor = this.state.color
    if(selectedColor){
      selectedColor.hexa = color.hex
      this.setState({
        color:selectedColor
      })
    }
  }

  refreshColor = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    return (
        <Page
            title="Color Detail"
            breadcrumbs={[{ name: 'color detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this color item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this color item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Color Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <Col>
                            <SketchPicker
                                width={""}
                                color={this.state.color?this.state.color.hexa:""}
                                onChangeComplete={ this.onPickerChange }
                            />
                          </Col>
                        </Row>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.color.name)?this.state.color.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.color
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        color:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter color name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Hexa</Label>
                              <Input
                                  type="text"
                                  name="hexa"
                                  value={!isEmpty(this.state.color.hexa)?this.state.color.hexa:""}
                                  readOnly
                              />
                            </FormGroup>
                            <ActiveOption
                                callback={(active)=>{
                                  let color = this.state.color
                                  if(color!=null){
                                    color.active = active
                                    this.setState({
                                      color:color
                                    })
                                  }

                                }}
                                default={this.state.color?this.state.color.active:false}
                            />
                          </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    color:{}
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.color!=null && this.state.color.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refreshColor(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refreshColor(this.state.ascending, "hexa"))}>Hexa</DropdownItem>
                                <DropdownItem onClick={e=>(this.refreshColor(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refreshColor(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refreshColor(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refreshColor(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Hexa</th>
                                <th>Color</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.colors.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td>{item.hexa}</td>
                                      <td>
                                        <div key={index} className="float-left" style={{backgroundColor:item.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>

                                      </td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button size="sm" color="danger" onClick={e => {
                                          this.setState({
                                            color:item
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button size="sm" color="primary" onClick={ e => {
                                          this.setState({
                                            color:item
                                          })
                                        }}>
                                          <MdEdit/>
                                        </Button>

                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
