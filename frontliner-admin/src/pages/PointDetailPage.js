import React from 'react';
import BasePage from "./BasePage";

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import Page from "../components/Page";
import ItemOption from "../components/Widget/ItemOption";
import Global, {ANY_ITEM, DD_MM_YYYY_HH_MM_SS, ITEM_POINT, PERCENTAGE_POINT, PURCHASE_POINT} from "../utils/Global";
import {allIsEmpty, imageSelector, isEmpty, parseDate, sortirMap} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit, MdSearch} from "react-icons/md";
import ProductModal from '../components/modal/ProductModal'
import PointInventoryModal from '../components/modal/PointInventoryModal'
import {MdPublish} from "react-icons/md/index";
import PriceInput from "../components/Widget/PriceInput";
import DateInput from "../components/Widget/DateInput";
import Pagination from '../components/Pagination'
import SearchInput from '../components/SearchInput'


export default class PointDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let state = this.state
        state.point={}
        state.pointIdQuery= queryString.parse(this.props.query).id
        state.modalUpdateConfirm=false
        state.modalDeleteConfirm=false
        state.products=[]
        state.product=null
        state.search=""
        state.page=queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
        state.ascending=true
        state.sortir='p.created'
        state.modalUpdateItem=false
        state.modalActivation=false
        state.modalUpdatingLabel=""
        state.edit=false
        state.totalPage=0
        state.totalElement=0
        state.pageElement=0
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAllProducts(this.state.pointIdQuery)
            })
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchPointDetail(this.state.pointIdQuery)
    }

    confirmSave = (callback) =>{
        var point = this.state.point
        if(allIsEmpty(point.name, point.description, point.point, point.periodType, point.benefitType, point.pointType)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            if(point.periodType===Global._DYNAMIC){
                if(!point.startPeriod || !point.endPeriod){
                    this.showDialog("Notice", "Please complete this form fields")
                    return ;
                }
            }
            if(point.periodType===Global._STATIC){
                if(isEmpty(point.validDay)){
                    this.showDialog("Notice", "Please complete this form fields")
                    return ;
                }
            }
            if(!point.point){
                this.showDialog("Notice", "Please complete this form fields")
                return ;
            }
            if(point.benefitType===Global._PURCHASE_POINT){
                if(!point.purchaseNominal){
                    this.showDialog("Notice", "Please complete this form fields")
                    return ;
                }
            }

            this.openConfirmDialog("Confirmation", "Do you want to update this point item ?", ()=>{
                this.save(callback)
            })

            // this.setState({
            //     modalUpdatingLabel:"Do you want to update this point item ?",
            //     modalUpdateConfirm:true
            // })
        }
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.product!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.PRODUCT_DELETE+"/"+this.state.product.id, null, null, res => {
                    if(res.code===200){
                        this.setState({
                            product:null
                        }, () => {
                            this.fetchAllProducts(this.state.point.id)
                        })
                    }else{

                    }
                }, true, true);
            })
        }
    }


    save = (callback) => {
        this.setState({
            modalUpdateConfirm:false
        }, () => {
            this.post(Global.API.POINT_SAVE,this.state.point, null,res=>{
                if(res.code===200){
                    this.successToast(res.message)
                    this.setState({
                        point:res.data
                    }, () => {
                        if(callback){
                            callback()
                        }else{
                            this.fetchPointDetail(this.state.point.id)
                        }
                    })
                }else{
                    this.errorToast(res.message)
                }
            },  true, true)
        })
    }


    fetchPointDetail = (id) => {
        if(id!=null){
            this.get(Global.API.POINT, {
                params :{
                    id : id
                }
            }, null, res =>{
                if(res.code===200){
                    this.setState({
                        point:res.data,
                        image:res.data.imageLink
                    }, () => {
                        this.fetchAllProducts(this.state.point.id)
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }

    fetchAllProducts = (pointId) =>{
        this.get(Global.API.POINT_PRODUCTS, {
            params:{
                pointId:pointId,
                search:this.state.search,
                page:this.state.page-1,
                ascending:this.state.ascending,
                sortir:this.state.sortir
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    products : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }

        }, true, true)
    }


    closePointModal = () =>{
        this.setState({
            modalUpdateItem:false,
            product:{}
        }, () => {
            this.fetchAllProducts(this.state.point.id)
        })
    }

    closeDialog = () =>{
        this.setState({
            modalUpdateConfirm:false,
            modalDeleteConfirm:false,
            modalActivation:false,
        }, () => {
            this.fetchPointDetail(this.state.point.id)
        })
    }

    refreshProduct = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAllProducts(this.state.point.id)
        })
    )

    closeProductInventoryModal = () =>{
        this.setState({
            modalUpdateItem:this.state.edit,
            pointInventoryModal:false
        }, () => {
            this.refreshProduct(this.state.ascending, this.state.sortir)
        })
    }


    openProductInventoryModal  = (product) =>{
        this.setState({
            product:product,
            pointInventoryModal:true
        })
    }

    activationConfirm = () =>{
        this.setState({
            modalUpdatingLabel:!this.state.point.active?"Do you want to publish this point for your customer?":"Do you want to unpublish this point for your customer ?",
            modalActivation:true,
        })
    }

    activate = ()=>{
        this.get(Global.API.POINT_ACTIVATE, {
            params:{
                id:this.state.point.id,
                active:!this.state.point.active
            }
        }, null, response=>{
            this.closeDialog()
        }, true, true)
    }

    render() {
        let point = this.state.point?this.state.point:{}
        return (
            <Page
                title="Point Form"
                breadcrumbs={[{ name: 'point form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalUpdateConfirm}
                    title="Updating Confirmation"
                    message={this.state.modalUpdatingLabel}
                    okCallback={this.save}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalActivation}
                    title="Activation Confirmation"
                    message={this.state.modalUpdatingLabel}
                    okCallback={this.activate}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <ProductModal
                    showing={this.state.modalUpdateItem}
                    edit={this.state.edit}
                    point={this.state.point}
                    close={this.closePointModal}
                    openProductInventoryModal={this.openProductInventoryModal}
                    refresh={this.refresh}
                />
                <PointInventoryModal
                    showing={this.state.pointInventoryModal}
                    edit={this.state.edit}
                    product={this.state.product}
                    point={this.state.point}
                    close={this.closeProductInventoryModal}
                    refresh={this.refresh}
                />


                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Point</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="name">Name</Label>
                                            <Input
                                                type="text"
                                                name="name"
                                                value={point.name}
                                                onChange={(e) =>{
                                                    e.preventDefault()
                                                    point.name = e.target.value
                                                    this.setState({
                                                        point:point
                                                    })
                                                }}
                                                placeholder="Enter point name"
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="description">Description</Label>
                                            <Input
                                                type="textarea"
                                                name="description"
                                                rows="5"
                                                value={point.description}
                                                onChange={(e) =>{
                                                    point.description = e.target.value
                                                    this.setState({
                                                        point:point
                                                    })
                                                }}
                                                placeholder="Enter description"
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Point Type"}
                                                objects={Global.POINT_TYPE_OPTIONS}
                                                default={point.pointType}
                                                callback={(pointType)=>{
                                                    point.pointType = pointType.id
                                                    this.setState({
                                                        point:point
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                        {
                                            (point.pointType)&&(
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="point">Point Bonus {(point.pointType&&point.pointType===PERCENTAGE_POINT)&&(<span>%</span>)}</Label>
                                                            <Input
                                                                type="text"
                                                                name="point"
                                                                value={point.point}
                                                                maxLength="9"
                                                                onChange={(e) =>{
                                                                    point.point = e.target.value.replace(/\D/,'')
                                                                    this.setState({
                                                                        point:point
                                                                    })
                                                                }}
                                                                placeholder={"0"}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    {
                                                        (point.pointType&&point.pointType===PERCENTAGE_POINT)&&(
                                                            <Col>
                                                                <FormGroup>
                                                                    <Label>&nbsp;</Label>
                                                                    <Input value="%" readOnly={true}/>
                                                                </FormGroup>
                                                            </Col>
                                                        )
                                                    }
                                                </Row>
                                            )
                                        }
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Period"}
                                                objects={[{id:"all_time",name:"All Time"},{id:"dynamic",name:"Dynamic"},{id:"static",name:"Static"}]}
                                                default={point.periodType}
                                                callback={(periodType)=>{
                                                    point.periodType = periodType.id
                                                    this.setState({
                                                        point:point
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                        {
                                            (point.periodType &&point.periodType===Global._DYNAMIC)?(
                                                <Row>
                                                    <Col>
                                                        <Label for="startPeriod">Start Period</Label>
                                                        <DateInput
                                                            id="startPeriod"
                                                            maxdate={point.endPeriod}
                                                            mindate={new Date()}
                                                            value={point.startPeriod}
                                                            onChange={(e) =>{
                                                                let value = e.target.value
                                                                let point = this.state.point
                                                                point.startPeriod = value
                                                                this.setState({
                                                                    point : point
                                                                })
                                                            }}
                                                            placeholder="Enter start period">
                                                        </DateInput>
                                                    </Col>
                                                    <Col>
                                                        <Label for="endPeriod">End Period</Label>
                                                        <DateInput
                                                            id="endPeriod"
                                                            maxdate={null}
                                                            mindate={point.startPeriod}
                                                            value={point.endPeriod}
                                                            onChange={(e) =>{
                                                                let value = e.target.value
                                                                let point = this.state.point
                                                                point.endPeriod = value
                                                                this.setState({
                                                                    point : point
                                                                })
                                                            }}
                                                            placeholder="Enter end period">
                                                        </DateInput>
                                                    </Col>
                                                </Row>
                                            ):(null)
                                        }
                                        {
                                            (point.periodType &&point.periodType===Global._STATIC)?(
                                                <Row>
                                                    <Col>
                                                        <Label for="validDay">Valid Day</Label>
                                                        <Input
                                                            id="validDay"
                                                            type="text"
                                                            name="validDay"
                                                            value={this.state.point.validDay}
                                                            onChange={(e) =>{
                                                                let value = e.target.value.replace(/\D/,'')
                                                                let point = this.state.point
                                                                point.validDay = value
                                                                this.setState({
                                                                    point : point
                                                                })
                                                            }}
                                                            placeholder="0"
                                                        />
                                                    </Col>
                                                    <Col>
                                                    </Col>
                                                </Row>
                                            ):(null)

                                        }
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Benefit"}
                                                objects={
                                                    [
                                                        {id:Global._ITEM_POINT,name:Global.ITEM_POINT},
                                                        {id:Global._PURCHASE_POINT,name:Global.PURCHASE_POINT}]}
                                                default={this.state.point!=null?this.state.point.benefitType:null}
                                                callback={(periodType)=>{
                                                    let prd = this.state.point
                                                    if(prd!=null){
                                                        prd.benefitType = periodType.id
                                                        this.setState({
                                                            point:prd,
                                                        })
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                        {
                                            (this.state.point!=null&&this.state.point.benefitType
                                                && (this.state.point.benefitType===Global._PURCHASE_POINT))?(
                                                <FormGroup>
                                                    <Label for="purchaseNominal">Purchase Minimum</Label>
                                                    <PriceInput
                                                        name="purchaseNominal"
                                                        className="form-control"
                                                        value={this.state.point.purchaseNominal}
                                                        onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                            e.preventDefault()
                                                            let point = this.state.point
                                                            point.purchaseNominal = floatvalue
                                                            this.setState({
                                                                point: point
                                                            })
                                                        }}
                                                        placeholder="0">
                                                    </PriceInput>
                                                </FormGroup>
                                            ):(null)
                                        }
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <ButtonGroup className="float-left">
                                            <Button color="danger" onClick={e => (
                                                this.setState({
                                                    point:null
                                                }, () => {
                                                    this.props.history.goBack();
                                                })
                                            )}> Back </Button>
                                        </ButtonGroup>
                                    </Col>
                                    <Col>
                                        <ButtonGroup className="float-right">
                                            <Button color="primary" onClick={e=>{
                                                e.preventDefault()
                                                this.confirmSave()
                                            }}> {
                                                (this.state.point!=null && this.state.point.id!=null)?"Update":"Save"
                                            } </Button>
                                            &nbsp;&nbsp;&nbsp;
                                            <Button color={this.state.point.active?"danger":"warning"} onClick={e=>{
                                                e.preventDefault()
                                                this.activationConfirm()
                                            }}><MdPublish/>&nbsp;{this.state.point.active?"Unpublish":"Publish"}</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {
                    (this.state.point!=null && this.state.point.id!=null && this.state.point.benefitType && this.state.point.benefitType==ITEM_POINT)&&
                        <Row key={2}>
                        <Col>
                            <Card className="mb-6">
                                <CardHeader>Point Item</CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col md={4}>
                                                    Sort By :
                                                    <UncontrolledButtonDropdown key={1}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                sortirMap(this.state.sortir)
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "created"))}>Created</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "quantity"))}>Quantity</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "weight"))}>Weight</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "price"))}>Price</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "updated"))}>Last Updated</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                                <Col md={4}>
                                                    Sortir :
                                                    <UncontrolledButtonDropdown key={2}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                this.state.ascending?"Ascending":"Descending"
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir))}>Ascending</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir))}>Descending</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={3}>
                                            <SearchInput
                                                placeholder={"Type and enter to search..."}
                                                value={this.state.search}
                                                onChange={e=>{
                                                    this.setState({
                                                        search:e.target.value
                                                    }, () => {
                                                        if(this.state.search===''){
                                                            this.fetchAllProducts(this.state.point.id)
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.search!==''){
                                                        this.fetchAllProducts(this.state.point.id)
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <ButtonGroup className="float-right">
                                                <Button color="primary" onClick={e=>{
                                                    this.confirmSave(()=>{
                                                        this.setState({
                                                            edit:true,
                                                            modalUpdateItem : true
                                                        })
                                                    })
                                                }}>Update Item</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Table hover>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Heading</th>
                                                <th>Category</th>
                                                <th>Available Stock</th>
                                                <th>Last Update</th>
                                                <th>Edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.products.map((item, index)=>(
                                                    <tr key={index}>
                                                        <th scope="row">{++index}</th>
                                                        <td>{item.name}</td>
                                                        <td>{item.heading?item.heading.name:"-"}</td>
                                                        <td>{item.category?item.category.name:"-"}</td>
                                                        <td>{item.inventoryQuantity}</td>
                                                        <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                        <td>
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.setState({
                                                                    edit:false
                                                                }, () => {
                                                                    this.openProductInventoryModal(item)
                                                                })
                                                            }}>
                                                            <MdEdit/>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </CardBody>
                                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                            </Card>
                        </Col>
                        </Row>

                }
            </Page>
        );
    }


}