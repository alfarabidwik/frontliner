import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Input,
  Label,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, cloneObject, isEmpty, parseDate, currencyFormat} from "../utils/Utilities";
import {MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdPublish} from "react-icons/md/index";
import NumberInput from "../components/Widget/NumberInput";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class ConfigurationPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      configurations : [],
      configuration:{},
      modalAddConfirm:false,
      modalPublishConfirm:false,
      modal: false,
      ascending:true,
      sortir:'created',
      provinces:[],
      cities:[],
      districts:[],
      thumbnailBlob:null,
      imageBlob:null,

    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))
    this.fetchAll()
  }

  confirmSave = () =>{
    var configuration = this.state.configuration
    if(allIsEmpty(configuration.name, configuration.companyName)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.CONFIGURATION_SAVE, this.state.configuration, null, res => {
        if(res.code===200){
          this.successToast(res.message)
          this.setState({
            configuration:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  confirmDelete = () =>{
    this.setState({
      modalPublishConfirm:true
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalPublishConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.CONFIGURATIONS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        let configurations = response.data
        this.setState({
          configurations : configurations
        }, () => {
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )

  activate = () =>{
    this.get(Global.API.CONFIGURATION_ACTIVATE, {params:{
        configurationId:this.state.configuration.id
      }}, null, response=>{
      if(response.code===200){
        this.setState({
          configuration:{}
        }, () => {
          this.fetchAll()
          this.closeDialog()
        })
      }
    }, true, true)
  }


  render() {
    return (
        <Page
            title="Application Configuration"
            breadcrumbs={[{ name: 'Application Configuration', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this Application Setting item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalPublishConfirm}
              title="Publish Confirmation"
              message="Do you want to Publish this Application Setting item from your list ?"
              okCallback={this.activate}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Application Detail Setting</CardHeader>
                <CardBody >
                  <Row>
                    <Col>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Company Name</th>
                                <th>Min Withdrawal</th>
                                <th>Page Row</th>
                                <th>Active</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.configurations.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td>{item.companyName}</td>
                                      <td>{currencyFormat(item.minWithdrawal, CURRENCY_SYMBOL)}</td>
                                      <td>{item.pageRowSize}</td>
                                      <td>
                                        {
                                          item.active?
                                              (<Button color="primary"><MdPublish/>&nbsp;Active</Button>)
                                              :
                                              (<Button color="danger" onClick={e=>{
                                                e.preventDefault()
                                                this.setState({
                                                  configuration:cloneObject(item),
                                                  modalPublishConfirm:true,
                                                })
                                              }}><MdPublish/>&nbsp;Activate</Button>)
                                        }
                                      </td>
                                      <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="secondary" onClick={e=>(
                                            this.setState({
                                              configuration:cloneObject(item),
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      <Card body>
                        <Row>
                          <Col md={12}>
                            <Card>
                              <CardBody>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="name">Name</Label>
                                      <Input
                                          type="text"
                                          name="name"
                                          value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.name)?this.state.configuration.name:""}
                                          onChange={(e) =>{
                                            let ctg = this.state.configuration
                                            if(ctg!=null){
                                              ctg.name = e.target.value
                                              this.setState({
                                                configuration:ctg
                                              })
                                            }
                                          }}
                                          placeholder="Fill a setting name"
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="companyName">Company Name</Label>
                                      <Input
                                          type="text"
                                          name="companyName"
                                          value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.companyName)?this.state.configuration.companyName:""}
                                          onChange={(e) =>{
                                            let configuration = this.state.configuration
                                            if(configuration!=null){
                                              configuration.companyName = e.target.value
                                              this.setState({
                                                configuration:configuration
                                              })
                                            }
                                          }}
                                          placeholder="Enter Company name"
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="companyDescription">Company Description</Label>
                                      <Input
                                          type="textarea"
                                          name="companyDescription"
                                          value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.companyDescription)?this.state.configuration.companyDescription:""}
                                          onChange={(e) =>{
                                            let configuration = this.state.configuration
                                            if(configuration!=null){
                                              configuration.companyDescription = e.target.value
                                              this.setState({
                                                configuration:configuration
                                              })
                                            }
                                          }}
                                          placeholder="Enter Company Description"
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="companyPhoneNumber">Company Phone Number</Label>
                                      <Input
                                          name="companyPhoneNumber"
                                          className="form-control"
                                          type="text"
                                          value={!isEmpty(this.state.configuration)?this.state.configuration.companyPhoneNumber:""}
                                          onChange={(e) =>{
                                            let configuration = this.state.configuration
                                            if(configuration!=null){
                                              configuration.companyPhoneNumber = e.target.value.replace(/\D/,'')
                                              this.setState({
                                                configuration:configuration
                                              })
                                            }
                                          }}
                                          placeholder="0"
                                          maxLength={15}>
                                      </Input>
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="contactPersonPhoneNumber">Contact Person Phone Number</Label>
                                      <Input
                                          name="contactPersonPhoneNumber"
                                          className="form-control"
                                          type="text"
                                          value={!isEmpty(this.state.configuration)?this.state.configuration.contactPersonPhoneNumber:""}
                                          onChange={(e) =>{
                                            let configuration = this.state.configuration
                                            if(configuration!=null){
                                              configuration.contactPersonPhoneNumber = e.target.value.replace(/\D/,'')
                                              this.setState({
                                                configuration:configuration
                                              })
                                            }
                                          }}
                                          placeholder="0"
                                          maxLength={15}>
                                      </Input>
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="contactWhatsapp">Contact Whatsapp</Label>
                                      <Input
                                          name="contactWhatsapp"
                                          className="form-control"
                                          type="text"
                                          value={!isEmpty(this.state.configuration)?this.state.configuration.contactPersonPhoneNumber:""}
                                          onChange={(e) =>{
                                            let configuration = this.state.configuration
                                            if(configuration!=null){
                                              configuration.contactWhatsapp = e.target.value.replace(/\D/,'')
                                              this.setState({
                                                configuration:configuration
                                              })
                                            }
                                          }}
                                          placeholder="0"
                                          maxLength={15}>
                                      </Input>
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="pageRowSize">Page Row Size</Label>
                                      <NumberInput
                                          name="pageRowSize"
                                          className="form-control"
                                          value={!isEmpty(this.state.configuration)?this.state.configuration.pageRowSize:""}
                                          onChangeEvent={(e, maskedvalue, floatvalue) => {
                                            e.preventDefault()
                                            let configuration = this.state.configuration
                                            configuration.pageRowSize = floatvalue
                                            this.setState({
                                              configuration: configuration
                                            })
                                          }}
                                          placeholder="0"
                                          maxLength={2}>
                                      </NumberInput>
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="minWithdrawal">Minimum Withdrawal</Label>
                                      <NumberInput
                                          name="minWithdrawal"
                                          className="form-control"
                                          value={!isEmpty(this.state.configuration)?this.state.configuration.minWithdrawal:""}
                                          onChangeEvent={(e, maskedvalue, floatvalue) => {
                                            e.preventDefault()
                                            let configuration = this.state.configuration
                                            configuration.minWithdrawal = floatvalue
                                            this.setState({
                                              configuration: configuration
                                            })
                                          }}
                                          placeholder="0"
                                          maxLength={20}>
                                      </NumberInput>
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                  </Col>
                                </Row>
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                        <br/>
                        <Card>
                          <CardHeader>
                            Social Media
                          </CardHeader>
                          <CardBody>
                            <Row>
                              <Col>
                                <FormGroup>
                                  <Label for="hashtag">#hashtag</Label>
                                  <Input
                                      type="text"
                                      name="hashtag"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.hashtag)?this.state.configuration.hashtag:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.hashtag = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter #hastag"
                                  />
                                </FormGroup>
                              </Col>
                              <Col>
                                <FormGroup>
                                  <Label for="instagramLink">Instagram Link</Label>
                                  <Input
                                      type="text"
                                      name="instagramLink"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.instagramLink)?this.state.configuration.instagramLink:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.instagramLink = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter Instagram Link"
                                  />
                                </FormGroup>
                              </Col>
                            </Row>
                            <Row>
                              <Col>
                                <FormGroup>
                                  <Label for="facebookLink">Facebook Link</Label>
                                  <Input
                                      type="text"
                                      name="facebookLink"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.facebookLink)?this.state.configuration.facebookLink:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.facebookLink = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter Facebook Link"
                                  />
                                </FormGroup>
                              </Col>
                              <Col>
                                <FormGroup>
                                  <Label for="twitterLink">Twitter Link</Label>
                                  <Input
                                      type="text"
                                      name="facebookLink"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.twitterLink)?this.state.configuration.twitterLink:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.twitterLink = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter Twitter Link"
                                  />
                                </FormGroup>
                              </Col>

                            </Row>
                          </CardBody>
                        </Card>
                        <br/>
                        <Card>
                          <CardHeader>
                            Keys
                          </CardHeader>
                          <CardBody>
                            <Row>
                              <Col>
                                <FormGroup>
                                  <Label for="facebookAppId">Facebook Application Id</Label>
                                  <Input
                                      type="text"
                                      name="facebookAppId"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.facebookAppId)?this.state.configuration.facebookAppId:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.facebookAppId = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter facebook app id"
                                  />
                                </FormGroup>
                              </Col>
                              <Col>
                                <FormGroup>
                                  <Label for="googleClientId">Google Client Id</Label>
                                  <Input
                                      type="text"
                                      name="googleClientId"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.googleClientId)?this.state.configuration.googleClientId:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.googleClientId = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter google client id"
                                  />
                                </FormGroup>
                              </Col>
                            </Row>
                            <Row>
                              <Col>
                                <FormGroup>
                                  <Label for="freshChatToken">Fresh Chat Token</Label>
                                  <Input
                                      type="text"
                                      name="freshChatToken"
                                      value={!isEmpty(this.state.configuration)&& !isEmpty(this.state.configuration.freshChatToken)?this.state.configuration.freshChatToken:""}
                                      onChange={(e) =>{
                                        let configuration = this.state.configuration
                                        if(configuration!=null){
                                          configuration.freshChatToken = e.target.value
                                          this.setState({
                                            configuration:configuration
                                          })
                                        }
                                      }}
                                      placeholder="Enter Fresh Chat Token"
                                  />
                                </FormGroup>
                              </Col>
                              <Col>
                              </Col>
                            </Row>
                          </CardBody>
                        </Card>
                        <br/>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    configuration:{}
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.configuration!=null && this.state.configuration.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
