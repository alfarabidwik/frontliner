import BasePage from "./BasePage";
import React from 'react'
import queryString from "query-string";
import Page from "../components/Page";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import {CardBody, CardHeader, CardImg, FormGroup, Input, Label} from "reactstrap";
import {
    allIsEmpty,
    currencyFormat,
    imageSelector,
    isEmpty,
    parseDate,
    statusColor,
    weightFormat,
    navigateBack
} from "../utils/Utilities";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS, PRINT_DATA} from "../utils/Global";
import userProfilePicture from 'assets/img/users/user_pp.png';
import {IoMdOpen} from "react-icons/io";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Table from "react-bootstrap/Table";
import CardFooter from "reactstrap/es/CardFooter";
import Button from "reactstrap/es/Button";
import {IoMdEye, IoMdStats} from "react-icons/io/index";
import {MdPictureAsPdf, MdPrint, MdWarning} from "react-icons/md/index";
import UpdateTransactionStatusModal from '../components/modal/UpdateTransactionStatusModal'
import VoucherInfoModal from '../components/modal/VoucherInfoModal'
import SockJsClient from 'react-stomp'
import PrintInvoiceButton from '../components/Widget/PrintInvoiceButton'
import {Link} from "react-router-dom";
import {storeData} from "../utils/StorageUtil";

export default class TransactionDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let transactionIdQuery = queryString.parse(this.props.query).id
        let state = this.state
        state.transactionIdQuery = transactionIdQuery
        state.transaction = {}
        state.customer = {}
        state.updateStatusModal = false
        state.voucherInfoModal = false

        if(isEmpty(transactionIdQuery)){
            this.props.history.goBack();
        }

        this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchTransacationDetail()
    }

    fetchTransacationDetail =() =>{
        this.get(Global.API.TRANSACTION, {
                params :{
                    id : this.state.transactionIdQuery
                }
        }, null, response =>{
            if(response.code===200){
                this.setState({
                    transaction : response.data,
                    customer:response.data.customer
                })
            }
        }, true, true)
    }

    render() {
        let customer = this.state.customer;
        customer = customer?customer:{}

        let transaction = this.state.transaction?this.state.transaction:{}

        let transactionStatus = transaction.transactionStatus?transaction.transactionStatus:{}

        let village = customer.village?customer.village:{}
        let district = village.district?village.district:{}
        let city = district.city?district.city:{}
        let province = city.province?city.province:{}
        let courierCost = transaction.cost?transaction.cost:{}
        let courierFee = transaction.courierFee?transaction.courierFee:0
        let voucher = transaction.voucher?transaction.voucher:{}

        let updateStatusModal = this.state.updateStatusModal
        let voucherInfoModal = this.state.voucherInfoModal
        let customerAddress = transaction.customerAddress?transaction.customerAddress:{}

        return (
            <Page
                title="Transaction Detail"
                breadcrumbs={[{ name: 'transaction detail', active: true }]}>
                {super.render()}
                <UpdateTransactionStatusModal modal={updateStatusModal} transaction={transaction} okCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    }, () => {
                        this.fetchTransacationDetail()
                    })
                }}
                closeCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    })
                }}/>
                <VoucherInfoModal modal={voucherInfoModal} voucher={transaction.voucher} closeCallback={()=>{
                    this.setState({
                        voucherInfoModal:false
                    })
                }}/>
                <Row key={1}>
                    <Col>
                        <Card body>
                            <Row>
                                <Col md={3}>
                                    <Card>
                                        <CardHeader>Customer</CardHeader>
                                        <CardImg top src={
                                            customer.image!=null?customer.imageLink:userProfilePicture}
                                                 onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                        <CardBody>
                                            <Row>
                                                <Col md={4}>
                                                    Name
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(customer.id)?"-":customer.firstname+" "+this.state.customer.lastname}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Phone
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(customer.id)?"-":customer.phoneNumber}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Email
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(customer.id)?"-":customer.email}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Address
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(village.id)?"-":
                                                        customer.address+", "+
                                                        village.name+", "+
                                                        district.name+", "+
                                                        city.name+", "+
                                                        province.name+". "+
                                                        village.postalCode+" "
                                                    }
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <div  className="float-right">
                                                        <span
                                                            color="blue"
                                                            onClick={e=>{this.props.history.push('/customerDetail?id='+customer.id)}}
                                                            style={{cursor:'pointer'}}>
                                                            Detail
                                                        </span>
                                                        &nbsp;&nbsp;
                                                        <IoMdOpen
                                                           color="blue" style={{cursor:'pointer'}}
                                                           onClick={e=>{
                                                               this.props.history.push('/customerDetail?id='+customer.id)
                                                           }}/>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                    <br/>
                                    {
                                        transaction.id&&(
                                            <span>
                                                <Row>
                                                    <Col>
                                                        <Button style={{fontWeight:'bold', width: '100%', background: statusColor(transactionStatus.id)}}>Current Status : {transactionStatus.name}</Button>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <Button style={{width: '100%'}} color="danger" onClick={event=>{
                                                            event.preventDefault()
                                                            this.setState({
                                                                updateStatusModal:true
                                                            })
                                                            }}>Update Status Manually &nbsp;<MdWarning/></Button>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <Button color="green" style={{fontWeight:'bold', width: '100%'}} onClick={event=>{
                                                            event.preventDefault()
                                                            storeData(PRINT_DATA, transaction)
                                                            let printPageWindow = window.open('/printInvoice', 'tab')
                                                            if(printPageWindow){
                                                                printPageWindow.addEventListener('DOMContentLoaded', (event) => {
                                                                    setInterval(()=>{
                                                                        printPageWindow.postMessage("PRINT", "*")
                                                                    }, 300)
                                                                });
                                                            }
                                                        }}>PRINT&nbsp;<MdPrint/></Button>
                                                    </Col>
                                                </Row>
                                            </span>

                                        )
                                    }
                                </Col>
                                <Col md={9}>
                                    <Card>
                                        <CardHeader>Transaction Info</CardHeader>
                                        <CardBody>
                                            <Row>
                                                <Col md={4}>
                                                    <Card>
                                                        <CardHeader>
                                                            Courier
                                                        </CardHeader>
                                                        <CardBody>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="resiCode">Resi Code</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="resiCode"
                                                                            readOnly={true}
                                                                            value={transaction.resiCode?transaction.resiCode:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="courierName">Name</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="courierName"
                                                                            readOnly={true}
                                                                            value={transaction.courierName?transaction.courierName:"-"}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="courierCode">Code</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="courierCode"
                                                                            readOnly={true}
                                                                            value={transaction.courierCode?transaction.courierCode:"-"}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="service">Service</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="service"
                                                                            readOnly={true}
                                                                            value={courierCost.service?courierCost.service:"-"}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="weight">Weight</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="weight"
                                                                            readOnly={true}
                                                                            value={weightFormat(transaction.totalWeight)}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="courierFee">Courier Cost</Label>
                                                                        <Input
                                                                            style={{fontWeight:'bold', color:'#e95261'}}
                                                                            type="text"
                                                                            name="courierFee"
                                                                            readOnly={true}
                                                                            value={isEmpty(courierFee)?"0":currencyFormat(courierFee, CURRENCY_SYMBOL)}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                                <Col md={4}>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="refCode">Ref Code</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="refCode"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.id)?"-":transaction.refCode}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="created">Created</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="created"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.id)?"-":parseDate(transaction.created, 'dd-MM-yyyy hh:mm:ss')}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="updated">Last Update</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="updated"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.id)?"-":parseDate(transaction.updated, 'dd-MM-yyyy hh:mm:ss')}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    {
                                                        voucher.id&&(
                                                            <Row>
                                                                <Col>
                                                                    <Card>
                                                                        <CardHeader>
                                                                            Voucher
                                                                        </CardHeader>
                                                                        <CardBody>
                                                                            <Row>
                                                                                <Col>
                                                                                    <FormGroup>
                                                                                        <Label for="benefitType">Benefit</Label>
                                                                                        <Input
                                                                                            type="text"
                                                                                            name="courierName"
                                                                                            readOnly={true}
                                                                                            value={voucher.benefitType}
                                                                                        />
                                                                                    </FormGroup>
                                                                                </Col>
                                                                            </Row>
                                                                            <Row>
                                                                                <Col>
                                                                                    <FormGroup>
                                                                                        <Label for="code">Code</Label>
                                                                                        <Input
                                                                                            type="text"
                                                                                            name="code"
                                                                                            readOnly={true}
                                                                                            value={voucher.code}
                                                                                        />
                                                                                    </FormGroup>
                                                                                </Col>
                                                                            </Row>
                                                                            <Row>
                                                                                <Col>
                                                                                    <FormGroup>
                                                                                        <Label for="cutOff">Cut Off</Label>
                                                                                        <Input
                                                                                            style={{fontWeight:'bold', color:'#54bc00'}}
                                                                                            type="text"
                                                                                            name="cutOff"
                                                                                            reaOnly={true}
                                                                                            value={
                                                                                                voucher.discountType===Global._PERCENTAGE_DISCOUNT?voucher.percent+" %":currencyFormat(voucher.nominal, "Rp ")
                                                                                            }
                                                                                        />
                                                                                    </FormGroup>
                                                                                </Col>
                                                                            </Row>
                                                                            <Row>
                                                                                <Col>
                                                                                    <Button color="primary" className="float-right" onClick={event=>{
                                                                                        this.setState({
                                                                                            voucherInfoModal:true
                                                                                        })
                                                                                    }}>See Detail <IoMdEye/></Button>
                                                                                </Col>
                                                                            </Row>

                                                                        </CardBody>
                                                                    </Card>
                                                                </Col>
                                                            </Row>
                                                        )
                                                    }
                                                </Col>
                                                <Col md={4}>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="transactionStatus">Transaction Status</Label>
                                                                <Input
                                                                    style={{fontWeight:'bold', color:statusColor(transactionStatus.id), borderColor:statusColor(transactionStatus.id)}}
                                                                    type="text"
                                                                    name="transactionStatus"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.transactionStatus)?"-":transaction.transactionStatus.name}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="totalItemPrice">Item Price</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="totalItemPrice"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.totalItemPrice)?"0":currencyFormat(transaction.totalItemPrice, CURRENCY_SYMBOL)}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="bubbleWrapFee">Bubble Wrap</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="bubbleWrapFee"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.bubbleWrapFee)?"0":currencyFormat(transaction.bubbleWrapFee, CURRENCY_SYMBOL)}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="uniquePrice">Unique Price</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="uniquePrice"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.uniquePrice)?"0":currencyFormat(transaction.uniquePrice, CURRENCY_SYMBOL)}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="totalPay">Total Pay</Label>
                                                                <Input
                                                                    style={{color:'#3dc296', fontWeight:'bold', fontSize:'18px'}}
                                                                    type="text"
                                                                    name="totalPay"
                                                                    readOnly={true}
                                                                    value={isEmpty(transaction.totalPay)?"-":currencyFormat(transaction.totalPay, CURRENCY_SYMBOL)}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardHeader>
                                                            Shipping Address
                                                        </CardHeader>
                                                        <CardBody>
                                                            <Row>
                                                                <Col md={6}>
                                                                    <FormGroup>
                                                                        <Label for="address">Address</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="address"
                                                                            readOnly={true}
                                                                            value={customerAddress.id?customerAddress.address:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col md={4}>
                                                                    <FormGroup>
                                                                        <Label for="village">Village</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="village"
                                                                            readOnly={true}
                                                                            value={customerAddress.village?customerAddress.village.name:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col md={2}>
                                                                    <FormGroup>
                                                                        <Label for="postalCode">Postal Code</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="postalCode"
                                                                            readOnly={true}
                                                                            value={customerAddress.village?customerAddress.village.postalCode:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="district">District</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="district"
                                                                            readOnly={true}
                                                                            value={customerAddress.village?customerAddress.village.district.name:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="city">City</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="city"
                                                                            readOnly={true}
                                                                            value={customerAddress.village?customerAddress.village.district.city.name:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="province">Province</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="province"
                                                                            readOnly={true}
                                                                            value={customerAddress.village?customerAddress.village.district.city.province.name:'-'}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardHeader>Status Log</CardHeader>
                                                        <CardBody>
                                                            <Tabs
                                                                activeKey={this.state.key}
                                                                onSelect={key =>{
                                                                    if(key===1){

                                                                    }else if(key===2){

                                                                    }
                                                                }}>
                                                                <Tab eventKey={1} title={"Payment"}>
                                                                    {this.renderPayment()}
                                                                </Tab>
                                                                <Tab eventKey={2} title={"Status"}>
                                                                    {this.renderTransactionStatus()}
                                                                </Tab>
                                                            </Tabs>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                <Row key={2}>
                    <Col>
                        <Card>
                            <CardHeader>Transaction Item</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col>
                                        <Table>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product</th>
                                                <th>Type</th>
                                                <th>Color</th>
                                                <th>Size/Weight</th>
                                                <th>Image</th>
                                                <th>Quantity</th>
                                                <th>Price@Item</th>
                                                <th>Total Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                isEmpty(this.state.transaction.transactionDetails)?
                                                    null
                                                    :
                                                    this.state.transaction.transactionDetails.map((item, index)=>(
                                                        <tr key={index}>
                                                            <th scope="row">{++index}</th>
                                                            <td>{item.inventory.product.name} &nbsp;&nbsp;<IoMdOpen color="#5f86dd" onClick={ e => (
                                                                this.props.history.push('/productDetail?id='+item.inventory.product.id)
                                                            )}/>
                                                            </td>
                                                            <td>{item.inventory.typeName}&nbsp;&nbsp;<IoMdOpen color="#5f86dd" onClick={ e => (
                                                                this.props.history.push('/inventoryDetail?productId='+item.inventory.product.id+"&inventoryId="+item.inventory.id)
                                                            )}/></td>
                                                            <td>
                                                                {
                                                                    item.inventory.colors.map((color, index1)=>(
                                                                        color!=null?
                                                                            <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                                            :
                                                                            null
                                                                    ))
                                                                }
                                                            </td>
                                                            <td>{item.inventory.size?item.inventory.size.name:"-"} / {weightFormat(item.inventory.weight)}</td>
                                                            <td width="5%">
                                                                <Button onClick={event=>{
                                                                    event.preventDefault()
                                                                    this.openLightBox(item.inventory.inventoryImages)
                                                                }}><IoMdEye/></Button>
                                                            </td>
                                                            <td>{item.quantity}</td>
                                                            <td>{currencyFormat(item.inventory.finalPrice, CURRENCY_SYMBOL)}</td>
                                                            <td>{currencyFormat(item.totalPrice, CURRENCY_SYMBOL)}</td>
                                                        </tr>

                                                    ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Col>
                                </Row>
                                <CardFooter style={{fontSize:'18px'}}>
                                    <br/>
                                    <Row>
                                        <Col>
                                        </Col>
                                        <Col>
                                            Total Quantity : {!isEmpty(transaction.quantity)?transaction.quantity:"0"}
                                        </Col>
                                        <Col>
                                            Total Weight : {!isEmpty(transaction.totalWeight)?weightFormat(transaction.totalWeight):"0 g"}
                                        </Col>
                                        <Col>
                                            <strong style={{color:'#e95261'}}>
                                                Total Price : {!isEmpty(transaction.totalItemPrice)?currencyFormat(transaction.totalItemPrice, CURRENCY_SYMBOL):"Rp. 0"}
                                            </strong>
                                        </Col>
                                        <Col>
                                            <strong style={{color:'#3dc296'}}>
                                                Total Pay : {!isEmpty(transaction.totalPay)?currencyFormat(transaction.totalPay, CURRENCY_SYMBOL):"Rp. 0"}
                                            </strong>
                                        </Col>
                                    </Row>
                                </CardFooter>
                            </CardBody>
                            <SockJsClient
                                url={Global.BASE_URL}
                                topics={[Global.API.LISTEN_TRANSACTION+"/"+transaction.id]}
                                onMessage={(message) => {
                                    this.successToast(message)
                                    this.fetchTransacationDetail()
                                }}
                                ref={ (client) => { this.clientRef = client }} />

                        </Card>
                    </Col>
                </Row>
            </Page>
        );
    }

    renderPayment = () =>{
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Payment Media</th>
                    <th>Image</th>
                    <th>Amount</th>
                    <th>Note</th>
                    <th>Source</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>
                {
                    isEmpty(this.state.transaction.payments)?
                        null
                        :
                        this.state.transaction.payments.map((item, index)=>(
                            <tr key={index}>
                                <th scope="row">{++index}</th>
                                <th>{item.paymentMedia?item.paymentMedia.name:"-"}</th>
                                <th>{item.paymentMedia?item.paymentMedia.imageLink:""}</th>
                                <td>{isEmpty(item.amount)?"-":currencyFormat(item.amount, CURRENCY_SYMBOL)}</td>
                                <td>{item.note}</td>
                                <td>{item.source}</td>
                                <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            </tr>

                        ))
                }
                </tbody>
            </Table>
        )
    }

    renderTransactionStatus = () =>{
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>Note</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>
                {
                    isEmpty(this.state.transaction.pvTransactionStatuses)?
                        null
                        :
                        this.state.transaction.pvTransactionStatuses.map((item, index)=>(
                            <tr key={index}>
                                <th scope="row">{++index}</th>
                                <td>{isEmpty(item.transactionStatus.name)?"-":item.transactionStatus.name}</td>
                                <td>{isEmpty(item.note)?"-":item.note}</td>
                                <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            </tr>

                        ))
                }
                </tbody>
            </Table>
        )
    }


}