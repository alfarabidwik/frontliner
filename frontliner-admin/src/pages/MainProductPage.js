import Page from 'components/Page';
import React from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {
  BANNER_IMAGE_MAX_FILE_SIZE,
  DD_MM_YYYY,
  DD_MM_YYYY_HH_MM_SS,
  MAIN_PRODUCT_MAX_FILE_SIZE, MIME_JPEG
} from "../utils/Global";
import {allIsEmpty, imageSelector, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import shoppingBag from 'assets/img/products/shopping-bag.png';
import axios from "axios";
import Img from 'react-image'
import WarningLabel from "../components/Widget/WarningLabel";

import 'react-datepicker/dist/react-datepicker.css';

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class MainProductPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      mainProducts : [],
      mainProduct:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      image:null,
      sortir:"created",
      ascending:true,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir)
  }

  confirmSave = () =>{
    var mainProduct = this.state.mainProduct
    var image = this.state.image
    if(allIsEmpty(image, mainProduct.name, mainProduct.pagelink, mainProduct.sortir)
        || mainProduct.sortir === 0){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  saveUpload = () => {
    this.showProgress()
    axios({
      url: this.state.image,
      method: 'GET',
      responseType: 'blob', // important
    }).then((response) => {
      this.closeProgress()
      var file = response.data ;
      var formData = new FormData();
      formData.append("mainProductDtoGson", JSON.stringify(this.state.mainProduct))
      formData.append("multipartFile", file);
      this.setState({
        modalAddConfirm:false
      }, () => {
        this.post(Global.API.MAIN_PRODUCT_SAVE_UPLOAD, formData, null, res=>{
          this.setState({
            mainProduct:res.data,
            image:res.data.imageLink
          }, () => {
            this.showDialog("Success", res.message)
            this.fetchAll(this.state.ascending, this.state.sortir)
          })
        },  true, true)
      })
    })
  }


  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.MAIN_PRODUCT_DELETE+"/"+this.state.mainProduct.id, null, null, res => {
        if(res.code===200){
          this.setState({
            mainProduct:null
          }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir)
          })
        }
      });
    }, true, true)
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = (ascending, sortir) => {
    this.get(Global.API.MAIN_PRODUCTS,{
      params:{
        ascending:ascending,
        sortir:sortir,
      }
    }, null, res =>{
      if(res.code === 200){
        this.setState({
          mainProducts : res.data
        })
      }
    }, true, true);
  }

  render() {
    return (
        <Page
            title="Main Product"
            breadcrumbs={[{ name: 'mainProduct', active: true }]}
            className="TablePage">
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this mainProduct item to your list ?"
              okCallback={this.saveUpload}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this mainProduct item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          {super.render()}
          <Row key={1}>
            <Col md={3}>
              <Card>
                <CardBody>
                  <Row>
                    <CardImg top style={{width:'100%', height:'100%'}} src={
                      this.state.image!=null?this.state.image:shoppingBag}
                      onClick={e=>{
                        if(this.state.image!=null){
                          this.setState({
                            imageLink:this.state.image,
                            openPreview:!this.state.openPreview
                          })
                        }
                      }}
                    />
                  </Row>
                  <Row>
                    <Col>
                      <WarningLabel message={"*Use Jpeg Format / Max "+MAIN_PRODUCT_MAX_FILE_SIZE+" Kb"}/>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Row>
                    <Col>
                      <ButtonGroup className="float-left">
                        <Button color="primary" onClick={e=>(
                            imageSelector(file =>{
                              this.setState({
                                image:file
                              })
                            }, MIME_JPEG, MAIN_PRODUCT_MAX_FILE_SIZE).click()
                        )}>Upload</Button>
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col>
                      <FormGroup>
                        <Label for="name">Name</Label>
                        <Input
                            type="text"
                            name="name"
                            value={(this.state.mainProduct!=null && this.state.mainProduct.name!=undefined)?this.state.mainProduct.name:""}
                            onChange={(e) =>{
                              let mainProduct = this.state.mainProduct
                              if(mainProduct!=null){
                                mainProduct.name = e.target.value
                                this.setState({
                                  mainProduct:mainProduct
                                })
                              }
                            }}
                            placeholder="Enter Name"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="sortir">Sortir</Label>
                        <Input
                            type="number"
                            name="sortir"
                            value={(this.state.mainProduct!=null && this.state.mainProduct.sortir!=undefined)?this.state.mainProduct.sortir:""}
                            onChange={(e) =>{
                              let mainProduct = this.state.mainProduct
                              if(mainProduct!=null){
                                mainProduct.sortir = e.target.value
                                this.setState({
                                  mainProduct:mainProduct
                                })
                              }
                            }}
                            placeholder="Enter sortir"
                        />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <ActiveOption
                          callback={(active)=>{
                            let mainProduct = this.state.mainProduct
                            if(mainProduct!=null){
                              mainProduct.active = active
                              this.setState({
                                mainProduct:mainProduct
                              })
                            }

                          }}
                          default={this.state.mainProduct?this.state.mainProduct.active:false}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={10}>
                      <FormGroup>
                        <Label for="pagelink">Pagelink</Label>
                        <Input
                            type="link"
                            name="pagelink"
                            value={(this.state.mainProduct!=null)?this.state.mainProduct.pagelink:""}
                            onChange={(e) =>{
                              let mainProduct = this.state.mainProduct
                              if(mainProduct!=null){
                                mainProduct.pagelink = e.target.value
                                this.setState({
                                  mainProduct:mainProduct
                                })
                              }
                            }}
                            placeholder="Enter pagelink"
                        />
                      </FormGroup>
                    </Col>
                    <Col md={2}>
                      <FormGroup>
                        <Label for="pagelink">Go Pagelink</Label>
                        <br/>
                        <Badge href={this.state.mainProduct.pagelink} color="primary" className="form-text mr-1" target="_blank">
                          Go site
                        </Badge>
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <Row>
                    <Col>
                      <ButtonGroup className="float-left">
                        <Button color="danger" onClick={e => (
                            this.setState({
                              mainProduct:{},
                              image:null
                            })
                        )}> Cancel </Button>
                      </ButtonGroup>
                    </Col>
                    <Col>
                      <ButtonGroup className="float-right">
                        <Button color="primary" onClick={e=>(
                            this.confirmSave()
                        )}> {
                          (this.state.mainProduct!=null && this.state.mainProduct.id!=null)?"Update":"Add"
                        } </Button>
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card body>
                <Table hover>
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Sortir</th>
                    <th>Pagelink</th>
                    <th>Status</th>
                    <th>Image</th>
                    <th>Updated</th>
                    <th>Edit</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.mainProducts.map((item, index) =>(
                        <tr key={index}>
                          <th scope="row">{++index}</th>
                          <td>{item.name}</td>
                          <td>{item.sortir}</td>
                          <td><a href={item.pagelink} target="_blank">Go site</a></td>
                          <td>{translate(item.active)}</td>
                          <td width="10%">
                            <Img
                                className="img-thumbnail"
                                src={item.imageLink}
                                onClick={e=>{
                                  this.setState({
                                    imageLink:item.imageLink,
                                    openPreview:true
                                  })
                                }}
                            />
                          </td>
                          <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                          <td>
                            <Button size="sm" color="danger" onClick={e => {
                              e.preventDefault()
                              this.setState({
                                mainProduct:item,
                                image:item.imageLink
                              }, () => {
                                this.confirmDelete()
                              })
                            }}>
                              <MdDelete/>
                            </Button>
                            &nbsp;
                            <Button size="sm" color="primary" onClick={ e => (
                                this.setState({
                                  mainProduct:item,
                                  image:item.imageLink
                                })
                            )}>
                              <MdEdit/>
                            </Button>
                          </td>
                        </tr>
                    ))
                  }
                  </tbody>
                </Table>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
