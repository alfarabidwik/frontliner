import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardTitle,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Input,
    Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import shoppingBag from 'assets/img/products/shopping-bag.png';
import Page from "../components/Page";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import Global, {
    CURRENCY_SYMBOL,
    DD_MM_YYYY_HH_MM_SS,
    MIME_PNG_JPEG,
    PARTNER_IMAGE_MAX_FILE_SIZE,
    PARTNER_TYPES
} from "../utils/Global";
import {
    allIsEmpty,
    currencyFormat,
    deleteParam,
    imageSelector,
    parseDate,
    sortirMap,
    weightFormat
} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit} from "react-icons/md";
import {IoMdEye} from "react-icons/io";
import InventoryQuantityModal from '../components/modal/InventoryQuantityModal'
import {getData} from "../utils/StorageUtil";

import ImageCropper from "../components/modal/ImageCropper";
import SearchInput from '../components/SearchInput'
import ActiveLabel from "../components/Widget/ActiveLabel";
import Pagination from '../components/Pagination'

import {ContentState, convertToRaw, EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import WarningLabel from "../components/Widget/WarningLabel";

import TagsInput from 'react-tagsinput'
import DateInput from "../components/Widget/DateInput";
import NumberInput from "../components/Widget/NumberInput";


export default class PartnerDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let state = this.state
        state.partner = {}
        state.partnerType = {}
        state.company = null
        state.personal = null
        state.categories = []
        state.image  = null
        state.provinces = []
        state.cities = []
        state.districts = []
        state.villages = []
        state.province = {}
        state.city= {}
        state.district = {}
        state.village = {}
        state.partnerIdQuery = queryString.parse(this.props.query).id
        state.category = {}
        state.modalAddConfirm = false
        state.modalDeleteConfirm = false
        state.inventories = []
        state.inventory = null
        state.ascending = true
        state.sortir = 'created'
        state.quantityModal = false
        state.page = queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
        state.totalPage = 0
        state.totalElement = 0
        state.pageElement = 0
        this.setState(state)
        this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))

    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchPartnerDetail(this.state.partnerIdQuery)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                // this.fetchAllInventories(this.state.partner.id, this.state.ascending, this.state.sortir, true)
            })
        }

    }


    confirmSave = () =>{
        var partner = this.state.partner
        var image = this.state.image
        let company = this.state.company
        let personal = this.state.personal

        if(allIsEmpty(partner.partnerType, partner.fullName)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            if(partner.partnerType===PARTNER_TYPES[0].name){
                partner.company = null ;
                partner.personal = personal
            }
            if(partner.partnerType===PARTNER_TYPES[1].name){
                partner.company = company ;
                partner.personal = null
            }
            this.setState({
                modalAddConfirm:true
            })
        }
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.inventory!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.INVENTORY_DELETE+"/"+this.state.inventory.id, null, null, res => {
                    if(res.code===200){
                        this.setState({
                            inventory:null
                        }, () => {
                            // this.fetchAllInventories(this.state.partner.id, this.state.ascending, this.state.sortir, true)
                        })
                    }
                }, true, true);
            })
        }
    }

    saveUpload = () => {
        var formData = new FormData();
        let partner = this.state.partner;

        formData.append("partnerDtoGson", JSON.stringify(partner))
        formData.append("multipartFile", this.state.imageBlob);
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.PARTNER_SAVE_UPLOAD, formData, null, res=>{
                if(res.code===200){
                    this.setState({
                        partner:res.data
                    }, () => {
                        this.showDialog("Success", res.message)
                        this.fetchPartnerDetail(this.state.partner.id)
                        // changeParam(this.props, 'id', this.state.partner.id)
                    })
                }
            },  true, true)
        })
    }


    fetchPartnerDetail = (id) => {
        if(id!=null){
            this.get(Global.API.PARTNER, {
                params :{
                    id : id
                }
            }, null, response=>{
                if(response.code===200){
                    let partner = response.data
                    let company = partner.company
                    let personal = partner.personal
                    let village = company&&company.village?company.village:null
                    let district = company&&company.village?company.village.district:null
                    let city = company&& company.village?company.village.district.city:null
                    let province = company&&company.village?company.village.district.city.province:null
                    let partnerType = null ;
                    if(partner&& partner.partnerType){
                        PARTNER_TYPES.forEach(value => {
                            if(partner.partnerType===value.name){
                                partnerType = value ;
                            }
                        })
                    }
                    this.setState({
                        partner:partner,
                        partnerType:partnerType,
                        company:company,
                        personal:personal,
                        taskCategory:response.data.taskCategory,
                        image:response.data.imageUrl,
                        village:village,
                        district:district,
                        city:city,
                        province:province,
                    }, () => {
                        if(this.state.village){
                            this.fetchCities(this.state.province.id, cities=>{
                                this.setState({
                                    cities:cities
                                })
                            })
                        }
                        if(this.state.city){
                            this.fetchDistricts(this.state.city.id, districts =>{
                                this.setState({
                                    districts:districts
                                })
                            })
                        }
                        if(this.state.district){
                            this.fetchVillages(this.state.district.id, villages=>{
                                this.setState({
                                    villages:villages
                                })
                            })
                        }
                        // this.fetchAllInventories(this.state.partner.id, this.state.ascending, this.state.sortir, true)
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }


    fetchAllInventories = (partnerId, ascending, sortir, progressing) =>{
        this.get(Global.API.INVENTORIES, {
            params:{
                partnerId:partnerId,
                ascending:ascending,
                sortir:sortir,
                page:this.state.page-1,
                search:this.state.search
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    inventories : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }

        }, progressing, true)
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
        })
    }

    refreshInventory = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            // this.fetchAllInventories(this.state.partner.id, this.state.ascending, this.state.sortir, progressing)
        })
    )

    openQuantityModal=()=>{
        this.setState({
            quantityModal:true
        })
    }
    closeQuantityModal=()=>{
        this.setState({
            quantityModal:false
        })
    }

    updateQuantity=(item, quantity, note, substraction)=>{
        let form = new FormData()
        form.append("inventoryId", item.id)
        form.append("newStock", quantity)
        form.append("note", note)
        if(substraction){
            form.append("substraction", true)
        }else{
            form.append("substraction", false)
        }
        this.post(Global.API.INVENTORY_UPDATE_STOCK, form, null, response=>{
            if(response.code===200){
                // this.fetchAllInventories(this.state.partner.id, this.state.ascending, this.state.sortir, true)
                this.closeQuantityModal()
            }
        }, true, true)
    }


    render() {
        var ids = [];
        var labels = [];

        let configuration = getData(Global.CONFIGURATION)
        let aspect = 4/4
        if(configuration.partnerImageDimensionWidth && configuration.partnerImageDimensionHeight){
            aspect = configuration.partnerImageDimensionWidth/configuration.partnerImageDimensionHeight
        }


        let partner = this.state.partner?this.state.partner:{}
        let company = this.state.company?this.state.company:{}
        let personal = this.state.personal?this.state.personal:{}



        return (
            <Page
                title="Partner Form"
                breadcrumbs={[{ name: 'partner form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this partner item to your list ?"
                    okCallback={this.saveUpload}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this partner item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>

                <InventoryQuantityModal modal={this.state.quantityModal} inventory={this.state.inventory} okCallback={this.updateQuantity} closeCallback={this.closeQuantityModal}/>
                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Partner</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={3}>
                                        <Card>
                                            <CardImg top src={
                                                this.state.image!=null?this.state.image:shoppingBag} onClick={e=>{
                                                imageSelector(file=>{
                                                    this.setState({
                                                        image:file,
                                                        cropperModal:true
                                                    })
                                                }, MIME_PNG_JPEG, PARTNER_IMAGE_MAX_FILE_SIZE).click()
                                            }}
                                            onError={(elm)=>this.defaultImage(elm, shoppingBag)}/>
                                            <CardBody>
                                                <CardTitle>Partner Image</CardTitle>
                                                <Row>
                                                    <Col>
                                                        <WarningLabel message={"*Use PNG or Jpeg Format / Max "+PARTNER_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                                                    </Col>
                                                </Row>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    partner:null,
                                                                    image:null
                                                                })
                                                            )}>Cancel</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                imageSelector(file=>{
                                                                    this.setState({
                                                                        image:file,
                                                                        cropperModal:true
                                                                    })
                                                                }, MIME_PNG_JPEG, PARTNER_IMAGE_MAX_FILE_SIZE).click()
                                                            }}>Upload</Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={9}>
                                        <Card>
                                            <CardBody>
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="name">Fullname</Label>
                                                            <Input
                                                                type="text"
                                                                name="name"
                                                                value={(partner)?partner.fullName:""}
                                                                onChange={(e) =>{
                                                                    e.preventDefault()
                                                                    let partner = this.state.partner
                                                                    if(partner!=null){
                                                                        partner.fullName = e.target.value
                                                                        this.setState({
                                                                            partner:partner
                                                                        })
                                                                    }
                                                                }}
                                                                placeholder="Enter fullname"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="name">Website</Label>
                                                            <Input
                                                                type="text"
                                                                name="name"
                                                                value={(partner)?partner.website:""}
                                                                onChange={(e) =>{
                                                                    e.preventDefault()
                                                                    if(partner!=null){
                                                                        partner.website = e.target.value
                                                                        this.setState({
                                                                            partner:partner
                                                                        })
                                                                    }
                                                                }}
                                                                placeholder="Enter website"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="name">Email</Label>
                                                            <Input
                                                                type="text"
                                                                name="name"
                                                                value={(partner)?partner.email:""}
                                                                onChange={(e) =>{
                                                                    e.preventDefault()
                                                                    let itm = this.state.partner
                                                                    if(itm!=null){
                                                                        itm.email = e.target.value
                                                                        this.setState({
                                                                            partner:itm
                                                                        })
                                                                    }
                                                                }}
                                                                placeholder="Enter email"
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <FormGroup>
                                                            <ItemOption
                                                                title={"Select Type"}
                                                                objects={PARTNER_TYPES}
                                                                default={this.state.partnerType?this.state.partnerType.id:null}
                                                                callback={(partnerType)=>{
                                                                    let partner = this.state.partner
                                                                    let company = null ;
                                                                    let personal = null ;
                                                                    if(partnerType.name===PARTNER_TYPES[0].name){
                                                                        if(partner.personal){
                                                                            personal = partner.personal
                                                                        }else{
                                                                            personal = {}
                                                                        }
                                                                    }
                                                                    if(partnerType.name===PARTNER_TYPES[1].name){
                                                                        if(partner.company){
                                                                            company = partner.company
                                                                        }else{
                                                                            company = {}
                                                                        }
                                                                    }
                                                                    if(partner!=null){
                                                                        partner.partnerType = partnerType.name
                                                                        this.setState({
                                                                            partner:partner,
                                                                            company:company,
                                                                            personal:personal,
                                                                            partnerType:partnerType,
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                {
                                                    partner.partnerType===PARTNER_TYPES[1].name?(
                                                        <>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Company Phone Number</Label>
                                                                        <NumberInput
                                                                            name="officePhoneNumber"
                                                                            className="form-control"
                                                                            value={(company)?company.officePhoneNumber:""}
                                                                            onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                                                e.preventDefault()
                                                                                if(company!=null){
                                                                                    company.officePhoneNumber = e.target.value
                                                                                    this.setState({
                                                                                        company:company
                                                                                    })
                                                                                }
                                                                            }}
                                                                            maxLength="15"
                                                                            placeholder="0">
                                                                        </NumberInput>
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Company Faximile</Label>
                                                                        <NumberInput
                                                                            name="officeFax"
                                                                            className="form-control"
                                                                            value={(company)?company.officeFax:""}
                                                                            onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                                                e.preventDefault()
                                                                                if(company!=null){
                                                                                    company.officeFax = e.target.value
                                                                                    this.setState({
                                                                                        company:company
                                                                                    })
                                                                                }
                                                                            }}
                                                                            maxLength="15"
                                                                            placeholder="0">
                                                                        </NumberInput>
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Company Description</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="companyDescription"
                                                                            value={(company)?company.companyDescription:""}
                                                                            onChange={(e) =>{
                                                                                e.preventDefault()
                                                                                if(company!=null){
                                                                                    company.companyDescription = e.target.value
                                                                                    this.setState({
                                                                                        company:company
                                                                                    })
                                                                                }
                                                                            }}
                                                                            placeholder="Enter Company Description"
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Business Field</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="businessField"
                                                                            value={(company)?company.businessField:""}
                                                                            onChange={(e) =>{
                                                                                e.preventDefault()
                                                                                if(company!=null){
                                                                                    company.businessField = e.target.value
                                                                                    this.setState({
                                                                                        company:company
                                                                                    })
                                                                                }
                                                                            }}
                                                                            placeholder="Enter business field"
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Build Date</Label>
                                                                        <DateInput
                                                                            id="buildDate"
                                                                            maxdate={new Date()}
                                                                            value={company.buildDate}
                                                                            onChange={(e) =>{
                                                                                let value = e.target.value
                                                                                company.buildDate = value
                                                                                this.setState({
                                                                                    company : company
                                                                                })
                                                                            }}
                                                                            placeholder="Enter build date">
                                                                        </DateInput>

                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="name">Address</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="address"
                                                                            value={(company)?company.address:""}
                                                                            onChange={(e) =>{
                                                                                e.preventDefault()
                                                                                if(company!=null){
                                                                                    company.address = e.target.value
                                                                                    this.setState({
                                                                                        company:company
                                                                                    })
                                                                                }
                                                                            }}
                                                                            placeholder="Enter Company Address"
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <ItemOption
                                                                            title={"Select Province"}
                                                                            objects={this.state.provinces}
                                                                            default={!allIsEmpty(this.state.province)?this.state.province.id:null}
                                                                            callback={(province)=>{
                                                                                if(province!=null){
                                                                                    this.setState({
                                                                                        province:province,
                                                                                        cities:[],
                                                                                        districts:[],
                                                                                        villages:[],
                                                                                    }, () => {
                                                                                        this.fetchCities(province.id, cities=>{
                                                                                            this.setState({
                                                                                                cities:cities
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                }else{
                                                                                    this.setState({
                                                                                        province:null,
                                                                                        cities:[],
                                                                                        districts:[],
                                                                                        villages:[],
                                                                                    })
                                                                                }
                                                                            }}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <ItemOption
                                                                            title={"Select City"}
                                                                            objects={this.state.cities}
                                                                            default={!allIsEmpty(this.state.city)?this.state.city.id:null}
                                                                            callback={(city)=>{
                                                                                if(city!=null){
                                                                                    this.setState({
                                                                                        city:city,
                                                                                        districts:[],
                                                                                        villages:[],
                                                                                    }, () => {
                                                                                        this.fetchDistricts(city.id, districts =>{
                                                                                            this.setState({
                                                                                                districts:districts
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                }else{
                                                                                    this.setState({
                                                                                        city:null,
                                                                                        districts:[],
                                                                                        villages:[],
                                                                                    })
                                                                                }
                                                                            }}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <ItemOption
                                                                            title={"Select District"}
                                                                            objects={this.state.districts}
                                                                            default={!allIsEmpty(this.state.district)?this.state.district.id:null}
                                                                            callback={(district)=>{
                                                                                if(district!=null){
                                                                                    this.setState({
                                                                                        district:district,
                                                                                        villages:[],
                                                                                    }, () => {
                                                                                        this.fetchVillages(district.id, villages=>{
                                                                                            this.setState({
                                                                                                villages:villages
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                }else{
                                                                                    this.setState({
                                                                                        district:null,
                                                                                        villages:[],
                                                                                    })
                                                                                }
                                                                            }}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <ItemOption
                                                                            title={"Select Village"}
                                                                            objects={this.state.villages}
                                                                            default={!allIsEmpty(this.state.village)?this.state.village.id:null}
                                                                            callback={(village)=>{
                                                                                if(village!=null){
                                                                                    let customer = this.state.customer
                                                                                    company.village = village
                                                                                    this.setState({
                                                                                        company:company,
                                                                                        village:village,
                                                                                    })
                                                                                }
                                                                            }}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                        </>
                                                    ):(null)
                                                }
                                                <FormGroup>
                                                    <ActiveOption
                                                        callback={(active)=>{
                                                            let prd = this.state.partner
                                                            if(prd!=null){
                                                                prd.active = active
                                                                this.setState({
                                                                    partner:prd
                                                                })
                                                            }

                                                        }}
                                                        default={this.state.partner?this.state.partner.active:false}
                                                    />
                                                </FormGroup>
                                                    <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    partner:null
                                                                }, () => {
                                                                   this.props.history.goBack();
                                                                })
                                                                )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.partner!=null && this.state.partner.id!=null)?"Update":"Create"
                                                            } </Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        this.setState({
                            image:file,
                            cropperModal:false,
                            imageBlob:fileBlob,
                        })
                    })
                }} cancelCallback={()=>{
                    this.setState({
                        cropperModal:false,
                        image:partner.imageUrl,
                        imageBlob:null,
                    })
                }}/>

            </Page>
        );
    }


}