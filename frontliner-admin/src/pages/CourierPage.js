import Page from 'components/Page';
import React from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, imageSelector, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import imageBuilding from 'assets/img/widget/bank_building.png';
import ImageCropper from "../components/modal/ImageCropper";
import axios from "axios";
import {getData} from "../utils/StorageUtil";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Img from 'react-image'


const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class CourierPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      couriers : [],
      courier:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'name',
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }


  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var courier = this.state.courier
    if(isEmpty(courier.name) || isEmpty(courier.label) || isEmpty(courier.code) || (isEmpty(this.state.imageBlob)&&isEmpty(courier.image))){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let courier = this.state.courier
    if(!courier.id && !courier.active){
      courier.active = false ;
    }

    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.COURIER_SAVE, courier, null, res => {
        if(res.code===200){
          this.successToast(res.message)
          this.setState({
            courier:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  saveUpload = () => {
    let courier = this.state.courier
    if(!courier.id && !courier.active){
      courier.active = false ;
    }
    let data = new FormData()
    data.append("courierDtoGson", JSON.stringify(courier))
    data.append("multipartFile", this.state.imageBlob)
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.COURIER_SAVE_UPLOAD, data, null, res => {
        if(res.code===200){
          this.successToast(res.message)
          this.setState({
            courier:{},
            image:null,
            imageBlob:null,
          }, () => {

            this.fetchAll()
          })
        }
      }, true, true);
    })
  }


  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.COURIER_DELETE+"/"+this.state.courier.id, null, null, res => {
        if(res.code===200){
          this.setState({
            image:null,
            imageBlob:null,
            courier:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.COURIERS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          couriers : response.data,
          totalPage: response.totalPage,
          totalElement: response.totalElement,
          pageElement: response.pageElement,
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    let configuration = getData(Global.CONFIGURATION)
    let aspect = 4/4
    if(configuration.courierImageDimensionWidth && configuration.courierImageDimensionHeight){
      aspect = configuration.courierImageDimensionWidth/configuration.courierImageDimensionHeight
    }
    return (
        <Page
            title="Courier Detail"
            breadcrumbs={[{ name: 'courier detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this courier item to your list ?"
              okCallback={this.saveUpload}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this courier item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Courier Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <Col>
                            <Card>
                              <CardHeader>
                                Courier Image
                              </CardHeader>
                              <CardBody>
                                <CardImg top src={
                                  this.state.image!=null?this.state.image:imageBuilding}
                                         onClick={e=>{
                                           if(this.state.image!=null){
                                             this.setState({
                                               imageLink:this.state.image,
                                               openPreview:!this.state.openPreview
                                             })
                                           }
                                         }}/>
                              </CardBody>
                              <CardFooter>
                                <Row>
                                  <Col>
                                    <ButtonGroup className="float-left">
                                      <Button color="primary" onClick={e=>(
                                          imageSelector(file=>{
                                            this.setState({
                                              image:file,
                                              cropperModal:true
                                            })
                                          }).click()
                                      )}>Upload</Button>
                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </CardFooter>
                            </Card>
                          </Col>
                        </Row>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="label">Label</Label>
                              <Input
                                  type="text"
                                  name="label"
                                  value={!isEmpty(this.state.courier.label)?this.state.courier.label:""}
                                  onChange={(e) =>{
                                    let courier = this.state.courier
                                    if(courier!=null){
                                      courier.label = e.target.value
                                      this.setState({
                                        courier:courier
                                      })
                                    }
                                  }}
                                  placeholder="Enter courier label"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.courier.name)?this.state.courier.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.courier
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        courier:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter courier name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="code">Code</Label>
                              <Input
                                  type="text"
                                  name="code"
                                  value={!isEmpty(this.state.courier.code)?this.state.courier.code:""}
                                  onChange={(e) =>{
                                    let courier = this.state.courier
                                    if(courier!=null){
                                      courier.code = e.target.value
                                      this.setState({
                                        courier:courier
                                      })
                                    }
                                  }}
                                  placeholder="Enter courier code"
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.courier.active}
                                callback={(active)=>{
                                  let ctg = this.state.courier
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      courier:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    image:null,
                                    imageBlob:null,
                                    courier:{}
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.courier!=null && this.state.courier.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Label</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th width="15%">Image</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.couriers.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.label}</td>
                                      <td>{item.name}</td>
                                      <td>{item.code}</td>
                                      <td width="15%">
                                        <Img
                                          className="img-thumbnail"
                                          src={item.imageLink}></Img></td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            courier:item,
                                            image:item.imageLink
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="primary" onClick={e=>(
                                            this.setState({
                                              courier:item,
                                              image:item.imageLink
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
            axios({
              url: file,
              method: 'GET',
              responseType: 'blob', // important
            }).then((response) => {
              var fileBlob = response.data ;
              this.setState({
                image:file,
                cropperModal:false,
                imageBlob:fileBlob,
              }, () => {
                // this.uploadPhotoProfile(file, login)
              })
            })
          }} cancelCallback={()=>{
            this.setState({
              cropperModal:false,
              image:null,
              imageBlob:null,
            })
          }}/>
        </Page>
    );
  }
}
