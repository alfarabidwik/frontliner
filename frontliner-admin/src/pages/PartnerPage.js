import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'

import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Img from 'react-image'
import {Link} from "react-router-dom";

const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class PartnerPage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
      partners : [],
      partner:{},
      modalDeleteConfirm:false,
      ascending:false,
      sortir:'jp.created',
      search:"",
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
            })
        }
    }



    fetchAll = (ascending, sortir, search, page, progressing) => {
        this.get(Global.API.PARTNERS, {
            params:{
                ascending:ascending,
                sortir:sortir,
                search:search,
                page: page-1
            }
        }, null, res=>{
            if(res.code === 200){
                this.setState({
                    partners : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        },progressing, true);
    }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.PARTNER_DELETE+"/"+this.state.partner.id, null, null, res=>{
          if(res.code===200){
              this.successToast(res.message)
              this.setState({
                  category:null
              }, () => {
                  this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
              })
          }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshPartner = (ascending, sortir, progressing) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, progressing)
      })
  )

    render() {
        var i = 0 ;

    return (
        <Page
            title="Partners"
            breadcrumbs={[{ name: 'partner', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this partner item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Partner</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={2}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.full_name", true))}>Fullname</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.website", true))}>Website</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.task_count", true))}>Task Count</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={2}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshPartner(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            <SearchInput
                                placeholder={"Type and enter to search..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            deleteParam(this.props, 'page')
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        deleteParam(this.props, 'page')
                                    }
                                }}
                            />
                        </Col>
                        <Col md={5}>
                            <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>{
                                    this.props.history.push('/partnerDetail')
                                }}>Add Partner</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Fullname</th>
                              <th>Type</th>
                              <th>Website</th>
                              <th>Task Count</th>
                              <th>Image</th>
                              <th>Status</th>
                              <th>Created</th>
                              <th>Updated</th>
                              <th>Edit</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.partners.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.fullName}</td>
                                      <td>{item.partnerType}</td>
                                      <td>
                                          {
                                              item.website?(
                                                  <a href={item.website.includes("http")?item.website:("http://"+item.website)} target="_blank">{item.website}</a>
                                              ):
                                                  ("-")
                                          }
                                      </td>
                                      <td>{item.taskCount}</td>
                                      <td width="5%">
                                          <Img
                                              className="img-thumbnail"
                                              src={item.imageUrl}
                                              onClick={e=>{
                                                  this.setState({
                                                      imageUrl:item.imageUrl,
                                                      openPreview:true
                                                  })
                                              }}
                                          />
                                          </td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          <Button size="sm" color="danger" onClick={e => {
                                              this.setState({
                                                  partner:item
                                              }, () => {
                                                  this.confirmDelete()
                                              })
                                          }}>
                                          <MdDelete/>
                                          </Button>
                                          &nbsp;
                                          <Button size="sm" color="primary" onClick={ e => (
                                              this.props.history.push('/partnerDetail?id='+item.id)
                                          )}>
                                          <MdEdit/>
                                          </Button>
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }

};
