import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, deleteParam, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import Pagination from "../components/Pagination";
import queryString from "query-string";
import ActiveLabel from "../components/Widget/ActiveLabel";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class VillagePage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      villages : [],
      village:{},
      districts : [],
      district:{},
      provinces : [],
      province:{},
      cities : [],
      city:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'name',
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentWillReceiveProps(props, nextContext) {
    if(props!=this.props){
      this.setState({
        page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
      }, () => {
        this.fetchAll()
      })
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
    this.fetchProvinces()
  }

  confirmSave = () =>{
    var district = this.state.district
    if(isEmpty(district.name) || isEmpty(district.heading)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let village = this.state.village
    if(!village.id && !village.active){
      village.active = false ;
    }

    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.VILLAGE_SAVE, village, null, res => {
        if(res.code===200){
          this.setState({
            district:{}
          }, () => {
            this.fetchDistricts()
          })
        }
      }, true, true);
    })
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.VILLAGE_DELETE+"/"+this.state.village.id, null, null, res => {
        if(res.code===200){
          this.setState({
            district:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll= () => {
    this.get(Global.API.VILLAGES, {
      params:{
        districtId:this.state.district.id,
        ascending:this.state.ascending,
        sortir:this.state.sortir,
        page:this.state.page-1,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          villages : response.data,
          totalPage: response.totalPage,
          totalElement: response.totalElement,
          pageElement: response.pageElement,
        })
      }
    }, true, true);
  }


  fetchDistricts = () => {
    this.get(Global.API.DISTRICTS, {
      params:{
        cityId:this.state.city.id,
        ascending:this.state.ascending,
        sortir:this.state.sortir
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          districts : response.data
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )

  fetchProvinces = () => {
    this.get(Global.API.PROVINCES, {
      params:{
        ascending:true,
        sortir:'name'
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          provinces : response.data
        })
      }
    }, true, true);
  }

  fetchCities = () => {
    if(this.state.province.id){
      this.get(Global.API.CITIES, {
        params:{
          provinceId:this.state.province.id,
          ascending:this.state.ascending,
          sortir:this.state.sortir
        }
      }, null, response=>{
        if(response.code === 200){
          this.setState({
            cities : response.data
          })
        }
      }, true, true);
    }else{
      this.setState({cities : []})
    }
  }



  render() {
    let provinces = this.state.provinces
    let province = this.state.province
    let cities = this.state.cities
    let city = this.state.city
    let districts = this.state.districts
    let district = this.state.district
    let villages = this.state.villages
    let village = this.state.village

    return (
        <Page
            title="District Detail"
            breadcrumbs={[{ name: 'district detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this district item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this district item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>District Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.village.name)?this.state.village.name:""}
                                  onChange={(e) =>{
                                    let village = this.state.village
                                    if(village!=null){
                                      village.name = e.target.value
                                      this.setState({
                                        village:village
                                      })
                                    }
                                  }}
                                  placeholder="Enter village name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <ItemOption
                                  title={"Select Province"}
                                  objects={provinces}
                                  default={province!=null?province.id:null}
                                  callback={(province)=>{
                                    if(province!=null){
                                      this.setState({
                                        province:province
                                      }, () => {
                                        this.fetchCities()
                                      })
                                    }else{
                                      this.setState({
                                        province:{}
                                      }, () => {
                                        this.fetchCities()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                            <FormGroup>
                              <ItemOption
                                  title={"Select City"}
                                  objects={cities}
                                  default={city!=null?city.id:null}
                                  callback={(city)=>{
                                    if(city!=null){
                                      this.setState({
                                        city:city
                                      }, () => {
                                          this.fetchDistricts()
                                      })
                                    }else{
                                      this.setState({
                                        city:{}
                                      }, () => {
                                        this.fetchDistricts()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                            <FormGroup>
                              <ItemOption
                                  title={"Select District"}
                                  objects={districts}
                                  default={district!=null?district.id:null}
                                  callback={(district)=>{
                                    if(district!=null){
                                      this.setState({
                                        district:district
                                      }, () => {
                                        this.fetchAll()
                                      })
                                    }else{
                                      this.setState({
                                        district:{}
                                      }, () => {
                                        this.fetchAll()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>

                            <FormGroup>
                              <ActiveOption
                                  default={this.state.district.active}
                                  callback={(active)=>{
                                    let ctg = this.state.district
                                    if(ctg!=null){
                                      ctg.active = active
                                      this.setState({
                                        district:ctg
                                      })
                                    }

                                  }}
                              />
                            </FormGroup>
                          </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    district:null
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.district!=null && this.state.district.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            <FormGroup>
                              <ItemOption
                                  title={"Select Province"}
                                  objects={this.state.provinces}
                                  default={this.state.province!=null?this.state.province.id:null}
                                  callback={(province)=>{
                                    let district = this.state.district
                                    if(province!=null && district!=null){
                                      district.province = province
                                      this.setState({
                                        district:district,
                                        province:province
                                      }, () => {
                                        this.fetchCities()
                                      })
                                    }else{
                                      district.province = {}
                                      this.setState({
                                        district:district,
                                        province:{}
                                      }, () => {
                                        this.fetchCities()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <ItemOption
                                  title={"Select City"}
                                  objects={cities}
                                  default={city!=null?city.id:null}
                                  callback={(city)=>{
                                    if(city!=null){
                                      this.setState({
                                        city:city
                                      }, () => {
                                        this.fetchDistricts()
                                      })
                                    }else{
                                      this.setState({
                                        city:{}
                                      }, () => {
                                        this.fetchDistricts()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <ItemOption
                                  title={"Select District"}
                                  objects={districts}
                                  default={district!=null?district.id:null}
                                  callback={(district)=>{
                                    if(district!=null){
                                      this.setState({
                                        district:district
                                      }, () => {
                                        this.fetchAll()
                                      })
                                    }else{
                                      this.setState({
                                        district:{}
                                      }, () => {
                                        this.fetchAll()
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Postal Code</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                villages.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.name}</td>
                                      <td>{item.postalCode}</td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            village:item,
                                            district:item.district,
                                            city:item.district.city,
                                            province:item.district.city.province,
                                          }, () => {
                                            this.fetchDistricts()
                                            this.fetchCities()
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="primary" onClick={e=>(
                                            this.setState({
                                              village:item,
                                              district:item.district,
                                              city:item.district.city,
                                              province:item.district.city.province,
                                            }, () => {
                                              this.fetchDistricts()
                                              this.fetchCities()
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
