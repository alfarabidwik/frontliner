import BasePage from "./BasePage";
import React from 'react'
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {changeParam2, changeParam3, currencyFormat, parseDate, sortirMap} from "../utils/Utilities";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye} from "react-icons/io";
import SearchInput from '../components/SearchInput'
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {IoMdOpen} from "react-icons/io/index";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import Button from "reactstrap/es/Button";
import SockJsClient from 'react-stomp'
import UpdateWithdrawalRequestStatusModal from "./../components/modal/UpdateWithdrawalRequestStatusModal";
import WithdrawalRequestTrackerModal from "./../components/modal/WithdrawalRequestTrackerModal";


export default class WithdrawalRequestPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:false,
            sortir:'wr.created',
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            key:queryString.parse(this.props.query).tab?queryString.parse(this.props.query).tab:"All",
            totalPage:0,
            totalElement:0,
            pageElement:0,
            withdrawalStatuses:[],
            withdrawalRequests:[],
            withdrawalStatus:null,
            withdrawalStatusId:null,
            search:queryString.parse(this.props.query).search?queryString.parse(this.props.query).search:"",
            inputSearch : "",
            withdrawalStatusTrackerModal:false
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchWithdrawalStatus()
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            let page = queryString.parse(props.query).page?queryString.parse(props.query).page:1
            let key = queryString.parse(props.query).tab?queryString.parse(props.query).tab:"All"
            let search = queryString.parse(props.query).search?queryString.parse(props.query).search:""
            let withdrawalStatusId = null
            if(key==="All"){
                withdrawalStatusId = null
            }else{
                withdrawalStatusId = key
            }
            this.setState({
                page:page,
                key:key,
                search:search,
                withdrawalStatusId:withdrawalStatusId
            }, () => {
                this.fetchWithdrawalRequest(true)
            })
        }
    }


    fetchWithdrawalStatus = () =>{
        this.get(Global.API.WITHDRAWAL_STATUSES, null, null, response=>{
            if(response.code===200){
                this.setState({
                    withdrawalStatuses : response.data
                }, () => {
                    this.fetchWithdrawalRequest(true)
                })
            }
        }, true, true)
    }

    fetchWithdrawalRequest = (progressing) =>{
        this.get(Global.API.WITHDRAWAL_REQUESTS, {
            params:{
                withdrawalStatusId:this.state.withdrawalStatusId,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    withdrawalRequests : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,

                })
            }
        }, progressing, true)
    }

    refreshWithdrawalRequest = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
            this.fetchWithdrawalRequest(progressing)
        })
    )


    render() {

        let updateStatusModal = this.state.updateStatusModal
        let withdrawalStatusTrackerModal = this.state.withdrawalStatusTrackerModal
        let withdrawalRequest = this.state.withdrawalRequest?this.state.withdrawalRequest:{}

        return (
            <div>
                {super.render()}
                {this.renderWithdrawalRequest()}
                <UpdateWithdrawalRequestStatusModal modal={updateStatusModal} withdrawalRequest={withdrawalRequest} okCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    }, () => {
                        this.refreshWithdrawalRequest()
                    })
                }} closeCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    })
                }}/>
                <WithdrawalRequestTrackerModal user={withdrawalRequest.user} showing={withdrawalStatusTrackerModal} pvWithdrawalRequestStatuses={withdrawalRequest.pvWithdrawalRequestStatuses} close={()=>{
                    this.setState({
                        withdrawalStatusTrackerModal:false
                    })
                }}/>
            </div>
        )
    }

    renderWithdrawalRequest = () =>{
        return (
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader>Withdrawal Request</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={3}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir?this.state.sortir.toString():'')
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "u.agentCode", true))}>Agent Code</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "u.fullname", true))}>Name</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "u.email", true))}>Email</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "u.mobilePhone", true))}>Mobile Phone</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "wr.amount", true))}>Amount</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "wr.created", true))}>Created</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(this.state.ascending, "wr.updated", true))}>Updated</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshWithdrawalRequest(false, this.state.sortir, true))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={4}>
                                    <Row>
                                        <Col>
                                            <SearchInput
                                                placeholder={"Agent name, Agent code, Phone, Org..."}
                                                value={this.state.inputSearch}
                                                onChange={e=>{
                                                    this.setState({
                                                        inputSearch:e.target.value
                                                    }, () => {
                                                        if(this.state.inputSearch===''){
                                                            changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.inputSearch!==''){
                                                        changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col>
                                            {/*<Button onClick={event=>{*/}
                                                {/*let key = this.state.key*/}
                                                {/*changeParam3(this.props, 'page', 1, 'tab', key, 'search', this.state.inputSearch)*/}
                                            {/*}}><MdSearch/></Button>*/}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={key =>{
                                    changeParam2(this.props, 'page', 1, 'tab', key)
                                }
                                }>
                                <Tab eventKey="All" title="All">
                                    {this.renderWithdrawalRequestTable()}
                                </Tab>
                                {
                                    this.state.withdrawalStatuses.map((item, index)=>(
                                        <Tab key={index} eventKey={item.id} title={item.name}>
                                            {this.renderWithdrawalRequestTable()}
                                        </Tab>
                                    ))
                                }
                            </Tabs>
                        </CardBody>
                        <SockJsClient
                            url={Global.BASE_URL}
                            topics={[Global.API.LISTEN_WITHDRAWAL_REQUEST]}
                            onMessage={(message) => {
                                this.successToast(message)
                                this.fetchWithdrawalRequest(true)
                            }}
                            ref={ (client) => { this.clientRef = client }} />
                    </Card>
                </Col>
            </Row>
        )
    }


    renderWithdrawalRequestTable = () =>{
        let updateStatusModal = this.state.updateStatusModal

        return (
            <Card>
                <CardBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Agent Code</th>
                            <th>Fullname</th>
                            <th>Mobile Phone</th>
                            <th>Organization</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Track</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.withdrawalRequests.map((item, index)=>(
                                <tr key={item.id}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.user.agentCode}</td>
                                    <td>
                                        {item.user.fullname}
                                        &nbsp;&nbsp;
                                        <IoMdOpen
                                            color="blue" style={{cursor:'pointer'}}
                                            onClick={e=>{
                                                this.props.history.push('/userDetail?id='+item.user.id)
                                            }
                                            }/>
                                    </td>
                                    <td>{item.user.mobilePhone}</td>
                                    <td>{item.user.organization}</td>
                                    <td>{currencyFormat(item.amount, CURRENCY_SYMBOL)}</td>
                                    <td>{item.withdrawalStatus.description}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>
                                        <Button className="float-right" onClick={event=>{
                                            this.setState({
                                                withdrawalStatusTrackerModal:true,
                                                withdrawalRequest:item
                                            })
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td>
                                        <Button color={"primary"} className="float-right" onClick={event=>{
                                            this.setState({
                                                updateStatusModal:true,
                                                withdrawalRequest:item
                                            })
                                        }}><IoMdOpen/></Button>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }


}