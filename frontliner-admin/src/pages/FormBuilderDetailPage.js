import * as React from 'react';
import Page from './../components/Page';
import {Button, Card, CardBody, CardHeader, Col, FormGroup, Label, Row} from 'reactstrap';
import BasePage from "./BasePage";
import Board from '@lourenci/react-kanban'
import FormLaneModal from "../components/modal/FormLaneModal";
import FormModal from "../components/modal/FormModal";
import {FormLane} from "../model/formbuilder/FormLane";
import FormCard from "../components/Card/FormCard";
import {GroupForm} from "../model/formbuilder/GroupForm";
import Input from "reactstrap/es/Input";
import WarningLabel from "../components/Widget/WarningLabel";
import Global from "../utils/Global";
import * as queryString from "query-string";

class FormBoard{
    lanes  = new Array()
}

const tableTypes = ['', 'bordered', 'striped', 'hover'];
export default class FormBuilderDetailPage extends BasePage{
    board = new FormBoard()
  // board = {
  //   lanes : [
  //     {
  //       id: 0,
  //       title: 'Example',
  //       cards: [
  //         {
  //           id: 1,
  //           tag:"TAG",
  //           title: 'Form 1',
  //           placeHolder:"Placeholder",
  //           description: 'Card content'
  //         }        ]
  //     }
  //   ]
  // }

    validator = {
        title : ""
    }

  constructor(props) {
    super(props);
    let groupForm  = new GroupForm();
    this.state = {
        categories: [],
        category: {},
        headings: [],
        heading: {},
        modalAddConfirm: false,
        modalDeleteConfirm: false,
        formLaneModal: false,
        formModal: false,
        ascending: true,
        sortir: 'created',
        board: this.board,
        groupForm: groupForm,
        groupFormId : queryString.parse(this.props.query).id,
        validator : this.validator
    }
  }

  componentDidMount() {
    super.componentDidMount();
    if(this.state.groupFormId){
        this.fetchDetail()
    }
  }

  fetchDetail(){
    let board = this.state.board
        this.get(Global.API.GROUP_FORM+'/'+this.state.groupFormId, null, null, (res)=>{
        if(res.code===200){
            board.lanes = res.data.formLanes
            this.setState({
                groupForm:res.data,
                board : board
            })
        }
    }, false, false)
  }


  onClickSave(){
    let groupForm = this.state.groupForm
    let validator = this.state.validator
    let board = this.state.board;
    if(!groupForm.name){
        validator.title = "Please enter a group name"
        this.setState({
            validator:validator
        })
        return ;
    }
    if(!board.lanes || board.lanes.length===0){
        this.showDialog("Warning", "Please add a minimum single form")
        return;
    }
    if(board.lanes.length>0){
        board.lanes.forEach(value => {
            if(!value.cards || value.cards.length<=0){
                this.showDialog("Warning", "Please add a minimum single form or remove it")
                return
            }
        })
    }
    groupForm.formLanes = board.lanes
      groupForm.formLanes.forEach((value, index) => {
        value.id = index+1
          value.cards.forEach((value1, index1) => {
              value1.id = index1+1
          })
    })
    console.log(JSON.stringify(groupForm))
    this.post(Global.API.GROUP_FORM_SAVE, groupForm, null, (res)=>{
        if(res.code===200){
            this.successToast(res.message)
            this.setState({
                groupForm:res.data
            })
        }
    }, true, true)
  }

  render() {
    let board = this.state.board;
    let formLaneModal = this.state.formLaneModal
    let formModal = this.state.formModal
    let validator = this.state.validator
    let groupForm = this.state.groupForm ;
    // let pagelength = 0
    // let cardLength = 0 ;
    // if(board.lanes){
    //     board.lanes.forEach(value => {
    //         if(pagelength<=value.id){
    //             pagelength=value.id+1
    //         }
    //         value.cards.forEach(value1 => {
    //             if(cardLength<=value1.id){
    //                 cardLength=value1.id+1
    //             }
    //         })
    //     })
    // }

    return (
        <Page
            title="Form Builder"
            breadcrumbs={[{ name: 'form builder', active: true }]}
            className="TablePage">
          {super.render()}
          <FormLaneModal id={new Date().getTime()} showing={formLaneModal} onAdd={(formLane)=>{
            let lanes = board.lanes
            lanes.push(formLane)
            board.lanes = lanes
            this.setState({
              formLaneModal:false,
              board:board
            })
          }} close={()=>{
              this.setState({
                  formLaneModal:false,
              })
          }}/>
          <FormModal id={new Date().getTime()} showing={formModal} lanes={board.lanes} lane={null} onAdd={(form , lane )=>{
                let laneIndex = -1;
              board.lanes.forEach((value, index) => {
                  if(value.id===lane.id){
                      laneIndex =  index ;
                  }
              })
              if(laneIndex>-1){
                  let lanes = board.lanes
                  lanes[laneIndex].cards.push(form)
                  board.lanes = lanes
                  this.setState({
                      formModal:false,
                      board:board
                  })
              }
          }} close={()=>{
              this.setState({
                  formModal:false,
              })
          }}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Form Builder</CardHeader>
                <CardBody >
                  <Row>
                    <Col>
                        <FormGroup>
                            <Label for="name">Form Name</Label>
                            <Input
                                type="text"
                                name="name"
                                value={groupForm?groupForm.name:""}
                                onChange={(e) =>{
                                    groupForm.name = e.target.value
                                    validator.title = ""
                                    this.setState({
                                        groupForm:groupForm,
                                        validator:validator
                                    })
                                }}
                                placeholder="Enter group name"
                            />
                            <WarningLabel message={validator.title}/>
                        </FormGroup>
                    </Col>
                      <Col>
                          <FormGroup>
                              <Label for="name">Created</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={groupForm?groupForm.created:"-"}
                                  onChange={(e) =>{
                                      groupForm.created = e.target.value
                                      this.setState({
                                          groupForm:groupForm
                                      })
                                  }}
                                  disabled={true}
                              />
                          </FormGroup>
                      </Col>
                      <Col>
                          <FormGroup>
                          <Label for="name">Updated</Label>
                          <Input
                              type="text"
                              name="name"
                              value={groupForm?groupForm.updated:"-"}
                              onChange={(e) =>{
                                  groupForm.updated = e.target.value
                                  this.setState({
                                      groupForm:groupForm
                                  })
                              }}
                              disabled={true}
                          />
                        </FormGroup>
                      </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
            <Row>
                <Col>
                    <Card>
                        <CardHeader>
                            <Button style={{marginRight:'10px'}} color='primary' onClick={e=>{
                                this.setState({
                                    formLaneModal:true
                                })
                            }}>Add Form Page +</Button>
                            {
                                (board.lanes.length>0)&&(
                                    <Button color='secondary' onClick={e=>{
                                        this.setState({
                                            formModal:true
                                        })
                                    }}>Add Form Card +</Button>
                                )
                            }
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col>
                                    <Board
                                        allowRemoveLane
                                        allowRenameLane
                                        allowRemoveCard
                                        allowAddLane
                                        onLaneRemove={(board, lane)=>{
                                            this.setState({
                                                board:board
                                            })
                                        }}
                                        onCardRemove={(board, lane, card)=>{
                                            this.setState({
                                                board:board
                                            })
                                        }}
                                        onLaneRename={(board, lane)=>{
                                            this.setState({
                                                board:board
                                            })
                                        }}
                                        initialBoard={this.state.board}
                                        onCardDragEnd={(board, source, destination)=>{
                                            this.setState({
                                                board:board
                                            })
                                        }}
                                        onLaneDragEnd={(board, source, destination)=>{
                                            this.setState({
                                                board:board
                                            })
                                        }}
                                        renderCard={(card, cardBag) => (
                                            <FormCard form={card} cardBag={cardBag}/>
                                        )}
                                    />
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Button onClick={(e)=>{
                        this.onClickSave()
                    }}>Save Form</Button>
                </Col>
            </Row>
          </Page>
    );
  }
}
