import BasePage from "./BasePage";
import React from 'react'
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem,
    DropdownMenu,
    DropdownToggle, FormGroup, Input, Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye} from "react-icons/io";
import SearchInput from '../components/SearchInput'
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {IoMdOpen} from "react-icons/io/index";
import PropTypes from "prop-types";
import StockAuditInventoryPage from "./StockAuditInventoryPage";
import queryString from "query-string";
import Pagination from '../components/Pagination'
import ItemOption from "../components/Widget/ItemOption";
import DateInput from "../components/Widget/DateInput";

export default class StockAuditStockPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:false,
            sortir:'created',
            stockAudits:[],
            search:"",
            creatorName:"",
            show:this.props.show,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            substraction:null,
            type:null,
            createdStartDate:null,
            createdEndDate:null
        }
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.state.show){
            this.fetchTransaction(true)
        }
    }

    componentWillReceiveProps(props, nextContext) {
        let show = this.state.show
        let currentPage = this.state.page
        let propsPage = queryString.parse(props.query).page?queryString.parse(props.query).page:1
        if((show!=props.show && props.show===true)||currentPage!=propsPage){
            this.setState({
                show:props.show,
                page:propsPage,
                totalPage:0,
                totalElement:0,
                pageElement:0,
            }, () => {
                this.fetchTransaction(true)
            })
        }else{
            this.setState({
                show:props.show
            })
        }
    }


    fetchTransaction = (progressing) =>{
        let dateRange = {}
        if(this.state.createdStartDate && this.state.createdEndDate){
            dateRange = {
                startDate:this.state.createdStartDate,
                endDate:this.state.createdEndDate,
            }
        }
        this.get(Global.API.STOCK_AUDITS, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                creatorName:this.state.creatorName,
                page:this.state.page-1,
                type:this.state.type,
                substraction:this.state.substraction,
                ...dateRange
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    stockAudits : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }


    render() {
        return this.renderTransaction()
    }

    renderTransaction = () =>{

        let substractions = [
            {
                id:false,
                name:"+"
            },
            {
                id:true,
                name:"-"
            }
        ]
        let types = [
            {
                id: 'SOLD_OUT',
                name: 'SOLD OUT'
            },
            {
                id: 'MANUAL_REDUCTION',
                name: 'MANUAL REDUCTION'
            },
            {
                id: 'PROCUREMENT',
                name: 'PROCUREMENT'
            },
        ]
        let directions = [
            {id:true, name:'Ascending'},{id:false, name:'Descending'}
        ]
        let sortirs = [
            {id:'type', name:'Type'},{id:'quantity', name:'Quantity'},{id:'note', name:'Note'},{id:'inventory.product.name', name:'Product'},{id:'inventory.name', name:'Inventory'},{id:'creator.firstname', name:'Creator'},{id:'created', name:'Created'}

        ]
        return (
            <Card  className="border-top-0">
                {super.render()}
                <CardBody>
                    <Row>
                        <Col md={3}>
                            <ItemOption
                                title={"Select Sortir"}
                                objects={sortirs}
                                default={this.state.sortir}
                                hideOptionLabel={true}
                                callback={(sortir)=>{
                                    if(sortir!=null){
                                        this.setState({
                                            sortir:sortir.id
                                        }, () => {
                                            this.fetchTransaction(true)
                                        })
                                    }
                                }}
                            />

                        </Col>
                        <Col md={3}>
                            <ItemOption
                                title={"Select Direction"}
                                objects={directions}
                                default={this.state.ascending}
                                hideOptionLabel={true}
                                callback={(ascending)=>{
                                    if(ascending!=null){
                                        this.setState({
                                            ascending:ascending.id
                                        }, () => {
                                            this.fetchTransaction(true)
                                        })
                                    }
                                }}
                            />
                        </Col>
                        <Col md={3}>
                            <ItemOption
                                title={"Select In/Out"}
                                objects={substractions}
                                default={this.state.substraction!=null?this.state.substraction:null}
                                callback={(substraction)=>{
                                    if(substraction!=null){
                                        this.setState({
                                            substraction:substraction.id
                                        }, () => {
                                            this.fetchTransaction(true)
                                        })
                                    }
                                }}
                            />
                        </Col>
                        <Col md={3}>
                            <ItemOption
                                title={"Select Type"}
                                objects={types}
                                default={this.state.type!=null?this.state.type:null}
                                callback={(type)=>{
                                    if(type!=null){
                                        this.setState({
                                            type:type.id
                                        }, () => {
                                            this.fetchTransaction(true)
                                        })
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>
                            <Label for="createdStartDate">Created Start Date</Label>
                            <DateInput
                                id="createdStartDate"
                                maxdate={this.state.createdEndDate}
                                value={this.state.createdStartDate}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        createdStartDate : value?value:null
                                    }, () => {
                                        if(this.state.createdStartDate && this.state.createdEndDate){
                                            this.fetchTransaction(true)
                                        }else if(!this.state.createdStartDate && this.state.createdEndDate){
                                            this.fetchTransaction(true)
                                        }
                                    })
                                }}
                                placeholder="Enter start date"
                            />
                        </Col>
                        <Col md={3}>
                            <Label for="createdEndDate">Created End Date</Label>
                            <DateInput
                                id="createdEndDate"
                                mindate={this.state.createdStartDate}
                                value={this.state.createdEndDate}
                                onChange={(e) =>{
                                    let previousEndDate = this.state.createdEndDate;
                                    let value = e.target.value
                                    this.setState({
                                        createdEndDate :  value?value:null
                                    }, () => {
                                        if(this.state.createdStartDate && this.state.createdEndDate){
                                            this.fetchTransaction(true)
                                        }else if(this.state.createdStartDate && !this.state.createdEndDate){
                                            this.fetchTransaction(true)
                                        }
                                    })
                                }}
                                placeholder="Enter end date"
                            />
                        </Col>
                        <Col md={3}>
                            <Label for="search">Search</Label>
                            <SearchInput
                                tooltip={"Type and enter to search..."}
                                placeholder={"Note, product, inventory..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value.replace(/[^\w\s]/gi, '')
                                    }, () => {
                                        if(this.state.search===''){
                                            this.setState({
                                                show:false
                                            }, () => {
                                                deleteParam(this.props, 'page')
                                            })
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        this.setState({
                                            show:false
                                        }, () => {
                                            deleteParam(this.props, 'page')
                                        })
                                    }
                                }}
                            />

                        </Col>
                        <Col md={3}>
                            <Label for="searchCreator">Search Creator</Label>
                            <SearchInput
                                placeholder={"Search by creator name... "}
                                value={this.state.creatorName}
                                onChange={e=>{
                                    this.setState({
                                        creatorName:e.target.value.replace(/[^\w\s]/gi, '')
                                    }, () => {
                                        if(this.state.creatorName===''){
                                            this.setState({
                                                show:false
                                            }, () => {
                                                deleteParam(this.props, 'page')
                                            })
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.creatorName!==''){
                                        this.setState({
                                            show:false
                                        }, () => {
                                            deleteParam(this.props, 'page')
                                        })
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {
                                this.renderTransactionTable()
                            }
                        </Col>
                    </Row>
                </CardBody>
                <Pagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} key={'audit'}/>
            </Card>
        )
    }


    renderTransactionTable = () =>{
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Quantity</th>
                    <th>Note</th>
                    <th align="center" className="text-center">+&nbsp;&nbsp;|&nbsp;&nbsp;-</th>
                    <th>Product</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Created</th>
                    <th>Creator</th>
                    <th className="text-center">View</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.stockAudits.map((item, index)=>(
                        <tr key={index}>
                            <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                            <td>{item.type}</td>
                            <td>{item.quantity}</td>
                            <td>
                                {item.note}
                            </td>
                            <td align="center">{item.substraction?"-":"+"}</td>
                            <td>{item.inventory.product.name}</td>
                            <td>{item.inventory.typeName}</td>
                            <td>
                                <Button onClick={event=>{
                                    event.preventDefault()
                                    this.openLightBox(item.inventory.inventoryImages)
                                }}><IoMdEye/></Button>
                            </td>
                            <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            <td>{item.creator.firstname}</td>
                            <td align='center'>
                                <IoMdOpen
                                    color="green"
                                    style={{cursor:'pointer'}}
                                    onClick={e=>{
                                        this.props.history.push('/inventoryDetail?productId='+item.inventory.product.id+'&inventoryId='+item.inventory.id)
                                    }}
                                />
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </Table>
        )
    }
}
StockAuditStockPage.propTypes = {
    show:PropTypes.bool.isRequired
}