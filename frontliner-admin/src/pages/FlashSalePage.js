import BasePage from "./BasePage";
import React from "react"
import Page from "../components/Page";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem, DropdownMenu, DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {isEmpty, parseDate, sortirMap} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
import FlashSaleModal from "../components/modal/FlashSaleModal";
import {IoMdOpen} from "react-icons/io";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";
import ActiveLabel from "../components/Widget/ActiveLabel";


export default class FlashSalePage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            modalDeleteConfirm:false,
            modalAddFlashSale:false,
            ascending:true,
            sortir:'created',
            flashSales:[],
            flashSale:{},
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            edit:false
        }
    }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(true)
    }

    fetchAll = (progressing) => {
        this.get(Global.API.FLASHSALES, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page: this.state.page-1
            }
        }, null, response =>{
            if(response.code === 200){
                this.setState({
                    flashSales : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }


    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        this.setState({
            modalDeleteConfirm:false
        }, () => {
            this.get(Global.API.FLASHSALE_DELETE+"/"+this.state.product.id, null, null, response=>{
                if(response.code===200){
                    this.setState({
                        flashSale:{}
                    }, () => {
                        this.fetchAll(true)
                    })
                }
            }, true, true)
        })
    }

    refresh = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending!=undefined && ascending!=null ?ascending:this.state.ascending,
            sortir:sortir?sortir:this.state.sortir
        }, () => {
            this.fetchAll(progressing)
        })
    )

    closeFlashSaleModal = () =>{
        this.setState({
            modalAddFlashSale:!this.state.modalAddFlashSale,
            flashSale:{},
            edit:!this.state.edit
        })
    }

    render() {
        return (
            <Page
                title="Flash Sale"
                breadcrumbs={[{ name: 'flash sale', active: true }]}
                className="TablePage">
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this flash sale item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <FlashSaleModal
                    showing={this.state.modalAddFlashSale}
                    edit={this.state.edit}
                    flashSale={this.state.flashSale}
                    close={this.closeFlashSaleModal}
                    refresh={this.refresh}
                />
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>Flash Sale</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={2}>
                                        Sort By :
                                        <UncontrolledButtonDropdown key={1}>
                                            <DropdownToggle
                                                caret
                                                color="white"
                                                className="text-capitalize m-1">
                                                {
                                                    sortirMap(this.state.sortir)
                                                }
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name", true))}>Name</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created", true))}>Created</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated", true))}>Updated</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "startDate", true))}>Start Date</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "endDate", true))}>End Date</DropdownItem>
                                            </DropdownMenu>
                                        </UncontrolledButtonDropdown>
                                    </Col>
                                    <Col md={2}>
                                        Sortir :
                                        <UncontrolledButtonDropdown key={2}>
                                            <DropdownToggle
                                                caret
                                                color="white"
                                                className="text-capitalize m-1">
                                                {
                                                    this.state.ascending?"Ascending":"Descending"
                                                }
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir, true))}>Descending</DropdownItem>
                                            </DropdownMenu>
                                        </UncontrolledButtonDropdown>
                                    </Col>
                                    <Col md={3}>
                                        <SearchInput
                                            placeholder={"Search flashsale name..."}
                                            value={this.state.search}
                                            onChange={e=>{
                                                this.setState({
                                                    search:e.target.value
                                                }, () => {
                                                    if(this.state.search===''){
                                                        deleteParam(this.props, 'page')
                                                    }
                                                })
                                            }}
                                            onEnter={e=>{
                                                if(this.state.search!==''){
                                                    deleteParam(this.props, 'page')
                                                }
                                            }}
                                        />

                                        {/*<SearchInput*/}
                                            {/*value={this.state.search} onChange={e=>{*/}
                                            {/*this.setState({*/}
                                                {/*search:e.target.value*/}
                                            {/*}, () => {*/}
                                                {/*deleteParam(this.props, 'page')*/}
                                                {/*this.refresh(this.state.ascending, this.state.sortir, false)*/}
                                            {/*})*/}
                                        {/*}}/>*/}
                                    </Col>
                                    <Col md={5}>
                                        <ButtonGroup className="float-right">
                                            <Button color="primary" onClick={e=>{
                                                this.setState({
                                                    modalAddFlashSale:!this.state.modalAddFlashSale
                                                })
                                            }}>Add Flash Sale</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Table hover>
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Content/Stock</th>
                                            <th>Sold Stock</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.flashSales.map((item, index)=>(
                                                <tr>
                                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                    <td>{item.name}</td>
                                                    <th>0/0</th>
                                                    <th>0</th>
                                                    <td>{parseDate(item.startDate)}</td>
                                                    <td>{parseDate(item.endDate)}</td>
                                                    <td>
                                                        {/*<Typography className={item.active?"text-primary":"text-warning"}>*/}
                                                            {/*<strong>*/}
                                                            {/*{*/}
                                                                {/*item.active?"Active":"Inactive"*/}
                                                            {/*}*/}
                                                            {/*</strong>*/}
                                                        {/*</Typography>*/}
                                                        <ActiveLabel active={item.active}/>
                                                    </td>
                                                    <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                    <td>
                                                        <IoMdOpen color="blue" onClick={ e => {
                                                            this.props.history.push('/flashSaleDetail?id='+item.id)
                                                        }}/>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                        </tbody>
                                    </Table>
                                </Row>
                            </CardBody>
                            <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                        </Card>
                    </Col>
                </Row>
                {super.render()}
            </Page>
        );
    }
}