import * as React from "react";
import Page from './../components/Page';
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS, PRODUCT_KEUANGAN_ID} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'

import Pagination from '../components/Pagination'
import * as queryString from 'query-string/index';
import {deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import ActiveLabel from "../components/Widget/ActiveLabel";
const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class TaskPage extends BasePage{


  constructor(props) {
    super(props);
    let tasks  = new Array();
    this.state ={
      tasks : tasks,
      task:{},
      modalDeleteConfirm:false,
      ascending:false,
      sortir:'p.created',
      search:"",
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
            })
        }
    }



    fetchAll = (ascending, sortir, search, page, progressing) => {
        this.get(Global.API.TASKS, {
            params:{
                ascending:ascending,
                sortir:sortir,
                search:search,
                page: page-1
            }
        }, null, res=>{
            if(res.code === 200){
                this.setState({
                    tasks : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        },progressing, true);
    }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.TASK_DELETE+"/"+this.state.task.id, null, null, res=>{
          if(res.code===200){
              this.successToast(res.message)
              this.setState({
                  category:null
              }, () => {
                  this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
              })
          }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshTask = (ascending, sortir, progressing) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, progressing)
      })
  )

    render() {
        var i = 0 ;

    return (
        <Page
            title="Tasks"
            breadcrumbs={[{ name: 'task', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this task item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Task</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={2}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "jt.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "jt.title", true))}>Title</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "jt.suggestion", true))}>Suggestion</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "p.fullName", true))}>Partner</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "jt.applicationCount", true))}>Task Done</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(this.state.ascending, "jt.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={2}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshTask(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTask(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            <SearchInput
                                placeholder={"Type and enter to search..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            deleteParam(this.props, 'page')
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        deleteParam(this.props, 'page')
                                    }
                                }}
                            />
                        </Col>
                        <Col md={5}>
                            <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>{
                                    this.props.history.push('/taskDetail')
                                }}>Add Task</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Title</th>
                              <th>Partner</th>
                              <th>Period</th>
                              <th>Apply Limit</th>
                              <th>Category</th>
                              <th>Form/Link</th>
                              <th>Task Done</th>
                              <th>Suggestion</th>
                              <th>Published</th>
                              <th>Last Update</th>
                              <th>Edit</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.tasks.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.title}</td>
                                      <td>{item.partner.fullName}</td>
                                      <td>{item.periodType}</td>
                                      <td>{item.applicationLimitType}</td>
                                      <td>{item.taskCategory.name}</td>
                                      <td>
                                          {
                                              item.taskCategory.id===PRODUCT_KEUANGAN_ID?
                                                  (
                                                    item.taskAddressLink
                                                  )
                                                  :
                                                  (
                                                      item.groupForm?item.groupForm.name:"-"
                                                  )
                                          }
                                      </td>
                                      <td>{item.applicationCount}</td>
                                      <td>{item.suggestion}</td>
                                      <td><ActiveLabel active={item.published} trueLabel={"Published"} falseLabel={"Unpublished"}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          {/*<Button size="sm" color="danger" onClick={e => {*/}
                                              {/*this.setState({*/}
                                                  {/*task:item*/}
                                              {/*}, () => {*/}
                                                  {/*this.confirmDelete()*/}
                                              {/*})*/}
                                          {/*}}>*/}
                                          {/*<MdDelete/>*/}
                                          {/*</Button>*/}
                                          <Button size="sm" color="primary" onClick={ e => (
                                              this.props.history.push('/taskDetail?id='+item.id)
                                          )}>
                                          <MdEdit/>
                                          </Button>
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }

};
