import { AnnouncementCard, TodosCard } from 'components/Card';
import Page from 'components/Page';
import { IconWidget, NumberWidget } from 'components/Widget';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import {
  avatarsData,
  chartjs,
  productsData,
  supportTicketsData,
  todosData,
  userProgressTableData,
} from 'demos/dashboardPage';
import React from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  MdBubbleChart,
  MdInsertChart,
  MdPersonPin,
  MdPieChart,
  MdRateReview,
  MdShare,
  MdShowChart,
  MdThumbUp,
} from 'react-icons/md';
import InfiniteCalendar from 'react-infinite-calendar';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardDeck,
  CardGroup,
  CardHeader,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
} from 'reactstrap';
import { getColor } from 'utils/colors';
import BasePage from "./BasePage";
import Global, {_ORDER, _PAID, CURRENCY_SYMBOL} from "../utils/Global";
import { randomNum } from 'utils/demos';
import {changeParam2, currencyFormat} from "../utils/Utilities";
import {MdRefresh} from "react-icons/md/index";
import DashboardTaskApplicationCard from '../components/tab/dashboard/DashboardTaskApplicationCard'
import DashboardCategoriyAnalytic from './../components/tab/dashboard/DashboardCategoryAnalytic'
import TransactionItemAnalytic from './../components/tab/dashboard/TransactionItemAnalytic'
import TransactionAnalytic from './../components/tab/dashboard/TransactionAnalytic'
import SockJsClient from 'react-stomp'
import {AdminDashboard} from "../model/model";

const today = new Date();
const lastWeek = new Date(
  today.getFullYear(),
  today.getMonth(),
  today.getDate() - 7,
);

let transactionCardRef =  React.createRef()
let categoryCardRef =  React.createRef()
let transactionItemCardRef =  React.createRef()
let transactionAnalyticCardRef =  React.createRef()


const genLineData = (moreData = {}, moreData2 = {}) => {
  return {
    datasets: [
      {
        label: 'Dataset 1',
        backgroundColor: getColor('primary'),
        borderColor: getColor('primary'),
        borderWidth: 1,
        data: [
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
        ],
        ...moreData,
      },
      {
        label: 'Dataset 2',
        backgroundColor: getColor('secondary'),
        borderColor: getColor('secondary'),
        borderWidth: 1,
        data: [
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
          randomNum(),
        ],
        ...moreData2,
      },
    ],
  };
};



class MainDashboardPage extends BasePage {

  constructor(props) {
    super(props);
    let state = this.state
    state.mounted = false
    state.adminDashboard = new AdminDashboard()
    state.transactionStatusId = _PAID
    this.setState(state)
  }


  componentDidMount() {
    // this is needed, because InfiniteCalendar forces window scroll
    window.scrollTo(0, 0);
    if(!this.state.mounted){
      // this.fetchAdminDashboard()
      this.setState({
        mounted : true
      }, () => {
        this.fetchAdminDashboard(true)
      })
    }
  }


  fetchAdminDashboard=(progressing)=>{
    this.get(Global.API.ADMIN_DASHBOARD, null, null, response=>{
      if(response.code===200){
        this.setState({
          adminDashboard:response.data
        })

      }else{
        this.errorToast(response.message)
      }
    }, progressing, true)
  }

  updateAnalytic=(message)=>{
    this.fetchAdminDashboard(false, message)
    if(transactionCardRef){
      transactionCardRef.current.updateAnalytic()
    }
    if(categoryCardRef){
      categoryCardRef.current.fetchCategoryAnalytic()
    }
    if(transactionItemCardRef){
      transactionItemCardRef.current.fetchTransactionAnalytic()
    }
    if(transactionAnalyticCardRef){
      transactionAnalyticCardRef.current.fetchTransactionAnalytic()
    }
  }

  render() {

    const primaryColor = getColor('primary');
    const secondaryColor = getColor('secondary');
    let adminDashboard = this.state.adminDashboard
    let transactionStatusId = this.state.transactionStatusId
    return (
      <Page
        className="DashboardPage"
        title="Dashboard"
        breadcrumbs={[{ name: 'Dashboard', active: true }]}
      >
        {super.render()}
        <Row>
          <Col>
            <Button className="float-right" onClick={event=>{
              this.fetchAdminDashboard(true)
            }}>Refresh <MdRefresh/></Button>
          </Col>
        </Row>
        <Row>
          <Col lg={3} md={6} sm={6} xs={12}>
            <NumberWidget
                title="Total User"
                subtitle={"Previous month : "+adminDashboard.previousMonthUser}
                number={adminDashboard.totalUser&&adminDashboard.totalUser.toString()}
                color="secondary"
                progress={{
                  value: adminDashboard.thisMonthUserPercent,
                  label: "This month : "+adminDashboard.thisMonthUser,
                }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <NumberWidget
                title="Total Task"
                subtitle={"Previous month : "+adminDashboard.previousMonthTask}
                number={adminDashboard.totalTask&&adminDashboard.totalTask.toString()}
                color="secondary"
                progress={{
                  value: adminDashboard.thisMonthTaskPercent,
                  label: 'This month : '+adminDashboard.thisMonthTask,
                }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <NumberWidget
                title="Total Task Done"
                subtitle={"Previous month : "+adminDashboard.previousMonthTaskDone}
                number={adminDashboard.totalTaskDone&&adminDashboard.totalTaskDone.toString()}
                color="secondary"
                progress={{
                  value: adminDashboard.thisMonthTaskDonePercent,
                  label: 'This month : '+adminDashboard.thisMonthTask,
                }}
            />
          </Col>

          <Col lg={3} md={6} sm={6} xs={12}>
            <NumberWidget
                title="Total Transfer"
                subtitle={"Previous month : "+adminDashboard.previousMonthPayment}
                number={adminDashboard.totalPayment&&currencyFormat(adminDashboard.totalPayment, CURRENCY_SYMBOL)}
                color="secondary"
                progress={{
                  value: adminDashboard.thisMonthPaymentPercent,
                  label: 'This month : '+currencyFormat(adminDashboard.thisMonthPayment, CURRENCY_SYMBOL),
                }}
            />
          </Col>
        </Row>

        <Row>
          <Col md={7}>
            <DashboardTaskApplicationCard ref={transactionCardRef} {...this.props}/>
          </Col>
          <Col md={5}>
            <DashboardCategoriyAnalytic ref={categoryCardRef} {...this.props}/>
          </Col>
        </Row>
        {/*<Row>*/}
          {/*<Col md={5}>*/}
            {/*<TransactionItemAnalytic ref={transactionItemCardRef} {...this.props}/>*/}
          {/*</Col>*/}
          {/*<Col md={7}>*/}
            {/*<TransactionAnalytic ref={transactionAnalyticCardRef} {...this.props}/>*/}
          {/*</Col>*/}
        {/*</Row>*/}
        <SockJsClient
            url={Global.BASE_URL}
            topics={[Global.API.LISTEN_PAID_TRANSACTION]}
            onMessage={(message) => {
              this.updateAnalytic(message)
            }}
            ref={ (client) => { this.clientRef = client }} />
        <SockJsClient
            url={Global.BASE_URL}
            topics={[Global.API.LISTEN_ORDER_TRANSACTION]}
            onMessage={(message) => {
              this.updateAnalytic(message)
            }}
            ref={ (client) => { this.clientRef = client }} />
      </Page>
    );
  }
}
export default MainDashboardPage;
