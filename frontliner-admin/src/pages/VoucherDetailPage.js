import React from 'react';
import BasePage from "./BasePage";
import { EditorState , convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import SearchInput from '../components/SearchInput'


import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import Page from "../components/Page";
import ItemOption from "../components/Widget/ItemOption";
import Global, {
    ANY,
    ANY_ITEM, DD_MM_YYYY_HH_MM_SS, DD_MMMM_YYYY_HH_MM_SS,
    FREE_SHIPPING,
    PURCHASE_DISCOUNT,
    SHIPPING_DISCOUNT,
    SINGLE_ITEM,
    SPECIFIC
} from "../utils/Global";
import {allIsEmpty, deleteParam, imageSelector, isEmpty, parseDate, sortirMap} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit, MdSearch} from "react-icons/md";
import VoucherItemModal from '../components/modal/VoucherItemModal'
import VoucherItemInventoryModal from '../components/modal/VoucherItemInventoryModal'
import CustomerVoucherModal from '../components/modal/CustomerVoucherModal'
import {IoMdEye, IoMdOpen} from "react-icons/io/index";
import {MdPublish} from "react-icons/md/index";
import moment from "moment";
import {getData} from "../utils/StorageUtil";
import Pagination from '../components/Pagination'
import { SketchPicker } from 'react-color';
import NumberInput from "../components/Widget/NumberInput";
import PriceInput from "../components/Widget/PriceInput";
import PercentInput from "../components/Widget/PercentInput";
import DateInput from "../components/Widget/DateInput";


export default class VoucherDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let state = this.state
        state.voucher={}
        state.categories=[]
        state.voucherIdQuery=queryString.parse(this.props.query).id
        state.category={}
        state.modalUpdateConfirm=false
        state.modalDeleteConfirm=false
        state.voucherItems=[]
        state.voucherItem=null
        state.ascending=true
        state.sortir='created'
        state.modalUpdateItem=false
        state.modalUpdatingLabel=""
        state.edit=false
        state.voucherItemPage = 1
        state.voucherItemTotalPage = 0
        state.voucherItemTotalElement = 0
        state.voucherItemPageElement = 0

        state.voucherCustomers=[]
        state.voucherCustomer=null
        state.voucherCustomerAscending=true
        state.voucherCustomerSortir='created'
        state.voucherCustomerPage = 1
        state.voucherCustomerTotalPage = 0
        state.voucherCustomerTotalElement = 0
        state.voucherCustomerPageElement = 0
        state.editorState = EditorState.createEmpty()
        state.voucherItemSearch = "";
        state.customerSearch = "";
        this.setState(state)
    }

    onEditorStateChange= (editorState) => {
        let voucher = this.state.voucher
        voucher.termAndCondition = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        this.setState({
            voucher:voucher,
            editorState:editorState,
        });
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchVoucherDetail(this.state.voucherIdQuery)
    }

    confirmSave = (callback) =>{
        var voucher = this.state.voucher
        if(allIsEmpty(voucher.name, voucher.description, voucher.ownerType, voucher.code, voucher.maxUsage, voucher.periodType, voucher.benefitType, voucher.themeColor)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            if(voucher.periodType===Global._DYNAMIC){
                if(!voucher.startPeriod || !voucher.endPeriod){
                    this.showDialog("Notice", "Please complete start and end period form fields")
                    return ;
                }
            }
            if(voucher.periodType===Global._STATIC){
                if(isEmpty(voucher.validDay)){
                    this.showDialog("Notice", "Please complete valid day form")
                    return ;
                }
            }
            if(voucher.benefitType!=FREE_SHIPPING){
                if(!voucher.discountType){
                    this.showDialog("Notice", "Please complete discount type")
                    return ;
                }
                if(voucher.discountType){
                    if(voucher.discountType===Global._PERCENTAGE_DISCOUNT){
                        if(!voucher.percent){
                            this.showDialog("Notice", "Please complete percent form field")
                            return ;
                        }
                    }
                    if(voucher.discountType===Global._PRICE_DISCOUNT){
                        if(!voucher.nominal){
                            this.showDialog("Notice", "Please complete nominal form field")
                            return ;
                        }
                    }
                }
            }
            this.openConfirmDialog("Confirmation", "Do you want to update this voucher item ?", ()=>{
                if(callback){
                    callback()
                }else{
                    this.save()
                }
            })
        }
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.voucherItem!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.VOUCHER_ITEM_DELETE+"/"+this.state.voucherItem.id, null, res => {
                    if(res.code===200){
                        this.setState({
                            voucherItem:null
                        }, () => {
                            this.fetchAllVoucherItems()
                        })
                    }else{

                    }
                }, true, true);
            })
        }
    }


    save = () => {
        this.post(Global.API.VOUCHER_SAVE, null, {data:this.state.voucher}, res=>{
            if(res.code===200){
                let voucher = res.data ;
                let editorState = this.state.editorState
                if(voucher.termAndCondition){
                    const termAndCondContentBlock = htmlToDraft(voucher.termAndCondition);
                    if (termAndCondContentBlock) {
                        const contentState = ContentState.createFromBlockArray(termAndCondContentBlock.contentBlocks);
                        editorState = EditorState.createWithContent(contentState);
                    }else{
                        editorState = EditorState.createEmpty()
                    }
                }else{
                    editorState = EditorState.createEmpty()
                }

                this.setState({
                    voucher:voucher,
                    category:res.data.category,
                    image:res.data.imageLink,
                    editorState:editorState
                }, () => {
                    this.fetchAllVoucherItems()
                    this.fetchAllVoucherCustomers()
                    this.successToast(res.message)
                })
            }else{

            }
        },  true, true)
    }


    fetchVoucherDetail = (id) => {
        if(id!=null){
            this.get(Global.API.VOUCHER, {
                params :{
                    id : id
                }
            }, null, res =>{
                if(res.code===200){
                    let voucher = res.data ;
                    let editorState = this.state.editorState
                    if(voucher.termAndCondition){
                        const termAndCondContentBlock = htmlToDraft(voucher.termAndCondition);
                        if (termAndCondContentBlock) {
                            const contentState = ContentState.createFromBlockArray(termAndCondContentBlock.contentBlocks);
                            editorState = EditorState.createWithContent(contentState);
                        }else{
                            editorState = EditorState.createEmpty()
                        }
                    }else{
                        editorState = EditorState.createEmpty()
                    }
                    this.setState({
                        voucher:voucher,
                        category:res.data.category,
                        image:res.data.imageLink,
                        editorState:editorState
                    }, () => {
                        this.fetchAllVoucherItems()
                        this.fetchAllVoucherCustomers()
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }

    fetchAllVoucherItems = () =>{
        this.get(Global.API.VOUCHER_ITEMS, {
            params:{
                voucherId:this.state.voucher.id,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                page:this.state.voucherItemPage-1,
                keyword:this.state.voucherItemSearch
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    voucherItems : response.data,
                    voucherItemTotalPage: response.totalPage,
                    voucherItemTotalElement: response.totalElement,
                    voucherItemPageElement: response.pageElement,

                })
            }

        }, true, true)
    }

    fetchAllVoucherCustomers = () =>{
        this.get(Global.API.VOUCHER_CUSTOMERS, {
            params:{
                voucherId:this.state.voucher.id,
                ascending:this.state.voucherCustomerAscending,
                sortir:this.state.voucherCustomerSortir,
                page:this.state.voucherCustomerPage-1,
                search: this.state.customerSearch
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    voucherCustomers : response.data,
                    voucherCustomerTotalPage: response.totalPage,
                    voucherCustomerTotalElement: response.totalElement,
                    voucherCustomerPageElement: response.pageElement,
                })
            }

        }, true, true)
    }



    closeVoucherModal = () =>{
        this.setState({
            modalUpdateItem:false,
            product:{}
        }, () => {
            this.fetchAllVoucherItems()
        })
    }

    closeDialog = () =>{
        this.setState({
            modalDeleteConfirm:false,
        }, () => {
            this.fetchVoucherDetail(this.state.voucher.id)
        })
    }

    refreshVoucherItem = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAllVoucherItems()
        })
    )

    refreshVoucherCustomer = (ascending, sortir) => (
        this.setState({
            voucherCustomerAscending:ascending,
            voucherCustomerSortir:sortir
        }, () => {
            this.fetchAllVoucherCustomers()
        })
    )


    closeVoucherItemInventoryModal = (voucherItemInventories) =>{
        this.setState({
            modalUpdateItem:this.state.edit,
            voucherItemInventoryModal:false,
        }, () => {
            if(this.state.inventoryCallback){
                this.state.inventoryCallback(voucherItemInventories)
            }
            this.refreshVoucherItem(this.state.ascending, this.state.sortir)
        })
    }


    openVoucherItemInventoryModal  = (voucherItem, product, inventoryCallback) =>{
        this.setState({
            product:product,
            voucherItem:voucherItem,
            inventoryCallback:inventoryCallback,
            voucherItemInventoryModal:true
        })
    }

    activationConfirm = () =>{
        this.openConfirmDialog("Confirmation", !this.state.voucher.active?"Do you want to publish this voucher for your customer?":"Do you want to unpublish this voucher for your customer ?", ()=>{
            this.activate()
        })
    }

    activate = ()=>{
        this.get(Global.API.VOUCHER_ACTIVATE, {
            params:{
                id:this.state.voucher.id,
                active:!this.state.voucher.active
            }
        }, null, response=>{
            this.closeDialog()
        }, true, true)
    }

    generateCode = () =>{
        this.get(Global.API.VOUCHER_GENERATE_CODE, null, null, response=>{
            if(response.code===200){
                let voucher = this.state.voucher ;
                voucher.code = response.data
                this.setState({
                    voucher:voucher
                })
            }else{
                this.errorToast(response.message)
            }
        }, true, true)
    }

    render() {
        var ids = [];
        var labels = [];
        this.state.categories.map((item, index)=>{
            ids.push(item.id);
            labels.push(item.name);
        })

        let configuration = getData(Global.CONFIGURATION)
        if(configuration===null){
            configuration = {}
        }

        let voucher = this.state.voucher?this.state.voucher:{}
        let editorState = this.state.editorState

        let aggregateTypeOptions = []
        if(voucher.benefitType===PURCHASE_DISCOUNT){
            aggregateTypeOptions = Global.VOUCHER_AGGREGATE_OPTIONS
        }else if(voucher.benefitType===SHIPPING_DISCOUNT || voucher.benefitType===FREE_SHIPPING){
            aggregateTypeOptions = Global.VOUCHER_AGGREGATE_OPTIONS_ANY
        }

        return (
            <Page
                title="Voucher Form"
                breadcrumbs={[{ name: 'voucher form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this voucher item ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <VoucherItemModal
                    showing={this.state.modalUpdateItem}
                    edit={this.state.edit}
                    voucher={this.state.voucher}
                    close={this.closeVoucherModal}
                    openVoucherItemInventoryModal={this.openVoucherItemInventoryModal}
                    refresh={this.refresh}
                />
                <VoucherItemInventoryModal
                    showing={this.state.voucherItemInventoryModal}
                    edit={this.state.edit}
                    voucherItem={this.state.voucherItem}
                    product={this.state.product}
                    voucher={this.state.voucher}
                    close={this.closeVoucherItemInventoryModal}
                    refresh={this.refresh}
                />
                <CustomerVoucherModal
                    showing={this.state.customerVoucherModal}
                    close={()=>{
                        this.setState({
                            customerVoucherModal:false
                        })
                        this.fetchAllVoucherCustomers()
                    }}
                    voucher={this.state.voucher}
                />
                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Voucher</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={4}>
                                        <Card>
                                            <CardHeader>Theme Color</CardHeader>
                                            <CardBody>
                                                <SketchPicker
                                                    width={""}
                                                    color={!isEmpty(voucher.themeColor)?voucher.themeColor:""}
                                                    onChangeComplete={ (color)=>{
                                                        if(voucher){
                                                            voucher.themeColor = color.hex
                                                            this.setState({
                                                                voucher:voucher
                                                            })
                                                        }
                                                    } }
                                                />
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={8}>
                                        <Row>
                                            <Col>
                                                <FormGroup>
                                                    <Label for="name">Name</Label>
                                                    <Input
                                                        type="text"
                                                        name="name"
                                                        value={(voucher!=null && voucher.name)?voucher.name:""}
                                                        onChange={(e) =>{
                                                            e.preventDefault()
                                                            if(voucher!=null){
                                                                voucher.name = e.target.value
                                                                this.setState({
                                                                    voucher:voucher
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter voucher name"
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label for="description">Description</Label>
                                                    <Input
                                                        type="textarea"
                                                        name="description"
                                                        rows="5"
                                                        value={(voucher!=null)?voucher.description:""}
                                                        onChange={(e) =>{
                                                            if(voucher!=null){
                                                                voucher.description = e.target.value
                                                                this.setState({
                                                                    voucher:voucher
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter description"
                                                    />
                                                </FormGroup>
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <ItemOption
                                                                title={"Select Owner Type"}
                                                                objects={Global.VOUCHER_OWNER_TYPES}
                                                                default={voucher!=null?voucher.ownerType:null}
                                                                callback={(aggregate)=>{
                                                                    if(voucher!=null){
                                                                        voucher.ownerType = aggregate.id
                                                                        if(voucher.ownerType===ANY){
                                                                            voucher.pointExchange = 0
                                                                        }
                                                                        this.setState({
                                                                            voucher:voucher
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                {
                                                    voucher.ownerType===SPECIFIC&&(
                                                        <Row>
                                                            <Col>
                                                                <FormGroup>
                                                                    <Label for="pointExchange">Point Exchange</Label>
                                                                    <NumberInput
                                                                        name="pointExchange"
                                                                        className="form-control"
                                                                        value={!isEmpty(voucher)?voucher.pointExchange:""}
                                                                        onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                                            e.preventDefault()
                                                                            voucher.pointExchange = floatvalue
                                                                            this.setState({
                                                                                voucher: voucher
                                                                            })
                                                                        }}
                                                                        placeholder="0"
                                                                        maxLength={15}>
                                                                    </NumberInput>
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>
                                                    )
                                                }
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="maxUsage">Max Usage</Label>
                                                            <NumberInput
                                                                name="maxUsage"
                                                                className="form-control"
                                                                value={!isEmpty(voucher)?voucher.maxUsage:""}
                                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                                    e.preventDefault()
                                                                    voucher.maxUsage = floatvalue
                                                                    this.setState({
                                                                        voucher: voucher
                                                                    })
                                                                }}
                                                                placeholder="0"
                                                                maxLength={15}>
                                                            </NumberInput>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="currentUsage">Current Usage</Label>
                                                            <NumberInput
                                                                name="currentUsage"
                                                                className="form-control"
                                                                value={!isEmpty(voucher)?voucher.currentUsage:""}
                                                                allowNegative={false}
                                                                placeholder="0"
                                                                maxLength={2}
                                                                disabled={true}>
                                                            </NumberInput>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="code">Code</Label>
                                                            <Row>
                                                                <Col md={10}>
                                                                    <Input
                                                                        type="text"
                                                                        name="prefixCode"
                                                                        value={(voucher!=null && voucher.code)?voucher.code:""}
                                                                        maxLength={configuration.voucherCodeLength}
                                                                        onChange={(e) =>{
                                                                            if(voucher!=null){
                                                                                voucher.code = e.target.value.replace(/[\W_]+/g,"").toUpperCase()
                                                                                this.setState({
                                                                                    voucher:voucher
                                                                                })
                                                                            }
                                                                        }}
                                                                        placeholder="Enter Voucher Code "
                                                                    />
                                                                </Col>
                                                                <Col md={2}>
                                                                    <Button className="float-right" onClick={event=>{
                                                                        this.generateCode()
                                                                    }}>Generate</Button>
                                                                </Col>
                                                            </Row>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Period"}
                                                        objects={[{id:"all_time",name:"All Time"},{id:"dynamic",name:"Dynamic"},{id:"static",name:"Static"}]}
                                                        default={voucher!=null?voucher.periodType:null}
                                                        callback={(periodType)=>{
                                                            if(voucher!=null){
                                                                voucher.periodType = periodType.id
                                                                this.setState({
                                                                    voucher:voucher
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                {
                                                    (voucher!=null&&voucher.periodType &&voucher.periodType===Global._DYNAMIC)?(
                                                        <Row>
                                                            <Col>
                                                                <Label for="startPeriod">Start Period</Label>
                                                                <DateInput
                                                                    id="startPeriod"
                                                                    maxdate={voucher.endPeriod}
                                                                    mindate={new Date()}
                                                                    value={voucher.startPeriod}
                                                                    onChange={(e) =>{
                                                                        let value = e.target.value
                                                                        voucher.startPeriod = value
                                                                        this.setState({
                                                                            voucher : voucher
                                                                        })
                                                                    }}
                                                                    placeholder="Enter start period">
                                                                </DateInput>
                                                            </Col>
                                                            <Col>
                                                                <Label for="endPeriod">End Period</Label>
                                                                <DateInput
                                                                    id="endPeriod"
                                                                    maxdate={null}
                                                                    mindate={voucher.startPeriod}
                                                                    value={voucher.endPeriod}
                                                                    onChange={(e) =>{
                                                                        let value = e.target.value
                                                                        voucher.endPeriod = value
                                                                        this.setState({
                                                                            voucher : voucher
                                                                        })
                                                                    }}
                                                                    placeholder="Enter end period">
                                                                </DateInput>
                                                            </Col>
                                                        </Row>
                                                    ):(null)
                                                }
                                                {
                                                    (voucher!=null&&voucher.periodType &&voucher.periodType===Global._STATIC)?(
                                                        <Row>
                                                            <Col>
                                                                <Label for="validDay">Valid Day</Label>
                                                                <Input
                                                                    id="validDay"
                                                                    type="text"
                                                                    name="validDay"
                                                                    value={voucher.validDay}
                                                                    onChange={(e) =>{
                                                                        let value = e.target.value.replace(/\D/,'')
                                                                        voucher.validDay = value
                                                                        this.setState({
                                                                            voucher : voucher
                                                                        })
                                                                    }}
                                                                    placeholder="0"
                                                                />
                                                            </Col>
                                                            <Col>
                                                            </Col>
                                                        </Row>
                                                    ):(null)

                                                }
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Benefit"}
                                                        objects={Global.VOUCHER_BENEFIT_TYPE_OPTIONS}
                                                        default={voucher!=null?voucher.benefitType:null}
                                                        callback={(benefitType)=>{
                                                            if(voucher!=null){
                                                                voucher.benefitType = benefitType.id
                                                                if(benefitType.id===FREE_SHIPPING){
                                                                    voucher.discountType = null
                                                                }
                                                                this.setState({
                                                                    voucher:voucher
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                {
                                                    (voucher!=null&&voucher.benefitType
                                                        && (voucher.benefitType===PURCHASE_DISCOUNT || voucher.benefitType===SHIPPING_DISCOUNT ))?(
                                                        <FormGroup>
                                                            <ItemOption
                                                                title={"Select Discount Type"}
                                                                objects={
                                                                    [
                                                                        {id:Global._PERCENTAGE_DISCOUNT,name:"Percentage Discount"},
                                                                        {id:Global._PRICE_DISCOUNT,name:"Price Discount"}
                                                                    ]
                                                                }
                                                                default={voucher!=null?voucher.discountType:null}
                                                                callback={(periodType)=>{
                                                                    if(voucher!=null){
                                                                        voucher.discountType = periodType.id
                                                                        if(voucher.discountType===Global._PERCENTAGE_DISCOUNT){
                                                                            voucher.nominal = 0
                                                                        }else if(voucher.discountType===Global._PRICE_DISCOUNT){
                                                                            voucher.percent = 0
                                                                        }
                                                                        this.setState({
                                                                            voucher:voucher
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </FormGroup>
                                                    ):(null)
                                                }
                                                {
                                                    (voucher != null && voucher.discountType
                                                        && (voucher.discountType === Global._PERCENTAGE_DISCOUNT)) ? (
                                                        <FormGroup>
                                                            <PercentInput
                                                                name="percent"
                                                                className="form-control"
                                                                value={voucher.percent}
                                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                                    e.preventDefault()
                                                                    if (floatvalue > 100.0) {
                                                                        floatvalue = floatvalue % 100
                                                                    }
                                                                    voucher.percent = floatvalue
                                                                    this.setState({
                                                                        voucher: voucher
                                                                    })
                                                                }}
                                                                allowNegative={false}
                                                                placeholder="0">
                                                            </PercentInput>
                                                        </FormGroup>
                                                    ) : (null)
                                                }
                                                {
                                                    (voucher != null && voucher.discountType
                                                        && voucher.discountType === Global._PRICE_DISCOUNT) ? (
                                                        <FormGroup>
                                                            <PriceInput
                                                                name="price"
                                                                className="form-control"
                                                                value={voucher.nominal}
                                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                                    e.preventDefault()
                                                                    voucher.nominal = floatvalue
                                                                    this.setState({
                                                                        voucher: voucher
                                                                    })
                                                                }}
                                                                allowNegative={false}
                                                                placeholder="0">
                                                            </PriceInput>
                                                        </FormGroup>
                                                    ) : (null)
                                                }
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Aggregate Type"}
                                                        objects={aggregateTypeOptions}
                                                        default={voucher!=null?voucher.byAggregate:null}
                                                        callback={(aggregate)=>{
                                                            if(voucher!=null){
                                                                voucher.byAggregate = aggregate.id
                                                                this.setState({
                                                                    voucher:voucher
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={12}>
                                                <Card>
                                                    <CardHeader>Term and condition</CardHeader>
                                                    <CardBody>
                                                        <Editor
                                                            editorState={editorState}
                                                            onEditorStateChange={this.onEditorStateChange}/>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <ButtonGroup className="float-left">
                                            <Button color="danger" onClick={e => (
                                                this.setState({
                                                    voucher:null
                                                }, () => {
                                                    this.props.history.goBack();
                                                })
                                            )}> Back </Button>
                                        </ButtonGroup>
                                    </Col>
                                    <Col>
                                        <ButtonGroup className="float-right">
                                            <Button color="primary" onClick={e=>{
                                                e.preventDefault()
                                                this.confirmSave()
                                            }}> {
                                                (this.state.voucher!=null && this.state.voucher.id!=null)?"Update":"Save"
                                            } </Button>
                                            &nbsp;&nbsp;&nbsp;
                                            <Button color={this.state.voucher&&this.state.voucher.active?"danger":"warning"} onClick={e=>{
                                                e.preventDefault()
                                                this.activationConfirm()
                                            }}><MdPublish/>&nbsp;{this.state.voucher&&this.state.voucher.active?"Unpublish":"Publish"}</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {
                    (this.state.voucher!=null && this.state.voucher.id!=null && this.state.voucher.byAggregate && this.state.voucher.byAggregate!=ANY_ITEM)&&
                        <Row key={2}>
                        <Col>
                            <Card className="mb-6">
                                <CardHeader>Voucher Item</CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col md={4}>
                                                    Sort By :
                                                    <UncontrolledButtonDropdown key={1}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                sortirMap(this.state.sortir)
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(this.state.ascending, "created"))}>Created</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(this.state.ascending, "availableStock"))}>Stock</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(this.state.ascending, "minimumQuantity"))}>Min Quantity</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(this.state.ascending, "updated"))}>Updated</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                                <Col md={4}>
                                                    Sortir :
                                                    <UncontrolledButtonDropdown key={2}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                this.state.ascending?"Ascending":"Descending"
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(true, this.state.sortir))}>Ascending</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshVoucherItem(false, this.state.sortir))}>Descending</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={3}>
                                            <SearchInput
                                                value={this.state.voucherItemSearch}
                                                onChange={e=>{
                                                    this.setState({
                                                        voucherItemSearch:e.target.value
                                                    }, () => {
                                                        if(this.state.voucherItemSearch===''){
                                                            this.fetchAllVoucherItems()
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.voucherItemSearch!==''){
                                                        this.fetchAllVoucherItems()
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <ButtonGroup className="float-right">
                                                <Button color="primary" onClick={e=>{
                                                    this.confirmSave(()=>{
                                                        this.post(Global.API.VOUCHER_SAVE, null, {data:this.state.voucher}, res=>{
                                                            if(res.code===200){
                                                                let voucher = res.data ;
                                                                this.setState({
                                                                    voucher:voucher,
                                                                    edit:true,
                                                                    modalUpdateItem : true
                                                                })
                                                            }
                                                        }, true, true)
                                                    })
                                                }}>Update Item</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Table hover>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Available Stock</th>
                                                <th>Minimum Quantity</th>
                                                <th>Created</th>
                                                <th>Updated</th>
                                                <th>Edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.voucherItems.map((item, index)=>(
                                                    <tr key={index}>
                                                        <th scope="row">{((this.state.voucherItemPage-1)* this.state.voucherItemPageElement)+(++index)}</th>
                                                        <td>
                                                            {item.product&&item.product.name}
                                                            &nbsp;&nbsp;
                                                            <IoMdOpen
                                                                color="blue" style={{cursor:'pointer'}}
                                                                onClick={e=>{
                                                                    this.props.history.push('/productDetail?id='+item.product.id)
                                                                }
                                                                }/>
                                                        </td>
                                                        <td>{item.product&&item.product.category?item.product.category.name:"-"}</td>
                                                        <td>{item.availableStock}</td>
                                                        <td>{item.minimumQuantity}</td>
                                                        <td>{parseDate(item.created, DD_MMMM_YYYY_HH_MM_SS)}</td>
                                                        <td>{parseDate(item.updated, DD_MMMM_YYYY_HH_MM_SS)}</td>
                                                        <td>
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.setState({
                                                                    edit:false
                                                                }, () => {
                                                                    this.openVoucherItemInventoryModal(item, item.product)
                                                                })
                                                            }}>
                                                            <MdEdit/>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </CardBody>
                                <Pagination {...this.props} currentPage={this.state.voucherItemPage} pageCount={this.state.voucherItemTotalPage} callback={(page)=>{
                                        this.setState({
                                            voucherItemPage:page
                                        }, () => {
                                            this.fetchAllVoucherItems()
                                        })
                                    }
                                }/>

                            </Card>
                        </Col>
                        </Row>

                }
                {
                    (this.state.voucher!=null && this.state.voucher.id!=null && this.state.voucher.ownerType===SPECIFIC)&&(
                        <Row>
                            <Col>
                                <Card className="mb-6">
                                    <CardHeader>Voucher Customer</CardHeader>
                                    <CardBody>
                                        <Row>
                                            <Col md={6}>
                                                <Row>
                                                    <Col md={4}>
                                                        Sort By :
                                                        <UncontrolledButtonDropdown key={1}>
                                                            <DropdownToggle
                                                                caret
                                                                color="white"
                                                                className="text-capitalize m-1">
                                                                {
                                                                    sortirMap(this.state.voucherCustomerSortir)
                                                                }
                                                            </DropdownToggle>
                                                            <DropdownMenu>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(this.state.voucherCustomerAscending, "created"))}>Created</DropdownItem>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(this.state.voucherCustomerAscending, "customer.firstname"))}>Name</DropdownItem>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(this.state.voucherCustomerAscending, "customer.email"))}>Email</DropdownItem>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(this.state.voucherCustomerAscending, "customer.phoneNumber"))}>Phone Number</DropdownItem>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(this.state.voucherCustomerAscending, "updated"))}>Last Updated</DropdownItem>
                                                            </DropdownMenu>
                                                        </UncontrolledButtonDropdown>
                                                    </Col>
                                                    <Col md={4}>
                                                        Sortir :
                                                        <UncontrolledButtonDropdown key={2}>
                                                            <DropdownToggle
                                                                caret
                                                                color="white"
                                                                className="text-capitalize m-1">
                                                                {
                                                                    this.state.voucherCustomerAscending?"Ascending":"Descending"
                                                                }
                                                            </DropdownToggle>
                                                            <DropdownMenu>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(true, this.state.voucherCustomerSortir))}>Ascending</DropdownItem>
                                                                <DropdownItem onClick={e=>(this.refreshVoucherCustomer(false, this.state.voucherCustomerSortir))}>Descending</DropdownItem>
                                                            </DropdownMenu>
                                                        </UncontrolledButtonDropdown>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col md={3}>
                                                <SearchInput
                                                    value={this.state.customerSearch}
                                                    onChange={e=>{
                                                        this.setState({
                                                            customerSearch:e.target.value
                                                        }, () => {
                                                            if(this.state.customerSearch===''){
                                                                this.fetchAllVoucherCustomers()
                                                            }
                                                        })
                                                    }}
                                                    onEnter={e=>{
                                                        if(this.state.customerSearch!==''){
                                                            this.fetchAllVoucherCustomers()
                                                        }
                                                    }}
                                                />
                                            </Col>
                                            <Col md={3}>
                                                <ButtonGroup className="float-right">
                                                    <Button color="primary" onClick={e=>{
                                                        this.confirmSave(()=>{
                                                            this.post(Global.API.VOUCHER_SAVE, null, {data:this.state.voucher}, res=>{
                                                                if(res.code===200){
                                                                    let voucher = res.data ;
                                                                    this.setState({
                                                                        voucher:voucher,
                                                                        edit:true,
                                                                        customerVoucherModal : true
                                                                    })
                                                                }
                                                            }, true, true)
                                                        })
                                                    }}>Update Customer</Button>
                                                </ButtonGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Table hover>
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Last Update</th>
                                                    <th>Remove</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.voucherCustomers.map((item, index)=>{
                                                        return (
                                                            <tr key={item.id}>
                                                                <th scope="row">{((this.state.voucherCustomerPage-1)* this.state.voucherCustomerPageElement)+(++index)}</th>
                                                                <td>
                                                                    {item.customer.firstname+" "+item.customer.lastname}
                                                                    &nbsp;&nbsp;
                                                                    <IoMdOpen
                                                                        color="blue" style={{cursor:'pointer'}}
                                                                        onClick={e=>{
                                                                            this.props.history.push('/customerDetail?id='+item.customer.id)
                                                                        }
                                                                        }/>
                                                                </td>
                                                                <td>{item.customer.email}</td>
                                                                <td>{item.customer.phoneNumber}</td>
                                                                <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                                <td>
                                                                    <Button size="sm" color="danger" onClick={e => {
                                                                        e.preventDefault()
                                                                        this.openConfirmDialog("Confirmation", "Do you want to delete this voucher for it owner ?", ()=>{
                                                                            this.get(Global.API.VOUCHER_CUSTOMER_DELETE, {
                                                                                params:{
                                                                                    id:item.id
                                                                                }
                                                                            }, null, response=>{
                                                                                if(response.code===200){
                                                                                    this.successToast(response.message)
                                                                                    this.fetchAllVoucherCustomers()
                                                                                }else{
                                                                                    this.errorToast(response.message)
                                                                                }
                                                                            }, true, true)
                                                                        })
                                                                    }}>
                                                                        <MdDelete/>
                                                                    </Button>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>
                                            </Table>
                                        </Row>
                                    </CardBody>
                                    <Pagination {...this.props} currentPage={this.state.voucherCustomerPage} pageCount={this.state.voucherCustomerTotalPage} callback={(page)=>{
                                        this.setState({
                                            voucherCustomerPage:page
                                        }, () => {
                                            this.fetchAllVoucherCustomers()
                                        })
                                    }
                                    }/>

                                </Card>
                            </Col>
                        </Row>
                    )
                }
            </Page>
        );
    }


}