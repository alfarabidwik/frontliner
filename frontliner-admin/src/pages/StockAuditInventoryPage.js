import BasePage from "./BasePage";
import React from 'react'
import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {currencyFormat, deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";

import PropTypes from 'prop-types'
import queryString from "query-string";
import Pagination from '../components/Pagination'
import {MdDelete, MdEdit} from "react-icons/md";
import {IoMdEye, IoMdOpen} from "react-icons/io";


export default class StockAuditInventoryPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:true,
            sortir:'created',
            inventories:[],
            search:"",
            show:this.props.show,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.state.show){
            this.fetchTransaction()
        }
    }


    componentWillReceiveProps(props, nextContext) {
        let show = this.state.show
        let currentPage = this.state.page
        let propsPage = queryString.parse(props.query).page?queryString.parse(props.query).page:1
        if((show!=props.show && props.show===true)||currentPage!=propsPage){
            this.setState({
                show:props.show,
                page:propsPage,
                totalPage:0,
                totalElement:0,
                pageElement:0,
            }, () => {
                this.fetchTransaction(true)
            })
        }else{
            this.setState({
                show:props.show
            })
        }
    }


    fetchTransaction = () =>{
        this.get(Global.API.INVENTORIES, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    inventories : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,

                })
            }
        }, true, true)
    }

    refreshTransaction = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
            this.fetchTransaction()
        })
    )


    render() {
        return this.renderTransaction()
    }

    renderTransaction = () =>{
        return (
            <Card className="border-top-0">
                {super.render()}
                <CardBody>
                    <Row>
                        <Col md={3}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir.toString())
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "product.name"))}>Product Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "product.category.name"))}>Category Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "created"))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "quantity"))}>Quantity</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "weight"))}>Weight</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "price"))}>Price</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "updated"))}>Last Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(true, this.state.sortir))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshTransaction(false, this.state.sortir))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            <SearchInput
                                placeholder={"Search inventory, heading, category..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            this.setState({
                                                show:false
                                            }, () => {
                                                deleteParam(this.props, 'page')
                                            })
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        this.setState({
                                            show:false
                                        }, () => {
                                            deleteParam(this.props, 'page')
                                        })
                                    }
                                }}
                            />
                        </Col>
                        <Col md={3}>
                        </Col>
                    </Row>
                    <Row>
                        <Table hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Heading</th>
                                <th>Category</th>
                                <th>Type</th>
                                <th>Size</th>
                                <th>Color</th>
                                <th>Quantity</th>
                                <th>Price @item</th>
                                <th>Image</th>
                                <th>Last Update</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.inventories.map((item, index)=>(
                                    <tr key={index}>
                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                        <td>{item.product.name}</td>
                                        <td>{item.product.heading?item.product.heading.name:"-"}</td>
                                        <td>{item.product.category?item.product.category.name:"-"}</td>
                                        <td>{item.typeName}</td>
                                        <td>{item.size?item.size.name:"-"}</td>
                                        <td>
                                            {
                                                item.colors.map((color, index1)=>(
                                                    color!=null?
                                                        <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                        :
                                                        null
                                                ))
                                            }
                                        </td>
                                        <td><Button onClick={event=>{
                                            event.preventDefault()
                                            this.setState({
                                                inventory:item
                                            }, () => {
                                                this.openQuantityModal()
                                            })
                                        }}><MdEdit/></Button>&nbsp;{item.quantity}</td>
                                        <td>{currencyFormat(item.price, CURRENCY_SYMBOL)}</td>
                                        <td width="5%">
                                            <Button onClick={event=>{
                                                event.preventDefault()
                                                this.openLightBox(item.inventoryImages)
                                            }}><IoMdEye/></Button>
                                        </td>
                                        <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                        <td>
                                            <IoMdOpen
                                                color="green"
                                                style={{cursor:'pointer'}}
                                                onClick={e=>{
                                                    this.props.history.push('/inventoryDetail?productId='+item.product.id+'&inventoryId='+item.id)
                                                }}
                                            />
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                    </Row>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }


    renderTransactionTable = () =>{
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Ref Code</th>
                    <th>Customer</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Last Update</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.inventories.map((item, index)=>(null))
                }
                </tbody>
            </Table>
        )
    }


}

StockAuditInventoryPage.propTypes = {
    show:PropTypes.bool.isRequired
}