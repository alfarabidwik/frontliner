import Page from 'components/Page';
import React from 'react';
import logoImage from 'assets/img/logo/combine.png';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg, CardTitle,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {
  allIsEmpty, cloneObject,
  decimalToFraction,
  deleteParam,
  imageSelector,
  isEmpty,
  parseDate,
  translate
} from "../utils/Utilities";
import {MdAlarm, MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import ActiveLabel from "../components/Widget/ActiveLabel";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class BankAccountPage extends BasePage{

  constructor(props) {
    super(props);
    this.state = {
      bankAccounts : [],
      bankAccount:{},
      banks:[],
      bank:{},
      modalAddConfirm:false,
      modalPublishConfirm:false,
      modal: false,
      ascending:true,
      sortir:'created',
      thumbnail:null,
      image:null,

    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
    this.fetchBanks()
  }

  confirmSave = () =>{
    var bankAccount = this.state.bankAccount
    if(isEmpty(bankAccount.customerName) || isEmpty(bankAccount.accountCode) || isEmpty(bankAccount.mobileAppsCellNumber)
        || isEmpty(bankAccount.smsBankingSenderId) || isEmpty(bankAccount.smsBankingTemplate) || isEmpty(bankAccount.currencyFormat) || isEmpty(this.state.bank)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      let bankAccount = this.state.bankAccount
      bankAccount.bank = this.state.bank
      this.setState({
        modalAddConfirm:true,
        bankAccount:bankAccount
      })
    }
  }

  save = () => {
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.BANK_ACCOUNT_SAVE, this.state.bankAccount, null, res => {
        if(res.code===200){
          this.setState({
            bankAccount:{},
            bank:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.BANK_ACCOUNT_DELETE+"/"+this.state.bankAccount.id, null, null, res => {
        if(res.code===200){
          this.setState({
            bankAccount:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }


  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.BANK_ACCOUNTS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        let bankAccounts = response.data
        this.setState({
          bankAccounts : bankAccounts
        }, () => {
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  fetchBanks = () => {
    this.get(Global.API.BANKS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          banks : response.data
        })
      }
    }, true, true);
  }


  render() {
    let bankAccount = this.state.bankAccount
    let banks = this.state.banks
    let bank = this.state.bank

    return (
        <Page
            title="Bank Account Detail"
            breadcrumbs={[{ name: 'Bank Account detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this Bank Account item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this bank item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Bank Account Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col>
                      <Card body>
                        <Row>
                          <Col>
                            <FormGroup>
                              <Label for="customerName">Customer Name</Label>
                              <Input
                                  type="text"
                                  name="customerName"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.customerName)?bankAccount.customerName:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.customerName = e.target.value
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Enter Bank Account Customer Name"
                              />
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <Label for="accountCode">Account Number</Label>
                              <Input
                                  type="text"
                                  name="accountCode"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.accountCode)?bankAccount.accountCode:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.accountCode = e.target.value
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Enter Account Number"
                              />
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <ItemOption
                                  title={"Select Bank"}
                                  objects={banks}
                                  default={bank!=null?bank.id:null}
                                  callback={(bank)=>{
                                    if(bank!=null){
                                      this.setState({
                                        bank:bank
                                      })
                                    }else{
                                      this.setState({
                                        bank:{}
                                      })
                                    }
                                  }}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <FormGroup>
                              <Label for="mobileAppsCellNumber">Mobile Apps Cell Number</Label>
                              <Input
                                  type="text"
                                  name="mobileAppsCellNumber"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.mobileAppsCellNumber)?bankAccount.mobileAppsCellNumber:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.mobileAppsCellNumber = e.target.value.replace(/\D/,'')
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Enter Mobile Apps Cell Number"/>
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <Label for="smsBankingSenderId">SMS Banking Sender Id</Label>
                              <Input
                                  type="text"
                                  name="smsBankingSenderId"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.smsBankingSenderId)?bankAccount.smsBankingSenderId:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.smsBankingSenderId = e.target.value.replace(/[^\w\s]/gi, '').toUpperCase()
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Enter SMS Banking Sender Id"/>
                            </FormGroup>
                          </Col>
                          <Col>
                            <FormGroup>
                              <Label for="smsPrefixIndicator">SMS Prefix Indicator</Label>
                              <Input
                                  type="text"
                                  name="smsPrefixIndicator"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.smsPrefixIndicator)?bankAccount.smsPrefixIndicator:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.smsPrefixIndicator = e.target.value
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Enter SMS Prefix Indicator"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={4}>
                            <FormGroup>
                              <Label for="smsBankingTemplate">SMS Banking Template</Label>
                              <Input
                                  type="textarea"
                                  rows="5"
                                  name="smsBankingTemplate"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.smsBankingTemplate)?bankAccount.smsBankingTemplate:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.smsBankingTemplate = e.target.value
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="SMS Banking Template"/>
                            </FormGroup>
                          </Col>
                          <Col md={4}>
                            <FormGroup>
                              <Label for="currencyFormat">Currency Format</Label>
                              <Input
                                  type="text"
                                  name="currencyFormat"
                                  value={!isEmpty(bankAccount)&&!isEmpty(bankAccount.currencyFormat)?bankAccount.currencyFormat:""}
                                  onChange={(e) =>{
                                    if(bankAccount!=null){
                                      bankAccount.currencyFormat = e.target.value
                                      this.setState({
                                        bankAccount:bankAccount
                                      })
                                    }
                                  }}
                                  placeholder="Example : ##.###,##"/>
                            </FormGroup>
                          </Col>
                          <Col  md={4}>
                            <ActiveOption
                                callback={(active)=>{
                                  let bankAccount = this.state.bankAccount
                                  if(bankAccount!=null){
                                    bankAccount.active = active
                                    this.setState({
                                      bankAccount:bankAccount
                                    })
                                  }

                                }}
                                default={this.state.bankAccount?this.state.bankAccount.active:false}
                            />
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    bankAccount:{},
                                    bank:{}
                                  })
                              )}>Cancel</Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (bankAccount!=null && bankAccount.id!=null)?"Update":"Add"
                              } </Button>
                            </ButtonGroup>
                          </Col>
                        </Row>

                      </Card>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Bank</th>
                                <th>Customer Name</th>
                                <th>Account Code</th>
                                <th>Mobile Apps Cell Number</th>
                                <th>Currency Format</th>
                                <th>Active</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.bankAccounts.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.bank.name}</td>
                                      <td>{item.customerName}</td>
                                      <td>{item.accountCode}</td>
                                      <td>{item.mobileAppsCellNumber}</td>
                                      <td>{item.currencyFormat}</td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            bankAccount:cloneObject(item),
                                            bank:cloneObject(item.bank),
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="secondary" onClick={e=>(
                                            this.setState({
                                              bankAccount:cloneObject(item),
                                              bank:cloneObject(item.bank),
                                              thumbnail:cloneObject(item.thumbnail?item.thumbnailLink:null),
                                              image:cloneObject(item.image?item.imageLink:null),
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
