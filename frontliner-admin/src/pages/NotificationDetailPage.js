import Page from 'components/Page';
import React from 'react';
import {Button, ButtonGroup, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Table} from 'reactstrap';
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import Global, {NOTIFICATION_TOPICS} from "../utils/Global";
import {allIsEmpty, isEmpty} from "../utils/Utilities";
import {MdDelete} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {NotificationDto} from "../model/model";
import ItemOption from "../components/Widget/ItemOption";
import UserSelectionModal from '../components/modal/UserSelectionModal'
import WarningLabel from "../components/Widget/WarningLabel";
import queryString from "query-string";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class NotificationDetailPage extends BasePage{

  users = []
  notification = new NotificationDto()


  constructor(props) {
    super(props);
    this.state = {
      notification : this.notification,
      notificationIdQuery : queryString.parse(this.props.query).id,
      users : this.users,
      notificationTopic : NOTIFICATION_TOPICS[0],
      modalAddConfirm:false,
      image : null,
      modal: false,
      ascending:true,
      sortir:'created',
    }
  }

  componentDidMount() {
    super.componentDidMount();
    if(this.state.notificationIdQuery){
      this.fetchNotificationDetail()
    }
  }

  fetchNotificationDetail(){
    this.get(Global.API.NOTIFICATION+"/"+this.state.notificationIdQuery, null, null, (response)=>{
      if(response.code===200){
        let users = this.state.users
        let notification = response.data
        if(notification.data){
          notification.addressLink = notification.data.addressLink
        }
        let selectedUser = new Object()
        let notificationUsers = notification.notificationUsers?notification.notificationUsers:[]
        if(notificationUsers){
          users.splice(0, users.length)
          notificationUsers.forEach(notificationUser => {
            users.push(notificationUser.user)
            selectedUser[notificationUser.user.id] = notificationUser.user
          })
        }
        let notificationTopic = null
        NOTIFICATION_TOPICS.forEach(value =>{
          if(value.name===notification.topic){
            notificationTopic = value ;
          }
        })
        this.setState({
          notification : notification,
          users:users,
          selectedUser:selectedUser,
          notificationTopic:notificationTopic
        })
      }
    }, true, true)
  }


  sendNotification = () => {
    let notification = this.state.notification
    let users = this.state.users

    let notificationTopic = this.state.notificationTopic
    notification.topic = notificationTopic.name
    var formData = new FormData();

    if(allIsEmpty(notification.title, notification.message)){
      return;
    }
    if(notification.topic===NOTIFICATION_TOPICS[3].name){
      if(!users || users.length<=0){
        this.showDialog("Warning", "Please add at least single receiver")
        return;
      }
      let userIds = []
      users.forEach(value => {
        userIds.push(value.id)
      })
      formData.append("userIds", userIds)
    }
    if(notification.topic===NOTIFICATION_TOPICS[0].name || notification.topic===NOTIFICATION_TOPICS[1].name){
      if(isEmpty(notification.addressLink)){
        return ;
      }
      formData.append("addressLink", notification.addressLink)
    }


    formData.append("topic", notification.topic)
    formData.append("title", notification.title)
    formData.append("message", notification.message)
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.NOTIFICATION_SEND, formData, null, res=>{
        if(res.code===200){
          this.setState({
            notification:res.data
          }, () => {
            this.showDialog("Success", res.message)
          })
        }
      },  true, true)
    })
  }


  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
    })
  }


  render() {

    let users = this.state.users
    let notification = this.state.notification
    let notificationTopic = this.state.notificationTopic

    let selectedUser = new Object()
    users.forEach(value => {
      selectedUser[value.id] = value
    })

    return (
        <Page
            title="Notification Detail"
            breadcrumbs={[{ name: 'task category detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Sending Confirmation"
              message="Do you want to send this notification ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>

          <UserSelectionModal
              showing={this.state.userSelectionModalMode}
              selectedUser={selectedUser}
              okCallback={(selectedUser)=>{
                users.splice(0, users.length)
                Object.keys(selectedUser).forEach(value => {
                  users.push(selectedUser[value])
                })
                this.setState({
                  users:users,
                  selectedUser:selectedUser
                })
              }}
              cancelCallback={()=>{
                this.setState({userSelectionModalMode:false})
              }}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Notification Detail</CardHeader>
                <CardBody>
                  <Row>
                    <Col>
                      <Card body>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="title">Title</Label>
                              <Input
                                  type="text"
                                  name="title"
                                  value={notification.title}
                                  onChange={(e) =>{
                                    if(notification!=null){
                                      notification.title = e.target.value
                                      this.setState({
                                        notification:notification
                                      })
                                    }
                                  }}
                                 placeholder="Enter title"
                              />
                              <WarningLabel show={(isEmpty(notification.title))} message={"Enter title"} />
                            </FormGroup>
                            <FormGroup>
                              <Label for="message">Message</Label>
                              <Input
                                  type="text"
                                  name="message"
                                  value={notification.message}
                                  onChange={(e) =>{
                                    if(notification!=null){
                                      notification.message = e.target.value
                                      this.setState({
                                        notification:notification
                                      })
                                    }
                                  }}
                                  placeholder="Enter message"
                              />
                              <WarningLabel show={(isEmpty(notification.message))} message={"Enter message"} />
                            </FormGroup>
                            <FormGroup>
                              <ItemOption
                                  title={"Select Topic"}
                                  hideOptionLabel={true}
                                  default={notificationTopic.id}
                                  objects={NOTIFICATION_TOPICS}
                                  callback={(notificationTopic)=>{
                                  notification.topic = notificationTopic.name
                                  this.setState({
                                    notificationTopic : notificationTopic,
                                    notification : notification
                                  })
                                }}
                            />
                            </FormGroup>
                            {
                              (notificationTopic.name===NOTIFICATION_TOPICS[0].name || notificationTopic.name===NOTIFICATION_TOPICS[1].name)&&
                              (
                                  <FormGroup>
                                    <Label for="addressLink">Address Link</Label>
                                    <Input
                                        type="text"
                                        name="addressLink"
                                        value={notification.addressLink}
                                        onChange={(e) =>{
                                          if(notification!=null){
                                            notification.addressLink = e.target.value
                                            this.setState({
                                              notification:notification
                                            })
                                          }
                                        }}
                                        placeholder="Example : http://news.site/today/hot/news...."
                                    />
                                    <WarningLabel show={(isEmpty(notification.addressLink))} message={"Enter address link"} />
                                  </FormGroup>
                              )
                            }
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    notification:null
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.sendNotification()
                              )}>Send Notification</Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                  {
                    notificationTopic.name===NOTIFICATION_TOPICS[3].name&&
                    (
                        <Row>
                          <Col>
                            <Card body>
                              <Row>
                                <Col sm={6}>
                                </Col>
                                <Col sm={3}>
                                </Col>
                                <Col sm={3}>
                                  <ButtonGroup style={{float:'right'}}>
                                    <Button onClick={event => {
                                      event.preventDefault()
                                      this.setState({userSelectionModalMode:true})
                                    }}>Update User</Button>
                                  </ButtonGroup>
                                </Col>
                              </Row>
                              <Row>
                                <Col>
                                  <Table hover>
                                    <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Fullname</th>
                                      <th>Email</th>
                                      <th>Mobile Phone</th>
                                      <th>Agent Code</th>
                                      <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                      users.map((item, index) =>(
                                          <tr key={index}>
                                            <th scope="row">{++index}</th>
                                            <td>{item.fullname}</td>
                                            <td>{item.email}</td>
                                            <td>{item.mobilePhone}</td>
                                            <td>{item.agentCode}</td>
                                            <td>
                                              <Button color="primary" onClick={e=> {
                                                e.preventDefault()
                                                users.splice(index-1, 1)
                                                this.setState({users: users})
                                              }}>
                                                <MdDelete/>
                                              </Button>
                                            </td>
                                          </tr>
                                      ))
                                    }
                                    </tbody>
                                  </Table>
                                </Col>
                              </Row>
                            </Card>
                          </Col>
                        </Row>
                    )
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>

        </Page>
    );
  }
}
