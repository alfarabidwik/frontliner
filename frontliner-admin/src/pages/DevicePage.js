import Page from 'components/Page';
import React from 'react';
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'
import {deleteParam, parseDate, sortirMap} from "../utils/Utilities";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {MdDelete} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";


const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class DevicePage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
        userDevices : [],
        userDevice:{},
        modalDeleteConfirm:false,
        ascending:true,
        sortir:'ud.created',
        search:"",
        page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
        totalPage:0,
        totalElement:0,
        pageElement:0,

    }
  }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search,true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
    }

  fetchAll = (ascending, sortir, search, progressing) => {
    this.get(Global.API.USER_DEVICES, {
      params:{
        page:this.state.page-1,
        ascending:ascending,
        sortir:sortir,
        search:search,
      }
    }, null, res=>{
        if(res.code === 200){
            this.setState({
                userDevices : res.data,
                totalPage: res.totalPage,
                totalElement: res.totalElement,
                pageElement: res.pageElement,
            })
        }
    }, progressing, true);
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.USER_DEVICE_DELETE+"/"+this.state.userDevice.id, null, null, res => {
          if(res.code===200){
              this.successToast(res.message)
              this.setState({
                  category:null
              }, () => {
                  this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
              })
          }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

    refreshUserDevice = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )


    render() {
        var i = 0 ;

    return (
        <Page
            title="User Devices"
            breadcrumbs={[{ name: 'userDevice', active: true }]}
            className="TablePage"
        >
            {super.render()}
            <ConfirmDialog
                showing={this.state.modalDeleteConfirm}
                title="Delete Confirmation"
                message="Do you want to delete this userDevice item from your list ?"
                okCallback={this.delete}
                cancelCallback={this.closeDialog}/>
            <Row key={1}>
                <Col>
                    <Card className="mb-6">
                        <CardHeader>UserDevice</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={2}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir)
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "ud.created", true))}>Created</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "u.firstname", true))}>Firstname</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "u.agentCode", true))}>Agent Code</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "u.mobilePhone", true))}>Mobile Phone</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(this.state.ascending, "u.email", true))}>Email</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={2}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshUserDevice(false, this.state.sortir, true))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    <SearchInput
                                        placeholder={"Search device, agent code, phone..."}
                                        value={this.state.search}
                                        onChange={e=>{
                                            this.setState({
                                                search:e.target.value
                                            }, () => {
                                                if(this.state.search===''){
                                                    deleteParam(this.props, 'page')
                                                }
                                            })
                                        }}
                                        onEnter={e=>{
                                            if(this.state.search!==''){
                                                deleteParam(this.props, 'page')
                                            }
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Table hover>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Platform</th>
                                        <th>Device ID</th>
                                        <th>Firstname</th>
                                        <th>Agent Code</th>
                                        <th>Mobile Phone</th>
                                        <th>Email</th>
                                        <th>Created</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.userDevices.map((item, index)=>(
                                            <tr key={item.id}>
                                                <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                <td>{item.platform}</td>
                                                <td>{item.deviceId}</td>
                                                <td>
                                                    {item.user.fullname}
                                                    &nbsp;&nbsp;
                                                    <IoMdOpen
                                                        color="blue" style={{cursor:'pointer'}}
                                                        onClick={e=>{
                                                            this.props.history.push('/userDetail?id='+item.user.id)
                                                        }
                                                        }/>
                                                </td>
                                                <td>{item.user.agentCode}</td>
                                                <td>{item.user.mobilePhone}</td>
                                                <td>{item.user.email}</td>
                                                <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                                <td>
                                                    <Button size="sm" color="danger" onClick={e => {
                                                        e.preventDefault()
                                                        this.setState({
                                                            userDevice:item
                                                        }, () => {
                                                            this.confirmDelete()
                                                        })
                                                    }}>
                                                        <MdDelete/>
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                    </tbody>
                                </Table>
                            </Row>
                        </CardBody>
                        <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />

                    </Card>
                </Col>
            </Row>
        </Page>
    );
  }
};
