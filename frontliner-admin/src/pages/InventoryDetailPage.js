import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Input,
    Label,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import shoppingBag from 'assets/img/products/shopping-bag.png';
import Page from "../components/Page";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import Global, {MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE} from "../utils/Global";
import {imageSelector, navigatePage, parseDate, changeParam} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit} from "react-icons/md";
import ColorContainer from '../components/Widget/color/ColorContainer'
import PointModal from "../components/modal/PointModal";
import VerticalImageGallery from "../components/Widget/VerticalImageGallery";
import InventoryQuantityModal from '../components/modal/InventoryQuantityModal'
import ImageCropper from "../components/modal/ImageCropper";
import {getData} from "../utils/StorageUtil";
import PriceInput from "../components/Widget/PriceInput";
import WeightInput from "../components/Widget/WeightInput";
import NumberInput from "../components/Widget/NumberInput";
import WarningLabel from "../components/Widget/WarningLabel";



export default class InventoryDetailPage extends BasePage{

    constructor(props) {
        super(props);
        this.state = {
            inventory:{},
            products:[],
            sizes:[],
            colors:[],
            image :null,
            inventoryImage:{},
            productIdQuery: queryString.parse(this.props.query).productId,
            inventoryIdQuery: queryString.parse(this.props.query).inventoryId,
            product:{},
            size:{},
            modalAddConfirm:false,
            inventories:[],
            ascending:true,
            sortir:'created',
            quantityModal:false,

        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchProductDetail(this.state.productIdQuery)
        this.fetchAllSize()
        this.fetchAllProducts(true, "name")
        this.fetchInventoryDetail(this.state.inventoryIdQuery)
        this.fetchAllColor()
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                productIdQuery: queryString.parse(props.query).productId,
                inventoryIdQuery: queryString.parse(props.query).inventoryId,
            }, () => {
                this.fetchProductDetail(this.state.productIdQuery)
                this.fetchInventoryDetail(this.state.inventoryIdQuery)
            })
        }
    }

    confirmSave = () =>{
        this.setState({
            modalAddConfirm:true
        })
    }



    save = () => {
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.INVENTORY_SAVE, null, {data:this.state.inventory}, res=>{
                if(res.code===200){
                    this.setState({
                        inventory:res.data,
                        product:res.data.product,
                        image:res.data.imageLink,
                        size:res.data.size,
                        productIdQuery: this.state.product.id,
                        inventoryIdQuery: this.state.inventory.id,
                    }, () => {
                        this.showDialog("Success", res.message)
                        this.fetchInventoryDetail(this.state.inventory.id)
                    })
                }
            },  true, true)
        })
    }

    saveUpload = () => {
        axios({
            url: this.state.image,
            method: 'GET',
            responseType: 'blob', // important
        }).then((response) => {
            var file = response.data ;
            var formData = new FormData();
            formData.append("inventoryDtoGson", JSON.stringify(this.state.inventory))
            formData.append("multipartFile", file);
            this.setState({
                modalAddConfirm:false
            }, () => {
                this.post(Global.API.INVENTORY_SAVE_UPLOAD, formData, null, res=>{
                    if(res.code===200){
                        this.setState({
                            inventory:res.data,
                            product:res.data.product,
                            image:res.data.imageLink,
                            size:res.data.size
                        }, () => {
                            this.showDialog("Success", res.message)
                        })
                    }
                },  true, true)
            })
        })
    }

    uploadImage = (file) =>{
        let formData = new FormData();
        formData.append("inventoryId", this.state.inventory.id);
        if(this.state.inventoryImage.id){
            formData.append("inventoryImageId", this.state.inventoryImage.id);
        }
        formData.append("multipartFile", file);
        this.post(Global.API.INVENTORY_UPLOAD_IMAGE, formData, null, response=>{
            if(response.code===200){
                this.setState({
                    inventoryImage:{}
                }, () => {
                    this.fetchInventoryDetail(this.state.inventory.id)
                })
            }
        }, true, true)
    }

    deleteImage = () =>{
        this.get(Global.API.INVENTORY_DELETE_IMAGE, {
            params:{
                inventoryImageId:this.state.inventoryImage.id
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    image:null,
                    inventoryImage:{}
                })
                this.fetchInventoryDetail(this.state.inventory.id)

            }
        }, true, true)
    }

    fetchInventoryDetail = (id) => {
        if(id!=null){
            this.get(Global.API.INVENTORY, {
                params :{
                    id : id
                }
            }, null, res =>{
                if(res.code===200){
                    this.setState({
                        inventory:res.data,
                        product:res.data.product,
                        image:res.data.imageLink,
                        size:res.data.size
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }

    fetchAllProducts = (ascending, sortir) =>{
        this.get(Global.API.PRODUCTS_DROPDOWN, {
            params:{
                ascending:ascending,
                sortir:sortir
            }
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    products : res.data
                })
            }
        }, true, true);
    }

    fetchAllSize = () =>{
        this.get(Global.API.SIZES, null, null, response=>{
            if(response.code === 200){
                this.setState({
                    sizes : response.data
                })
            }
        }, true, true)
    }

    fetchAllColor = () =>{
        this.get(Global.API.COLORS, null, null, response=>{
            if(response.code === 200){
                this.setState({
                    colors : response.data
                })
            }
        }, true, true)
    }


    fetchProductDetail = (id) => {
        if(id!=null){
            this.get(Global.API.PRODUCT, {
                params :{
                    id : id
                }
            }, null, res =>{
                if(res.code===200){
                    var inventory = this.state.inventory
                    inventory.product = res.data
                    this.setState({
                        product:res.data,
                        inventory:inventory
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false
        })
    }

    closePointModal = () =>{
        this.setState({
            pointModal:false
        }, () => {
            this.fetchInventoryDetail(this.state.inventory.id)
        })
    }

    openQuantityModal=()=>{
        this.setState({
            quantityModal:true
        })
    }
    closeQuantityModal=()=>{
        this.setState({
            quantityModal:false
        })
    }

    updateQuantity=(item, quantity, note, substraction)=>{
        let form = new FormData()
        form.append("inventoryId", item.id)
        form.append("newStock", quantity)
        form.append("note", note)
        if(substraction){
            form.append("substraction", true)
        }else{
            form.append("substraction", false)
        }
        this.post(Global.API.INVENTORY_UPDATE_STOCK, form, null, response=>{
            if(response.code===200){
                this.fetchInventoryDetail(this.state.inventory.id)
                this.closeQuantityModal()
            }
        }, true, true)
    }



    render() {
        var ids = [];
        var labels = [];
        this.state.products.map((item, index)=>{
            ids.push(item.id);
            labels.push(item.name);
        })
        let configuration = getData(Global.CONFIGURATION)
        let aspect = 4/4
        if(configuration.productImageDimensionWidth && configuration.productImageDimensionHeight){
            aspect = configuration.productImageDimensionWidth/configuration.productImageDimensionHeight
        }

        let inventory = this.state.inventory?this.state.inventory:{}

        return (
            <Page
                title="Inventory Form"
                breadcrumbs={[{ name: 'inventory form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this inventory item to your list ?"
                    okCallback={this.save}
                    cancelCallback={this.closeDialog}/>
                <PointModal
                    showing={this.state.pointModal}
                    edit={this.state.edit}
                    inventory={this.state.inventory}
                    close={this.closePointModal}
                />
                {
                    (this.state.inventory.id)&&(
                        <InventoryQuantityModal modal={this.state.quantityModal} inventory={this.state.inventory} okCallback={this.updateQuantity} closeCallback={this.closeQuantityModal}/>
                    )
                }
                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Inventory</CardHeader>
                            <CardBody>
                                <Row>
                                    {
                                        (this.state.inventory&&this.state.inventory.id)&&(
                                            <Col md={4}>
                                                <Card>
                                                    <CardBody>
                                                        <Row>
                                                            <Col md={9}>
                                                                <Row>
                                                                    <Col>
                                                                        <CardImg top src={
                                                                            this.state.image!=null?this.state.image:shoppingBag}
                                                                                 onClick={e=>{
                                                                                     if(this.state.inventory.id){
                                                                                         imageSelector(file =>{
                                                                                             this.setState({
                                                                                                 image:file,
                                                                                                 cropperModal:true
                                                                                             })
                                                                                         }, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE).click()
                                                                                     }
                                                                                 }}
                                                                                 onError={(elm)=>this.defaultImage(elm, shoppingBag)}/>
                                                                    </Col>
                                                                </Row>
                                                                <Row>
                                                                    <Col>
                                                                        <WarningLabel message={"*Use Jpeg Format / Max "+PRODUCT_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                            <Col>
                                                                <VerticalImageGallery addFunction={true} images={this.state.inventory&&this.state.inventory.inventoryImages?this.state.inventory.inventoryImages:[]} onClick={(event, photo, image)=>{
                                                                    event.preventDefault();
                                                                    if(this.state.inventory.inventoryImages && photo.index===this.state.inventory.inventoryImages.length){
                                                                        this.setState({
                                                                            image:null,
                                                                            inventoryImage:image,
                                                                        }, () => {
                                                                            imageSelector(file =>{
                                                                                this.setState({
                                                                                    image:file,
                                                                                    cropperModal:true
                                                                                })
                                                                            }, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE).click()
                                                                        })
                                                                    }else{
                                                                        this.setState({
                                                                            image:photo.photo.src,
                                                                            inventoryImage:image
                                                                        })
                                                                    }
                                                                }}/>
                                                            </Col>
                                                        </Row>
                                                    </CardBody>
                                                    <CardBody>
                                                        {
                                                            (this.state.inventoryImage.id)?(
                                                                <Row>
                                                                    <Col>
                                                                        <ButtonGroup className="float-left">
                                                                            <Button color="danger" onClick={e => {
                                                                                this.deleteImage()
                                                                            }
                                                                            }>Delete</Button>
                                                                        </ButtonGroup>
                                                                    </Col>
                                                                    <Col>
                                                                        <ButtonGroup className="float-right">
                                                                            <Button color="primary" onClick={e=>{
                                                                                imageSelector(file =>{
                                                                                    this.setState({
                                                                                        image:file,
                                                                                        cropperModal:true
                                                                                    })
                                                                                }, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE).click()
                                                                            }}>Update</Button>
                                                                        </ButtonGroup>
                                                                    </Col>

                                                                </Row>
                                                            ):(null)
                                                        }
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        )
                                    }
                                    <Col>
                                        <Card>
                                            <CardBody>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Product"}
                                                        objects={this.state.products}
                                                        default={this.state.product!=null?this.state.product.id:null}
                                                        disable={this.state.product.id!=null?true:false}
                                                        callback={(product)=>{
                                                            let inv = this.state.inventory
                                                            if(inv!=null && product!=null){
                                                                inv.product = product
                                                                this.setState({
                                                                    inventory:inv,
                                                                    product:product
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label for="typeName">Type Name</Label>
                                                    <Input
                                                        type="text"
                                                        name="typeName"
                                                        value={this.state.inventory&&this.state.inventory.typeName?this.state.inventory.typeName:""}
                                                        onChange={event=>{
                                                            event.preventDefault()
                                                            let inventory = this.state.inventory
                                                            inventory.typeName = event.target.value
                                                            this.setState({inventory:inventory})
                                                        }}
                                                        placeholder="Enter type name"/>
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Size"}
                                                        objects={this.state.sizes}
                                                        default={this.state.size?this.state.size.id:null}
                                                        callback={(size)=>{
                                                            let inv = this.state.inventory
                                                            if(inv!=null && size!=null){
                                                                inv.size = size
                                                                this.setState({
                                                                    inventory:inv,
                                                                    size:size
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <ActiveOption
                                                        callback={(active)=>{
                                                            let inventory = this.state.inventory
                                                            if(inventory!=null){
                                                                inventory.active = active
                                                                this.setState({
                                                                    inventory:inventory
                                                                })
                                                            }

                                                        }}
                                                        default={this.state.inventory?this.state.inventory.active:false}
                                                    />
                                                </FormGroup>
                                                <Row>
                                                    <Col md={4}>
                                                        <FormGroup>
                                                            <Label for="color">Color</Label>
                                                            <ColorContainer
                                                                colors={this.state.colors}
                                                                selectedColors={this.state.inventory!=null?this.state.inventory.colors:[]}
                                                                callback={colors=>{
                                                                    let inventory = this.state.inventory
                                                                    if(inventory!=null){
                                                                        inventory.colors = colors
                                                                        this.setState({
                                                                            inventory:inventory
                                                                        })
                                                                    }
                                                                }}/>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col md={8}>
                                                        <FormGroup>
                                                            <Label for="quantity">Quantity</Label>
                                                            <Row>
                                                                <Col md={(this.state.inventory.id)?10:12}>
                                                                    <NumberInput
                                                                        name="quantity"
                                                                        className="form-control"
                                                                        value={(this.state.inventory&&this.state.inventory.quantity!=null)?this.state.inventory.quantity:0}
                                                                        onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                                            let itm = this.state.inventory
                                                                            if(itm!=null){
                                                                                itm.quantity = floatvalue
                                                                                this.setState({
                                                                                    inventory:itm
                                                                                })
                                                                            }
                                                                        }}
                                                                        placeholder="Enter quantity">
                                                                    </NumberInput>
                                                                </Col>
                                                                {
                                                                    (this.state.inventory.id)&&(
                                                                        <Col md={2}>
                                                                            <Button onClick={event=>{
                                                                                event.preventDefault()
                                                                                this.openQuantityModal()
                                                                            }}><MdEdit/></Button>
                                                                        </Col>
                                                                    )
                                                                }
                                                            </Row>
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <Label for="weight">Weight (gram)</Label>
                                                            <WeightInput
                                                                name="weight"
                                                                className="form-control"
                                                                value={(this.state.inventory&&this.state.inventory.weight!=null)?this.state.inventory.weight:0}
                                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                                    let itm = this.state.inventory
                                                                    if(itm!=null){
                                                                        itm.weight = floatvalue
                                                                        this.setState({
                                                                            inventory:itm
                                                                        })
                                                                    }
                                                                }}
                                                                placeholder="Enter weight">
                                                            </WeightInput>
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <Label for="price">Price</Label>
                                                            <PriceInput
                                                                name="price"
                                                                className="form-control"
                                                                value={(this.state.inventory&&this.state.inventory.price!=null)?this.state.inventory.price:0}
                                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                                    let itm = this.state.inventory
                                                                    if(itm!=null){
                                                                        itm.price = floatvalue
                                                                        this.setState({
                                                                            inventory:itm
                                                                        })
                                                                    }
                                                                }}
                                                                placeholder="Enter price">
                                                            </PriceInput>
                                                        </FormGroup>
                                                        <FormGroup>
                                                            <sLabel for="point">Point</sLabel>
                                                            <Row>
                                                                <Col md={this.state.inventory.id?11:12}>
                                                                    <Input value={this.state.inventory&&this.state.inventory.point?this.state.inventory.point.name:""} readOnly/>
                                                                </Col>
                                                                {
                                                                    this.state.inventory.id&&(
                                                                        <Col md={1}>
                                                                        <Button onClick={event=>{
                                                                            event.preventDefault()
                                                                            this.setState({
                                                                                inventory:this.state.inventory,
                                                                                pointModal:true
                                                                            })
                                                                        }}><MdEdit/></Button>
                                                                        </Col>
                                                                    )
                                                                }
                                                            </Row>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.props.history.goBack()
                                                                )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.inventory!=null && this.state.inventory.id!=null)?"Update":"Create"
                                                            } </Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>

                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        this.setState({
                            image:file,
                            cropperModal:false,
                            imageBlob:fileBlob,
                        }, () => {
                            this.uploadImage(this.state.imageBlob)
                        })
                    })
                }} cancelCallback={()=>{
                    this.setState({
                        cropperModal:false,
                        image:inventory.imageLink,
                        imageBlob:null,
                    })
                }}/>

            </Page>
        );
    }


}