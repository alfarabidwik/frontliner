import BasePage from "./BasePage";
import React from 'react'
import Page from "../components/Page";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import CardHeader from "reactstrap/es/CardHeader";
import CardBody from "reactstrap/es/CardBody";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {isEmpty, parseDate, sortirMap} from "../utils/Utilities";
import {MdAdd, MdDelete, MdEdit, MdFilter, MdFilter1} from "react-icons/md";
import ButtonGroup from "reactstrap/es/ButtonGroup";
import Button from "reactstrap/es/Button";
import FlashSaleModal from "../components/modal/FlashSaleModal";
import {IoMdAdd, IoMdOpen} from "react-icons/io";
import SearchInput from '../components/SearchInput'
import {
    ButtonToolbar,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Form,
    Input,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import FormGroup from "reactstrap/es/FormGroup";
import FlashSaleProductModal from "../components/modal/FlashSaleProductModal";
import FlashSaleContentModal from "../components/modal/FlashSaleContentModal";
import {MdPublish} from "react-icons/md/index";
import CardFooter from "reactstrap/es/CardFooter";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";


export default class FlashSaleDetailPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            modalDeleteConfirm:false,
            modalAddConfirm:false,
            modalActivateConfirm:false,
            flashSaleProductModal:false,
            flashSaleContentModal:false,
            flashSaleId:queryString.parse(this.props.query).id,
            flashSale : {},
            flashSaleProducts : [],
            flashSaleProduct : {},
            flashSaleEditModal:false,
            sortir:"fs.created",
            ascending:false,
            search:"",
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            product:null,
        }
    }

    componentDidMount() {
        this.fetchDetail()
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchProductByFlashSale()
            })
        }
    }


    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
            flashSaleContentModal:false,
            flashSaleProductModal:false,
            modalActivateConfirm:false,
            product:null,
        })
    }

    fetchDetail = () =>{
        this.get(Global.API.FLASHSALE, {
            params:{
                id:this.state.flashSaleId,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    flashSale : response.data
                }, () => {
                    this.fetchProductByFlashSale()
                })
            }else{
                // this.props.history.goBack();
            }
        }, true, true)
    }

    fetchProductByFlashSale = () =>{
        this.get(Global.API.PRODUCT_BY_FLASHSALE, {
            params:{
                id:this.state.flashSale.id,
                search:this.state.search,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                page: this.state.page-1

            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    flashSaleProducts : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }


    closeFlashSaleModal = () =>{
        this.setState({
            flashSaleEditModal:false,
            flashSaleProductModal:false,
        })
    }

    closeFlashSaleContentModal = () =>{
        this.setState({
            flashSaleContentModal:false
        }, () => {
            this.refresh(this.state.ascending, this.state.sortir)
        })
    }


    openFlashSaleContentModal  = (product) =>{
        this.setState({
            product:product,
            flashSaleContentModal:true
        })
    }

    refresh = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchProductByFlashSale()
        })
    )


    deleteConfirm =(product)=>{
        if(!product){
            product = this.state.product
        }
        this.setState({
            product : product,
            modalDeleteConfirm : true
        })
    }

    delete = () => {
        this.get(Global.API.FLASHSALE_PRODUCT_DELETE, {
            params:{
                flashSaleId:this.state.flashSale.id,
                productId:this.state.product.id
            }
        }, null , response=>{
            this.closeDialog()
            if(response.code===200){
                this.setState({
                    modalDeleteConfirm:false,
                    product:null
                }, () => {
                    this.refresh(this.state.ascending, this.state.sortir)
                })
            }else{
                this.showDialog("Error", response.message)
            }
        }, true, true)
    }

    activateConfirm = () =>{
        this.setState({
            modalActivateConfirm : true
        })
    }

    activate = () =>{
        this.get(Global.API.FLASHSALE_ACTIVATE, {
            params:{
                flashSaleId:this.state.flashSale.id,
                active:!this.state.flashSale.active,
            }
        }, null, response=>{
            if(response.code===200){
                this.fetchDetail()
            }else{
                this.showDialog(response.message)
                this.fetchDetail()
            }
            this.closeDialog()
        }, true, true)
    }


    render() {
        let flashSale = this.state.flashSale
        return (
            <Page
                title="Flash Sale Content"
                breadcrumbs={[{ name: 'flash sale content', active: true }]}
                className="TablePage">
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalActivateConfirm}
                    title={this.state.flashSale.active?"Unactivate Confirmation":"Activate Confirmation"}
                    message={this.state.flashSale.active?"Do you want to unactivate this flash sale program ?":"Do you want to activate this flash sale program ?"}
                    okCallback={this.activate}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this product item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <FlashSaleModal
                    showing={this.state.flashSaleEditModal}
                    edit={true}
                    flashSale={flashSale}
                    close={this.closeFlashSaleModal}
                    refresh={this.fetchDetail}
                />
                <FlashSaleProductModal
                    showing={this.state.flashSaleProductModal}
                    flashSale={flashSale}
                    deleteConfirm={this.deleteConfirm}
                    close={this.closeFlashSaleModal}
                    openFlashSaleContentModal={this.openFlashSaleContentModal}
                />
                <FlashSaleContentModal
                    showing={this.state.flashSaleContentModal}
                    flashSale={flashSale}
                    product={this.state.product}
                    close={this.closeFlashSaleContentModal}
                />
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                Flash Sale Content
                            </CardHeader>
                            <span className="pt-3 pr-3 pl-3 block-example border-bottom">
                                {
                                    isEmpty(flashSale.id)?
                                        (null)
                                        :
                                        (<Row>
                                            <Col md={6}>
                                                <Button>
                                                    Name&nbsp;&nbsp;:&nbsp;&nbsp;
                                                    <span className="font-weight-bold">{flashSale.name}</span>
                                                </Button>
                                                &nbsp;
                                                <Button>
                                                    <span className="font-weight-bold">{parseDate(flashSale.startDate)}</span>
                                                    &nbsp;&nbsp;s/d&nbsp;&nbsp;
                                                    <span className="font-weight-bold">{parseDate(flashSale.endDate)}</span>
                                                </Button>
                                                &nbsp;
                                                <Button  onClick={e=>{
                                                    e.preventDefault()
                                                    this.setState({
                                                        flashSaleEditModal:!this.state.flashSaleEditModal
                                                    })
                                                }}>
                                                    <MdEdit color="white"/>
                                                </Button>
                                            </Col>
                                            <Col md={6}>
                                                <ButtonGroup className="float-right">
                                                    <Button color={flashSale.active?"danger":"primary"} onClick={e => (
                                                        this.activateConfirm()
                                                    )}><MdPublish/>&nbsp;{flashSale.active?"Unactivate":"Activate"}</Button>
                                                </ButtonGroup>
                                            </Col>
                                        </Row>)
                                }
                            </span>
                            <span className="pt-3 pr-3 pl-3 block-example border-bottom">
                                <Row>
                                    <Col md={3}>
                                        Sort By :
                                        <UncontrolledButtonDropdown key={1}>
                                            <DropdownToggle
                                                caret
                                                color="white"
                                                className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir)
                                            }
                                            </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "p.name"))}>Name</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "p.category.name"))}>Category Name</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "stock"))}>Stock</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "fs.created"))}>Created</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "fs.updated"))}>Updated</DropdownItem>
                                                </DropdownMenu>
                                        </UncontrolledButtonDropdown>
                                    </Col>
                                    <Col md={3}>
                                        Sortir :
                                        <UncontrolledButtonDropdown key={2}>
                                            <DropdownToggle
                                                caret
                                                color="white"
                                                className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                                            </DropdownMenu>
                                        </UncontrolledButtonDropdown>
                                    </Col>
                                    <Col md={4}>
                                        <FormGroup className="float-right">
                                        <SearchInput
                                            value={this.state.search}
                                            onChange={e=>{
                                                this.setState({
                                                    search:e.target.value
                                                }, () => {
                                                    if(this.state.search===''){
                                                        deleteParam(this.props, 'page')
                                                    }
                                                })
                                            }}
                                            onEnter={e=>{
                                                if(this.state.search!==''){
                                                    deleteParam(this.props, 'page')
                                                }
                                            }}
                                        />

                                        {/*<SearchInput*/}
                                            {/*value={this.state.search} onChange={e=>{*/}
                                            {/*this.setState({*/}
                                                {/*search:e.target.value*/}
                                            {/*}, () => {*/}
                                                {/*deleteParam(this.props, 'page')*/}
                                                {/*this.fetchProductByFlashSale()*/}
                                            {/*})*/}
                                        {/*}}/>*/}
                                        </FormGroup>
                                    </Col>
                                    <Col md={2}>
                                        <FormGroup className="float-right">
                                            &nbsp;
                                            <ButtonGroup>
                                                <Button size="md" color="info" onClick={e=>{
                                                    e.preventDefault()
                                                    this.setState({
                                                        flashSaleProductModal:!this.state.flashSaleProductModal
                                                    })
                                                }}><MdAdd/>&nbsp;Add</Button>
                                            </ButtonGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </span>
                            <CardBody>
                                <Row>
                                    <Table hover>
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Stock</th>
                                            <th>Current Usage</th>
                                            <th>Created</th>
                                            <th>Updated</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            !isEmpty(this.state.flashSaleProducts)?
                                            this.state.flashSaleProducts.map((item, index)=>(
                                                <tr key={index}>
                                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                    <td>{item.name}</td>
                                                    <td>{item.category!=null?item.category.name:""}</td>
                                                    <td>{item.flashSaleStock}</td>
                                                    <td>{item.flashSaleStockUsage}</td>
                                                    <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                    <td>
                                                        <Button color="danger" onClick={e => {
                                                            this.setState({
                                                                product:item
                                                            }, () => {
                                                                this.deleteConfirm()
                                                            })
                                                        }}>
                                                            <MdDelete/>
                                                        </Button>
                                                        &nbsp;
                                                        &nbsp;
                                                        <Button color="primary" onClick={e => {
                                                            e.preventDefault()
                                                            this.setState({
                                                                product:item,
                                                                flashSaleContentModal:true
                                                            })
                                                        }}>
                                                            <IoMdOpen/>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))
                                                :
                                                null
                                        }
                                        </tbody>
                                    </Table>
                                </Row>
                            </CardBody>
                            <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                        </Card>
                    </Col>
                </Row>
            </Page>
        );
    }


}