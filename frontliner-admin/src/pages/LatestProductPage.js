import Page from 'components/Page';
import React from 'react';
import {Link} from 'react-router-dom'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {isEmpty, parseDate} from "../utils/Utilities";
import LatestProductSelectorModal from "../components/modal/LatestProductSelectorModal";
import Pagination from '../components/Pagination'
import queryString from "query-string";
import Img from 'react-image'


const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class LatestProductPage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
      products : [],
      product:{},
      modalDeleteConfirm:false,
      ascending:true,
      sortir:'created',
      search:"",
      page :1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
      productModal:false
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page)
  }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page)
            })
        }
    }


    fetchAll = (ascending, sortir, search, page) => {
    this.get(Global.API.PRODUCT_LATEST, {
      params:{
        ascending:ascending,
        sortir:sortir,
        search:search,
        page: page-1
      }
    }, null, res =>{
      if(res.code === 200){
        this.setState({
          products : res.data,
          totalPage: res.totalPage,
          totalElement: res.totalElement,
          pageElement: res.pageElement,
        })
      }
    }, true, true);
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
        this.get(Global.API.PRODUCT_DELETE_LATEST,{
            params:{
                productId:this.state.product.id,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    category:null
                }, () => {
                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page)
                })
            }
        }, true, true)
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshProduct = (ascending, sortir) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page)
      })
  )
  closeProductModal = () =>{
      this.setState({
          productModal:false,
      })
  }


  productSelected = (product) =>{
    this.closeProductModal()
    this.get(Global.API.PRODUCT_UPDATE_LATEST, {
        params:{
            productId:product.id,
            latestSort:product.latestSort
        }
    }, null, response=>{
      if(response.code===200){
          this.refreshProduct(this.state.ascending, this.state.sortir)
      }
    }, true, true)
  }

    render() {
    return (
        <Page
            title="Latest Products"
            breadcrumbs={[{ name: 'latest product', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this latest item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
            <LatestProductSelectorModal
                showing={this.state.productModal}
                close={this.closeProductModal}
                selected={this.productSelected}
            />
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardBody>
                  <Row>
                        <Col md={2}>
                        </Col>
                        <Col md={2}>
                        </Col>
                        <Col md={3}>
                        </Col>
                        <Col md={5}>
                            <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>{
                                    this.setState({
                                        productModal:!this.state.productModal
                                    })
                                }}>Add Product</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Category</th>
                              <th>Image</th>
                              <th className="text-center">Latest</th>
                              <th>Last Update</th>
                              <th>Remove</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.products.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.name}</td>
                                      <td>{item.category!=null?item.category.name:""}</td>
                                      <td width="5%">
                                          <Img
                                              className="img-thumbnail"
                                              src={item.imageLink}
                                              onClick={e=>{
                                                  this.setState({
                                                      imageLink:item.imageLink,
                                                      openPreview:true
                                                  })
                                              }}
                                          />
                                          </td>
                                      <th className="text-center">{isEmpty(item.latestSort)?"-":item.latestSort}</th>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          {
                                              item.latestSort&&(
                                                  <Button size="sm" color="danger" onClick={e => {
                                                      this.setState({
                                                          product:item
                                                      }, () => {
                                                          this.confirmDelete()
                                                      })
                                                  }}>
                                                      <MdDelete/>
                                                  </Button>
                                              )
                                          }
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
};
