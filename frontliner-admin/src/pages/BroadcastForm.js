import React from 'react';
import BasePage from "./BasePage";
import {Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row} from "reactstrap";
import Page from "../components/Page";
import Global, {MIME_TEXT_HTML} from "../utils/Global";
import {cloneObject, fileSelector, weightForma} from "../utils/Utilities";
import {getData} from './../utils/StorageUtil'
import {IoMdCloudUpload} from "react-icons/io/index";
import Checkbox from './../components/Checkbox'
import CardFooter from "reactstrap/es/CardFooter";
import DateTimeInput from "../components/Widget/DateTimeInput";
import Chips from 'react-chips'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import {convertToRaw, EditorState} from "draft-js";
import ItemOption from "../components/Widget/ItemOption";
import {Editor} from 'react-draft-wysiwyg';
import renderHTML from 'react-render-html';

let defaultSendEmail = {
    regularSender:true,
    sendDirectly:true,
    allUser:false,
    contentType:false,
}



function uploadImageCallback(file){
    return new Promise(
        (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open('POST', Global.BASE_URL+Global.API.EMAIL_UPLOAD_IMAGE);
            // xhr.setRequestHeader('Authorization', 'Client-ID XXXXX');
            const data = new FormData();
            data.append('multipartFile', file);
            xhr.send(data);
            xhr.addEventListener('load', () => {
                const response = JSON.parse(xhr.responseText);
                resolve(response);
            });
            xhr.addEventListener('error', () => {
                const error = JSON.parse(xhr.responseText);
                reject(error);
            });
        }
    );
}

export default class BroadcastForm extends BasePage{

    editor = null ;

    constructor(props) {
        super(props);
        let emptyContent =EditorState.createEmpty()
        let state = this.state
        state.sendEmail = cloneObject(defaultSendEmail)
        state.userEmails = []
        emptyContent.currentContent = ''
        state.editorState =emptyContent
         this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchUserEmails()
        if(this.editor){
        }
    }

    fetchUserEmails=()=>{
        this.get(Global.API.USER_EMAILS, null, null, response=>{
            if(response.code===200){
                this.setState({
                    userEmails:response.data
                })
            }
        }, false, true)
    }


    onEditorStateChange= (editorState) => {
        let sendEmail = this.state.sendEmail
        sendEmail.content = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        this.setState({
            sendEmail:sendEmail,
            editorState:editorState,
        });
    }


    send=()=>{
        let sendEmail = this.state.sendEmail
        if(!sendEmail.subject || sendEmail.subject===''){
            this.showDialog("Warning", "Email subject cannot be empty")
            return
        }
        if(!sendEmail.regularSender && (!sendEmail.sender || sendEmail.sender==='')){
            this.showDialog("Warning", "Sender cannot be empty when you choose non regular sender")
            return
        }
        let userEmailChips = this.state.userEmailChips
        let userEmails = []
        if((!sendEmail.allUser) && ((!userEmailChips || userEmailChips.length===0))){
            this.showDialog("Warning", "Please Enter at least one email receiver")
            return
        }

        if(!sendEmail.content){
            this.showDialog("Warning", "Please Enter the body email content")
            return
        }

        if(userEmailChips && userEmailChips.length>0){
            userEmailChips.map((item, index)=>{
                userEmails.push({
                    email:item
                })
            })
        }
        sendEmail.userEmails = userEmails

        if(sendEmail.sendDirectly){
            this.openConfirmDialog("Confirmation","Do you want to submit this announcement directly to your email list, please be careful, it cannot be canceled", ()=>{
                this.post(Global.API.SEND_EMAIL, sendEmail, null, response=>{
                    if(response.code===200){
                        this.successToast(response.message)
                        this.setState({
                            sendEmail:cloneObject(defaultSendEmail)
                        }, () => {
                            this.props.history.goBack()
                        })
                    }
                }, true, true)

            })
        }else{
            this.openConfirmDialog("Confirmation","Do you want scheduling this announcement to these email list, please be careful it cannot be canceled", ()=>{
                this.post(Global.API.SEND_EMAIL, sendEmail, null, response=>{
                    if(response.code===200){
                        this.successToast(response.message)
                        this.setState({
                            sendEmail:cloneObject(defaultSendEmail)
                        }, () => {
                            this.props.history.goBack()
                        })
                    }
                }, true, true)

            })
        }
    }



    render() {
        let sendEmail = this.state.sendEmail ;
        let configuration = getData(Global.CONFIGURATION)
        if(!configuration){
            configuration = {}
        }

        if(sendEmail.regularSender){
            sendEmail.sender = configuration.regularEmailSender
        }


        let editorState = this.state.editorState

        return (
            <Page
                title="Send Announcement"
                breadcrumbs={[{ name: 'Form', active: true }]}>
                {super.render()}
                {
                    <Card>
                        <CardBody>
                            <Row>
                                <Col md={4}>
                                    <FormGroup>
                                        <Label>Email Subject</Label>
                                        <Input
                                            value={sendEmail.subject}
                                            onChange={(e)=>{
                                                let value = e.target.value
                                                sendEmail.subject = value
                                                this.setState({
                                                    sendEmail:sendEmail
                                                })
                                            }}/>
                                    </FormGroup>
                                </Col>
                                <Col md={8}>
                                    <Label>
                                        Sender
                                    </Label>
                                    <Row style={{padding:'0px'}}>
                                        <Col md={3}>
                                            <Checkbox checked={sendEmail.regularSender} onChange={(checked)=>{
                                                sendEmail.regularSender = checked
                                                sendEmail.sender = configuration.regularEmailSender
                                                this.setState({sendEmail:sendEmail})
                                            }}/>
                                            &nbsp;
                                            Regular Sender
                                        </Col>
                                        <Col md={4}>
                                            <Input
                                                value={sendEmail.sender}
                                                onChange={(e)=>{
                                                    let value = e.target.value
                                                    if(value===configuration.regularEmailSender){
                                                        sendEmail.regularSender = true
                                                    }else{
                                                        sendEmail.regularSender = false
                                                    }
                                                    sendEmail.sender=value
                                                    this.setState({
                                                        sendEmail:sendEmail
                                                    })
                                                }}
                                                placeholder="Fill it for specific email account sender.."/>
                                        </Col>
                                        <Col md={3}>
                                            <Input
                                                disabled={true}
                                                style={{marginLeft:'0px', paddingLeft:'0px', backgroundColor:"transparent", fontWeight:'bold', color:'blue'}}
                                                className="border-0"
                                                defaultValue={"@"+configuration.mailDomain}
                                                value={"@"+configuration.mailDomain}
                                                onChange={(e)=>{}}/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4}>
                                    <Row>
                                        <Col>
                                            <Input value="User Receiver" style={{border:'0', backgroundColor:'transparent', paddingLeft:'0px'}} disabled={true} onChange={()=>{}}/>
                                        </Col>
                                        <Col className="float-right">
                                            <span className="float-right">
                                                <Checkbox checked={sendEmail.allUser} onChange={(checked)=>{
                                                    sendEmail.allUser = checked
                                                    this.setState({sendEmail:sendEmail})
                                                }}/>
                                                &nbsp;
                                                All User
                                            </span>
                                        </Col>
                                    </Row>
                                    {
                                        (!sendEmail.allUser)&&(
                                            <Chips
                                                value={this.state.userEmailChips}
                                                onChange={(chips)=>{
                                                    this.setState({
                                                        userEmailChips:chips
                                                    })
                                                }}
                                                className="cr-search-form__input"
                                                suggestions={this.state.userEmails}
                                                placeholder="Enter user receiver email..."
                                            />
                                        )
                                    }
                                </Col>
                                <Col md={4}>
                                    <Row>
                                        <Col>
                                            <Input value="Content Options" style={{border:'0', backgroundColor:'transparent', paddingLeft:'0px'}} disabled={true} onChange={()=>{}}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <ItemOption
                                                hideLabel={true}
                                                title={"Content Options"}
                                                default={sendEmail.contentType?1:2}
                                                objects={[
                                                    {id:1, name:'Manual Typing'}, {id:2, name:'Upload File'},
                                                ]}
                                                callback={(object)=>{
                                                    if(object.id===1){
                                                        sendEmail.contentType = true
                                                    }else{
                                                        sendEmail.contentType = false
                                                    }
                                                    this.setState({
                                                        sendEmail:sendEmail
                                                    })
                                                }}
                                                hideOptionLabel={true}/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            {
                                sendEmail.contentType?
                                    (
                                        <Card>
                                            <CardHeader>Manual Content (HTML)</CardHeader>
                                            <CardBody>
                                                <Row>
                                                    <Col>

                                                        <Editor
                                                            toolbar={
                                                                {
                                                                    image: {
                                                                        className: undefined,
                                                                        component: undefined,
                                                                        popupClassName: undefined,
                                                                        urlEnabled: true,
                                                                        uploadEnabled: true,
                                                                        alignmentEnabled: true,
                                                                        uploadCallback: uploadImageCallback,
                                                                        previewImage: true,
                                                                        inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
                                                                        alt: {present: false, mandatory: false},
                                                                        defaultSize: {
                                                                            height: 'auto',
                                                                            width: 'auto',
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            editorState={editorState}
                                                            onEditorStateChange={this.onEditorStateChange}
                                                            style={{height:'500px'}}
                                                        />
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    )
                                        :
                                    (
                                        <Row>
                                            <Col md={10}>
                                                <Card>
                                                    <CardHeader>
                                                        HTML File Preview
                                                    </CardHeader>
                                                    <CardBody>
                                                        {
                                                            renderHTML(sendEmail.content?sendEmail.content:"<center>Html Preview</center>")
                                                        }
                                                    </CardBody>
                                                    <CardFooter>
                                                        <span style={{color:'red', fontStyle:"italic", fontSize:'14px'}}>*Write your css inline<br/>*Use image which is already stored at the server, and write the src by using server link for every image tag like an example : src='https://api.focallure.co.id/api/...filename.png'</span>
                                                    </CardFooter>
                                                </Card>
                                            </Col>
                                            <Col md={2}>
                                                <Button onClick={event=>{
                                                    fileSelector((file, text)=>{
                                                        sendEmail.content = text
                                                        this.setState({
                                                            sendEmail:sendEmail
                                                        })
                                                    }, MIME_TEXT_HTML).click()
                                                }}>
                                                    <IoMdCloudUpload/>
                                                    &nbsp;
                                                    Upload
                                                </Button>
                                            </Col>
                                        </Row>
                                    )
                            }
                        </CardBody>
                        <CardFooter>
                            <Row>
                                <Col>
                                    <Button>Cancel</Button>
                                </Col>
                                <Col>
                                    <Button color="primary" className="float-right" onClick={(event)=>{
                                        this.send()
                                    }}>Submit</Button>
                                </Col>
                            </Row>
                        </CardFooter>
                    </Card>
                }
            </Page>
        );
    }

}