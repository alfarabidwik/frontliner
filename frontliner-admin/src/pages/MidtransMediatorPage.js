import Page from 'components/Page';
import React from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, cloneObject, imageSelector, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import midtransMediatorBuilding from 'assets/img/widget/bank_building.png';
import ImageCropper from "../components/modal/ImageCropper";
import axios from "axios";
import {getData} from "../utils/StorageUtil";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Img from 'react-image'


const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class MidtransMediatorPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      midtransMediators : [],
      midtransMediator:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'name',
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }


  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var midtransMediator = this.state.midtransMediator
    if(isEmpty(midtransMediator.name) || isEmpty(midtransMediator.label) || (isEmpty(this.state.imageBlob)&&isEmpty(midtransMediator.image))){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let midtransMediator = this.state.midtransMediator
    if(!midtransMediator.id && !midtransMediator.active){
      midtransMediator.active = false ;
    }

    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.MIDTRANS_MEDIATOR_SAVE, midtransMediator, null, res => {
        if(res.code===200){
          this.setState({
            midtransMediator:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  saveUpload = () => {
    let midtransMediator = this.state.midtransMediator
    if(!midtransMediator.id && !midtransMediator.active){
      midtransMediator.active = false ;
    }
    let data = new FormData()
    data.append("midtransMediatorDtoGson", JSON.stringify(midtransMediator))
    data.append("multipartFile", this.state.imageBlob)
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.MIDTRANS_MEDIATOR_SAVE_UPLOAD, data, null, res => {
        if(res.code===200){
          this.setState({
            midtransMediator:{},
            image:null,
            imageBlob:null,
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }


  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.MIDTRANS_MEDIATOR_DELETE+"/"+this.state.midtransMediator.id, null, null, res => {
        if(res.code===200){
          this.setState({
            image:null,
            imageBlob:null,
            midtransMediator:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.MIDTRANS_MEDIATORS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          midtransMediators : response.data,
          totalPage: response.totalPage,
          totalElement: response.totalElement,
          pageElement: response.pageElement,
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    let configuration = getData(Global.CONFIGURATION)
    let aspect = 4/4
    if(configuration.midtransMediatorImageDimensionWidth && configuration.midtransMediatorImageDimensionHeight){
      aspect = configuration.midtransMediatorImageDimensionWidth/configuration.midtransMediatorImageDimensionHeight
    }
    return (
        <Page
            title="Midtrans Mediator Detail"
            breadcrumbs={[{ name: 'midtrans Mediator detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this Midtrans Mediator item to your list ?"
              okCallback={this.saveUpload}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this Midtrans Mediator item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>MidtransMediator Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <Col>
                            <Card>
                              <CardHeader>
                                MidtransMediator Image
                              </CardHeader>
                              <CardBody>
                                <CardImg top src={
                                  this.state.image!=null?this.state.image:midtransMediatorBuilding}
                                         onClick={e=>{
                                           if(this.state.image!=null){
                                             this.setState({
                                               imageLink:this.state.image,
                                               openPreview:!this.state.openPreview
                                             })
                                           }
                                         }}/>
                              </CardBody>
                              <CardFooter>
                                <Row>
                                  <Col>
                                    <ButtonGroup className="float-left">
                                      <Button color="primary" onClick={e=>(
                                          imageSelector(file=>{
                                            this.setState({
                                              image:file,
                                              cropperModal:true
                                            })
                                          }).click()
                                      )}>Upload</Button>
                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </CardFooter>
                            </Card>
                          </Col>
                        </Row>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="label">Label</Label>
                              <Input
                                  type="text"
                                  name="label"
                                  value={!isEmpty(this.state.midtransMediator.label)?this.state.midtransMediator.label:""}
                                  onChange={(e) =>{
                                    let midtransMediator = this.state.midtransMediator
                                    if(midtransMediator!=null){
                                      midtransMediator.label = e.target.value
                                      this.setState({
                                        midtransMediator:midtransMediator
                                      })
                                    }
                                  }}
                                  placeholder="Enter Midtrans Mediator label"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.midtransMediator.name)?this.state.midtransMediator.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.midtransMediator
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        midtransMediator:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter Midtrans Mediator name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.midtransMediator.active}
                                callback={(active)=>{
                                  let ctg = this.state.midtransMediator
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      midtransMediator:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    image:null,
                                    imageBlob:null,
                                    midtransMediator:{}
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.midtransMediator!=null && this.state.midtransMediator.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th width="15%">Image</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.midtransMediators.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{(++index)}</th>
                                      <td>{item.name}</td>
                                      <td width="15%">
                                        <Img
                                          className="img-thumbnail"
                                          src={item.imageLink}></Img></td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            midtransMediator:cloneObject(item),
                                            image:cloneObject(item.imageLink)
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="primary" onClick={e=>(
                                            this.setState({
                                              midtransMediator:cloneObject(item),
                                              image:cloneObject(item.imageLink)
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
            axios({
              url: file,
              method: 'GET',
              responseType: 'blob', // important
            }).then((response) => {
              var fileBlob = response.data ;
              this.setState({
                image:file,
                cropperModal:false,
                imageBlob:fileBlob,
              }, () => {
                // this.uploadPhotoProfile(file, login)
              })
            })
          }} cancelCallback={()=>{
            this.setState({
              cropperModal:false,
              image:null,
              imageBlob:null,
            })
          }}/>
        </Page>
    );
  }
}
