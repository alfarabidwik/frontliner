import BasePage from "./BasePage";
import React from 'react'
import queryString from "query-string";
import Page from "../components/Page";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import {CardBody, CardHeader, CardImg, FormGroup, Input, Label} from "reactstrap";
import {
    allIsEmpty,
    currencyFormat,
    imageSelector,
    isEmpty,
    parseDate,
    statusColor,
    weightFormat,
    navigateBack
} from "../utils/Utilities";
import Global, {
    APPLICATION_LIMIT_TYPES,
    CURRENCY_SYMBOL,
    DD_MM_YYYY,
    DD_MM_YYYY_HH_MM_SS, PERIOD_TYPES,
    PRINT_DATA
} from "../utils/Global";
import userProfilePicture from 'assets/img/users/user_pp.png';
import {IoMdOpen} from "react-icons/io";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Table from "react-bootstrap/Table";
import CardFooter from "reactstrap/es/CardFooter";
import Button from "reactstrap/es/Button";
import {IoMdEye, IoMdStats} from "react-icons/io/index";
import {MdPictureAsPdf, MdPrint, MdWarning} from "react-icons/md/index";
import UpdateTaskApplicationStatusModal from '../components/modal/UpdateTaskApplicationStatusModal'
import VoucherInfoModal from '../components/modal/VoucherInfoModal'
import SockJsClient from 'react-stomp'
import PrintInvoiceButton from '../components/Widget/PrintInvoiceButton'
import {Link} from "react-router-dom";
import {storeData} from "../utils/StorageUtil";
import {FormType} from "../model/formbuilder/FormType";

export default class TaskApplicationDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let taskApplicationIdQuery = queryString.parse(this.props.query).id
        let state = this.state
        state.taskApplicationIdQuery = taskApplicationIdQuery
        state.taskApplication = {}
        state.user = {}
        state.updateStatusModal = false
        state.voucherInfoModal = false

        if(isEmpty(taskApplicationIdQuery)){
            this.props.history.goBack();
        }

        this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchTransacationDetail()
    }

    fetchTransacationDetail =() =>{
        this.get(Global.API.TASK_APPLICATION, {
            params :{
                id : this.state.taskApplicationIdQuery
            }
        }, null, response =>{
            if(response.code===200){
                this.setState({
                    taskApplication : response.data,
                    user:response.data.user
                })
            }
        }, true, true)
    }

    render() {
        let user = this.state.user;
        user = user?user:{}

        let taskApplication = this.state.taskApplication?this.state.taskApplication:{}
        let formLaneApplications = taskApplication.formLaneApplications?taskApplication.formLaneApplications:new Array()
        formLaneApplications.forEach((item)=>{
            item.formApplications.sort((a, b) => {return a.form.id-b.form.id})
        })
        let task = taskApplication&&taskApplication.task?taskApplication.task:{}
        let partner = task&&task.partner?task.partner:{}
        let taskCategory = task&&task.taskCategory?task.taskCategory:{}

        let taskApplicationStatus = taskApplication.taskApplicationStatus?taskApplication.taskApplicationStatus:{}

        let village = user.village?user.village:{}
        let district = village.district?village.district:{}
        let city = district.city?district.city:{}
        let province = city.province?city.province:{}
        let courierCost = taskApplication.cost?taskApplication.cost:{}
        let courierFee = taskApplication.courierFee?taskApplication.courierFee:0
        let voucher = taskApplication.voucher?taskApplication.voucher:{}

        let updateStatusModal = this.state.updateStatusModal
        let voucherInfoModal = this.state.voucherInfoModal
        let userAddress = taskApplication.userAddress?taskApplication.userAddress:{}

        return (
            <Page
                title="TaskApplication Detail"
                breadcrumbs={[{ name: 'taskApplication detail', active: true }]}>
                {super.render()}
                <UpdateTaskApplicationStatusModal modal={updateStatusModal} taskApplication={taskApplication} okCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    }, () => {
                        this.fetchTransacationDetail()
                    })
                }} closeCallback={()=>{
                    this.setState({
                        updateStatusModal:false
                    })
                }}/>
                <VoucherInfoModal modal={voucherInfoModal} voucher={taskApplication.voucher} closeCallback={()=>{
                    this.setState({
                        voucherInfoModal:false
                    })
                }}/>
                <Row key={1}>
                    <Col>
                        <Card body>
                            <Row>
                                <Col md={3}>
                                    <Card>
                                        <CardHeader>User</CardHeader>
                                        <CardImg top src={
                                            user.photo!=null?user.photoUrl:userProfilePicture}
                                                 onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                        <CardBody>
                                            <Row>
                                                <Col md={4}>
                                                    Name
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(user.id)?"-":user.firstname+" "+this.state.user.lastname}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Phone
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(user.id)?"-":user.mobilePhone}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Email
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(user.id)?"-":user.email}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4}>
                                                    Address
                                                </Col>
                                                <Col md={8}>
                                                    {isEmpty(village.id)?"-":
                                                        user.address+", "+
                                                        village.name+", "+
                                                        district.name+", "+
                                                        city.name+", "+
                                                        province.name+". "+
                                                        village.postalCode+" "
                                                    }
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <div  className="float-right">
                                                        <span
                                                            color="blue"
                                                            onClick={e=>{this.props.history.push('/userDetail?id='+user.id)}}
                                                            style={{cursor:'pointer'}}>
                                                            Detail
                                                        </span>
                                                        &nbsp;&nbsp;
                                                        <IoMdOpen
                                                            color="blue" style={{cursor:'pointer'}}
                                                            onClick={e=>{
                                                                this.props.history.push('/userDetail?id='+user.id)
                                                            }}/>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                    <br/>
                                    {
                                        taskApplication.id&&(
                                            <span>
                                                <Row>
                                                    <Col>
                                                        <Button style={{fontWeight:'bold', width: '100%', background: statusColor(taskApplicationStatus.id)}}>Current Status : {taskApplicationStatus.name}</Button>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        <Button style={{width: '100%'}} color="danger" onClick={event=>{
                                                            event.preventDefault()
                                                            this.setState({
                                                                updateStatusModal:true
                                                            })
                                                        }}>Update Status &nbsp;<MdWarning/></Button>
                                                    </Col>
                                                </Row>
                                            </span>

                                        )
                                    }
                                </Col>
                                <Col md={9}>
                                    <Card>
                                        <CardHeader>TaskApplication Info</CardHeader>
                                        <CardBody>
                                            <Row>
                                                <Col md={12}>
                                                    <Card>
                                                        <CardHeader>
                                                            Task
                                                        </CardHeader>
                                                        <CardBody>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="resiCode">Partner</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="fullName"
                                                                            readOnly={true}
                                                                            value={partner.fullName}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="courierName">Title</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="title"
                                                                            readOnly={true}
                                                                            value={task.title}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    {
                                                                        task.applicationLimitType===APPLICATION_LIMIT_TYPES[0].name?
                                                                            (
                                                                                <FormGroup>
                                                                                    <Label for="applicationLimitType">Limit Type</Label>
                                                                                    <Input
                                                                                        type="text"
                                                                                        name="applicationLimitType"
                                                                                        readOnly={true}
                                                                                        value={task.applicationLimitType}
                                                                                    />
                                                                                </FormGroup>
                                                                            )
                                                                            :
                                                                            (
                                                                                <FormGroup>
                                                                                    <Label for="courierCode">Limit</Label>
                                                                                    <Input
                                                                                        type="text"
                                                                                        name="limit"
                                                                                        readOnly={true}
                                                                                        value={task.applicationLimitCount}
                                                                                    />
                                                                                </FormGroup>
                                                                            )
                                                                    }
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    {
                                                                        task.periodType===PERIOD_TYPES[0].name?
                                                                            (
                                                                                <FormGroup>
                                                                                    <Label for="periodType">Period</Label>
                                                                                    <Input
                                                                                        type="text"
                                                                                        name="periodType"
                                                                                        readOnly={true}
                                                                                        value={task.periodType}
                                                                                    />
                                                                                </FormGroup>
                                                                            )
                                                                            :
                                                                            (
                                                                                <FormGroup>
                                                                                    <Label for="periodType">Period</Label>
                                                                                    <Input
                                                                                        type="text"
                                                                                        name="periodType"
                                                                                        readOnly={true}
                                                                                        value={parseDate(task.startPeriod, 'dd-MM-yyyy')+" - "+parseDate(task.endPeriod, 'dd-MM-yyyy')}
                                                                                    />
                                                                                </FormGroup>
                                                                            )
                                                                    }
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="weight">Verification Type</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="workerVerificationType"
                                                                            readOnly={true}
                                                                            value={task.workerVerificationType}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="taskCategory">Task Category</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="taskCategory"
                                                                            readOnly={true}
                                                                            value={taskCategory.name}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="service">Application Count</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="applicationCount"
                                                                            readOnly={true}
                                                                            value={task.applicationCount}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="fee">Fee</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="fee"
                                                                            readOnly={true}
                                                                            value={currencyFormat(task.fee, CURRENCY_SYMBOL)}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="fee">Total Fee</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="fee"
                                                                            readOnly={true}
                                                                            value={currencyFormat(taskApplication.totalFee, CURRENCY_SYMBOL)}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="orderReference">Order Reference</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="orderReference"
                                                                            readOnly={true}
                                                                            value={taskApplication.orderReference}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                                <Col>
                                                                    <FormGroup>
                                                                        <Label for="partnerOrderReference">Partner Reference</Label>
                                                                        <Input
                                                                            type="text"
                                                                            name="partnerOrderReference"
                                                                            readOnly={true}
                                                                            value={taskApplication.partnerOrderReference?taskApplication.partnerOrderReference:"-"}
                                                                        />
                                                                    </FormGroup>
                                                                </Col>
                                                            </Row>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Card>
                                                        <CardHeader>Status Log</CardHeader>
                                                        <CardBody>
                                                            <Tabs
                                                                activeKey={this.state.key}
                                                                onSelect={key =>{
                                                                    if(key===1){

                                                                    }else if(key===2){

                                                                    }
                                                                }}>
                                                                <Tab eventKey={1} title={"Status"}>
                                                                    {this.renderTaskApplicationStatus()}
                                                                </Tab>
                                                                <Tab eventKey={2} title={"Partner Status"}>
                                                                    {this.renderPartnerStatus()}
                                                                </Tab>
                                                            </Tabs>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                {
                    formLaneApplications.length>0?(
                        <Row>
                            <Col>
                                <Card>
                                    <CardHeader>Application Form</CardHeader>
                                    <CardBody>
                                        <>
                                            {
                                                formLaneApplications.map((item, index)=>{
                                                    return (
                                                        <Row key={index}>
                                                            <Col>
                                                                <Card>
                                                                    <CardHeader>{item.title}</CardHeader>
                                                                    <CardBody>
                                                                        <Row>
                                                                            {
                                                                                item.formApplications.map((value, index1) => {
                                                                                    return (
                                                                                        <Col sm={4}>
                                                                                            <Card>
                                                                                                <CardHeader>{value.form.title}</CardHeader>
                                                                                                <CardBody>
                                                                                                    {
                                                                                                        value.form.type === FormType.IMAGE ?
                                                                                                            (
                                                                                                                <>
                                                                                                                    <CardImg src={value.imageUrl} style={{width:'100px', height:'100px', textAlign:'center'}} onClick={event => {
                                                                                                                        this.openLightBoxSingleImage(value.imageUrl)
                                                                                                                    }}/>
                                                                                                                </>
                                                                                                            )
                                                                                                            :
                                                                                                            (
                                                                                                                <>
                                                                                                                    <Row>
                                                                                                                        <Col>ID</Col>
                                                                                                                        <Col>:</Col>
                                                                                                                        <Col>{value.valueId?value.valueId:"-"}</Col>
                                                                                                                    </Row>
                                                                                                                    <Row>
                                                                                                                        <Col>Value</Col>
                                                                                                                        <Col>:</Col>
                                                                                                                        <Col>{value.value}</Col>
                                                                                                                    </Row>
                                                                                                                </>
                                                                                                            )
                                                                                                    }
                                                                                                </CardBody>
                                                                                            </Card>
                                                                                        </Col>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </Row>
                                                                    </CardBody>
                                                                </Card>
                                                            </Col>
                                                        </Row>
                                                    )
                                                })
                                            }
                                        </>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    ):(null)
                }
                {
                    taskApplication.referralMembers&&(
                        <Row>
                            <Col>
                            {
                                this.renderReferralTable()
                            }
                            </Col>
                        </Row>
                    )
                }
                <SockJsClient
                    url={Global.BASE_URL}
                    topics={[Global.API.LISTEN_TASK_APPLICATION+"/"+taskApplication.id]}
                    onMessage={(message) => {
                        this.successToast(message)
                        this.fetchTransacationDetail()
                    }}
                    ref={ (client) => { this.clientRef = client }} />
            </Page>
        );
    }

    renderPartnerStatus = () =>{
        let taskApplication = this.state.taskApplication
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Status Order</th>
                    <th>Status Code</th>
                    <th>statusLabel</th>
                    <th>statusInfo</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>
                {
                    isEmpty(taskApplication.taskPartnerStatuses)?
                        null
                        :
                        taskApplication.taskPartnerStatuses.map((item, index)=>(
                            <tr key={index}>
                                <td scope="row">{++index}</td>
                                <td>{item.statusOrder?item.statusOrder:"-"}</td>
                                <td>{item.statusCode?item.statusCode:""}</td>
                                <td>{item.statusLabel?item.statusLabel:""}</td>
                                <td>{item.statusInfo?item.statusInfo:""}</td>
                                <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            </tr>

                        ))
                }
                </tbody>
            </Table>
        )
    }

    renderTaskApplicationStatus = () =>{
        return (
            <Table hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Note</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>
                {
                    isEmpty(this.state.taskApplication.pvTaskApplicationStatuses)?
                        null
                        :
                        this.state.taskApplication.pvTaskApplicationStatuses.map((item, index)=>(
                            <tr key={index}>
                                <th scope="row">{++index}</th>
                                <td>{isEmpty(item.taskApplicationStatus.name)?"-":item.taskApplicationStatus.name}</td>
                                <td>{isEmpty(item.taskApplicationStatus.description)?"-":item.taskApplicationStatus.description}</td>
                                <td>{isEmpty(item.note)?"-":item.note}</td>
                                <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            </tr>

                        ))
                }
                </tbody>
            </Table>
        )
    }
    renderReferralTable = ()=>{
        let taskApplication = this.state.taskApplication
        let referralMembers = taskApplication.referralMembers?taskApplication.referralMembers:[]
        return(
            <>
                <Card>
                    <CardHeader>Referred User</CardHeader>
                    <CardBody>
                        <Row>
                            <Col>
                                <Table hover>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Fullname</th>
                                        <th>Email</th>
                                        <th>Mobile Phone</th>
                                        <th>Agent Code</th>
                                        <th>Organization</th>
                                        <th>Verification</th>
                                        <th>Status</th>
                                        <th>Updated</th>
                                        <th>View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        referralMembers.map((item, index)=>{
                                            return (
                                                <tr key={item.id}>
                                                    <th scope="row">{(++index)}</th>
                                                    <td>{item.user.fullname}</td>
                                                    <td>{item.user.email}</td>
                                                    <td>{item.user.mobilePhone}</td>
                                                    <td>{item.user.agentCode}</td>
                                                    <td>{item.user.organization}</td>
                                                    <td>{item.user.verificationStatus}</td>
                                                    <td>{item.user.userStatus}</td>
                                                    <td>{parseDate(item.user.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                    <td>
                                              <span title="See Detail" style={{cursor:'pointer'}}>
                                          <IoMdOpen color="blue" onClick={ e => (
                                              this.props.history.push('/userDetail?id='+item.id)
                                          )}/>
                                          </span>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </>
        )
    }


}