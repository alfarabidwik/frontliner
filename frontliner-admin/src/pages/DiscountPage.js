import BasePage from "./BasePage";
import React from "react"
import Page from "../components/Page";
import Global, {DD_MMMM_YYYY, DD_MMMM_YYYY_HH_MM_SS} from "../utils/Global";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem, DropdownMenu, DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';import CardFooter from "reactstrap/es/CardFooter";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {parseDate, sortirMap} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
// import DiscountModal from "../components/modal/DiscountModal";
import DiscountProductModal from '../components/modal/DiscountProductModal'
import DiscountContentModal from '../components/modal/DiscountContentModal'
import {IoMdOpen} from "react-icons/io";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";




export default class DiscountPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            modalDeleteConfirm:false,
            modalAddDiscount:false,
            ascending:true,
            sortir:'p.created',
            products:[],
            product:{},
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            edit:false,
            tabkey:"ACTIVE"
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(true)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(true)
            })
        }
    }


    fetchAll = (progressing) => {
        this.get(Global.API.DISCOUNTS, {
            params:{
                period:this.state.tabkey,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page: this.state.page-1
            }
        }, null, response =>{
            if(response.code === 200){
                this.setState({
                    products : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        this.setState({
            modalDeleteConfirm:false
        }, () => {
            this.get(Global.API.DISCOUNT_DELETE+"/"+this.state.product.id, null, null, response=>{
                if(response.code===200){
                    this.setState({
                        product:{}
                    }, () => {
                        this.fetchAll(true)
                    })
                }
            }, true, true)
        })
    }

    refresh = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(progressing)
        })
    )

    closeDiscountModal = () =>{
        this.setState({
            modalAddDiscount:false,
            product:{},
            edit:!this.state.edit
        })
    }

    closeDiscountContentModal = () =>{
        this.setState({
            discountContentModal:false
        }, () => {
            this.refresh(this.state.ascending, this.state.sortir,true)
        })
    }


    openDiscountContentModal  = (product) =>{
        this.setState({
            product:product,
            discountContentModal:true
        })
    }


    render() {
        return (
            <Page
                title="Discount"
                breadcrumbs={[{ name: 'discount', active: true }]}
                className="TablePage">
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this product item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <DiscountProductModal
                    showing={this.state.modalAddDiscount}
                    edit={this.state.edit}
                    product={this.state.product}
                    close={this.closeDiscountModal}
                    openDiscountContentModal={this.openDiscountContentModal}
                    refresh={this.refresh}
                />
                <DiscountContentModal
                    showing={this.state.discountContentModal}
                    edit={this.state.edit}
                    product={this.state.product}
                    close={this.closeDiscountContentModal}
                    refresh={this.refresh}
                />
                <Row>
                    <Col>
                    <Card>
                        <Tabs
                            activeKey={this.state.tabkey}
                            onSelect={key =>{
                                this.setState({
                                    tabkey:key
                                }, () => {
                                    this.refresh(this.state.ascending, this.state.sortir, true)
                                })
                            }
                            }>
                            <Tab eventKey={Global.ACTIVE} title="Active">
                                {this.renderProductTable()}
                            </Tab>
                            <Tab eventKey={Global.EXPIRED} title="Expired">
                                {this.renderProductTable()}
                            </Tab>
                            <Tab eventKey={Global.ALL} title="All">
                                {this.renderProductTable()}
                            </Tab>
                        </Tabs>
                        </Card>
                    </Col>
                </Row>
                {super.render()}
            </Page>
        );
    }

    renderProductTable = () =>{
        return(
            <Card>
            <CardBody>
                <Row>
                    <Col md={2}>
                        Sort By :
                        <UncontrolledButtonDropdown key={1}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    sortirMap(this.state.sortir)
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name", true))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created", true))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated", true))}>Updated</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "d.start_date", true))}>Start Date</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "d.end_date", true))}>End Date</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={2}>
                        Sortir :
                        <UncontrolledButtonDropdown key={2}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    this.state.ascending?"Ascending":"Descending"
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir, true))}>Descending</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={3}>
                        <SearchInput
                            value={this.state.search}
                            onChange={e=>{
                                this.setState({
                                    search:e.target.value
                                }, () => {
                                    if(this.state.search===''){
                                        deleteParam(this.props, 'page')
                                    }
                                })
                            }}
                            onEnter={e=>{
                                if(this.state.search!==''){
                                    deleteParam(this.props, 'page')
                                }
                            }}
                            placeholder={"Type and enter to search..."}
                        />
                    </Col>
                    <Col md={5}>
                        {
                            this.state.tabkey===Global.ACTIVE?(
                                <ButtonGroup className="float-right">
                                    <Button color="primary" onClick={e=>{
                                        this.setState({
                                            modalAddDiscount:!this.state.modalAddDiscount
                                        })
                                    }}>Add Product</Button>
                                </ButtonGroup>
                            ):null
                        }
                    </Col>
                </Row>
                <Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Content/Stock</th>
                            <th>Discount</th>
                            <th>Period</th>
                            <th>Updated</th>
                            {
                                (this.state.tabkey===Global.ACTIVE)?
                                    (
                                        <th>Edit</th>
                                    )
                                    :
                                    null
                            }
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.products.map((item, index)=>(
                                <tr key={index}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.name}</td>
                                    <th>{item.discountItem}/{item.inventoryDiscountQuantity}</th>
                                    <th>
                                        {
                                            item.minDiscount===item.maxDiscount?item.minDiscount+"%":item.minDiscount+"% - "+item.maxDiscount+"%"

                                        }
                                    </th>
                                    <td><p style={{color:(new Date(item.endDiscountDate).getTime()<=new Date().getTime())?"red":"green"}}>{parseDate(item.startDiscountDate)} - {parseDate(item.endDiscountDate)}</p></td>
                                    <td>{parseDate(item.updated, DD_MMMM_YYYY_HH_MM_SS)}</td>
                                    {
                                        (this.state.tabkey===Global.ACTIVE || this.state.tabkey===Global.ALL)?
                                            (
                                                <td>
                                                <IoMdOpen color="blue" onClick={ e => {
                                                    e.preventDefault()
                                                    this.openDiscountContentModal(item)
                                                }}/>
                                                </td>
                                            )
                                            :
                                            null
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </Row>
            </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }



}