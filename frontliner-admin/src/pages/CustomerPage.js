import Page from 'components/Page';
import React from 'react';
import {Link} from 'react-router-dom'
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem, DropdownMenu, DropdownToggle, FormGroup, Input, Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'
import {allIsEmpty, isEmpty, parseDate, sortirMap} from "../utils/Utilities";
import ItemOption from "../components/Widget/ItemOption";
import {
    IoMdArrowDown, IoMdArrowRoundDown,
    IoMdArrowRoundUp,
    IoMdArrowUp,
    IoMdExpand,
    IoMdEye,
    IoMdMenu,
    IoMdOpen
} from "react-icons/io/index";
import {Collapse} from 'reactstrap'
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";
import Img from 'react-image'


import {FiArrowDownCircle, FiArrowUpCircle} from "react-icons/fi/index";
import DateInput from "../components/Widget/DateInput";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class CustomerPage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
        customers : [],
        customer:{},
        modalDeleteConfirm:false,
        ascending:false,
        sortir:'c.created',
        search:"",
        page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
        totalPage:0,
        totalElement:0,
        pageElement:0,
        createdStartDate:null,
        createdEndDate:null,
        transactionStartDate:null,
        transactionEndDate:null,
        genders:[],
        gender:{},
        customerStatuses:[],
        customerStatus:{},
        provinces:[],
        cities:[],
        districts:[],
        villages:[],
        province:{},
        city:{},
        district:{},
        village:{},
        customerFilterExpand:false,
        customerTransactionExpand:false,
    }

    this.fetchGenders((genders)=>this.setState({genders:genders}))
    this.fetchAllCustomerStatus()
    this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))



  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
            })
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
    }

  fetchAll = (ascending, sortir, search, progressing) => {
    this.get(Global.API.CUSTOMERS, {
      params:{
          page:this.state.page-1,
          ascending:ascending,
          sortir:sortir,
          search:search,
          customerCreatedStart:this.state.createdStartDate,
          customerCreatedEnd:this.state.createdEndDate,
          customerTransactionCreatedStart:this.state.transactionStartDate,
          customerTransactionCreatedEnd:this.state.transactionEndDate,
          genderId:this.state.gender.id,
          customerStatusId:this.state.customerStatus.id,
          provinceId:this.state.province.id,
          cityId:this.state.city.id,
          districtId:this.state.district.id,
          villageId:this.state.village.id,
      }
    }, null, res =>{
      if(res.code === 200){
        this.setState({
          customers : res.data,
          totalPage: res.totalPage,
          totalElement: res.totalElement,
          pageElement: res.pageElement,
        })
      }
    }, progressing, true);
  }

    fetchAllCustomerStatus = () =>{
        this.get(Global.API.CUSTOMER_STATUSES, null, null, res =>{
            if(res.code === 200){
                this.setState({
                    customerStatuses : res.data
                })
            }
        }, false, false);
    }



   confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.CUSTOMER_DELETE+"/"+this.state.customer.id, null, null, res => {
        if(res.code===200){
          this.setState({
            category:null
          }, () => {
              this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
          })
        }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

    refreshCustomer = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )


    render() {
        var i = 0 ;
        let customer = this.state.customer?this.state.customer:{}
        let genders = this.state.genders?this.state.genders:[]
        let gender = customer.gender?customer.gender:{}

    return (

        <Page
            title="Customers"
            title="Customers"
            breadcrumbs={[{ name: 'customer', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this customer item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
            <Row key={1}>
                <Col>
                    <Card>
                        <CardHeader onClick={event=>{
                            this.setState({
                                customerFilterExpand:!this.state.customerFilterExpand
                            })
                        }}>
                            {
                                (this.state.customerFilterExpand)?(<FiArrowUpCircle/>):((<FiArrowDownCircle/>))
                            }
                            &nbsp;Profile Filter
                        </CardHeader>
                        <Collapse isOpen={this.state.customerFilterExpand}>
                            <CardHeader>
                                <Row>
                                    <Col md={3}>
                                        <Label for="createdStartDate">Reg/Created Start</Label>
                                        <DateInput
                                            id="createdStartDate"
                                            value={this.state.createdStartDate}
                                            maxdate={this.state.createdEndDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    createdStartDate : value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter start date"
                                        />
                                    </Col>
                                    <Col md={3}>
                                        <Label for="createdEndDate">Reg/Created End</Label>
                                        <DateInput
                                            id="createdEndDate"
                                            mindate={this.state.createdStartDate}
                                            value={this.state.createdEndDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    createdEndDate :  value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter end date"
                                        />
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Gender"}
                                                objects={this.state.genders}
                                                disable={(this.state.customer!=null && this.state.customer.id)}
                                                default={!isEmpty(this.state.gender)?this.state.gender.id:null}
                                                callback={(gender)=>{
                                                    this.setState({
                                                        gender:gender
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Status"}
                                                objects={this.state.customerStatuses}
                                                default={!isEmpty(this.state.customerStatus)?this.state.customerStatus.id:null}
                                                callback={(customerStatus)=>{
                                                    this.setState({
                                                        customerStatus:customerStatus
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Province"}
                                                objects={this.state.provinces}
                                                default={!allIsEmpty(this.state.province)?this.state.province.id:null}
                                                callback={(province)=>{
                                                    this.setState({
                                                        province:!isEmpty(province.id)?province:{},
                                                        cities:[],
                                                        city:!isEmpty(province.id)?this.state.city:{},
                                                        districts:[],
                                                        district:!isEmpty(province.id)?this.state.district:{},
                                                        villages:[],
                                                        village:!isEmpty(province.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(province.id)){
                                                            this.fetchCities(province.id, cities=>{
                                                                this.setState({
                                                                    cities:cities
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"City"}
                                                objects={this.state.cities}
                                                default={!allIsEmpty(this.state.city)?this.state.city.id:null}
                                                callback={(city)=>{
                                                    this.setState({
                                                        city:!isEmpty(city.id)?city:{},
                                                        districts:[],
                                                        district:!isEmpty(city.id)?this.state.district:{},
                                                        villages:[],
                                                        village:!isEmpty(city.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(city.id)){
                                                            this.fetchDistricts(city.id, districts =>{
                                                                this.setState({
                                                                    districts:districts
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"District"}
                                                objects={this.state.districts}
                                                default={!allIsEmpty(this.state.district)?this.state.district.id:null}
                                                callback={(district)=>{
                                                    this.setState({
                                                        district:!isEmpty(district.id)?district:{},
                                                        villages:[],
                                                        village:!isEmpty(district.id)?this.state.village:{},
                                                    }, () => {
                                                        if(!isEmpty(district.id)){
                                                            this.fetchVillages(district.id, villages=>{
                                                                this.setState({
                                                                    villages:villages
                                                                })
                                                            })
                                                        }
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Village"}
                                                objects={this.state.villages}
                                                default={!allIsEmpty(this.state.village)?this.state.village.id:null}
                                                callback={(village)=>{
                                                    this.setState({
                                                        village:!isEmpty(village)?village:{},
                                                    }, () => {
                                                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                    })
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </CardHeader>
                        </Collapse>
                    </Card>
                </Col>
            </Row>
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader onClick={event=>{
                            this.setState({
                                customerTransactionExpand:!this.state.customerTransactionExpand
                            })
                        }}>
                            {
                                (this.state.customerTransactionExpand)?(<FiArrowUpCircle/>):((<FiArrowDownCircle/>))
                            }
                            &nbsp;Transaction Filter
                        </CardHeader>
                        <Collapse isOpen={this.state.customerTransactionExpand}>
                            <CardHeader>
                                <Row>
                                    <Col md={3}>
                                        <Label for="transactionStartDate">Transaction Start</Label>
                                        <DateInput
                                            id="transactionStartDate"
                                            maxdate={this.state.transactionEndDate}
                                            value={this.state.transactionStartDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    transactionStartDate : value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter transaction start date"
                                        />
                                    </Col>
                                    <Col md={3}>
                                        <Label for="transactionEndDate">Transaction End</Label>
                                        <DateInput
                                            id="transactionEndDate"
                                            mindate={this.state.transactionStartDate}
                                            value={this.state.transactionEndDate}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                this.setState({
                                                    transactionEndDate : value?value:null
                                                }, () => {
                                                    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                                                })
                                            }}
                                            placeholder="Enter transaction end date"
                                        />
                                    </Col>
                                </Row>
                            </CardHeader>
                        </Collapse>
                    </Card>
                </Col>
            </Row>
            <Row key={3}>
            <Col>
              <Card>
                  <CardHeader>Sortir</CardHeader>
                  <CardBody>
                    <Row>
                          <Col md={2}>
                              <UncontrolledButtonDropdown key={1}>
                                  <DropdownToggle
                                      caret
                                      color="white"
                                      className="text-capitalize m-1">
                                      {
                                          sortirMap(this.state.sortir)
                                      }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "c.created", true))}>Created</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "c.firstname", true))}>Name</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "c.email", true))}>Email</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "c.phoneNumber", true))}>Phone Number</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "c.updated", true))}>Last Update</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "trx.startCreated", true))}>Trx Date</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "transactionCount", true))}>Trx Count</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "favoriteCount", true))}>Fav Count</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "cartCount", true))}>Cart Count</DropdownItem>
                                  </DropdownMenu>
                              </UncontrolledButtonDropdown>
                          </Col>
                          <Col md={2}>
                              <UncontrolledButtonDropdown key={2}>
                                  <DropdownToggle
                                      caret
                                      color="white"
                                      className="text-capitalize m-1">
                                      {
                                          this.state.ascending?"Ascending":"Descending"
                                      }
                                  </DropdownToggle>
                                  <DropdownMenu>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                      <DropdownItem onClick={e=>(this.refreshCustomer(false, this.state.sortir, true))}>Descending</DropdownItem>
                                  </DropdownMenu>
                              </UncontrolledButtonDropdown>
                          </Col>
                          <Col md={3}>
                              <SearchInput
                                  placeholder={"Search name, email, phone..."}
                                  value={this.state.search}
                                  onChange={e=>{
                                      this.setState({
                                          search:e.target.value
                                      }, () => {
                                          if(this.state.search===''){
                                              deleteParam(this.props, "page")
                                          }
                                      })
                                  }}
                                  onEnter={e=>{
                                      if(this.state.search!==''){
                                          deleteParam(this.props, "page")
                                      }
                                  }}
                              />
                          </Col>
                          <Col md={5}>
                              <ButtonGroup className="float-right">
                                  <Button color="primary" onClick={e=>{
                                      this.props.history.push('/customerDetail')
                                  }}>Add Customer</Button>
                              </ButtonGroup>
                          </Col>
                      </Row>
                    <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Fullname</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>City</th>
                              <th>Status</th>
                              <th>Trx/Fav/Cart</th>
                              <th>Trx during</th>
                              <th>Image</th>
                              <th>Created</th>
                              <th>Updated</th>
                              <th>View</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.customers.map((item, index)=>{
                                  return (
                                      <tr key={item.id}>
                                          <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                          <td>{item.firstname+" "+item.lastname}</td>
                                          <td>{item.email}</td>
                                          <td>{item.phoneNumber}</td>
                                          <td>{item.cityName}</td>
                                          <td>{item.statusName}</td>
                                          <td>
                                              {isEmpty(item.transactionCount)?0:item.transactionCount}
                                              /
                                              {isEmpty(item.favoriteCount)?0:item.favoriteCount}
                                              /
                                              {isEmpty(item.cartCount)?0:item.cartCount}</td>
                                          <td>
                                              {
                                                  (item.transactionStartDate)&&(parseDate(item.transactionStartDate, DD_MM_YYYY_HH_MM_SS))
                                              }
                                              {
                                                  (item.transactionStartDate)&&(<span>&nbsp;/&nbsp;</span>)
                                              }

                                              {
                                                  (item.transactionEndDate)&&(parseDate(item.transactionEndDate, DD_MM_YYYY_HH_MM_SS))
                                              }
                                          </td>
                                          <td width="5%">{
                                              item.image!=undefined?
                                                  <Img
                                                      className="img-thumbnail"
                                                      src={item.imageLink}
                                                      onClick={e=>{
                                                          this.setState({
                                                              imageLink:item.imageLink,
                                                              openPreview:true
                                                          })
                                                      }}
                                                  />
                                                  :
                                                  "-"
                                          }</td>
                                          <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                          <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                          <td>
                                              <span title="See Detail" style={{cursor:'pointer'}}>
                                          <IoMdOpen color="blue" onClick={ e => (
                                              this.props.history.push('/customerDetail?id='+item.id)
                                          )}/>
                                          </span>
                                          </td>
                                      </tr>
                                  )
                              })
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
            </Col>
          </Row>
        </Page>
    );
  }
};
