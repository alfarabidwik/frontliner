import BasePage from "./BasePage";
import React from "react"
import Page from "../components/Page";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS, DD_MMMM_YYYY, DD_MMMM_YYYY_HH_MM_SS} from "../utils/Global";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem, DropdownMenu, DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';import CardFooter from "reactstrap/es/CardFooter";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {parseDate, sortirMap} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
import {MdDelete, MdEdit} from "react-icons/md";
import VoucherItemInventoryModal from '../components/modal/VoucherItemInventoryModal'
import {IoMdOpen} from "react-icons/io";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";



export default class VoucherPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            modalDeleteConfirm:false,
            ascending:true,
            sortir:'v.created',
            products:[],
            product:{},
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            tabkey:"ACTIVE"
        }

    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(true)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(true)
            })
        }
    }


    fetchAll = (progressing) => {
        this.get(Global.API.VOUCHERS, {
            params:{
                status:this.state.tabkey,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page: this.state.page-1
            }
        }, null, response =>{
            if(response.code === 200){
                this.setState({
                    products : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        this.setState({
            modalDeleteConfirm:false
        }, () => {
            this.get(Global.API.DISCOUNT_DELETE+"/"+this.state.product.id, null, null, response=>{
                if(response.code===200){
                    this.setState({
                        product:{}
                    }, () => {
                        this.fetchAll(true)
                    })
                }
            }, true, true)
        })
    }

    refresh = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(progressing)
        })
    )




    render() {
        return (
            <Page
                title="Voucher"
                breadcrumbs={[{ name: 'voucher', active: true }]}
                className="TablePage">
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this product item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <Row>
                    <Col>
                    <Card>
                        <Tabs
                            activeKey={this.state.tabkey}
                            onSelect={key =>{
                                this.setState({
                                    tabkey:key
                                }, () => {
                                    this.refresh(this.state.ascending, this.state.sortir, true)
                                })
                            }
                            }>
                            <Tab eventKey={Global.ACTIVE} title="Active">
                                {this.renderProductTable()}
                            </Tab>
                            <Tab eventKey={Global.EXPIRED} title="Expired">
                                {this.renderProductTable()}
                            </Tab>
                            <Tab eventKey={Global.INACTIVE} title="Inactive">
                                {this.renderProductTable()}
                            </Tab>
                            <Tab eventKey={Global.ALL} title="All">
                                {this.renderProductTable()}
                            </Tab>
                        </Tabs>
                        <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                    </Card>
                    </Col>
                </Row>
                {super.render()}
            </Page>
        );
    }

    renderProductTable = () =>{
        return(
            <CardBody>
                <Row>
                    <Col md={2}>
                        Sort By :
                        <UncontrolledButtonDropdown key={1}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    sortirMap(this.state.sortir)
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "v.name", true))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "v.created", true))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "v.updated", true))}>Updated</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "v.start_period", true))}>Start Period</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "v.end_period", true))}>End Period</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={2}>
                        Sortir :
                        <UncontrolledButtonDropdown key={2}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    this.state.ascending?"Ascending":"Descending"
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir, true))}>Descending</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={3}>
                        <SearchInput
                            placeholder={"Type and enter to search..."}
                            value={this.state.search}
                            onChange={e=>{
                                this.setState({
                                    search:e.target.value
                                }, () => {
                                    if(this.state.search===''){
                                        deleteParam(this.props, 'page')
                                    }
                                    // this.refresh(this.state.ascending, this.state.sortir, false)
                                })
                            }}
                            onEnter={e=>{
                                if(this.state.search!==''){
                                    deleteParam(this.props, 'page')
                                }

                            }}
                        />
                    </Col>
                    <Col md={5}>
                        {
                            this.state.tabkey===Global.ACTIVE?(
                                <ButtonGroup className="float-right">
                                    <Button color="primary" onClick={e=>{
                                        this.props.history.push('/voucherDetail')
                                    }}>Add Voucher</Button>
                                </ButtonGroup>
                            ):null
                        }
                    </Col>
                </Row>
                <Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Code</th>
                            <th>Point</th>
                            <th>Owner</th>
                            <th>Theme</th>
                            <th>Period</th>
                            <th>Benefit</th>
                            <th>Aggregate</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.products.map((item, index)=>(
                                <tr key={index}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.name}</td>
                                    <td>{item.description}</td>
                                    <td>{item.code}</td>
                                    <td>{item.pointExchange}</td>
                                    <td>{item.ownerType}</td>
                                    <td>
                                        {
                                            item.themeColor&&(
                                                <div className="float-left" style={{backgroundColor:item.themeColor.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                            )
                                        }
                                    </td>
                                    <td>
                                        {
                                        item.periodType===Global._ALL_TIME?Global.ALL_TIME:(null)

                                        }
                                        {
                                        item.periodType===Global._STATIC?(null):(null)

                                        }
                                        {
                                        item.periodType===Global._DYNAMIC?(
                                        <span>
                                        {parseDate(item.startPeriod, DD_MMMM_YYYY)}
                                        &nbsp; - &nbsp;
                                            {parseDate(item.endPeriod, DD_MMMM_YYYY)}
                                        </span>
                                        ):(null)

                                        }

                                    </td>
                                    <td>
                                        {item.benefitLabel}
                                    </td>
                                    <td>
                                        {item.byAggregate}
                                    </td>
                                    <td>{parseDate(item.created, DD_MMMM_YYYY_HH_MM_SS)}</td>
                                    <td>{parseDate(item.updated, DD_MMMM_YYYY_HH_MM_SS)}</td>
                                    <td>
                                        <IoMdOpen color="blue" onClick={ e => {
                                            e.preventDefault()
                                            this.props.history.push('/voucherDetail?id='+item.id)
                                        }}/>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </Row>
            </CardBody>
        )
    }



}