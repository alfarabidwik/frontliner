import Page from 'components/Page';
import React from 'react';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader, CardImg,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {
  BANNER_IMAGE_MAX_FILE_SIZE,
  DD_MM_YYYY,
  DD_MM_YYYY_HH_MM_SS, MIME_PNG,
  ONLINE_SHOP_MAX_FILE_SIZE
} from "../utils/Global";
import {allIsEmpty, cloneObject, imageSelector, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import building from 'assets/img/widget/bank_building.png';
import ImageCropper from "../components/modal/ImageCropper";
import axios from "axios";
import {getData} from "../utils/StorageUtil";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Img from 'react-image'
import WarningLabel from "../components/Widget/WarningLabel";


const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class OnlineShopPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      onlineShops : [],
      onlineShop:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'name',
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }


  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var onlineShop = this.state.onlineShop
    if(isEmpty(onlineShop.name) || isEmpty(onlineShop.label) || isEmpty(onlineShop.link) || (isEmpty(this.state.imageBlob) && isEmpty(onlineShop.image))){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let onlineShop = this.state.onlineShop
    if(!onlineShop.id && !onlineShop.active){
      onlineShop.active = false ;
    }

    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.ONLINE_SHOP_SAVE, onlineShop, null, res => {
        if(res.code===200){
          this.setState({
            onlineShop:{}
          }, () => {
            this.fetchAll()
          })
        }
      }, true, true);
    })
  }

  saveUpload = () => {
    let onlineShop = this.state.onlineShop
    if(!onlineShop.id && !onlineShop.active){
      onlineShop.active = false ;
    }
    let data = new FormData()
    data.append("onlineShopDtoGson", JSON.stringify(onlineShop))
    data.append("multipartFile", this.state.imageBlob)
    this.setState({
      modalAddConfirm:false
    }, () => {
      this.post(Global.API.ONLINE_SHOP_SAVE_UPLOAD, data, null, res => {
        if(res.code===200){
          this.setState({
            onlineShop:{},
            image:null,
            imageBlob:null,
          }, () => {

            this.fetchAll()
          })
        }
      }, true, true);
    })
  }


  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.ONLINE_SHOP_DELETE+"/"+this.state.onlineShop.id, null, null, res => {
        if(res.code===200){
          this.setState({
            image:null,
            imageBlob:null,
            onlineShop:{}
          }, () => {
            this.fetchAll()
          })
        }
      },true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.ONLINE_SHOPS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          onlineShops : response.data,
          totalPage: response.totalPage,
          totalElement: response.totalElement,
          pageElement: response.pageElement,
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )


  render() {
    let configuration = getData(Global.CONFIGURATION)
    let aspect = 4/4
    if(configuration.onlineShopImageWidth && configuration.onlineShopImageHeight){
      aspect = configuration.onlineShopImageWidth/configuration.onlineShopImageHeight
    }
    return (
        <Page
            title="Online Shop Detail"
            breadcrumbs={[{ name: 'Online Shop detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this online shop item to your list ?"
              okCallback={this.saveUpload}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this online shop item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>OnlineShop Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <Col>
                            <Card>
                              <CardHeader>
                                OnlineShop Image
                              </CardHeader>
                              <CardBody>
                                <Row>
                                  <Col>
                                    <CardImg top src={
                                      this.state.image!=null?this.state.image:building}
                                             onClick={e=>{
                                               if(this.state.image!=null){
                                                 this.setState({
                                                   imageLink:this.state.image,
                                                   openPreview:!this.state.openPreview
                                                 })
                                               }
                                             }}
                                             onError={(elm)=>this.defaultImage(elm, building)}/>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <WarningLabel message={"*Use PNG Format / Max "+ONLINE_SHOP_MAX_FILE_SIZE+" Kb"}/>
                                  </Col>
                                </Row>
                              </CardBody>
                              <CardFooter>
                                <Row>
                                  <Col>
                                    <ButtonGroup className="float-left">
                                      <Button color="primary" onClick={e=>(
                                          imageSelector(file=>{
                                            this.downloadBlobFile(file, (imageBlob)=>{
                                              this.setState({
                                                image:file,
                                                imageBlob:imageBlob,
                                              })
                                            })
                                          }, MIME_PNG, ONLINE_SHOP_MAX_FILE_SIZE).click()
                                      )}>Upload</Button>
                                    </ButtonGroup>
                                  </Col>
                                </Row>
                              </CardFooter>
                            </Card>
                          </Col>
                        </Row>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="label">Label</Label>
                              <Input
                                  type="text"
                                  name="label"
                                  value={!isEmpty(this.state.onlineShop.label)?this.state.onlineShop.label:""}
                                  onChange={(e) =>{
                                    let onlineShop = this.state.onlineShop
                                    if(onlineShop!=null){
                                      onlineShop.label = e.target.value
                                      this.setState({
                                        onlineShop:onlineShop
                                      })
                                    }
                                  }}
                                  placeholder="Enter online shop label"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.onlineShop.name)?this.state.onlineShop.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.onlineShop
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        onlineShop:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter online shop name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="link">Link</Label>
                              <Input
                                  type="text"
                                  name="link"
                                  value={!isEmpty(this.state.onlineShop.link)?this.state.onlineShop.link:""}
                                  onChange={(e) =>{
                                    let onlineShop = this.state.onlineShop
                                    if(onlineShop!=null){
                                      onlineShop.link = e.target.value
                                      this.setState({
                                        onlineShop:onlineShop
                                      })
                                    }
                                  }}
                                  placeholder="Enter online shop link"
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.onlineShop.active}
                                callback={(active)=>{
                                  let ctg = this.state.onlineShop
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      onlineShop:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    image:null,
                                    imageBlob:null,
                                    onlineShop:{}
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.onlineShop!=null && this.state.onlineShop.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th width="15%">Image</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.onlineShops.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td>{item.link}</td>
                                      <td width="15%">
                                        <Img
                                          className="img-thumbnail"
                                          src={item.imageLink}></Img></td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            onlineShop:cloneObject(item),
                                            image:cloneObject(item.imageLink)
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="primary" onClick={e=>(
                                            this.setState({
                                              onlineShop:cloneObject(item),
                                              image:cloneObject(item.imageLink)
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
