import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardTitle,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Input,
    Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import taskBanner from 'assets/img/banner/banner_task.png';
import Page from "../components/Page";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import Global, {
    APPLICATION_LIMIT_TYPES,
    BANNER_IMAGE_MAX_FILE_SIZE,
    CURRENCY_SYMBOL,
    DD_MM_YYYY_HH_MM_SS,
    MIME_JPEG,
    PERIOD_TYPES, PRODUCT_KEUANGAN_ID, REGULAR_ID, WORKER_VERIFICATION_TYPES, TASK_TYPES,
} from "../utils/Global";
import {
    allIsEmpty,
    currencyFormat,
    deleteParam,
    imageSelector,
    isEmpty,
    parseDate,
    sortirMap,
    weightFormat
} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit} from "react-icons/md";
import {IoMdEye, IoMdOpen} from "react-icons/io";
import {getData} from "../utils/StorageUtil";

import ImageCropper from "../components/modal/ImageCropper";
import SearchInput from '../components/SearchInput'
import ActiveLabel from "../components/Widget/ActiveLabel";
import Pagination from '../components/Pagination'

import {ContentState, convertToRaw, EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import WarningLabel from "../components/Widget/WarningLabel";

import TagsInput from 'react-tagsinput'
import DateInput from "../components/Widget/DateInput";
import NumberInput from "../components/Widget/NumberInput";
import PartnerTaskSelectorModal from '../components/modal/PartnerTaskSelectorModal'


export default class TaskDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let state = this.state
        state.task = {}
        state.periodType = PERIOD_TYPES[0]
        state.applicationLimitType = APPLICATION_LIMIT_TYPES[0]
        state.workerVerificationType = WORKER_VERIFICATION_TYPES[0]
        state.taskCategories = []
        state.groupForms = []
        state.bannerImage  = null
        state.taskIdQuery = queryString.parse(this.props.query).id
        state.taskCategory = {}
        state.modalAddConfirm = false
        state.modalDeleteConfirm = false
        state.inventories = []
        state.inventory = null
        state.ascending = true
        state.sortir = 'created'
        state.quantityModal = false
        state.page = queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
        state.totalPage = 0
        state.totalElement = 0
        state.pageElement = 0
        state.descriptionEditorState = EditorState.createEmpty()
        state.partnerTaskSelectorModalMode = false
        this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchGroupForms()
        this.fetchTaskDetail(this.state.taskIdQuery)
        this.fetchAllCategories()

    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                // this.fetchAllInventories(this.state.task.id, this.state.ascending, this.state.sortir, true)
            })
        }

    }

    onDescriptionEditorStateChange= (descriptionEditorState) => {
        let task = this.state.task
        task.description = draftToHtml(convertToRaw(descriptionEditorState.getCurrentContent()))
        this.setState({
            task:task,
            descriptionEditorState:descriptionEditorState,
        });
    }

    onAdditionalInfoEditorStateChange= (additionalInfoEditorState) => {
        let task = this.state.task
        task.additionalInfo = draftToHtml(convertToRaw(additionalInfoEditorState.getCurrentContent()))
        this.setState({
            task:task,
            additionalInfoEditorState:additionalInfoEditorState,
        });
    }


    confirmSave = () =>{
        var task = this.state.task
        var bannerImage = this.state.bannerImage
        if(allIsEmpty(task.title, task.taskCategory, task.fee, task.validatingTimeInHour)){
            this.showDialog("Warning", "Please complete all required form fields")
            return
        }
        if(task.taskCategory.id===PRODUCT_KEUANGAN_ID){
            if(!task.taskAddressLink){
                this.showDialog("Warning", "Please complete all required form fields")
                return ;
            }
            if(!task.callbackId){
                this.showDialog("Warning", "Please complete all required form fields")
                return ;
            }
        }
        this.setState({
            modalAddConfirm:true
        })

    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.inventory!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.INVENTORY_DELETE+"/"+this.state.inventory.id, null, null, res => {
                    if(res.code===200){
                        this.setState({
                            inventory:null
                        }, () => {
                            // this.fetchAllInventories(this.state.task.id, this.state.ascending, this.state.sortir, true)
                        })
                    }
                }, true, true);
            })
        }
    }

    saveUpload = () => {
        var formData = new FormData();
        let task = this.state.task;
        let applicationLimitType = this.state.applicationLimitType
        task.applicationLimitType = applicationLimitType.name
        let periodType = this.state.periodType
        task.periodType = periodType.name
        let workerVerificationType = this.state.workerVerificationType
        task.workerVerificationType = workerVerificationType.name


        formData.append("taskDtoGson", JSON.stringify(task))
        formData.append("multipartFile", this.state.imageBlob);
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.TASK_SAVE_UPLOAD, formData, null, res=>{
                if(res.code===200){
                    this.setState({
                        task:res.data
                    }, () => {
                        this.showDialog("Success", res.message)
                        this.fetchTaskDetail(this.state.task.id)
                        // changeParam(this.props, 'id', this.state.task.id)
                    })
                }
            },  true, true)
        })
    }


    fetchTaskDetail = (id) => {
        if(id!=null){
            this.get(Global.API.TASK, {
                params :{
                    id : id
                }
            }, null, response=>{
                if(response.code===200){
                    let task = response.data
                    let descriptionEditorState = this.state.descriptionEditorState
                    if(task.description){
                        const descriptionContentBlock = htmlToDraft(task.description);
                        if (descriptionContentBlock) {
                            let contentState = ContentState.createFromBlockArray(descriptionContentBlock.contentBlocks);
                            descriptionEditorState = EditorState.createWithContent(contentState);
                        }else{
                            descriptionEditorState = EditorState.createEmpty()
                        }
                    }else{
                        descriptionEditorState = EditorState.createEmpty()
                    }
                    let additionalInfoEditorState = this.state.additionalInfoEditorState
                    if(task.additionalInfo){
                        const additionalInfoContentBlock = htmlToDraft(task.additionalInfo);
                        if (additionalInfoContentBlock) {
                            let contentState = ContentState.createFromBlockArray(additionalInfoContentBlock.contentBlocks);
                            additionalInfoEditorState = EditorState.createWithContent(contentState);
                        }else{
                            additionalInfoEditorState = EditorState.createEmpty()
                        }
                    }else{
                        additionalInfoEditorState = EditorState.createEmpty()
                    }

                    let periodType = this.state.periodType?this.state.periodType:PERIOD_TYPES[0]
                    if(task.periodType){
                        PERIOD_TYPES.forEach(value => {
                            if(value.name===task.periodType){
                                periodType=value
                            }
                        })
                    }

                    let applicationLimitType = this.state.applicationLimitType?this.state.applicationLimitType:APPLICATION_LIMIT_TYPES[0]
                    if(task.applicationLimitType){
                        APPLICATION_LIMIT_TYPES.forEach(value => {
                            if(value.name===task.applicationLimitType){
                                applicationLimitType = value ;
                            }
                        })
                    }

                    let workerVerificationType = this.state.workerVerificationType?this.state.workerVerificationType:WORKER_VERIFICATION_TYPES[0]
                    if(task.workerVerificationType){
                        WORKER_VERIFICATION_TYPES.forEach(value => {
                            if(value.name===task.workerVerificationType){
                                workerVerificationType = value
                            }
                        })
                    }
                    this.setState({
                        task:task,
                        taskCategory:task.taskCategory,
                        bannerImage:task.bannerImageUrl,
                        periodType:periodType,
                        applicationLimitType:applicationLimitType,
                        workerVerificationType:workerVerificationType,
                        descriptionEditorState:descriptionEditorState,
                        additionalInfoEditorState:additionalInfoEditorState
                    }, () => {
                        this.fetchAllCategories()
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }


    fetchAllCategories = () =>{
        let task = this.state.task?this.state.task:{}
        let type = task.id?task.type:null
        this.get(Global.API.TASK_CATEGORIES, {
            params:{
                active:true,
                type : type,
            }
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    taskCategories : res.data
                })
            }
        }, true, true);
    }

    fetchGroupForms = () => {
        this.get(Global.API.GROUP_FORMS, {
            params:{
                page: this.state.page-1,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
            }
        }, null, response=>{
            if(response.code === 200){
                this.setState({
                    groupForms : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,

                })
            }
        }, true, true);
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
        })
    }

    refreshInventory = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            // this.fetchAllInventories(this.state.task.id, this.state.ascending, this.state.sortir, progressing)
        })
    )

    openQuantityModal=()=>{
        this.setState({
            quantityModal:true
        })
    }
    closeQuantityModal=()=>{
        this.setState({
            quantityModal:false
        })
    }


    render() {
        var ids = [];
        var labels = [];
        this.state.taskCategories.map((item, index)=>{
            ids.push(item.id);
            labels.push(item.name);
        })

        let configuration = getData(Global.CONFIGURATION)
        let aspect = 36/10
        if(configuration.taskBannerWidth && configuration.taskBannerHeight){
            aspect = configuration.taskBannerWidth/configuration.taskBannerHeight
        }

        let descriptionEditorState = this.state.descriptionEditorState
        let additionalInfoEditorState = this.state.additionalInfoEditorState

        let task = this.state.task?this.state.task:{}

        let tags = this.state.tags?this.state.tags:[]
        let periodType = this.state.periodType?this.state.periodType:{}
        let applicationLimitType = this.state.applicationLimitType?this.state.applicationLimitType:{}
        let workerVerificationType = this.state.workerVerificationType?this.state.workerVerificationType:{}

        return (
            <Page
                title="Task Form"
                breadcrumbs={[{ name: 'task form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this task item to your list ?"
                    okCallback={this.saveUpload}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this inventory item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <PartnerTaskSelectorModal
                    close={()=>{
                        this.setState({
                            partnerTaskSelectorModalMode:false
                        })
                    }}
                    onSelected={(partner)=>{
                        if(partner){
                            task.partner = partner
                            this.setState({
                                task:task,
                                partnerTaskSelectorModalMode:false
                            })
                        }
                    }}
                    showing={this.state.partnerTaskSelectorModalMode}
                />
                {/*<InventoryQuantityModal modal={this.state.quantityModal} inventory={this.state.inventory} okCallback={this.updateQuantity} closeCallback={this.closeQuantityModal}/>*/}
                <Row key={1}>
                    <Col>
                        <Card>
                            <CardHeader>Task</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label for="name">Title</Label>
                                            <Input
                                                type="text"
                                                name="title"
                                                value={(task!=null && task.title !=null)?task.title:""}
                                                onChange={(e) =>{
                                                    e.preventDefault()
                                                    let task = this.state.task
                                                    if(task!=null){
                                                        task.title = e.target.value
                                                        this.setState({
                                                            task:task
                                                        })
                                                    }
                                                }}
                                                placeholder="Enter title"
                                            />
                                            <WarningLabel show={!task.title} message={"Enter title"}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label for="name">Subtitle</Label>
                                            <Input
                                                type="text"
                                                name="subtitle"
                                                value={(task!=null && task.subtitle !=null)?task.subtitle:""}
                                                onChange={(e) =>{
                                                    e.preventDefault()
                                                    let itm = this.state.task
                                                    if(itm!=null){
                                                        itm.subtitle = e.target.value
                                                        this.setState({
                                                            task:itm
                                                        })
                                                    }
                                                }}
                                                placeholder="Enter subtitle"
                                            />
                                            <WarningLabel show={!task.subtitle} message={"Enter subtitle"}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Period"}
                                                objects={PERIOD_TYPES}
                                                default={periodType?periodType.id:null}
                                                hideOptionLabel={true}
                                                callback={(periodType)=>{
                                                    let task = this.state.task
                                                    if(task!=null){
                                                        task.periodType = periodType.name
                                                        this.setState({
                                                            task:task,
                                                            periodType:periodType
                                                        })
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                        {
                                            (task.periodType===PERIOD_TYPES[1].name)?(
                                                <Row>
                                                    <Col>
                                                        <Label for="startPeriod">Start Period</Label>
                                                        <DateInput
                                                            id="startPeriod"
                                                            maxdate={task.endPeriod}
                                                            mindate={new Date()}
                                                            value={task.startPeriod}
                                                            onChange={(e) =>{
                                                                let value = e.target.value
                                                                task.startPeriod = value
                                                                this.setState({
                                                                    task : task
                                                                })
                                                            }}
                                                            placeholder="Enter start period">
                                                        </DateInput>
                                                    </Col>
                                                    <Col>
                                                        <Label for="endPeriod">End Period</Label>
                                                        <DateInput
                                                            id="endPeriod"
                                                            maxdate={null}
                                                            mindate={task.startPeriod}
                                                            value={task.endPeriod}
                                                            onChange={(e) =>{
                                                                let value = e.target.value
                                                                task.endPeriod = value
                                                                this.setState({
                                                                    task : task
                                                                })
                                                            }}
                                                            placeholder="Enter end period">
                                                        </DateInput>
                                                    </Col>
                                                </Row>
                                            ):(null)
                                        }
                                        <FormGroup>
                                            <ItemOption
                                                title={"Apply Limit"}
                                                objects={APPLICATION_LIMIT_TYPES}
                                                default={applicationLimitType?applicationLimitType.id:null}
                                                hideOptionLabel={true}
                                                callback={(applicationLimitType)=>{
                                                    let task = this.state.task
                                                    if(task!=null){
                                                        task.applicationLimitType = applicationLimitType.name
                                                        this.setState({
                                                            task:task,
                                                            applicationLimitType:applicationLimitType
                                                        })
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                        {
                                            task.applicationLimitType===APPLICATION_LIMIT_TYPES[1].name&&(
                                                <Row>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="applicationLimitCount">Limit Count</Label>
                                                            <NumberInput
                                                                name="applicationLimitCount"
                                                                className="form-control"
                                                                value={!isEmpty(task.applicationLimitCount)?task.applicationLimitCount:""}
                                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                                    e.preventDefault()
                                                                    task.applicationLimitCount = floatvalue
                                                                    this.setState({
                                                                        task: task
                                                                    })
                                                                }}
                                                                placeholder="0"
                                                                maxLength={15}>
                                                            </NumberInput>
                                                        </FormGroup>
                                                    </Col>
                                                    <Col>
                                                        <FormGroup>
                                                            <Label for="applicationCount">Application Count</Label>
                                                            <NumberInput
                                                                name="applicationCount"
                                                                className="form-control"
                                                                value={!isEmpty(task.applicationCount)?task.applicationCount:""}
                                                                placeholder="0"
                                                                maxLength={15}
                                                                disabled={true}>
                                                            </NumberInput>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                            )
                                        }
                                        <FormGroup>
                                            <ItemOption
                                                title={"Worker Verification Type"}
                                                objects={WORKER_VERIFICATION_TYPES}
                                                default={workerVerificationType?workerVerificationType.id:null}
                                                hideOptionLabel={true}
                                                callback={(workerVerificationType)=>{
                                                    let task = this.state.task
                                                    if(task!=null){
                                                        task.workerVerificationType = workerVerificationType.name
                                                        this.setState({
                                                            task:task,
                                                            workerVerificationType:workerVerificationType
                                                        })
                                                    }
                                                }}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <Card>
                                            <CardImg top src={
                                                this.state.bannerImage?this.state.bannerImage:taskBanner} onClick={e=>{
                                                imageSelector(file=>{
                                                    this.setState({
                                                        bannerImage:file,
                                                        cropperModal:true
                                                    })
                                                }, MIME_JPEG, BANNER_IMAGE_MAX_FILE_SIZE).click()
                                            }}
                                            onError={(elm)=>this.defaultImage(elm, taskBanner)}/>
                                            <CardBody>
                                                <CardTitle>Task Image (Scale : 36/10)</CardTitle>
                                                <Row>
                                                    <Col>
                                                        <WarningLabel message={"*Use Jpeg Format / Max "+BANNER_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                                                    </Col>
                                                </Row>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    task:null,
                                                                    bannerImage:null
                                                                })
                                                            )}>Cancel</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                imageSelector(file=>{
                                                                    this.setState({
                                                                        bannerImage:file,
                                                                        cropperModal:true
                                                                    })
                                                                }, MIME_JPEG, BANNER_IMAGE_MAX_FILE_SIZE).click()
                                                            }}>Upload</Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={6}>
                                        <FormGroup>
                                            <ItemOption
                                                title={"Select Category"}
                                                objects={this.state.taskCategories}
                                                default={this.state.taskCategory?this.state.taskCategory.id:null}
                                                callback={(taskCategory)=>{
                                                    let prd = this.state.task
                                                    if(prd!=null){
                                                        prd.taskCategory = taskCategory
                                                        this.setState({
                                                            task:prd,
                                                            taskCategory:taskCategory,
                                                        })
                                                    }
                                                }}
                                            />
                                            <WarningLabel show={!task.taskCategory} message={"Select Category"}/>
                                        </FormGroup>
                                    </Col>
                                    {
                                        (task.taskCategory&& task.taskCategory.id===PRODUCT_KEUANGAN_ID)&&
                                            (
                                                <>
                                                    <Col>
                                                    <FormGroup>
                                                        <Label for="taskAddressLink">Task Address Link</Label>
                                                        <Input
                                                            name={"taskAddressLink"}
                                                            value={task.taskAddressLink?task.taskAddressLink:""}
                                                            onChange={event => {
                                                                event.preventDefault()
                                                                task.taskAddressLink = event.target.value
                                                                this.setState({
                                                                    task:task
                                                                })
                                                            }}
                                                            placeholder={"Example : https://...."}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col sm={2}>
                                                        <FormGroup>
                                                            <Label for="callbackId">Callback Id</Label>
                                                            <Input
                                                                name={"callbackId"}
                                                                value={task.callbackId?task.callbackId:""}
                                                                onChange={event => {
                                                                    event.preventDefault()
                                                                    task.callbackId = event.target.value
                                                                    this.setState({
                                                                        task:task
                                                                    })
                                                                }}
                                                                placeholder={"Unique callback id"}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </>
                                            )
                                    }
                                    {
                                        ((!task.type || (task.type===TASK_TYPES[1] && task.taskCategory && task.taskCategory.id===REGULAR_ID )))&&(
                                            <Col>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Group Form"}
                                                        objects={this.state.groupForms}
                                                        default={task.groupForm?task.groupForm.id:null}
                                                        callback={(groupForm)=>{
                                                            let task = this.state.task
                                                            if(task!=null){
                                                                task.groupForm = groupForm
                                                                this.setState({
                                                                    task:task,
                                                                    groupForm:groupForm,
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        )
                                    }

                                </Row>
                                <Row>
                                    <Col>
                                        <FormGroup>
                                            <Label for="fee">Fee</Label>
                                            <NumberInput
                                                name="fee"
                                                className="form-control"
                                                value={!isEmpty(task.fee)?task.fee:""}
                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                    e.preventDefault()
                                                    task.fee = floatvalue
                                                    this.setState({
                                                        task: task
                                                    })
                                                }}
                                                placeholder="0"
                                                maxLength={15}>
                                            </NumberInput>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup>
                                            <Label for="validatingTimeInHour">Time Validating</Label>
                                            <NumberInput
                                                name="validatingTimeInHour"
                                                className="form-control"
                                                value={!isEmpty(task.validatingTimeInHour)?task.validatingTimeInHour:""}
                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                    e.preventDefault()
                                                    task.validatingTimeInHour = floatvalue
                                                    this.setState({
                                                        task: task
                                                    })
                                                }}
                                                placeholder="0"
                                                maxLength={15}>
                                            </NumberInput>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <FormGroup>
                                            <ActiveOption
                                                label={"Published"}
                                                trueLabel={"Published"}
                                                falseLabel={"Unpublished"}
                                                callback={(active)=>{
                                                    let prd = this.state.task
                                                    if(prd!=null){
                                                        prd.published = active
                                                        this.setState({
                                                            task:prd
                                                        })
                                                    }

                                                }}
                                                default={task?task.published:false}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup>
                                            <Label for="partner">Partner</Label>
                                            <Input
                                                name={"partner"}
                                                value={task.partner?task.partner.fullName:""}
                                                contentEditable={false}
                                                focusable={false}
                                                onClick={event => {
                                                    event.preventDefault()
                                                    this.setState({
                                                        partnerTaskSelectorModalMode:true
                                                    })
                                                }}
                                                placeholder={"--Select Partner--"}
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup>
                                            <Label for="suggestion">Suggestion</Label>
                                            <NumberInput
                                                name="suggestion"
                                                className="form-control"
                                                value={!isEmpty(task.suggestion)?task.suggestion:""}
                                                onChangeEvent={(e, maskedvalue, floatvalue) => {
                                                    e.preventDefault()
                                                    task.suggestion = floatvalue
                                                    this.setState({
                                                        task: task
                                                    })
                                                }}
                                                placeholder="0"
                                                maxLength={15}>
                                            </NumberInput>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Card>
                                            <CardHeader>Description</CardHeader>
                                            <CardBody>
                                                <Editor
                                                    editorState={descriptionEditorState}
                                                    onEditorStateChange={this.onDescriptionEditorStateChange}/>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Card>
                                            <CardHeader>Additional Info</CardHeader>
                                            <CardBody>
                                                <Editor
                                                    editorState={additionalInfoEditorState}
                                                    onEditorStateChange={this.onAdditionalInfoEditorStateChange}/>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <ButtonGroup className="float-left">
                                            <Button color="danger" onClick={e => (
                                                this.setState({
                                                    task:null
                                                }, () => {
                                                    this.props.history.goBack();
                                                })
                                            )}> Back </Button>
                                        </ButtonGroup>
                                    </Col>
                                    <Col>
                                        <ButtonGroup className="float-right">
                                            <Button color="primary" onClick={e=>{
                                                e.preventDefault()
                                                this.confirmSave()
                                            }}> {
                                                (this.state.task!=null && this.state.task.id!=null)?"Update":"Create"
                                            } </Button>

                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <ImageCropper src={this.state.bannerImage} aspect={aspect} show={this.state.cropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        this.setState({
                            bannerImage:file,
                            cropperModal:false,
                            imageBlob:fileBlob,
                        })
                    })
                }} cancelCallback={()=>{
                    this.setState({
                        cropperModal:false,
                        bannerImage:task.bannerImageLink,
                        imageBlob:null,
                    })
                }}/>

            </Page>
        );
    }



}