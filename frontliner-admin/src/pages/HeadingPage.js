import Page from './../components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, cloneObject, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import ActiveLabel from "../components/Widget/ActiveLabel";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class HeadingPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      headings : [],
      heading:{},
      categories:[],
      category:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'created',
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var heading = this.state.heading
    if(isEmpty(heading.name)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let heading = this.state.heading
    if(!heading.id && !heading.active){
      heading.active = false ;
    }

    this.setState({
      modalAddConfirm:false
    }, () => {
      kitchen.post(Global.API.HEADING_SAVE, heading, null).then(res => {
        if(res.data.code===200){
          this.setState({
            heading:{}
          }, () => {
            this.fetchAll()
          })
        }else{

        }
      }, true, true);
    })
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      kitchen.get(Global.API.HEADING_DELETE+"/"+this.state.heading.id).then(res => {
        if(res.data.code===200){
          this.setState({
            heading:{}
          }, () => {
            this.fetchAll()
          })
        }else{

        }
      });
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.HEADINGS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          headings : response.data
        })
      }
    }, true, true);
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )

  render() {
    return (
        <Page
            title="Heading Detail"
            breadcrumbs={[{ name: 'heading detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this heading item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this heading item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>Heading Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.heading.name)?this.state.heading.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.heading
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        heading:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter heading name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="iconClassName">Icon Class Name</Label>
                              <Input
                                  type="text"
                                  name="iconClassName"
                                  value={!isEmpty(this.state.heading.iconClassName)?this.state.heading.iconClassName:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.heading
                                    if(ctg!=null){
                                      ctg.iconClassName = e.target.value
                                      this.setState({
                                        heading:ctg
                                      })
                                    }
                                  }}
                                  placeholder="Enter Icon Class Name"
                              />
                            </FormGroup>

                            <FormGroup>
                              <Label for="name">Product Item</Label>
                              <Input
                                  type="text"
                                  name="productItem"
                                  value={(this.state.heading)?this.state.heading.productItem:""}
                                  readOnly
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Inventory Item</Label>
                              <Input
                                  type="text"
                                  name="inventoryItem"
                                  value={(this.state.heading)?this.state.heading.inventoryItem:""}
                                  readOnly
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label for="name">Quantity</Label>
                              <Input
                                  type="text"
                                  name="quantity"
                                  value={(this.state.heading)?this.state.heading.quantity:""}
                                  readOnly
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.heading.active}
                                callback={(active)=>{
                                  let ctg = this.state.heading
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      heading:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    heading:null
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.heading && this.state.heading.id)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Icon Class Name</th>
                                <th>Product/Qty</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.headings.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td>{item.iconClassName}</td>
                                      <td></td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                        <Button color="danger" onClick={e => {
                                          this.setState({
                                            heading:cloneObject(item)
                                          }, () => {
                                            this.confirmDelete()
                                          })
                                        }}>
                                          <MdDelete/>
                                        </Button>
                                        &nbsp;
                                        <Button color="primary" onClick={e=>(
                                            this.setState({
                                              heading:cloneObject(item)
                                            })
                                        )}>
                                          <MdEdit/>
                                        </Button>
                                      </td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
