import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup, ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import Page from "../components/Page";
import kitchen from "../utils/AxiosInstance";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {imageSelector, parseDate, sortirMap, weightForma, currencyFormat, weightFormat} from "../utils/Utilities";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit, MdSearch} from "react-icons/md";
import SearchInput from '../components/SearchInput'
import {IoMdEye} from "react-icons/io/index";
import InventoryQuantityModal from '../components/modal/InventoryQuantityModal'

import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";
import ActiveLabel from "../components/Widget/ActiveLabel";
import {getData} from "../utils/StorageUtil";



export default class InventoryPage extends BasePage{

    constructor(props) {
        super(props);
        this.state = {
            product:{},
            categories:[],
            image :null,
            category:{},
            modalDeleteConfirm:false,
            inventories:[],
            inventory:null,
            ascending:true,
            sortir:'created',
            search:"",
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            quantityModal:false,
        }
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAllInventories(true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAllInventories(true)
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.inventory!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.INVENTORY_DELETE+"/"+this.state.inventory.id, null, null, response=>{
                    if(response.code===200){
                        this.successToast(response.message)
                        this.setState({
                            inventory:null
                        }, () => {
                            this.fetchAllInventories(true)
                        })
                    }
                }, true, true);
            })
        }
    }

    fetchAllInventories = (progressing) =>{
        this.get(Global.API.INVENTORIES, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    inventories : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }

        }, progressing, true)
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
        })
    }

    refreshInventory = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAllInventories(progressing)
        })
    )

    openQuantityModal=()=>{
        this.setState({
            quantityModal:true
        })
    }
    closeQuantityModal=()=>{
        this.setState({
            quantityModal:false
        })
    }

    updateQuantity=(item, quantity, note, substraction)=>{
        let form = new FormData()
        form.append("inventoryId", item.id)
        form.append("newStock", quantity)
        form.append("note", note)
        if(substraction){
            form.append("substraction", true)
        }else{
            form.append("substraction", false)
        }
        this.post(Global.API.INVENTORY_UPDATE_STOCK, form, null, response=>{
            if(response.code===200){
                this.fetchAllInventories(true)
                this.closeQuantityModal()
            }
        }, true, true)
    }

    render() {
        return (
            <Page
                title="Inventory"
                breadcrumbs={[{ name: 'inventory', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this inventory item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <InventoryQuantityModal modal={this.state.quantityModal} inventory={this.state.inventory} okCallback={this.updateQuantity} closeCallback={this.closeQuantityModal}/>
                {
                    <Row key={2}>
                        <Col>
                            <Card className="mb-6">
                                <CardHeader>Inventory</CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col md={3}>
                                            Sort By :
                                            <UncontrolledButtonDropdown key={1}>
                                                <DropdownToggle
                                                    caret
                                                    color="white"
                                                    className="text-capitalize m-1">
                                                    {
                                                        sortirMap(this.state.sortir.toString())
                                                    }
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "typeName", true))}>Type</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "product.name", true))}>Product</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "product.brand.name", true))}>Brand</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "product.category.name", true))}>Category</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "product.heading.name", true))}>Heading</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "created", true))}>Created</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "quantity", true))}>Quantity</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "weight", true))}>Weight</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "price", true))}>Price</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "updated", true))}>Last Updated</DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledButtonDropdown>
                                        </Col>
                                        <Col md={3}>
                                            Sortir :
                                            <UncontrolledButtonDropdown key={2}>
                                                <DropdownToggle
                                                    caret
                                                    color="white"
                                                    className="text-capitalize m-1">
                                                    {
                                                        this.state.ascending?"Ascending":"Descending"
                                                    }
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshInventory(false, this.state.sortir, true))}>Descending</DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledButtonDropdown>
                                        </Col>
                                        <Col md={3}>
                                            <SearchInput
                                                placeholder={"Type and enter to search..."}
                                                value={this.state.search}
                                                onChange={e=>{
                                                    this.setState({
                                                        search:e.target.value
                                                    }, () => {
                                                        if(this.state.search===''){
                                                            deleteParam(this.props, 'page')
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.search!==''){
                                                        deleteParam(this.props, 'page')
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <ButtonGroup className="float-right">
                                                <Button color="primary" onClick={e=>{
                                                    this.props.history.push("/inventoryDetail")
                                                }}>Add Inventory</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Table hover>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Type</th>
                                                <th>Brand</th>
                                                <th>Product</th>
                                                <th>Heading</th>
                                                <th>Category</th>
                                                <th>Type</th>
                                                <th>Size</th>
                                                <th>Color</th>
                                                <th>Quantity</th>
                                                <th>Weight @item</th>
                                                <th>Price @item</th>
                                                <th>Image</th>
                                                <th>Active</th>
                                                <th>Last Update</th>
                                                <th>Edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.inventories.map((item, index)=>(
                                                    <tr key={index}>
                                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                        <td>{item.typeName}</td>
                                                        <td>{item.product.brand?item.product.brand.name:"-"}</td>
                                                        <td>{item.product.name}</td>
                                                        <td>{item.product.heading?item.product.heading.name:"-"}</td>
                                                        <td>{item.product.category?item.product.category.name:"-"}</td>
                                                        <td>{item.typeName}</td>
                                                        <td>{item.size?item.size.name:""}</td>
                                                        <td>
                                                            {
                                                                item.colors.map((color, index1)=>(
                                                                    color!=null?
                                                                        <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                                        :
                                                                        null
                                                                ))
                                                            }
                                                        </td>
                                                        <td><Button onClick={event=>{
                                                            event.preventDefault()
                                                            this.setState({
                                                                inventory:item
                                                            }, () => {
                                                                this.openQuantityModal()
                                                            })
                                                        }}><MdEdit/></Button>&nbsp;{item.quantity}</td>
                                                        <td>{weightFormat(item.weight)}</td>
                                                        <td>{currencyFormat(item.price, CURRENCY_SYMBOL)}</td>
                                                        <td width="5%">
                                                            <Button onClick={event=>{
                                                                event.preventDefault()
                                                                this.openLightBox(item.inventoryImages)
                                                            }}><IoMdEye/></Button>
                                                        </td>
                                                        <td><ActiveLabel active={item.active}/></td>
                                                        <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                        <td>
                                                            <Button color="danger" onClick={e => {
                                                                this.setState({
                                                                    inventory:item
                                                                }, () => {
                                                                    this.confirmDelete()
                                                                })
                                                            }}>
                                                                <MdDelete/>
                                                            </Button>
                                                            &nbsp;
                                                            <Button color="primary" onClick={ e => (
                                                                this.props.history.push("/inventoryDetail?productId="+item.product.id+"&inventoryId="+item.id)
                                                            )}>
                                                                <MdEdit/>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </CardBody>
                                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                            </Card>
                        </Col>
                    </Row>
                }
            </Page>
        );
    }

}