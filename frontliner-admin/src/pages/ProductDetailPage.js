import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardTitle,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    FormGroup,
    Input,
    Label,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import shoppingBag from 'assets/img/products/shopping-bag.png';
import Page from "../components/Page";
import ActiveOption from "../components/Widget/ActiveOption";
import ItemOption from "../components/Widget/ItemOption";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE} from "../utils/Global";
import {
    allIsEmpty,
    currencyFormat,
    deleteParam,
    imageSelector,
    parseDate,
    sortirMap,
    weightFormat
} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {MdDelete, MdEdit} from "react-icons/md";
import {IoMdEye} from "react-icons/io";
import InventoryQuantityModal from '../components/modal/InventoryQuantityModal'
import {getData} from "../utils/StorageUtil";

import ImageCropper from "../components/modal/ImageCropper";
import SearchInput from '../components/SearchInput'
import ActiveLabel from "../components/Widget/ActiveLabel";
import Pagination from '../components/Pagination'

import {ContentState, convertToRaw, EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import WarningLabel from "../components/Widget/WarningLabel";

import TagsInput from 'react-tagsinput'


export default class ProductDetailPage extends BasePage{

    constructor(props) {
        super(props);
        let state = this.state
        state.product = {}
        state.brands = []
        state.categories = []
        state.headings = []
        state.image  = null
        state.productIdQuery = queryString.parse(this.props.query).id
        state.category = {}
        state.heading = {}
        state.modalAddConfirm = false
        state.modalDeleteConfirm = false
        state.inventories = []
        state.inventory = null
        state.ascending = true
        state.sortir = 'created'
        state.quantityModal = false
        state.page = queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
        state.totalPage = 0
        state.totalElement = 0
        state.pageElement = 0
        state.editorState = EditorState.createEmpty()
        this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAllBrands()
        this.fetchAllHeading()
        this.fetchProductDetail(this.state.productIdQuery)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAllInventories(this.state.product.id, this.state.ascending, this.state.sortir, true)
            })
        }

    }

    onEditorStateChange= (editorState) => {
        let product = this.state.product
        product.description = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        this.setState({
            product:product,
            editorState:editorState,
        });
    }


    confirmSave = () =>{
        var product = this.state.product
        var image = this.state.image
        if(allIsEmpty(image, product.taskCategory, product.name)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            this.setState({
                modalAddConfirm:true
            })
        }
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        if(this.state.inventory!=null){
            this.setState({
                modalDeleteConfirm:false
            }, () => {
                this.get(Global.API.INVENTORY_DELETE+"/"+this.state.inventory.id, null, null, res => {
                    if(res.code===200){
                        this.setState({
                            inventory:null
                        }, () => {
                            this.fetchAllInventories(this.state.product.id, this.state.ascending, this.state.sortir, true)
                        })
                    }
                }, true, true);
            })
        }
    }

    saveUpload = () => {
        var formData = new FormData();
        let product = this.state.product;
        let tags = this.state.tags
        let tagString = tags.toString().replace(/,/gi, " ");
        product.relatedHashtag = tagString

        formData.append("productDtoGson", JSON.stringify(product))
        formData.append("multipartFile", this.state.imageBlob);
        this.setState({
            modalAddConfirm:false
        }, () => {
            this.post(Global.API.PRODUCT_SAVE_UPLOAD, formData, null, res=>{
                if(res.code===200){
                    this.setState({
                        product:res.data
                    }, () => {
                        this.showDialog("Success", res.message)
                        this.fetchProductDetail(this.state.product.id)
                        // changeParam(this.props, 'id', this.state.product.id)
                    })
                }
            },  true, true)
        })
    }


    fetchProductDetail = (id) => {
        if(id!=null){
            this.get(Global.API.PRODUCT, {
                params :{
                    id : id
                }
            }, null, response=>{
                if(response.code===200){
                    let product = response.data
                    let editorState = this.state.editorState
                    if(product.description){
                        const descriptionContentBlock = htmlToDraft(product.description);
                        if (descriptionContentBlock) {
                            const contentState = ContentState.createFromBlockArray(descriptionContentBlock.contentBlocks);
                            editorState = EditorState.createWithContent(contentState);
                        }else{
                            editorState = EditorState.createEmpty()
                        }
                    }else{
                        editorState = EditorState.createEmpty()
                    }
                    let tags = []
                    if(product.relatedHashtag){
                        tags = product.relatedHashtag.split(" ")
                    }
                    this.setState({
                        product:product,
                        tags:tags,
                        brand:response.data.brand,
                        taskCategory:response.data.taskCategory,
                        heading:response.data.heading,
                        taskCategories:response.data.heading?response.data.heading.taskCategories:[],
                        image:response.data.imageLink,
                        editorState:editorState
                    }, () => {
                        this.fetchAllCategories(this.state.heading.id)
                        this.fetchAllInventories(this.state.product.id, this.state.ascending, this.state.sortir, true)
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true)
        }
    }

    fetchAllHeading = () =>{
        this.get(Global.API.HEADINGS, {
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    headings : res.data
                })
            }
        }, true, true);
    }


    fetchAllCategories = (headingId) =>{
        this.get(Global.API.TASK_CATEGORIES, {
            params:{
                active:true,
                headingId:headingId
            }
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    taskCategories : res.data
                })
            }
        }, true, true);
    }

    fetchAllBrands = () =>{
        this.get(Global.API.BRANDS, {
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    brands : res.data
                })
            }
        }, true, true);
    }


    fetchAllInventories = (productId, ascending, sortir, progressing) =>{
        this.get(Global.API.INVENTORIES, {
            params:{
                productId:productId,
                ascending:ascending,
                sortir:sortir,
                page:this.state.page-1,
                search:this.state.search
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    inventories : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }

        }, progressing, true)
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
        })
    }

    refreshInventory = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAllInventories(this.state.product.id, this.state.ascending, this.state.sortir, progressing)
        })
    )

    openQuantityModal=()=>{
        this.setState({
            quantityModal:true
        })
    }
    closeQuantityModal=()=>{
        this.setState({
            quantityModal:false
        })
    }

    updateQuantity=(item, quantity, note, substraction)=>{
        let form = new FormData()
        form.append("inventoryId", item.id)
        form.append("newStock", quantity)
        form.append("note", note)
        if(substraction){
            form.append("substraction", true)
        }else{
            form.append("substraction", false)
        }
        this.post(Global.API.INVENTORY_UPDATE_STOCK, form, null, response=>{
            if(response.code===200){
                this.fetchAllInventories(this.state.product.id, this.state.ascending, this.state.sortir, true)
                this.closeQuantityModal()
            }
        }, true, true)
    }


    render() {
        var ids = [];
        var labels = [];
        this.state.taskCategories.map((item, index)=>{
            ids.push(item.id);
            labels.push(item.name);
        })

        let configuration = getData(Global.CONFIGURATION)
        let aspect = 4/4
        if(configuration.productImageDimensionWidth && configuration.productImageDimensionHeight){
            aspect = configuration.productImageDimensionWidth/configuration.productImageDimensionHeight
        }

        let editorState = this.state.editorState

        let product = this.state.product?this.state.product:{}

        let tags = this.state.tags?this.state.tags:[]

        return (
            <Page
                title="Product Form"
                breadcrumbs={[{ name: 'product form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this product item to your list ?"
                    okCallback={this.saveUpload}
                    cancelCallback={this.closeDialog}/>
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this inventory item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>

                <InventoryQuantityModal modal={this.state.quantityModal} inventory={this.state.inventory} okCallback={this.updateQuantity} closeCallback={this.closeQuantityModal}/>
                <Row key={1}>
                    <Col>
                        <Card className="mb-6">
                            <CardHeader>Product</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={3}>
                                        <Card>
                                            <CardImg top src={
                                                this.state.image!=null?this.state.image:shoppingBag} onClick={e=>{
                                                imageSelector(file=>{
                                                    this.setState({
                                                        image:file,
                                                        cropperModal:true
                                                    })
                                                }, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE).click()
                                            }}
                                            onError={(elm)=>this.defaultImage(elm, shoppingBag)}/>
                                            <CardBody>
                                                <CardTitle>Product Image</CardTitle>
                                                <Row>
                                                    <Col>
                                                        <WarningLabel message={"*Use Jpeg Format / Max "+PRODUCT_IMAGE_MAX_FILE_SIZE+" Kb"}/>
                                                    </Col>
                                                </Row>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    product:null,
                                                                    image:null
                                                                })
                                                            )}>Cancel</Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                imageSelector(file=>{
                                                                    this.setState({
                                                                        image:file,
                                                                        cropperModal:true
                                                                    })
                                                                }, MIME_JPEG, PRODUCT_IMAGE_MAX_FILE_SIZE).click()
                                                            }}>Upload</Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={9}>
                                        <Card>
                                            <CardBody>
                                                <FormGroup>
                                                    <Label for="name">Name</Label>
                                                    <Input
                                                        type="text"
                                                        name="name"
                                                        value={(this.state.product!=null && this.state.product.name !=null)?this.state.product.name:""}
                                                        onChange={(e) =>{
                                                            e.preventDefault()
                                                            let itm = this.state.product
                                                            if(itm!=null){
                                                                itm.name = e.target.value
                                                                this.setState({
                                                                    product:itm
                                                                })
                                                            }
                                                        }}
                                                        placeholder="Enter product name"
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Brand"}
                                                        objects={this.state.brands}
                                                        default={this.state.brand?this.state.brand.id:null}
                                                        callback={(brand)=>{
                                                            let prd = this.state.product
                                                            if(prd!=null){
                                                                prd.brand = brand
                                                                this.setState({
                                                                    product:prd,
                                                                    brand:brand,
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Heading"}
                                                        objects={this.state.headings}
                                                        default={this.state.heading?this.state.heading.id:null}
                                                        callback={(heading)=>{
                                                            let prd = this.state.product
                                                            if(prd!=null){
                                                                prd.heading = heading
                                                                this.setState({
                                                                    product:prd,
                                                                    heading:heading,
                                                                    taskCategories:heading.taskCategories?heading.taskCategories:[],
                                                                    taskCategory:null
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <ItemOption
                                                        title={"Select Category"}
                                                        objects={this.state.taskCategories}
                                                        default={this.state.taskCategory?this.state.taskCategory.id:null}
                                                        callback={(taskCategory)=>{
                                                            let prd = this.state.product
                                                            if(prd!=null){
                                                                prd.taskCategory = taskCategory
                                                                this.setState({
                                                                    product:prd,
                                                                    taskCategory:taskCategory,
                                                                })
                                                            }
                                                        }}
                                                    />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Label for="relatedHashtag">Related Product Hashtag</Label>
                                                    <TagsInput value={tags} onChange={(tags)=>{
                                                        tags.map((tag, index)=>{
                                                            if(tag.charAt(0)!=='#'){
                                                                tags.splice(index, 1, "#"+tag)
                                                            }
                                                        })
                                                        this.setState({tags})
                                                    }}/>
                                                </FormGroup>
                                                <FormGroup>
                                                    <ActiveOption
                                                        callback={(active)=>{
                                                            let prd = this.state.product
                                                            if(prd!=null){
                                                                prd.active = active
                                                                this.setState({
                                                                    product:prd
                                                                })
                                                            }

                                                        }}
                                                        default={this.state.product?this.state.product.active:false}
                                                    />
                                                </FormGroup>
                                                <Row>\
                                                    <Col>
                                                        <Card>
                                                            <CardHeader>Description</CardHeader>
                                                            <CardBody>
                                                                <Editor
                                                                    editorState={editorState}
                                                                    onEditorStateChange={this.onEditorStateChange}/>
                                                            </CardBody>
                                                        </Card>
                                                    </Col>
                                                </Row>
                                                    <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    product:null
                                                                }, () => {
                                                                   this.props.history.goBack();
                                                                })
                                                                )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.product!=null && this.state.product.id!=null)?"Update":"Create"
                                                            } </Button>

                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {
                    (this.state.product!=null && this.state.product.id!=null)?
                        <Row key={2}>
                        <Col>
                            <Card className="mb-6">
                                <CardHeader>Inventory</CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col md={6}>
                                            <Row>
                                                <Col md={4}>
                                                    Sort By :
                                                    <UncontrolledButtonDropdown key={1}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                sortirMap(this.state.sortir.toString())
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "typeName", true))}>Type</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "created", true))}>Created</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "quantity", true))}>Quantity</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "weight", true))}>Weight</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "price", true))}>Price</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, "updated", true))}>Last Updated</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                                <Col md={4}>
                                                    Sortir :
                                                    <UncontrolledButtonDropdown key={2}>
                                                        <DropdownToggle
                                                            caret
                                                            color="white"
                                                            className="text-capitalize m-1">
                                                            {
                                                                this.state.ascending?"Ascending":"Descending"
                                                            }
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                                            <DropdownItem onClick={e=>(this.refreshInventory(false, this.state.sortir, true))}>Descending</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledButtonDropdown>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={3}>
                                            <SearchInput
                                                placeholder={"Search inventory..."}
                                                value={this.state.search}
                                                onChange={e=>{
                                                    this.setState({
                                                        search:e.target.value
                                                    }, () => {
                                                        if(this.state.search===''){
                                                            deleteParam(this.props, 'page')
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.search!==''){
                                                        deleteParam(this.props, 'page')
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col md={3}>
                                            <ButtonGroup className="float-right">
                                                <Button color="primary" onClick={e=>{
                                                    this.props.history.push("/inventoryDetail?productId="+this.state.product.id)
                                                }}>Add Inventory</Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Table hover>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Type</th>
                                                <th>Size</th>
                                                <th>Color</th>
                                                <th>Quantity</th>
                                                <th>Weight @item</th>
                                                <th>Price @item</th>
                                                <th>Image</th>
                                                <th>Active</th>
                                                <th>Last Update</th>
                                                <th>Edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.inventories.map((item, index)=>(
                                                    <tr key={index}>
                                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                        <td>{item.typeName}</td>
                                                        <td>{item.size?item.size.name:"-"}</td>
                                                        <td>
                                                            {
                                                                item.colors&&item.colors.map((color, index1)=>(
                                                                    color!=null?
                                                                        <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                                        :
                                                                        null
                                                                ))
                                                            }
                                                        </td>
                                                        <td><Button onClick={event=>{
                                                            event.preventDefault()
                                                            this.setState({
                                                                inventory:item
                                                            }, () => {
                                                                this.openQuantityModal()
                                                            })
                                                        }}><MdEdit/></Button>&nbsp;{item.quantity}</td>
                                                        <td>{weightFormat(item.weight)}</td>
                                                        <td>{currencyFormat(item.price, CURRENCY_SYMBOL)}</td>
                                                        <td width="5%">
                                                            <Button onClick={event=>{
                                                                event.preventDefault()
                                                                this.openLightBox(item.inventoryImages)
                                                            }}><IoMdEye/></Button>
                                                        </td>
                                                        <td><ActiveLabel active={item.active}/></td>
                                                        <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                                        <td>
                                                            <Button color="danger" onClick={e => {
                                                                this.setState({
                                                                    inventory:item
                                                                }, () => {
                                                                    this.confirmDelete()
                                                                })
                                                            }}>
                                                                <MdDelete/>
                                                            </Button>
                                                            &nbsp;
                                                            <Button color="primary" onClick={ e => (
                                                                this.props.history.push("/inventoryDetail?productId="+item.product.id+"&inventoryId="+item.id)
                                                            )}>
                                                                <MdEdit/>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </CardBody>
                                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                            </Card>
                        </Col>
                        </Row>
                        :
                        null

                }

                <ImageCropper src={this.state.image} aspect={aspect} show={this.state.cropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        this.setState({
                            image:file,
                            cropperModal:false,
                            imageBlob:fileBlob,
                        })
                    })
                }} cancelCallback={()=>{
                    this.setState({
                        cropperModal:false,
                        image:product.imageLink,
                        imageBlob:null,
                    })
                }}/>

            </Page>
        );
    }


}