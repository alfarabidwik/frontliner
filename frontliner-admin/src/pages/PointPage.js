import BasePage from "./BasePage";
import React from "react"
import Page from "../components/Page";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem, DropdownMenu, DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {changeParam, currencyFormat, parseDate, sortirMap, weightFormat} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
import {MdDelete, MdEdit} from "react-icons/md";
import PointModal from "../components/modal/PointModal";
import {IoMdEye, IoMdOpen} from "react-icons/io";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";



export default class PointPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            modalDeleteConfirm:false,
            ascending:true,
            sortir:'v.created',
            inventorySortir:'product.name',
            points:[],
            product:{},
            inventories:[],
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            tabkey:"ACTIVE",
            sortirName:"v.name",
            sortirCreated:"v.created",
            sortirUpdated:"v.updated",
            sortirStartPeriod:"v.startPeriod",
            sortirEndPeriod:"v.endPeriod",
            inventorySortirName:"product.name",
            inventorySortirCreated:"created",
            inventorySortirUpdated:"updated",
            inventorySortirStartPeriod:"point.startPeriod",
            inventorySortirEndPeriod:"point.endPeriod",

        }

    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                if(this.state.tabkey&& this.state.tabkey===Global.ALL_INVENTORY){
                    this.fetchAllInventories()
                }else{
                    this.refresh(this.state.ascending, this.state.sortir, true)
                }
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAll()
    }

    fetchAll = (progressing) => {
        this.get(Global.API.POINTS, {
            params:{
                status:this.state.tabkey,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page: this.state.page-1
            }
        }, null, response =>{
            if(response.code === 200){
                this.setState({
                    points : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    delete = () => {
        this.setState({
            modalDeleteConfirm:false
        }, () => {
            this.get(Global.API.DISCOUNT_DELETE+"/"+this.state.product.id, null, null, response=>{
                if(response.code===200){
                    this.setState({
                        product:{}
                    }, () => {
                        this.fetchAll(true)
                    })
                }
            }, true, false)
        })
    }

    refresh = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(progressing)
        })
    )

    refreshInventory = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            inventorySortir:sortir
        }, () => {
            this.fetchAllInventories()
        })
    )

    closePointModal = () =>{
        this.setState({
            pointModal:false
        }, () => {
            this.refreshInventory(this.state.ascending, this.state.inventorySortir)
        })
    }


    render() {
        return (
            <Page
                title="Point"
                breadcrumbs={[{ name: 'point', active: true }]}
                className="TablePage">
                <ConfirmDialog
                    showing={this.state.modalDeleteConfirm}
                    title="Delete Confirmation"
                    message="Do you want to delete this item from your list ?"
                    okCallback={this.delete}
                    cancelCallback={this.closeDialog}/>
                <PointModal
                    showing={this.state.pointModal}
                    edit={this.state.edit}
                    inventory={this.state.inventory}
                    close={this.closePointModal}
                />

                <Row>
                    <Col>
                    <Card>
                        <Tabs
                            activeKey={this.state.tabkey}
                            onSelect={key =>{
                                this.setState({
                                    tabkey:key
                                }, () => {
                                    changeParam(this.props, 'page', 1)
                                    if(this.state.tabkey&& this.state.tabkey===Global.ALL_INVENTORY){
                                        this.fetchAllInventories()

                                    }else{
                                        this.refresh(this.state.ascending, this.state.sortir, true)
                                    }
                                })
                            }
                            }>
                            <Tab eventKey={Global.ACTIVE} title="Active">
                                {
                                    this.renderProductTable()                                }
                            </Tab>
                            <Tab eventKey={Global.EXPIRED} title="Expired">
                                {
                                    this.renderProductTable()                                }
                            </Tab>
                            <Tab eventKey={Global.INACTIVE} title="Inactive">
                                {
                                    this.renderProductTable()                                }
                            </Tab>
                            <Tab eventKey={Global.ALL} title="All">
                                {
                                    this.renderProductTable()
                                }
                            </Tab>
                            <Tab eventKey={Global.ALL_INVENTORY} title="All Inventory">
                                {
                                    this.renderInventoryTable()
                                }
                            </Tab>
                        </Tabs>
                        </Card>
                    </Col>
                </Row>
                {super.render()}
            </Page>
        );
    }

    renderProductTable = () =>{
        return(
            <Card>
            <CardBody>
                <Row>
                    <Col md={2}>
                        Sort By :
                        <UncontrolledButtonDropdown key={1}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    sortirMap(this.state.sortir)
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>{
                                    e.preventDefault()
                                    this.refresh(this.state.ascending, this.state.sortirName, true)
                                }}>Name</DropdownItem>
                                <DropdownItem onClick={e=>{
                                    e.preventDefault()
                                    this.refresh(this.state.ascending, this.state.sortirCreated, true)
                                }}>Created</DropdownItem>
                                <DropdownItem onClick={e=>{
                                    e.preventDefault()
                                    this.refresh(this.state.ascending, this.state.sortirUpdated, true)
                                }}>Updated</DropdownItem>
                                <DropdownItem onClick={e=>{
                                    e.preventDefault()
                                    this.refresh(this.state.ascending, this.state.sortirStartPeriod, true)
                                }}>Start Period</DropdownItem>
                                <DropdownItem onClick={e=>{
                                    e.preventDefault()
                                    this.refresh(this.state.ascending, this.state.sortirEndPeriod, true)
                                }}>End Period</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={2}>
                        Sortir :
                        <UncontrolledButtonDropdown key={2}>
                            <DropdownToggle
                                caret
                                color="white"
                                className="text-capitalize m-1">
                                {
                                    this.state.ascending?"Ascending":"Descending"
                                }
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir, true))}>Descending</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledButtonDropdown>
                    </Col>
                    <Col md={3}>
                        <SearchInput
                            value={this.state.search}
                            onChange={e=>{
                                this.setState({
                                    search:e.target.value
                                }, () => {
                                    if(this.state.search===''){
                                        deleteParam(this.props, 'page')
                                    }
                                })
                            }}
                            onEnter={e=>{
                                if(this.state.search!==''){
                                    deleteParam(this.props, 'page')
                                }
                            }}
                        />
                    </Col>
                    <Col md={5}>
                        {
                            this.state.tabkey===Global.ACTIVE?(
                                <ButtonGroup className="float-right">
                                    <Button color="primary" onClick={e=>{
                                        this.props.history.push('/pointDetail')
                                    }}>Add Point</Button>
                                </ButtonGroup>
                            ):null
                        }
                    </Col>
                </Row>
                <Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Period</th>
                            <th>Benefit</th>
                            <th>Point</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.points.map((item, index)=>(
                                <tr key={index}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.name}</td>
                                    <td>{item.description}</td>
                                    <td>
                                        {
                                            item.periodType===Global._ALL_TIME?Global.ALL_TIME:(null)

                                        }
                                        {
                                            item.periodType===Global._STATIC?(null):(null)

                                        }
                                        {
                                            item.periodType===Global._DYNAMIC?(
                                                <span>
                                                    {parseDate(item.startPeriod, DD_MM_YYYY)}
                                                    &nbsp; - &nbsp;
                                                    {parseDate(item.endPeriod, DD_MM_YYYY)}
                                                </span>
                                            ):(null)

                                        }
                                    </td>
                                    <td>
                                        {
                                            item.benefitType===Global._ITEM_POINT?Global.ITEM_POINT:(null)

                                        }
                                        {
                                            item.benefitType===Global._PURCHASE_POINT?Global.PURCHASE_POINT+" || "+currencyFormat(item.purchaseNominal, CURRENCY_SYMBOL):(null)

                                        }
                                    </td>
                                    <td>{item.point}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>
                                        <IoMdOpen color="blue" onClick={ e => {
                                            e.preventDefault()
                                            this.props.history.push('/pointDetail?id='+item.id)
                                        }}/>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </Row>
            </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }

    renderInventoryTable =()=>{
         return(
             <Card>
             <CardBody>
                 <Row>
                     <Col md={2}>
                         Sort By :
                         <UncontrolledButtonDropdown key={1}>
                             <DropdownToggle
                                 caret
                                 color="white"
                                 className="text-capitalize m-1">
                                 {
                                     sortirMap(this.state.inventorySortir)
                                 }
                             </DropdownToggle>
                             <DropdownMenu>
                                 <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, this.state.inventorySortirName))}>Name</DropdownItem>
                                 <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, this.state.inventorySortirCreated))}>Created</DropdownItem>
                                 <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, this.state.inventorySortirUpdated))}>Updated</DropdownItem>
                                 <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, this.state.inventorySortirStartPeriod))}>Start Period</DropdownItem>
                                 <DropdownItem onClick={e=>(this.refreshInventory(this.state.ascending, this.state.inventorySortirEndPeriod))}>End Period</DropdownItem>
                             </DropdownMenu>
                         </UncontrolledButtonDropdown>
                     </Col>
                     <Col md={2}>
                         Sortir :
                         <UncontrolledButtonDropdown key={2}>
                             <DropdownToggle
                                 caret
                                 color="white"
                                 className="text-capitalize m-1">
                                 {
                                     this.state.ascending?"Ascending":"Descending"
                                 }
                             </DropdownToggle>
                             <DropdownMenu>
                                 <DropdownItem onClick={e=>(this.refreshInventory(true, this.state.inventorySortir))}>Ascending</DropdownItem>
                                 <DropdownItem onClick={e=>(this.refreshInventory(false, this.state.inventorySortir))}>Descending</DropdownItem>
                             </DropdownMenu>
                         </UncontrolledButtonDropdown>
                     </Col>
                     <Col md={3}>
                         <SearchInput
                             value={this.state.search}
                             onChange={e=>{
                                 this.setState({
                                     search:e.target.value
                                 }, () => {
                                     if(this.state.search===''){
                                         deleteParam(this.props, 'page')
                                     }
                                 })
                             }}
                             onEnter={e=>{
                                 if(this.state.search!==''){
                                     deleteParam(this.props, 'page')
                                 }
                             }}
                         />
                     </Col>
                     <Col md={5}>
                         {
                             this.state.tabkey===Global.ACTIVE?(
                                 <ButtonGroup className="float-right">
                                     <Button color="primary" onClick={e=>{
                                         // this.setState({
                                         //     modalAddPoint:!this.state.modalAddPoint
                                         // })
                                         this.props.history.push('/pointDetail')
                                     }}>Add Point</Button>
                                 </ButtonGroup>
                             ):null
                         }
                     </Col>
                 </Row>
                 <Row>
                     <Table hover>
                         <thead>
                         <tr>
                             <th>#</th>
                             <th>Type</th>
                             <th>Product</th>
                             <th>Category</th>
                             <th>Size</th>
                             <th>Color</th>
                             <th>Quantity</th>
                             <th>Weight @item</th>
                             <th>Price @item</th>
                             <th>Image</th>
                             <th>Point Name</th>
                             <th>Point</th>
                             <th>Last Update</th>
                             <th>Detail</th>
                         </tr>
                         </thead>
                         <tbody>
                         {
                             this.state.inventories.map((item, index)=>(
                                 <tr key={index}>
                                     <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                     <td>{item.typeName}</td>
                                     <td>{item.product.name}</td>
                                     <td>{item.product.category?item.product.category.name:"-"}</td>
                                     <td>{item.size?item.size.name:"-"}</td>
                                     <td>
                                         {
                                             item.colors.map((color, index1)=>(
                                                 color!=null?
                                                     <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                     :
                                                     null
                                             ))
                                         }
                                     </td>
                                     <td>{item.quantity}</td>
                                     <td>{weightFormat(item.weight)} gram</td>
                                     <td>{currencyFormat(item.price, CURRENCY_SYMBOL)}</td>
                                     <td width="5%">
                                         <Button onClick={event=>{
                                             event.preventDefault()
                                             this.openLightBox(item.inventoryImages)
                                         }}><IoMdEye/></Button>

                                     </td>
                                     <td><Button color="primary" onClick={event=>{
                                         event.preventDefault()
                                         this.setState({
                                             inventory:item,
                                             pointModal:true
                                         })
                                     }}><MdEdit/>&nbsp;{item.point?item.point.name:""}</Button></td>
                                     <td>
                                         {item.point?item.point.point:"0"}
                                     </td>
                                     <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                     <td>
                                         <Button onClick={ e => (
                                             this.props.history.push("/inventoryDetail?productId="+item.product.id+"&inventoryId="+item.id)
                                         )}>
                                             <IoMdOpen/>
                                         </Button>
                                     </td>

                                 </tr>
                             ))
                         }
                         </tbody>
                     </Table>
                 </Row>
             </CardBody>
                 <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
             </Card>
         )
    }


    fetchAllInventories = () =>{
         this.get(Global.API.INVENTORIES, {params:{
             page:this.state.page-1,
             ascending:this.state.ascending,
             sortir:this.state.inventorySortir,
             search:this.state.search
             }}, null, response=>{
                if(response.code===200){
                    this.setState({
                        inventories:response.data,
                        totalPage: response.totalPage,
                        totalElement: response.totalElement,
                        pageElement: response.pageElement,
                    })
                }
         }, true, true)
    }



}