import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import {isEmpty, parseDate} from "../utils/Utilities";
import PopularProductSelectorModal from "../components/modal/PopularProductSelectorModal";
import ActiveLabel from "../components/Widget/ActiveLabel";
import Pagination from '../components/Pagination'
import queryString from "query-string";
import Img from 'react-image'


const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class PopularProductPage extends BasePage{


  constructor(props) {
    super(props);
    let state = this.state
      state.products = []
      state.product={}
      state.modalDeleteConfirm=false
      state.productModal=false
      state.page =queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
      state.totalPage=0
      state.totalElement=0
      state.pageElement=0
      this.setState(state)
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page)
  }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll()
            })
        }
    }


    fetchAll = () => {
        this.get(Global.API.PRODUCT_POPULARS, {
            params:{
                page: this.state.page-1
            }
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    products : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        }, true, true);
    }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
        this.get(Global.API.PRODUCT_DELETE_POPULAR,{
            params:{
                productId:this.state.product.id,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    category:null
                }, () => {
                    this.fetchAll()
                })
            }
        }, true, true)
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshProduct = (ascending, sortir) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll()
      })
  )
  closeProductModal = () =>{
      this.setState({
          productModal:false,
      })
  }


  productSelected = (product) =>{
        this.closeProductModal()
        this.get(Global.API.PRODUCT_UPDATE_POPULAR, {
            params:{
                productId:product.id,
                popularSort:product.popularSort
            }
        }, null, response=>{
          if(response.code===200){
              this.refreshProduct(this.state.ascending, this.state.sortir)
          }
        }, true, true)

  }

    render() {
    return (
        <Page
            title="Popular Products"
            breadcrumbs={[{ name: 'popular product', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this popular item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
            <PopularProductSelectorModal
                showing={this.state.productModal}
                close={this.closeProductModal}
                selected={this.productSelected}
            />
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardBody>
                  <Row>
                        <Col md={2}>
                        </Col>
                        <Col md={2}>
                        </Col>
                        <Col md={3}>
                        </Col>
                        <Col md={5}>
                            <ButtonGroup className="float-right">
                                <Button color="primary" onClick={e=>{
                                    this.setState({
                                        productModal:!this.state.productModal
                                    })
                                }}>Add Product</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Category</th>
                              <th>Image</th>
                              <th className="text-center">Popular</th>
                              <th>Order Qty</th>
                              <th>Active</th>
                              <th>Last Update</th>
                              <th>Remove</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.products.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.name}</td>
                                      <td>{item.category?item.category.name:""}</td>
                                      <td width="5%">
                                          <Img
                                              className="img-thumbnail"
                                              src={item.imageLink}
                                              onClick={e=>{
                                                  this.setState({
                                                      imageLink:item.imageLink,
                                                      openPreview:true
                                                  })
                                              }}
                                          />
                                          </td>
                                      <th className="text-center">{isEmpty(item.popularSort)?"-":item.popularSort}</th>
                                      <th className="text-center">{isEmpty(item.orderQuantity)?"-":item.orderQuantity}</th>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          {
                                              isEmpty(item.popularSort)?("-"):
                                                  <Button size="sm" color="danger" onClick={e => {
                                                      this.setState({
                                                          product:item
                                                      }, () => {
                                                          this.confirmDelete()
                                                      })
                                                  }}>
                                                      <MdDelete/>
                                                  </Button>
                                          }
                                          {/*<Button size="sm" color="primary" onClick={ e => (*/}
                                              {/*this.props.history.push('/productDetail?id='+item.id)*/}
                                          {/*)}>*/}
                                              {/*<MdEdit/>*/}
                                          {/*</Button>*/}
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
};
