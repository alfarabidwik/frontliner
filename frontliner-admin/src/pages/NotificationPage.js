import React from 'react';
import BasePage from "./BasePage";
import {
    Button,
    ButtonGroup, ButtonToolbar,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import Page from "../components/Page";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {imageSelector, parseDate, sortirMap, weightForma, currencyFormat, weightFormat} from "../utils/Utilities";
import SearchInput from '../components/SearchInput'
import {IoMdEye, IoMdSend} from "react-icons/io/index";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import BroadcastEmailPreviewModal from '../components/modal/BroadcastEmailPreviewModal'
import {NotificationDto} from "../model/model";



export default class NotificationPage extends BasePage{

    notifications = new Array(NotificationDto)


    constructor(props) {
        super(props);
        this.state = {
            notifications:this.notifications,
            ascending:false,
            sortir:'created',
            search:"",
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            broadcastEmailPreviewModal:false
        }
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAllNotifications(true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAllNotifications(true)
    }

    confirmDelete = () =>{
        this.setState({
            modalDeleteConfirm:true
        })
    }

    fetchAllNotifications = (progressing) =>{
        this.get(Global.API.NOTIFICATIONS, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                createByAdmin:true,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response => {
            if(response.code==200){
                this.setState({
                    notifications : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }

        }, progressing, true)
    }

    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
            modalDeleteConfirm:false,
        })
    }

    refreshNotification = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAllNotifications(progressing)
        })
    )

    render() {
        return (
            <Page
                title="Notification"
                breadcrumbs={[{ name: 'Notification', active: true }]}>
                {super.render()}
                {
                    <Row key={2}>
                        <Col>
                            <Card className="mb-6">
                                <CardHeader>Notification</CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col md={3}>
                                            Sort By :
                                            <UncontrolledButtonDropdown key={1}>
                                                <DropdownToggle
                                                    caret
                                                    color="white"
                                                    className="text-capitalize m-1">
                                                    {
                                                        this.state.sortir.toString()
                                                    }
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem onClick={e=>(this.refreshNotification(this.state.ascending, "title", true))}>Title</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshNotification(this.state.ascending, "created", true))}>Created</DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledButtonDropdown>
                                        </Col>
                                        <Col md={3}>
                                            Sortir :
                                            <UncontrolledButtonDropdown key={2}>
                                                <DropdownToggle
                                                    caret
                                                    color="white"
                                                    className="text-capitalize m-1">
                                                    {
                                                        this.state.ascending?"Ascending":"Descending"
                                                    }
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    <DropdownItem onClick={e=>(this.refreshNotification(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                                    <DropdownItem onClick={e=>(this.refreshNotification(false, this.state.sortir, true))}>Descending</DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledButtonDropdown>
                                        </Col>
                                        <Col md={3}>
                                            <SearchInput
                                                placeholder={"Search subject..."}
                                                value={this.state.search}
                                                onChange={e=>{
                                                this.setState({
                                                    search:e.target.value
                                                }, () => {
                                                    this.refreshNotification(this.state.ascending, this.state.sortir,false)
                                                })
                                            }}/>
                                        </Col>
                                        <Col md={3}>
                                            <ButtonGroup className="float-right">
                                                <Button color="primary" onClick={e=>{
                                                    this.props.history.push("/notificationDetail")
                                                }}>Send Notification &nbsp;&nbsp;<IoMdSend/> </Button>
                                            </ButtonGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Table hover>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Message</th>
                                                <th>Publisher</th>
                                                <th>Topic</th>
                                                <th>Created</th>
                                                <th>View</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.notifications.map((item, index)=>(
                                                    <tr key={index}>
                                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                                        <td>{item.title}</td>
                                                        <td>{item.message}</td>
                                                        <td>{item.admin?item.admin.fullname:"-"}</td>
                                                        <td>{item.topic}</td>
                                                        <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                                        <td>
                                                            <Button color="primary" onClick={event=>{
                                                                event.preventDefault()
                                                                this.props.history.push("/notificationDetail?id="+item.id)
                                                            }}>
                                                                <IoMdEye/>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                            }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </CardBody>
                                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
                            </Card>
                        </Col>
                    </Row>
                }
            </Page>
        );
    }

}