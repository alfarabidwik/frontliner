import * as React from "react";
import Page from './../components/Page';
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import SearchInput from '../components/SearchInput'

import Pagination from '../components/Pagination'
import * as queryString from 'query-string/index';
import {deleteParam, parseDate, sortirMap, currencyFormat} from "../utils/Utilities";
import DateInput from "../components/Widget/DateInput";

const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class BalanceMutationPage extends BasePage{


  constructor(props) {
    super(props);
    let balanceMutations  = new Array();
    this.state ={
      balanceMutations : balanceMutations,
      balanceMutation:{},
      modalDeleteConfirm:false,
      ascending:false,
      sortir:'bm.created',
      search:"",
      page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
      totalPage:0,
      totalElement:0,
      pageElement:0,
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
  }
    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, true)
            })
        }
    }



    fetchAll = (ascending, sortir, search, page, progressing) => {
        this.get(Global.API.BALANCE_MUTATIONS, {
            params:{
                ascending:ascending,
                sortir:sortir,
                search:search,
                page: page-1,
                startDate:this.state.createdStartDate,
                endDate:this.state.createdEndDate
            }
        }, null, res=>{
            if(res.code === 200){
                this.setState({
                    balanceMutations : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        },progressing, true);
    }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }


  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  refreshBalanceMutation = (ascending, sortir, progressing) => (
      this.setState({
          ascending:ascending,
          sortir:sortir
      }, () => {
          this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, this.state.page, progressing)
      })
  )

    render() {
        var i = 0 ;

    return (
        <Page
            title="Balance Mutations"
            breadcrumbs={[{ name: 'balanceMutation', active: true }]}
            className="TablePage"
        >
            {super.render()}
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Balance Mutation</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={3}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "bm.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "bm.updated", true))}>Updated</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "u.firstname", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "u.agentCode", true))}>Agent Code</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "u.mobilePhone", true))}>Mobile Phone</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(this.state.ascending, "u.email", true))}>Email</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshBalanceMutation(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                      <Col md={3}>
                            <SearchInput
                                placeholder={"Type and enter to search..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            deleteParam(this.props, 'page')
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        deleteParam(this.props, 'page')
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>
                            <p style={{float:'left', marginTop:'7px', marginRight:'5px'}}>Created Start :</p>
                            <DateInput
                                id="createdStartDate"
                                value={this.state.createdStartDate}
                                maxdate={this.state.createdEndDate}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        createdStartDate : value?value:null
                                    }, () => {
                                        if(this.state.createdStartDate && this.state.createdEndDate || !value){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, 1, true)
                                            })
                                        }
                                    })
                                }}
                                style={{float:'left', width:'60%'}}
                                placeholder="Enter start date"
                            />
                        </Col>
                        <Col md={3}>
                            <p style={{float:'left', marginTop:'7px', marginRight:'5px'}}>Created End :</p>
                            <DateInput
                                id="createdStartDate"
                                value={this.state.createdEndDate}
                                maxdate={this.state.createdEndDate}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        createdEndDate : value?value:null
                                    }, () => {
                                        if(this.state.createdStartDate && this.state.createdEndDate || !value){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, 1, true)
                                            })
                                        }
                                    })
                                }}
                                style={{float:'left', width:'60%'}}
                                placeholder="Enter end date"
                            />
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Agent</th>
                              <th>Agent Code</th>
                              <th>Mobile Phone</th>
                              <th>Email</th>
                              <th>Debit</th>
                              <th>Credit</th>
                              <th>Created</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.balanceMutations.map((item, index)=>(
                                  <tr key={index}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.user.fullname}</td>
                                      <td>{item.user.agentCode}</td>
                                      <td>{item.user.mobilePhone}</td>
                                      <td>{item.user.email}</td>
                                      <td>{item.debitAmount?currencyFormat(item.debitAmount, CURRENCY_SYMBOL):""}</td>
                                      <td>{item.creditAmount?currencyFormat(item.creditAmount, CURRENCY_SYMBOL):""}</td>
                                      <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }

};
