import BasePage from "./BasePage";
import React from 'react'
import {
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import StockAuditStockPage from './StockAuditStockPage'
import StockAuditInventoryPage from './StockAuditInventoryPage'
import Page from "../components/Page";

export default class StockAuditPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:true,
            sortir:'created',
            tabs:[
                {
                    id:1,
                    name:"Stock Log"
                },
                {
                    id:2,
                    name:"Inventory"
                }
                ],
            stockAudits:[],
            tab:null,
            tabId:1,
            search:""
        }
    }


    render() {
        return this.renderTransaction()
    }

    renderTransaction = () =>{
        return (
            <Page>
            <Card>
                <CardHeader>Stock Audit</CardHeader>
                <CardBody>
                    <Tabs
                        activeKey={this.state.key}
                        onSelect={key =>{
                            let previousTab = this.state.tabId
                            this.setState({
                                tabId:key
                            }, () => {
                                if(previousTab!=this.state.tabId){
                                    // deleteParam(this.props, 'page')
                                }
                            })
                        }
                        }>
                        {
                            this.state.tabs.map((item, index)=>(
                                <Tab eventKey={item.id} title={item.name}>
                                    {
                                        item.id===1?(<StockAuditStockPage {...this.props} show={this.state.tabId==item.id}/>):(<StockAuditInventoryPage {...this.props} show={this.state.tabId==item.id}/>)
                                    }
                                </Tab>
                            ))
                        }
                    </Tabs>
                </CardBody>
            </Card>
            </Page>
        )
    }


}