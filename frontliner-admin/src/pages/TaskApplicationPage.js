import BasePage from "./BasePage";
import React from 'react'
import {
    Card,
    CardBody,
    CardHeader,
    Col, DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Row,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {
    changeParam,
    changeParam2,
    changeParam3,
    currencyFormat,
    deleteParam, hasBeenDelivery,
    parseDate,
    sortirMap
} from "../utils/Utilities";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye, IoMdCar, IoMdBus, IoMdBicycle, IoMdNavigate} from "react-icons/io";
import SearchInput from '../components/SearchInput'
import Global, {_DELIVERY, CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS, PRINT_DATA} from "../utils/Global";
import {IoMdOpen} from "react-icons/io/index";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import Button from "reactstrap/es/Button";
import {MdPrint, MdRefresh, MdSearch} from "react-icons/md/index";
import SockJsClient from 'react-stomp'
import {storeData} from "../utils/StorageUtil";



export default class TaskApplicationPage extends BasePage{

    constructor(props) {
        super(props);
        this.state ={
            ascending:false,
            sortir:'ja.updated',
            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            key:queryString.parse(this.props.query).tab?queryString.parse(this.props.query).tab:"All",
            totalPage:0,
            totalElement:0,
            pageElement:0,
            taskApplicationStatuses:[],
            taskApplications:[],
            taskApplicationStatus:null,
            taskApplicationStatusId:null,
            search:queryString.parse(this.props.query).search?queryString.parse(this.props.query).search:"",
            inputSearch : "",
        }
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchTaskApplicationStatus()
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            let page = queryString.parse(props.query).page?queryString.parse(props.query).page:1
            let key = queryString.parse(props.query).tab?queryString.parse(props.query).tab:"All"
            let search = queryString.parse(props.query).search?queryString.parse(props.query).search:""
            let taskApplicationStatusId = null
            if(key==="All"){
                taskApplicationStatusId = null
            }else{
                taskApplicationStatusId = key
            }
            this.setState({
                page:page,
                key:key,
                search:search,
                taskApplicationStatusId:taskApplicationStatusId
            }, () => {
                this.fetchTaskApplication(true)
            })
        }
    }


    fetchTaskApplicationStatus = () =>{
        this.get(Global.API.TASK_APPLICATION_STATUSES, null, null, response=>{
            if(response.code===200){
                this.setState({
                    taskApplicationStatuses : response.data
                }, () => {
                    this.fetchTaskApplication(true)
                })
            }
        }, true, true)
    }

    fetchTaskApplication = (progressing) =>{
        this.get(Global.API.TASK_APPLICATIONS, {
            params:{
                taskApplicationStatusId:this.state.taskApplicationStatusId,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    taskApplications : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,

                })
            }
        }, progressing, true)
    }

    refreshTaskApplication = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
            this.fetchTaskApplication(progressing)
        })
    )


    render() {
        return (
            <div>
                {super.render()}
                {this.renderTaskApplication()}
            </div>
        )
    }

    renderTaskApplication = () =>{
        return (
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader>TaskApplication</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={3}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir.toString())
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(this.state.ascending, "jjv.title", true))}>Title</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(this.state.ascending, "ja.itemFee", true))}>Fee</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(this.state.ascending, "ja.created", true))}>Created</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(this.state.ascending, "ja.updated", true))}>Updated</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTaskApplication(false, this.state.sortir, true))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={4}>
                                    <Row>
                                        <Col>
                                            <SearchInput
                                                placeholder={"Customer name, Ref code, Resi Code..."}
                                                value={this.state.inputSearch}
                                                onChange={e=>{
                                                    this.setState({
                                                        inputSearch:e.target.value
                                                    }, () => {
                                                        if(this.state.inputSearch===''){
                                                            changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                        }
                                                    })
                                                }}
                                                onEnter={e=>{
                                                    if(this.state.inputSearch!==''){
                                                        changeParam3(this.props, 'page', 1, 'tab', this.state.key, 'search', this.state.inputSearch)
                                                    }
                                                }}
                                            />
                                        </Col>
                                        <Col>
                                            {/*<Button onClick={event=>{*/}
                                                {/*let key = this.state.key*/}
                                                {/*changeParam3(this.props, 'page', 1, 'tab', key, 'search', this.state.inputSearch)*/}
                                            {/*}}><MdSearch/></Button>*/}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={key =>{
                                    changeParam2(this.props, 'page', 1, 'tab', key)
                                }
                                }>
                                <Tab eventKey="All" title="All">
                                    {this.renderTaskApplicationTable()}
                                </Tab>
                                {
                                    this.state.taskApplicationStatuses.map((item, index)=>(
                                        <Tab key={index} eventKey={item.id} title={item.name}>
                                            {this.renderTaskApplicationTable()}
                                        </Tab>
                                    ))
                                }
                            </Tabs>
                        </CardBody>
                        <SockJsClient
                            url={Global.BASE_URL}
                            topics={[Global.API.LISTEN_TASK_APPLICATION]}
                            onMessage={(message) => {
                                this.successToast(message)
                                this.fetchTaskApplication(true)
                            }}
                            ref={ (client) => { this.clientRef = client }} />
                    </Card>
                </Col>
            </Row>
        )
    }


    renderTaskApplicationTable = () =>{
        return (
            <Card>
                <CardBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref Code</th>
                            <th>Task</th>
                            <th>Category</th>
                            <th>Agent</th>
                            <th>Agent Code</th>
                            <th>Publisher</th>
                            <th>Fee</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.taskApplications.map((item, index)=>(
                                <tr key={item.id}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.viewOrderReference}</td>
                                    <td>{item.task.title}</td>
                                    <td>{item.task.taskCategory.name}</td>
                                    <td>
                                        {item.user.fullname}
                                        &nbsp;&nbsp;
                                        <IoMdOpen
                                            color="blue" style={{cursor:'pointer'}}
                                            onClick={e=>{
                                                this.props.history.push('/userDetail?id='+item.user.id)
                                            }
                                        }/>
                                    </td>
                                    <td>{item.user.agentCode}</td>
                                    <td>{item.task.partner.fullName}</td>
                                    <td>{currencyFormat(item.itemFee, CURRENCY_SYMBOL)}</td>
                                    <td>{item.taskApplicationStatus.description}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td>
                                        <IoMdEye
                                            color="green"
                                            style={{cursor:'pointer'}}
                                            onClick={e=>{
                                                this.props.history.push('/taskApplicationDetail?id='+item.id)
                                            }}
                                         />
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }


}