import Page from 'components/Page';
import React from 'react';
import {Link} from 'react-router-dom'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'
import {parseDate, sortirMap} from "../utils/Utilities";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";
import {AuthConsumer} from "../utils/AuthContext";
import Img from 'react-image'
import ActiveLabel from "../components/Widget/ActiveLabel";


const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class SubscriberPage extends BasePage{


  constructor(props) {
    super(props);
    this.state ={
        subscribers : [],
        subscriber:{},
        modalDeleteConfirm:false,
        ascending:true,
        sortir:'created',
        search:"",
        page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
        totalPage:0,
        totalElement:0,
        pageElement:0,

    }
  }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search,true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
    }

  fetchAll = (ascending, sortir, search, progressing) => {
    this.get(Global.API.SUBSCRIBERS, {
      params:{
        ascending:ascending,
        sortir:sortir,
        search:search,
      }
    }, null, res=>{
        if(res.code === 200){
            this.setState({
                subscribers : res.data,
                totalPage: res.totalPage,
                totalElement: res.totalElement,
                pageElement: res.pageElement,
            })
        }
    }, progressing, true);
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      this.get(Global.API.SUBSCRIBER_DELETE+"/"+this.state.subscriber.id, null, null, res => {
          if(res.code===200){
              this.successToast(res.message)
              this.setState({
                  category:null
              }, () => {
                  this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
              })
          }
      }, true, true);
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

    refreshSubscriber = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )


    render() {
        var i = 0 ;

    return (
        <AuthConsumer>
            {({subscriber, logout, login})=>(
                    <Page
                        title="Subscribers"
                        breadcrumbs={[{ name: 'subscriber', active: true }]}
                        className="TablePage"
                    >
            {super.render()}
                        <ConfirmDialog
                            showing={this.state.modalDeleteConfirm}
                            title="Delete Confirmation"
                            message="Do you want to delete this subscriber item from your list ?"
                            okCallback={this.delete}
                            cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Subscriber</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={2}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshSubscriber(this.state.ascending, "created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshSubscriber(this.state.ascending, "email", true))}>Name</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={2}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshSubscriber(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshSubscriber(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={3}>
                            <SearchInput
                                placeholder={"Search subscriber..."}
                                value={this.state.search}
                                onChange={e=>{
                                    this.setState({
                                        search:e.target.value
                                    }, () => {
                                        if(this.state.search===''){
                                            deleteParam(this.props, 'page')
                                        }
                                    })
                                }}
                                onEnter={e=>{
                                    if(this.state.search!==''){
                                        deleteParam(this.props, 'page')
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Email</th>
                              <th>Status</th>
                              <th>Created</th>
                              <th>Delete</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.subscribers.map((item, index)=>(
                                  <tr key={item.id}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.email}</td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          <Button size="sm" color="danger" onClick={e => {
                                              e.preventDefault()
                                              this.setState({
                                                  subscriber:item
                                              }, () => {
                                                  this.confirmDelete()
                                              })
                                          }}>
                                              <MdDelete/>
                                          </Button>
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />

              </Card>
            </Col>
          </Row>
        </Page>
            )}
        </AuthConsumer>
    );
  }
};
