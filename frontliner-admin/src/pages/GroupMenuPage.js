import Page from 'components/Page';
import React from 'react';
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import CardFooter from "reactstrap/es/CardFooter";
import CardBlock from "reactstrap/es/CardBlock";
import BasePage from "./BasePage";
import kitchen from "../utils/AxiosInstance";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {allIsEmpty, isEmpty, parseDate, translate} from "../utils/Utilities";
import {MdDelete, MdEdit} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import ActiveOption from "../components/Widget/ActiveOption";
import ActiveLabel from "../components/Widget/ActiveLabel";
import {IoMdMenu} from "react-icons/io";

const tableTypes = ['', 'bordered', 'striped', 'hover'];


export default class GroupMenuPage extends BasePage{
  constructor(props) {
    super(props);
    this.state = {
      groupMenu:{},
      groupMenus:[],
      category:{},
      modalAddConfirm:false,
      modalDeleteConfirm:false,
      modal: false,
      ascending:true,
      sortir:'created',
    }
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAll()
  }

  confirmSave = () =>{
    var groupMenu = this.state.groupMenu
    if(isEmpty(groupMenu.name)){
      this.showDialog("Notice", "Please complete this form fields")
    }else{
      this.setState({
        modalAddConfirm:true
      })
    }
  }

  save = () => {
    let groupMenu = this.state.groupMenu
    if(!groupMenu.id && !groupMenu.active){
      groupMenu.active = false ;
    }
    this.setState({
      modalAddConfirm:false
    }, () => {
      kitchen.post(Global.API.GROUP_MENU_SAVE, groupMenu, null).then(res => {
        if(res.data.code===200){
          this.setState({
            groupMenu:{}
          }, () => {
            this.fetchAll()
          })
        }else{

        }
      });
    })
  }

  confirmDelete = () =>{
    this.setState({
      modalDeleteConfirm:true
    })
  }

  delete = () => {
    this.setState({
      modalDeleteConfirm:false
    }, () => {
      kitchen.get(Global.API.GROUP_MENU_DELETE+"/"+this.state.groupMenu.id).then(res => {
        if(res.data.code===200){
          this.setState({
            groupMenu:{}
          }, () => {
            this.fetchAll()
          })
        }else{

        }
      });
    })
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

  fetchAll = () => {
    this.get(Global.API.GROUP_MENUS, {
      params:{
        ascending:this.state.ascending,
        sortir:this.state.sortir,
      }
    }, null, response=>{
      if(response.code === 200){
        this.setState({
          groupMenus : response.data
        })
      }
    });
  }

  refresh = (ascending, sortir) => (
      this.setState({
        ascending:ascending,
        sortir:sortir
      }, () => {
        this.fetchAll()
      })
  )

  render() {
    return (
        <Page
            title="GroupMenu Detail"
            breadcrumbs={[{ name: 'groupMenu detail', active: true }]}
            className="TablePage">
          {super.render()}
          <ConfirmDialog
              showing={this.state.modalAddConfirm}
              title="Update Confirmation"
              message="Do you want to update this groupMenu item to your list ?"
              okCallback={this.save}
              cancelCallback={this.closeDialog}/>
          <ConfirmDialog
              showing={this.state.modalDeleteConfirm}
              title="Delete Confirmation"
              message="Do you want to delete this groupMenu item from your list ?"
              okCallback={this.delete}
              cancelCallback={this.closeDialog}/>
          <Row key={1}>
            <Col>
              <Card>
                <CardHeader>GroupMenu Detail</CardHeader>
                <CardBody >
                  <Row>
                    <Col md={4}>
                      <Card body>
                        <Row>
                          <CardBlock>
                            <FormGroup>
                              <Label for="name">Name</Label>
                              <Input
                                  type="text"
                                  name="name"
                                  value={!isEmpty(this.state.groupMenu.name)?this.state.groupMenu.name:""}
                                  onChange={(e) =>{
                                    let ctg = this.state.groupMenu
                                    if(ctg!=null){
                                      ctg.name = e.target.value
                                      this.setState({
                                        groupMenu:ctg
                                      })
                                    }
                                  }}
                                 placeholder="Enter groupMenu name"
                              />
                            </FormGroup>
                            <FormGroup>
                              <ActiveOption
                                default={this.state.groupMenu.active}
                                callback={(active)=>{
                                  let ctg = this.state.groupMenu
                                  if(ctg!=null){
                                    ctg.active = active
                                    this.setState({
                                      groupMenu:ctg
                                    })
                                  }

                                }}
                            />
                            </FormGroup>
                        </CardBlock>
                        </Row>
                        <Row>
                          <Col>
                            <ButtonGroup className="float-left">
                              <Button color="danger" onClick={e => (
                                  this.setState({
                                    groupMenu:null
                                  })
                              )}> Cancel </Button>
                            </ButtonGroup>
                          </Col>
                          <Col>
                            <ButtonGroup className="float-right">
                              <Button color="primary" onClick={e=>(
                                  this.confirmSave()
                              )}> {
                                (this.state.groupMenu!=null && this.state.groupMenu.id!=null)?"Update":"Add"
                              } </Button>

                            </ButtonGroup>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                    <Col md={8}>
                      <Card body>
                        <Row>
                          <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.sortir
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "name"))}>Name</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "created"))}>Created</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(this.state.ascending, "updated"))}>Updated</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                          <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                              <DropdownToggle
                                  caret
                                  color="white"
                                  className="text-capitalize m-1">
                                {
                                  this.state.ascending?"Ascending":"Descending"
                                }
                              </DropdownToggle>
                              <DropdownMenu>
                                <DropdownItem onClick={e=>(this.refresh(true, this.state.sortir))}>Ascending</DropdownItem>
                                <DropdownItem onClick={e=>(this.refresh(false, this.state.sortir))}>Descending</DropdownItem>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Table hover>
                              <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Updated</th>
                                <th>Edit</th>
                                <th>Menu</th>
                              </tr>
                              </thead>
                              <tbody>
                              {
                                this.state.groupMenus.map((item, index) =>(
                                    <tr key={index}>
                                      <th scope="row">{++index}</th>
                                      <td>{item.name}</td>
                                      <td><ActiveLabel active={item.active}/></td>
                                      <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                        <td>
                                          <Button color="danger" onClick={e => {
                                            this.setState({
                                              groupMenu:item
                                            }, () => {
                                              this.confirmDelete()
                                            })
                                          }}>
                                            <MdDelete/>
                                          </Button>
                                          &nbsp;
                                          <Button color="primary" onClick={e=>(
                                              this.setState({
                                                groupMenu:item
                                              })
                                          )}>
                                            <MdEdit/>
                                          </Button>
                                        </td>
                                      <td><Button color="primary"><IoMdMenu/></Button></td>
                                    </tr>
                                ))
                              }
                              </tbody>
                            </Table>
                          </Col>
                        </Row>
                      </Card>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Page>
    );
  }
}
