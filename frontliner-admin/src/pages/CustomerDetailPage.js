import React from 'react';
import BasePage from "./BasePage";
import axios from "axios"

import {
    Button,
    ButtonGroup,
    Card,
    CardBody,
    CardHeader,
    CardImg,
    CardText,
    CardTitle,
    Col, DropdownItem, DropdownMenu, DropdownToggle, Form, FormFeedback,
    FormGroup,
    Input,
    Label, Nav,
    Row, Table, UncontrolledButtonDropdown
} from "reactstrap";
import userProfilePicture from 'assets/img/users/user_pp.png';
import Page from "../components/Page";
import ItemOption from "../components/Widget/ItemOption";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {
    allIsEmpty,
    currencyFormat, deleteParam,
    imageSelector,
    isEmpty,
    isValidEmail,
    parseDate,
    sortirMap
} from "../utils/Utilities";
import queryString from 'query-string';
import ConfirmDialog from "../components/modal/ConfirmDialog";
import FormText from "reactstrap/es/FormText";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {FaFacebook, FaGoogle} from "react-icons/fa/index";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import {IoMdEye} from "react-icons/io/index";
import SearchInput from '../components/SearchInput'
import Pagination from '../components/Pagination'



export default class CustomerDetailPage extends BasePage{

    constructor(props) {
        super(props);
        this.state = {
            phoneNumberWarning:"",
            customer:{},
            customerStatuses:[],
            image :null,
            customerIdQuery: queryString.parse(this.props.query).id,
            customerStatus:{},
            modalAddConfirm:false,
            ascending:false,
            sortir:'created',
            genders:[],
            gender:{},
            provinces:[],
            cities:[],
            districts:[],
            villages:[],
            province:{},
            city:{},
            district:{},
            village:{},
            transactionStatuses:[],
            transactions:[],
            transactionStatus:null,
            refCode:"",

            page :queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1,
            totalPage:0,
            totalElement:0,
            pageElement:0,


        }
        this.fetchProvinces((provinces)=>this.setState({provinces:provinces}))
        this.fetchGenders((genders)=>this.setState({genders:genders}))
        this.fetchTransactionStatus()
    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchAllCustomerStatus()
        this.fetchCustomerDetail(this.state.customerIdQuery)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1
            }, () => {
                this.fetchTransaction()
            })
        }
    }


    confirmSave = () =>{
        var customer = this.state.customer
        var image = this.state.image
        if(allIsEmpty(image, customer.firstname, customer.lastname, customer.email, customer.phoneNumber, customer.birthdate, customer.address, customer.village, customer.gender, customer.customerStatus)){
            this.showDialog("Notice", "Please complete this form fields")
        }else{
            this.setState({
                modalAddConfirm:true
            })
        }
    }

    saveUpload = () => {
        this.showProgress()
        axios({
            url: this.state.image,
            method: 'GET',
            responseType: 'blob', // important
        }).then((response) => {
            this.closeProgress()
            var file = response.data ;
            var formData = new FormData();
            formData.append("customerDtoGson", JSON.stringify(this.state.customer))
            formData.append("multipartFile", file);
            this.setState({
                modalAddConfirm:false
            }, () => {
                this.post(Global.API.CUSTOMER_SAVE_UPLOAD, formData, null, res=>{
                    this.setState({
                        customer:res.data
                    }, () => {
                        this.showDialog("Success", res.message)
                        this.fetchCustomerDetail(this.state.customer.id)
                    })
                },  true, true)
            })
        })
    }


    fetchCustomerDetail = (id) => {
        if(id!=null){
            this.get(Global.API.CUSTOMER, {
                params :{
                    id : id
                }
            }, null, res=>{
                if(res.code===200){
                    let customer = res.data;
                    this.setState({
                        customer:customer,
                        customerStatus:customer.customerStatus,
                        gender:customer.gender,
                        image:customer.image!=undefined?customer.imageLink:null,
                        village:customer.village?customer.village:null,
                        district:customer.village?customer.village.district:null,
                        city:customer.village?customer.village.district.city:null,
                        province:customer.village?customer.village.district.city.province:null,
                    }, () => {
                        if(this.state.village){
                            this.fetchCities(this.state.province.id, cities=>{
                                this.setState({
                                    cities:cities
                                })
                            })
                        }
                        if(this.state.city){
                            this.fetchDistricts(this.state.city.id, districts =>{
                                this.setState({
                                    districts:districts
                                })
                            })
                        }
                        if(this.state.disctrict){
                            this.fetchVillages(this.state.district.id, villages=>{
                                this.setState({
                                    villages:villages
                                })
                            })
                        }
                        this.fetchTransaction()
                    })
                }else{
                    this.props.history.goBack();
                }
            }, true, true);
        }
    }

    fetchAllCustomerStatus = () =>{
        this.get(Global.API.CUSTOMER_STATUSES, null, null, res =>{
            if(res.code === 200){
                this.setState({
                    customerStatuses : res.data
                })
            }
        });
    }

    fetchTransactionStatus = () =>{
        this.get(Global.API.TRANSACTION_STATUSES, null, null, response=>{
            if(response.code===200){
                this.setState({
                    transactionStatuses : response.data
                })
            }
        }, true, true)
    }

    fetchTransaction = () =>{
        this.get(Global.API.TRANSACTIONS, {
            params:{
                customerId:this.state.customer.id,
                transactionStatusId:this.state.transactionStatusId,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.refCode,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    transactions : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }


    closeDialog = () =>{
        this.setState({
            modalAddConfirm:false,
        })
    }

    verifyEmail = (email) =>{
        this.get(Global.API.USER_VALIDATE_EMAIL, {
            params:{
                email:email
            }
        }, null, response=>{
            if(response.code!=200){
                this.setState({
                    emailWarning:response.message
                })
            }
        }, true, true)

    }

    refreshTransaction = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir,
        }, () => {
            this.fetchTransaction()
        })
    )


    render() {
        var ids = [];
        var labels = [];

        let customer = this.state.customer?this.state.customer:{}
        let genders = this.state.genders?this.state.genders:[]
        let gender = this.state.gender?this.state.gender:{}

        return (
            <Page
                title="Customer Form"
                breadcrumbs={[{ name: 'customer form', active: true }]}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalAddConfirm}
                    title="Update Confirmation"
                    message="Do you want to update this customer item to your list ?"
                    okCallback={this.saveUpload}
                    cancelCallback={this.closeDialog}/>
                <Row key={1}>
                    <Col>
                        <Card>
                            <CardHeader>Customer</CardHeader>
                            <CardBody>
                                <Row>
                                    <Col md={4}>
                                        <Card>
                                            <CardHeader>
                                                Customer Image
                                            </CardHeader>
                                            <CardBody>
                                                <CardImg top src={
                                                    this.state.image!=null?this.state.image:userProfilePicture}
                                                         onClick={e=>{
                                                             if(allIsEmpty(this.state.customer, this.state.customer.id)) {
                                                                 imageSelector(file =>{
                                                                     this.setState({
                                                                         image:file
                                                                     })
                                                                 }).click()
                                                             }
                                                         }}
                                                         onError={(elm)=>this.defaultImage(elm, userProfilePicture)}/>
                                            </CardBody>
                                            <CardBody style={{verticalAlign:'text-center'}}>
                                                <Row>
                                                    <Col>
                                                        {
                                                            (allIsEmpty(this.state.customer, this.state.customer.id)) ?
                                                                <ButtonGroup className="float-left">
                                                                    <Button color="danger" onClick={e => (
                                                                        this.setState({
                                                                            image:null
                                                                        })
                                                                    )}>Cancel</Button>
                                                                </ButtonGroup>
                                                                :
                                                                null
                                                        }
                                                    </Col>
                                                    <Col>
                                                        {
                                                            (allIsEmpty(this.state.customer, this.state.customer.id)) ?
                                                                <ButtonGroup className="float-right">
                                                                    <Button color="primary" onClick={e=>{
                                                                        imageSelector(file =>{
                                                                            this.setState({
                                                                                image:file
                                                                            })
                                                                        }).click()
                                                                    }}>Upload</Button>
                                                                </ButtonGroup>
                                                                :
                                                                null
                                                        }
                                                    </Col>
                                                </Row>
                                                <br/>
                                                <Row style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Created
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {this.state.customer.created?parseDate(this.state.customer.created, DD_MM_YYYY_HH_MM_SS):"-"}
                                                    </Col>
                                                </Row>
                                                <Row style={{margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Transaction Count
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {isEmpty(this.state.customer.transactionCount)?0:this.state.customer.transactionCount}
                                                    </Col>
                                                </Row>
                                                <Row  style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Transaction Item Qty
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {isEmpty(this.state.customer.transactionQuantity)?0:this.state.customer.transactionQuantity}
                                                    </Col>
                                                </Row>
                                                <Row style={{margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Cart Count
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {isEmpty(this.state.customer.cartCount)?0:this.state.customer.cartCount}
                                                    </Col>
                                                </Row>
                                                <Row  style={{backgroundColor:'#d5d5d5', margin:'auto', padding:'8px'}}>
                                                    <Col md={6} style={{margin:'auto'}}>
                                                        Favorite Count
                                                    </Col>
                                                    <Col md={1} style={{margin:'auto'}}>
                                                        :
                                                    </Col>
                                                    <Col md={5} style={{margin:'auto'}}>
                                                        {isEmpty(this.state.customer.favoriteCount)?0:this.state.customer.favoriteCount}
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col md={8}>
                                        <Card>
                                            <CardBody>
                                                <Card body>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="firstname">Firstname</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="firstname"
                                                                    disabled={(this.state.customer!=null && this.state.customer.id)}
                                                                    value={(this.state.customer!=null && this.state.customer.firstname)?this.state.customer.firstname:""}
                                                                    onChange={(e) =>{
                                                                        let customer = this.state.customer
                                                                        if(customer!=null){
                                                                            customer.firstname = e.target.value
                                                                            this.setState({
                                                                                customer:customer
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter firstname"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="lastname">Lastname</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="lastname"
                                                                    disabled={(this.state.customer!=null && this.state.customer.id)}
                                                                    value={(this.state.customer!=null && this.state.customer.lastname)?this.state.customer.lastname:""}
                                                                    onChange={(e) =>{
                                                                        let customer = this.state.customer
                                                                        if(customer!=null){
                                                                            customer.lastname = e.target.value
                                                                            this.setState({
                                                                                customer:customer
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter lastname"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="email">Email</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="email"
                                                                    disabled={(this.state.customer!=null && this.state.customer.id)}
                                                                    value={(this.state.customer!=null && this.state.customer.email)?this.state.customer.email:""}
                                                                    onChange={(e) =>{
                                                                        let email = e.target.value
                                                                        let customer = this.state.customer
                                                                        if(customer!=null){
                                                                            customer.email = email
                                                                            this.setState({
                                                                                customer:customer
                                                                            })
                                                                        }
                                                                        if(!isValidEmail(email)){
                                                                            this.setState({
                                                                                emailWarning:"Invalid email address"
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                emailWarning:undefined
                                                                            }, () => {
                                                                                this.verifyEmail(this.state.customer.email)
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter email"
                                                                />
                                                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.emailWarning}</span></FormText>
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="phoneNumber">Phone Number</Label>
                                                                <Input type="text"
                                                                       className="form-control"
                                                                       disabled={(this.state.customer!=null && this.state.customer.id)}
                                                                       value={(this.state.customer!=null && this.state.customer.phoneNumber)?this.state.customer.phoneNumber:""}
                                                                       pattern={"[0-9]*"}
                                                                       onChange={(e)=>{
                                                                           let phoneNumber = e.target.value
                                                                           let customer = this.state.customer
                                                                           if(customer!=null && phoneNumber!=null){
                                                                               customer.phoneNumber = phoneNumber.replace(/\D/,'')
                                                                               this.setState({
                                                                                   customer:customer
                                                                               })
                                                                           }
                                                                       }}
                                                                       placeholder={"Enter phone number"}/>
                                                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.phoneNumberWarning}</span></FormText>
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="birthdate">Birthdate</Label>
                                                                <br/>
                                                                <DatePicker
                                                                    autoComplete={"off"}
                                                                    name="birthdate"
                                                                    className="input-group-text text-left"
                                                                    selected={(this.state.customer!=null && this.state.customer.birthdate)?this.state.customer.birthdate:new Date()}
                                                                    onChange={(mom) =>{
                                                                        let customer = this.state.customer
                                                                        if(customer!=null){
                                                                            customer.birthdate = moment(mom).toDate()
                                                                            this.setState({
                                                                                customer:customer
                                                                            })
                                                                        }
                                                                    }}
                                                                    dateFormat={"dd/MM/yyyy"}
                                                                    placeholderText="d/MM/yyyy"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Gender"}
                                                                    objects={genders}
                                                                    disable={(customer!=null && customer.id)}
                                                                    default={gender.id?gender.id:null}
                                                                    callback={(gender)=>{
                                                                        customer.gender = gender
                                                                        this.setState({
                                                                            customer:customer,
                                                                            gender:gender
                                                                        })
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Status"}
                                                                    objects={this.state.customerStatuses}
                                                                    default={this.state.customerStatus!=null && this.state.customerStatus.id!=undefined?this.state.customerStatus.id:null}
                                                                    callback={(customerStatus)=>{
                                                                        let customer = this.state.customer
                                                                        if(customer!=null && customerStatus!=null){
                                                                            customer.customerStatus = customerStatus
                                                                            this.setState({
                                                                                customer:customer,
                                                                                customerStatus:customerStatus
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <Label for="oAuth">OAuth</Label>
                                                                <br/>
                                                                {
                                                                    !allIsEmpty(this.state.customer, this.state.customer.facebookToken)?<FaFacebook color="blue"/>:<FaFacebook color="gray"/>
                                                                }
                                                                {' '}
                                                                {
                                                                    !allIsEmpty(this.state.customer, this.state.customer.googleToken)?<FaGoogle color="red"/>:<FaGoogle color="gray"/>
                                                                }
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                                <br/>
                                                <Card body>
                                                    <Row>
                                                        <Col md={9}>
                                                            <FormGroup>
                                                                <Label for="address">Address</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="address"
                                                                    disabled={(this.state.customer!=null && this.state.customer.id)}
                                                                    value={(this.state.customer!=null && this.state.customer.address)?this.state.customer.address:""}
                                                                    onChange={(e) =>{
                                                                        let customer = this.state.customer
                                                                        if(customer!=null){
                                                                            customer.address = e.target.value
                                                                            this.setState({
                                                                                customer:customer
                                                                            })
                                                                        }
                                                                    }}
                                                                    placeholder="Enter address"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col md={3}>
                                                            <FormGroup>
                                                                <Label for="postalCode">Postal Code</Label>
                                                                <Input
                                                                    type="text"
                                                                    name="postalCode"
                                                                    disabled={true}
                                                                    value={(this.state.customer!=null && this.state.customer.village)?this.state.customer.village.postalCode:""}
                                                                    placeholder="Postal Code"
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Province"}
                                                                    objects={this.state.provinces}
                                                                    disable={(this.state.customer!=null && this.state.customer.id)}
                                                                    default={!allIsEmpty(this.state.province)?this.state.province.id:null}
                                                                    callback={(province)=>{
                                                                        if(province!=null){
                                                                            this.setState({
                                                                                province:province,
                                                                                cities:[],
                                                                                districts:[],
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchCities(province.id, cities=>{
                                                                                    this.setState({
                                                                                        cities:cities
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                province:null,
                                                                                cities:[],
                                                                                districts:[],
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select City"}
                                                                    objects={this.state.cities}
                                                                    disable={(this.state.customer!=null && this.state.customer.id)}
                                                                    default={!allIsEmpty(this.state.city)?this.state.city.id:null}
                                                                    callback={(city)=>{
                                                                        if(city!=null){
                                                                            this.setState({
                                                                                city:city,
                                                                                districts:[],
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchDistricts(city.id, districts =>{
                                                                                    this.setState({
                                                                                        districts:districts
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                city:null,
                                                                                districts:[],
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select District"}
                                                                    objects={this.state.districts}
                                                                    disable={(this.state.customer!=null && this.state.customer.id)}
                                                                    default={!allIsEmpty(this.state.district)?this.state.district.id:null}
                                                                    callback={(district)=>{
                                                                        if(district!=null){
                                                                            this.setState({
                                                                                district:district,
                                                                                villages:[],
                                                                            }, () => {
                                                                                this.fetchVillages(district.id, villages=>{
                                                                                    this.setState({
                                                                                        villages:villages
                                                                                    })
                                                                                })
                                                                            })
                                                                        }else{
                                                                            this.setState({
                                                                                district:null,
                                                                                villages:[],
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                        <Col>
                                                            <FormGroup>
                                                                <ItemOption
                                                                    title={"Select Village"}
                                                                    objects={this.state.villages}
                                                                    disable={(this.state.customer!=null && this.state.customer.id)}
                                                                    default={!allIsEmpty(this.state.village)?this.state.village.id:null}
                                                                    callback={(village)=>{
                                                                        if(village!=null){
                                                                            let customer = this.state.customer
                                                                            customer.village = village
                                                                            this.setState({
                                                                                customer:customer,
                                                                                village:village,
                                                                            })
                                                                        }
                                                                    }}
                                                                />
                                                            </FormGroup>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                                <br/>
                                                <Row>
                                                    <Col>
                                                        <ButtonGroup className="float-left">
                                                            <Button color="danger" onClick={e => (
                                                                this.setState({
                                                                    customer:{}
                                                                }, () => {
                                                                    this.props.history.goBack();
                                                                })
                                                            )}> Back </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup className="float-right">
                                                            <Button color="primary" onClick={e=>{
                                                                e.preventDefault()
                                                                this.confirmSave()
                                                            }}> {
                                                                (this.state.customer!=null && this.state.customer.id!=null)?"Update":"Create"
                                                            } </Button>
                                                        </ButtonGroup>
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                {
                    !isEmpty(this.state.customer.id)?
                        this.renderTransaction()
                        :
                        null
                }
            </Page>

        );
    }
    renderTransaction = () =>{
        return (
            <Row key={2}>
                <Col>
                    <Card>
                        <CardHeader>Transaction</CardHeader>
                        <CardBody>
                            <Row>
                                <Col md={3}>
                                    Sort By :
                                    <UncontrolledButtonDropdown key={1}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                sortirMap(this.state.sortir.toString())
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "refCode"))}>Ref Code</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "item"))}>Item</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "quantity"))}>Quantity</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "price"))}>Price</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "created"))}>Created</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(this.state.ascending, "updated"))}>Last Updated</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    Sortir :
                                    <UncontrolledButtonDropdown key={2}>
                                        <DropdownToggle
                                            caret
                                            color="white"
                                            className="text-capitalize m-1">
                                            {
                                                this.state.ascending?"Ascending":"Descending"
                                            }
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(true, this.state.sortir))}>Ascending</DropdownItem>
                                            <DropdownItem onClick={e=>(this.refreshTransaction(false, this.state.sortir))}>Descending</DropdownItem>
                                        </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                </Col>
                                <Col md={3}>
                                    <SearchInput
                                        placeholder={"Search by ref code..."}
                                        value={this.state.refCode}
                                        onChange={e=>{
                                            this.setState({
                                                refCode:e.target.value
                                            }, () => {
                                                if(this.state.refCode===''){
                                                    this.refreshTransaction(this.state.ascending, this.state.sortir)
                                                }
                                            })
                                        }}
                                        onEnter={e=>{
                                            if(this.state.refCode!==''){
                                                this.refreshTransaction(this.state.ascending, this.state.sortir)
                                            }
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Tabs
                                activeKey={this.state.key}
                                onSelect={key =>{
                                if(key==='All'){
                                    this.setState({
                                        transactionStatusId:null
                                    }, () => {
                                        this.fetchTransaction()
                                    })
                                }else{
                                    this.setState({
                                        transactionStatusId:key
                                    }, () => {
                                        this.fetchTransaction()
                                    })
                                }
                            }
                            }>
                                <Tab eventKey="All" title="All">
                                    {this.renderTransactionTable()}
                                </Tab>
                                {
                                    this.state.transactionStatuses.map((item, index)=>(
                                        <Tab eventKey={item.id} title={item.name}>
                                            {this.renderTransactionTable()}
                                        </Tab>
                                    ))
                                }
                            </Tabs>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }

    renderTransactionTable = () =>{
        return (
            <Card>
                <CardBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ref Code</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Total Pay</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>last Update</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.transactions.map((item, index)=>(
                                <tr key={item.id}>
                                    <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                    <td>{item.refCode}</td>
                                    <td>{item.itemCount}</td>
                                    <td>{item.quantity}</td>
                                    <td>{currencyFormat(item.totalPay, CURRENCY_SYMBOL)}</td>
                                    <td>{item.transactionStatus&&item.transactionStatus.name}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                    <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td><IoMdEye color="green" onClick={event=>{
                                        this.props.history.push('/transactionDetail?id='+item.id)
                                    }}/></td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                </CardBody>
                <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />
            </Card>
        )
    }
}