import Page from 'components/Page';
import React from 'react';
import {Link} from 'react-router-dom'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardHeader,
  Col, DropdownItem, DropdownMenu, DropdownToggle,
  Row,
  Table,
  UncontrolledButtonDropdown
} from 'reactstrap';
import BasePage from "./BasePage";
import Global, {DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../utils/Global";
import {MdDelete, MdEdit, MdViewArray, MdSearch} from "react-icons/md";
import ConfirmDialog from "../components/modal/ConfirmDialog";
import SearchInput from '../components/SearchInput'
import {changeParam2, parseDate, sortirMap} from "../utils/Utilities";
import Pagination from '../components/Pagination'
import queryString from 'query-string';
import {deleteParam} from "../utils/Utilities";
import {AuthConsumer} from "../utils/AuthContext";
import Img from 'react-image'
import {IoMdOpen} from "react-icons/io";
import CusttomerInventoryCartModal from '../components/modal/CustomerInventoryCartModal'

const tableTypes = ['', 'bordered', 'striped', 'hover'];

export default class CartPage extends BasePage{


  constructor(props) {
    super(props);
    let state = this.state
      state.customers = []
      state.customer = {}
      state.customerInventoryCartModal = false
      state.modalDeleteConfirm = false
      state.ascending = false
      state.sortir = 'cic.created'
      state.search = ""
      state.page = queryString.parse(this.props.query).page?queryString.parse(this.props.query).page:1
      state.totalPage = 0
      state.totalElement = 0
      state.pageElement = 0
      this.setState(state)
  }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({
                page:queryString.parse(props.query).page?queryString.parse(props.query).page:1,
                search:queryString.parse(props.query).search?queryString.parse(props.query).search:""
            }, () => {
                this.fetchAll(this.state.ascending, this.state.sortir, this.state.search,true)
            })
        }
    }


    componentDidMount() {
        super.componentDidMount();
        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
    }

  fetchAll = (ascending, sortir, search, progressing) => {
    this.get(Global.API.CART_CUSTOMERS, {
      params:{
          ascending:ascending,
          sortir:sortir,
          page:this.state.page-1,
          search:search,
      }
    }, null, res=>{
        if(res.code === 200){
            this.setState({
                customers : res.data,
                totalPage: res.totalPage,
                totalElement: res.totalElement,
                pageElement: res.pageElement,
            })
        }
    }, progressing, true);
  }

  closeDialog = () =>{
    this.setState({
      modalAddConfirm:false,
      modalDeleteConfirm:false
    })
  }

    refreshCustomer = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )


    render() {
        var i = 0 ;

    return (
        <AuthConsumer>
            {({})=>(
            <Page
                title="Customer's Cart"
                breadcrumbs={[{ name: 'Customer\'s Cart', active: true }]}
                className="TablePage">
            {super.render()}
            <ConfirmDialog
                showing={this.state.modalDeleteConfirm}
                title="Delete Confirmation"
                message="Do you want to delete these all cart item from his cart list ?"
                okCallback={this.delete}
                cancelCallback={this.closeDialog}/>
            <CusttomerInventoryCartModal customer={this.state.customer} showing={this.state.customerInventoryCartModal} close={()=>{
                this.setState({
                    customerInventoryCartModal:false
                }, () => {
                    this.refreshCustomer(this.state.ascending, this.state.sortir, true)
                })
            }}/>
          <Row key={1}>
            <Col>
              <Card className="mb-6">
                <CardHeader>Customer</CardHeader>
                <CardBody>
                  <Row>
                        <Col md={2}>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "firstname", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "cartCount", true))}>Cart Count</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "cic.created", true))}>Created</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={2}>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col md={5}>
                            <Row>
                                <Col>
                                    <SearchInput
                                        placeholder={"Search customer, product, inventory..."}
                                        value={this.state.search}
                                        onChange={e=>{
                                            this.setState({
                                                search:e.target.value
                                            }, () => {
                                                if(this.state.search===''){
                                                    changeParam2(this.props, 'page', 1, 'search', this.state.search)
                                                }
                                            })
                                        }}
                                        onEnter={e=>{
                                            if(this.state.search!==''){
                                                changeParam2(this.props, 'page', 1, 'search', this.state.search)
                                            }
                                        }}
                                    />

                                </Col>
                                <Col>
                                    {/*<Button onClick={event=>{*/}
                                        {/*changeParam2(this.props, 'page', 1, 'search', this.state.search)*/}
                                    {/*}}><MdSearch/></Button>*/}
                                </Col>
                            </Row>
                        </Col>
                        <Col md={3}>
                            <ButtonGroup className="float-right">
                                <Button color="danger" onClick={e=>{
                                    this.openConfirmDialog("Confirmation", "Do you want to remove all customer's cart from their cart list", ()=>{
                                        this.get(Global.API.CART_REMOVE_ALL, null, null, response=>{
                                            if(response.code===200){
                                                this.successToast(response.message)
                                                this.refreshCustomer(this.state.ascending, this.state.sortir, true)
                                            }
                                        }, true, true)
                                    })
                                }}>Remove All Customer's Cart&nbsp;<MdDelete/></Button>
                            </ButtonGroup>
                        </Col>
                    </Row>
                  <Row>
                      <Table hover>
                          <thead>
                          <tr>
                              <th>#</th>
                              <th>Fullname</th>
                              <th>Email</th>
                              <th>Image</th>
                              <th>Item</th>
                              <th className="text-center">Cart Count</th>
                              <th>Created</th>
                              <th>Remove Cart Respectively</th>
                          </tr>
                          </thead>
                          <tbody>
                          {
                              this.state.customers.map((item, index)=>(
                                  <tr key={item.id}>
                                      <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                      <td>{item.firstname+" "+(item.lastname!=null?item.lastname:"")}&nbsp;
                                          <IoMdOpen
                                              color="blue" style={{cursor:'pointer'}}
                                              onClick={e=>{
                                                  this.props.history.push('/customerDetail?id='+item.id)
                                              }
                                              }/>
                                      </td>
                                      <td>{item.email}</td>
                                      <td width="5%">{
                                          item.image?
                                              <Img
                                                  className="img-thumbnail"
                                                  src={item.imageLink}
                                                  onClick={e=>{
                                                      this.setState({
                                                          imageLink:item.imageLink,
                                                          openPreview:true
                                                      })
                                                  }}
                                              />
                                              :
                                              "-"
                                      }</td>
                                      <td>
                                          <pre style={{fontSize:'12px', color:"blue"}}>{
                                              item.cartItemInfo
                                          }</pre>
                                      </td>
                                      <td align="center"><Button size="sm" onClick={event=>{
                                            this.setState({
                                                customer:item,
                                                customerInventoryCartModal:true
                                            })
                                      }}>{item.cartCount}&nbsp;<MdViewArray/></Button></td>
                                      <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                      <td>
                                          <Button size="sm" color="danger" onClick={e => {
                                              e.preventDefault()
                                              this.setState({
                                                  customer:item
                                              }, () => {
                                                  this.openConfirmDialog("Confirmation", "Do you want to remove all from his cart ?", ()=>{
                                                      this.get(Global.API.CART_REMOVE_ALL_BY_CUSTOMER, {
                                                          params:{
                                                              customerId:item.id
                                                          }
                                                      }, null, response=>{
                                                          if(response.code===200){
                                                              this.successToast(response.message)
                                                              this.refreshCustomer(this.state.ascending, this.state.sortir, true)
                                                          }
                                                      }, true, true)
                                                  })
                                              })
                                          }}>
                                             Remove His Cart &nbsp;
                                              <MdDelete/>
                                          </Button>
                                      </td>
                                  </tr>
                              ))
                          }
                          </tbody>
                      </Table>
                  </Row>
                </CardBody>
                  <Pagination {...this.props} currentPage={parseInt(this.state.page)} pageCount={this.state.totalPage} />

              </Card>
            </Col>
          </Row>
        </Page>
            )}
        </AuthConsumer>
    );
  }
};
