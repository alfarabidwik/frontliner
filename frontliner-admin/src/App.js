import {STATE_LOGIN, STATE_SIGNUP} from 'components/AuthForm';
import GAListener from 'components/GAListener';
import {EmptyLayout, LayoutRoute, MainLayout} from 'components/Layout';
import AuthPage from 'pages/AuthPage';
import {AuthProvider} from './utils/AuthContext'
import kitchen from "../src/utils/AxiosInstance";
// pages
import React from 'react';
import componentQueries from 'react-component-queries';
import {BrowserRouter, Redirect, Switch} from 'react-router-dom';
import './styles/reduction.scss';
import ProtectedRoute from "./components/Layout/ProtectedRoute";
import InventoryDetailPage from 'pages/InventoryDetailPage'
import ProductPage from 'pages/ProductPage'
import ProductDetailPage from 'pages/ProductDetailPage'
import TaskCategoryPage from 'pages/TaskCategoryPage'
import HeadingPage from 'pages/HeadingPage'
import RolePage from 'pages/RolePage'
import InventoryPage from 'pages/InventoryPage'
import BannerPage from 'pages/BannerPage'
import AdminPage from 'pages/AdminPage'
import AdminDetailPage from 'pages/AdminDetailPage'
import CustomerPage from 'pages/CustomerPage'
import CustomerDetailPage from 'pages/CustomerDetailPage'
import TransactionPage from 'pages/TransactionPage'
import TransactionDetailPage from 'pages/TransactionDetailPage'
import FlashSalePage from 'pages/FlashSalePage'
import FlashSaleDetailPage from 'pages/FlashSaleDetailPage'
import SelebgramPage from 'pages/SelebgramPage'
import ColorPage from 'pages/ColorPage'
import SizePage from 'pages/SizePage'
import LatestProductPage from 'pages/LatestProductPage'
import PopularProductPage from 'pages/PopularProductPage'
import DiscountPage from 'pages/DiscountPage'
import VoucherPage from 'pages/VoucherPage'
import VoucherDetailPage from 'pages/VoucherDetailPage'
import PointPage from 'pages/PointPage'
import PointDetailPage from 'pages/PointDetailPage'
import ConfigurationPage from 'pages/ConfigurationPage'
import MainProductPage from 'pages/MainProductPage'
import RoleMenuDetailPage from 'pages/RoleMenuDetailPage'
import ProfilePage from "./pages/ProfilePage";
import ProvincePage from "./pages/ProvincePage";
import CityPage from "./pages/CityPage";
import DistrictPage from "./pages/DistrictPage";
import VillagePage from "./pages/VillagePage";
import BankPage from "./pages/BankPage";
import Global from "./utils/Global";
import {storeData} from "./utils/StorageUtil";
import BankAccountPage from "./pages/BankAccountPage";
import MidtransMediatorPage from "./pages/MidtransMediatorPage";
import PaymentMediaPage from "./pages/PaymentMediaPage";
import StockAuditPage from "./pages/StockAuditPage";
import AdminActivityPage from "./pages/AdminActivityPage";
import UserActivityPage from "./pages/UserActivityPage";
import CourierPage from "./pages/CourierPage";
import MainDashboardPage from "./pages/MainDashboardPage";
import OnlineShopPage from "./pages/OnlineShopPage";
import PrintInvoicePage from "./pages/PrintInvoicePage";
import "./App.css"
import BroadcastPage from "./pages/BroadcastPage";
import BroadcastForm from "./pages/BroadcastForm";
import SubscriberPage from "./pages/SubscriberPage";
import CartPage from "./pages/CartPage";
import BrandPage from "./pages/BrandPage";
import FormBuilderDetailPage from "./pages/FormBuilderDetailPage";
import FormBuilderPage from "./pages/FormBuilderPage";
import TaskPage from "./pages/TaskPage";
import TaskDetailPage from "./pages/TaskDetailPage";
import TaskApplicationPage from "./pages/TaskApplicationPage";
import PartnerPage from "./pages/PartnerPage";
import PartnerDetailPage from "./pages/PartnerDetailPage";
import FaqPage from "./pages/FaqPage";
import UserPage from "./pages/UserPage";
import UserDetailPage from "./pages/UserDetailPage";
import DevicePage from "./pages/DevicePage";
import VerificationPage from "./pages/VerificationPage";
import RegistrationPage from "./pages/RegistrationPage";
import NotificationPage from "./pages/NotificationPage";
import NotificationDetailPage from "./pages/NotificationDetailPage";
import TaskApplicationDetailPage from "./pages/TaskApplicationDetailPage";
import WithdrawalRequestPage from "./pages/WithdrawalRequestPage";
import BalanceMutationPage from "./pages/BalanceMutationPage";


const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {


  componentDidMount() {
    fetchAppSetting()
  }


  render() {
    return (
      <BrowserRouter basename={getBasename()}>
        <AuthProvider>
          <GAListener>
            <Switch>
              <ProtectedRoute
                  exact
                  path="/tasks"
                  layout={MainLayout}
                  component={TaskPage}
              />
              <ProtectedRoute
                  exact
                  path="/taskDetail"
                  layout={MainLayout}
                  component={TaskDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/taskApplications"
                  layout={MainLayout}
                  component={TaskApplicationPage}
              />
              <ProtectedRoute
                  exact
                  path="/taskApplicationDetail"
                  layout={MainLayout}
                  component={TaskApplicationDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/withdrawalRequests"
                  layout={MainLayout}
                  component={WithdrawalRequestPage}
              />
              <ProtectedRoute
                  exact
                  path="/balanceMutations"
                  layout={MainLayout}
                  component={BalanceMutationPage}
              />
              <ProtectedRoute
                  exact
                  path="/partners"
                  layout={MainLayout}
                  component={PartnerPage}
              />
              <ProtectedRoute
                  exact
                  path="/partnerDetail"
                  layout={MainLayout}
                  component={PartnerDetailPage}
              />

              <ProtectedRoute
                  exact
                  path="/faqs"
                  layout={MainLayout}
                  component={FaqPage}
              />
                <ProtectedRoute
                    exact
                    path="/users"
                    layout={MainLayout}
                    component={UserPage}
                />
                <ProtectedRoute
                    exact
                    path="/userDetail"
                    layout={MainLayout}
                    component={UserDetailPage}
                />
                <ProtectedRoute
                    exact
                    path="/userDevices"
                    layout={MainLayout}
                    component={DevicePage}
                />

              <ProtectedRoute
                  exact
                  path="/needToVerify"
                  layout={MainLayout}
                  component={VerificationPage}
              />
              <ProtectedRoute
                  exact
                  path="/notifications"
                  layout={MainLayout}
                  component={NotificationPage}
              />
              <ProtectedRoute
                  exact
                  path="/notificationDetail"
                  layout={MainLayout}
                  component={NotificationDetailPage}
              />

              <ProtectedRoute
                  exact
                  path="/registrations"
                  layout={MainLayout}
                  component={RegistrationPage}
              />

              <ProtectedRoute
                  exact
                  path="/brands"
                  layout={MainLayout}
                  component={BrandPage}
              />
              <ProtectedRoute
                  exact
                  path="/categories"
                  layout={MainLayout}
                  component={TaskCategoryPage}
              />
              <ProtectedRoute
                  exact
                  path="/provinces"
                  layout={MainLayout}
                  component={ProvincePage}
              />
              <ProtectedRoute
                  exact
                  path="/cities"
                  layout={MainLayout}
                  component={CityPage}
              />
              <ProtectedRoute
                  exact
                  path="/districts"
                  layout={MainLayout}
                  component={DistrictPage}
              />
                <ProtectedRoute
                    exact
                    path="/villages"
                    layout={MainLayout}
                    component={VillagePage}
                />

              <ProtectedRoute
                  exact
                  path="/headings"
                  layout={MainLayout}
                  component={HeadingPage}
              />
              <ProtectedRoute
                  exact
                  path="/roles"
                  layout={MainLayout}
                  component={RolePage}
              />
              <ProtectedRoute
                  exact
                  path="/paymentMedias"
                  layout={MainLayout}
                  component={PaymentMediaPage}
              />
              <ProtectedRoute
                  exact
                  path="/banks"
                  layout={MainLayout}
                  component={BankPage}
              />
              <ProtectedRoute
                  exact
                  path="/couriers"
                  layout={MainLayout}
                  component={CourierPage}
              />
                <ProtectedRoute
                    exact
                    path="/midtransMediators"
                    layout={MainLayout}
                    component={MidtransMediatorPage}
                />
              <ProtectedRoute
                  exact
                  path="/bankAccounts"
                  layout={MainLayout}
                  component={BankAccountPage}
              />
              <ProtectedRoute
                  exact
                  path="/onlineShops"
                  layout={MainLayout}
                  component={OnlineShopPage}
              />

              <ProtectedRoute
                  exact
                  path="/menu"
                  layout={MainLayout}
                  component={RoleMenuDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/inventories"
                  layout={MainLayout}
                  component={InventoryPage}
              />
              <ProtectedRoute
                  exact
                  path="/inventoryDetail"
                  layout={MainLayout}
                  component={InventoryDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/banners"
                  layout={MainLayout}
                  component={BannerPage}
              />
              <ProtectedRoute
                  exact
                  path="/admins"
                  layout={MainLayout}
                  component={AdminPage}
              />
              <ProtectedRoute
                  exact
                  path="/adminDetail"
                  layout={MainLayout}
                  component={AdminDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/customers"
                  layout={MainLayout}
                  component={CustomerPage}
              />
              <ProtectedRoute
                  exact
                  path="/customerDetail"
                  layout={MainLayout}
                  component={CustomerDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/transactions"
                  layout={MainLayout}
                  component={TransactionPage}
              />
              <ProtectedRoute
                  exact
                  path="/transactionDetail"
                  layout={MainLayout}
                  component={TransactionDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/carts"
                  layout={MainLayout}
                  component={CartPage}
              />

              <ProtectedRoute
                  exact
                  path="/stockAudit"
                  layout={MainLayout}
                  component={StockAuditPage}
              />
              <ProtectedRoute
                  exact
                  path="/adminActivities"
                  layout={MainLayout}
                  component={AdminActivityPage}
              />
              <ProtectedRoute
                  exact
                  path="/userActivities"
                  layout={MainLayout}
                  component={UserActivityPage}
              />
              <ProtectedRoute
                  exact
                  path="/flashSales"
                  layout={MainLayout}
                  component={FlashSalePage}
              />
              <ProtectedRoute
                  exact
                  path="/flashSaleDetail"
                  layout={MainLayout}
                  component={FlashSaleDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/selebgrams"
                  layout={MainLayout}
                  component={SelebgramPage}
              />
              <ProtectedRoute
                  exact
                  path="/colors"
                  layout={MainLayout}
                  component={ColorPage}
              />
              <ProtectedRoute
                  exact
                  path="/sizes"
                  layout={MainLayout}
                  component={SizePage}
              />
              <ProtectedRoute
                  exact
                  path="/latests"
                  layout={MainLayout}
                  component={LatestProductPage}
              />
              <ProtectedRoute
                  exact
                  path="/populars"
                  layout={MainLayout}
                  component={PopularProductPage}
              />
              <ProtectedRoute
                  exact
                  path="/configurations"
                  layout={MainLayout}
                  component={ConfigurationPage}
              />
              <ProtectedRoute
                  exact
                  path="/discounts"
                  layout={MainLayout}
                  component={DiscountPage}
              />
              <ProtectedRoute
                  exact
                  path="/vouchers"
                  layout={MainLayout}
                  component={VoucherPage}
              />
              <ProtectedRoute
                  exact
                  path="/voucherDetail"
                  layout={MainLayout}
                  component={VoucherDetailPage}
              />
              <ProtectedRoute
                  exact
                  path="/points"
                  layout={MainLayout}
                  component={PointPage}
              />
              <ProtectedRoute
                  exact
                  path="/pointDetail"
                  layout={MainLayout}
                  component={PointDetailPage}
              />
                <ProtectedRoute
                    exact
                    path="/mainProducts"
                    layout={MainLayout}
                    component={MainProductPage}
                />
                <LayoutRoute
                  exact
                  path="/login"
                  layout={EmptyLayout}
                  component={props => (
                      <AuthPage {...props} authState={STATE_LOGIN} />
                  )}
              />
              <ProtectedRoute
                  exact
                  path="/broadcast"
                  layout={MainLayout}
                  component={BroadcastPage}
              />
                <ProtectedRoute
                    exact
                    path="/sendAnnouncement"
                    layout={MainLayout}
                    component={BroadcastForm}
                />
                <ProtectedRoute
                    exact
                    path="/subscriber"
                    layout={MainLayout}
                    component={SubscriberPage}
                />
              <LayoutRoute
                  exact
                  path="/signup"
                  layout={EmptyLayout}
                  component={props => (
                      <AuthPage {...props} authState={STATE_SIGNUP} />
                  )}
              />
              <ProtectedRoute
                  exact
                  path="/"
                  layout={MainLayout}
                  component={MainDashboardPage}
              />
              <ProtectedRoute
                  exact
                  path="/profile"
                  layout={MainLayout}
                  component={ProfilePage}
              />
              <ProtectedRoute
                  exact
                  path="/printInvoice"
                  layout={EmptyLayout}
                  component={PrintInvoicePage}
              />
              <ProtectedRoute
                  exact
                  path="/formBuilders"
                  layout={MainLayout}
                  component={FormBuilderPage}
              />
              <ProtectedRoute
                  exact
                  path="/formBuilderDetail"
                  layout={MainLayout}
                  component={FormBuilderDetailPage}
              />

              <Redirect to="/" />
            </Switch>
          </GAListener>
        </AuthProvider>
      </BrowserRouter>
    );
  }
}
function fetchAppSetting(){
  get(Global.API.CONFIGURATION_CURRENT_ACTIVE, null, null, response=>{
    if(response.data.code===200){
      storeData(Global.CONFIGURATION, response.data.data)
    }
  })
}

function get(url, params, config, callback){
  kitchen.get(url, params, config).then(response=>{
    callback(response)
  }).catch(e=>{

  });
}




const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
