import React from 'react'
import CurrencyInput from 'react-currency-input';
import PropTypes from 'prop-types'
import {CURRENCY_SYMBOL} from "../../utils/Global";

export default class PriceInput extends React.Component{

    constructor(props) {
        super(props);

    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({})
        }

    }


    render() {
        let props = this.props
        return (
            <CurrencyInput
                name={props.name}
                className={props.className}
                prefix={CURRENCY_SYMBOL+" "}
                value={props.value}
                ref={props.name}
                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                    if(props.onChangeEvent){
                        props.onChangeEvent(e, maskedvalue, floatvalue)
                    }
                }}
                allowNegative={false}
                precision={0}
                placeholder={props.placeholder}>
            </CurrencyInput>
        );
    }
}

PriceInput.propTypes = {
    name:PropTypes.string,
    className:PropTypes.string,
    value:PropTypes.number,
    placeholder:PropTypes.string,
    onChangeEvent:PropTypes.func

}
