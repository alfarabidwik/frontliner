import React from 'react'
import CurrencyInput from 'react-currency-input';
import PropTypes from 'prop-types'
import {CURRENCY_SYMBOL} from "../../utils/Global";

export default class PercentInput extends React.Component{

    constructor(props) {
        super(props);

    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({})
        }

    }


    render() {
        let props = this.props
        return (
            <CurrencyInput {...props}
                name={props.name}
                className={props.className}
                value={props.value}
                ref={props.name}
                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                    if(props.onChangeEvent){
                        props.onChangeEvent(e, maskedvalue, floatvalue)
                    }
                }}
                allowNegative={false}
                thousandSeparator=""
                precision={2}
                placeholder={props.placeholder}>
            </CurrencyInput>
        );
    }
}

PercentInput.propTypes = {
    name:PropTypes.string,
    className:PropTypes.string,
    value:PropTypes.number,
    placeholder:PropTypes.string,
    onChangeEvent:PropTypes.func

}
