import React from 'react'
import PropTypes from 'prop-types'
import addImage from './../../assets/img/widget/add.png'
import Gallery from 'react-photo-gallery'


export default class VerticalImageGallery extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            images : this.props.images,
            addFunction : this.props.addFunction,
        }
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                images : props.images,
                addFunction : props.addFunction,
            })
        }
    }

    render() {
        const addFunctionButton = {
            id:undefined,
            imageLink: addImage
        }
        let photos = [];
        let images = []
        images.push(...this.state.images)
        images.push(addFunctionButton)
        images.map((item, index)=>{
            let photo = {
                src : item.imageLink,
                width:1,
                height:1,
                key:index
            }
            photos.push(photo)
        })

        return (
            <Gallery
                photos={photos}
                direction={"column"}
                columns={1}
                onClick={(event, photo)=>this.props.onClick(event, photo, images[photo.index])}
            />
        );
    }

}

VerticalImageGallery.propTypes = {
    images : PropTypes.array,
    addFunction : PropTypes.bool,
    onClick: PropTypes.func,
}