import React from 'react';
import {Form, FormGroup, Input, Label} from "reactstrap";
import PropTypes from 'prop-types'
export default class ActiveOption extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            callback : this.props.callback,
            default:this.props.default?this.props.default:false,
            label:this.props.label
        }

    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(this.props!=nextProps){
            this.setState({
                callback : nextProps.callback,
                default:nextProps.default?nextProps.default:false,
                label:this.props.label
            })
        }
    }

    render() {
        let label = this.state.label?this.state.label:"Select Status"
        let trueLabel = this.state.trueLabel?this.state.trueLabel:"Active"
        let falseLabel = this.state.falseLabel?this.state.falseLabel:"Inactive"

        return (
            <FormGroup>
                <Label for="select">{label}</Label>
                <Input type="select" name="select" onClick={e => {
                    if(this.props.callback!=undefined){
                        this.props.callback(e.target.value)
                    }
                }} value={this.state.default} onChange={e=>{
                    e.preventDefault()
                    this.setState({
                        default:e.target.value
                    })
                }}>
                    <option value={true}>{trueLabel}</option>
                    <option value={false}>{falseLabel}</option>
                </Input>
            </FormGroup>
        );
    }
}

ActiveOption.propTypes = {
    label:PropTypes.string,
    callback : PropTypes.func,
    default:PropTypes.any,
    trueLabel:PropTypes.string,
    falseLabel:PropTypes.string

}
