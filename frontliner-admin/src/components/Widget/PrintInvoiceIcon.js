import React from 'react';
import printLogo from "../../img/invoice/logo.png";
import BaseComponent from "../BaseComponent";
import Button from "reactstrap/es/Button";
import {currencyFormat, parseDate, statusColor} from "../../utils/Utilities";
import PropTypes from 'prop-types'
import {MdPictureAsPdf, MdPrint} from "react-icons/md";
import ReactToPdf from 'react-to-pdf'
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY} from "../../utils/Global";
import {getData} from "../../utils/StorageUtil";

const ref = React.createRef();

export default class PrintInvoiceIcon extends BaseComponent {

    constructor(props) {
        super(props);
        let state = this.state
        state.transaction = this.props.transaction
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            let state = this.state
            state.transaction = props.transaction
            this.setState(state)
        }

    }


    render() {
        let configuration = getData(Global.CONFIGURATION)
        if(!configuration){
            configuration = {}
        }

        let transaction = this.state.transaction?this.state.transaction:{} ;
        let transactionDetails = transaction.transactionDetails?transaction.transactionDetails:[]
        let customerAddress = transaction.customerAddress?transaction.customerAddress:{}

        let currentDate = new Date();
        let courierCost = transaction.cost?transaction.cost:{}



        return (
        <div>
            <ReactToPdf targetRef={ref} filename={parseDate(currentDate, 'yyyymmdd_hhMMss')+"_"+transaction.refCode+".pdf"}>
                {({toPdf}) => (
                    <Button color="primary" onClick={toPdf}><MdPictureAsPdf/></Button>
                )}
            </ReactToPdf>
            <div className="print-container" ref={ref} style={{position:'absolute', zIndex:'-1'}}>
            <div className="invoice-wrap">
                <div className="header">
                    <div className="logo">
                        <img src={printLogo} width="270px;"/>
                    </div>
                    <div className="invoice-id-wrap">
                        <div className="invoice-id">#{parseDate(transaction.created, DD_MM_YYYY)}/{transaction.refCode}</div>
                        <span className="separator">·</span>
                        <div className="invoice-date">{parseDate(currentDate, DD_MM_YYYY)}</div>
                    </div>
                </div>
                <div className="content">
                    <table>
                        <tbody>
                        <tr>
                            <th width="62%" align="left">PESANAN</th>
                            <th width="50px">JUMLAH</th>
                            <th width="120px" align="right">HARGA</th>
                            <th align="right">SUBTOTAL</th>
                        </tr>
                        {
                            transactionDetails.map((item, index)=>(
                                <tr key={index} className="firstlist">
                                    <td>{item.inventory.product.name}<br/><span className="type">{item.inventory.typeName}</span></td>
                                    <td align="center">{item.quantity}</td>
                                    <td align="right">{currencyFormat(item.inventory.finalPrice, CURRENCY_SYMBOL)}</td>
                                    <td align="right">{currencyFormat(item.totalPrice, CURRENCY_SYMBOL)}</td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <div className="row">
                        <div className="col-50">
                            <div className="label">ALAMAT PENERIMA</div>
                            <div className="address">
                                <div className="title-addres"><span id="selected__title">{customerAddress.title}</span></div>
                                <div className="title-addres"><span id="selected__title">{customerAddress.receiverName}</span></div>
                                <div className="detail-addres">
                                    <div id="selected__address">{customerAddress.address}</div>
                                    <div>
                                        <span id="selected__area">{customerAddress.village?customerAddress.village.name:"-"}, {customerAddress.village?customerAddress.village.district.name:"-"}</span>,
                                        <span id="selected__city">{customerAddress.village?customerAddress.village.district.city.name:"-"}</span></div>
                                    <div>
                                        <span id="selected__province">{customerAddress.village?customerAddress.village.district.city.province.name:"-"}</span>,
                                        <span id="elected__post_code">{customerAddress.village?customerAddress.village.postalCode:"-"},</span></div>
                                    <div id="selected__phone">{customerAddress.receiverPhoneNumber}</div>
                                </div>
                                <div className="shipping">
                                    <div className="jne"></div>
                                    <div className="shipping-provider"><strong>{transaction.courierName}</strong>&nbsp;{courierCost.service}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-50 total-payment">
                            <div className="row">
                                <div className="col-50">
                                    <div className="label">SUBTOTAL</div>
                                    {/*jika terdapat potongan voucher */}
                                    <div className="label">POTONGAN VOUCHER</div>
                                    <div className="label">BIAYA PENGIRIMAN</div>
                                    {/*jika terdapat potongan ongkir*/}
                                    <div className="label">POTONGAN ONGKIR</div>
                                    <div className="label">TOTAL</div>
                                </div>
                                <div className="col-50">
                                    <div className="total">{
                                        currencyFormat(transaction.totalItemPrice, CURRENCY_SYMBOL)
                                    }</div>
                                    <div className="total">{
                                        currencyFormat(transaction.itemCutOff, CURRENCY_SYMBOL)
                                    }</div>
                                    <div className="total">{
                                        currencyFormat(transaction.courierCost, CURRENCY_SYMBOL)
                                    }</div>
                                    <div className="total">{
                                        currencyFormat(transaction.deliveryFeeCutOff, CURRENCY_SYMBOL)
                                    }</div>
                                    <div className="total price-all"><strong>{
                                        currencyFormat(transaction.totalPay, CURRENCY_SYMBOL)
                                    }</strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="cut-separator"></div>
            <div className="shipping-label">
                <div className="shipping-label-container">
                    <div className="logo">
                        <img src={printLogo} width="170px;"/>
                    </div>
                    <div className="invoice-id-wrap">
                        <div className="invoice-id">#{parseDate(transaction.created, DD_MM_YYYY)}/{transaction.refCode}</div>
                    </div>
                    <div className="receiver">
                        <div className="label">KEPADA</div>
                        <div className="address">
                            <div className="detail-addres">
                                {/*<div className="title-addres"><span id="selected__title">{customerAddress.title}</span></div>*/}
                                <div className="title-addres"><span id="selected__title">{customerAddress.receiverName}</span></div>
                                <div className="detail-addres">
                                    <div id="selected__address">{customerAddress.address}</div>
                                    <div>
                                        <span id="selected__area">{customerAddress.village?customerAddress.village.name:"-"}, {customerAddress.village?customerAddress.village.district.name:"-"}</span>,
                                        <span id="selected__city">{customerAddress.village?customerAddress.village.district.city.name:"-"}</span></div>
                                    <div>
                                        <span id="selected__province">{customerAddress.village?customerAddress.village.district.city.province.name:"-"}</span>,
                                        <span id="elected__post_code">{customerAddress.village?customerAddress.village.postalCode:"-"}</span></div>
                                    <div id="selected__phone">{customerAddress.receiverPhoneNumber}</div>
                                </div>
                                <div className="shipping">
                                    <div className="jne"></div>
                                    <div className="shipping-provider"><strong>{transaction.courierName}</strong>&nbsp;{courierCost.service}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="note">
                        Terima kasih telah berbelanja di {configuration.companyName}
                    </div>
                </div>
            </div>
            </div>
        </div>
    );
  }
}

PrintInvoiceIcon.propTypes = {
    transaction : PropTypes.object.isRequired
}
