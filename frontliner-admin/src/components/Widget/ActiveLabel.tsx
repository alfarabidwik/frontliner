import * as React from 'react'
import Typography from "../../components/Typography";

class Props{
    active : boolean = false
    trueLabel : string = ""
    falseLabel : string = ""
}

export default class ActiveLabel extends React.Component{

    props : Props = new Props()
    state : any = {}

    constructor(props : any) {
        super(props);
        this.state = {
            trueLabel:this.props.trueLabel,
            falseLabel:this.props.falseLabel
        }
    }
    componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        this.setState({
            trueLabel:nextProps.trueLabel,
            falseLabel:nextProps.falseLabel
        })
    }

    render() {
        let trueLabel = this.state.trueLabel
        let falseLabel = this.state.falseLabel
        if(!trueLabel){
            trueLabel = "Active"
        }
        if(!falseLabel){
            falseLabel = "Inactive"
        }

        return (
            <Typography tag={""} type={""} className={this.props.active?"text-primary":"text-warning"}>
                <strong>
                    {
                        this.props.active?trueLabel:falseLabel
                    }
                </strong>
            </Typography>
        );
    }
}

// ActiveLabel.propTypes = {
//     active:PropTypes.bool.isRequired,
//     trueLabel:PropTypes.string,
//     falseLabel:PropTypes.string,
// }