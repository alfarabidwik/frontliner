import BaseComponent from "../../BaseComponent";
import React from 'react'
import './ColorItem.css'

export default class ColorItem extends BaseComponent{

    constructor(props) {
        super(props);
        this.state = {
            color : this.props.color,
            selected:this.props.selected!=undefined?this.props.selected:false,
        }

    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(this.props!=nextProps){
            this.setState({
                color : nextProps.color,
                selected:nextProps.selected!=undefined?nextProps.selected:false,
            }, () => {
            })
        }

    }

    render() {
        const className = this.state.selected?"color-pallete selected":"color-pallete"
        return (
            <div className="color-box">
                {/*<div className={"color-pallete "+this.state.color.name.toString()+(this.state.selected?" selected":"")} style={{backgroundColor:this.state.color.hexa}}>*/}
                <div className={className} style={{backgroundColor:this.state.color.hexa}} onClick={e=>{
                    this.setState({
                        selected:!this.state.selected
                    }, () => {
                        if(this.state.selected){
                            this.props.onSelect(this.state.color)
                        }else{
                            this.props.onRemove(this.state.color)
                        }
                    })
                }}>
                    <div className="check"/>
                </div>
            </div>
        )
    }


}