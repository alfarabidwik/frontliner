import BaseComponent from "../../BaseComponent";
import React from 'react'
import './ColorContainer.css'
import ColorItem from './ColorItem'

export default class ColorContainer extends BaseComponent{

    constructor(props) {
        super(props);
        this.state = {
            colors : [],
            selectedColors:this.props.selectedColors!=null?this.props.selectedColors:[],
            selectedColorMap : new Object()
        }
        this.state.selectedColors.map((color, index) =>{
            if(color!=null){
                this.state.selectedColorMap[color.id] = color
            }
        })
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(this.props!=nextProps){
            this.setState({
                colors : nextProps.colors,
                selectedColors:nextProps.selectedColors!=null?nextProps.selectedColors:[],
            }, () => {
                this.state.selectedColors.map((color, index) =>{
                    if(color!=null){
                        this.state.selectedColorMap[color.id] = color
                    }
                })
            })
        }
    }

    onSelect = (color) =>{
        var selectedColorMap = this.state.selectedColorMap
        selectedColorMap[color.id] = color

        this.setState({
            selectedColorMap:selectedColorMap
        }, () => {
            var selectedColors = []
            var i = 0;
            for(var key in selectedColorMap){
                if(selectedColorMap.hasOwnProperty(key)){
                    selectedColors[i] = selectedColorMap[key]
                }
                i++
            }
            for (let i = 0; i < selectedColorMap.length; i++) {
                selectedColors[i] = selectedColorMap[i]
            }
            this.props.callback(selectedColors)
        })
    }

    onRemove = (color) =>{
        var selectedColorMap = this.state.selectedColorMap
        selectedColorMap[color.id] = null
        this.setState({
            selectedColorMap:selectedColorMap
        }, () => {
            var selectedColors = []
            var i = 0;
            for(var key in selectedColorMap){
                if(selectedColorMap.hasOwnProperty(key)){
                    selectedColors[i] = selectedColorMap[key]
                }
                i++
            }
            this.props.callback(selectedColors)
        })
    }

    render() {
        return (
            <div className="color-container">
                {
                    this.state.colors.map((color,index)=>(
                        <ColorItem
                            key={color.id}
                            color={color}
                            selected={this.state.selectedColorMap[color.id]!=null?true:false}
                            onSelect={this.onSelect}
                            onRemove={this.onRemove}/>

                    ))
                }
            </div>
        )
    }

}