import React from 'react'
import PropTypes from 'prop-types'
import {Col, Input} from "reactstrap";
import {parseDate} from "../../utils/Utilities";
import moment from "moment";
import Datetime from 'react-datetime'

export default class DateInput extends React.Component{

    state={}

    constructor(props) {
        super(props);

    }

    componentWillReceiveProps(props, nextContext) {
        if(props!=this.props){
            this.setState({})
        }

    }

    /*
    *             <Input {...props}
                id={props.id}
                type="date"
                max={props.maxdate?parseDate(props.maxdate, "yyyy-mm-dd"):""}
                min={props.mindate?parseDate(props.mindate, "yyyy-mm-dd"):""}
                value={props.value?moment(props.value).format("YYYY-MM-DD"):""}
                onChange={(e) =>{
                    if(props.onChange){
                        props.onChange(e)
                    }
                }}
                placeholder={props.placeholder}
            />

    * */

    render() {
        let props = this.props
        return (
            <Datetime
                id={props.id}
                open={this.state.open}
                inputProps={
                    {
                        disabled:props.disabled,
                        placeholder:'Scheduled time...'
                    }
                }
                value={props.value?moment(props.value):""}
                onChange={(e) =>{
                    if(props.onChange){
                        props.onChange(e)
                    }
                }}
                closeOnSelect={true}
            />
        );
    }
}

DateInput.propTypes = {
    id:PropTypes.string,
    maxdate:PropTypes.object,
    mindate:PropTypes.object,
    value:PropTypes.object,
    placeholder:PropTypes.string,
    onChange:PropTypes.func,
    disabled:PropTypes.bool
}
