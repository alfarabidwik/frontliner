import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import {allIsEmpty, isEmpty, parseDate} from "../../utils/Utilities";
import DatePicker from 'react-datepicker';
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import FormText from "reactstrap/es/FormText";
import DateInput from "../Widget/DateInput";

/*= (message, okCallback, cancelCallback) =>*/
export default class FlashSaleModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            edit:this.props.edit,
            flashSale:this.props.flashSale,
            close:this.props.close,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                edit:props.edit,
                flashSale:props.flashSale,
                close:props.close,
            })
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    saveConfirm = () =>{
        let flashSale = this.state.flashSale
        if(isEmpty(flashSale.name)){
            this.setState({
                nameWarning:"Enter a name"
            })
        }
        if(isEmpty(flashSale.startDate)){
            this.setState({
                startDateWarning:"Enter a start date"
            })
        }
        if(isEmpty(flashSale.endDate)){
            this.setState({
                endDateWarning:"Enter a end date"
            })
        }

        if(!allIsEmpty(flashSale.name, flashSale.startDate, flashSale.endDate)){
            this.save()
        }
    }

    save = () =>{
        let flashSale = this.state.flashSale;
        flashSale.flashSaleContents = null
        if(!flashSale.active){
            flashSale.active = false
        }
        this.setState({
            flashSale : flashSale
        })
        this.post(Global.API.FLASHSALE_SAVE, this.state.flashSale, null, response=>{
            if(response.code===200){
                this.props.refresh(null, null, true)
                this.close()
            }else{
                let message = response.message
                if(response.messageError){
                    message = ""
                    response.messageError.map((s, index)=>(
                        message = message+s+", "
                    ))
                }
            }
        }, true, true)
    }

    close = () =>{
        this.setState({
            flashSale:{},
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    render(){
        let flashSale = this.state.flashSale?this.state.flashSale:{}
        return (
            /*xl-lg-sm*/
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    {this.state.edit?"Edit Flash Sale":"Create FLash Sale"}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Flash Sale Name</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={flashSale.name?flashSale.name:""}
                                    onChange={(e) =>{
                                        this.setState({
                                            nameWarning:null
                                        })
                                        flashSale.name = e.target.value
                                        this.setState({
                                            flashSale:flashSale
                                        })
                                    }}
                                    placeholdertext="Enter flash sale name"
                                />
                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.nameWarning}</span></FormText>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="startDate">Start Date</Label>
                                <DateInput
                                    id="startDate"
                                    value={flashSale.startDate}
                                    maxdate={flashSale.endDate}
                                    onChange={(e) =>{
                                        this.setState({
                                            startDateWarning:null
                                        })
                                        flashSale.startDate = e.target.value
                                        this.setState({
                                            flashSale:flashSale
                                        })
                                    }}
                                    placeholdertext="Enter flash sale name"
                                />
                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.startDateWarning}</span></FormText>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="endDate">End Date</Label>
                                <DateInput
                                    id="endDate"
                                    value={flashSale.endDate}
                                    mindate={flashSale.startDate}
                                    onChange={(e) =>{
                                        this.setState({
                                            endDateWarning:null
                                        })
                                        flashSale.endDate = e.target.value
                                        this.setState({
                                            flashSale:flashSale
                                        })
                                    }}
                                    placeholdertext="Enter flash sale name"
                                />
                                <FormText className={"font-italic"}><span style={{color:'red'}}>{this.state.endDateWarning}</span></FormText>
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        //
                        this.saveConfirm()
                    }}>
                        Ok
                    </Button>{' '}
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
