import React from 'react';
import {Button, CardImg, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import Input from "reactstrap/es/Input";
import {
    allIsEmpty, currencyFormat,
    finalPriceByPercent,
    isEmpty,
    parseDate,
    percentByDiscount,
    percentToPrice
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {CURRENCY_SYMBOL} from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import Checkbox from "../../components/Checkbox"
import FormText from "reactstrap/es/FormText";
import ConfirmDialog from "./ConfirmDialog";
import NumberInput from "../Widget/NumberInput";
import PriceInput from "../Widget/PriceInput";
import PercentInput from "../Widget/PercentInput";

/*= (message, okCallback, cancelCallback) =>*/
export default class FlashSaleContentModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            flashSale:this.props.flashSale,
            product:this.props.product,
            close:this.props.close,
            flashSaleContents:[],
            checkedAll:false,
            allPercentageDiscount:0.0,
            allPriceDiscount:0.0,
            allFinalPrice:0.0,
            allStock:0,
            modalSaveConfirm:false
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                flashSale:props.flashSale,
                close:props.close,
                product:props.product,
                modalSaveConfirm:false
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.flashSaleContentByFlashSale(true)
                }
            })
        }
    }

    flashSaleContentByFlashSale = (progressing) =>{
        if(!allIsEmpty(this.state.flashSale,this.state.product)){
            this.get(Global.API.FLASHSALE_CONTENT_BY_FLASHSALE, {
                params:{
                    flashSaleId:this.state.flashSale.id,
                    productId:this.state.product.id
                }
            }, null, response=>{
                if(response.code===200){
                    let flashSaleContents = response.data
                    if(flashSaleContents){
                        flashSaleContents.map((item, index)=>{
                            if(item.id){
                                item.checked = true
                            }

                        })
                    }
                    this.setState({
                        flashSaleContents:flashSaleContents
                    })
                }
            }, progressing, true)
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            flashSale:{},
            flashSaleContents:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    overrideAllCheckbox = (checked) =>{
        let flashSale = null;
        if(checked){
            flashSale = this.state.flashSale
        }else{
            this.setState({
                allPercentageDiscount:0.0,
                allPriceDiscount:0.0,
                allFinalPrice:0.0,
                allStock:0
            })
        }

        if(!isEmpty(this.state.flashSaleContents)){
            this.state.flashSaleContents.map((item, index)=>{
                item.checked = checked
                item.flashSale = flashSale
                if(!checked){
                    item.priceDiscount = 0
                    item.percentageDiscount = 0
                    item.finalPrice = 0
                    item.stock = 0
                }
            })
            this.setState({
                flashSaleContents:this.state.flashSaleContents,
                checkedAll:checked,
            })
        }
    }

    overrideAllStock = (value) =>{
        if(!isEmpty(this.state.flashSaleContents)){
            this.state.flashSaleContents.map((item, index)=>{
                if(item.checked){
                    var value2 = value
                    if(value2>item.inventory.quantity){
                        value2 = item.inventory.quantity
                    }
                    item.stock = value2
                }else{
                    item.stock = 0
                }
            })
            this.setState({
                allStock:value,
                flashSaleContents:this.state.flashSaleContents,
            })
        }
    }

    overrideAllPercentageDiscount = (value) =>{
        if(!isEmpty(this.state.flashSaleContents)){
            this.state.flashSaleContents.map((item, index)=>{
                if(item.checked){
                    item.percentageDiscount = value
                    item.priceDiscount = percentToPrice(value, item.inventory.price)
                    item.finalPrice = finalPriceByPercent(value, item.inventory.price)
                }else{
                    item.percentageDiscount = 0
                    item.priceDiscount = 0
                    item.finalPrice = 0
                }
            })
            this.setState({
                flashSaleContents:this.state.flashSaleContents,
            })
        }
    }

    overrideAllPriceDiscount = (value) =>{
        if(!isEmpty(this.state.flashSaleContents)){
            this.state.flashSaleContents.map((item, index)=>{
                var value2 = value
                if(value2>item.inventory.price){
                    value2 = item.inventory.price
                }
                if(item.checked){
                    item.priceDiscount = value2
                    item.percentageDiscount = percentByDiscount(value2, item.inventory.price)
                    item.finalPrice = finalPriceByPercent(item.percentageDiscount, item.inventory.price)
                }else{
                    item.priceDiscount = 0
                    item.percentageDiscount = 0
                    item.finalPrice = 0
                }
            })
            this.setState({
                flashSaleContents:this.state.flashSaleContents,
            })
        }
    }

    overrideAllFinalPrice = (value) =>{
        if(!isEmpty(this.state.flashSaleContents)){
            this.state.flashSaleContents.map((item, index)=>{
                var value2 = value
                if(value2>item.inventory.price){
                    value2 = item.inventory.price
                }
                if(item.checked){
                    item.finalPrice = value2
                    item.priceDiscount = item.finalPrice - item.inventory.price
                    item.percentageDiscount = percentByDiscount(item.priceDiscount, item.inventory.price)
                }else{
                    item.finalPrice = 0
                    item.priceDiscount = 0
                    item.percentageDiscount = 0
                }
            })
            this.setState({
                flashSaleContents:this.state.flashSaleContents,
            })
        }
    }

    saveConfirm = () =>{
        if(!isEmpty(this.state.flashSaleContents)){
            var confirm = true ;
            this.state.flashSaleContents.map((item, index)=>{
                item.priceDiscountWarning = null
                item.stockWarning = null
                if(!isEmpty(item.flashSale)){
                    if(isEmpty(item.priceDiscount) || item.priceDiscount===0){
                        item.priceDiscountWarning = "Fill the price"
                        if(confirm){
                            confirm = false
                        }
                    }
                    if(isEmpty(item.stock) || item.stock===0){
                        item.stockWarning = "Fill the stock"
                        if(confirm){
                            confirm = false
                        }
                    }
                }
            })
            this.setState({
                flashSaleContents:this.state.flashSaleContents,
                modalSaveConfirm : confirm
            })
        }else{
            this.showDialog("Something error, please reload")
        }
    }

    save = () => {
        this.setState({
            modalSaveConfirm:false,
        }, () => {
            this.post(Global.API.FLASHSALE_CONTENT_SAVE+"/"+this.state.flashSale.id, this.state.flashSaleContents, null, response=>{
                if(response.code===200){
                    this.close()
                }else{
                    this.showDialog(response.message)
                }
            }, true, true)
        })
    }

    closeDialog = () =>{
        this.setState({
            modalSaveConfirm:false,
        })
    }



    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                   isOpen={this.state.modal}
                   backdrop={true} style={{zIndex:"1"}}>
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalSaveConfirm}
                    title="Saving Confirmation"
                    message="Do you want to update this inventory as flashsale item to your list ?"
                    okCallback={this.save}
                    cancelCallback={this.closeDialog}/>
                <ModalHeader>
                    Inventory
                </ModalHeader>
                <ModalBody>
                    <Table hover>
                        <thead>
                        <tr>
                            <th width="4%" >#</th>
                            <th width="4.5%" >Type</th>
                            <th width="4%">Image</th>
                            <th width="8.5%" >Ori Price</th>
                            <th width="4.5%" >Qty</th>
                            <th width="9.5%" >Stock</th>
                            <th width="4.5%" >Usage</th>
                            <th width="9.5%" >%</th>
                            <th width="15%" >- Price</th>
                            <th width="15%" >Final</th>
                            <th width="4%" >Check</th>
                        </tr>
                        <tr>
                            <th width="4%"></th>
                            <th width="4.5%" ></th>
                            <th width="4%"></th>
                            <th width="8.5%" ></th>
                            <th width="4.5%"></th>
                            <th width="9.5%" >
                                <NumberInput
                                    name="allStock"
                                    className="form-control"
                                    value={this.state.allStock?this.state.allStock:''}
                                    onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                        e.preventDefault()
                                        let value = e.target.value
                                        value = value.replace(/\D/,'')
                                        this.overrideAllStock(value?value:0)
                                    }}
                                    precision={0}
                                    disabled={!this.state.checkedAll}
                                    maxLength="10"
                                    placeholder="0">
                                </NumberInput>
                            </th>
                            <th width="4.5%" ></th>
                            <th width="9.5%" >
                                <PercentInput
                                    name="percentDiscount"
                                    className="form-control"
                                    value={this.state.allPercentageDiscount}
                                    disabled={!this.state.checkedAll}
                                    onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                        e.preventDefault()
                                        if(floatvalue>100.0){
                                            floatvalue = floatvalue%100
                                        }
                                        this.setState({
                                            allPercentageDiscount:floatvalue
                                        }, () => {
                                            this.overrideAllPercentageDiscount(floatvalue?floatvalue:0)
                                        })
                                    }}
                                    placeholder="0">
                                </PercentInput>
                            </th>
                            <th width="15%" >
                                <PriceInput
                                    name="priceDiscount"
                                    className="form-control"
                                    prefix=""
                                    value={this.state.allPriceDiscount}
                                    disabled={!this.state.checkedAll}
                                    onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                        e.preventDefault()
                                        this.setState({
                                            allPriceDiscount:floatvalue
                                        }, () => {
                                            this.overrideAllPriceDiscount(floatvalue?floatvalue:0)
                                        })
                                    }}
                                    placeholder="0">
                                </PriceInput>
                            </th>
                            <th width="15%" >
                                <PriceInput
                                    name="finalPrice"
                                    className="form-control"
                                    value={this.state.allFinalPrice}
                                    disabled={!this.state.checkedAll}
                                    onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                        e.preventDefault()
                                        this.setState({
                                            allFinalPrice:floatvalue
                                        }, () => {
                                            this.overrideAllFinalPrice(floatvalue?floatvalue:0)
                                        })
                                    }}
                                    placeholder="0">
                                </PriceInput>
                            </th>
                            <th width="4%" ><Checkbox checked={this.state.checkedAll} onChange={checked =>this.overrideAllCheckbox(checked)}/></th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(this.state.flashSaleContents)?
                                this.state.flashSaleContents.map((item, index)=>(
                                    <tr key={index} valign="center">
                                        <th width="4%" scope="row">{++index}</th>
                                        <td width="4.5%" >{item.inventory.typeName}</td>
                                        <td>
                                            <Button onClick={event=>{
                                                event.preventDefault()
                                                this.openLightBox(item.inventory.inventoryImages)
                                            }}><IoMdEye/></Button>

                                        </td>
                                        <td width="15%" >{currencyFormat(item.inventory.price, CURRENCY_SYMBOL)}</td>
                                        <td width="4.5%" >{item.inventory.quantity}</td>
                                        <td width="9.5%" >
                                            <NumberInput
                                                name="stock"
                                                className="form-control"
                                                value={item.stock?item.stock:''}
                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                    e.preventDefault()
                                                    let value = e.target.value
                                                    value = value.replace(/\D/,'')
                                                    if(value>item.inventory.quantity){
                                                        value = item.inventory.quantity
                                                    }
                                                    item.stock = value
                                                    this.setState({
                                                        flashSaleContents:this.state.flashSaleContents
                                                    })
                                                }}
                                                disabled={!item.checked}
                                                maxLength="10"
                                                placeholder="0">
                                            </NumberInput>
                                            <FormText className={"font-italic"}><span style={{color:'red'}}>{item.stockWarning}</span></FormText>
                                        </td>
                                        <td width="4.5%" >{item.currentUsage}</td>
                                        <td width="9.5%" >
                                            <PercentInput
                                                name="percentDiscount"
                                                className="form-control"
                                                value={(item.percentageDiscount!=null)?item.percentageDiscount:0}
                                                ref="percentDiscount"
                                                disabled={!item.checked}
                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                    if(floatvalue>100.0){
                                                        floatvalue = floatvalue%100
                                                    }
                                                    item.percentageDiscount = floatvalue
                                                    item.priceDiscount = percentToPrice(floatvalue, item.inventory.price)
                                                    item.finalPrice = finalPriceByPercent(floatvalue, item.inventory.price)
                                                    this.setState({
                                                        flashSaleContents:this.state.flashSaleContents
                                                    })
                                                }}
                                                placeholder="0">
                                            </PercentInput>
                                        </td>
                                        <td width="8.5%" >
                                            <PriceInput
                                                name="priceDiscount"
                                                className="form-control"
                                                value={(item.priceDiscount!=null)?item.priceDiscount:0}
                                                disabled={!item.checked}
                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                    if(floatvalue>item.inventory.price){
                                                        floatvalue = item.inventory.price
                                                    }
                                                    item.priceDiscount = floatvalue
                                                    item.percentageDiscount = percentByDiscount(floatvalue, item.inventory.price)
                                                    item.finalPrice = finalPriceByPercent(item.percentageDiscount, item.inventory.price)
                                                    this.setState({
                                                        flashSaleContents:this.state.flashSaleContents
                                                    })
                                                }}
                                                placeholder="0">
                                            </PriceInput>
                                            <FormText className={"font-italic"}><span style={{color:'red'}}>{item.priceDiscountWarning}</span></FormText>
                                        </td>
                                        <td width="15%" >
                                            <PriceInput
                                                name="finalPrice"
                                                className="form-control"
                                                value={(item.finalPrice!=null)?item.finalPrice:0}
                                                disabled={!item.checked}
                                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                    if(floatvalue>item.inventory.price){
                                                        floatvalue = item.inventory.price
                                                    }
                                                    item.finalPrice = floatvalue
                                                    item.priceDiscount = item.finalPrice - item.inventory.price
                                                    item.percentageDiscount = percentByDiscount(item.priceDiscount, item.inventory.price)
                                                    this.setState({
                                                        flashSaleContents:this.state.flashSaleContents
                                                    })
                                                }}
                                                placeholder="0">
                                            </PriceInput>
                                        </td>
                                        <td width="4%" ><Checkbox checked={item.checked} onChange={
                                            checked => {
                                                item.checked = checked
                                                if(checked){
                                                    item.flashSale = this.state.flashSale
                                                }else{
                                                    item.flashSale = null
                                                    item.stock = 0
                                                    item.finalPrice = 0
                                                    item.priceDiscount = 0
                                                    item.percentageDiscount = 0
                                                }
                                                this.setState({
                                                    flashSaleContents:this.state.flashSaleContents
                                                })
                                            }
                                        }/></td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Cancel
                    </Button>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        this.saveConfirm()
                    }}>
                        Update
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
