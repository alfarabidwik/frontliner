import React from 'react';
import {Button, CardImg, Col, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {
    allIsEmpty, currencyFormat,
    finalPriceByPercent,
    isEmpty,
    parseDate,
    percentByDiscount,
    percentToPrice
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {CURRENCY_SYMBOL} from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import Checkbox from "../../components/Checkbox"
import FormText from "reactstrap/es/FormText";
import ConfirmDialog from "./ConfirmDialog";
import moment from "moment";
import PercentInput from "../Widget/PercentInput";
import DateInput from "../../components/Widget/DateInput";

/*= (message, okCallback, cancelCallback) =>*/
export default class DiscountContentModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.backdrop = true
        state.modal = this.props.showing
        state.discount = this.props.discount
        state.product = this.props.product
        state.close = this.props.close
        state.discounts = []
        state.checkedAll = false
        state.allStartDate = null
        state.allEnddDate = null
        state.modalSaveConfirm = false
        state.sortir = "created"
        state.ascending = true
        this.setState(state)
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                discount:props.discount,
                close:props.close,
                product:props.product,
                modalSaveConfirm:false
            }, () => {
                if(props.showing && props.showing!==modal) {
                    this.discountContentByProduct()
                }
            })
        }
    }

    discountContentByProduct = () =>{
        if(!allIsEmpty(this.state.product.id)){
            this.get(Global.API.DISCOUNT_INVENTORY, {
                params:{
                    productId:this.state.product.id,
                    sortir:this.state.sortir,
                    ascending:this.state.ascending
                }
            }, null, response=>{
                if(response.code===200){
                    let discounts = response.data
                    if(discounts){
                        discounts.map((item, index)=>{
                            if(item.id){
                                item.checked = true
                            }

                        })
                    }
                    discounts.map((item, index)=>{
                        if(item.id===null){
                            item.onDelete = true
                        }
                    })
                    this.setState({
                        discounts:discounts
                    })
                }
            }, true, true)
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            discount:{},
            discounts:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    overrideAllCheckbox = (checked) =>{
        let discount = null;
        if(checked){
            discount = this.state.discount
        }else{
            this.setState({
                allPercent:0.0,
                allStartDate:0.0,
                allFinalPrice:0.0,
                allStock:0
            })
        }

        if(!isEmpty(this.state.discounts)){
            this.state.discounts.map((item, index)=>{
                item.checked = checked
                item.discount = discount
                if(!checked){
                    item.percent = 0
                    item.onDelete = true
                }else{
                    item.onDelete = false
                }
            })
            this.setState({
                discounts:this.state.discounts,
                checkedAll:checked,
            })
        }
    }

    overrideAllPercent = (value) =>{
        if(!isEmpty(this.state.discounts)){
            this.state.discounts.map((item, index)=>{
                if(item.checked){
                    item.percent = value
                }else{
                    item.percent = 0
                }
            })
            this.setState({
                discounts:this.state.discounts,
            })
        }
    }

    overrideAllStartDate = (value) =>{
        if(!isEmpty(this.state.discounts)){
            this.state.discounts.map((item, index)=>{
                var value2 = value
                if(item.checked){
                    item.startDate = value2
                }
            })
            this.setState({
                discounts:this.state.discounts,
            })
        }
    }

    overrideAllEndDate = (value) =>{
        if(!isEmpty(this.state.discounts)){
            this.state.discounts.map((item, index)=>{
                var value2 = value
                if(item.checked){
                    item.endDate = value2
                }
            })
            this.setState({
                discounts:this.state.discounts,
            })
        }
    }

    saveConfirm = () =>{
        if(!isEmpty(this.state.discounts)){
            var confirm = true ;
            this.state.discounts.map((item, index)=>{
                item.percentWarning = null
                item.startDateWarning = null
                item.endDateWarning = null
                if(!isEmpty(item)){
                    if(!item.onDelete && (isEmpty(item.percent) || item.percent === 0)){
                        item.percentWarning = "Fill the percent for it discount"
                        if(confirm){
                            confirm = false
                        }
                    }
                    if(!item.onDelete && !isEmpty(item.percent) && item.percent != 0 && isEmpty(item.startDate)){
                        item.startDateWarning = "Fill the startDate"
                        if(confirm){
                            confirm = false
                        }
                    }
                    if(!item.onDelete && !isEmpty(item.percent) && item.percent != 0 && isEmpty(item.endDate)){
                        item.endDateWarning = "Fill the endDate"
                        if(confirm){
                            confirm = false
                        }
                    }
                }
            })
            this.setState({
                discounts:this.state.discounts,
                modalSaveConfirm : confirm
            })
        }else{
            this.showDialog("Something error, please reload")
        }
    }

    save = () => {
        this.post(Global.API.DISCOUNT_SAVE, this.state.discounts, null, response=>{
            if(response.code===200){
                this.setState({
                    modalSaveConfirm:false,
                }, () => {
                    this.close()
                })
            }else{
                this.showDialog(response.message)
            }
        }, true, true)
    }

    closeDialog = () =>{
        this.setState({
            modalSaveConfirm:false,
        })
    }

    render(){
        return (
            /*xl-lg-sm*/
            <Modal
                size="xl" centered={true} isOpen={this.state.modal} backdrop={true} zIndex="1">
                {super.render()}
                <ConfirmDialog
                    showing={this.state.modalSaveConfirm}
                    title="Saving Confirmation"
                    message="Do you want to update this inventory as discount item to your list ?"
                    okCallback={this.save}
                    cancelCallback={this.closeDialog}/>
                <ModalHeader>
                    Inventory
                </ModalHeader>
                <Table hover>
                    <thead>
                    <tr>
                        <th width="4%" >#</th>
                        <th width="3%" >Type</th>
                        <th width="3%">Img</th>
                        <th width="15%" >Ori Price</th>
                        <th width="20%" >%</th>
                        <th width="20%" >Final</th>
                        <th width="4%" >Start Date</th>
                        <th width="4%" >End Date</th>
                        <th width="4%" >Check</th>
                    </tr>
                    <tr>
                        <th width="4%"></th>
                        <th width="12.5%" ></th>
                        <th width="3%"></th>
                        <th width="15%" ></th>
                        <th width="20%" >
                            <PercentInput
                                name="percentDiscount"
                                className="form-control"
                                value={this.state.allPercent}
                                disabled={!this.state.checkedAll}
                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                    e.preventDefault()
                                    if(floatvalue>100.0){
                                        floatvalue = floatvalue%100
                                    }
                                    this.setState({
                                        allPercent:floatvalue
                                    }, () => {
                                        this.overrideAllPercent(floatvalue?floatvalue:0)
                                    })
                                }}
                                placeholder="0">
                            </PercentInput>
                        </th>
                        <th width="20%" >
                        </th>
                        <th>
                            <DateInput
                                id="startDate"
                                className="col-sm-11"
                                maxdate={this.state.allEndDate}
                                value={this.state.allStartDate?moment(this.state.allStartDate).format("YYYY-MM-DD"):null}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        allStartDate : value?value:null
                                    }, () => {
                                        this.overrideAllStartDate(value)
                                    })
                                }}
                                disabled={!this.state.checkedAll}
                                placeholder="Enter start date">
                            </DateInput>
                        </th>
                        <th>
                            <DateInput
                                id="endDate"
                                className="col-sm-11"
                                mindate={this.state.allStartDate}
                                value={this.state.allEndDate}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        allEndDate : value?value:null
                                    }, () => {
                                        this.overrideAllEndDate(value)
                                    })
                                }}
                                disabled={!this.state.checkedAll}
                                placeholder="Enter end date">
                            </DateInput>
                        </th>
                        <th width="4%" ><Checkbox checked={this.state.checkedAll} onChange={checked =>this.overrideAllCheckbox(checked)}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !isEmpty(this.state.discounts)?
                            this.state.discounts.map((item, index)=>(
                                <tr valign="center" key={index}>
                                    <th width="4%" scope="row">{++index}</th>
                                    <td width="3%" >{item.inventory.typeName}</td>
                                    <td>
                                        <Button onClick={event=>{
                                            event.preventDefault()
                                            this.openLightBox(item.inventory.inventoryImages)
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td width="15%" >{currencyFormat(item.inventory.price, CURRENCY_SYMBOL)}</td>
                                    <td width="20%" >
                                        <PercentInput
                                            name="percent"
                                            className="form-control"
                                            value={(item.percent!=null)?item.percent:0}
                                            disabled={!item.checked}
                                            onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                if(floatvalue>100.0){
                                                    floatvalue = floatvalue%100
                                                }
                                                item.percent = floatvalue
                                                this.setState({
                                                    discounts:this.state.discounts
                                                })
                                            }}
                                            placeholder="0">
                                        </PercentInput>
                                        <FormText className={"font-italic"}><span style={{color:'red'}}>{item.percentWarning}</span></FormText>
                                    </td>
                                    <td width="20%" >
                                        {
                                            (isEmpty(item.percent)?
                                                currencyFormat(item.inventory.price, CURRENCY_SYMBOL)
                                                :
                                                currencyFormat(finalPriceByPercent(item.percent, item.inventory.price), CURRENCY_SYMBOL))
                                        }
                                    </td>
                                    <td>
                                        <DateInput
                                            id="startDate"
                                            className="col-sm-11"
                                            mindate={new Date()}
                                            maxdate={item.endDate}
                                            value={item.startDate?moment(item.startDate).format("YYYY-MM-DD"):null}
                                            disabled={!item.checked}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                item.startDate = value
                                                this.setState({
                                                    discounts:this.state.discounts
                                                })
                                            }}
                                            placeholder="Enter start date">
                                        </DateInput>
                                        <FormText className={"font-italic"}><span style={{color:'red'}}>{item.startDateWarning}</span></FormText>
                                    </td>
                                    <td>
                                        <DateInput
                                            id="endDate"
                                            className="col-sm-11"
                                            mindate={item.startDate}
                                            value={item.endDate?moment(item.endDate).format("YYYY-MM-DD"):null}
                                            disabled={!item.checked}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                item.endDate = value
                                                this.setState({
                                                    discounts:this.state.discounts
                                                })
                                            }}
                                            placeholder="Enter end date">
                                        </DateInput>
                                        <FormText className={"font-italic"}><span style={{color:'red'}}>{item.endDateWarning}</span></FormText>
                                    </td>
                                    <td width="4%" >
                                        <Checkbox checked={item.checked} onChange={
                                        checked => {
                                            item.checked = checked
                                            if(checked){
                                                item.discount = this.state.discount
                                                item.onDelete = false ;
                                            }else{
                                                item.discount = null
                                                item.percent = 0
                                                item.onDelete = true
                                            }
                                            this.setState({
                                                discounts:this.state.discounts
                                            })
                                        }
                                    }/></td>
                                </tr>
                            ))
                            :
                            null
                    }
                    </tbody>
                </Table>
                
                <ModalFooter>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Cancel
                    </Button>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        this.saveConfirm()
                    }}>
                        Update
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
