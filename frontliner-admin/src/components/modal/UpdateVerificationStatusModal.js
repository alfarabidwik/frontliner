import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import ItemOption from "../Widget/ItemOption";
import Global, {_DELIVERY, ACTION_VERIFICATION_STATUSES, VERIFICATION_STATUSES} from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {statusColor} from "../../utils/Utilities";
/*= (message, okCallback, cancelCallback) =>*/
export default class UpdateVerificationStatusModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.modal= this.props.modal
        state.backdrop= true
        state.user = this.props.user
        state.verification = null
        state.verificationStatus = {}
        state.verificationStatuses = ACTION_VERIFICATION_STATUSES
        state.newVerificationStatus = {}
        state.okCallback = this.props.okCallback
        state.closeCallback = this.props.closeCallback
        state.note = ""
        state.resiCodeMessage=""
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.state.modal!=props.modal && props.modal){
            let user = props.user
            let verificationStatus = null
            ACTION_VERIFICATION_STATUSES.forEach(value => {
                if(value.name===user.verificationStatus){
                    verificationStatus = value
                }
            })
            this.setState({
                modal:props.modal,
                user:user,
                okCallback : props.okCallback,
                closeCallback : props.closeCallback,
                verificationStatus:verificationStatus,
                newVerificationStatus:{},
                note:"",
                resiCodeMessage : "",
            })
        }
    }


    updateVerificationStatus=()=>{
        let verification = this.state.verification?this.state.verification:{}
        let verificationStatus = this.state.verificationStatus?this.state.verificationStatus:{}
        let newVerificationStatus = this.state.newVerificationStatus?this.state.newVerificationStatus:{}
        let note = this.state.note?this.state.note:""
        let user = this.state.user
        let confirmationMessage = "Do you want to update this verification status from = "+verificationStatus.name+" to = "+newVerificationStatus.name+" ? "
        this.openConfirmDialog("Confirmation", confirmationMessage, ()=>{
            let formData = new FormData();
            formData.append("userId", user.id)
            formData.append("verificationStatus", newVerificationStatus.name)
            formData.append("note", note)
            this.post(Global.API.USER_UPDATE_VERIFICATION, formData, null, response=>{
                if(response.code===200){
                    let verification = response.data
                    this.successToast(response.message)
                    this.setState({
                        modal:false
                    }, () => {
                        if(this.state.okCallback){
                            this.state.okCallback(verification)
                        }
                    })
                }
            }, true, true)
        })
    }

    render(){
        let verification = this.state.verification?this.state.verification:{}
        let verificationStatus = this.state.verificationStatus?this.state.verificationStatus:{}
        let verificationStatuses = this.state.verificationStatuses?this.state.verificationStatuses:[]
        let newVerificationStatus = this.state.newVerificationStatus?this.state.newVerificationStatus:{}

        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Update Verification Status : {verification&&verification.refCode}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="currentStatus">Current Status</Label>
                                <Input
                                    style={{fontSize:'bold', color:statusColor(verificationStatus.id), borderColor: statusColor(verificationStatus.id)}}
                                    id="currentStatus"
                                    type="text"
                                    name="currentStatus"
                                    value={verificationStatus.name}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <ItemOption
                                    title={"Select Status"}
                                    objects={verificationStatuses}
                                    default={newVerificationStatus.id}
                                    callback={(newVerificationStatus)=>{
                                        if(newVerificationStatus!=null){
                                            this.setState({
                                                newVerificationStatus:newVerificationStatus
                                            })
                                        }else{
                                            this.setState({
                                                newVerificationStatus:{}
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                            <Label for="note">Note</Label>
                            <Input
                                id="note"
                                type="text"
                                name="note"
                                value={this.state.note}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        note : value?value:null
                                    })
                                }}
                                placeholder="Enter note (recommended)"
                            />
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Row>
                        {
                            newVerificationStatus.id&&(
                                <Col>
                                    <Button color="primary" onClick={e=>{
                                        this.updateVerificationStatus()
                                    }}>
                                        Update
                                    </Button>
                                </Col>
                            )
                        }
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                this.setState({
                                    modal:false
                                })
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }
                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

UpdateVerificationStatusModal.propTypes = {
    modal:PropTypes.bool,
    user:PropTypes.object,
    okCallback:PropTypes.func,
    closeCallback:PropTypes.func,
}
