import React from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
export default class Dialog extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            modal: this.props.showing,
            modal_backdrop: false,
            modal_nested_parent: false,
            modal_nested: false,
            backdrop: true,
            title : this.props.title,
            message : this.props.message,
        }
        this.toggle = this.toggle.bind(this)
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                message : props.message
            })
        }
    }

    toggle(modalType){
        if (!modalType) {
            this.setState({
                modal: !this.state.modal,
                [`modal_${modalType}`]: !this.state[`modal_${modalType}`],
            });
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        this.toggle('backdrop')
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    render(){
        return (
            <Modal
                isOpen={this.state.modal}
                toggle={this.toggle('backdrop')}
                backdrop={true}>
                <ModalHeader toggle={this.toggle('backdrop')}>
                    {this.state.title}
                </ModalHeader>
                <ModalBody>
                    {this.state.message}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={e=>(this.buttonClick(e, "Ok"))}>
                        Ok
                    </Button>{' '}
                </ModalFooter>
            </Modal>
        )
    }
}
