import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, CardBody,
    CardImg,
    Col,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table
} from "reactstrap";
import {
    allIsEmpty, currencyFormat,
    finalPriceByPercent,
    isEmpty,
    parseDate,
    percentByDiscount,
    percentToPrice
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY, DD_MM_YYYY_HH_MM_SS} from "../../utils/Global";
import Checkbox from "../../components/Checkbox"
import kitchen from "../../utils/AxiosInstance";
import ModalPagination from "../ModalPagination";


/*= (message, okCallback, cancelCallback) =>*/
export default class PointModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            edit:this.props.edit,
            inventory:this.props.inventory,
            benefitType:Global._ITEM_POINT,
            close:this.props.close,
            point:{},
            points:[],
            allStartDate:null,
            allEndDate:null,
            modalSaveConfirm:false,
            sortir:"created",
            page:0,
            search:"",
            status:"all",
            ascending:true,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                edit:props.edit,
                inventory:props.inventory,
                point:props.inventory?props.inventory.point:null,
                close:props.close,
                modalSaveConfirm:false
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.fetchPoints()
                }
            })
        }
    }

    fetchPoints = () =>{
        if(!isEmpty(this.state.inventory)){
            this.get(Global.API.POINTS, {
                params:{
                    status:this.state.status,
                    search:this.state.search,
                    benefitType:this.state.benefitType,
                    page:this.state.page-1,
                    sortir:this.state.sortir,
                    ascending:this.state.ascending
                }
            }, null, response=>{
                if(response.code===200){
                    let points = response.data
                    this.setState({
                        points:points,
                        totalPage: response.totalPage,
                        totalElement: response.totalElement,
                        pageElement: response.pageElement,
                    })
                }
            }, true, true)
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            inventory:{},
            points:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    registerInventory  = (point) =>{
        this.get(Global.API.POINT_REGISTER_INVENTORY, {
            params:{
                pointId:point.id,
                inventoryId:this.state.inventory.id,
            }
        }, null, response=>{
            if(response.code===200){
                // this.fetchInventoryDetail(this.state.inventory.id)
                this.close()
            }
        }, true, true)
    }
    unregisterInventory  = () =>{
        this.get(Global.API.POINT_UNREGISTER_INVENTORY, {
            params:{
                inventoryId:this.state.inventory.id,
            }
        }, null, response=>{
            if(response.code===200){
                // this.fetchInventoryDetail(this.state.inventory.id)
                this.close()
            }
        }, true, true)
    }

    fetchInventoryDetail = (id) => {
        if(id!=null){
            kitchen.get(Global.API.INVENTORY, {
                params :{
                    id : id
                }
            }).then(res =>{
                if(res.data.code===200){
                    this.setState({
                        inventory:res.data.data,
                    })
                }
            })
        }
    }


    closeDialog = () =>{
        this.setState({
            modalSaveConfirm:false,
        })
    }

    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                   isOpen={this.state.modal}
                   backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Point
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Table hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Period</th>
                                <th>Benefit</th>
                                <th>Point</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Select</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.points.map((item, index)=>(
                                    <tr key={index}>
                                        <th scope="row">{(this.state.pageElement*(this.state.totalPage-1))+(++index)}</th>
                                        <td>{item.name}</td>
                                        <td>
                                            {
                                                item.periodType===Global._ALL_TIME?Global.ALL_TIME:(null)

                                            }
                                            {
                                                item.periodType===Global._STATIC?(null):(null)

                                            }
                                            {
                                                item.periodType===Global._DYNAMIC?(
                                                    <span>
                                                        {parseDate(item.startPeriod, DD_MM_YYYY)}
                                                        &nbsp; - &nbsp;
                                                        {parseDate(item.endPeriod, DD_MM_YYYY)}
                                                </span>
                                                ):(null)

                                            }
                                        </td>
                                        <td>
                                            {
                                                item.benefitType===Global._ITEM_POINT?Global.ITEM_POINT:(null)

                                            }
                                            {
                                                item.benefitType===Global._PURCHASE_POINT?Global.PURCHASE_POINT+" || "+currencyFormat(item.purchaseNominal, CURRENCY_SYMBOL):(null)

                                            }
                                        </td>
                                        <td>{item.point}</td>
                                        <td>{parseDate(item.created, DD_MM_YYYY)}</td>
                                        <td>{parseDate(item.updated, DD_MM_YYYY_HH_MM_SS)}</td>
                                        <td>
                                            <Checkbox checked={this.state.point&&this.state.point.id===item.id} onChange={
                                                selected => {
                                                    if(selected){
                                                        this.setState({
                                                            point:item
                                                        })
                                                        // this.registerInventory(item)
                                                    }else{
                                                        this.setState({
                                                            point:null
                                                        })
                                                        // this.unregisterInventory()
                                                    }
                                                }
                                            }/>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                    </Row>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.fetchPoints()
                    })
                }}/>

                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        if(!isEmpty(this.state.point)){
                            if(isEmpty(this.state.inventory.point)){
                                this.registerInventory(this.state.point)
                            }else{
                                if(this.state.inventory.point.id!=this.state.point.id){
                                    this.registerInventory(this.state.point)
                                }else{
                                    this.close()
                                }
                            }
                        }else{
                            if(this.state.inventory.point){
                                this.unregisterInventory()
                            }
                        }
                    }}>
                        Update
                    </Button>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }

    paginationButton = () =>{
        let button = []
        for (let i = 0; i < this.state.totalPage; i++) {
            button.push(<Button key={i} onClick={e=>{
                e.preventDefault()
                this.setState({
                    page:--i
                }, () => {
                    this.refresh(this.state.ascending, this.state.sortir)
                })

            }}>{++i}</Button>)

        }
        return button
    }

}
