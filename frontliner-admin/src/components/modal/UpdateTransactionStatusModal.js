import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import ItemOption from "../Widget/ItemOption";
import Global, {_DELIVERY} from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {statusColor} from "../../utils/Utilities";
/*= (message, okCallback, cancelCallback) =>*/
export default class UpdateTransactionStatusModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.modal= this.props.modal
        state.backdrop= true
        state.transaction = this.props.transaction
        state.transactionStatus = {}
        state.transactionStatuses = []
        state.newTransactionStatus = {}
        state.okCallback = this.props.okCallback
        state.closeCallback = this.props.closeCallback
        state.note = ""
        state.resiCode = ""
        state.resiCodeMessage=""
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.state.modal!=props.modal){
            this.setState({
                modal:props.modal,
                transaction: props.transaction,
                okCallback : props.okCallback,
                closeCallback : props.closeCallback,
                transactionStatus:{},
                transactionStatuses:[],
                newTransactionStatus:{},
                note:"",
                resiCode : "",
                resiCodeMessage : "",
            }, () => {
                if(props.modal && modal!==props.modal){
                    this.fetchTransactionStatus()
                }
            })
        }
    }

    fetchTransactionStatus = () =>{
        let transaction = this.state.transaction?this.state.transaction:{}
        let transactionStatus = transaction.transactionStatus?transaction.transactionStatus:{}
        if(transactionStatus.id){
            this.get(Global.API.TRANSACTION_STATUSES, null, null, response=>{
                if(response.code===200){
                    let transactionStatuses = response.data
                    let transactionStatusOptions = []
                    transactionStatuses.map((item, index)=>{
                        if(item.id>transactionStatus.id){
                            transactionStatusOptions.push(item)
                        }
                    })
                    this.setState({
                        transactionStatuses : transactionStatusOptions
                    })
                }
            }, true, true)
        }
    }


    updateTransactionStatus=()=>{
        let transaction = this.state.transaction?this.state.transaction:{}
        let transactionStatus = transaction.transactionStatus?transaction.transactionStatus:{}
        let newTransactionStatus = this.state.newTransactionStatus?this.state.newTransactionStatus:{}
        let note = this.state.note?this.state.note:""
        let resiCode = this.state.resiCode?this.state.resiCode:""
        let resiCodeMessage = this.state.resiCodeMessage
        if(newTransactionStatus.id===_DELIVERY && (resiCode===null || resiCode==="")){
            this.setState({
                resiCodeMessage:"Resi Code cannot be empty"
            })
            return ;
        }else{
            let confirmationMessage = "";
            if(newTransactionStatus.id===_DELIVERY){
                confirmationMessage= "Do you want to update this transaction status from = "+transactionStatus.name+" to = "+newTransactionStatus.name+" with resi code is "+resiCode+" ? "
            }else{
                confirmationMessage= "Do you want to update this transaction status from = "+transactionStatus.name+" to = "+newTransactionStatus.name+" ? "
            }
            this.openConfirmDialog("Confirmation", confirmationMessage, ()=>{
                let formData = new FormData();
                formData.append("transactionId", transaction.id)
                formData.append("transactionStatusId", newTransactionStatus.id)
                formData.append("note", note)
                formData.append("resiCode", resiCode)
                this.post(Global.API.TRANSACTION_UPDATE_STATUS, formData, null, response=>{
                    if(response.code===200){
                        let transaction = response.data
                        this.successToast(response.message)
                        this.setState({
                            modal:false
                        }, () => {
                            if(this.state.okCallback){
                                this.state.okCallback(transaction)
                            }
                        })
                    }
                }, true, true)
            })
        }
    }

    render(){
        let transaction = this.state.transaction?this.state.transaction:{}
        let transactionStatus = transaction.transactionStatus?transaction.transactionStatus:{}
        let transactionStatuses = this.state.transactionStatuses?this.state.transactionStatuses:[]
        let newTransactionStatus = this.state.newTransactionStatus?this.state.newTransactionStatus:{}

        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Update Transaction Status : {transaction&&transaction.refCode}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="currentStatus">Current Status</Label>
                                <Input
                                    style={{fontSize:'bold', color:statusColor(transactionStatus.id), borderColor: statusColor(transactionStatus.id)}}
                                    id="currentStatus"
                                    type="text"
                                    name="currentStatus"
                                    value={transactionStatus.name}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <ItemOption
                                    title={"Select Status"}
                                    objects={transactionStatuses}
                                    default={newTransactionStatus.id}
                                    callback={(newTransactionStatus)=>{
                                        if(newTransactionStatus!=null){
                                            this.setState({
                                                newTransactionStatus:newTransactionStatus
                                            })
                                        }else{
                                            this.setState({
                                                newTransactionStatus:{}
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                            <Label for="note">Note</Label>
                            <Input
                                id="note"
                                type="text"
                                name="note"
                                value={this.state.note}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        note : value?value:null
                                    })
                                }}
                                placeholder="Enter note (recommended)"
                            />
                            </FormGroup>
                        </Col>
                    </Row>
                    {
                        (newTransactionStatus.id===_DELIVERY)&&(
                            <Row>
                                <Col>
                                    <FormGroup>
                                        <Label for="resiCode">Resi Code</Label>
                                        <Input
                                            id="resiCode"
                                            type="text"
                                            name="resiCode"
                                            value={this.state.resiCode}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                let resiCodeMessage = this.state.resiCodeMessage
                                                if(value!==null || value!==''){
                                                    resiCodeMessage = ""
                                                }
                                                this.setState({
                                                    resiCode : value?value:null,
                                                    resiCodeMessage : resiCodeMessage
                                                })
                                            }}
                                            placeholder="Enter resi code..."
                                        />
                                        <br/>
                                        <span style={{color:"#ff061a"}}>{this.state.resiCodeMessage}</span>
                                    </FormGroup>
                                </Col>
                            </Row>
                        )
                    }
                </ModalBody>
                <ModalFooter>
                    <Row>
                        {
                            newTransactionStatus.id&&(
                                <Col>
                                    <Button color="primary" onClick={e=>{
                                        this.updateTransactionStatus()
                                    }}>
                                        Update
                                    </Button>
                                </Col>
                            )
                        }
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                this.setState({
                                    modal:false
                                })
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }
                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

UpdateTransactionStatusModal.propTypes = {
    modal:PropTypes.bool,
    transaction:PropTypes.object,
    okCallback:PropTypes.func,
    closeCallback:PropTypes.func,
}
