import React from 'react';
import {Button, CardImg, Col, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {
    isEmpty,
    parseDate,
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import PropTypes from 'prop-types'
import {MdDelete} from "react-icons/md";


/*= (message, okCallback, cancelCallback) =>*/
export default class CustomerInventoryCartModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.customer = this.props.customer
        state.modal = this.props.showing
        state.customerInventoryCart = {}
        state.close = this.props.close
        state.customerInventoryCarts = []
        state.sortir = "created"
        state.ascending = true
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let showing = this.state.modal
        if(this.props!=props){
            this.setState({
                customer : props.customer,
                close:props.close,
                modal:props.showing,
            }, () => {
                if(props.showing && showing!==props.showing) {
                    this.customerInventoryCarts()
                }
            })
        }
    }

    customerInventoryCarts = () =>{
        if(!isEmpty(this.state.customer)){
            this.get(Global.API.CART_BY_CUSTOMER, {
                params:{
                    customerId:this.state.customer.id,
                }
            }, null, response=>{
                if(response.code===200){
                    let customerInventoryCarts = response.data
                    this.setState({
                        customerInventoryCarts:customerInventoryCarts
                    })
                }
            }, true, true)
        }
    }
    close = () =>{
        this.setState({
            customerInventoryCart:{},
            customerInventoryCarts:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    render(){
        let customerInventoryCarts = this.state.customerInventoryCarts
        if(!customerInventoryCarts){
            customerInventoryCarts = []
        }

        let customer = this.state.customer
        if(!customer){
            customer = {}
        }
        return (
            /*xl-lg-sm*/
            <Modal
                size="xl" centered={true} isOpen={this.state.modal} backdrop={true} zIndex="1">
                {super.render()}
                <ModalHeader>
                    {customer.firstname}'s Cart
                </ModalHeader>
                <Table hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Heading</th>
                        <th>Category</th>
                        <th>Img</th>
                        <th>Quantity</th>
                        <th>Created</th>
                        <th>Remove</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !isEmpty(customerInventoryCarts)?
                            customerInventoryCarts.map((item, index)=>(
                                <tr valign="center" key={index}>
                                    <th scope="row">{++index}</th>
                                    <td>{item.inventory.product.name}</td>
                                    <td>{item.inventory.typeName}</td>
                                    <td>{item.inventory.size?item.inventory.size.name:""}</td>
                                    <td>{item.inventory.product.heading?item.inventory.product.heading.name:"-"}</td>
                                    <td>{item.inventory.product.category?item.inventory.product.category.name:"-"}</td>
                                    <td>
                                        <Button onClick={event=>{
                                            event.preventDefault()
                                            this.openLightBox(item.inventory.inventoryImages)
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td>{item.quantity}</td>
                                    <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                    <td><Button color='danger' onClick={event=>{
                                        this.openConfirmDialog("Confirmation", "Do you want to remove this item from his cart ?", ()=>{
                                            this.get(Global.API.CART_REMOVE_SINGLE_ITEM, {
                                                params:{
                                                    customerInventoryCartId:item.id
                                                }
                                            }, null, response=>{
                                                if(response.code===200){
                                                    this.successToast(response.message)
                                                    this.customerInventoryCarts()
                                                }
                                            }, true, true)

                                        })
                                    }}><MdDelete/></Button></td>
                                </tr>
                            ))
                            :
                            null
                    }
                    </tbody>
                </Table>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

CustomerInventoryCartModal.propTypes = {
   customer : PropTypes.object.isRequired,
    showing:PropTypes.bool.isRequired,
    close:PropTypes.func
}
