import React from 'react';
import {Button, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ValidatorLabel from "../Widget/ValidatorLabel";
import PropTypes from 'prop-types'
import Global from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {toast} from "../BaseComponent";
import {saveAdminSession} from "../../utils/StorageUtil";

/*= (message, okCallback, cancelCallback) =>*/

export default class ChangePasswordDialog extends BaseComponent {

    constructor(props){
        super(props);
        let state = this.state
        state.modal = this.props.modal
        state.oldPassword = null
        state. newPassword = null
        state.confirmPassword = null
        state.oldPasswordError = null
        state.newPasswordError = null
        state.confirmPasswordError = null
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                modal:props.modal,
                oldPassword:null,
                newPassword:null,
                confirmPassword:null,
                oldPasswordError:null,
                newPasswordError:null,
                confirmPasswordError:null,
            })
        }
    }

    changePassword=()=>{
        let state = this.state
        state.oldPasswordError=null
        state.newPasswordError=null
        state.confirmPasswordError=null

        let oldPassword = state.oldPassword
        let newPassword = state.newPassword
        let confirmPassword = state.confirmPassword
        if(!oldPassword || oldPassword===''){
            state.oldPasswordError = "Password lama harus diisi"
            this.setState(state)
            return
        }
        if(!newPassword || newPassword===''){
            state.newPasswordError = "Password baru harus diisi"
            this.setState(state)
            return
        }
        if(!confirmPassword || confirmPassword===''){
            state.confirmPasswordError = "Konfirmasi password harus diisi"
            this.setState(state)
            return
        }
        if(newPassword.length<6){
            state.newPasswordError = "Password setidaknya minimal terdiri dari 6 karakter"
            this.setState(state)
            return
        }
        if(confirmPassword.length<6){
            state.confirmPasswordError = "Password setidaknya minimal terdiri dari 6 karakter"
            this.setState(state)
            return
        }

        if(oldPassword===newPassword){
            state.newPasswordError = "Password lama dan password baru tidak boleh sama"
            this.setState(state)
            return
        }
        if(confirmPassword!=newPassword){
            state.confirmPasswordError = "Konfirmasi password dan password baru tidak sesuai"
            this.setState(state)
            return
        }

        let formData = new FormData()
        this.openConfirmDialog("Confirmation", "Do you want to change your password", ()=>{
            formData.append("oldPassword", oldPassword);
            formData.append("newPassword", newPassword);
            this.post(Global.API.ADMIN_CHANGE_PASSWORD, formData, null, response=>{
                if(response.code===200){
                    saveAdminSession(response.data, true)
                    toast.success(response.message)
                    this.setState({
                        modal:false
                    }, () => {
                        if(this.props.okCallback!=null){
                            this.props.okCallback()
                        }
                    })
                }
            }, true, true)
        }, ()=>{

        })
    }


    render(){
        return (
            <Modal
                isOpen={this.state.modal}
                centered={true}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Change Password
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="oldPassword">Old Password</Label>
                                <Input
                                    type="password"
                                    name="oldPassword"
                                    value={(this.state.oldPassword!=null && this.state.oldPassword)?this.state.oldPassword:""}
                                    onChange={(e) =>{
                                        this.setState({
                                            oldPassword:e.target.value
                                        })
                                    }}
                                    placeholder="Enter old password"
                                />
                                <ValidatorLabel message={this.state.oldPasswordError}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="newPassword">New Password</Label>
                                <Input
                                    type="password"
                                    name="newPassword"
                                    value={(this.state.newPassword!=null && this.state.newPassword)?this.state.newPassword:""}
                                    onChange={(e) =>{
                                        this.setState({
                                            newPassword:e.target.value
                                        })
                                    }}
                                    minLength={6}
                                    placeholder="Enter new password"
                                />
                                <ValidatorLabel message={this.state.newPasswordError}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="confirmPassword">Confirm Password</Label>
                                <Input
                                    type="password"
                                    name="confirmPassword"
                                    value={(this.state.confirmPassword!=null && this.state.confirmPassword)?this.state.confirmPassword:""}
                                    onChange={(e) =>{
                                        this.setState({
                                            confirmPassword:e.target.value
                                        })
                                    }}
                                    minLength={6}
                                    placeholder="Enter confirm password"
                                />
                                <ValidatorLabel message={this.state.confirmPasswordError}/>
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        this.changePassword()
                    }}>
                        Ok
                    </Button>{' '}
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.setState({
                            modal:false
                        }, () => {
                            if(this.props.cancelCallback!=null){
                                this.props.cancelCallback()
                            }
                        })
                    }}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

ChangePasswordDialog.propTypes = {
    modal: PropTypes.bool.isRequired,
    okCallback: PropTypes.func,
}
