import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, deleteParam, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {MdCheck, MdDelete, MdEdit} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import ModalPagination from "../ModalPagination";


/*= (message, okCallback, cancelCallback) =>*/
export default class ProductModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            point: this.props.point,
            products:[],
            ascending:true,
            sortir:'p.created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props && props.showing){
            this.setState({
                modal:props.showing,
                close:props.close,
                point: props.point,
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.productMergeProducts(true)
                }
            })
        }else{
            this.setState({
                modal:props.showing,
            })
        }
    }

    productMergeProducts = (progressing) =>{
        this.get(Global.API.POINT_UNREGISTERED_PRODUCT, {
            params:{
                pointId:this.state.point.id,
                search:this.state.search,
                page:this.state.page-1,
                sortir:this.state.sortir,
                ascending:this.state.ascending,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    products:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }

    refreshProduct = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.productMergeProducts(progressing)
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            products:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    updateProduct = (item, responseCallback) =>{
        this.get(Global.API.POINT_ITEM_UPDATE, {
            params:{
                productId:item.id,
                pointId:this.state.point.id,
                minimumQuantity:item.minimumQuantity,
                allInventory:item.allInventory,
            }
        }, null, response=>{
            responseCallback(response)
        })
    }

    updateMinimumQuantity = (item, responseCallback) =>{
        this.get(Global.API.POINT_ITEM_UPDATE_MIN_QUANTITY, {
            params:{
                productId:item.id,
                minimumQuantity:item.minimumQuantity,
            }
        }, null, response=>{
            responseCallback = response
        })
    }

    delete = (item) =>{
        this.get(Global.API.POINT_ITEM_DELETE, {
            params:{
                id:item.id,
            }
        }, null, response=>{
            item.id = null;
            item.productInventories = []
            item.minimumQuantity = 1
        })
    }



    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Name : {this.state.point.name} / {this.state.point.point} Point
                </ModalHeader>
                <ModalHeader>
                    Products
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.name", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.category.name", true))}>Category Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Search product..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                        }
                                    }}
                                />

                                {/*<SearchInput*/}
                                    {/*value={this.state.search}*/}
                                    {/*onChange={e=>{*/}
                                    {/*this.setState({*/}
                                        {/*search:e.target.value*/}
                                    {/*}, () => {*/}
                                        {/*this.refreshProduct(this.state.ascending, this.state.sortir, false)*/}
                                    {/*})*/}
                                {/*}}/>*/}
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Heading</th>
                            <th>Category</th>
                            <th>Belong To Current</th>
                            <th>Belong To Another</th>
                            <th>No Point</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(this.state.products)?
                                this.state.products.map((item, index)=>(
                                    <tr>
                                        <td scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</td>
                                        <td>{item.name}</td>
                                        <td>{item.heading?item.heading.name:"-"}</td>
                                        <td>{item.category?item.category.name:"-"}</td>
                                        <td>
                                            <Button onClick={e=>{
                                                e.preventDefault()
                                                if(this.props.openProductInventoryModal){
                                                    this.props.close()
                                                    this.props.openProductInventoryModal(item)
                                                }
                                            }}><IoMdOpen/>&nbsp;
                                                {item.currentPointCount}
                                            </Button>
                                        </td>
                                        <td>{item.anotherPointCount}</td>
                                        <td>{item.hasNoPointCount}</td>
                                        <td>{parseDate(item.created)}</td>
                                        <td>{parseDate(item.updated)}</td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.productMergeProducts(true)
                    })
                }}/>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close Dialog
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }

}
