import * as React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import BaseComponent from "../BaseComponent";
import * as PropTypes from "prop-types";
import {Form} from "../../model/formbuilder/Form.js";
import ItemOption from "../Widget/ItemOption";
import {Option} from "../../model/formbuilder/Option";
import {FormType} from "../../model/formbuilder/FormType";
import Global from "../../utils/Global";
import {getData} from "../../utils/StorageUtil";
import Card from "reactstrap/es/Card";
import CardBody from "reactstrap/es/CardBody";
import CardHeader from "reactstrap/es/CardHeader";
import {QueryParam} from "../../model/formbuilder/QueryParam";
import WarningLabel from "../Widget/WarningLabel";
import {MdDelete} from "react-icons/md";


/*= (message, okCallback, cancelCallback) =>*/
export default class FormModal extends BaseComponent{

    props  = {}

    validator = {
        tag: "",
        title: "'",
        placeHolder:"",
        type: "",
        fetchApi : "",
        queryParams:"",
        imageDirectory:""
    }

    typeOptions  = [
        {id:1, name:FormType.TEXT},
        {id:2, name:FormType.EMAIL},
        {id:3, name:FormType.NUMBER},
        {id:4, name:FormType.PHONE},
        {id:5, name:FormType.CURRENCY},
        {id:6, name:FormType.DATE},
        {id:7, name:FormType.IMAGE},
        // {id:8, name:FormType.DROPDOWN},
        {id:9, name:FormType.DROPDOWN_API_DATA}
    ]

    mandatoryOptions = [
        {id:1, name:false},
        {id:2, name:true}
    ]


    constructor(props){
        super(props);
        let form = new Form()
        form.id = this.props.id
        let lanes = this.props.lanes
        let lane = {}
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            onAdd:this.props.onAdd,
            close:this.props.close,
            form : form,
            lanes : lanes,
            lane:(!this.props.lane?(lanes && lanes.length>0?lanes[0]:null):lane),
            typeOption: this.typeOptions[0],
            mandatoryOption:this.mandatoryOptions[0],
            validator:this.validator
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props.showing!=props.showing){
            let form = new Form();
            form.id = props.id
            let lanes = props.lanes
            let lane = props.lane
            let typeOption = this.typeOptions.find(value => {
                if(value.name===form.type){
                    return value ;
                }
            })
            let mandatoryOption = this.mandatoryOptions.find(value => {
                if(value.name===form.mandatory){
                    return value ;
                }
            })

            this.setState({
                id:props.id,
                modal:props.showing,
                form : form,
                lanes : lanes,
                lane:(!props.lane?(lanes && lanes.length>0?lanes[0]:null):lane),
                typeOption:typeOption?typeOption:this.typeOptions[0],
                mandatoryOption:mandatoryOption?mandatoryOption:this.mandatoryOptions[0],
                validator:this.validator,
                close:props.close
            })
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    renderFormApi(){

        let configuration = getData(Global.CONFIGURATION)
        if(!configuration){
            configuration = {}
        }
        let form = this.state.form
        let validator = this.state.validator

        return (
            <>
                <Card>
                    <CardHeader>
                        API Description
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col sm={5}>
                                <FormGroup>
                                    <Label for="name">Base API</Label>
                                    <Input
                                        type="text"
                                        value={(configuration.baseApi?configuration.baseApi:"")}
                                        disabled
                                    />
                                </FormGroup>
                            </Col>
                            <Col sm={7}>
                                <FormGroup>
                                    <Label for="name">Path</Label>
                                    <Input
                                        type="text"
                                        name="name"
                                        value={form.fetchApi?form.fetchApi:""}
                                        onChange={(e) =>{
                                            form.fetchApi = e.target.value
                                            this.setState({
                                                form:form
                                            })
                                        }}
                                        placeholder="/some/service/ or /some/service/{query}"
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button onClick={e=>{
                                    e.preventDefault()
                                    let queryParams = form.queryParams
                                    queryParams.push(new QueryParam())
                                    form.queryParams = queryParams
                                    this.setState({
                                        form:form
                                    })
                                }}>Add Query</Button>
                            </Col>
                        </Row>
                        <br/>
                        {
                            form.queryParams.map((value, index) => {
                                return (
                                    <Row key={index+1}>
                                        <Col sm={5}>
                                            <FormGroup>
                                                <Label for="name">Query Key</Label>
                                                <Input
                                                    type="text"
                                                    name="name"
                                                    value={value.key?value.key:""}
                                                    onChange={(e) =>{
                                                        value.key = e.target.value
                                                        this.setState({
                                                            form:form
                                                        })
                                                    }}
                                                    placeholder="Enter query key"
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col sm={5}>
                                            <FormGroup>
                                                <Label for="name">Query Reference</Label>
                                                <Input
                                                    type="text"
                                                    name="name"
                                                    value={value.reference?value.reference:""}
                                                    onChange={(e) =>{
                                                        value.reference = e.target.value
                                                        this.setState({
                                                            form:form
                                                        })
                                                    }}
                                                    placeholder="Enter reference"
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col sm={2}>
                                            <br/>
                                            <Button color="primary" onClick={e=>{
                                                let queryParams = form.queryParams
                                                queryParams.splice(index, 1)
                                                form.queryParams = queryParams
                                                this.setState({
                                                    form:form
                                                })
                                            }
                                            }>
                                                <MdDelete color={"warning"} />
                                            </Button>
                                        </Col>
                                    </Row>
                                )
                            })
                        }
                        <WarningLabel show={!validator.fetchApi} message={validator.fetchApi}/>
                    </CardBody>
                </Card>
                <br/>
            </>

        )
    }

    renderFormImage(){

        let configuration = getData(Global.CONFIGURATION)
        if(!configuration){
            configuration = {}
        }
        let form  = this.state.form
        let validator = this.state.validator
        return (
            <>
                <Card>
                    <CardHeader>
                        Image Description
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col>
                                <FormGroup>
                                    <Label for="name">Image Directory</Label>
                                    <Input
                                        name="name"
                                        value={form.imageDirectory?form.imageDirectory:""}
                                        onChange={(e) =>{
                                            form.imageDirectory = e.target.value
                                            this.setState({
                                                form:form
                                            })
                                        }}
                                        placeholder="example : /opt/home/image/"
                                    />
                                    <WarningLabel show={!validator.imageDirectory} message={validator.imageDirectory}/>
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <br/>
            </>

        )
    }


    onClickOk(){
        let form = this.state.form
        let lane = this.state.lane
        let typeOption = this.state.typeOption
        let mandatoryOption = this.state.mandatoryOption
        let print = {
            form, lane, typeOption, mandatoryOption
        }
        let validator = this.state.validator
        if(!form.tag){
            validator.tag = "Please enter a form tag"
            this.setState({validator:validator})
            return ;
        }
        if(!form.title){
            validator.title = "Please enter a form title"
            this.setState({validator:validator})
            return ;
        }

        if(!form.placeHolder){
            validator.placeHolder = "Please enter a placeholder"
            this.setState({validator:validator})
            return ;
        }

        if(typeOption.name===FormType.IMAGE){
            if(!form.imageDirectory){
                validator.imageDirectory = "Please enter an image directory"
                this.setState({validator:validator})
                return ;
            }
        }
        if(typeOption.name===FormType.DROPDOWN_API_DATA){
            if(!form.fetchApi || form.fetchApi===''){
                validator.fetchApi = "Please enter a valid API path"
                this.setState({validator:validator})
                return ;
            }
            let containQuery = false ;
            if(containQuery){
                if(form.queryParams==null || form.queryParams.length>=0){
                    validator.fetchApi = "Please enter an additional query param"
                    this.setState({validator:validator})
                    return ;
                }
            }
        }
        form.type = typeOption.name
        form.mandatory = mandatoryOption.name

        if(this.props.onAdd){
            this.props.onAdd(form, lane)
            this.close()
        }

    }


    render(){
        let configuration = getData(Global.CONFIGURATION)
        if(!configuration){
            configuration = {}
        }
        let form = this.state.form

        let pageOptions = new Array();
        let lanes = this.state.lanes
        if(!lanes){
            lanes = new Array()
        }
        let lane = this.state.lane
        if(!lane){
            lane = {}
        }
        let validator = this.state.validator

        lanes.forEach(value => {
            let pageOption = new Option();
            pageOption.id = value.id
            pageOption.name = value.title
            pageOptions.push(pageOption)
        })
        let typeOption = this.state.typeOption
        let mandatoryOption = this.state.mandatoryOption

        return (
            /*xl-lg-sm*/
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Form Card
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Form Page</Label>
                                <ItemOption
                                    type="text"
                                    name="name"
                                    default={lane.id}
                                    objects={pageOptions}
                                    hideLabel={true}
                                    hideOptionLabel={true}
                                    callback={(e) =>{
                                        lane = e
                                        this.setState({
                                            lane:lane
                                        })
                                    }}
                                    placeholder="Enter page"
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">TAG</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={form.tag?form.tag:""}
                                    onChange={(e) =>{
                                        form.tag = e.target.value
                                        validator.tag  = ""
                                        this.setState({
                                            form:form,
                                            validator:validator
                                        })
                                    }}
                                    placeholder="Enter tag"
                                />
                                <WarningLabel show={!validator.tag} message={validator.tag}/>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="name">Type</Label>
                                <ItemOption
                                    type="text"
                                    name="name"
                                    default={typeOption.id}
                                    objects={this.typeOptions}
                                    hideLabel={true}
                                    hideOptionLabel={true}
                                    title={"Select Type"}
                                    callback={(typeOption) =>{
                                        this.setState({
                                            typeOption:typeOption
                                        })
                                    }}
                                    placeholder="Enter form type"
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    {
                     typeOption.name===FormType.DROPDOWN&&(null)
                    }
                    {
                        typeOption.name===FormType.DROPDOWN_API_DATA&&(
                            this.renderFormApi()
                        )
                    }
                    {
                        typeOption.name===FormType.IMAGE&&(
                            this.renderFormImage()
                        )
                    }
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Title</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={form.title?form.title:""}
                                    onChange={(e) =>{
                                        form.title = e.target.value
                                        validator.title = ""
                                        this.setState({
                                            form:form,
                                            validator:validator
                                        })
                                    }}
                                    placeholder="Enter form title"
                                />
                                <WarningLabel show={!validator.title} message={validator.title}/>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="name">Placeholder</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={form.placeHolder?form.placeHolder:""}
                                    onChange={(e) =>{
                                        form.placeHolder = e.target.value
                                        validator.placeHolder = ""
                                        this.setState({
                                            form:form,
                                            validator:validator
                                        })
                                    }}
                                    placeholder="Enter placeholder"
                                />
                                <WarningLabel show={!validator.placeHolder} message={validator.placeHolder}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Mandatory</Label>
                                <ItemOption
                                    type="text"
                                    name="name"
                                    default={mandatoryOption.id}
                                    objects={this.mandatoryOptions}
                                    hideLabel={true}
                                    hideOptionLabel={true}
                                    callback={(e) =>{
                                        mandatoryOption = e
                                        this.setState({
                                            mandatoryOption:mandatoryOption
                                        })
                                    }}
                                />
                            </FormGroup>
                        </Col>
                        {
                            (typeOption.name !== FormType.IMAGE && typeOption.name !== FormType.DROPDOWN && typeOption.name !== FormType.DROPDOWN_API_DATA) && (
                                <Col>
                                    <FormGroup>
                                        <Label for="name">Allowed Characters</Label>
                                        <Input
                                            type="text"
                                            name="name"
                                            value={form.allowedCharacters?form.allowedCharacters:""}
                                            onChange={(e) =>{
                                                form.allowedCharacters = e.target.value
                                                this.setState({
                                                    form:form
                                                })
                                            }}
                                            placeholder="Enter allowed characters"
                                        />
                                    </FormGroup>
                                </Col>
                            )
                        }
                    </Row>
                    {
                        (typeOption.name!==FormType.IMAGE && typeOption.name!==FormType.DROPDOWN && typeOption.name!==FormType.DROPDOWN_API_DATA)&&(
                            <Row>
                                <Col>
                                    <FormGroup>
                                        <Label for="name">Max Length</Label>
                                        <Input
                                            type="number"
                                            name="name"
                                            value={form.maxLength?form.maxLength:""}
                                            onChange={(e) =>{
                                                form.maxLength = e.target.value
                                                this.setState({
                                                    form:form
                                                })
                                            }}
                                            placeholder="Enter Max Length"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col>
                                    <FormGroup>
                                        <Label for="name">Line</Label>
                                        <Input
                                            type="number"
                                            name="name"
                                            value={form.line?form.line:""}
                                            onChange={(e) =>{
                                                form.line = e.target.value
                                                this.setState({
                                                    form:form
                                                })
                                            }}
                                            placeholder="Enter line count"
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                        )
                    }
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        // if(this.props.onAdd){
                        //     this.props.onAdd(form)
                        // }
                        this.onClickOk()
                        // this.close()
                    }}>
                        Ok
                    </Button>{' '}
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

FormModal.propTypes = {
    id:PropTypes.number.required,
    showing:PropTypes.bool.required,
    onAdd:PropTypes.func.required,
    close:PropTypes.func.required,
    lanes:PropTypes.array,
    lane:PropTypes.any
}