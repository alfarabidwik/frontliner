import React from 'react';
import {Button, Col, FormGroup, Modal, ModalFooter, ModalHeader, Table, Row, ModalBody} from "reactstrap";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import Checkbox from "../../components/Checkbox"
import {UserDto} from "../../model/model";
import PropTypes from 'prop-types'
import ModalPagination from "../ModalPagination";
import SearchInput from '../../components/SearchInput'

/*= (message, okCallback, cancelCallback) =>*/
export default class UserSelectionModal extends BaseComponent{

    users = new Array(UserDto)
    user = new UserDto();
    selectedUser = new Object();

    constructor(props){
        super(props);
        let state = this.state
        state.backdrop = true
        state.showing = this.props.showing
        state.users = this.users
        state.user = this.user
        state.selectedUser = this.props.selectedUser?this.props.selectedUser:this.selectedUser
        state.checkedAll = false
        state.allStartDate = null
        state.allEnddDate = null
        state.page = 1
        state.sortir = "created"
        state.ascending = true
        this.setState(state)
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let showing = this.state.showing
        if(this.props!=props){
            this.setState({
                showing:props.showing,
                selectedUser : props.selectedUser?props.selectedUser:this.selectedUser
            }, () => {
                if(props.showing && props.showing!==showing) {
                    this.fetchUsers()
                }
            })
        }
    }

    fetchUsers = () =>{
        let selectedUser = this.state.selectedUser

        this.get(Global.API.USERS, {
            params:{
                page:this.state.page-1,
                ascending:this.state.ascending,
                sortir: this.state.sortir,
                search:this.state.search,
            }
        }, null, response=>{
            if(response.code===200){
                let users = response.data
                if(selectedUser){
                    users.forEach((value, index) => {
                        if(selectedUser[users[index].id]){
                            users[index].checked = true
                        }else{
                            users[index].false = true
                        }
                    })
                    this.setState({users:users})
                }
                this.setState({
                    users:users,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            user:{},
            users:[],
            showing: false,
        }, () => {
            this.props.cancelCallback()
        })
    }

    render(){
        let selectedUser = this.state.selectedUser ;
        let users = this.state.users ;
        return (
            /*xl-lg-sm*/
            <Modal
                size="xl" centered={true} isOpen={this.state.showing} backdrop={true} zIndex="1">
                {super.render()}
                <ModalHeader>
                    User
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.fetchUsers()
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.fetchUsers()
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>

                        </Col>
                    </Row>
                    <Row>
                        <Table hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Fullname</th>
                                <th>Email</th>
                                <th>Mobile Phone</th>
                                <th>Agent Code</th>
                                <th>Check</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                users.map((item, index)=>(
                                    <tr valign="center" key={index}>
                                        <th scope="row">{++index}</th>
                                        <td>{item.fullname}</td>
                                        <td>{item.email}</td>
                                        <td>{item.mobilePhone}</td>
                                        <td>{item.agentCode}</td>
                                        <td>
                                            <Checkbox checked={item.checked} onChange={
                                                checked => {
                                                    if(checked){
                                                        selectedUser[item.id] = item
                                                    }else{
                                                        delete selectedUser[item.id]
                                                    }
                                                    item.checked = checked
                                                    this.setState({
                                                        selectedUser:selectedUser,
                                                        users:users
                                                    })
                                                }
                                            }/></td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                    </Row>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.fetchUsers()
                    })
                }}/>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        if(this.props.cancelCallback){
                            this.props.cancelCallback()
                        }
                        this.close()
                    }}>
                        Cancel
                    </Button>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        if(this.props.okCallback){
                            this.props.okCallback(this.state.selectedUser)
                        }
                        this.close()
                    }}>
                        Update
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

UserSelectionModal.propTypes = {
    selectedUser : PropTypes.any,
    showing : PropTypes.bool.required,
    okCallback : PropTypes.func,
    cancelCallback : PropTypes.func
}
