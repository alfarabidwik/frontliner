import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Checkbox from "../../components/Checkbox"
import Input from "reactstrap/es/Input";
import NumberInput from "../Widget/NumberInput";
/*= (message, okCallback, cancelCallback) =>*/
export default class InventoryQuantityModal extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            modal: this.props.modal,
            backdrop: true,
            inventory: this.props.inventory,
            okCallback : this.props.okCallback,
            closeCallback : this.props.closeCallback,
            newStock:0,
            positiveCheck:true,
            negativeCheck:false,
            note:"",
        }
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(modal!=props.showing){
            this.setState({
                modal:props.modal,
                inventory: props.inventory,
                okCallback : props.okCallback,
                closeCallback : props.closeCallback,
                newStock:0,
                positiveCheck:true,
                negativeCheck:false,
                note:"",
            })
        }
    }
    render(){
        let inventory = this.state.inventory
        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                <ModalHeader>
                    Update Stock / Quantity : {inventory&&this.state.inventory.product.name}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col md={6}>
                            <NumberInput
                                name="quantity"
                                className="form-control"
                                value={this.state.newStock}
                                onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                    if(this.state.negativeCheck){
                                        let newStock = floatvalue
                                        if(newStock<=this.state.inventory.quantity){
                                            this.setState({
                                                newStock:newStock
                                            })
                                        }else{
                                            this.setState({
                                                newStock:this.state.newStock
                                            })
                                        }

                                    }else{
                                        let newStock = floatvalue
                                        this.setState({
                                            newStock:newStock
                                        })
                                    }
                                }}
                                placeholder="0">
                            </NumberInput>
                        </Col>
                        <Col md={3}>
                            <Checkbox checked={this.state.positiveCheck} onChange={
                                checked => {
                                    if(checked){
                                        let positiveCheck = checked
                                        this.setState({
                                            positiveCheck:positiveCheck,
                                            negativeCheck:!positiveCheck,
                                        })
                                    }
                                }
                            }/>&nbsp;&nbsp;+
                        </Col>
                        <Col  md={3}>
                            <Checkbox checked={this.state.negativeCheck} onChange={
                                checked => {
                                    if(checked){
                                        let negativeCheck = checked
                                        let newStock = this.state.newStock
                                        if(newStock>this.state.inventory.quantity){
                                            newStock = this.state.inventory.quantity
                                        }
                                        this.setState({
                                            positiveCheck:!negativeCheck,
                                            newStock:newStock,
                                            negativeCheck:negativeCheck,
                                        })
                                    }
                                }
                            }/>&nbsp;&nbsp;-
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                            <Label for="note">Note</Label>
                            <Input
                                id="note"
                                type="text"
                                name="note"
                                value={this.state.note}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        note : value?value:null
                                    })
                                }}
                                placeholder="Enter note (recommended)"
                            />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="quantity">Current Stock</Label>
                                <Input
                                    id="quantity"
                                    type="text"
                                    name="note"
                                    value={this.state.inventory&&(this.state.inventory.quantity)}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="quantity">Total</Label>
                                <Input
                                    id="quantity"
                                    type="text"
                                    name="note"
                                    value={this.state.inventory&&(this.state.positiveCheck?(this.state.inventory.quantity+this.state.newStock):(this.state.inventory.quantity-this.state.newStock))}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="size">Size</Label>
                                <Input
                                    id="size"
                                    type="text"
                                    name="size"
                                    value={inventory&&inventory.size?inventory.size.name:""}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="colors">Color</Label>
                                <br/>
                                {
                                    inventory&&(inventory.colors.map((color, index1)=>(
                                        color!=null?
                                            <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                            :
                                            null
                                    )))
                                }
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Row>
                        <Col>
                            <Button color="primary" onClick={e=>{
                                if(this.state.okCallback){
                                    this.state.okCallback(this.state.inventory, this.state.newStock, this.state.note, this.state.negativeCheck)
                                }
                            }}>
                                Submit
                            </Button>
                        </Col>
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }

                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

InventoryQuantityModal.propTypes = {
    modal:PropTypes.bool,
    inventory:PropTypes.object,
    okCallback:PropTypes.func,
    closeCallback:PropTypes.func,
}
