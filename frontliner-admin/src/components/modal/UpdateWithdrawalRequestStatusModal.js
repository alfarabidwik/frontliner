import React from 'react';
import {
    Button, ButtonGroup,
    Card,
    CardBody,
    CardHeader, CardImg,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import ItemOption from "../Widget/ItemOption";
import Global, { WITHDRAWAL_STATUSES} from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {allIsEmpty, imageSelector, statusColor} from "../../utils/Utilities";
import receiptPicture from 'assets/img/receipt.png';
import ImageCropper from "./../../components/modal/ImageCropper";
import axios from "axios";

/*= (message, okCallback, cancelCallback) =>*/
export default class UpdateWithdrawalRequestStatusModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.modal= this.props.modal
        state.backdrop= true
        state.withdrawalRequest = this.props.withdrawalRequest
        state.withdrawalStatus = {}
        state.withdrawalStatuses = []
        state.newWithdrawalStatus = {}
        state.okCallback = this.props.okCallback
        state.closeCallback = this.props.closeCallback
        state.note = ""
        state.resiCode = ""
        state.resiCodeMessage=""
        state.receiptImage = ""
        state.image = null
        state.imageBlob = null
        state.accountNumber = null
        state.bank = null
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.state.modal!=props.modal){
            let withdrawalRequest = props.withdrawalRequest;
            let user = withdrawalRequest.user?withdrawalRequest.user:{};
            let bank = user.bank?user.bank:{};

            this.setState({
                modal:props.modal,
                withdrawalRequest: withdrawalRequest,
                user : user,
                bank : bank,
                accountNumber : user.accountNumber,
                okCallback : props.okCallback,
                closeCallback : props.closeCallback,
                withdrawalStatus:{},
                withdrawalStatuses:[],
                newWithdrawalStatus:{},
                note:"",
                resiCode : "",
                resiCodeMessage : "",
                image:null,
                imageBlob : null,
            }, () => {
                if(props.modal && modal!==props.modal){
                    this.fetchWithdrawalStatus()
                }
            })
        }
    }

    fetchWithdrawalStatus = () =>{
        let withdrawalRequest = this.state.withdrawalRequest?this.state.withdrawalRequest:{}
        let withdrawalStatus = withdrawalRequest.withdrawalStatus?withdrawalRequest.withdrawalStatus:{}
        console.log("APAAAA "+JSON.stringify(withdrawalRequest))
        if(withdrawalStatus.id){
            this.get(Global.API.WITHDRAWAL_STATUSES, null, null, response=>{
                if(response.code===200){
                    let withdrawalStatuses = response.data
                    let withdrawalStatusOptions = []
                    withdrawalStatuses.map((item, index)=>{
                        if(withdrawalStatus.id==WITHDRAWAL_STATUSES[2].id){
                            // if(item.id==WITHDRAWAL_STATUSES[4].id){
                            //     withdrawalStatusOptions.push(item)
                            // }
                        }else if(withdrawalStatus.id==WITHDRAWAL_STATUSES[3].id){
                            // Dont add any revert status
                        }else if(withdrawalStatus.id==WITHDRAWAL_STATUSES[4].id){
                            if(item.id==WITHDRAWAL_STATUSES[2].id){
                                withdrawalStatusOptions.push(item)
                            }
                            if(item.id==WITHDRAWAL_STATUSES[3].id){
                                withdrawalStatusOptions.push(item)
                            }
                        }else{
                            if(item.id>withdrawalStatus.id){
                                withdrawalStatusOptions.push(item)
                            }
                        }
                    })
                    this.setState({
                        withdrawalStatuses : withdrawalStatusOptions
                    })
                }
            }, true, true)
        }
    }


    async updateWithdrawalStatus(){
        let withdrawalRequest = this.state.withdrawalRequest?this.state.withdrawalRequest:{}
        let withdrawalStatus = withdrawalRequest.withdrawalStatus?withdrawalRequest.withdrawalStatus:{}
        let newWithdrawalStatus = this.state.newWithdrawalStatus?this.state.newWithdrawalStatus:{}
        let receiptCode = this.state.receiptCode
        let note = this.state.note?this.state.note:""
        let imageBlob = this.state.imageBlob
        if(imageBlob){
            imageBlob = await new Promise(resolve => {
                axios({
                    url: imageBlob,
                    method: 'GET',
                    responseType: 'blob', // important
                }).then((response) => {
                    this.closeProgress()
                    var file = response.data ;
                    resolve(file)
                })
            })
        }
        if(newWithdrawalStatus.id===WITHDRAWAL_STATUSES[2].id){
            if(!this.state.accountNumber){
                return ;
            }
            if(!this.state.bank){
                return ;
            }
            if(!imageBlob){
                return ;
            }
        }
        let confirmationMessage = "Do you want to update this withdrawalRequest status from = "+withdrawalStatus.name+" to = "+newWithdrawalStatus.name+" ? "
        this.openConfirmDialog("Confirmation", confirmationMessage, ()=>{
            let formData = new FormData();
            formData.append("withdrawalRequestId", withdrawalRequest.id)
            formData.append("withdrawalStatusId", newWithdrawalStatus.id)
            formData.append("note", note)
            formData.append("receiptCode", receiptCode)
            if(this.state.accountNumber){
                formData.append("accountNumber", this.state.accountNumber)
            }
            if(this.state.bank.id){
                formData.append("bankId", this.state.bank.id)
            }

            if(imageBlob){
                formData.append("multipartFile", imageBlob);
            }

            this.post(Global.API.WITHDRAWAL_REQUEST_UPDATE_STATUS, formData, null, response=>{
                if(response.code===200){
                    let withdrawalRequest = response.data
                    this.successToast(response.message)
                    this.setState({
                        modal:false
                    }, () => {
                        if(this.state.okCallback){
                            this.state.okCallback(withdrawalRequest)
                        }
                    })
                }
            }, true, true)
        })
    }

    render(){
        let imageAspect = 4/4

        let withdrawalRequest = this.state.withdrawalRequest?this.state.withdrawalRequest:{}
        let user = this.state.user
        let bank = this.state.bank
        let withdrawalStatus = withdrawalRequest.withdrawalStatus?withdrawalRequest.withdrawalStatus:{}
        let withdrawalStatuses = this.state.withdrawalStatuses?this.state.withdrawalStatuses:[]
        let newWithdrawalStatus = this.state.newWithdrawalStatus?this.state.newWithdrawalStatus:{}

        let receiptImage = this.state.receiptImage

        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Update Withdrawal Status
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="currentStatus">Current Status</Label>
                                <Input
                                    style={{fontSize:'bold', color:statusColor(withdrawalStatus.id), borderColor: statusColor(withdrawalStatus.id)}}
                                    id="currentStatus"
                                    type="text"
                                    name="currentStatus"
                                    value={withdrawalStatus.name}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <ItemOption
                                    title={"Select Status"}
                                    objects={withdrawalStatuses}
                                    default={newWithdrawalStatus.id}
                                    callback={(newWithdrawalStatus)=>{
                                        if(newWithdrawalStatus){
                                            this.setState({
                                                newWithdrawalStatus:newWithdrawalStatus
                                            })
                                        }else{
                                            this.setState({
                                                newWithdrawalStatus:{}
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FormGroup>
                            <Label for="note">Note</Label>
                            <Input
                                id="note"
                                type="text"
                                name="note"
                                value={this.state.note}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        note : value?value:null
                                    })
                                }}
                                placeholder="Enter note (recommended)"
                            />
                            </FormGroup>
                        </Col>
                    </Row>
                    {
                        newWithdrawalStatus.id==WITHDRAWAL_STATUSES[2].id?
                            (
                                <>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <Label for="accountNumber">Account Number</Label>
                                                <Input
                                                    id="accountNumber"
                                                    type="text"
                                                    name="note"
                                                    value={user.accountNumber}
                                                    disabled={true}
                                                />
                                            </FormGroup>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <Label for="bank">Bank</Label>
                                                <Input
                                                    id="bank"
                                                    type="text"
                                                    name="note"
                                                    value={bank.label?bank.label:''}
                                                    disabled={true}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <FormGroup>
                                                <Label for="receiptCode">Receipt Code</Label>
                                                <Input
                                                    id="receiptCode"
                                                    type="text"
                                                    name="note"
                                                    value={this.state.receiptCode}
                                                    onChange={(e) =>{
                                                        let value = e.target.value
                                                        this.setState({
                                                            receiptCode : value?value:null
                                                        })
                                                    }}
                                                    placeholder="Enter Receipt Code (recommended)"
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Card>
                                                <CardHeader>
                                                    Receipt Image
                                                </CardHeader>
                                                <CardBody>
                                                    <Row>
                                                        <Col className="text-center text-md-center">
                                                            <CardImg
                                                                style={{width:'300px', height:'300px'}}
                                                                top src={withdrawalRequest.imageUrl?withdrawalRequest.imageUrl:receiptImage}
                                                                className="justify-content-center"
                                                                onError={(elm)=>this.defaultImage(elm, receiptPicture)}/>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            <ButtonGroup className="float-left">
                                                                <Button color="danger" onClick={e => {
                                                                    let image = null
                                                                    this.setState({
                                                                        image:image
                                                                    })
                                                                }}>Cancel</Button>
                                                            </ButtonGroup>
                                                        </Col>
                                                        <Col>
                                                            <ButtonGroup className="float-right">
                                                                <Button color="primary" onClick={e=>{
                                                                    imageSelector(file =>{
                                                                        this.openCropper(file, (blob)=>{
                                                                            let image = blob
                                                                            this.setState({
                                                                                image:image
                                                                            })
                                                                        })
                                                                    }).click()
                                                                }}>Upload</Button>
                                                            </ButtonGroup>
                                                        </Col>
                                                    </Row>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </>
                            )
                            :
                            (null)
                    }
                </ModalBody>
                <ModalFooter>
                    <Row>
                        {
                            newWithdrawalStatus.id&&(
                                <Col>
                                    <Button color="primary" onClick={e=>{
                                        this.updateWithdrawalStatus()
                                    }}>
                                        Update
                                    </Button>
                                </Col>
                            )
                        }
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                this.setState({
                                    modal:false
                                })
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }
                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
                <ImageCropper src={this.state.image} aspect={imageAspect} show={this.state.imageCropperModal} callback={file=>{
                    axios({
                        url: file,
                        method: 'GET',
                        responseType: 'blob', // important
                    }).then((response) => {
                        var fileBlob = response.data ;
                        let cropperCallback = this.state.cropperCallback
                        let imageUrl = file
                        let imageBlob = file ;
                        withdrawalRequest.imageUrl = imageUrl
                        this.setState({
                            withdrawalRequest:withdrawalRequest,
                            imageCropperModal:false,
                            imageBlob:imageBlob
                        })
                        if(cropperCallback){
                            cropperCallback(file)
                        }
                    })
                }} cancelCallback={()=>{
                    let imageUrl = null
                    let imageBlob = null ;
                    withdrawalRequest.imageUrl = imageUrl
                    this.setState({
                        withdrawalRequest:withdrawalRequest,
                        imageCropperModal:false,
                        imageBlob:imageBlob
                    })
                    let cancelCallback = this.state.cancelCallback
                    if(cancelCallback){
                        cancelCallback()
                    }
                }}/>
            </Modal>
        )
    }

    openCropper(image, cropperCallback, cancelCallback){
        this.setState({
            image:image,
            imageCropperModal:true,
            cropperCallback:cropperCallback,
            cancelCallback:cancelCallback
        })
    }

}

UpdateWithdrawalRequestStatusModal.propTypes = {
    modal:PropTypes.bool,
    withdrawalRequest:PropTypes.object,
    okCallback:PropTypes.func,
    closeCallback:PropTypes.func,
}
