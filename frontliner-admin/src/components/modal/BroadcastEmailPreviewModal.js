import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {allIsEmpty, isEmpty, parseDate} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {DD_MM_YYYY_HH_MM_SS} from "../../utils/Global";
import renderHTML from 'react-render-html';

/*= (message, okCallback, cancelCallback) =>*/
export default class BroadcastEmailPreviewModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            modal: this.props.showing,
            edit:this.props.edit,
            sendEmail:this.props.sendemail,
            close:this.props.close,
        }
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                edit:props.edit,
                sendEmail:props.sendemail,
                close:props.close,
            })
        }
    }

    close = () =>{
        this.setState({
            sendEmail:{},
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    render(){
        let sendEmail = this.state.sendEmail?this.state.sendEmail:{}
        return (
            /*xl-lg-sm*/
            <Modal centered={true}
                isOpen={this.state.modal}
                style={{minWidth:'1000px', minHeight:'1000px'}}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Broadcast Content Preview
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Subject : <strong>{sendEmail.subject}</strong>
                        </Col>
                        <Col>
                            From : <strong>{sendEmail.sender}</strong>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            Send Directly : <strong>{sendEmail.sendDirectly?"Yes":"Scheduled"}</strong>
                        </Col>
                        <Col>
                            Send At : <strong>{parseDate(sendEmail.sendAt, DD_MM_YYYY_HH_MM_SS)}</strong>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            User Receiver : <strong>{sendEmail.allUser?"[All User]":sendEmail.userReceivers}</strong>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalBody>
                    {
                        renderHTML(sendEmail.content?sendEmail.content:"")
                    }
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
