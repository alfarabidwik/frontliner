import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, deleteParam, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {MdCheck, MdDelete, MdEdit} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import ModalPagination from "../ModalPagination";


/*= (message, okCallback, cancelCallback) =>*/
export default class DiscountProductModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            products:[],
            ascending:true,
            sortir:'p.created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            if(props.showing && props.showing!==modal) {
                this.setState({
                    modal:props.showing,
                    close:props.close,
                    page :1,
                    totalPage:0,
                    totalElement:0,
                    pageElement:0,
                    search:""
                }, () => {
                    this.productWithExistDiscount(true)
                })
            }
        }
    }

    productWithExistDiscount = (progressing) =>{
        this.get(Global.API.PRODUCT_WITH_EXIST_DISCOUNT, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    products:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    }

    refreshProduct = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.productWithExistDiscount(progressing)
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            products:[],
            modal: false,
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page:0
        }, () => {
            this.props.close()
        })
    }

    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Products
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "name", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "c.name", true))}>Category</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "h.name", true))}>Heading</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "b.name", true))}>Brand</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value,
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Heading</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Discount</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(this.state.products)?
                                this.state.products.map((item, index)=>(
                                    <tr key={index}>
                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                        <td>{item.name}</td>
                                        <td>{item.heading?item.heading.name:""}</td>
                                        <td>{item.category?item.category.name:""}</td>
                                        <td>{item.brand?item.brand.name:""}</td>
                                        <td>{item.hasActiveDiscount?(<MdCheck color="blue"/>):(null)}</td>
                                        <td>{parseDate(item.created)}</td>
                                        <td>{parseDate(item.updated)}</td>
                                        <td>
                                            {item.hasActiveDiscount?(
                                                <Button size="sm" color="danger" onClick={e => {
                                                    this.setState({
                                                        product:item
                                                    }, () => {
                                                        if(this.props.deleteConfirm){
                                                            this.props.deleteConfirm(item)
                                                        }
                                                    })
                                                }}><MdDelete/> Delete</Button>
                                            ):(null)}
                                            {' '}
                                            <Button size="sm" color={item.hasActiveDiscount?"primary":"info"} onClick={e => {
                                                this.close()
                                                if(this.props.openDiscountContentModal){
                                                    this.props.openDiscountContentModal(item)
                                                }
                                            }}>
                                                {
                                                    item.hasActiveDiscount?(<span><IoMdOpen/> Update</span>):(<span><IoMdOpen/> Select</span>)

                                                }
                                            </Button>
                                        </td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.productWithExistDiscount(true)
                    })
                }}/>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
