import React from 'react';
import {Button, CardImg, Col, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import Input from "reactstrap/es/Input";
import {
    allIsEmpty,
    finalPriceByPercent,
    isEmpty,
    parseDate,
    percentByDiscount,
    percentToPrice
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import Checkbox from "../../components/Checkbox"
import PropTypes from 'prop-types'


/*= (message, okCallback, cancelCallback) =>*/
export default class VoucherItemInventoryModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            product:this.props.product,
            voucher:this.props.voucher,
            voucherItem:this.props.voucherItem,
            edit:this.props.edit,
            vii:{},
            viis:[],
            selectedAll:false,
            allStartDate:null,
            allEnddDate:null,
            sortir:"created",
            ascending:true,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props&& props.showing&& props.showing!=modal){
            this.setState({
                edit:props.edit,
                modal:props.showing,
                voucher:props.voucher,
                voucherItem:props.voucherItem,
                close:props.close,
                product:props.product,
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.viiContentByProduct()
                }
            })
        }
    }

    viiContentByProduct = () =>{
        if(!isEmpty(this.state.product)){
            this.get(Global.API.VOUCHER_ITEM_INVENTORIES, {
                params:{
                    productId:this.state.product.id,
                    voucherId:this.state.voucher.id,
                    sortir:this.state.sortir,
                    ascending:this.state.ascending
                }
            }, null, response=>{
                if(response.code===200){
                    let viis = response.data
                    this.setState({
                        viis:viis
                    })
                }
            }, true, true)
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    overrideAllCheckbox = (selected) =>{
        let viis = null;

        if(!isEmpty(this.state.viis)){
            viis = this.state.viis
            viis.map((vii, index)=>{
                vii.selected = selected
                this.updateInventory(vii.inventory.id, vii.selected, (response)=>{
                    if(response.code===200){
                        vii = response.data
                    }
                })
            })
            this.setState({
                viis:viis,
                selectedAll:selected,
            })
        }
    }

    updateInventory  = (inventoryId, selected, responseCallback) =>{
        this.get(Global.API.VOUCHER_ITEM_INVENTORY_UPDATE, {
            params:{
                voucherItemId:this.state.voucherItem.id,
                inventoryId:inventoryId,
                selected:selected,
            }
        }, null, response=>{
            if(response.code===200){
                responseCallback(response)
            }
        }, true, true)
    }

    validateEmpty = () =>{
        if(this.state.voucherItem.id){
            this.get(Global.API.VOUCHER_ITEM_INVENTORY_VALIDATE_EMPTY, {
                params:{
                    voucherItemId:this.state.voucherItem.id,
                }
            }, null, response=>{
                this.closeDialog()
            }, true, true)
        }else{
            this.closeDialog()
        }
    }

    closeDialog = () =>{
        let viis = this.state.viis
        let voucherItemInventories = [] ;
        viis.map((item, index)=>{
            if(item.selected){
                voucherItemInventories.push(item)
            }
        })
        this.setState({
            vii:{},
            viis:[],
            modal: false,
        }, () => {
            if(this.props.close){
                this.props.close(voucherItemInventories)
            }
        })
    }

    render(){
        let viis = this.state.viis
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                   isOpen={this.state.modal}
                   backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Inventory
                </ModalHeader>
                <Table hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Type</th>
                        <th>S</th>
                        <th>Color</th>
                        <th >Img</th>
                        <th >Stock</th>
                        <th width="10%" ><Checkbox checked={this.state.selectedAll} onChange={selected =>this.overrideAllCheckbox(selected)}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !isEmpty(viis)?
                            viis.map((item, index)=>(
                                <tr key={index} valign="center">
                                    <th scope="row">{++index}</th>
                                    <td >{item.inventory.product.name}</td>
                                    <td >{item.inventory.typeName}</td>
                                    <td >{item.inventory.size?item.inventory.size.name:""}</td>
                                    <td>
                                        {
                                            item.inventory.colors.map((color, index1)=>(
                                                color&&<div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                            ))
                                        }
                                    </td>
                                    <td width="7%">
                                        <Button onClick={event=>{
                                            event.preventDefault()
                                            this.openLightBox(item.inventory.inventoryImages)
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td>{item.inventory.quantity}</td>
                                    <td width="10%"><Checkbox checked={item.selected} onChange={
                                        selected => {
                                            item.selected = selected
                                            this.updateInventory(item.inventory.id, item.selected, (response)=>{
                                                if(response.code===200){
                                                    item = response.data
                                                }else {

                                                }
                                                this.setState({
                                                    viis:viis
                                                })
                                            })
                                        }
                                    }/></td>
                                </tr>
                            ))
                            :
                            null
                    }
                    </tbody>
                </Table>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        this.validateEmpty()
                    }}>
                        Close Dialog
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

VoucherItemInventoryModal.propTypes  = {
    showing: PropTypes.bool,
    close:PropTypes.any,
    product:PropTypes.any,
    voucher:PropTypes.any,
    voucherItem:PropTypes.any,
    edit:PropTypes.any

}
