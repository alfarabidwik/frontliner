import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import ModalPagination from "../ModalPagination";
import Checkbox from "../../components/Checkbox"

import PropTypes from 'prop-types'

/*= (message, okCallback, cancelCallback) =>*/
export default class CustomerVoucherModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            voucher:this.props.voucher,
            customers:[],
            ascending:true,
            sortir:'created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
            checkedCustomer:{}
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props && ((props.showing && props.showing!=modal))){
            this.setState({
                modal:props.showing,
                close:props.close,
                voucher:props.voucher,
                page :1,
                search:"",
                totalPage:0,
                totalElement:0,
                pageElement:0,
                checkedCustomer:{}
            }, () => {
                if(props.showing && props.showing!==modal){
                    this.fetchAll(this.state.ascening, this.state.sortir, this.state.search, true)
                }
            })
        }
    }


    refreshCustomer = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, progressing)
        })
    )

    fetchAll = (ascending, sortir, search, progressing) => {
        this.get(Global.API.CUSTOMERS_NO_FILTER, {
            params:{
                page:this.state.page-1,
                ascending:ascending,
                sortir:sortir,
                search:search
            }
        }, null, res =>{
            if(res.code === 200){
                this.setState({
                    customers : res.data,
                    totalPage: res.totalPage,
                    totalElement: res.totalElement,
                    pageElement: res.pageElement,
                })
            }
        }, progressing, true);
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            customers:[],
            modal: false,
            totalPage: 0,
            totalElement: 0,
            pageElement: 0,
            page:0
        }, () => {
            this.props.close()
        })
    }

    update=()=>{
        let checkedCustomer = this.state.checkedCustomer
        let customerIds = []
        Object.keys(checkedCustomer).forEach(function eachKey(key) {
            if(checkedCustomer[key]){
                customerIds.push(key)
            }
        })

        this.post(Global.API.VOUCHER_CUSTOMER_UPDATE+"?voucherId="+this.state.voucher.id, customerIds, null, response=>{
            if(response.code===200){
                this.successToast(response.message)
                this.close()
            }else{
                this.errorToast(response.message)
            }
        }, true, true)
    }

    render(){
        let checkedCustomer = this.state.checkedCustomer
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Customer
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "firstname", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "email", true))}>Email</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "phoneNumber", true))}>Phone Number</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(this.state.ascending, "updated", true))}>Last Update</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshCustomer(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value,
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.refreshCustomer(this.state.ascending, this.state.sortir, false)
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        this.setState({
                                            page:1
                                        }, () => {
                                            if(this.state.search!==''){
                                                this.refreshCustomer(this.state.ascending, this.state.sortir, false)
                                            }
                                        })
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fullname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Updated</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.customers.map((item, index)=>{

                                return (
                                    <tr key={item.id}>
                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                        <td>{item.firstname+" "+item.lastname}</td>
                                        <td>{item.email}</td>
                                        <td>{item.phoneNumber}</td>
                                        <td>{item.customerStatus.name}</td>
                                        <td>{parseDate(item.updated)}</td>
                                        <td>
                                            <Checkbox checked={checkedCustomer[item.id]} onChange={
                                                checked => {
                                                    checkedCustomer[item.id] = checked
                                                    this.setState({
                                                        checkedCustomer:checkedCustomer
                                                    })
                                                }
                                            }/>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.fetchAll(this.state.ascending, this.state.sortir, this.state.search, true)
                    })
                }}/>
                <ModalFooter>
                    <Row>
                        <Col>
                            <Button className="float-left" color="secondary" onClick={e=>(this.close())}>
                                Cancel
                            </Button>
                        </Col>
                        <Col>
                            <Button className="float-right" color="primary" onClick={e=>(
                                this.update()
                            )}>
                                Update
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

CustomerVoucherModal.propTypes = {
    showing: PropTypes.bool,
    close: PropTypes.func,
    voucher:PropTypes.object.isRequired
}
