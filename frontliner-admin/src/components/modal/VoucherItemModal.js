import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {MdCheck, MdDelete, MdEdit, MdUpdate} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import WarningCheckbox from "../../components/WarningCheckbox"
import Checkbox from "../../components/Checkbox"
import ModalPagination from "../ModalPagination";
import Tooltip from "react-simple-tooltip"
import NumberInput from "../Widget/NumberInput";


/*= (message, okCallback, cancelCallback) =>*/
export default class VoucherItemModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            voucher: this.props.voucher,
            voucherItems:[],
            ascending:true,
            sortir:'p.created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentDidMount() {
        super.componentDidMount();
        // this.productMergeVoucherItems()
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal

        if(this.props!=props && props.showing && props.showing!=modal){
            this.setState({
                modal:props.showing,
                close:props.close,
                voucher: this.props.voucher,
                page :1,
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.productMergeVoucherItems()
                }
            })
        }
    }

    productMergeVoucherItems = () =>{
        this.get(Global.API.PRODUCT_MERGE_VOUCHER_ITEMS, {
            params:{
                voucherId:this.state.voucher.id,
                keyword:this.state.search,
                page:this.state.page-1,
                ascending:this.state.ascending,
                sortir:this.state.sortir,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    voucherItems:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }

    refreshProduct = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.productMergeVoucherItems()
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            voucherItems:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    updateVoucherItem = (item, responseCallback) =>{
        this.get(Global.API.VOUCHER_ITEM_UPDATE, {
            params:{
                voucherItemId:item.id,
                voucherId:this.state.voucher.id,
                productId:item.product.id,
                minimumQuantity:item.minimumQuantity,
                allInventory:item.allInventory,
            }
        }, null, response=>{
            if(response.code===200){
                this.successToast(response.message)
                responseCallback(response)
            }
        }, true, true)
    }

    updateMinimumQuantity = (item, responseCallback) =>{
        this.get(Global.API.VOUCHER_ITEM_UPDATE_MIN_QUANTITY, {
            params:{
                voucherItemId:item.id,
                minimumQuantity:item.minimumQuantity,
            }
        }, null, response=>{
            if(response.code===200){
                this.successToast("Stock updated")
                responseCallback = response
            }
        }, true, true)
    }

    delete = (item, responseCallback) =>{
        this.get(Global.API.VOUCHER_ITEM_DELETE, {
            params:{
                id:item.id,
            }
        }, null, response=>{
            if(response.code===200){
                this.successToast("Item has been removed from voucher")
                item.id = null;
                item.voucherItemInventories = []
                item.minimumQuantity = 0
                responseCallback(response)
            }
        }, true, true)
    }



    render(){
        let voucherItems = this.state.voucherItems
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Products
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "created"))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "name"))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "h.name"))}>Heading</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "c.name"))}>Category</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "updated"))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search} onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.refreshProduct(this.state.ascending, this.state.sortir)
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.refreshProduct(this.state.ascending, this.state.sortir)
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Heading / Category</th>
                            <th>Exclude</th>
                            <th>Min Qty</th>
                            <th>All</th>
                            <th>Per Inventory</th>
                            <th>Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(voucherItems)?
                                voucherItems.map((item, index)=>(
                                    <tr>
                                        <th scope="row">{index+1}</th>
                                        <td>{item.product.name}</td>
                                        <td>
                                            {item.product.heading?item.product.heading.name:"-"}
                                            &nbsp; / &nbsp;
                                            {item.product.category?item.product.category.name:"-"}
                                        </td>
                                        <td width="10%"><WarningCheckbox checked={!item.selected}  onChange={checked=>{
                                            item.selected = false
                                            item.allInventory = false ;
                                            item.minimumQuantity = 0
                                            this.setState({
                                                voucherItems:voucherItems
                                            }, () => {
                                                if(item.id){
                                                    this.delete(item, response=>{
                                                        if(response.code===200){

                                                        }
                                                    })
                                                }
                                            })
                                        }}/></td>
                                        <td width="15%">
                                            <Row>
                                                <Col>
                                                    <NumberInput
                                                        name="minimumQuantity"
                                                        className="form-control"
                                                        value={(item.minimumQuantity!=null)?item.minimumQuantity:0}
                                                        disabled={!item.selected}
                                                        onChangeEvent={(e, maskedvalue, floatvalue)=>{
                                                            e.preventDefault()
                                                            item.minimumQuantity = floatvalue
                                                            this.setState({
                                                                voucherItems:voucherItems
                                                            })
                                                        }}
                                                        maxLength="3"
                                                        placeholder="0">
                                                    </NumberInput>
                                                </Col>
                                                <Col>
                                                    {
                                                        item.selected&&(

                                                    <Tooltip fontSize="12px" content="Click to update quantity">
                                                        <Button onClick={event=>{
                                                            event.preventDefault()
                                                            if(item.selected){
                                                                this.updateMinimumQuantity(item, response=>{
                                                                    if(response.code===200){
                                                                        item = response.data
                                                                        this.setState({
                                                                            voucherItems:voucherItems
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        }}>
                                                            <MdUpdate/>
                                                        </Button>
                                                    </Tooltip>
                                                        )
                                                    }
                                                </Col>
                                            </Row>
                                        </td>
                                        <td>
                                            <Checkbox checked={item.allInventory && item.selected} onChange={checked=>{
                                                item.selected = true
                                                item.allInventory = checked ;
                                                this.updateVoucherItem(item, response=>{
                                                    if(response.code===200){
                                                        item = response.data
                                                        voucherItems[index] = item
                                                        this.setState({
                                                            voucherItems:voucherItems
                                                        })
                                                    }
                                                })
                                            }}/>
                                        </td>
                                        <td>
                                            <Checkbox checked={!item.allInventory && item.selected} onChange={checked=>{
                                                item.selected = true
                                                item.allInventory = false ;
                                                this.updateVoucherItem(item, response=>{
                                                    if(response.code===200){
                                                        item = response.data
                                                        this.setState({
                                                            voucherItems : voucherItems
                                                        }, () => {
                                                            if(!item.allInventory){
                                                                if(this.props.openVoucherItemInventoryModal){
                                                                    this.props.openVoucherItemInventoryModal(item, item.product, (voucherItemInventories)=>{
                                                                        // item.voucherItemInventories = voucherItemInventories
                                                                        this.productMergeVoucherItems()
                                                                        // this.setState({
                                                                        //     voucherItems : voucherItems
                                                                        // }, () => {
                                                                        //     this.forceUpdate()
                                                                        // })
                                                                    })
                                                                }
                                                            }
                                                        })
                                                    }
                                                })
                                            }}/>
                                            &nbsp;
                                            <Button color={!item.allInventory && item.selected?"primary":"light"} onClick={event=>{
                                                event.preventDefault()
                                                if(!item.allInventory && item.id){
                                                    if(this.props.openVoucherItemInventoryModal){
                                                        this.props.openVoucherItemInventoryModal(item, item.product, (voucherItemInventories)=>{
                                                            this.productMergeVoucherItems()
                                                            // item.voucherItemInventories= voucherItemInventories
                                                            // this.setState({
                                                            //     voucherItems : voucherItems
                                                            // })
                                                        })
                                                    }
                                                }
                                            }}>
                                                {
                                                    item.selected&&item.voucherItemInventories?item.voucherItemInventories.length:0
                                                }
                                                &nbsp;Item
                                            </Button>
                                        </td>
                                        <td>{parseDate(item.product.updated)}</td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.refreshProduct(this.state.ascending, this.state.sortir)
                    })
                }}/>
                {/*<ModalFooter>*/}
                    {/*<ButtonToolbar className="float-right">*/}
                        {/*<ButtonGroup className="mr-2">*/}
                            {/*{*/}
                                {/*this.paginationButton()*/}
                            {/*}*/}
                        {/*</ButtonGroup>*/}
                        {/*<ButtonGroup className="mr-2">*/}
                            {/*<Button>></Button>*/}
                        {/*</ButtonGroup>*/}
                        {/*<ButtonGroup className="mr-2">*/}
                            {/*<Button>>></Button>*/}
                        {/*</ButtonGroup>*/}
                    {/*</ButtonToolbar>*/}
                {/*</ModalFooter>*/}
                <ModalFooter>
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close Dialog
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
    // paginationButton = () =>{
    //     let button = []
    //     for (let i = 0; i < this.state.totalPage; i++) {
    //         button.push(<Button onClick={e=>{
    //             e.preventDefault()
    //             this.setState({
    //                 page:--i
    //             }, () => {
    //                 this.refreshProduct(this.state.ascending, this.state.sortir)
    //             })
    //
    //         }}>{++i}</Button>)
    //
    //     }
    //     return button
    // }

}
