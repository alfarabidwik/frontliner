import React from 'react';
import {Button, CardImg, Col, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {
    isEmpty,
    parseDate,
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS} from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import PropTypes from 'prop-types'
import {MdDelete} from "react-icons/md";


/*= (message, okCallback, cancelCallback) =>*/
export default class WithdrawalRequestTrackerModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.user = this.props.user
        state.modal = this.props.showing
        state.pvWithdrawalRequestStatus = {}
        state.close = this.props.close
        state.pvWithdrawalRequestStatuses = []
        state.sortir = "created"
        state.ascending = true
        state.pvWithdrawalRequestStatuses = this.props.pvWithdrawalRequestStatuses
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let showing = this.state.modal
        if(this.props!=props && props.showing){
            this.setState({
                user : props.user,
                close:props.close,
                modal:props.showing,
                pvWithdrawalRequestStatuses:props.pvWithdrawalRequestStatuses
            })
        }
    }

    close = () =>{
        this.setState({
            pvWithdrawalRequestStatus:{},
            pvWithdrawalRequestStatuses:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    render(){
        let pvWithdrawalRequestStatuses = this.state.pvWithdrawalRequestStatuses
        if(!pvWithdrawalRequestStatuses){
            pvWithdrawalRequestStatuses = []
        }

        let user = this.state.user
        if(!user){
            user = {}
        }
        return (
            /*xl-lg-sm*/
            <Modal
                size="xl" centered={true} isOpen={this.state.modal} backdrop={true} zIndex="1">
                {super.render()}
                <ModalHeader>
                    {user.firstname}'s Withdrawal Status Tracker
                </ModalHeader>
                <Table hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Description</th>
                        <th>Executor</th>
                        <th>Note</th>
                        <th>Receipt Code</th>
                        <th>Attachment</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !isEmpty(pvWithdrawalRequestStatuses)?
                            pvWithdrawalRequestStatuses.map((item, index)=>(
                                <tr valign="center" key={index}>
                                    <th scope="row">{++index}</th>
                                    <td>{item.statusName}</td>
                                    <td>{item.statusDescription}</td>
                                    <td>{item.adminExecutor?item.adminExecutor.fullname:"-"}</td>
                                    <td>{item.note}</td>
                                    <td>{item.receiptCode}</td>
                                    <td>
                                        {
                                            item.image?
                                                (<img src={item.imageUrl} style={{width:'50px',height:'50px'}} onClick={event => {
                                                    this.openLightBoxSingleImage(item.imageUrl)}
                                                }/>)
                                                :
                                                (null)
                                        }
                                    </td>
                                    <td>{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                                </tr>
                            ))
                            :
                            null
                    }
                    </tbody>
                </Table>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

WithdrawalRequestTrackerModal.propTypes = {
   user : PropTypes.object.isRequired,
    showing:PropTypes.bool.isRequired,
    close:PropTypes.func,
    pvWithdrawalRequestStatuses:PropTypes.any.isRequired
}
