import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import ItemOption from "../Widget/ItemOption";
import Global, {_DELIVERY} from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {statusColor} from "../../utils/Utilities";
/*= (message, okCallback, cancelCallback) =>*/
export default class UpdateTaskApplicationStatusModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.modal= this.props.modal
        state.backdrop= true
        state.taskApplication = this.props.taskApplication
        state.taskApplicationStatus = {}
        state.taskApplicationStatuses = []
        state.newTaskApplicationStatus = {}
        state.okCallback = this.props.okCallback
        state.closeCallback = this.props.closeCallback
        state.note = ""
        state.resiCode = ""
        state.resiCodeMessage=""
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.state.modal!=props.modal){
            this.setState({
                modal:props.modal,
                taskApplication: props.taskApplication,
                okCallback : props.okCallback,
                closeCallback : props.closeCallback,
                taskApplicationStatus:{},
                taskApplicationStatuses:[],
                newTaskApplicationStatus:{},
                note:"",
                resiCode : "",
                resiCodeMessage : "",
            }, () => {
                if(props.modal && modal!==props.modal){
                    this.fetchTaskApplicationStatus()
                }
            })
        }
    }

    fetchTaskApplicationStatus = () =>{
        let taskApplication = this.state.taskApplication?this.state.taskApplication:{}
        let taskApplicationStatus = taskApplication.taskApplicationStatus?taskApplication.taskApplicationStatus:{}
        if(taskApplicationStatus.id){
            this.get(Global.API.TASK_APPLICATION_STATUSES, null, null, response=>{
                if(response.code===200){
                    let taskApplicationStatuses = response.data
                    let taskApplicationStatusOptions = []
                    taskApplicationStatuses.map((item, index)=>{
                        if(item.id>taskApplicationStatus.id){
                            taskApplicationStatusOptions.push(item)
                        }
                    })
                    this.setState({
                        taskApplicationStatuses : taskApplicationStatusOptions
                    })
                }
            }, true, true)
        }
    }


    updateTaskApplicationStatus=()=>{
        let taskApplication = this.state.taskApplication?this.state.taskApplication:{}
        let taskApplicationStatus = taskApplication.taskApplicationStatus?taskApplication.taskApplicationStatus:{}
        let newTaskApplicationStatus = this.state.newTaskApplicationStatus?this.state.newTaskApplicationStatus:{}
        let note = this.state.note?this.state.note:""
        let resiCode = this.state.resiCode?this.state.resiCode:""
        let resiCodeMessage = this.state.resiCodeMessage
        if(newTaskApplicationStatus.id===_DELIVERY && (resiCode===null || resiCode==="")){
            this.setState({
                resiCodeMessage:"Resi Code cannot be empty"
            })
            return ;
        }else{
            let confirmationMessage = "";
            if(newTaskApplicationStatus.id===_DELIVERY){
                confirmationMessage= "Do you want to update this taskApplication status from = "+taskApplicationStatus.name+" to = "+newTaskApplicationStatus.name+" with resi code is "+resiCode+" ? "
            }else{
                confirmationMessage= "Do you want to update this taskApplication status from = "+taskApplicationStatus.name+" to = "+newTaskApplicationStatus.name+" ? "
            }
            this.openConfirmDialog("Confirmation", confirmationMessage, ()=>{
                let formData = new FormData();
                formData.append("taskApplicationId", taskApplication.id)
                formData.append("taskApplicationStatusId", newTaskApplicationStatus.id)
                formData.append("note", note)
                formData.append("resiCode", resiCode)
                this.post(Global.API.TASK_APPLICATION_UPDATE_STATUS, formData, null, response=>{
                    if(response.code===200){
                        let taskApplication = response.data
                        this.successToast(response.message)
                        this.setState({
                            modal:false
                        }, () => {
                            if(this.state.okCallback){
                                this.state.okCallback(taskApplication)
                            }
                        })
                    }
                }, true, true)
            })
        }
    }

    render(){
        let taskApplication = this.state.taskApplication?this.state.taskApplication:{}
        let taskApplicationStatus = taskApplication.taskApplicationStatus?taskApplication.taskApplicationStatus:{}
        let taskApplicationStatuses = this.state.taskApplicationStatuses?this.state.taskApplicationStatuses:[]
        let newTaskApplicationStatus = this.state.newTaskApplicationStatus?this.state.newTaskApplicationStatus:{}

        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Update TaskApplication Status : {taskApplication&&taskApplication.refCode}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="currentStatus">Current Status</Label>
                                <Input
                                    style={{fontSize:'bold', color:statusColor(taskApplicationStatus.id), borderColor: statusColor(taskApplicationStatus.id)}}
                                    id="currentStatus"
                                    type="text"
                                    name="currentStatus"
                                    value={taskApplicationStatus.name}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <ItemOption
                                    title={"Select Status"}
                                    objects={taskApplicationStatuses}
                                    default={newTaskApplicationStatus.id}
                                    callback={(newTaskApplicationStatus)=>{
                                        if(newTaskApplicationStatus!=null){
                                            this.setState({
                                                newTaskApplicationStatus:newTaskApplicationStatus
                                            })
                                        }else{
                                            this.setState({
                                                newTaskApplicationStatus:{}
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                            <Label for="note">Note</Label>
                            <Input
                                id="note"
                                type="text"
                                name="note"
                                value={this.state.note}
                                onChange={(e) =>{
                                    let value = e.target.value
                                    this.setState({
                                        note : value?value:null
                                    })
                                }}
                                placeholder="Enter note (recommended)"
                            />
                            </FormGroup>
                        </Col>
                    </Row>
                    {
                        (newTaskApplicationStatus.id===_DELIVERY)&&(
                            <Row>
                                <Col>
                                    <FormGroup>
                                        <Label for="resiCode">Resi Code</Label>
                                        <Input
                                            id="resiCode"
                                            type="text"
                                            name="resiCode"
                                            value={this.state.resiCode}
                                            onChange={(e) =>{
                                                let value = e.target.value
                                                let resiCodeMessage = this.state.resiCodeMessage
                                                if(value!==null || value!==''){
                                                    resiCodeMessage = ""
                                                }
                                                this.setState({
                                                    resiCode : value?value:null,
                                                    resiCodeMessage : resiCodeMessage
                                                })
                                            }}
                                            placeholder="Enter resi code..."
                                        />
                                        <br/>
                                        <span style={{color:"#ff061a"}}>{this.state.resiCodeMessage}</span>
                                    </FormGroup>
                                </Col>
                            </Row>
                        )
                    }
                </ModalBody>
                <ModalFooter>
                    <Row>
                        {
                            newTaskApplicationStatus.id&&(
                                <Col>
                                    <Button color="primary" onClick={e=>{
                                        this.updateTaskApplicationStatus()
                                    }}>
                                        Update
                                    </Button>
                                </Col>
                            )
                        }
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                this.setState({
                                    modal:false
                                })
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }
                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

UpdateTaskApplicationStatusModal.propTypes = {
    modal:PropTypes.bool,
    taskApplication:PropTypes.object,
    okCallback:PropTypes.func,
    closeCallback:PropTypes.func,
}
