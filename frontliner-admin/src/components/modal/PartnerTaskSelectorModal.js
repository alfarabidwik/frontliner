import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import Input from "reactstrap/es/Input";
import ModalPagination from "../ModalPagination";
import PropTypes from 'prop-types'

/*= (message, okCallback, cancelCallback) =>*/
export default class PartnerTaskSelectorModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            partners:[],
            partner:{},
            ascending:true,
            sortir:'jp.created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
        this.fetchAll()
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                close:props.close,
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.fetchAll()
                }
            })
        }
    }

    fetchAll = () => {
        this.get(Global.API.PARTNERS, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    partners:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }

    refreshPartner = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll()
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            partners:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    partnerSelected = (e) =>{
        e.preventDefault()
        let partner = this.state.partner
        this.setState({
            partner:partner
        },() => {
            if(this.props.onSelected){
                this.props.onSelected(partner)
            }
            this.setState({
                partner:{}
            })
        })
    }

    render(){
        var partners = this.state.partners
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Partners
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.fullName", true))}>Fullname</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.website", true))}>Website</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.email", true))}>Email</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(this.state.ascending, "jp.updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshPartner(true, this.state.sortir))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshPartner(false, this.state.sortir))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.refreshPartner(this.state.ascending, this.state.sortir)
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.refreshPartner(this.state.ascending, this.state.sortir)
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fullname</th>
                            <th>Website</th>
                            <th>Email</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(partners)?
                                partners.map((item, index)=>(
                                    <tr>
                                        <th scope="row">{++index}</th>
                                        <td>{item.fullName}</td>
                                        <td>{item.website}</td>
                                        <td>{item.email}</td>
                                        <td>{parseDate(item.created)}</td>
                                        <td>{parseDate(item.updated)}</td>
                                        <td>
                                            <Input
                                                checked={item.selected}
                                                type="radio"
                                                onChange={e=>{
                                                    var partners = this.state.partners
                                                    partners.map((item2, index)=>{
                                                        if(item2.id===item.id){
                                                            item2.selected = e.target.value
                                                        }else{
                                                            item2.selected = false
                                                        }
                                                    })
                                                    item.selected = e.target.value
                                                    this.setState({
                                                        partners:partners,
                                                        partner:item
                                                    })
                                                }}/>
                                        </td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.refreshPartner(this.state.ascending, this.state.sortir)
                    })
                }}/>

                <ModalFooter>
                    {
                        isEmpty(this.state.partner.selected)?(null):(
                            <Button color="primary" onClick={this.partnerSelected}>
                                Save
                            </Button>
                        )
                    }
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
    paginationButton = () =>{
        let button = []
        for (let i = 0; i < this.state.totalPage; i++) {
            button.push(<Button onClick={e=>{
                e.preventDefault()
                this.setState({
                    page:--i
                }, () => {
                    this.refreshPartner(this.state.ascending, this.state.sortir)
                })

            }}>{++i}</Button>)

        }
        return button
    }

}


PartnerTaskSelectorModal.propTypes = {
    showing:PropTypes.bool,
    close:PropTypes.func.required,
    onSelected : PropTypes.func.required
}
