import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, deleteParam, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {MdCheck, MdDelete, MdEdit, MdSearch} from "react-icons/md";
import {IoMdOpen} from "react-icons/io";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import WarningCheckbox from "../../components/WarningCheckbox"
import Checkbox from "../../components/Checkbox"
import PropTypes from 'prop-types'
import ModalPagination from "../ModalPagination";



/*= (message, okCallback, cancelCallback) =>*/
export default class ProductSelebgramModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.modal,
            close:this.props.close,
            selebgram: this.props.selebgram,
            products:[],
            ascending:true,
            sortir:'created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentDidMount() {
        super.componentDidMount();
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(props.modal){
            this.setState({
                modal:props.modal,
                close:props.close,
                selebgram: props.selebgram,
            }, () => {
                if(props.modal && props.modal!==modal){
                    this.products(true)
                }
            })
        }
    }

    products = (progressing) =>{
        (this.state.selebgram&&this.state.selebgram.id)&&(
        this.get(Global.API.SELEBGRAM_PRODUCTS, {
            params:{
                selebgramId:this.state.selebgram.id,
                search:this.state.search,
                page:this.state.page-1,
                sortir:this.state.sortir,
                ascending:this.state.ascending,
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    products:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, progressing, true)
    )
    }

    refreshProduct = (ascending, sortir, progressing) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.products(progressing)
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            products:[],
            selebgram: {},
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    endorse = (item, responseCallback) =>{
        this.get(Global.API.SELEBGRAM_ENDORSE, {
            params:{
                productId:item.id,
                selebgramId:this.state.selebgram.id,
                minimumQuantity:item.minimumQuantity,
                allInventory:item.allInventory,
            }
        }, null, response=>{
            responseCallback(response)
        }, true, true)
    }

    delete = (item, responseCallback) =>{
        this.get(Global.API.SELEBGRAM_ENDORSEMENT_DELETE, {
            params:{
                productId:item.id,
                selebgramId:this.state.selebgram.id,
            }
        }, null, response=>{
            responseCallback(response)
        }, true, true)
    }



    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Name : {this.state.selebgram.name} / {this.state.selebgram.selebgram} Selebgram
                </ModalHeader>
                <ModalHeader>
                    Products
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "created", true))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "name", true))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "category.name", true))}>Category Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "updated", true))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir, true))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir, true))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Search product, category..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.refreshProduct(this.state.ascending, this.state.sortir, true)
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            {/*<Button color="primary" onClick={event=>{*/}
                                {/*this.setState({*/}
                                    {/*page:1,*/}
                                {/*}, () => {*/}
                                    {/*this.refreshProduct(this.state.ascending, this.state.sortir, true)*/}
                                {/*})*/}
                            {/*}}><MdSearch/></Button>*/}
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Endorsement</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(this.state.products)?
                                this.state.products.map((item, index)=>(
                                    <tr key={index}>
                                        <th scope="row">{((this.state.page-1)* this.state.pageElement)+(++index)}</th>
                                        <td>{item.name}</td>
                                        <td>{item.category?item.category.name:""}</td>
                                        <td width="4%" ><Checkbox checked={item.endorsed} onChange={
                                            checked => {
                                                if(checked){
                                                    this.endorse(item, response=>{
                                                        if(response.code===200){
                                                            item.endorsed = checked
                                                            let products = this.state.products
                                                            this.setState({
                                                                products:products
                                                            })
                                                        }
                                                    })
                                                }else{
                                                    this.delete(item, response=>{
                                                        if(response.code===200){
                                                            item.endorsed = checked
                                                            let products = this.state.products
                                                            this.setState({
                                                                products:products
                                                            })
                                                        }
                                                    })
                                                }

                                            }
                                        }/></td>
                                        <td>{parseDate(item.created)}</td>
                                        <td>{parseDate(item.updated)}</td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.products(true)
                    })
                }}/>
                <ModalFooter>
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close Dialog
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }

}

ProductSelebgramModal.propTypes = {
    modal: PropTypes.bool,
    close: PropTypes.func,
    selebgram: PropTypes.object,

}


