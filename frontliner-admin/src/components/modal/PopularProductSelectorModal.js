import React from 'react';
import {
    Button, ButtonGroup, ButtonToolbar, DropdownItem, DropdownMenu, DropdownToggle,
    FormGroup,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
    UncontrolledButtonDropdown
} from "reactstrap";
import {allIsEmpty, isEmpty, parseDate, sortirMap} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SearchInput from '../../components/SearchInput'
import Input from "reactstrap/es/Input";
import ModalPagination from "../ModalPagination";

/*= (message, okCallback, cancelCallback) =>*/
export default class PopularProductSelectorModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            products:[],
            product:{},
            ascending:true,
            sortir:'p.created',
            search:"",
            page :1,
            totalPage:0,
            totalElement:0,
            pageElement:0,
        }
        this.buttonClick = this.buttonClick.bind(this)
        this.fetchAll()
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props){
            this.setState({
                modal:props.showing,
                close:props.close,
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.fetchAll()
                }
            })
        }
    }

    fetchAll = () => {
        this.get(Global.API.PRODUCTS, {
            params:{
                ascending:this.state.ascending,
                sortir:this.state.sortir,
                search:this.state.search,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    products:response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                })
            }
        }, true, true)
    }

    refreshProduct = (ascending, sortir) => (
        this.setState({
            ascending:ascending,
            sortir:sortir
        }, () => {
            this.fetchAll()
        })
    )


    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            products:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    productSelected = (e) =>{
        e.preventDefault()
        let product = this.state.product
        if(product.popularSort>0){
            this.setState({
                product:product
            },() => {
                if(this.props.selected){
                    this.props.selected(product)
                }
                this.setState({
                    product:{}
                })
            })
        }
    }

    render(){
        var products = this.state.products
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Products
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            Sort By :
                            <UncontrolledButtonDropdown key={1}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        sortirMap(this.state.sortir)
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.name"))}>Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.category.name"))}>Category Name</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.created"))}>Created</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(this.state.ascending, "p.updated"))}>Updated</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            Sortir :
                            <UncontrolledButtonDropdown key={2}>
                                <DropdownToggle
                                    caret
                                    color="white"
                                    className="text-capitalize m-1">
                                    {
                                        this.state.ascending?"Ascending":"Descending"
                                    }
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem onClick={e=>(this.refreshProduct(true, this.state.sortir))}>Ascending</DropdownItem>
                                    <DropdownItem onClick={e=>(this.refreshProduct(false, this.state.sortir))}>Descending</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledButtonDropdown>
                        </Col>
                        <Col>
                            <FormGroup>
                                <SearchInput
                                    placeholder={"Type and enter to search..."}
                                    value={this.state.search}
                                    onChange={e=>{
                                        this.setState({
                                            search:e.target.value
                                        }, () => {
                                            if(this.state.search===''){
                                                this.setState({
                                                    page:1
                                                }, () => {
                                                    this.refreshProduct(this.state.ascending, this.state.sortir)
                                                })
                                            }
                                        })
                                    }}
                                    onEnter={e=>{
                                        if(this.state.search!==''){
                                            this.setState({
                                                page:1
                                            }, () => {
                                                this.refreshProduct(this.state.ascending, this.state.sortir)
                                            })
                                        }
                                    }}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Table hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Popular</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            !isEmpty(products)?
                                products.map((item, index)=>(
                                    <tr>
                                        <th scope="row">{++index}</th>
                                        <td>{item.name}</td>
                                        <td>{item.category?item.category.name:""}</td>
                                        <td>{isEmpty(item.popularSort)?"":(<Button size="sm" color="primary">{item.popularSort}</Button>)}</td>
                                        <td>{parseDate(item.created)}</td>
                                        <td>{parseDate(item.updated)}</td>
                                        <td>
                                            <Input
                                                checked={item.selected}
                                                type="radio"
                                                onChange={e=>{
                                                    var products = this.state.products
                                                    products.map((item2, index)=>{
                                                        if(item2.id===item.id){
                                                            item2.selected = e.target.value
                                                        }else{
                                                            item2.selected = false
                                                        }
                                                    })
                                                    item.selected = e.target.value
                                                    this.setState({
                                                        products:products,
                                                        product:item
                                                    })
                                                }}/>
                                        </td>
                                    </tr>
                                ))
                                :
                                null
                        }
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalPagination {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.refreshProduct(this.state.ascending, this.state.sortir)
                    })
                }}/>

                <ModalFooter>
                    {
                        isEmpty(this.state.product.selected)?(null):(
                            <Label className="col-xl-1 align-self-center" placeholder="0">Popular </Label>
                        )
                    }
                    {
                        isEmpty(this.state.product.selected)?(null):(
                            <Input
                                value={isEmpty(this.state.product.popularSort)&&this.state.product.popularSort!=0?"":this.state.product.popularSort}
                                type="text"
                                className="col-xl-1"
                                pattern={"[0-9]*"}
                                placeholder="0"
                                onChange={e=>{
                                    let product = this.state.product
                                    product.popularSort = e.target.value.replace(/\D/,'')
                                    this.setState({
                                        product:product
                                    })
                                }}
                            />
                        )
                    }
                    {
                        isEmpty(this.state.product.selected)?(null):(
                            <Button color="primary" onClick={this.productSelected}>
                                Save
                            </Button>
                        )
                    }
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Close
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
    paginationButton = () =>{
        let button = []
        for (let i = 0; i < this.state.totalPage; i++) {
            button.push(<Button onClick={e=>{
                e.preventDefault()
                this.setState({
                    page:--i
                }, () => {
                    this.refreshProduct(this.state.ascending, this.state.sortir)
                })

            }}>{++i}</Button>)

        }
        return button
    }

}
