import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import PropTypes from 'prop-types'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import ItemOption from "../Widget/ItemOption";
import Global, {_DELIVERY} from "../../utils/Global";
import BaseComponent from "../BaseComponent";
import {currencyFormat, parseDate, statusColor} from "../../utils/Utilities";
/*= (message, okCallback, cancelCallback) =>*/
export default class VoucherInfoModal extends BaseComponent{

    constructor(props){
        super(props);
        let state = this.state
        state.modal= this.props.modal
        state.backdrop= true
        state.voucher = this.props.voucher
        state.closeCallback = this.props.closeCallback
        this.setState(state)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props.modal!=props.showing){
            this.setState({
                modal:props.modal,
                voucher: props.voucher,
                closeCallback : props.closeCallback,
            })
        }
    }

    render(){
        let voucher = this.state.voucher?this.state.voucher:{}
        let periodLabel = ""
        if(voucher.benefitType===Global._ALL_TIME){
            periodLabel = "All time"
        }else if(voucher.benefitType===Global._STATIC){
            periodLabel = voucher.validDay+" days since " +parseDate(voucher.created, 'dd-mm-yyyyy')
        }else if(voucher.benefitType===Global._DYNAMIC){
            periodLabel = parseDate(voucher.startPeriod, 'dd-mm-yyyyy')+" to "+parseDate(voucher.endPeriod, 'dd-mm-yyyyy')
        }

        let discountLabel = ""
        if(voucher.discountType===Global._PERCENTAGE_DISCOUNT){
            discountLabel = voucher.percent+" %"
        }else if(voucher.benefitType===Global._PRICE_DISCOUNT){
            discountLabel = currencyFormat(voucher.nominal, 'Rp ')
        }


        return (
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Voucher Info : {voucher&&voucher.code}
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Name</Label>
                                <Input
                                    id="name"
                                    type="text"
                                    name="name"
                                    value={voucher.name}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="code">Code</Label>
                                <Input
                                    id="code"
                                    type="text"
                                    name="code"
                                    value={voucher.code}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="description">Description</Label>
                                <Input
                                    id="description"
                                    type="text"
                                    name="description"
                                    value={voucher.description}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="period">Period</Label>
                                <Input
                                    id="description"
                                    type="text"
                                    name="description"
                                    value={periodLabel}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="benefitType">Benefit</Label>
                                <Input
                                    id="benefitType"
                                    type="text"
                                    name="benefitType"
                                    value={voucher.benefitType}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="byAggregate">Aggregate</Label>
                                <Input
                                    id="byAggregate"
                                    type="text"
                                    name="byAggregate"
                                    value={voucher.byAggregate}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="discountLabel">Discount</Label>
                                <Input
                                    id="discountLabel"
                                    type="text"
                                    name="discountLabel"
                                    value={discountLabel}
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Row>
                        <Col>
                            <Button color="secondary" onClick={e=>{
                                e.preventDefault()
                                this.setState({
                                    modal:false
                                })
                                if(this.state.closeCallback){
                                    this.state.closeCallback()
                                }
                            }}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </ModalFooter>
            </Modal>
        )
    }
}

VoucherInfoModal.propTypes = {
    modal:PropTypes.bool,
    voucher:PropTypes.object,
    closeCallback:PropTypes.func,
}
