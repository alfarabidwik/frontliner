import React from 'react';
import {Button, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Input from "reactstrap/es/Input";
import BaseComponent from "../BaseComponent";
import PropTypes from "prop-types";
import {FormLane} from "../../model/formbuilder/FormLane";
import ValidatorLabel from "./FormModal";
import WarningLabel from "../Widget/WarningLabel";

/*= (message, okCallback, cancelCallback) =>*/
export default class FormLaneModal extends BaseComponent{


    validator = {
        titleError: false
    }

    constructor(props){
        super(props);
        let formLane = new FormLane()
        formLane.id = this.props.id
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            onAdd:this.props.onAdd,
            close:this.props.close,
            formLane : formLane,
            validator : this.validator
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props.showing!=props.showing){
            let formLane = new FormLane();
            formLane.id = props.id
            this.setState({
                id:props.id,
                modal:props.showing,
                formLane : formLane,
                close:props.close,
                validator : this.validator
            })
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    onOK(){
        let formLane = this.state.formLane
        if(!formLane.title){
            let validator = this.state.validator
            validator.titleError = true
            this.setState({
                validator : validator
            })
        }else{
            if(this.props.onAdd){
                this.props.onAdd(formLane)
            }
            this.close()
        }

    }



    render(){
        let formLane = this.state.formLane
        let validator = this.state.validator
        return (
            /*xl-lg-sm*/
            <Modal size="lg" centered={true}
                isOpen={this.state.modal}
                backdrop={true}>
                {super.render()}
                <ModalHeader>
                    Create Page Form
                </ModalHeader>
                <ModalBody>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="name">Page Title</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    value={formLane.title?formLane.title:""}
                                    onChange={(e) =>{
                                        validator.titleError = false
                                        formLane.title = e.target.value
                                        this.setState({
                                            formLane:formLane,
                                            validator:validator
                                        })
                                    }}
                                    onKeyDown={e=>{
                                        if(e.key==='Enter'){
                                            e.preventDefault()
                                            this.onOK()
                                        }
                                    }}
                                    placeholder="Enter form name"
                                />
                                {
                                    validator.titleError&&(
                                        <WarningLabel message={"Mohon lengkapi form title"}/>
                                    )
                                }
                            </FormGroup>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        this.onOK()
                    }}>
                        Ok
                    </Button>{' '}
                    <Button color="secondary" onClick={e=>(this.close())}>
                        Cancel
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}

FormLaneModal.propTypes = {
    id:PropTypes.number.required,
    showing:PropTypes.bool.required,
    onAdd:PropTypes.func.required,
    close:PropTypes.func.required
}