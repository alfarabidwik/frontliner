import React from 'react';
import {Button, CardImg, Col, FormGroup, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {
    isEmpty,
} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";
import {IoMdEye} from "react-icons/io/index";
import Checkbox from "../../components/Checkbox"

/*= (message, okCallback, cancelCallback) =>*/
export default class PointInventoryModal extends BaseComponent{

    constructor(props){
        super(props);
        this.state = {
            backdrop: true,
            modal: this.props.showing,
            close:this.props.close,
            product:this.props.product,
            point:this.props.point,
            edit:this.props.edit,
            inventory:{},
            inventories:[],
            selectedAll:false,
            allStartDate:null,
            allEnddDate:null,
            modalSaveConfirm:false,
            sortir:"i.created",
            ascending:true,
        }
        this.buttonClick = this.buttonClick.bind(this)
    }

    componentWillReceiveProps(props, nextContext) {
        let modal = this.state.modal
        if(this.props!=props && props.showing){
            this.setState({
                edit:props.edit,
                modal:props.showing,
                point:props.point,
                close:props.close,
                product:props.product,
                modalSaveConfirm:false
            }, () => {
                if(props.showing && modal!==props.showing){
                    this.inventoryContentByProduct()
                }
            })
        }
    }

    inventoryContentByProduct = () =>{

        if(!isEmpty(this.state.product)){
            this.get(Global.API.POINT_UNREGISTERED_INVENTORIES, {
                params:{
                    productId:this.state.product.id,
                    pointId:this.state.point.id,
                    sortir:this.state.sortir,
                    ascending:this.state.ascending
                }
            }, null, response=>{
                if(response.code===200){
                    let inventories = response.data
                    this.setState({
                        inventories:inventories
                    })
                }
            }, true, true)
        }
    }

    buttonClick (e, button){
        e.preventDefault()
        if(button==="Ok"){
            if(this.props.okCallback!=null){
                this.props.okCallback()
            }
        }else{
            if(this.props.cancelCallback!=null){
                this.props.cancelCallback()
            }
        }
    }

    close = () =>{
        this.setState({
            inventory:{},
            inventories:[],
            modal: false,
        }, () => {
            this.props.close()
        })
    }

    overrideAllCheckbox = (selected) =>{
        let inventories = null;

        if(!isEmpty(this.state.inventories)){
            inventories = this.state.inventories
            inventories.map((item, index)=>{
                item.selected = selected
                if(item.selected){
                    this.registerInventory(item.id, item.selected)
                }else{
                    this.unregisterInventory(item.id)

                }
            })
            this.setState({
                inventories:inventories,
                selectedAll:selected,
            })
        }
    }

    registerInventory  = (inventoryId, selected) =>{
        this.get(Global.API.POINT_REGISTER_INVENTORY, {
            params:{
                pointId:this.state.point.id,
                inventoryId:inventoryId,
            }
        }, null, response=>{
            this.inventoryContentByProduct()
        }, true, true)
    }
    unregisterInventory  = (inventoryId) =>{
        this.get(Global.API.POINT_UNREGISTER_INVENTORY, {
            params:{
                inventoryId:inventoryId,
            }
        }, null, response=>{
            this.inventoryContentByProduct()
        }, true, true)
    }

    closeDialog = () =>{
        this.setState({
            modalSaveConfirm:false,
        })
    }

    render(){
        return (
            /*xl-lg-sm*/
            <Modal size="xl" centered={true}
                   isOpen={this.state.modal}
                   backdrop={true}
                   zIndex="1">
                {super.render()}
                <ModalHeader>
                    Inventory
                </ModalHeader>
                <Table hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Type</th>
                        <th>Img</th>
                        <th>S</th>
                        <th>Color</th>
                        <th>Img</th>
                        <th>Stock</th>
                        <th width="10%" ><Checkbox checked={this.state.selectedAll} onChange={selected =>this.overrideAllCheckbox(selected)}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !isEmpty(this.state.inventories)?
                            this.state.inventories.map((item, index)=>(
                                <tr key={index} valign="center">
                                    <th scope="row">{++index}</th>
                                    <td>{item.product.name}</td>
                                    <td>{item.typeName}</td>
                                    <td>
                                        <Button onClick={event=>{
                                            event.preventDefault()
                                            this.openLightBox(item.inventoryImages)
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td >{item.size?item.size.name:"-"}</td>
                                    <td>
                                        {
                                            item.colors.map((color, index1)=>(
                                                color!=null?
                                                    <div key={index1} className="float-left" style={{backgroundColor:color.hexa.toString(), borderRadius:'50%', marginRight:'5px', width:'25px', height:'25px'}}/>
                                                    :
                                                    null
                                            ))
                                        }
                                    </td>
                                    <td width="7%">
                                        <Button onClick={event=>{
                                            event.preventDefault()
                                            this.openLightBox(item.inventoryImages)
                                        }}><IoMdEye/></Button>
                                    </td>
                                    <td>{item.quantity}</td>
                                    <td width="10%">
                                        <Checkbox checked={item.point&&item.point.id && item.point.id === this.state.point.id} onChange={
                                        selected => {
                                            item.selected = selected
                                            this.setState({
                                                inventories:this.state.inventories
                                            }, () => {
                                                if(item.selected){
                                                    this.registerInventory(item.id, item.selected)
                                                }else{
                                                    this.unregisterInventory(item.id)
                                                }
                                            })
                                        }
                                        }/>
                                     </td>
                                </tr>
                            ))
                            :
                            null
                    }
                    </tbody>
                </Table>
                <ModalFooter>
                    <Button color="primary" onClick={e=>{
                        e.preventDefault()
                        this.close()
                    }}>
                        Close Dialog
                    </Button>
                </ModalFooter>
            </Modal>
        )
    }
}
