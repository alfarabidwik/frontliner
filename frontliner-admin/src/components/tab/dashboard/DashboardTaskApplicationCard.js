import React from 'react'
import BaseComponent from "../../BaseComponent";
import {_SUBMITTED, _TAKEN} from "../../../utils/Global";
import {Button, Card, CardBody, CardHeader} from "reactstrap";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import DashboardTaskApplicationTable from "./DashboardTaskApplicationTable";
import CircularProgress from '@material-ui/core/CircularProgress';
import {MdRefresh} from "react-icons/md";

let submittedTaskApplicationRef = React.createRef()
let takenTaskApplication = React.createRef()

export default class DashboardTaskApplicationCard extends BaseComponent{

    constructor(props) {
        super(props);
        let state = this.state
        state.taskApplicationStatusId = _SUBMITTED
        state.fecthing = false
        this.setState(state)
    }

    updateAnalytic=()=>{
        let taskApplicationStatusId = this.state.taskApplicationStatusId
        if(parseInt(taskApplicationStatusId)===_SUBMITTED && submittedTaskApplicationRef){
            submittedTaskApplicationRef.current.fetchTransaction(true)
        }
        if(parseInt(taskApplicationStatusId)===_TAKEN && takenTaskApplication){
            takenTaskApplication.current.fetchTransaction(true)
        }

    }

    loading=(fecthing)=>{
        this.setState({
            fecthing : fecthing
        })
    }

    render() {
        let taskApplicationStatusId = this.state.taskApplicationStatusId
        let fetching = this.state.fetching
        return (
            <Card>
                <CardHeader>
                    On Going Task
                    {
                        (fetching)?(
                            <div className="float-right" >
                                <CircularProgress color="secondary" size={24}/>
                            </div>
                        ):(
                            <div className="float-right" >
                                <Button className="float-right" onClick={event=>{
                                    this.updateAnalytic()
                                }}><MdRefresh/></Button>
                            </div>
                        )
                    }
                </CardHeader>
                <CardBody>
                    <Tabs
                        activeKey={taskApplicationStatusId}
                        onSelect={key =>{
                            this.setState({
                                taskApplicationStatusId:key
                            })
                        }}>
                        <Tab eventKey={_SUBMITTED} title="SUBMITTED">
                            <DashboardTaskApplicationTable ref={submittedTaskApplicationRef} {...this.props} show={parseInt(taskApplicationStatusId)==_SUBMITTED} taskApplicationStatusId={parseInt(taskApplicationStatusId)} fetching={(fetching)=>{
                                this.setState({fetching:fetching})
                            }}/>
                        </Tab>
                        <Tab eventKey={_TAKEN} title="TAKEN">
                            <DashboardTaskApplicationTable ref={takenTaskApplication} {...this.props} show={parseInt(taskApplicationStatusId)==_TAKEN} taskApplicationStatusId={parseInt(taskApplicationStatusId)} fetching={(fetching)=>{
                                this.setState({fetching:fetching})
                            }}/>
                        </Tab>
                    </Tabs>
                </CardBody>
            </Card>
        );
    }

}