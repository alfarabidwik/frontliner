import React from 'react'
import BaseComponent from "../../BaseComponent";
import { Bar, Line } from 'react-chartjs-2';

import {
    Badge,
    Button,
    Card,
    CardBody,
    CardDeck,
    CardGroup,
    CardHeader,
    CardTitle,
    Col, Label,
    ListGroup,
    ListGroupItem,
    Row,
} from 'reactstrap';
import {
    MdBubbleChart,
    MdInsertChart,
    MdMonetizationOn,
    MdPieChart,
    MdRefresh,
    MdShoppingCart,
    MdShowChart
} from "react-icons/md";

import { getColor } from 'utils/colors';
import DateInput from "../../Widget/DateInput";
import {IoMdBus, IoMdKey} from "react-icons/io/index";
import CircularProgress from '@material-ui/core/CircularProgress';
import Global, {CURRENCY_SYMBOL} from "../../../utils/Global";
import FormGroup from "react-bootstrap/FormGroup";
import moment from "moment";
import ItemOption from "../../Widget/ItemOption";
import {currencyFormat} from "../../../utils/Utilities";
import {TaskCategoryDto} from "../../../model/model";

let report ={
    bar: {
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Agt','Sep','Oct','Nov','Des', ],
            datasets: [
                {
                    label: 'Succeed Transaction',
                    backgroundColor: '#6a82fb',
                    stack: 'Expense',
                    data: [10000, 30000, 50000, 80000, 60000, 20000, 10000, 10000,10000,10000,10000,10000],
                },
                {
                    label: 'Failed Transaction',
                    backgroundColor: '#fc5c7d',
                    stack: 'Expense',
                    data: [30000, 80000, 50000, 100000, 60000, 40000, 90000, 20000,20000,20000,20000,20000],
                },
            ],
        },
        options: {
            title: {
                display: true,
                text: 'Task Category Interest',
            },
            tooltips: {
                mode: 'index',
                intersect: true,
            },
            hover: {
                mode: 'index',
            },
            responsive: true,
            legend: {
                display: true,
            },
            scales: {
                xAxes: [
                    {
                        stacked: true,
                        display: true,
                    },
                ],
                yAxes: [
                    {
                        stacked: true,
                        display: true,
                    },
                ],
            },
        },
    }
}

let firstDayOfMonth = new Date(new Date().getFullYear(), new Date().getMonth(), 1)
let firstDayOfYear = new Date(new Date().getFullYear(), 0, 1)

export default class DashboardCategoryAnalytic extends BaseComponent{

    constructor(props) {
        super(props);
        let state = this.state
        state.taskCategories = new Array()
        state.startDate = firstDayOfYear
        state.endDate = new Date();
        state.analytic = {}
        state.taskCategory = new TaskCategoryDto()
        this.setState(state)

    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchCategoryAnalytic()
    }

    fetchCategoryAnalytic=()=>{
        let fetching = this.state.fetching
        this.setState({
            fetching:true
        }, () => {
            this.get(Global.API.DASHBOARD_CATGORY_ANALYTIC, {
                params:{
                    startDate : this.state.startDate,
                    endDate : this.state.endDate,
                    dataId : this.state.taskCategory.id,
                }
            }, null, response=>{
                this.setState({fetching:false})
                if(response.code===200){
                    this.setState({
                        analytic:response.data
                    })
                }
            })
        })

    }


    render() {
        const primaryColor = getColor('primary');

        let taskCategories = this.state.taskCategories
        let analytic = this.state.analytic
        let fetching = this.state.fetching
        let cartData = {
            labels:[],
            datasets:[]
        }

        if(analytic.barInfos){
            analytic.barInfos.map((item, index)=>(
                cartData.labels.push(item.label)
            ))
        }

        if(analytic.datasets){
            analytic.datasets.map((item, index)=>{
                cartData.datasets.push(item)
            })
        }

        return (
            <Card>
                <CardHeader>Category </CardHeader>
                <ListGroup flush>
                    <ListGroupItem>
                        <Row>
                            <Col md={5}>
                                <Label for="startDate">Start Date</Label>
                                <DateInput
                                    id="startDate"
                                    maxdate={this.state.endDate}
                                    value={this.state.startDate}
                                    onChange={(e) =>{
                                        let value = e.target.value
                                        this.setState({
                                            startDate : value?value:firstDayOfMonth
                                        }, () => {

                                        })
                                    }}
                                    placeholder="Start Date"
                                />
                            </Col>
                            <Col md={5}>
                                <Label for="endDate">End Date</Label>
                                <DateInput
                                    id="endDate"
                                    mindate={this.state.startDate}
                                    value={this.state.endDate}
                                    onChange={(e) =>{
                                        let value = e.target.value
                                        this.setState({
                                            endDate : value?value:new Date()
                                        }, () => {

                                        })
                                    }}
                                    placeholder="End Date"
                                />
                            </Col>
                            {
                                (fetching)?(
                                    <Col md={2}>
                                        <CircularProgress  style={{marginTop:'32px'}}  color="secondary" size={24}/>
                                    </Col>
                                ):(
                                    <Col md={2}>
                                        <Button style={{marginTop:'32px'}} className="float-right" onClick={event=>{
                                            this.fetchCategoryAnalytic()
                                        }}><MdRefresh/></Button>
                                    </Col>
                                )
                            }
                        </Row>
                    </ListGroupItem>
                </ListGroup>
                <CardBody>
                    <Bar data={cartData} options={report.bar.options} />
                </CardBody>
                <ListGroup flush>
                    <ListGroupItem>
                        <MdMonetizationOn size={25} color={primaryColor} /> Balance of All Users{' '}
                        <Badge style={{backgroundColor:'#2026ff'}} >{currencyFormat(analytic.allUserIncomeAmount, CURRENCY_SYMBOL)}</Badge>
                    </ListGroupItem>
                    <ListGroupItem>
                        <MdShoppingCart size={25} color={primaryColor} /> Paid Amount Of All Users {' '}
                        <Badge color="green">{currencyFormat(analytic.allUserWithdrawAmount, CURRENCY_SYMBOL)}</Badge>
                    </ListGroupItem>
                    {/*<ListGroupItem>*/}
                        {/*<MdBubbleChart size={25} color={primaryColor} /> Bubble Wrap Amount{' '}*/}
                        {/*<Badge color="yellow">{currencyFormat(analytic.bubbleWrapAmount, CURRENCY_SYMBOL)}</Badge>*/}
                    {/*</ListGroupItem>*/}
                    {/*<ListGroupItem>*/}
                        {/*<IoMdBus size={25} color={primaryColor} /> Courier Amount{' '}*/}
                        {/*<Badge color="purple">{currencyFormat(analytic.courierAmount, CURRENCY_SYMBOL)}</Badge>*/}
                    {/*</ListGroupItem>*/}
                    {/*<ListGroupItem>*/}
                        {/*<IoMdKey size={25} color={primaryColor} /> Unique Price {' '}*/}
                        {/*<Badge color="secondary">{currencyFormat(analytic.uniqueAmount, CURRENCY_SYMBOL)}</Badge>*/}
                    {/*</ListGroupItem>*/}
                </ListGroup>
            </Card>
        );
    }


}