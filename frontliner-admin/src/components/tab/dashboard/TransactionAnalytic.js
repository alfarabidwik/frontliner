import React from 'react'
import BaseComponent from "../../BaseComponent";
import { Bar, Line } from 'react-chartjs-2';

import {
    Badge,
    Button,
    Card,
    CardBody,
    CardDeck,
    CardGroup,
    CardHeader,
    CardTitle,
    Col, Label,
    ListGroup,
    ListGroupItem,
    Row,
} from 'reactstrap';
import {
    MdBubbleChart,
    MdInsertChart,
    MdMonetizationOn,
    MdPieChart,
    MdRefresh,
    MdShoppingCart,
    MdShowChart
} from "react-icons/md";

import { getColor } from 'utils/colors';
import DateInput from "../../Widget/DateInput";
import {IoMdBus, IoMdKey} from "react-icons/io/index";
import CircularProgress from '@material-ui/core/CircularProgress';
import Global, {CURRENCY_SYMBOL} from "../../../utils/Global";
import FormGroup from "react-bootstrap/FormGroup";
import moment from "moment";
import ItemOption from "../../Widget/ItemOption";
import {currencyFormat} from "../../../utils/Utilities";

let report ={
    bar: {
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Agt','Sep','Oct','Nov','Des', ],
            datasets: [
                {
                    label: 'Succeed Transaction',
                    backgroundColor: '#6a82fb',
                    stack: 'Expense',
                    data: [10000, 30000, 50000, 80000, 60000, 20000, 10000, 10000,10000,10000,10000,10000],
                },
                {
                    label: 'Failed Transaction',
                    backgroundColor: '#fc5c7d',
                    stack: 'Expense',
                    data: [30000, 80000, 50000, 100000, 60000, 40000, 90000, 20000,20000,20000,20000,20000],
                },
            ],
        },
        options: {
            title: {
                display: false,
                text: 'Chart.js Bar Chart - Stacked',
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            responsive: true,
            legend: {
                display: true,
            },
            scales: {
                xAxes: [
                    {
                        stacked: true,
                        display: false,
                    },
                ],
                yAxes: [
                    {
                        stacked: true,
                        display: false,
                    },
                ],
            },
        },
    },
    line: {
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [
                {
                    label: 'Revenue for this year',
                    borderColor: '#6a82fb',
                    backgroundColor: '#6a82fb',
                    data: [0, 1300, 2200, 3400, 4600, 3500, 3000],
                },

                {
                    label: 'Revenue for last year',
                    borderColor: '#fc5c7d',
                    backgroundColor: '#fc5c7d',
                    data: [0, 1300, 2200, 3400, 4600, 3500, 3000],
                },
            ],
        },
        options: {
            responsive: true,
            legend: {
                display: false,
            },
            title: {
                display: false,
                text: 'Chart.js Line Chart - Stacked Area',
            },
            tooltips: {
                intersect: false,
                mode: 'nearest',
            },
            hover: {
                mode: 'index',
            },
            scales: {
                xAxes: [
                    {
                        scaleLabel: {
                            display: false,
                            labelString: 'Month',
                        },
                        gridLines: {
                            display: false,
                        },
                    },
                ],
                yAxes: [
                    {
                        stacked: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value',
                        },
                        gridLines: {
                            display: false,
                        },
                    },
                ],
            },
        },
    },

}

let firstDayOfMonth = new Date().getFullYear()+'-01-01'// moment().startOf("year")//new Date(new Date().getFullYear(), 0)

export default class TransactionAnalytic extends BaseComponent{

    constructor(props) {
        super(props);
        let state = this.state
        state.startDate = firstDayOfMonth
        state.endDate = new Date();
        state.analytic = {}
        state.criteria = {
            id:1,
            name:'Heading'
        }
        this.setState(state)

    }

    componentDidMount() {
        super.componentDidMount();
        this.fetchTransactionAnalytic()
    }

    fetchTransactionAnalytic=()=>{
        let fetching = this.state.fetching
        this.setState({
            fetching:true
        }, () => {
            this.get(Global.API.DASHBOARD_TRANSACTION_ANALYTIC, {
                params:{
                    startDate : this.state.startDate,
                    endDate : this.state.endDate,
                }
            }, null, response=>{
                this.setState({fetching:false})
                if(response.code===200){
                    this.setState({
                        analytic:response.data
                    })
                }
            })
        })

    }

    render() {
        const primaryColor = getColor('primary');
        let analytic = this.state.analytic
        let fetching = this.state.fetching
        let cartData = {
            labels:[],
            datasets:[]
        }

        if(analytic.barInfos){
            analytic.barInfos.map((item, index)=>(
                cartData.labels.push(item.label)
            ))
        }

        if(analytic.datasets){
            analytic.datasets.map((item, index)=>{
                item.fill = false
                cartData.datasets.push(item)
            })
        }

        return (
            <Card>
                <CardHeader>
                    Total Quantity{' '}
                    <small className="text-muted text-capitalize">This year</small>
                </CardHeader>
                <ListGroup flush>
                    <ListGroupItem>
                        <Row>
                            <Col md={5}>
                                <Label for="startDate">Start Date</Label>
                                <DateInput
                                    id="startDate"
                                    maxdate={this.state.endDate}
                                    value={this.state.startDate}
                                    onChange={(e) =>{
                                        let value = e.target.value
                                        this.setState({
                                            startDate : value?value:firstDayOfMonth
                                        }, () => {

                                        })
                                    }}
                                    placeholder="Start Date"
                                />
                            </Col>
                            <Col md={5}>
                                <Label for="endDate">End Date</Label>
                                <DateInput
                                    id="endDate"
                                    mindate={this.state.startDate}
                                    value={this.state.endDate}
                                    onChange={(e) =>{
                                        let value = e.target.value
                                        this.setState({
                                            endDate : value?value:new Date()
                                        }, () => {

                                        })
                                    }}
                                    placeholder="End Date"
                                />
                            </Col>
                            {
                                (fetching)?(
                                    <Col md={2}>
                                        <CircularProgress  style={{marginTop:'32px'}}  color="secondary" size={24}/>
                                    </Col>
                                ):(
                                    <Col md={2}>
                                        <Button style={{marginTop:'32px'}} className="float-right" onClick={event=>{
                                            this.fetchTransactionAnalytic()
                                        }}><MdRefresh/></Button>
                                    </Col>
                                )
                            }
                        </Row>
                    </ListGroupItem>
                </ListGroup>
                <CardBody>
                    <Line data={cartData} />
                </CardBody>
            </Card>
        );
    }


}