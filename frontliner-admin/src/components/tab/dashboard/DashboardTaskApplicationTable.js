import React from 'react'
import BasePage from "../../../pages/BasePage";
import userAvatar from "../../../assets/img/users/user_avatar.png"
import PropTypes from 'prop-types'
import Global, {CURRENCY_SYMBOL, DD_MM_YYYY_HH_MM_SS, PRINT_DATA} from "../../../utils/Global";
import {Button, Modal, Table} from "reactstrap";
import {MdEdit, MdPersonPin, MdPrint} from "react-icons/md";
import withBadge from 'hocs/withBadge';
import {currencyFormat, parseDate, isEmpty} from "../../../utils/Utilities";
import Avatar from 'components/Avatar';
import {MdDetails} from "react-icons/md/index";
import CardPagination from "../../CardPagination";
import BaseComponent from "../../BaseComponent";
import {storeData} from "../../../utils/StorageUtil";

const AvatarWithBadge = withBadge({
    position: 'bottom-right',
    color: 'success',
})(Avatar);


export default class DashboardTaskApplicationTable extends BaseComponent{

    constructor(props) {
        super(props);
        let state = this.state
        state.page = 1 ;
        state.taskApplicationStatusId = this.props.taskApplicationStatusId
        state.taskApplications = []
        state.show = this.props.show
        state.fetching = this.props.fetching
        state.totalPage  = 0
        state.totalElement = 0
        state.pageElement = 0
        this.setState(state)
    }

    componentDidMount() {
        super.componentDidMount();
        if(this.state.show){
            this.fetchTransaction(true)
        }
        // this.fetchTransaction(true)
    }

    componentWillReceiveProps(props, nextContext) {
        let taskApplicationStatus = this.state.taskApplicationStatus
        let show = this.state.show
        if(show===false && props.show===true){
            let state = this.state
            state.page = 1
            state.taskApplicationStatusId = props.taskApplicationStatusId
            state.taskApplications = []
            state.show = props.show
            state.fetching = props.fetching
            this.setState(state, () => {
                this.fetchTransaction(true)
            })
        }else{
            let state = this.state
            state.show = props.show
            this.setState(state)
        }
    }


    fetchTransaction = (progressing) =>{
        let fetching = this.state.fetching
        if(fetching && progressing){
            fetching(true)
        }
        this.get(Global.API.TASK_APPLICATIONS, {
            params:{
                taskApplicationStatusId:this.state.taskApplicationStatusId,
                page:this.state.page-1
            }
        }, null, response=>{
            if(response.code===200){
                this.setState({
                    taskApplications : response.data,
                    totalPage: response.totalPage,
                    totalElement: response.totalElement,
                    pageElement: response.pageElement,
                }, () => {
                    fetching(false)
                })
            }else{
                fetching(false)
            }
        }, false, true)
    }

    print =(id) =>{
        let thisWindow = window
        this.get(Global.API.TRANSACTION, {
            params :{
                id : id
            }
        }, null, response =>{
            if(response.code===200){
                let taskApplication = response.data
                storeData(PRINT_DATA, taskApplication)
                let printPageWindow = thisWindow.open('/printInvoice', '_blank')
                if(printPageWindow){
                    printPageWindow.addEventListener('DOMContentLoaded', (event) => {
                        setInterval(()=>{
                            printPageWindow.postMessage("PRINT", "*")
                        }, 300)
                    });
                }
            }
        }, true, true)
    }


    render() {
        let taskApplications = this.state.taskApplications
        return (
            <span>
                {
                    super.render()
                }
            <Table responsive hover {...this.props}>
                <thead>
                <tr className="text-capitalize align-middle text-center">
                    <th><MdPersonPin size={25}/></th>
                    <th>Name</th>
                    <th>Agent</th>
                    <th>Task</th>
                    <th>Date</th>
                    <th>Detail</th>
                </tr>
                </thead>
                <tbody>
                {
                    taskApplications.map((item, index)=>(
                        <tr key={index}>
                            <td className="align-middle text-center">
                                <AvatarWithBadge src={isEmpty(item.user.photo)?userAvatar:item.user.photoUrl} onError={
                                    this.addDefaultUserAvatar
                                }/>
                            </td>
                            <td className="align-middle text-center">{item.user.fullname}</td>
                            <td className="align-middle text-center">{item.user.agentCode}</td>
                            <td className="align-middle text-center">{item.task.title}</td>
                            <td className="align-middle text-center">{parseDate(item.created, DD_MM_YYYY_HH_MM_SS)}</td>
                            <td className="align-middle text-center">
                                <Button size="sm" color="primary" onClick={ e => (
                                    this.props.history.push('/taskApplicationDetail?id='+item.id)
                                )}>
                                    <MdDetails/>
                                </Button>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </Table>
                <CardPagination  className="float-right" {...this.props} currentPage={this.state.page} pageCount={this.state.totalPage} onSelect={(page)=>{
                    this.setState({
                        page:page
                    }, () => {
                        this.fetchTransaction(true)
                    })
                }}/>
            </span>
        );
    }

}

DashboardTaskApplicationTable.propTypes = {
    taskApplicationStatusId : PropTypes.number.isRequired,
    show:PropTypes.bool,
    fetching:PropTypes.func.isRequired
}