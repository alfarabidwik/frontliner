import PropTypes from 'prop-types';
import React from 'react';
import {Button, Form, FormGroup, Input, Label} from 'reactstrap';
import {AuthConsumer} from "../utils/AuthContext";
import Global from "../utils/Global";
import BaseComponent from "./BaseComponent";
import {getData, storeData} from "../utils/StorageUtil";
import Img from 'react-image'
import catchadealLogo from 'assets/img/logo/logo_kerjayuk_color.png'


class AuthForm extends BaseComponent {

  constructor(props, context) {
    super(props, context);
    this.handleSubmit = this.handleSubmit.bind(this);
    let state = this.state
    state.email = Global.DEV_LOGIN_USERNAME
    state.password = Global.DEV_LOGIN_PASSWORD
    state.configuration = getData(Global.CONFIGURATION)
  }

  componentDidMount() {
    super.componentDidMount();
    this.fetchAppSetting()
  }

  get isLogin() {
    return this.props.authState === STATE_LOGIN;
  }

  get isSignup() {
    return this.props.authState === STATE_SIGNUP;
  }

  changeAuthState = authState => event => {
    event.preventDefault();

    this.props.onChangeAuthState(authState);
  };

  handleSubmit = (event, authProps) => {
    event.preventDefault();
    if(this.isLogin){
      this.post(Global.API.LOGIN, null, {
        params:{
          email:this.state.email,
          password:this.state.password
        }
      }, response=>{
        if(response.code===200){
          const admin = response.data ;
          storeData(Global.AUTHORIZATION, response.data.authorization)
          authProps.login(admin)
        }
      }, true, true)
    }
  };

  renderButtonText() {
    const { buttonText } = this.props;

    if (!buttonText && this.isLogin) {
      return 'Login';
    }

    if (!buttonText && this.isSignup) {
      return 'Signup';
    }

    return buttonText;
  }

  fetchAppSetting=()=>{
    this.get(Global.API.CONFIGURATION_CURRENT_ACTIVE, null, null, response=>{
      if(response.code===200){
        let configuration = response.data
        storeData(Global.CONFIGURATION, configuration)
        this.setState({
          configuration:configuration
        })
      }
    })
  }


  render() {
    const {
      showLogo,
      usernameLabel,
      usernameInputProps,
      passwordLabel,
      passwordInputProps,
      confirmPasswordLabel,
      confirmPasswordInputProps,
      children,
      onLogoClick,
    } = this.props;

    let configuration = this.state.configuration
    if(!configuration){
      configuration = {}
    }


    const baseRender = super.render();
    return (
          <AuthConsumer>
            {
              (authProps) => (
                  <div>
                    {baseRender}
                    {showLogo && (
                        <div className="text-center pb-4" style={{marginTop:'50px', paddingTop:'20px'}}>
                          <Img
                              src={catchadealLogo}
                              className="rounded"
                              style={{width:'70%', height:'30%'}}
                              alt="logo"
                              onClick={onLogoClick}
                          />
                        </div>
                    )}
                    <Form onSubmit={e=>this.handleSubmit(e,authProps)}>
                      <br/>
                      <FormGroup>
                        <Label for={usernameLabel}>{usernameLabel}</Label>
                        <Input {...usernameInputProps} value={this.state.email} onChange={e=>{this.setState({email:e.target.value})}}/>
                      </FormGroup>
                      <FormGroup>
                        <Label for={passwordLabel}>{passwordLabel}</Label>
                        <Input {...passwordInputProps} value={this.state.password} onChange={e=>{this.setState({password:e.target.value})}}  onKeyDown={e=>{
                          // alert(e.key)
                          if(e.key==='Enter'){
                            e.preventDefault()
                            this.handleSubmit(e, authProps)
                          }
                        }} />
                      </FormGroup>
                      {this.isSignup && (
                          <FormGroup>
                            <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
                            <Input {...confirmPasswordInputProps} value={this.state.password} onChange={e=>{this.setState({password:e.value})}}/>
                          </FormGroup>
                      )}
                      <hr />
                      <Button
                          size="lg"
                          block
                          color={"secondary"}
                          onClick={e=>this.handleSubmit(e, authProps)}>
                        {this.renderButtonText()}
                      </Button>
                      {children}
                    </Form>

                  </div>
              )
            }
          </AuthConsumer>
    );
  }
}

export const STATE_LOGIN = 'LOGIN';
export const STATE_SIGNUP = 'SIGNUP';

AuthForm.propTypes = {
  authState: PropTypes.oneOf([STATE_LOGIN, STATE_SIGNUP]).isRequired,
  showLogo: PropTypes.bool,
  usernameLabel: PropTypes.string,
  usernameInputProps: PropTypes.object,
  passwordLabel: PropTypes.string,
  passwordInputProps: PropTypes.object,
  confirmPasswordLabel: PropTypes.string,
  confirmPasswordInputProps: PropTypes.object,
  onLogoClick: PropTypes.func,
};

AuthForm.defaultProps = {
  authState: 'LOGIN',
  showLogo: true,
  usernameLabel: 'Email',
  usernameInputProps: {
    type: 'email',
    placeholder: 'your@email.com',
  },
  passwordLabel: 'Password',
  passwordInputProps: {
    type: 'password',
    placeholder: 'your password',
  },
  confirmPasswordLabel: 'Confirm Password',
  confirmPasswordInputProps: {
    type: 'password',
    placeholder: 'confirm your password',
  },
  onLogoClick: () => {},
};

export default AuthForm;
