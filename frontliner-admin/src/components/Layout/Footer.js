import React from 'react';

import { Navbar, Nav, NavItem } from 'reactstrap';

import SourceLink from 'components/SourceLink';
import Global from "../../utils/Global";

const Footer = () => {
  return (
    <Navbar>
      <Nav navbar>
        <NavItem>
            2020 <span className="copyleft" style={{display:'inline-block', transform:'rotate(180deg)'}}>&copy;</span> <a href="https://kerjayuk.id" target="_blank">Kerjayuk</a>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default Footer;
