import sidebarBgImage from 'assets/img/sidebar/sidebar-4.jpg';
import catchadealLogo from 'assets/img/logo/ic_logo_kerjayuk_white.png'
import SourceLink from 'components/SourceLink';
import React from 'react';
import {
  MdAccountCircle,
  MdArrowDropDownCircle,
  MdBorderAll,
  MdBrush,
  MdChromeReaderMode,
  MdDashboard,
  MdGroupWork,
  MdInsertChart,
  MdNotificationsActive,
  MdRadioButtonChecked,
  MdStar,
  MdTextFields,
  MdViewCarousel,
  MdViewDay,
  MdViewList,
  MdWeb,
  MdWidgets,
} from 'react-icons/md';
import {NavLink} from 'react-router-dom';
import {Collapse, Nav, Navbar, NavItem, NavLink as BSNavLink,} from 'reactstrap';
import bn from 'utils/bemnames';
import {AuthConsumer} from "../../utils/AuthContext";
import {IoMdOpen} from "react-icons/io";
import {getData} from "../../utils/StorageUtil";
import Global from "../../utils/Global";
import Img from 'react-image'

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};


const navComponents = [
  { to: '/buttons', name: 'buttons', exact: false, Icon: MdRadioButtonChecked },
  {
    to: '/button-groups',
    name: 'button groups',
    exact: false,
    Icon: MdGroupWork,
  },
  { to: '/forms', name: 'forms', exact: false, Icon: MdChromeReaderMode },
  { to: '/input-groups', name: 'input groups', exact: false, Icon: MdViewList },
  {
    to: '/dropdowns',
    name: 'dropdowns',
    exact: false,
    Icon: MdArrowDropDownCircle,
  },
  { to: '/badges', name: 'badges', exact: false, Icon: MdStar },
  { to: '/alerts', name: 'alerts', exact: false, Icon: MdNotificationsActive },
  { to: '/progress', name: 'progress', exact: false, Icon: MdBrush },
  { to: '/modals', name: 'modals', exact: false, Icon: MdViewDay },
];

const navContents = [
  { to: '/typography', name: 'typography', exact: false, Icon: MdTextFields },
  { to: '/tables', name: 'tables', exact: false, Icon: MdBorderAll },
];

const pageContents = [
  { to: '/login', name: 'login / signup', exact: false, Icon: MdAccountCircle },
  {
    to: '/login-modal',
    name: 'login modal',
    exact: false,
    Icon: MdViewCarousel,
  },
];

const navItems = [
  { to: '/', name: 'dashboard', exact: true, Icon: MdDashboard },
  { to: '/cards', name: 'cards', exact: false, Icon: MdWeb },
  { to: '/charts', name: 'charts', exact: false, Icon: MdInsertChart },
  { to: '/widgets', name: 'widgets', exact: false, Icon: MdWidgets },
];

const bem = bn.create('sidebar');

class Sidebar extends React.Component {
  state = {
    // isOpenProduct: true,
    isOpenComponents: true,
    isOpenContents: true,
    isOpenPages: true,
  };

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  render() {
    let configuration = getData(Global.CONFIGURATION)
    if(configuration===null){
      configuration = {}
    }


    return (
        <AuthConsumer>
          {
            ({admin}) => (
                <aside className={bem.b()} data-image={sidebarBgImage}>
                  <div className={bem.e('background')} style={sidebarBackground} />
                  <div className={bem.e('content')}>
                    <br/>
                    <Navbar>
                      <SourceLink>
                        <Img
                            src={catchadealLogo}
                            width="100%"
                            alt="Logo"
                        />
                        {/*<span className="text-white center">{configuration.companyName}</span>*/}
                      </SourceLink>
                    </Navbar>
                    <Nav vertical>
                      {
                        admin.role.treeMenus.map(({name, childMenus, link}, index)=>(
                            childMenus.length>0?(
                            <span key={index}>
                            <NavItem className={bem.e('nav-item')} onClick={event=>{
                              event.preventDefault()
                              this.setState({
                                ['isOpen'+name]:this.state['isOpen'+name]===undefined?true:!this.state['isOpen'+name]
                              })
                            }}>
                              <BSNavLink className={bem.e('nav-item-collapse')}>
                                <div className="d-flex">
                                  {/*<MdExtension className={bem.e('nav-item-icon')} />*/}
                                  <span className="align-self-start">{name.toUpperCase()}</span>
                                </div>
                                <MdArrowDropDownCircle
                                    className={bem.e('nav-item-icon')}
                                    style={{
                                      padding: 0,
                                      transform: this.state['isOpen'+name]
                                          ? 'rotate(0deg)'
                                          : 'rotate(-90deg)',
                                      transitionDuration: '0.3s',
                                      transitionProperty: 'transform',
                                    }}/>
                              </BSNavLink>
                            </NavItem>
                              {
                                <Collapse key={index} isOpen={this.state['isOpen'+name]}>
                                  {childMenus.map((item, index1) => (
                                      <NavItem key={index1} className={bem.e('nav-item')}>
                                        <BSNavLink
                                            id={`navItem-${item.name}-${index1}`}
                                            tag={NavLink}
                                            to={item.link}
                                            activeClassName="active"
                                            exact={true}
                                        >
                                          {/*<Icon className={bem.e('nav-item-icon')} />*/}
                                          <span className="">&nbsp;&nbsp;{item.name}</span>
                                        </BSNavLink>
                                      </NavItem>
                                  ))}
                                </Collapse>
                              }
                              </span>
                            ):(
                                <NavItem key={index} className={bem.e('nav-item')}>
                                  <BSNavLink
                                      id={`navItem-${name}-${index}`}
                                      className="text-uppercase"
                                      tag={NavLink}
                                      to={link}
                                      activeClassName="active"
                                      exact={true}
                                  >
                                    <IoMdOpen className={bem.e('nav-item-icon')} />
                                    <span className="">{name}</span>
                                  </BSNavLink>
                                </NavItem>
                            )
                        ))
                      }
                    </Nav>
                  </div>
                </aside>
            )
          }
        </AuthConsumer>
    );
  }
}

export default Sidebar;
