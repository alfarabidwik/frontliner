import Avatar from 'components/Avatar';
import { UserCard } from 'components/Card';
import Notifications from 'components/Notifications';
import SearchInput from 'components/SearchInput';
import { notificationsData } from 'demos/header';
import withBadge from 'hocs/withBadge';
import React from 'react';
import {
  MdClearAll,
  MdExitToApp,
  MdHelp,
  MdInsertChart,
  MdMessage,
  MdNotificationsActive,
  MdNotificationsNone,
  MdPersonPin,
  MdSettingsApplications,
} from 'react-icons/md';
import {
  Button,
  ListGroup,
  ListGroupItem,
  // NavbarToggler,
  Nav,
  Navbar,
  NavItem,
  NavLink,
  Popover,
  PopoverBody,
} from 'reactstrap';
import bn from 'utils/bemnames';
import {AuthConsumer} from "./../../utils/AuthContext"
import userImage from '../../assets/img/users/user_avatar.png';
import {parseDate} from "../../utils/Utilities";
import BaseComponent from "../BaseComponent";
import Global from "../../utils/Global";


const bem = bn.create('header');

const MdNotificationsActiveWithBadge = withBadge({
  size: 'md',
  color: 'primary',
  style: {
    top: -10,
    right: -10,
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  children: <small>5</small>,
})(MdNotificationsActive);

class Header extends BaseComponent {

  constructor(props) {
    super(props);
    let state = this.state
    state.isOpenNotificationPopover = false
    state.isNotificationConfirmed = false
    state.isOpenUserCardPopover = false
    this.setState(state)
  }


  toggleNotificationPopover = () => {
    this.setState({
      isOpenNotificationPopover: !this.state.isOpenNotificationPopover,
    });

    if (!this.state.isNotificationConfirmed) {
      this.setState({ isNotificationConfirmed: true });
    }
  };

  toggleUserCardPopover = () => {
    this.setState({
      isOpenUserCardPopover: !this.state.isOpenUserCardPopover,
    });
  };

  handleSidebarControlButton = event => {
    event.preventDefault();
    event.stopPropagation();

    document.querySelector('.cr-sidebar').classList.toggle('cr-sidebar--open');
  };


  // parseDate = (dateString) =>{
  //   const date = new Date(dateString)
  //   return new Intl.DateTimeFormat().format(date);
  // }

  render() {
    const { isNotificationConfirmed } = this.state;
    return (
        <AuthConsumer>
          {({admin, logout})=>(
              <Navbar light expand className={bem.b('bg-white')}>
                {super.render()}
                <Nav navbar className="mr-2">
                  <Button outline onClick={this.handleSidebarControlButton}>
                    <MdClearAll size={25} />
                  </Button>
                </Nav>
                <Nav navbar>
                  {/*<SearchInput />*/}
                </Nav>

                <Nav navbar className={bem.e('nav-right')}>
                  {/*<NavItem className="d-inline-flex">*/}
                    {/*<NavLink id="Popover1" className="position-relative">*/}
                      {/*{isNotificationConfirmed ? (*/}
                          {/*<MdNotificationsNone*/}
                              {/*size={25}*/}
                              {/*className="text-secondary can-click"*/}
                              {/*onClick={this.toggleNotificationPopover}*/}
                          {/*/>*/}
                      {/*) : (*/}
                          {/*<MdNotificationsActiveWithBadge*/}
                              {/*size={25}*/}
                              {/*className="text-secondary can-click animated swing infinite"*/}
                              {/*onClick={this.toggleNotificationPopover}*/}
                          {/*/>*/}
                      {/*)}*/}
                    {/*</NavLink>*/}
                    {/*<Popover*/}
                        {/*placement="bottom"*/}
                        {/*isOpen={this.state.isOpenNotificationPopover}*/}
                        {/*toggle={this.toggleNotificationPopover}*/}
                        {/*target="Popover1"*/}
                    {/*>*/}
                      {/*<PopoverBody>*/}
                        {/*<Notifications notificationsData={notificationsData} />*/}
                      {/*</PopoverBody>*/}
                    {/*</Popover>*/}
                  {/*</NavItem>*/}

                  <NavItem>
                    <NavLink id="Popover2">
                      <Avatar
                          src={admin.image&&admin.image!=''?admin.imageLink:userImage}
                          onClick={this.toggleUserCardPopover}
                          className="can-click"
                      />
                    </NavLink>
                    <Popover
                        placement="bottom-end"
                        isOpen={this.state.isOpenUserCardPopover}
                        toggle={this.toggleUserCardPopover}
                        target="Popover2"
                        className="p-0 border-0"
                        style={{ minWidth: 250 }}>
                      <PopoverBody className="p-0 border-light">
                        <UserCard
                            avatar={admin.image&&admin.image!=''?admin.imageLink:userImage}
                            title={admin.firstname+" "+admin.lastname}
                            subtitle={admin.email}
                            text={"Last updated "+parseDate(admin.updated, 'dd-mmmm-yyyy')}
                            className="border-light"
                            onClickAvatar={()=>{

                            }}
                        >
                          <ListGroup flush>
                            <ListGroupItem tag="button" action className="border-light" onClick={event=>{
                              event.preventDefault()
                              this.setState({
                                isOpenUserCardPopover:false
                              }, () => {
                                this.props.history.push("/profile")
                              })
                            }}>
                              <MdPersonPin /> Profile
                            </ListGroupItem>

                            {/*<ListGroupItem tag="button" action className="border-light">
                              <MdInsertChart /> Stats
                            </ListGroupItem>
                            <ListGroupItem tag="button" action className="border-light">
                              <MdMessage /> Messages
                            </ListGroupItem>
                            <ListGroupItem tag="button" action className="border-light">
                              <MdSettingsApplications /> Settings
                            </ListGroupItem>*/}

                            <ListGroupItem tag="button" action className="border-light">
                              <a href="https://eguide.numeralasia.com" target="_blank"  onClick={event=>{
                                this.setState({
                                  isOpenUserCardPopover:false
                                })
                              }}>
                                <MdHelp /> Help
                              </a>
                            </ListGroupItem>
                            <ListGroupItem tag="button" action className="border-light"  onClick={event=>{
                              event.preventDefault()
                              this.setState({
                                isOpenUserCardPopover:false
                              }, () => {
                                this.openConfirmDialog("Konfirmasi", "Apakah anda akan keluar dari aplikasi ? ", ()=>{
                                  this.get(Global.API.LOGOUT, null, null, response=>{
                                    logout()
                                  }, true, true)
                                }, ()=>{})
                              })
                            }}>
                              <MdExitToApp/> Signout
                            </ListGroupItem>
                          </ListGroup>
                        </UserCard>
                      </PopoverBody>
                    </Popover>
                  </NavItem>
                </Nav>
              </Navbar>
          )
          }
        </AuthConsumer>
    );
  }
}

export default Header;
