import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { AuthConsumer } from '../../utils/AuthContext'

const ProtectedRoute = ({ component: Component, layout: Layout, ...rest }) => (
    <AuthConsumer>
        {({ isAuth }) => (
            <Route
                render={props =>
                    isAuth ?
                        <Layout {...props}>
                            <Component {...props} query={props.location.search} />
                        </Layout>
                        :
                        <Redirect to="/login" />
                }
                {...rest}
            />
        )}
    </AuthConsumer>
)

export default ProtectedRoute