import BaseComponent from "./BaseComponent";
import React from 'react'
import {MdCheck} from "react-icons/md";
import {Button} from "reactstrap";

let checkedBgColor = "#f52119"
let unCheckedBgColor = "#d4d8ee"

let checkedColor = ""
let unCheckedColor = ""


export default class Checkbox extends BaseComponent{
    constructor(props) {
        super(props);
        this.state ={
            checked:this.props.checked
        }
    }

    componentWillReceiveProps(props, nextContext) {
        if(this.props!=props){
            this.setState({
                checked:props.checked
            })
        }
    }

    render() {
        let finalBgColor = this.state.checked?checkedBgColor:unCheckedBgColor
        return (
            <Button
                color="none"
                style={{backgroundColor:finalBgColor}}
                onClick={
                    e=>{
                        e.preventDefault()
                        this.setState({
                            checked:!this.state.checked
                        },() => {
                            if(this.props.onChange){
                                this.props.onChange(this.state.checked)
                            }
                        })
                    }
                }>
                <MdCheck color="white"/>
            </Button>
        );
    }

}