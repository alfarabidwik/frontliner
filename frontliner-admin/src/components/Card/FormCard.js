import * as React from "react";
import Card from "reactstrap/es/Card";
import Col from "reactstrap/es/Col";
import CardBody from "reactstrap/es/CardBody";
import Row from "reactstrap/es/Row";
import {MdDelete} from "react-icons/md";


export default class FormCard extends React.Component{
    props ;

    constructor(props) {
        super(props);
    }

    render() {
        let form = this.props.form
        return (
            <div style={{width:"400px", marginBottom:"20px"}}>
                <Card>
                    <CardBody>
                        <Row>
                            <Col>
                                Tag
                            </Col>
                            <Col>
                                <div>{form.tag}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                Mandatory
                            </Col>
                            <Col>
                                <div>
                                    {form.mandatory?"True":"False"}
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <div>Title</div>
                            </Col>
                            <Col>
                                <div>{form.title}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                Type
                            </Col>
                            <Col>
                                <div>{form.type}</div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <MdDelete onClick={(e)=>{
                                    if(this.props.cardBag){
                                        this.props.cardBag.removeCard()
                                    }
                                }}/>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </div>
        );
    }
}
