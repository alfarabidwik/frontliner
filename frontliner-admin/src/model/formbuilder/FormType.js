export class FormType{
    static TEXT  = "TEXT"
    static EMAIL  = "EMAIL"
    static NUMBER  = "NUMBER"
    static PHONE  = "PHONE"
    static CURRENCY  = "CURRENCY"
    static DATE  = "DATE"
    // static DROPDOWN : string = "DROPDOWN"
    static DROPDOWN_API_DATA  = "DROPDOWN_API_DATA"
    static IMAGE  = "IMAGE"
}