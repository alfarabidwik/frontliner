/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.19.577 on 2020-02-24 15:38:40.

export class EBaseDto implements Serializable {
    id: number;
    created: Date;
    createdBy: number;
    updated: Date;
    updatedBy: number;
    active: boolean;
}

export class WSResponse<D, E> {
    code: number;
    message: string;
    messageError: E;
    data: D;
    totalElement: number;
    totalPage: number;
    pageElement: number;
    success: boolean;
}

export class AdminDashboard extends EBaseDto {
    totalUser: number;
    thisMonthUser: number;
    previousMonthUser: number;
    totalTask: number;
    thisMonthTask: number;
    previousMonthTask: number;
    totalTaskDone: number;
    thisMonthTaskDone: number;
    previousMonthTaskDone: number;
    totalPayment: number;
    thisMonthPayment: number;
    previousMonthPayment: number;
    thisMonthTaskDonePercent: number;
    thisMonthUserPercent: number;
    thisMonthPaymentPercent: number;
    thisMonthTaskPercent: number;
}

export class BarInfo {
    id: number;
    label: string;
    startDate: Date;
    endDate: Date;
}

export class DashboardCategoryAnalytic {
    barInfos: BarInfo[];
    datasets: DataSet<number>[];
    allUserIncomeAmount: number;
    allUserWithdrawAmount: number;
}

export class DataSet<OBJECT> {
    label: string;
    backgroundColor: string;
    borderColor: string;
    stack: string;
    data: OBJECT[];
}

export class FormApplicationDto extends EBaseDto {
    formLaneApplication: FormLaneApplicationDto;
    valueId: number;
    form: Form;
    value: string;
    imageUrl: string;
}

export class FormLaneApplicationDto extends EBaseDto {
    title: string;
    formApplications: FormApplicationDto[];
}

export class MutationInfo implements Serializable {
    entityClassName: string;
    id: number;
    description: string;
}

export class PVTaskApplicationStatusDto extends EBaseDto {
    taskApplication: TaskApplicationDto;
    taskApplicationStatus: TaskApplicationStatusDto;
    note: string;
}

export class PVWithdrawalRequestStatusDto extends EBaseDto {
    withdrawalRequest: WithdrawalRequestDto;
    withdrawalStatus: WithdrawalStatusDto;
    adminExecutor: AdminDto;
    note: string;
    accountNumber: string;
    bank: BankDto;
    receiptCode: string;
    image: string;
    imageUrl: string;
    statusName: string;
    statusDescription: string;
}

export class ReferralMemberDto extends EBaseDto {
    user: UserDto;
}

export class TaskApplicationDto extends EBaseDto {
    user: UserDto;
    task: TaskDto;
    pvTaskApplicationStatuses: PVTaskApplicationStatusDto[];
    taskApplicationStatus: TaskApplicationStatusDto;
    orderReference: string;
    partnerOrderReference: string;
    viewOrderReference: string;
    groupFormName: string;
    formLaneApplications: FormLaneApplicationDto[];
    referralMembers: ReferralMemberDto[];
    itemFee: number;
    totalFee: number;
}

export class TaskApplicationStatusDto extends EBaseDto {
    name: string;
    description: string;
    sortir: number;
}

export class TaskDto extends EBaseDto {
    partner: PartnerDto;
    title: string;
    subtitle: string;
    published: boolean;
    periodType: string;
    validatingTimeInHour: number;
    description: string;
    additionalInfo: string;
    startPeriod: Date;
    endPeriod: Date;
    applicationLimitType: string;
    applicationLimitCount: number;
    applicationCount: number;
    workerVerificationType: string;
    bannerImage: string;
    bannerImageUrl: string;
    fee: number;
    taskAddressLink: string;
    callbackId: string;
    taskCategory: TaskCategoryDto;
    groupForm: GroupFormDto;
    suggestion: number;
    type: string;
    applyable: boolean;
    message: string;
}

export class TaskPartnerStatus implements Serializable {
    statusOrder: number;
    statusCode: string;
    statusLabel: string;
    statusInfo: string;
    created: Date;
}

export class WithdrawalRequestDto extends EBaseDto {
    user: UserDto;
    amount: number;
    pvWithdrawalRequestStatuses: PVWithdrawalRequestStatusDto[];
    withdrawalStatus: WithdrawalStatusDto;
    imageUrl: string;
}

export class WithdrawalStatusDto extends EBaseDto {
    name: string;
    description: string;
}

export class AttachmentDto extends EBaseDto {
    filename: string;
    fileLink: string;
}

export class SendEmailDto extends EBaseDto {
    subject: string;
    content: string;
    allCustomer: boolean;
    allSubscriber: boolean;
    sendStatus: string;
    sendDirectly: boolean;
    sendAt: Date;
    regularSender: boolean;
    sender: string;
    customerEmails: SendEmailPreference[];
    subscriberEmails: SendEmailPreference[];
    attachments: AttachmentDto[];
    contentType: boolean;
}

export class SendEmailPreference implements Serializable {
    email: string;
    responseStatus: string;
}

export class Form extends EBaseDto {
    tag: string;
    title: string;
    placeHolder: string;
    type: string;
    mandatory: boolean;
    allowedCharacters: string;
    maxLength: number;
    line: number;
    fetchApi: string;
    queryParams: QueryParam[];
    imageDirectory: string;
    dependAnotherTag: boolean;
}

export class FormLane extends EBaseDto {
    title: string;
    cards: Form[];
}

export class GroupFormDto extends EBaseDto {
    name: string;
    formLanes: FormLane[];
}

export class QueryParam implements Serializable {
    key: string;
    reference: string;
}

export class PushNotificationResponse {
    status: number;
    message: string;
}

export class BannerDto extends EBaseDto {
    image: string;
    startDate: Date;
    endDate: Date;
    sortir: number;
    pagelink: string;
    title: string;
    description: string;
    imageLink: string;
}

export class ConfigurationDto extends EBaseDto {
    workingAgreementFile: string;
    workingAgreementFileUrl: string;
    name: string;
    companyName: string;
    companyDescription: string;
    companyAddress: string;
    companyVillage: VillageDto;
    companyPhoneNumber: string;
    companyFaximile: string;
    contactPersonPhoneNumber: string;
    contactWhatsapp: string;
    pageRowSize: number;
    hashtag: string;
    instagramLink: string;
    facebookLink: string;
    twitterLink: string;
    minWithdrawal: number;
    facebookAppId: string;
    googleClientId: string;
    baseApi: string;
    bannerWidth: number;
    bannerHeight: number;
    mobileBannerWidth: number;
    mobileBannerHeight: number;
    regularEmailSender: string;
    mailDomain: string;
    taskBannerWidth: number;
    taskBannerHeight: number;
}

export class DataPage<O> {
    datas: O[];
    pageElement: number;
    totalPage: number;
    totalElement: number;
}

export class FaqDto extends EBaseDto {
    title: string;
    subtitle: string;
    link: string;
    image: string;
    imageLink: string;
}

export class NotificationData implements Serializable {
    notificationId: string;
    referenceId: string;
    referenceClass: string;
    addressLink: string;
}

export class NotificationDto extends EBaseDto implements Serializable {
    title: string;
    message: string;
    topic: string;
    type: string;
    notificationUsers: NotificationUserDto[];
    data: NotificationData;
    admin: AdminDto;
    image: string;
    imageLink: string;
}

export class NotificationUserDto extends EBaseDto implements Serializable {
    user: UserDto;
    notification: NotificationDto;
    read: boolean;
}

export class TaskCategoryDto extends EBaseDto {
    sortir: number;
    name: string;
    defaultIcon: string;
    hexaColor: string;
    topic: string;
    count: number;
    totalTask: number;
    totalAvailableTask: number;
    defaultIconUrl: string;
    coloredIconUrl: string;
}

export class WorkingDayDto extends EBaseDto implements Serializable {
    name: string;
}

export class BusinessAddressDto extends EBaseDto {
    title: string;
    address: string;
    village: VillageDto;
    latitude: number;
    longitude: number;
}

export class CompanyDto extends EBaseDto {
    companyName: string;
    buildDate: Date;
    officePhoneNumber: string;
    officeFax: string;
    companyDescription: string;
    logo: string;
    latitude: number;
    longitude: number;
    businessField: string;
    address: string;
    village: VillageDto;
    corporateType: CorporateTypeDto;
    logoUrl: string;
}

export class CorporateTypeDto extends EBaseDto {
    name: string;
}

export class DirectorDto extends EBaseDto {
    name: string;
    phoneNumber: string;
}

export class NumberOfEmployeeDto extends EBaseDto {
    rangeStart: number;
    rangeEnd: number;
}

export class PVPartnerStatusDto extends EBaseDto {
    partner: PartnerDto;
    partnerStatus: PartnerStatusDto;
    cause: string;
    note: string;
}

export class PartnerDto extends EBaseDto {
    partnerType: string;
    fullName: string;
    website: string;
    email: string;
    image: string;
    imageUrl: string;
    point: number;
    selfPromotion: boolean;
    company: CompanyDto;
    personal: PersonalDto;
    partnerStatus: PartnerStatusDto;
    idCard: string;
    siupOrNwpw: string;
    siupOrNwpwUrl: string;
    idCardUrl: string;
    taskCount: number;
}

export class PartnerStatusDto extends EBaseDto {
    name: string;
    description: string;
}

export class PersonalDto extends EBaseDto {
}

export class BankAccountDto extends EBaseDto {
    bank: BankDto;
    customerName: string;
    accountCode: string;
}

export class Option extends EBaseDto {
    name: string;
}

export class BankDto extends Option {
    label: string;
    code: string;
    image: string;
    imageLink: string;
}

export class PersonalityCharacterDto extends EBaseDto {
    title: string;
    type: string;
    personality: PersonalityDto;
}

export class PersonalityDto extends EBaseDto {
    title: string;
}

export class CityDto extends Option {
    platNumber: string;
    province: ProvinceDto;
    courierId: string;
}

export class DistrictDto extends Option {
    city: CityDto;
    courierId: string;
}

export class ProvinceDto extends Option {
    countryCode: string;
    courierId: string;
}

export class VillageDto extends Option {
    postalCode: string;
    district: DistrictDto;
}

export class ReportCategoryDto extends EBaseDto {
    name: string;
    description: string;
}

export class ReportStatusDto extends EBaseDto {
    name: string;
    description: string;
}

export class MenuDto extends EBaseDto {
}

export class RoleDto extends EBaseDto {
    name: string;
    roleMenus: RoleMenuDto[];
    admins: AdminDto[];
}

export class RoleMenuDto extends EBaseDto {
}

export class PurchaseStatusDto extends EBaseDto {
    name: string;
    description: string;
}

export class AdminDto extends EBaseDto {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    image: string;
    adminStatus: string;
    role: RoleDto;
    fullname: string;
}

export class PersonDto extends EBaseDto {
    email: string;
    homePhoneNumber: string;
    mobilePhone: string;
    firstname: string;
    lastname: string;
    idCardCode: string;
    idCard: string;
    photo: string;
    selfieIdCard: string;
    birthdate: Date;
    gender: string;
    maritalStatus: string;
    latitude: number;
    longitude: number;
    address: string;
    village: VillageDto;
    fullname: string;
    idCardUrl: string;
    photoUrl: string;
    selfieIdCardUrl: string;
}

export class RegistrationDto {
    firstname: string;
    lastname: string;
    mobilePhone: string;
    email: string;
    referralCode: string;
    agentCode: string;
    fullname: string;
}

export class UserDeviceDto extends EBaseDto {
    fcmToken: string;
    platform: string;
    deviceId: string;
}

export class UserDto extends PersonDto {
    agentCode: string;
    referralCode: string;
    organization: string;
    password: string;
    role: string;
    agreeTermAndCondition: boolean;
    firstLogin: boolean;
    totalIncome: number;
    currentBalance: number;
    totalWithdrawal: number;
    onGoingTask: number;
    totalTask: number;
    totalCompletedTask: number;
    totalExpiredTask: number;
    totalRejectedTask: number;
    userStatus: string;
    accountNumber: string;
    accountName: string;
    bank: BankDto;
    verificationStatus: string;
    registration: RegistrationDto;
    userDevices: UserDeviceDto[];
    authorization: string;
    registrationToken: string;
    verified: boolean;
}

export class UserTaskSummary {
    onGoingTask: number;
    totalTask: number;
}

export class WSTask extends WSResponse<any, any> {
    data: TaskDto;
}

export class WSTaskApplication extends WSResponse<any, any> {
    data: TaskApplicationDto;
}

export class WSTaskApplications extends WSResponse<any, any> {
    data: TaskApplicationDto[];
}

export class WSTasks extends WSResponse<any, any> {
    data: TaskDto[];
}

export class WSPersonal extends WSResponse<any, any> {
    data: PersonalDto;
}

export class WSPersonals extends WSResponse<any, any> {
    data: PersonalDto[];
}

export class WSConfiguration extends WSResponse<any, any> {
    data: ConfigurationDto;
}

export class WSConfigurations extends WSResponse<any, any> {
    data: ConfigurationDto[];
}

export class WSNotification extends WSResponse<any, any> {
    data: NotificationDto;
}

export class WSNotifications extends WSResponse<any, any> {
    data: NotificationDto[];
}

export class WSTaskCategories extends WSResponse<any, any> {
    data: TaskCategoryDto[];
}

export class WSTaskCategory extends WSResponse<any, any> {
    data: TaskCategoryDto;
}

export class WSWorkingDay extends WSResponse<any, any> {
    data: WorkingDayDto;
}

export class WSWorkingDays extends WSResponse<any, any> {
    data: WorkingDayDto[];
}

export class WSBank extends WSResponse<any, any> {
    data: BankDto;
}

export class WSBankAccount extends WSResponse<any, any> {
    data: BankAccountDto;
}

export class WSBankAccounts extends WSResponse<any, any> {
    data: BankAccountDto[];
}

export class WSBanks extends WSResponse<any, any> {
    data: BankDto[];
}

export class WSPersonalities extends WSResponse<any, any> {
    data: PersonalityDto[];
}

export class WSPersonality extends WSResponse<any, any> {
    data: PersonalityDto;
}

export class WSPersonalityCharacter extends WSResponse<any, any> {
    data: PersonalityCharacterDto;
}

export class WSPersonalityCharacters extends WSResponse<any, any> {
    data: PersonalityCharacterDto[];
}

export class WSReportCategories extends WSResponse<any, any> {
    data: ReportCategoryDto[];
}

export class WSReportCategory extends WSResponse<any, any> {
    data: ReportCategoryDto;
}

export class WSReportStatus extends WSResponse<any, any> {
    data: ReportStatusDto;
}

export class WSReportStatuses extends WSResponse<any, any> {
    data: ReportStatusDto[];
}

export class WSAdmin extends WSResponse<any, any> {
    data: AdminDto;
}

export class WSAdmins extends WSResponse<any, any> {
    data: AdminDto[];
}

export class WSPerson extends WSResponse<any, any> {
    data: PersonDto;
}

export class WSPersons extends WSResponse<any, any> {
    data: PersonDto[];
}

export class WSRegistration extends WSResponse<any, any> {
    data: RegistrationDto;
}

export class WSRegistrations extends WSResponse<any, any> {
    data: RegistrationDto[];
}

export class WSUser extends WSResponse<any, any> {
    data: UserDto;
}

export class WSUserDevice extends WSResponse<any, any> {
    data: UserDeviceDto;
}

export class WSUserDevices extends WSResponse<any, any> {
    data: UserDeviceDto[];
}

export class WSUsers extends WSResponse<any, any> {
    data: UserDto[];
}

export interface Serializable {
}
