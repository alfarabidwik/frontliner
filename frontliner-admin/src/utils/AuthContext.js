import React from 'react'
import "./StorageUtil"
import {getAdmin, removeStorage, removeData, saveAdminSession} from "./StorageUtil";
import Global from "./Global";
import ToastServive from "react-material-toast";
import SockJsClient from 'react-stomp'

const AuthContext = React.createContext()

export const toast = ToastServive.new({
    place:'topRight', //topLeft topRight bottomLeft bottomRight
    duration:4,
    maxCount:8,
    closable:true
});


//https://medium.freecodecamp.org/how-to-protect-your-routes-with-react-context-717670c4713a
class AuthProvider extends React.Component {
    state = {
        isAuth: getAdmin()!=null,
        admin: getAdmin()
    }

    constructor() {
        super()
        this.login = this.login.bind(this)
        this.logout = this.logout.bind(this)
        this.refresh = this.refresh.bind(this)
    }

    login(admin) {
        // alert(JSON.stringify(admin))
        saveAdminSession(admin, true)
        this.setState({
            admin : getAdmin(),
            isAuth:getAdmin()!=null
        }, () => {
            setTimeout(() => this.setState({ isAuth: true }), 1000)
        })
    }

    logout() {
        // removeStorage()
        removeData(Global.ADMIN)
        removeData(Global.AUTHORIZATION)
        this.setState({
            isAuth: getAdmin()!=null,
            admin: null
        })
    }

    refresh(){
        this.setState({
            isAuth: getAdmin()!=null,
            admin:getAdmin()
        }, () => {
            this.forceUpdate()
        })
    }

    render() {
        return (
            <AuthContext.Provider
                value={{
                    isAuth: this.state.isAuth,
                    admin: this.state.admin,
                    login: this.login,
                    logout: this.logout,
                    refresh:this.refresh,
                }}>
                {this.props.children}
                {
                    this.state.isAuth&&(
                        <SockJsClient
                            url={Global.BASE_URL}
                            topics={[Global.API.LISTEN_TRANSACTION]}
                            onMessage={(message) => {
                                toast.info(message)
                            }}
                            ref={ (client) => { this.clientRef = client }} />
                    )
                }
            </AuthContext.Provider>
        )
    }
}

const AuthConsumer = AuthContext.Consumer

export { AuthProvider, AuthConsumer }