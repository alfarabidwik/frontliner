import axios from "axios"
import Global from "./Global";
import AxiosLogger from 'axios-logger';

const kitchen = axios.create({
    baseURL:Global.BASE_URL,
    timeout:60000
})

if(Global.DEBUG){
    kitchen.interceptors.request.use(AxiosLogger.requestLogger, AxiosLogger.errorLogger);
    kitchen.interceptors.response.use(AxiosLogger.responseLogger, AxiosLogger.errorLogger);
}
export default kitchen