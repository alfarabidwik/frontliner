import CurrencyFormatter from 'currencyformatter.js'
import cloneDeep from 'lodash/cloneDeep';
import {
    _ACCEPTED_RETURN,
    _DELIVERY,
    _DONE,
    _ON_RETURN,
    _ORDER,
    _PACKAGING,
    _PAID,
    _REJECTED,
    CURRENCY_SYMBOL
} from "./Global";
import {getAdmin} from "./StorageUtil";
var dateFormat = require('dateformat');


export function cloneObject(object) {
    return cloneDeep(object)
}

export function parseDate(dateIn, format){
    if(!dateIn){
        return null
    }
    const date = new Date(dateIn)
    if(format){
        try {
            return dateFormat(date, format)
        }catch (e) {
            return null;
        }
    }else{
        return dateFormat(date, "dd/mm/yyyy")
    }
}

export function translate(state){
    if(state){
        return "Active";
    }else{
        return "Inactive";
    }
}

export function sortirMap(key) {
    var sortRed = new Object();
    sortRed['name'] = "Name"
    sortRed['product.name'] = "Product"
    sortRed['product.category.name'] = "Category"
    sortRed['product.heading.name'] = "Heading"
    sortRed['product.brand.name'] = "Brand"
    sortRed['created'] = "Created"
    sortRed['quantity'] = "Quantity"
    sortRed['price'] = "Price"
    sortRed['updated'] = "Updated"
    sortRed['firstname'] = "Name"
    sortRed['role.name'] = "Role"
    sortRed['email'] = "Email"
    sortRed['c.firstname'] = "Name"
    sortRed['c.created'] = "Created"
    sortRed['c.email'] = "Email"
    sortRed['c.phoneNumber'] = "Phone Number"
    sortRed['c.updated'] = "last Update"
    sortRed['transactionCount'] = "Trx Count"
    sortRed['trx.created'] = "Trx Date"
    sortRed['refCode'] = "Ref Code"
    sortRed['item'] = "Item"
    sortRed['fs.created'] = "Created"
    sortRed['fs.updated'] = "Updated"
    sortRed['p.heading.name'] = "Heading"
    sortRed['p.category.name'] = "Category"
    sortRed['p.brand.name'] = "Brand"
    sortRed['p.name'] = "Name"

    sortRed['h.name'] = "Heading"
    sortRed['c.name'] = "Category"
    sortRed['b.name'] = "Brand"

    sortRed['stock'] = "Stock"
    sortRed['startDate'] = "Start Date"
    sortRed['ednDate'] = "End Date"
    sortRed['s.name'] = "Name"
    sortRed['s.created'] = "Created"
    sortRed['s.updated'] = "Updated"
    sortRed['inventoryItem'] = "Inventory"
    sortRed['inventoryQuantity'] = "Quantity"
    sortRed['p.created'] = "Created"
    sortRed['p.updated'] = "Updated"
    sortRed['v.name'] = "Name"
    sortRed['v.created'] = "Created"
    sortRed['v.updated'] = "Updated"
    sortRed['v.startPeriod'] = "Start Period"
    sortRed['v.endPeriod'] = "End Period"
    sortRed['v.start_period'] = "Start Period"
    sortRed['v.end_period'] = "End Period"
    sortRed['typeName'] = "Type"
    sortRed['totalPay'] = "Total Pay"
    sortRed['trx.startCreated'] = "Trx Date"
    sortRed['transactionCount'] = "Trx Count"
    sortRed['favoriteCount'] = "Fav Count"
    sortRed['cartCount'] = "Cart Count"
    sortRed['cic.created'] = "Created"
    sortRed['cartCount'] = "Cart Count"
    sortRed['d.start_date'] = "Start Date"
    sortRed['d.end_date'] = "End Date"

    sortRed['minimumQuantity'] = "Min Quantity"
    sortRed['availableStock'] = "Stock"

    sortRed['customer.firstname'] = "Name"
    sortRed['customer.email'] = "Email"
    sortRed['customer.phoneNumber'] = "Phone Number"

    sortRed['jjv.title'] = "Title"
    sortRed['jjv.fee'] = "Fee"
    sortRed['ja.itemFee'] = "Fee"
    sortRed['ja.updated'] = "Updated"
    sortRed['ja.created'] = "Created"
    sortRed['jt.suggestion'] = "Suggestion"
    sortRed['jt.applicationCount'] = "Task Done"
    sortRed['jt.taskCategory.name'] = "Task Category"

    sortRed['jp.full_name'] = "Fullname"
    sortRed['jp.website'] = "Website"
    sortRed['jp.task_count'] = "Task Count"
    sortRed['jp.created'] = "Created"
    sortRed['jp.updated'] = "Updated"
    sortRed['p.fullName'] = "Partner"


    sortRed['u.created'] = "Created"
    sortRed['u.firstname'] = "Name"
    sortRed['u.email'] = "Email"
    sortRed['u.mobile_phone'] = "Mobile Phone"
    sortRed['u.updated'] = "Updated"
    sortRed['u.total_income'] = "Total Income"

    sortRed['ud.created'] = "Created"
    sortRed['u.agentCode'] = "Agent Code"
    sortRed['u.mobilePhone'] = "Mobile Phone"
    sortRed['u.email'] = "Email"
    sortRed['wr.amount'] = "Amount"
    sortRed['wr.created'] = "Created"
    sortRed['wr.updated'] = "Updated"

    sortRed['bm.created'] = "Created"
    sortRed['bm.updated'] = "Updated"

    return sortRed[key]
}

export function statusColor(statusId){
    if(statusId===_ORDER){
        return "#3dc296"
    }

    if(statusId===_PAID){
        return "#5f86dd"
    }

    if(statusId===_PACKAGING){
        return "#ff8f0c"
    }

    if(statusId===_DELIVERY){
        return "#ff8f0c"
    }
    if(statusId===_DONE){
        return "#3dc296"
    }

    if(statusId===_REJECTED){
        return "#ff2f5f"
    }

    if(statusId===_ON_RETURN){
        return "#ff2f5f"
    }

    if(statusId===_ACCEPTED_RETURN){
        return "#ff2f5f"
    }


}

export function imageSelector(callback, mimeType, maxSize){
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    if(mimeType){
        fileSelector.setAttribute('accept', mimeType);
    }else{
        fileSelector.setAttribute('accept', 'image/x-png, image/jpeg, image/x-icon');
    }
    // fileSelector.setAttribute('accept', 'image/x-png,image/gif,image/jpeg');
    // fileSelector.setAttribute('multiple', 'multiple');
    fileSelector.onchange = function(event) {
        // alert(event.target.value)
        event.preventDefault()
        if (event.target.files && event.target.files[0]) {
            let files = event.target.files || event.dataTransfer.files;
            var fileSize = files[0].size / 1024 ;// in KB
            if(maxSize){
               if(fileSize>maxSize){
                   alert("File cannot be larger than "+maxSize+" Kb")
                   return ;
               }
            }
            var stnkfile = URL.createObjectURL(event.target.files[0])
            callback(stnkfile)
        }
    }
    return fileSelector;
}

export function fileSelector(callback, mimeType){
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    if(mimeType){
        fileSelector.setAttribute('accept', mimeType);
    }
    fileSelector.onchange = function(event) {
        // alert(event.target.value)
        event.preventDefault()
        if (event.target.files && event.target.files[0]) {
            // let files = event.target.files || event.dataTransfer.files;
            // var file = URL.createObjectURL(event.target.files[0])
            var input = event.target
            var reader = new FileReader();
            reader.onload = function(){
                var text = reader.result;
                callback(input.files[0], text)
            };
            reader.readAsText(input.files[0]);
        }
    }
    return fileSelector;
}


export function toCurrency(amount) {
    return amount.toFixed(2);
};

export function isEmpty(input){
    return ((input===undefined) || (input===null) || (input===''))
}

export function allIsEmpty(...inputs){
    var empty = (inputs===undefined) || (inputs===null) || (inputs==='')
    if(!empty){
        loop:
        for (let i = 0; i < inputs.length; i++) {
            var input = inputs[i]
            empty = isEmpty(input)
            if(empty){
                break loop
            }
        }
    }
    return empty ;
}

export function isValidEmail(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}

export function percentToPrice(percent, price){
    if(isEmpty(percent) || percent===0 || isEmpty(price) || price===0){
        return 0 ;
    }
    return price*(percent/100)
}


export function finalPriceByPercent(percent, price){
    if(isEmpty(percent) || percent===0 || isEmpty(price) || price===0){
        return 0 ;
    }
    var finalPrice = price*(percent/100)
    return price - finalPrice
}

export function percentByDiscount(discount, price){
    if(isEmpty(discount) || discount===0 || isEmpty(price) || price===0){
        return 0 ;
    }
    return (discount*100)/price
}

export function navigatePage(props, currentPage) {
    let {pathname} = props.location
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.set('page', currentPage);
    props.history.push(pathname.includes("?")?(pathname+currentUrlParams.toString()):(pathname+"?"+currentUrlParams.toString()))
}

export function changeParam(props, param, value) {
    let {pathname} = props.location
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.set(param, value);
    props.history.push(pathname.includes("?")?(pathname+currentUrlParams.toString()):(pathname+"?"+currentUrlParams.toString()))
}

export function changeParam2(props, param1, value1, param2, value2) {
    let {pathname} = props.location
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.set(param1, value1);
    currentUrlParams.set(param2, value2);
    props.history.push(pathname.includes("?")?(pathname+currentUrlParams.toString()):(pathname+"?"+currentUrlParams.toString()))
}
export function changeParam3(props, param1, value1, param2, value2, param3, value3) {
    let {pathname} = props.location
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.set(param1, value1);
    currentUrlParams.set(param2, value2);
    currentUrlParams.set(param3, value3);
    props.history.push(pathname.includes("?")?(pathname+currentUrlParams.toString()):(pathname+"?"+currentUrlParams.toString()))
}

export function navigateBack(props) {
    props.history.goBack()
}



export function deleteParam(props, param, ignoreNavigate) {
    let {pathname} = props.location
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.delete(param);
    if(!ignoreNavigate){
        props.history.push(pathname.includes("?")?(pathname+currentUrlParams.toString()):(pathname+"?"+currentUrlParams.toString()))
    }
}

export function decimalToFraction(decimal){
    decimal = decimal*100
    let divides = 1*100
    decimal = (decimal/5)/5
    divides = (divides/5)/5
    return [decimal, divides]
}

export function weightFormat(number) {
    let gram = true
    if(number>=1000){
        gram = false
        number = number/1000
    }
    return CurrencyFormatter.format(number, {
        symbol:'',
        decimal:'.',
        group:',',
        pattern:(gram?'#,##0.0 gram':'#,##0.0 kg')+' !'
    })
}


export function currencyFormat(price, symbol) {
    if(!price){
        price = 0
    }
    return CurrencyFormatter.format(price, {
            symbol:(symbol !=undefined)?(symbol+' '):'',
            decimal:',',
            group:'.',
            pattern:(symbol?symbol+' #,##0':'#,##0')
        }
    )
}


export function validatePrivilege(props){
    let admin = getAdmin()
    let currentPath = props.location.pathname
    let valid = false
    let i = 1
    // admin.role.roleMenus.map((item, index)=>{
    //     if(currentPath===item.menu.link){
    //         valid = true
    //     }
    //     i++
    //
    // })
    // if(!valid){
    //     navigateBack(props)
    // }


}


// Object.prototype.push = function( key, value ){
//     this[ key ] = value;
//     return this;
// }
// Object.prototype.remove = function( key){
//     delete this[ key ];
//     return this;
// }


export function hasBeenDelivery(statusId){
    if(statusId){
        if(statusId===_DELIVERY || statusId===_DONE || statusId === _ON_RETURN || statusId === _ACCEPTED_RETURN){
            return true ;
        }
    }
    return false ;
}


// Adds a .toCurrency() method to Numbers.
Number.prototype.toCurrency = function() {
    return toCurrency(this);
}
